﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Pagers
{
    [DataContract]
    public class Pager
    {
        [DataMember(Name = "pageIndex")]
        public int PageIndex { get; set; }

        [DataMember(Name = "pageSize")]
        public int PageSize { get; set; }

        [DataMember(Name = "criteria")]
        public PagerCriteria Criteria { get; set; }
    }


    [DataContract]
    public class PagerCriteria
    {
        [DataMember(Name = "searchTerm")]
        public string SearchTerm { get; set; }

        [DataMember(Name = "filter")]
        public IList<PagerFilter> Filter { get; set; }

        [DataMember(Name = "orderby")]
        public IList<PagerSort> OrderBy { get; set; }

    }

    [DataContract]
    public   class PagerFilter
    {


        [DataMember(Name = "column")]
        public string Column { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "condition")]
        public string Condition { get; set; }

        [DataMember(Name = "value")]
        public object Value { get; set; }
    }

    [DataContract]
    public class PagerSort
    {
        [DataMember(Name = "column")]
        public string Column { get; set; }

        [DataMember(Name = "value")]
        public string Value { get; set; }
    }
}
