﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using PRM.Core.Models;

namespace PRM.Core.Common
{
    public class ReportUtility
    {
        public static LiveBidding GetLiveBiddingReportObject(DataRow row, int count)
        {
            LiveBidding detail = new LiveBidding();
            detail.Vendor = new User();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));

            detail.VendorCompany = new Company();
            detail.VendorCompany.CompanyID = Convert.ToInt32(row.GetColumnValue("COMPANY_ID", detail.VendorCompany.CompanyID.GetType()));
            detail.VendorCompany.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.VendorCompany.CompanyName.GetType()));

            List<KeyValuePair> keyValues = new List<KeyValuePair>();

            for(int i=0; i <= count; i++)
            {
                KeyValuePair keyValue = new KeyValuePair();
                keyValue.Key1 = Convert.ToString(row.GetColumnValue(i + "_BID_TIME", typeof(string)));
                keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue(i + "_MIN_PRICE", keyValue.DecimalVal.GetType()));
                keyValues.Add(keyValue);
            }

            detail.ArrayKeyValue = keyValues.ToArray();

            return detail;
        }

        public static KeyValuePair GetKeyValuePair(DataRow row, string KeyName, string valueName, Type valueType)
        {
            KeyValuePair keyValue = new KeyValuePair();
            keyValue.Key1 = Convert.ToString((row.GetColumnValue(KeyName, typeof(string))));
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue(valueName, valueType));

            return keyValue;
        }












        public static LiveBidding GetLiveBiddingReportObject2(DataRow row)
        {
            LiveBidding detail = new LiveBidding();
            detail.Vendor = new User();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));

            detail.VendorCompany = new Company();
            detail.VendorCompany.CompanyID = Convert.ToInt32(row.GetColumnValue("COMPANY_ID", detail.VendorCompany.CompanyID.GetType()));
            detail.VendorCompany.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.VendorCompany.CompanyName.GetType()));

            List<KeyValuePair> keyValues = new List<KeyValuePair>();







            detail.ArrayKeyValue = keyValues.ToArray();

            return detail;

            /*
            KeyValuePair keyValue = new KeyValuePair();
            keyValue.Key1 = "15 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("15_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);

            keyValue = new KeyValuePair();
            keyValue.Key1 = "14 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("14_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);

            keyValue = new KeyValuePair();
            keyValue.Key1 = "12 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("12_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);

            keyValue = new KeyValuePair();
            keyValue.Key1 = "10 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("10_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);

            keyValue = new KeyValuePair();
            keyValue.Key1 = "8 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("8_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);

            keyValue = new KeyValuePair();
            keyValue.Key1 = "6 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("6_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);

            keyValue = new KeyValuePair();
            keyValue.Key1 = "4 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("4_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);


            keyValue = new KeyValuePair();
            keyValue.Key1 = "2 Min";
            keyValue.DecimalVal = Convert.ToDecimal(row.GetColumnValue("2_MIN_PRICE", keyValue.DecimalVal.GetType()));
            keyValues.Add(keyValue);
            */
            
        }














        public static ItemWiseReport GetItemWiseReportObject(DataRow row)
        {
            ItemWiseReport detail = new ItemWiseReport();
            //Least Item Bidder
            detail.LeastItemVendor = new User();
            detail.LeastItemVendor.UserID = Convert.ToInt32(row.GetColumnValue("LEAST_ITEM_BIDDER_ID", detail.LeastItemVendor.UserID.GetType()));
            detail.LeastItemVendor.FirstName = Convert.ToString(row.GetColumnValue("LEAST_ITEM_BIDDER_FNAME", detail.LeastItemVendor.FirstName.GetType()));
            detail.LeastItemVendor.LastName = Convert.ToString(row.GetColumnValue("LEAST_ITEM_BIDDER_LNAME", detail.LeastItemVendor.LastName.GetType()));

            detail.LeastItemVendorCompany = new Company();
            detail.LeastItemVendorCompany.CompanyID = Convert.ToInt32(row.GetColumnValue("LEAST_ITEM_BIDDER_COMP_ID", detail.LeastItemVendorCompany.CompanyID.GetType()));
            detail.LeastItemVendorCompany.CompanyName = Convert.ToString(row.GetColumnValue("LEAST_ITEM_BIDDER_COMP_NAME", detail.LeastItemVendorCompany.CompanyName.GetType()));

            //Least Bidder
            detail.LeastVendor = new User();
            detail.LeastVendor.UserID = Convert.ToInt32(row.GetColumnValue("LEAST_BIDDER_ID", detail.LeastVendor.UserID.GetType()));
            detail.LeastVendor.FirstName = Convert.ToString(row.GetColumnValue("LEAST_BIDDER_FNAME", detail.LeastVendor.FirstName.GetType()));
            detail.LeastVendor.LastName = Convert.ToString(row.GetColumnValue("LEAST_BIDDER_LNAME", detail.LeastVendor.LastName.GetType()));

            detail.LeastVendorCompany = new Company();
            detail.LeastVendorCompany.CompanyID = Convert.ToInt32(row.GetColumnValue("LEAST_BIDDER_COMP_ID", detail.LeastVendorCompany.CompanyID.GetType()));
            detail.LeastVendorCompany.CompanyName = Convert.ToString(row.GetColumnValue("LEAST_BIDDER_COMP_NAME", detail.LeastVendorCompany.CompanyName.GetType()));

            detail.LeastBidderPrice = Convert.ToDecimal(row.GetColumnValue("LEAST_BIDDER_PRICE", detail.LeastBidderPrice.GetType()));
            detail.LeastBidderPrice = Convert.ToDecimal(row.GetColumnValue("UNIT_PRICE", detail.UnitPrice.GetType()));
            detail.LeastBidderUnitPrice = Convert.ToDecimal(row.GetColumnValue("LEAST_BIDDER_UNIT_PRICE", detail.LeastBidderUnitPrice.GetType()));

            detail.ItemDetails = new RequirementItems();
            detail.ItemDetails.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.ItemDetails.ProductIDorName.GetType()));
            detail.ItemDetails.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemDetails.ItemID.GetType()));
            detail.ItemDetails.ProductDescription = Convert.ToString(row.GetColumnValue("DESCRIPTION", detail.ItemDetails.ProductDescription.GetType()));

            return detail;
        }

        public static DeliveryTimeLine GetIDeliveryTimelineObject(DataRow row)
        {
            DeliveryTimeLine detail = new DeliveryTimeLine();
            //Least Item Bidder
            detail.Vendor = new User();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            detail.Vendor.PhoneNum = Convert.ToString(row.GetColumnValue("U_PHONE", detail.Vendor.PhoneNum.GetType()));

            detail.VendorCompany = new Company();
            detail.VendorCompany.CompanyID = Convert.ToInt32(row.GetColumnValue("COMPANY_ID", detail.VendorCompany.CompanyID.GetType()));
            detail.VendorCompany.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.VendorCompany.CompanyName.GetType()));

            detail.VendorInitPrice = Convert.ToDecimal(row.GetColumnValue("VEND_INIT_PRICE", detail.VendorInitPrice.GetType()));
            detail.VendorRunPrice = Convert.ToDecimal(row.GetColumnValue("VEND_RUN_PRICE", detail.VendorRunPrice.GetType()));
            detail.ReductionPrice = Convert.ToDecimal(row.GetColumnValue("RED_PERCENT", detail.ReductionPrice.GetType()));
            detail.ExpectedDelivery = Convert.ToString(row.GetColumnValue("EXPECTED_DELIVERY", detail.ExpectedDelivery.GetType()));
            detail.PaymentScheduleDate = Convert.ToString(row.GetColumnValue("PAYMENT_SCHEDULE_DATE", detail.PaymentScheduleDate.GetType()));

            return detail;
        }
    }
}