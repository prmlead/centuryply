﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using PRM.Core.Models;

namespace PRM.Core.Common
{
    public class TechEvalUtility
    {
        public static Questionnaire GetQuestionnaireObject(DataRow row)
        {
            Questionnaire detail = new Questionnaire();
            detail.EvalID = Convert.ToInt32(row.GetColumnValue("EVAL_ID", detail.EvalID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.UserID.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("QUEST_TITLE", detail.Title.GetType()));
            detail.Description = Convert.ToString(row.GetColumnValue("QUEST_DESC", detail.Description.GetType()));

            if (detail.StartDate == null)
            {
                detail.StartDate = DateTime.MinValue;
            }
            if (detail.EndDate == null)
            {
                detail.EndDate = DateTime.MaxValue;
            }

            detail.StartDate = Convert.ToDateTime(row.GetColumnValue("QUEST_START_DATE", detail.StartDate.GetType()));
            detail.EndDate = Convert.ToDateTime(row.GetColumnValue("QUEST_END_DATE", detail.EndDate.GetType()));
            detail.Category = Convert.ToString(row.GetColumnValue("QUEST_CATEGORY", detail.Description.GetType()));

            detail.TotalMarks = Convert.ToDouble(row.GetColumnValue("TOTAL_MARKS", detail.TotalMarks.GetType()));
            detail.IsApproved = Convert.ToInt32(row.GetColumnValue("IS_APPROVED", detail.IsApproved.GetType()));
            detail.IsSentToVendor = Convert.ToInt32(row.GetColumnValue("IS_EVAL_SENT", detail.IsApproved.GetType()));

            return detail;
        }

        public static Question GetQuestion(DataRow row)
        {
            Question detail = new Question();
            detail.EvalID = Convert.ToInt32(row.GetColumnValue("EVAL_ID", detail.EvalID.GetType()));
            detail.QuestionID = Convert.ToInt32(row.GetColumnValue("Q_ID", detail.QuestionID.GetType()));
            detail.Text = Convert.ToString(row.GetColumnValue("Q_TEXT", detail.Text.GetType()));
            detail.Options = Convert.ToString(row.GetColumnValue("Q_OPTIONS", detail.Options.GetType()));
            detail.Type = Convert.ToString(row.GetColumnValue("Q_TYPE", detail.Type.GetType()));
            detail.AttachmentID = Convert.ToInt32(row.GetColumnValue("Q_ATTACH_ID", detail.AttachmentID.GetType()));
            detail.Marks = Convert.ToDecimal(row.GetColumnValue("Q_MARKS", detail.Marks.GetType()));

            return detail;
        }

        public static Answer GetAnswer(DataRow row)
        {
            Answer detail = new Answer();
            detail.Vendor = new UserDetails();
            detail.AnswerID = Convert.ToInt32(row.GetColumnValue("RES_ID", detail.AnswerID.GetType()));
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.AnswerID.GetType()));
            detail.AnswerText = Convert.ToString(row.GetColumnValue("RES_TEXT", detail.AnswerText.GetType()));
            detail.ResComments = Convert.ToString(row.GetColumnValue("RES_COMMENTS", detail.ResComments.GetType()));
            detail.UserMarks = Convert.ToDecimal(row.GetColumnValue("RES_MARKS", detail.UserMarks.GetType()));
            detail.AttachmentID = Convert.ToInt32(row.GetColumnValue("RES_ATTACH_ID", detail.AttachmentID.GetType()));
            
            return detail;
        }

        public static Answer GetVendorReport(DataRow row)
        {
            Answer detail = new Answer();
            detail.Vendor = new UserDetails();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            detail.Vendor.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.Vendor.CompanyName.GetType()));
            detail.UserMarks = Convert.ToDecimal(row.GetColumnValue("RES_MARKS", detail.UserMarks.GetType()));

            return detail;
        }

        public static TechEval GetTechEvalReport(DataRow row)
        {
            TechEval detail = new TechEval();
            detail.Vendor = new UserDetails();
            detail.Vendor.FirstName = string.Empty;
            detail.Vendor.LastName = string.Empty;
            detail.Vendor.CompanyName = string.Empty;
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            detail.Vendor.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.Vendor.CompanyName.GetType()));


            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.EvalID = Convert.ToInt32(row.GetColumnValue("EVAL_ID", detail.EvalID.GetType()));
            detail.TotalMarks = Convert.ToDecimal(row.GetColumnValue("TOTAL_MARKS", detail.TotalMarks.GetType()));
            detail.IsApproved = Convert.ToInt32(row.GetColumnValue("IS_APPROVED", detail.IsApproved.GetType()));
            detail.DateModified = Convert.ToDateTime(row.GetColumnValue("DATE_MODIFIED", detail.DateModified.GetType()));
            detail.DateCreated = Convert.ToDateTime(row.GetColumnValue("DATE_CREATED", detail.DateModified.GetType()));
            detail.ModifiedBY = Convert.ToInt32(row.GetColumnValue("MODIFIED_BY", detail.ModifiedBY.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("COMMENTS", detail.Comments.GetType()));

            detail.IsAnswered = Convert.ToInt32(row.GetColumnValue("IS_ANSWERED", detail.IsAnswered.GetType()));

            

            return detail;
        }

        public static DataSet SaveAnswerEntity(Answer answer, MySqlCommand cmd)
        {
            cmd.CommandText = "te_RecordResponse";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_RES_ID", answer.AnswerID));
            cmd.Parameters.Add(new MySqlParameter("P_Q_ID", answer.QuestionDetails.QuestionID));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", answer.Vendor.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_RES_TEXT", answer.AnswerText));
            cmd.Parameters.Add(new MySqlParameter("P_RES_COMMENTS", answer.ResComments));
            cmd.Parameters.Add(new MySqlParameter("P_RES_ATTACH_ID", answer.AttachmentID));
            cmd.Parameters.Add(new MySqlParameter("P_MARKS", answer.UserMarks));
            cmd.Parameters.Add(new MySqlParameter("P_EVAL_ID", answer.QuestionDetails.EvalID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet SaveQuestionnaireEntity(Questionnaire questionnaire, MySqlCommand cmd)
        {
            cmd.CommandText = "te_CreateQuestionnaire";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_EVAL_ID", questionnaire.EvalID));
            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", questionnaire.ReqID));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", questionnaire.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_QUEST_TITLE", questionnaire.Title));
            cmd.Parameters.Add(new MySqlParameter("P_QUEST_DESC", questionnaire.Description));
            cmd.Parameters.Add(new MySqlParameter("P_QUEST_START_DATE", questionnaire.StartDate));
            cmd.Parameters.Add(new MySqlParameter("P_QUEST_END_DATE", questionnaire.EndDate));
            cmd.Parameters.Add(new MySqlParameter("P_CATEGORY", questionnaire.Category));
            cmd.Parameters.Add(new MySqlParameter("P_IS_EVAL_SENT", questionnaire.IsSentToVendor));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet AssignQuestionnaireEntity(Questionnaire questionnaire, MySqlCommand cmd)
        {
            cmd.CommandText = "te_AssignQuestionnaire";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_EVAL_ID", questionnaire.EvalID));
            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", questionnaire.ReqID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet SaveVendorForQuestionnaireEntity(Questionnaire questionnaire, VendorDetails vendor, MySqlCommand cmd)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = "te_SaveVendorForQuestionnaire";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_EVAL_ID", questionnaire.EvalID));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", vendor.VendorID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet SaveQuestionEntity(Question question, MySqlCommand cmd)
        {
            cmd.Parameters.Clear();
            cmd.CommandText = "te_CreateQuestion";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_EVAL_ID", question.EvalID));
            cmd.Parameters.Add(new MySqlParameter("P_Q_ID", question.QuestionID));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", question.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_Q_TYPE", question.Type));
            cmd.Parameters.Add(new MySqlParameter("P_Q_TEXT", question.Text));
            cmd.Parameters.Add(new MySqlParameter("P_Q_OPTIONS", question.Options));
            cmd.Parameters.Add(new MySqlParameter("P_Q_ATTACH_ID", question.AttachmentID));
            cmd.Parameters.Add(new MySqlParameter("P_Q_MARKS", question.Marks));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet SaveTechEvalEntity(TechEval techeval, MySqlCommand cmd)
        {
            cmd.CommandText = "te_SaveTechEvaluation";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", techeval.ReqID));
            cmd.Parameters.Add(new MySqlParameter("P_EVAL_ID", techeval.EvalID));            
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", techeval.Vendor.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_IS_APPROVED", techeval.IsApproved));
            cmd.Parameters.Add(new MySqlParameter("P_TOTAL_MARKS", techeval.TotalMarks));
            cmd.Parameters.Add(new MySqlParameter("P_USER", techeval.ModifiedBY));
            cmd.Parameters.Add(new MySqlParameter("P_COMMENTS", techeval.Comments));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static VendorDetails GetUserDetails(DataRow row)
        {
            VendorDetails detail = new VendorDetails();
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.VendorID.GetType()));
            detail.VendorName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.VendorName.GetType()));
            detail.VendorName += " " + Convert.ToString(row.GetColumnValue("U_LNAME", detail.VendorName.GetType()));
            detail.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.CompanyName.GetType()));

            return detail;
        }
    }
}