﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using PRM.Core.Models;

namespace PRM.Core.Common
{
    public class POUtility
    {
        public static POVendor GetPOVendorObject(DataRow row)
        {
            POVendor detail = new POVendor();
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));
            detail.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.ProductIDorName.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.Vendor = new UserDetails();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.Vendor.UserID.GetType()));
            detail.Vendor.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.Vendor.FirstName.GetType()));
            detail.Vendor.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.Vendor.LastName.GetType()));
            //detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", typeof(DateTime)));
            detail.ReqQuantity = Convert.ToDecimal(row.GetColumnValue("REQ_QUANTITY", detail.ReqQuantity.GetType()));
            detail.POQuantity = Convert.ToDecimal(row.GetColumnValue("PO_QUANTITY", detail.POQuantity.GetType()));
            detail.VendorPOQuantity = Convert.ToDecimal(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.VendorPOQuantity.GetType()));
            detail.Price = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.Price.GetType()));
            detail.Price = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.Price.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("PO_STATUS", detail.Status.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            
            return detail;
        }





        public static POVendor GetDescPoObject(DataRow row)
        {
            POVendor detail = new POVendor();
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.Vendor = new UserDetails();
            detail.Vendor.UserID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.Vendor.UserID.GetType()));
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.VendorID.GetType()));
            detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", detail.ExpectedDeliveryDate.GetType()));           
            detail.Price = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.Price.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("PO_STATUS", detail.Status.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", detail.Title.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.Title.GetType()));

            return detail;
        }



        public static DataSet SaveDescPoInfoEntity(POVendor povendor, MySqlCommand cmd)
        {
            cmd.CommandText = "po_SaveDesPoInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_PO_ID", povendor.POID));
            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", povendor.ReqID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_ID", povendor.VendorID));

            //povendor.ExpectedDeliveryDate = DateTime.MaxValue;

            cmd.Parameters.Add(new MySqlParameter("P_EXPECTED_DELIVERY_DATE", povendor.ExpectedDeliveryDate));
            cmd.Parameters.Add(new MySqlParameter("P_PO_PRICE", povendor.Price));
            cmd.Parameters.Add(new MySqlParameter("P_PO_COMMENTS", povendor.Comments));
            cmd.Parameters.Add(new MySqlParameter("P_PO_STATUS", povendor.Status));
            cmd.Parameters.Add(new MySqlParameter("P_CREATED_BY", povendor.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_MODIFIED_BY", povendor.ModifiedBY));
            cmd.Parameters.Add(new MySqlParameter("P_PURCHASE_ORDER_ID", povendor.PurchaseID));
            cmd.Parameters.Add(new MySqlParameter("P_PO_LINK", povendor.POLink));
            cmd.Parameters.Add(new MySqlParameter("P_DELIVERY_ADDR", povendor.DeliveryAddress));
            cmd.Parameters.Add(new MySqlParameter("P_INDENT_ID", povendor.IndentID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }


        public static DispatchTrack GetDescDispatchObject(DataRow row)
        {

            DispatchTrack detail = new DispatchTrack();

            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));

            //detail.PoVendors = new POVendor();
            //detail.PoVendors.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.PoVendors.POID.GetType()));

            detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
            detail.DispatchType = Convert.ToString(row.GetColumnValue("DISPATCH_TYPE", detail.DispatchType.GetType()));
            detail.DispatchComments = Convert.ToString(row.GetColumnValue("DISPATCH_COMMENTS", detail.DispatchComments.GetType()));

            detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));

            detail.DispatchMode = Convert.ToString(row.GetColumnValue("DISPATCH_MODE", detail.DispatchMode.GetType()));
            detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
            detail.DispatchLink = Convert.ToString(row.GetColumnValue("DISPATCH_LINK", detail.DispatchLink.GetType()));

            detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
            detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));
            detail.RecivedComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.RecivedComments.GetType()));
            detail.RecivedCode = Convert.ToString(row.GetColumnValue("RECEIVED_CODE", detail.RecivedCode.GetType()));
            detail.RecivedLink = Convert.ToInt32(row.GetColumnValue("RECEIVED_LINK", detail.RecivedLink.GetType()));
            detail.RecivedBy = Convert.ToString(row.GetColumnValue("RECEIVED_BY", detail.RecivedBy.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("STATUS", detail.Status.GetType()));

            return detail;
        }

        public static DataSet SaveDesDispatchTrackObject(DispatchTrack dispatchtrack, POVendor povendor, MySqlCommand cmd)
        {
            cmd.CommandText = "po_SaveDispatchTrack";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter("P_DT_ID", dispatchtrack.DTID));
            cmd.Parameters.Add(new MySqlParameter("P_PO_ID", povendor.POID));

            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_QUANTITY", dispatchtrack.DispatchQuantity));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_DATE", null));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_TYPE", dispatchtrack.DispatchType));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_COMMENTS", dispatchtrack.DispatchComments));

            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_CODE", dispatchtrack.DispatchCode));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_MODE", dispatchtrack.DispatchMode));
            cmd.Parameters.Add(new MySqlParameter("P_DELIVERY_TRACK_ID", dispatchtrack.DeliveryTrackID));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_LINK", dispatchtrack.DispatchLink));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_QUANTITY", dispatchtrack.RecivedQuantity));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_DATE", null));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_COMMENTS", dispatchtrack.RecivedComments));

            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_CODE", dispatchtrack.RecivedCode));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_LINK", dispatchtrack.RecivedLink));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_BY", dispatchtrack.RecivedBy));
            cmd.Parameters.Add(new MySqlParameter("P_STATUS", dispatchtrack.Status));
            cmd.Parameters.Add(new MySqlParameter("P_CREATED_BY", dispatchtrack.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_MODIFIED_BY", dispatchtrack.ModifiedBY));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }


















        

       

        public static DataSet SavePOInfoEntity(POVendor poVendor, MySqlCommand cmd)
        {
            cmd.CommandText = "po_SavePoInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_PO_ID", poVendor.POID));
            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", poVendor.ReqID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_ID", poVendor.Vendor.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_ITEM_ID", poVendor.ItemID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_PO_QUANTITY", poVendor.VendorPOQuantity));
            cmd.Parameters.Add(new MySqlParameter("P_PO_QUANTITY", poVendor.POQuantity));

            poVendor.ExpectedDeliveryDate = DateTime.Now;

            cmd.Parameters.Add(new MySqlParameter("P_EXPECTED_DELIVERY_DATE", poVendor.ExpectedDeliveryDate));
            cmd.Parameters.Add(new MySqlParameter("P_PO_PRICE", poVendor.Price));
            cmd.Parameters.Add(new MySqlParameter("P_PO_COMMENTS", poVendor.Comments));
            cmd.Parameters.Add(new MySqlParameter("P_PO_STATUS", poVendor.Status));
            cmd.Parameters.Add(new MySqlParameter("P_MODIFIED_BY", poVendor.ModifiedBY));
            cmd.Parameters.Add(new MySqlParameter("P_CREATED_BY", poVendor.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_PURCHASE_ORDER_ID", poVendor.PurchaseID));
            cmd.Parameters.Add(new MySqlParameter("P_PO_LINK", poVendor.POLink));
            cmd.Parameters.Add(new MySqlParameter("P_DELIVERY_ADDR", poVendor.DeliveryAddress));
            cmd.Parameters.Add(new MySqlParameter("P_INDENT_ID", poVendor.IndentID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        
        public static VendorDetails GetUserDetails(DataRow row)
        {
            VendorDetails detail = new VendorDetails();
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.VendorID.GetType()));
            detail.VendorName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.VendorName.GetType()));
            detail.VendorName += " " + Convert.ToString(row.GetColumnValue("U_LNAME", detail.VendorName.GetType()));
            detail.CompanyName = Convert.ToString(row.GetColumnValue("COMPANY_NAME", detail.CompanyName.GetType()));

            return detail;
        }







        //public static DispatchTrack GetDispatchTrackObject(DataRow row)
        //{

        //    DispatchTrack detail = new DispatchTrack();
        //    detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));

        //    detail.PoVendors = new POVendor();
        //    detail.PoVendors.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.PoVendors.POID.GetType()));
        //    detail.PoVendors.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.PoVendors.ReqID.GetType()));
        //    detail.PoVendors.VendorID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.PoVendors.VendorID.GetType()));

        //    detail.POInfo = new POInformation();

        //    detail.POInfo.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.POInfo.ItemID.GetType()));

        //    detail.DispatchQuantity = Convert.ToDecimal(row.GetColumnValue("DISPATCH_QUANTITY", detail.DispatchQuantity.GetType()));
        //    detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
        //    detail.DispatchType = Convert.ToString(row.GetColumnValue("DISPATCH_TYPE", detail.DispatchType.GetType()));
        //    detail.DispatchComments = Convert.ToString(row.GetColumnValue("DISPATCH_COMMENTS", detail.DispatchComments.GetType()));
        //    detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));
        //    detail.DispatchMode = Convert.ToString(row.GetColumnValue("DISPATCH_MODE", detail.DispatchMode.GetType()));

        //    detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
        //    detail.DispatchLink = Convert.ToString(row.GetColumnValue("DISPATCH_LINK", detail.DispatchLink.GetType()));

        //    detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
        //    detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));

        //    detail.RecivedComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.RecivedComments.GetType()));
        //    detail.RecivedCode = Convert.ToString(row.GetColumnValue("RECEIVED_CODE", detail.RecivedCode.GetType()));
        //    detail.RecivedLink = Convert.ToInt32(row.GetColumnValue("RECEIVED_LINK", detail.RecivedLink.GetType()));
        //    detail.RecivedBy = Convert.ToString(row.GetColumnValue("RECEIVED_BY", detail.RecivedBy.GetType()));
        //    detail.Status = Convert.ToString(row.GetColumnValue("STATUS", detail.Status.GetType()));

        //    detail.PoVendors.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PoVendors.PurchaseID.GetType()));
        //    detail.PoVendors.VendorPOQuantity = Convert.ToDecimal(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.PoVendors.VendorPOQuantity.GetType()));

        //    detail.PoVendors.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.PoVendors.IndentID.GetType()));



        //    return detail;
        //}












        public static UserDetails GetVendorPoObject(DataRow row)
        {
            UserDetails detail = new UserDetails();

            detail.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.UserID.GetType()));
            detail.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.FirstName.GetType()));
            detail.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.LastName.GetType()));            

            return detail;
        }

        public static Requirement GetVendorPoReqObject(DataRow row)
        {
            Requirement detail = new Requirement();

            detail.RequirementID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.RequirementID.GetType()));
            detail.CustomerID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.CustomerID.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", detail.Title.GetType()));
            
            return detail;
        }

        public static POItems GetPoObject(DataRow row)
        {
            POItems detail = new POItems();

            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));

            return detail;
        }


        public static DispatchTrack GetPaymentTrackObject(DataRow row)
        {
            DispatchTrack detail = new DispatchTrack();

            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
            detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));
            detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
            detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));
            detail.ReceivedQtyPrice = Convert.ToInt32(row.GetColumnValue("RECEIVED_QTY_PRICE", detail.ReceivedQtyPrice.GetType()));

            return detail;
        }

        public static PaymentInfo GetPaymentInfoObject(DataRow row)
        {
            PaymentInfo detail = new PaymentInfo();

            detail.PDID = Convert.ToInt32(row.GetColumnValue("PD_ID", detail.PDID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.PaymentAmount = Convert.ToDecimal(row.GetColumnValue("PAYMENT_AMOUNT", detail.PaymentAmount.GetType()));
            detail.PaymentDate = Convert.ToDateTime(row.GetColumnValue("PAYMENT_DATE", detail.PaymentDate.GetType()));
            detail.PaymentComments = Convert.ToString(row.GetColumnValue("PAYMENT_COMMENTS", detail.PaymentComments.GetType()));
            detail.PaymentCode = Convert.ToString(row.GetColumnValue("PAYMENT_CODE", detail.PaymentCode.GetType()));
            detail.TransactionID = Convert.ToString(row.GetColumnValue("PAYMENT_TRANSACT_ID", detail.TransactionID.GetType()));
            detail.PaymentMode = Convert.ToString(row.GetColumnValue("PAYMENT_MODE", detail.PaymentMode.GetType()));
            detail.AckComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.AckComments.GetType()));
           

            return detail;
        }

        public static POItems GetVendorPoItemsObject(DataRow row)
        {
            POItems detail = new POItems();            

            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.VendorID.GetType()));
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));

            detail.VendorPOQuantity = Convert.ToInt32(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.VendorPOQuantity.GetType()));
            detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", detail.ExpectedDeliveryDate.GetType()));
            detail.POPrice = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.POPrice.GetType()));
            detail.VendorPrice = Convert.ToDecimal(row.GetColumnValue("VENDOR_PRICE", detail.VendorPrice.GetType()));

            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.Status = Convert.ToString(row.GetColumnValue("PO_STATUS", detail.Status.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.IndentID.GetType()));
            detail.POLink = Convert.ToString(row.GetColumnValue("PO_LINK", detail.POLink.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            detail.ReqQuantity = Convert.ToDecimal(row.GetColumnValue("REQ_QUANTITY", detail.ReqQuantity.GetType()));
            detail.POQuantity = Convert.ToDecimal(row.GetColumnValue("PO_QUANTITY", detail.POQuantity.GetType()));
            detail.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.IndentID.GetType()));

            detail.CGst = Convert.ToDecimal(row.GetColumnValue("C_GST", detail.CGst.GetType()));
            detail.SGst = Convert.ToDecimal(row.GetColumnValue("S_GST", detail.SGst.GetType()));
            detail.IGst = Convert.ToDecimal(row.GetColumnValue("I_GST", detail.IGst.GetType()));
            detail.VendorTotalPrice = Convert.ToDecimal(row.GetColumnValue("REVICED_PRICE", detail.VendorTotalPrice.GetType()));
            
            

            return detail;
        }



        public static DataSet SavePOItemEntity(POItems poitems, VendorPO vendorpo, Requirement req, UserDetails vendor, MySqlCommand cmd)
        {
            cmd.CommandText = "po_SavePoInfo";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_PO_ID", poitems.POID));
            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", req.RequirementID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_ID", vendor.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_ITEM_ID", poitems.ItemID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_PO_QUANTITY", poitems.VendorPOQuantity));
            cmd.Parameters.Add(new MySqlParameter("P_PO_QUANTITY", poitems.POQuantity));

            //poitems.ExpectedDeliveryDate = DateTime.Now;

            cmd.Parameters.Add(new MySqlParameter("P_EXPECTED_DELIVERY_DATE", poitems.ExpectedDeliveryDate));
            cmd.Parameters.Add(new MySqlParameter("P_PO_PRICE", poitems.POPrice));
            cmd.Parameters.Add(new MySqlParameter("P_PO_COMMENTS", poitems.Comments));
            cmd.Parameters.Add(new MySqlParameter("P_PO_STATUS", poitems.Status));
            cmd.Parameters.Add(new MySqlParameter("P_MODIFIED_BY", poitems.ModifiedBY));
            cmd.Parameters.Add(new MySqlParameter("P_CREATED_BY", poitems.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_PURCHASE_ORDER_ID", poitems.PurchaseID));
            cmd.Parameters.Add(new MySqlParameter("P_PO_LINK", vendorpo.POLink));
            cmd.Parameters.Add(new MySqlParameter("P_DELIVERY_ADDR", poitems.DeliveryAddress));
            cmd.Parameters.Add(new MySqlParameter("P_INDENT_ID", poitems.IndentID));

            cmd.Parameters.Add(new MySqlParameter("P_PO_TOTAL_PRICE", poitems.PoTotalPrice));
            

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }




        public static DispatchTrack GetDispatchTrackObject(DataRow row)
        {           
            DispatchTrack detail = new DispatchTrack();

            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.IndentID.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.DispatchQuantity = Convert.ToDecimal(row.GetColumnValue("DISPATCH_QUANTITY", detail.DispatchQuantity.GetType()));
            detail.DispatchDate = Convert.ToDateTime(row.GetColumnValue("DISPATCH_DATE", detail.DispatchDate.GetType()));
            detail.DispatchType = Convert.ToString(row.GetColumnValue("DISPATCH_TYPE", detail.DispatchType.GetType()));
            detail.DispatchComments = Convert.ToString(row.GetColumnValue("DISPATCH_COMMENTS", detail.DispatchComments.GetType()));
            detail.DispatchCode = Convert.ToString(row.GetColumnValue("DISPATCH_CODE", detail.DispatchCode.GetType()));
            detail.DispatchMode = Convert.ToString(row.GetColumnValue("DISPATCH_MODE", detail.DispatchMode.GetType()));
            detail.DeliveryTrackID = Convert.ToString(row.GetColumnValue("DELIVERY_TRACK_ID", detail.DeliveryTrackID.GetType()));
            detail.DispatchLink = Convert.ToString(row.GetColumnValue("DISPATCH_LINK", detail.DispatchLink.GetType()));

            detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
            detail.RecivedDate = Convert.ToDateTime(row.GetColumnValue("RECEIVED_DATE", detail.RecivedDate.GetType()));
            detail.RecivedComments = Convert.ToString(row.GetColumnValue("RECEIVED_COMMENTS", detail.RecivedComments.GetType()));
            detail.RecivedCode = Convert.ToString(row.GetColumnValue("RECEIVED_CODE", detail.RecivedCode.GetType()));
            detail.RecivedLink = Convert.ToInt32(row.GetColumnValue("RECEIVED_LINK", detail.RecivedLink.GetType()));
            detail.RecivedBy = Convert.ToString(row.GetColumnValue("RECEIVED_BY", detail.RecivedBy.GetType()));

            detail.ReturnQuantity = Convert.ToDecimal(row.GetColumnValue("RETURN_QUANTITY", detail.ReturnQuantity.GetType()));

            detail.Status = Convert.ToString(row.GetColumnValue("STATUS", detail.Status.GetType()));

            return detail;
        }

        public static POItems GetDispatchPoItem(DataRow row)
        {
            POItems detail = new POItems();
            detail.DTID = Convert.ToInt32(row.GetColumnValue("DT_ID", detail.DTID.GetType()));
            detail.POID = Convert.ToInt32(row.GetColumnValue("PO_ID", detail.POID.GetType()));
            detail.ReqID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.ReqID.GetType()));
            detail.VendorID = Convert.ToInt32(row.GetColumnValue("VENDOR_ID", detail.VendorID.GetType()));
            detail.ItemID = Convert.ToInt32(row.GetColumnValue("ITEM_ID", detail.ItemID.GetType()));
            detail.PurchaseID = Convert.ToString(row.GetColumnValue("PURCHASE_ORDER_ID", detail.PurchaseID.GetType()));
            detail.VendorPOQuantity = Convert.ToDecimal(row.GetColumnValue("VENDOR_PO_QUANTITY", detail.VendorPOQuantity.GetType()));
            detail.IndentID = Convert.ToString(row.GetColumnValue("INDENT_ID", detail.IndentID.GetType()));
            detail.ProductIDorName = Convert.ToString(row.GetColumnValue("PROD_ID", detail.ProductIDorName.GetType()));
            detail.DispatchQuantity = Convert.ToDecimal(row.GetColumnValue("DISPATCH_QUANTITY", detail.DispatchQuantity.GetType()));
            detail.POPrice = Convert.ToDecimal(row.GetColumnValue("PO_PRICE", detail.POPrice.GetType()));
            detail.ExpectedDeliveryDate = Convert.ToDateTime(row.GetColumnValue("EXPECTED_DELIVERY_DATE", detail.ExpectedDeliveryDate.GetType()));
            detail.DeliveryAddress = Convert.ToString(row.GetColumnValue("DELIVERY_ADDR", detail.DeliveryAddress.GetType()));
            detail.Comments = Convert.ToString(row.GetColumnValue("PO_COMMENTS", detail.Comments.GetType()));
            detail.RecivedQuantity = Convert.ToDecimal(row.GetColumnValue("RECEIVED_QUANTITY", detail.RecivedQuantity.GetType()));
            detail.ReturnQuantity = Convert.ToDecimal(row.GetColumnValue("RETURN_QUANTITY", detail.ReturnQuantity.GetType()));
            detail.SumDispatchQuantity = Convert.ToDecimal(row.GetColumnValue("SUM_DISPATCH_QUANTITY", detail.SumDispatchQuantity.GetType()));
            detail.SumRecivedQuantity = Convert.ToDecimal(row.GetColumnValue("SUM_RECEIVED_QUANTITY", detail.SumRecivedQuantity.GetType()));
            detail.SumReturnQuantity = Convert.ToDecimal(row.GetColumnValue("SUM_RETURN_QUANTITY", detail.SumReturnQuantity.GetType()));

            return detail;
        }



        public static DataSet SaveDispatchTrackObject(DispatchTrack dis, POItems dispatchtrack, string type, MySqlCommand cmd)
        {
            cmd.CommandText = "po_SaveDispatchTrack";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter("P_DT_ID", dispatchtrack.DTID));
            cmd.Parameters.Add(new MySqlParameter("P_PO_ID", dispatchtrack.POID));

            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_QUANTITY", dispatchtrack.DispatchQuantity));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_DATE", dis.DispatchDate));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_TYPE", dis.DispatchType));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_COMMENTS", dis.DispatchComments));

            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_CODE", dis.DispatchCode));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_MODE", dis.DispatchMode));
            cmd.Parameters.Add(new MySqlParameter("P_DELIVERY_TRACK_ID", dis.DeliveryTrackID));
            cmd.Parameters.Add(new MySqlParameter("P_DISPATCH_LINK", dis.DispatchLink));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_QUANTITY", dispatchtrack.RecivedQuantity));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_DATE", dis.RecivedDate));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_COMMENTS", dis.RecivedComments));

            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_CODE", dis.RecivedCode));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_LINK", dis.RecivedLink));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_BY", dis.RecivedBy));
            cmd.Parameters.Add(new MySqlParameter("P_STATUS", dis.Status));
            cmd.Parameters.Add(new MySqlParameter("P_CREATED_BY", dis.CreatedBy));
            cmd.Parameters.Add(new MySqlParameter("P_MODIFIED_BY", dis.ModifiedBY));

            cmd.Parameters.Add(new MySqlParameter("P_RETURN_QUANTITY", dispatchtrack.ReturnQuantity));

            cmd.Parameters.Add(new MySqlParameter("P_REQUEST_TYPE", type));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            cmd.Parameters.Clear();

            return ds;
        }

        public static DataSet SavePaymentInfoObject(PaymentInfo paymentinfo, MySqlCommand cmd)
        {
            cmd.CommandText = "po_SavePaymentInfo";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add(new MySqlParameter("P_PD_ID", paymentinfo.PDID));
            cmd.Parameters.Add(new MySqlParameter("P_PO_ID", paymentinfo.POID));
            cmd.Parameters.Add(new MySqlParameter("P_PAYMENT_CODE", paymentinfo.PaymentCode));
            cmd.Parameters.Add(new MySqlParameter("P_PAYMENT_AMOUNT", paymentinfo.PaymentAmount));
            cmd.Parameters.Add(new MySqlParameter("P_PAYMENT_DATE", paymentinfo.PaymentDate));
            cmd.Parameters.Add(new MySqlParameter("P_PAYMENT_COMMENTS", paymentinfo.PaymentComments));
            cmd.Parameters.Add(new MySqlParameter("P_PAYMENT_MODE", paymentinfo.PaymentMode));
            cmd.Parameters.Add(new MySqlParameter("P_PAYMENT_TRANSACT_ID", paymentinfo.TransactionID));
            cmd.Parameters.Add(new MySqlParameter("P_RECEIVED_COMMENTS", paymentinfo.AckComments));
            
            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            cmd.Parameters.Clear();

            return ds;
        }
        



        public static UserDetails GetVendorDetails(DataRow row)
        {
            UserDetails detail = new UserDetails();

            detail.UserID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.UserID.GetType()));
            detail.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.FirstName.GetType()));
            detail.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.LastName.GetType()));

            return detail;
        }

        public static Requirement GetRequirementDetails(DataRow row)
        {
            Requirement detail = new Requirement();

            detail.RequirementID = Convert.ToInt32(row.GetColumnValue("REQ_ID", detail.RequirementID.GetType()));
            detail.CustomerID = Convert.ToInt32(row.GetColumnValue("U_ID", detail.CustomerID.GetType()));
            detail.Title = Convert.ToString(row.GetColumnValue("REQ_TITLE", detail.Title.GetType()));

            detail.PostedUser = new User();
            detail.PostedUser.FirstName = Convert.ToString(row.GetColumnValue("U_FNAME", detail.PostedUser.FirstName.GetType()));
            detail.PostedUser.LastName = Convert.ToString(row.GetColumnValue("U_LNAME", detail.PostedUser.LastName.GetType()));

            return detail;
        }





    }
}