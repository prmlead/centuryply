﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Companies
{
   public class CompanyUser
    {
       public int User_Id { get; set; }
       public string Name { get; set; }
       public string Email { get; set; }

    }
}
