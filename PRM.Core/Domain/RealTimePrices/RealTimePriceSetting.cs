﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.RealTimePrices
{
    [Table("realtimeprice_configuration")]
    public   class RealTimePriceSetting  : BaseEntity
    {
        public int Company_Id { get; set; }
        public string AllowedProducts { get; set; }

        public bool AllowThresholdNotification { get; set; }

        public bool AllowPriceDipNotification { get; set; }

    }
}
