﻿using PRM.Core.Domain.Vendors;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Logistics
{
    [Table("logistic_quotations")]
    public class LogisticQuotation
    {

        [Dapper.Column("QUOT_ID")]
        public int Id
        {
            get;
            set;
        }

        [Dapper.Column("ITEM_ID")]
        public int Item_Id { get; set; }

        [Dapper.Column("REQ_ID")]
        public int Requirment_Id { get; set; }

        [Dapper.Column("U_ID")]
        public int User_Id { get; set; }

        [Dapper.Column("AIRLINE")]
        public string Airline { get; set; }

        [Dapper.Column("UNIT_TARIFF")]
        public double UnitTariff { get; set; }

        [Dapper.Column("UNIT_TARIFF_TYPE")]
        public int UnitTariffType { get; set; }

        [Dapper.Column("UNIT_PRICE")]
        public double UnitPrice { get; set; }

        [Dapper.Column("UNIT_PRICE_TYPE")]
        public int UnitPriceType { get; set; }

        [Dapper.Column("REV_UNIT_PRICE")]
        public double RevUnitPrice { get; set; }


        [Dapper.Column("REV_UNIT_PRICE_TYPE")]
        public int RevUnitPriceType { get; set; }

        [Dapper.Column("PRICE")]

        public double Price { get; set; }

        [Dapper.Column("PRICE_TYPE")]

        public int PriceType { get; set; }

        [Dapper.Column("REVICED_PRICE")]

        public double RevicedPrice { get; set; }

        [Dapper.Column("REVICED_PRICE_TYPE")]

        public int RevicedPriceType { get; set; }

        [Dapper.Column("C_GST")]
        public double C_Gst { get; set; }

        [Dapper.Column("C_GST_TYPE")]
        public int C_GstType { get; set; }

        [Dapper.Column("S_GST")]
        public double S_Gst { get; set; }

        [Dapper.Column("S_GST_TYPE")]
        public int S_GstType { get; set; }

        [Dapper.Column("I_GST")]
        public double I_Gst { get; set; }

        [Dapper.Column("I_GST_TYPE")]
        public int I_GstType { get; set; }

        [Dapper.Column("X_RAY")]
        public double X_Ray { get; set; }

        [Dapper.Column("X_RAY_TYPE")]
        public int X_RayType { get; set; }

        [Dapper.Column("FSC")]
        public double FSC { get; set; }

        [Dapper.Column("FSC_TYPE")]
        public int FscType { get; set; }

        [Dapper.Column("SSC")]
        public double SSC { get; set; }

        [Dapper.Column("SSC_TYPE")]
        public int SccTYPE { get; set; }

        [Dapper.Column("MISC")]
        public double Misc { get; set; }

        [Dapper.Column("MISC_TYPE")]
        public int MiscType { get; set; }

        [Dapper.Column("HAZ_CHARGES")]
        public double HAZ_CHARGES { get; set; }

        [Dapper.Column("HAZ_CHARGES_TYPE")]
        public int HAZ_CHARGES_TYPE { get; set; }

        [Dapper.Column("CUSTOM_CLEARANCE")]
        public double CustomClearance { get; set; }

        [Dapper.Column("CUSTOM_CLEARANCE_TYPE")]
        public int CustomClearanceType { get; set; }

        [Dapper.Column("REV_CUSTOM_CLEARANCE")]
        public double RevCustomClearance { get; set; }

        [Dapper.Column("REV_CUSTOM_CLEARANCE_TYPE")]
        public int RevCustomClearanceType { get; set; }

        [Dapper.Column("CG_FEES")]
        public double CgFees { get; set; }

        [Dapper.Column("CG_FEES_TYPE")]
        public int CgFeesType { get; set; }

        [Dapper.Column("ADC")]
        public double Adc { get; set; }

        [Dapper.Column("ADC_TYPE")]
        public int AdcType { get; set; }

        [Dapper.Column("AWB_FEES")]
        public double AwbFees { get; set; }

        [Dapper.Column("AWB_FEES_TYPE")]
        public int AwbFeesType { get; set; }

        [Dapper.Column("EDI_FEES")]

        public double EdiFees { get; set; }

        [Dapper.Column("EDI_FEES_TYPE")]

        public int EdiFeesType { get; set; }

        [Dapper.Column("SERVICE_CHARGES")]
        public double ServiceCharges { get; set; }

        [Dapper.Column("SERVICE_CHARGES_TYPE")]
        public int ServiceChargesType { get; set; }

        [Dapper.Column("REV_SERVICE_CHARGES")]
        public double RevServiceCharges { get; set; }

        [Dapper.Column("REV_SERVICE_CHARGES_TYPE")]
        public int RevServiceChargesType { get; set; }

        [Dapper.Column("OTHER_CHARGES")]
        public double OtherCharges { get; set; }

        [Dapper.Column("OTHER_CHARGES_TYPE")]
        public int OtherChargesType { get; set; }

        [Dapper.Column("REV_OTHER_CHARGES")]
        public double RevOtherCharges { get; set; }

        [Dapper.Column("REV_OTHER_CHARGES_TYPE")]
        public int RevOtherChargesType { get; set; }

        [Dapper.Column("TERMINAL_HANDLING")]
        public double TerminalHandling { get; set; }

        [Dapper.Column("TERMINAL_HANDLING_TYPE")]
        public int TerminalHandlingType { get; set; }

        [Dapper.Column("AMS")]
        public double AMS { get; set; }

        [Dapper.Column("AMS_TYPE")]
        public int AMSType { get; set; }

        [Dapper.Column("FORWORD_DG_FEE")]
        public double ForwordDgFee { get; set; }

        [Dapper.Column("FORWORD_DG_FEE_TYPE")]
        public int ForwordDgFeeType { get; set; }

        [Dapper.Column("ROUTING")]
        public string Routing { get; set; }

        [Dapper.Column("TRANSIT")]
        public string Transit { get; set; }

        [Dapper.Column("IS_SELECTED")]
        public bool IsSelected { get; set; }

        [Dapper.Column("IS_REVISED")]
        public bool IsRevised { get; set; }

        [Dapper.Column("IMAGE_ID")]
        public int ImageId { get; set; }

        [Dapper.Column("REVISED_IMAGE_ID")]
        public int RevisedImageId { get; set; }

        [Dapper.Column("DATE_CREATED")]
        public DateTime DateCreated { get; set; }

        [Dapper.Column("DATE_MODIFIED")]
        public DateTime DateModified { get; set; }

        [Dapper.Column("CREATED_BY")]
        public int CreatedBy { get; set; }

        [Dapper.Column("MODIFIED_BY")]
        public int ModifiedBy { get; set; }

        [Dapper.Column("IS_DELETED")]
        public int IsDeleted { get; set; }

        [Dapper.NotMapped]
        public virtual Vendor Vendor { get; set; }

        [Dapper.Column("INVOICE_NUMBER")]
        public string InvoiceNumber { get; set; }

        [Dapper.Column("INVOICE_DATE")]
        public DateTime? InvoiceDate { get; set; }

        [Dapper.Column("CONSIGNEE_NAME")]
        public string ConsigneeName { get; set; }




        // QUOT_ID, ITEM_ID, REQ_ID, U_ID, AIRLINE, UNIT_TARIFF, UNIT_TARIFF_TYPE, UNIT_PRICE, UNIT_PRICE_TYPE, REV_UNIT_PRICE, REV_UNIT_PRICE_TYPE, PRICE, PRICE_TYPE, REVICED_PRICE, REVICED_PRICE_TYPE, C_GST, C_GST_TYPE, S_GST, S_GST_TYPE, I_GST, I_GST_TYPE, X_RAY, X_RAY_TYPE, FSC, FSC_TYPE, SSC, SSC_TYPE, MISC, MISC_TYPE, HAZ_CHARGES, HAZ_CHARGES_TYPE, CUSTOM_CLEARANCE, CUSTOM_CLEARANCE_TYPE, REV_CUSTOM_CLEARANCE, REV_CUSTOM_CLEARANCE_TYPE, CG_FEES, CG_FEES_TYPE, ADC, ADC_TYPE, AWB_FEES, AWB_FEES_TYPE, EDI_FEES, EDI_FEES_TYPE, SERVICE_CHARGES, SERVICE_CHARGES_TYPE, REV_SERVICE_CHARGES, REV_SERVICE_CHARGES_TYPE, OTHER_CHARGES, OTHER_CHARGES_TYPE, REV_OTHER_CHARGES, REV_OTHER_CHARGES_TYPE, TERMINAL_HANDLING, TERMINAL_HANDLING_TYPE, AMS, AMS_TYPE, FORWORD_DG_FEE, FORWORD_DG_FEE_TYPE, ROUTING, TRANSIT, IS_SELECTED, IS_REVISED, IMAGE_ID, REVISED_IMAGE_ID, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY, IS_DELETED

    }

}
