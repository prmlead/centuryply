﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Acl
{
    [Table("mapping_acl")]
     public class AclMapping  : BaseEntity
    {
        public int Type { get; set; }

        public string EntityName { get; set; }

        public int EntityId { get; set; }

        public int Value { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public bool Deleted { get; set; }

        public int Company_Id { get; set; }
    }
}
