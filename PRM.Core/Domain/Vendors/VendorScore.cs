﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Vendors
{
   public class VendorScore
    {
        public string Field { get; set; }
        public int Value { get; set; }
    }
}
