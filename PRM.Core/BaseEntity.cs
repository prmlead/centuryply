﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core
{
   public partial class BaseEntity
    {
        [Key]
        public virtual int Id { get; set; }
    }
}
