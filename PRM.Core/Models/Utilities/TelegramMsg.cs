﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class TelegramMsg : Entity
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "moduleName")]
        public string ModuleName { get; set; }
    }
}