﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class POInformation : ResponseAudit
    {
        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "reqQuantity")]
        public decimal ReqQuantity { get; set; }

        [DataMember(Name = "poQuantity")]
        public decimal POQuantity { get; set; }

        [DataMember(Name = "poVendors")]
        public List<POVendor> poVendors { get; set; }

    }
}