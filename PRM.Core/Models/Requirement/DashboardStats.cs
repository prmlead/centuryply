﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class DashboardStats : Entity
    {
        [DataMember(Name="totalTurnover")]
        public double TotalTurnover { get; set; }

        [DataMember(Name = "totalSaved")]
        public double TotalSaved { get; set; }

        [DataMember(Name = "totalOrders")]
        public int TotalOrders { get; set; }

        [DataMember(Name = "totalAuctions")]
        public int TotalAuctions { get; set; }
    }
}