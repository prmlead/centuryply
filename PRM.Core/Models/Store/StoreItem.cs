﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class StoreItem : ResponseAudit
    {
        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "storeDetails")]
        public Store StoreDetails { get; set; }

        string itemCode = string.Empty;
        [DataMember(Name = "itemCode")]
        public string ItemCode
        {
            get
            {
                if (!string.IsNullOrEmpty(itemCode))
                {
                    return itemCode;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    itemCode = value;
                }
            }
        }

        string itemType = string.Empty;
        [DataMember(Name = "itemType")]
        public string ItemType
        {
            get
            {
                if (!string.IsNullOrEmpty(itemType))
                {
                    return itemType;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    itemType = value;
                }
            }
        }

        string itemDescription = string.Empty;
        [DataMember(Name = "itemDescription")]
        public string ItemDescription
        {
            get
            {
                if (!string.IsNullOrEmpty(itemDescription))
                {
                    return itemDescription;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    itemDescription = value;
                }
            }
        }

        [DataMember(Name = "totalStock")]
        public decimal TotalStock { get; set; }

        [DataMember(Name = "inStock")]
        public decimal InStock { get; set; }

        [DataMember(Name = "onOrder")]
        public decimal OnOrder { get; set; }

        [DataMember(Name = "itemPrice")]
        public decimal ItemPrice { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "doAlert")]
        public int DoAlert { get; set; }

        [DataMember(Name = "threshold")]
        public int Threshold { get; set; }

        [DataMember(Name = "iGST")]
        public decimal IGST { get; set; }

        [DataMember(Name = "cGST")]
        public decimal CGST { get; set; }

        [DataMember(Name = "sGST")]
        public decimal SGST { get; set; }

        string itemUnits = string.Empty;
        [DataMember(Name = "itemUnits")]
        public string ItemUnits
        {
            get
            {
                if (!string.IsNullOrEmpty(itemUnits))
                {
                    return itemUnits;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    itemUnits = value;
                }
            }
        }

        string itemName = string.Empty;
        [DataMember(Name = "itemName")]
        public string ItemName
        {
            get
            {
                if (!string.IsNullOrEmpty(itemName))
                {
                    return itemName;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    itemName = value;
                }
            }
        }

        [DataMember(Name = "itemDetails")]
        public StoreItemDetails[] ItemDetails { get; set; }
    }
}