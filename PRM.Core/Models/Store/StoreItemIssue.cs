﻿using System;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class StoreItemIssue : ResponseAudit
    {
        [DataMember(Name = "ginID")]
        public int GINID { get; set; }

        string _ginCode = string.Empty;
        [DataMember(Name = "ginCode")]
        public string GINCode
        {
            get
            {
                return string.IsNullOrEmpty(_ginCode) ? string.Empty : _ginCode;                
            }
            set
            {
                _ginCode = value;
            }
        }        

        [DataMember(Name = "storeItemDetails")]
        public StoreItem[] StoreItemDetails { get; set; }

        string _indentNo = string.Empty;
        [DataMember(Name = "indentNo")]
        public string IndentNo
        {
            get
            {
                return string.IsNullOrEmpty(_indentNo) ? string.Empty : _indentNo;
            }
            set
            {
                _indentNo = value;
            }
        }

        string shipName = string.Empty;
        [DataMember(Name = "shipName")]
        public string ShipName
        {
            get
            {
                return string.IsNullOrEmpty(shipName) ? string.Empty : shipName;
            }
            set
            {
                shipName = value;
            }
        }

        string shipLine1 = string.Empty;
        [DataMember(Name = "shipLine1")]
        public string ShipLine1
        {
            get
            {
                return string.IsNullOrEmpty(shipLine1) ? string.Empty : shipLine1;
            }
            set
            {
                shipLine1 = value;
            }
        }

        string shipCity = string.Empty;
        [DataMember(Name = "shipCity")]
        public string ShipCity
        {
            get
            {
                return string.IsNullOrEmpty(shipCity) ? string.Empty : shipCity;
            }
            set
            {
                shipCity = value;
            }
        }

        string shipState = string.Empty;
        [DataMember(Name = "shipState")]
        public string ShipState
        {
            get
            {
                return string.IsNullOrEmpty(shipState) ? string.Empty : shipState;
            }
            set
            {
                shipState = value;
            }
        }

        string shipCountry = string.Empty;
        [DataMember(Name = "shipCountry")]
        public string ShipCountry
        {
            get
            {
                return string.IsNullOrEmpty(shipCountry) ? string.Empty : shipCountry;
            }
            set
            {
                shipCountry = value;
            }
        }

        string shipZip = string.Empty;
        [DataMember(Name = "shipZip")]
        public string ShipZip
        {
            get
            {
                return string.IsNullOrEmpty(shipZip) ? string.Empty : shipZip;
            }
            set
            {
                shipZip = value;
            }
        }

        string phone = string.Empty;
        [DataMember(Name = "phone")]
        public string Phone
        {
            get
            {
                return string.IsNullOrEmpty(phone) ? string.Empty : phone;
            }
            set
            {
                phone = value;
            }
        }

        DateTime issueDate = DateTime.MaxValue;
        [DataMember(Name = "issueDate")]
        public DateTime IssueDate
        {
            get
            {
                return this.issueDate;
            }
            set
            {
                this.issueDate = value;
            }
        }

        string issueLocation = string.Empty;
        [DataMember(Name = "issueLocation")]
        public string IssueLocation
        {
            get
            {
                return string.IsNullOrEmpty(issueLocation) ? string.Empty : issueLocation;
            }
            set
            {
                issueLocation = value;
            }
        }

        string shipMode = string.Empty;
        [DataMember(Name = "shipMode")]
        public string ShipMode
        {
            get
            {
                return string.IsNullOrEmpty(shipMode) ? string.Empty : shipMode;
            }
            set
            {
                shipMode = value;
            }
        }

        string shipTrackNo = string.Empty;
        [DataMember(Name = "shipTrackNo")]
        public string ShipTrackNo
        {
            get
            {
                return string.IsNullOrEmpty(shipTrackNo) ? string.Empty : shipTrackNo;
            }
            set
            {
                shipTrackNo = value;
            }
        }

        string shipModeDetails = string.Empty;
        [DataMember(Name = "shipModeDetails")]
        public string ShipModeDetails
        {
            get
            {
                return string.IsNullOrEmpty(shipModeDetails) ? string.Empty : shipModeDetails;
            }
            set
            {
                shipModeDetails = value;
            }
        }

        [DataMember(Name = "orderQty")]
        public decimal OrderQty { get; set; }

        [DataMember(Name = "shipQty")]
        public decimal ShipQty { get; set; }

        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                return string.IsNullOrEmpty(comments) ? string.Empty : comments;
            }
            set
            {
                comments = value;
            }
        }
    }
}