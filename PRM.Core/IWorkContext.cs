﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core
{
     public  interface IWorkContext
    {
        int CurrentUser { get; set; }

        int CurrentCompany { get; set; }


    }
}
