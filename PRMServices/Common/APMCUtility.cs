﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

using PRMServices.Models;
using PRMServices.SQLHelper;

namespace PRMServices.Common
{
    public class APMCUtility
    {
        private static MySQLBizClass sqlHelper = new MySQLBizClass();
        public static DataSet SaveAPMCObject(APMC apmc, MySqlCommand cmd)
        {
            cmd.CommandText = "cp_SaveAPMC";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_APMC_ID", apmc.APMCID));
            cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", apmc.Company.CompanyID));
            cmd.Parameters.Add(new MySqlParameter("P_APMC_NAME", apmc.APMCName));
            cmd.Parameters.Add(new MySqlParameter("P_APMC_DESC", apmc.APMCDescription));
            cmd.Parameters.Add(new MySqlParameter("P_IS_VALID", apmc.IsValid));
            cmd.Parameters.Add(new MySqlParameter("P_CATEGORY", apmc.Category));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", apmc.Customer.UserID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        public static DataSet SaveAPMCSettings(APMCSettings aPMCSettings)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_SET_ID", aPMCSettings.APMCSettingID);
            sd.Add("P_APMC_ID", aPMCSettings.APMCID);
            sd.Add("P_COMP_ID", aPMCSettings.CompanyID);
            sd.Add("P_RELATED_CESS", aPMCSettings.RelatedCess);
            sd.Add("P_HAMALI", aPMCSettings.Hamali);
            sd.Add("P_FREIGHT", aPMCSettings.Freight);
            sd.Add("P_SUTLI", aPMCSettings.Sutli);
            sd.Add("P_SC", aPMCSettings.SC);
            sd.Add("P_BROKERAGE", aPMCSettings.Brokerage);
            sd.Add("P_ADAT", aPMCSettings.Adat);
            sd.Add("P_PACKING", aPMCSettings.Packing);
            sd.Add("P_U_ID", aPMCSettings.Customer.UserID);

            DataSet ds = sqlHelper.SelectList("cp_SaveAPMCSettings", sd);

            return ds;
        }

        public static DataSet SaveAPMCNegotiation(APMCNegotiation apmcNegotiation, int apmcNegotiationID, int userID, MySqlCommand cmd)
        {
            cmd.Parameters.Clear();
            if(apmcNegotiation.NewPrice > 0)
            {
                apmcNegotiation.CustPriceInclFinal = apmcNegotiation.VendorPriceExclFinal = apmcNegotiation.NewPrice;
            }            
            cmd.CommandText = "cp_SaveAPMCNegotiation";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_APMC_NEG_ID", apmcNegotiation.APMCNegotiationID));
            cmd.Parameters.Add(new MySqlParameter("P_APMC_ID", apmcNegotiation.APMCID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_ID", apmcNegotiation.Vendor.UserID));

            cmd.Parameters.Add(new MySqlParameter("P_CUST_PRICE_INCL", apmcNegotiation.CustPriceInclFinal));
            
            cmd.Parameters.Add(new MySqlParameter("P_VEND_PRICE_EXCL", apmcNegotiation.VendorPriceExclFinal));

            cmd.Parameters.Add(new MySqlParameter("P_IS_FROZEN", apmcNegotiation.IsFrozen));
            cmd.Parameters.Add(new MySqlParameter("P_VEND_QUANTITY", apmcNegotiation.Quantity));
            cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", apmcNegotiation.VendorCompany.CompanyID));
            cmd.Parameters.Add(new MySqlParameter("P_QUANTITY_UPDATED", apmcNegotiation.QuantityUpdated));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", apmcNegotiation.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_ATTACHMENT", apmcNegotiation.AttachmentID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        internal static DataSet SaveAPMCVendor(APMCVendor apmcVendor, MySqlCommand cmd)
        {
            cmd.CommandText = "cp_SaveAPMCVendor";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_APMC_ID", apmcVendor.APMCID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_ID", apmcVendor.VendorID));
            cmd.Parameters.Add(new MySqlParameter("P_APMC_V_ID", apmcVendor.APMCVendorID));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", apmcVendor.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", apmcVendor.CompanyID));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }

        internal static DataSet SaveAPMCInput(APMCInput apmcInput)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_APMC_ID", apmcInput.APMCID);
            sd.Add("P_EXP_PRICE_INCL", apmcInput.InputPrice);
            sd.Add("P_U_ID", apmcInput.UserID);
            sd.Add("P_COMP_ID", apmcInput.Company.CompanyID);
            DataSet ds = sqlHelper.SelectList("cp_SaveAPMCInput", sd);
            return ds;
        }

        internal static DataSet SaveAPMCVendorQuantity(APMCVendorQuantity vendorQ, MySqlCommand cmd)
        {
            cmd.CommandText = "cp_SaveAPMCVendorQuantity";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter("P_APMC_NEG_ID", vendorQ.APMCNegotiationID));
            cmd.Parameters.Add(new MySqlParameter("P_APMC_VQ_ID", vendorQ.APMCVendorQuantityID));
            cmd.Parameters.Add(new MySqlParameter("P_U_ID", vendorQ.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_VENDOR_ID", vendorQ.UserID));
            cmd.Parameters.Add(new MySqlParameter("P_QUANTITY", vendorQ.Quantity));
            cmd.Parameters.Add(new MySqlParameter("P_PRICE", vendorQ.Price));
            cmd.Parameters.Add(new MySqlParameter("P_UNITS", vendorQ.Units));

            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);

            return ds;
        }
    }
}