﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

using PRM.Data;
using PRMServices.Models.Catalog;
using MySql.Data.MySqlClient;
using System.Reflection;
using System.Configuration;
using System.Runtime.Serialization;
using PRMServices.SQLHelper;

namespace PRMServices
{
    public static class CatalogUtility
    {
        private static MySQLBizClass sqlHelper = new MySQLBizClass();
        public static List<Category> GetCategoriesById(int id, MySqlCommand cmd)
        {
            DataTable data = GetCategories(cmd, "cp_getCategory");

            var result = (from row in data.AsEnumerable()
                          where row.Field<int>("CAT_ID") == id
                          select CreateItemFromRow<Category>(row)).ToList();

            return result;
        }

        public static DataTable GetCategories(MySqlCommand cmd, string spName)
        {
            cmd.CommandText = spName;
            cmd.CommandType = CommandType.StoredProcedure;
            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            myDA.Fill(ds);
            return ds.Tables[0];
        }

        internal static DataSet GetResultSet(int prop_ID, string v, List<string> list1, List<string> list2)
        {
            throw new NotImplementedException();
        }

        public static T CreateItemFromRow<T>(DataRow row) where T : new()
        {
            T item = new T();
            SetItemFromRow(item, row);
            return item;
        }

        public static void SetItemFromRow<T>(T item, DataRow row) where T : new()
        {
            foreach (DataColumn c in row.Table.Columns)
            {
                PropertyInfo p = item.GetType().GetProperty(c.ColumnName);
                if (p != null && row[c] != DBNull.Value)
                {
                    p.SetValue(item, c.DataType.ToString() == "System.Int64" ? Convert.ToInt32(row[c]) : row[c], null);
                }
            }

            //foreach (PropertyInfo prop in item.GetType().GetProperties())
            //{
            //    var attr = prop.GetCustomAttributes(false);
            //    var colmap = attr.FirstOrDefault(a => a.GetType() == typeof(DataMemberAttribute));
            //    if (colmap != null)
            //    {
            //        var mapsTo = colmap as DataMemberAttribute;
            //        if (row.Table.Columns.Contains(mapsTo.Name))
            //        {
            //            prop.SetValue(item, row[mapsTo.Name], null);
            //        }
            //    }
            //}
        }

        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        public static DataSet GetResultSet(string spName, List<string> columnNames, List<object> values)
        {
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                if (columnNames != null && columnNames.Count > 0)
                {
                    for (int ctr = 0; ctr < columnNames.Count; ctr++)
                    {
                        sd.Add(columnNames[ctr], values[ctr]);
                    }
                }

                ds = sqlHelper.SelectList(spName, sd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ds;
        }
    }
}