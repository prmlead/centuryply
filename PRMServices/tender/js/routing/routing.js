﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('view-tender', {
                    url: '/view-tender/:Id',
                    templateUrl: 'tender/views/tender-list-item.html',
                    params: {
                        detailsObj: null
                    }
                }).state('save-tender', {
                    url: '/save-tender/:Id',
                    templateUrl: 'tender/views/save-tender.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                }).state('view-tenders', {
                    url: '/view-tenders',
                    templateUrl: 'tender/views/view-tenders.html',
                    params: {
                        reqObj: null,
                        prDetail: null
                    }
                });
        }]);