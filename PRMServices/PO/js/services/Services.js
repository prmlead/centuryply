prmApp.constant('PRMPOServicesDomain', 'po/svc/PRMPOModuleService.svc/REST/');
prmApp.service('PRMPOServices', ["PRMPOServicesDomain", "SAPIntegrationServicesDomain", "userService", "httpServices", "poDomain",
    function (PRMPOServicesDomain, SAPIntegrationServicesDomain, userService, httpServices, poDomain) {

        var PRMPOServices = this;
        //PRMPOServices.GetSeries = function (series, seriestype, deptid) {
        //    var url = PRMPOServicesDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
        //    return httpServices.get(url);
        //};


        PRMPOServices.SavePODetailsToSAP = function (params) {
            let url = poDomain + 'postpodata';
            return httpServices.post(url, params);
        };

        PRMPOServices.getPOGenerateDetails = function (params) {
            let url = poDomain + 'getpogeneratedetails?compid=' + params.compid + '&template=' + params.template
                + '&vendorid=' + params.vendorid + '&status=' + params.status + '&creator=' + params.creator
                + '&plant=' + params.plant
                + '&purchasecode=' + params.purchasecode + '&search=' + params.search
                + '&sessionid=' + params.sessionid + '&page=' + params.page
                + '&pagesize=' + params.pageSize;
            return httpServices.get(url);
        };

        PRMPOServices.getPOItems = function (params) {
            let url = poDomain + 'getpoitems?ponumber=' + params.ponumber + '&quotno=' + params.quotno + '&sessionid=' + params.sessionid;

            return httpServices.get(url);
        };

        PRMPOServices.getFilterValues = function (params) {
            let url = poDomain + 'getFilterValues?compid=' + params.compid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        PRMPOServices.getRequirementPO = function (params) {
            let url = poDomain + 'getrequirementpo?compid=' + params.compid + '&reqid=' + params.reqid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        return PRMPOServices;

}]);