﻿prmApp
    .controller('poDomesticZSDMCtrl', ["$state", "$stateParams", "$scope", "$rootScope", "auctionsService", "userService", "$http", "$window", "domain",
        "fileReader", "growlService", "$log", "$filter", "ngDialog", "catalogService", "PRMPRServices", "PRMCustomFieldService", "$modal", "PRMPOServices",
        function ($state, $stateParams, $scope, $rootScope, auctionsService, userService, $http, $window, domain,
            fileReader, growlService, $log, $filter, ngDialog, catalogService, PRMPRServices, PRMCustomFieldService, $modal, PRMPOServices) {

            $scope.currentUserId = +userService.getUserId();
            $scope.isCustomer = $scope.customerType === "CUSTOMER" ? true : false;
            $scope.currentUserSessionId = userService.getUserToken();
            $scope.currentUsercompanyId = userService.getUserCompanyId();
            $scope.currentUserName = userService.getFirstname() + ' ' + userService.getLastname();
            $scope.currentUserSapId = $scope.currentUserId; //userService.getSAPUserId();
            $scope.qcsId = $stateParams.qcsId;
            $scope.reqId = $stateParams.reqId;
            $scope.poQuotId = $stateParams.quotId;
            $scope.requirementDetails = $stateParams.requirementDetails;
            $scope.vendorAssignments = $stateParams.detailsObj;
            $scope.poRAWJSON = $stateParams.poRawJSON;
            let poRoundDigits = 2;
            $scope.POServiceStatusText = '';
            $scope.documentType = $stateParams.templateName;
            $scope.selectedVendors = [];
            $scope.currentUIVendor = {};
            $scope.currentUIPlant = {};
            $scope.formDetails = {};
            $scope.formDetails.items = [];
            $scope.reqDepartments = [];
            $scope.requirementPRItems = [];
            $scope.requirementPOItems = [];
            $scope.requirementVendorCodeInfo = [];

            $scope.formDetails.MEMORY_TYPE = 'X';
            $scope.formDetails.PR_NUMBER = $scope.requirementDetails && $scope.requirementDetails.prNumbers ? $scope.requirementDetails.prNumbers : '';
            $scope.formDetails.PAYMENT_TERMS = $scope.requirementDetails && $scope.requirementDetails.paymentTerms ? $scope.requirementDetails.paymentTerms : '';
            $scope.formDetails.PO_CREATOR = $scope.currentUserSapId;//$scope.requirementDetails && $scope.requirementDetails.prCreator ? $scope.requirementDetails.prCreator : '';
            $scope.formDetails.INCO_TERMS1 = '';
            $scope.formDetails.INCO_TERMS2 = '';
            $scope.formDetails.DOC_TYPE = $scope.documentType;            
            $scope.requirementPlants = [];
            let requirementPlantsCodes = _.uniq($scope.requirementDetails.PLANT_CODES.split(","));
            let requirementPlantsNames = _.uniq($scope.requirementDetails.PLANTS.split(","));


            $scope.getplantfieldmapping = function (type) {
                var params = {
                    "type": "'" + requirementPlantsCodes.join("','") + "'"
                };

                PRMPRServices.getplantfieldmapping(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (plant) {
                                plant.PLANT_CODE = plant.FIELD_NAME;
                                plant.PLANT_NAME = plant.FIELD_VALUE;
                                plant.LOCATION = plant.FILTER1;
                            })
                            $scope.PlantsList = _.uniqBy(response,'LOCATION');
                        }
                    });

            };
            $scope.getplantfieldmapping(requirementPlantsCodes);

            if (requirementPlantsCodes && requirementPlantsCodes.length > 0) {
                let temp = 0;
                requirementPlantsCodes.forEach(function (code, index) {
                    $scope.requirementPlants.push({
                        PLANT_CODE: code,
                        PLANT_NAME: requirementPlantsNames[temp]
                    });

                    temp++;
                });
            }

            $scope.quantityError = false;
            var SelectedVendorDetails;

            $scope.companyConfigList = [];
            $scope.COMPANY_INCO_TERMS = [];
            auctionsService.GetCompanyConfiguration($scope.currentUsercompanyId, 'INCO', userService.getUserToken())
                .then(function (unitResponse) {

                    $scope.companyConfigList = unitResponse;
                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey === 'INCO') {
                            $scope.COMPANY_INCO_TERMS.push(item);
                        } 
                    });

                });

            if ($scope.vendorAssignments && $scope.vendorAssignments.length > 0) {

                $scope.vendorAssignments = _.filter($scope.vendorAssignments, function (vendorObj) {
                    return !vendorObj.poNumber;
                });

                $scope.vendorAssignments.forEach(function (assignment, index) {
                    let temp = _.filter($scope.selectedVendors, function (vendorObj) {
                        return vendorObj.vendorId === assignment.vendorID;
                    });

                    let vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === assignment.vendorID;
                    });

                    if (!temp || temp.length <= 0) {
                        $scope.selectedVendors.push({ 'vendorId': assignment.vendorID, 'vendorCompany': assignment.vendorName, vendorObj: vendorTemp[0] });
                    }
                });
            }

            //$scope.selectedVendors = _.uniq(_.map($scope.vendorAssignments, 'vendorName'));

            $scope.precisionRound = function (number) {
                var factor = Math.pow(10, poRoundDigits);
                return Math.round(number * factor) / factor;
            };
            $scope.addItem = function () {
                $scope.formDetails.items.push({
                    ITEM_OF_REQUESITION: '',
                    isDeleted: false
                });
            };
            $scope.deleteItem = function (item) {
                item.isDeleted = true;
            };

            $scope.pageNo = 1;
            $scope.nextpage = function (pageNo) {
                $scope.pageNo = $scope.pageNo + 1;
            };
            $scope.previouspage = function () {
                $scope.pageNo = $scope.pageNo - 1;

            };
            $scope.cancel = function () {
                $window.history.back();
            };

            $scope.filterSelectionChange = function () {
                if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId && $scope.currentUIPlant && $scope.currentUIPlant.PLANT_CODE) {
                    let filterPlantArray = _.filter($scope.requirementPlants, function (plant) {
                        return plant.PLANT_CODE.slice(0, 2) == $scope.currentUIPlant.PLANT_CODE.slice(0, 2);
                    });
                    $scope.formDetails.items = [];
                    $scope.formDetails.VENDOR_COMPANY = $scope.currentUIVendor.vendorCompany;
                    $scope.formDetails.VENDOR_ID = $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION = $stateParams.quoteLink ? $stateParams.quoteLink : $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.VENDOR_QUOTATION1 = $scope.reqId + '-' + $scope.currentUIVendor.vendorId;
                    $scope.formDetails.INCO_TERMS1 = $scope.currentUIVendor.vendorObj.INCO_TERMS;
                    $scope.formDetails.INCO_TERMS2 = '';
                    let currentVendorObj = _.filter($scope.requirementDetails.auctionVendors, function (vendorObj) {
                        return vendorObj.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    let totalVendorItems = 1;
                    let packageTax = 0;
                    let packageValWithTax = 0;
                    let packageValWithOutTax = 0;

                    let freightTax = 0;
                    let freightValWithTax = 0;
                    let freightValWithOutTax = 0;

                    let miscTax = 0;
                    let miscValWithTax = 0;
                    let miscValWithOutTax = 0;


                    //Packing & Forwarding charges
                    var packaging = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Packing & Forwarding charges';
                    });

                    if (packaging && packaging.length > 0) {
                        packageTax = packaging[0].iGst ? packaging[0].iGst : (packaging[0].sGst + packaging[0].cGst);
                        packageValWithTax = packaging[0].itemPrice;
                        packageValWithOutTax = packaging[0].unitPrice;
                    }

                    //Freight charges
                    var freightCharges = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Freight charges';
                    });

                    if (freightCharges && freightCharges.length > 0) {
                        freightTax = freightCharges[0].iGst ? freightCharges[0].iGst : (freightCharges[0].sGst + freightCharges[0].cGst);
                        freightValWithTax = freightCharges[0].itemPrice;
                        freightValWithOutTax = freightCharges[0].unitPrice;
                    }

                    //Miscellenous
                    var miscellenous = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                        return reqItem.productCode === 'Miscellenous';
                    });

                    if (miscellenous && miscellenous.length > 0) {
                        miscTax = miscellenous[0].iGst ? miscellenous[0].iGst : (miscellenous[0].sGst + miscellenous[0].cGst);
                        miscValWithTax = miscellenous[0].itemPrice;
                        miscValWithOutTax = miscellenous[0].unitPrice;
                    }

                    $scope.formDetails.PO_CURRENCY = currentVendorObj[0].vendorCurrency;
                    $scope.formDetails.VENDOR_NAME = $scope.currentUIVendor.vendorObj.vendorName;
                    $scope.formDetails.VENDOR_CODE = $scope.currentUIVendor.vendorObj.vendorCode;
                    if ($scope.currentUIVendor.vendorObj.gstNumber && $scope.requirementVendorCodeInfo && $scope.requirementVendorCodeInfo.length > 0) {
                        let vendorGstDetails = _.filter($scope.requirementVendorCodeInfo, function (vendorInfo) {
                            return vendorInfo.gstNumber && vendorInfo.vendorCode && vendorInfo.vendorID === $scope.currentUIVendor.vendorObj.vendorID &&
                                vendorInfo.gstNumber === $scope.currentUIVendor.vendorObj.gstNumber;
                        });

                        if (vendorGstDetails && vendorGstDetails.length > 0) {
                            $scope.formDetails.VENDOR_CODE = vendorGstDetails[0].vendorCode;
                        }
                    }

                    //$scope.formDetails.PAYMENT_TERMS = $scope.currentUIVendor.vendorObj.paymentTermCode + ' - ' + $scope.currentUIVendor.vendorObj.paymentTermDesc;
                    //$scope.formDetails.PAYMENT_TERMS_HIDE = $scope.currentUIVendor.vendorObj.paymentTermCode;

                    //Items Populate:
                    let vendorItems = _.filter($scope.vendorAssignments, function (assignment) {
                        return assignment.vendorID === $scope.currentUIVendor.vendorId;
                    });

                    if (vendorItems && vendorItems.length > 0) {
                        totalVendorItems = vendorItems.length;
                        vendorItems.forEach(function (vendorItem, index) {
                            let poItemObj = {
                                MPN_CODE: '',
                                Deliver_SCHEDULE: ''
                            };
                            var itemdetailsTemp = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.itemID === vendorItem.itemID;
                            });

                            let prItemObj = _.filter($scope.requirementPRItems, function (prItem) {
                                //return prItem.PRODUCT_ID === itemdetailsTemp[0].catalogueItemID && prItem.PLANT === $scope.currentUIPlant.PLANT_CODE;
                                return prItem.PRODUCT_ID === itemdetailsTemp[0].catalogueItemID && _.findIndex(filterPlantArray, function (plant) { return plant.PLANT_CODE === prItem.PLANT }) > -1;

                            });

                            //poItemObj.TAX_CODE = currentVendorObj[0].gstNumber;
                            if (itemdetailsTemp && itemdetailsTemp.length > 0) {
                                if ($scope.requirementDetails.isDiscountQuotation === 1) {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(itemdetailsTemp[0].revUnitDiscount, 2);

                                    poItemObj.PO_NETPRICE = parseFloat(itemdetailsTemp[0].unitMRP);
                                    poItemObj.PO_PRICE = parseFloat(itemdetailsTemp[0].unitMRP);
                                    poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(itemdetailsTemp[0].unitMRP));

                                } else {
                                    poItemObj.DISCOUNT_VALUE = $scope.precisionRound(parseFloat(((itemdetailsTemp[0].unitPrice - itemdetailsTemp[0].revUnitPrice) / itemdetailsTemp[0].unitPrice) * 100), 2);

                                    poItemObj.PO_NETPRICE = parseFloat(vendorItem.assignedPrice);
                                    poItemObj.PO_PRICE = parseFloat(vendorItem.assignedPrice);
                                    poItemObj.NET_PRICE_PUR_DOC = $scope.precisionRound(parseFloat(vendorItem.assignedPrice));
                                }

                                poItemObj.CGST = itemdetailsTemp[0].cGst;
                                poItemObj.SGST = itemdetailsTemp[0].sGst;
                                poItemObj.IGST = itemdetailsTemp[0].iGst;

                                poItemObj.MATERIAL_CODE = itemdetailsTemp[0].productCode.replace(/^0+/, ''); // to Remove leading zeros
                                poItemObj.CATEGORY_CODE = itemdetailsTemp[0].productCode.replace(/^0+/, ''); // to Remove leading zeros
                                //poItemObj.PRICE_UNIT = parseFloat(vendorItem.assignedPrice);
                                poItemObj.PRICE_UNIT = 1;
                                poItemObj.PO_UOM = itemdetailsTemp[0].productQuantityIn;
                                poItemObj.ORDER_PRICE_UNIT = itemdetailsTemp[0].productQuantityIn;



                                


                                poItemObj.QUANTITY = +itemdetailsTemp[0].qtyDistributed;
                                poItemObj.QUANTITY_TEMP = +itemdetailsTemp[0].qtyDistributed;
                                if (!poItemObj.QUANTITY) {
                                    poItemObj.quantityError = true;
                                }
                            }

                            if (prItemObj && prItemObj.length > 0) {
                                let prNumbers = _.map(prItemObj, 'PR_NUMBER').join(); //_.uniq(_.map(prItemObj, 'PR_NUMBER')).join();
                                poItemObj.PR_NUMBER = prNumbers;
                                let prItemTotalQuantity = _.sumBy(prItemObj, function (item) {
                                    return item.REQUIRED_QUANTITY;
                                });

                                if (poItemObj.QUANTITY && prItemTotalQuantity && poItemObj.QUANTITY > prItemTotalQuantity) {
                                    poItemObj.QUANTITY = prItemTotalQuantity;
                                }

                                if ($scope.requirementPOItems && $scope.requirementPOItems.length > 0) {
                                    var currentVendorPOItems = _.filter($scope.requirementPOItems, function (reqPOItem) {
                                        return reqPOItem.VENDOR_ID === +$scope.formDetails.VENDOR_ID && reqPOItem.MATNR === prItemObj[0].ITEM_CODE.replace(/^0+/, '');
                                    });

                                    if (poItemObj.QUANTITY && currentVendorPOItems && currentVendorPOItems.length > 0) {
                                        poItemObj.QUANTITY = poItemObj.QUANTITY - currentVendorPOItems.MENGE;
                                    }
                                }

                                let prLineItems = _.map(prItemObj, 'ITEM_NUM').join(); //_.uniq(_.map(prItemObj, 'ITEM_NUM')).join();
                                poItemObj.ITEM_OF_REQUESITION = prLineItems;
                                let prPlants = _.map(prItemObj, 'PLANT').join(); //_.uniq(_.map(prItemObj, 'ITEM_NUM')).join();
                                poItemObj.PLANT = prPlants;
                                poItemObj.MATERIAL_TYPE = prItemObj[0].MATERIAL_TYPE;
                                poItemObj.SHORT_TEXT = prItemObj[0].SHORT_TEXT;
                                //poItemObj.PLANT = prItemObj[0].PLANT;
                                //poItemObj.PO_NETPRICE = vendorItem.assignedPrice * vendorItem.assignedQty;
                                poItemObj.MATERIAL_CODE = prItemObj[0].ITEM_CODE.replace(/^0+/, ''); // to Remove leading zeros
                                poItemObj.PRODUCT_ID = prItemObj[0].PRODUCT_ID;
                                poItemObj.CATEGORY_ID = prItemObj[0].CATEGORY_ID;
                                poItemObj.PO_MATERIAL_DESC = prItemObj[0].ITEM_DESC;
                                poItemObj.CATEGORY_CODE = prItemObj[0].CategoryCode;
                                poItemObj.PRODUCT_CODE = prItemObj[0].ITEM_CODE;
                            }

                            //Packing & Forwarding charges
                            var packaging = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Packing & Forwarding charges';
                            });

                            if (packaging && packaging.length > 0) {
                                poItemObj.PACK_FORWARD_PER = packageTax; //parseFloat((packageTax / totalVendorItems).toFixed(2));
                                poItemObj.PACK_VALUE = parseFloat((packageValWithTax / totalVendorItems).toFixed(2));
                            }

                            //Freight charges
                            var freightCharges = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Freight charges';
                            });

                            if (freightCharges && freightCharges.length > 0) {
                                poItemObj.FREIGHT_TAXABLE_PER = freightTax;//parseFloat((freightTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_VALUE = parseFloat((freightValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.FREIGHT_NON_TAX_PER = parseFloat((freightValWithOutTax / totalVendorItems).toFixed(2));
                            }

                            //Miscellenous
                            var miscellenous = _.filter(currentVendorObj[0].listRequirementItems, function (reqItem) {
                                return reqItem.productCode === 'Miscellenous';
                            });

                            if (miscellenous && miscellenous.length > 0) {
                                poItemObj.MISC_TAXABLE_PER = miscTax; //parseFloat(( miscTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_TAXABLE_VAL = parseFloat((miscValWithTax / totalVendorItems).toFixed(2));
                                poItemObj.MISC_VALUE_NONTAX = parseFloat((miscValWithOutTax / totalVendorItems).toFixed(2));
                            }


                            userService.getProfileDetails({ "userid": $scope.formDetails.VENDOR_ID, "sessionid": $scope.currentUserSessionId })
                                .then(function (response) {
                                    if (response) {
                                        SelectedVendorDetails = response;
                                        if (SelectedVendorDetails) {
                                            //$scope.formDetails.PAYMENT_TERMS = SelectedVendorDetails.paymentTermDesc;
                                            //$scope.formDetails.PAYMENT_TERMS_HIDE = SelectedVendorDetails.paymentTermCode;
                                            //$scope.formDetails.INCO_TERMS1 = SelectedVendorDetails.incoTerm;
                                            if (!$scope.formDetails.VENDOR_CODE) {
                                                $scope.formDetails.VENDOR_CODE = SelectedVendorDetails.sapVendorCode ? SelectedVendorDetails.sapVendorCode : '';
                                            }

                                            $scope.COMPANY_INCO_TERMS.forEach(function (term, idx) {
                                                if (term.configValue == $scope.formDetails.INCO_TERMS1) {
                                                    $scope.formDetails.INCO_TERMS2 = term.configText;
                                                }
                                            });
                                        }
                                    }
                                });


                            if (poItemObj.PR_NUMBER) {
                                $scope.formDetails.items.push(poItemObj);
                            }
                        });
                    }
                }
            };

            $scope.GetPRItemsByRequirementId = function () {
                $scope.formDetails.PURCHASE_GROUP_NAMES = '';
                $scope.formDetails.PURCHASE_GROUP_CODE = '';

                if ($state.current.name === "po-service-zssr") {
                    $scope.formDetails.PURCHASE_GROUP_CODE = "144";
                    $scope.formDetails.ACCTASSCAT = "K";
                } else if ($state.current.name === "po-domestic-zsdm") {
                    $scope.formDetails.PURCHASE_GROUP_CODE = "119";
                    $scope.formDetails.ACCTASSCAT = "A";
                } else if ($state.current.name === "po-bonded-wh") {
                    $scope.formDetails.PURCHASE_GROUP_CODE = "119";
                    $scope.formDetails.ACCTASSCAT = "K";
                } else {
                    $scope.formDetails.PURCHASE_GROUP_CODE = "119";
                    $scope.formDetails.ACCTASSCAT = "";
                }

                PRMPRServices.GetPRItemsByReqId({ reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPRItems = response;
                            if ($scope.requirementPRItems && $scope.requirementPRItems.length > 0) {
                                //$scope.formDetails.PURCHASE_GROUP_CODE = $scope.requirementPRItems[0].PURCHASE_GROUP;
                                $scope.formDetails.GMP_TYPE = $scope.requirementPRItems[0].GMP_NGMP === 'GMP' ? true : false;
                                $scope.formDetails.GMP_TYPE_HIDE = $scope.requirementPRItems[0].GMP_NGMP;
                            }
                        }

                        if ($scope.requirementPlants.length === 1) {
                            $scope.currentUIPlant = $scope.requirementPlants[0];
                        }
                        if ($scope.selectedVendors.length === 1) {
                            $scope.currentUIVendor = $scope.selectedVendors[0];
                        }

                        if ($scope.currentUIVendor && $scope.currentUIVendor.vendorId && $scope.currentUIPlant && $scope.currentUIPlant.PLANT_CODE) {
                            $scope.filterSelectionChange();                            
                        }

                        if ($scope.poRAWJSON) {
                            if ($scope.poRAWJSON && $scope.poRAWJSON.items && $scope.poRAWJSON.items.length > 0) {
                                let selectedPlant = $scope.poRAWJSON.items[0].PLANT;
                                let selectedVendor = $scope.poRAWJSON.VENDOR_ID;
                                if ($scope.requirementPlants && $scope.requirementPlants.length > 1) {
                                    let filteredPlant = _.filter($scope.requirementPlants, function (plant) {
                                        return plant.PLANT_CODE === selectedPlant;
                                    });

                                    if (filteredPlant && filteredPlant.length > 0) {
                                        $scope.currentUIPlant = filteredPlant[0];
                                    }
                                }

                                if ($scope.selectedVendors && $scope.selectedVendors.length > 1) {
                                    let filteredVendor = _.filter($scope.selectedVendors, function (vendor) {
                                        return vendor.vendorId === selectedVendor;
                                    });

                                    if (filteredVendor && filteredVendor.length > 0) {
                                        $scope.currentUIVendor = filteredVendor[0];
                                    }
                                }
                            }

                            $scope.formDetails = $scope.poRAWJSON;
                        }
                    });
            };

            $scope.GetPRItemsByRequirementId();

            $scope.GetRequirementPO = function () {
                PRMPOServices.getRequirementPO({ compid: $scope.currentUsercompanyId, reqid: $scope.reqId })
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementPOItems = response;
                        }
                    });
            };

            $scope.GetRequirementPO();

            $scope.GetRequirementVendorCodes = function () {
                auctionsService.getRequirementVendorCodes($scope.reqId, $scope.currentUserSessionId)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.requirementVendorCodeInfo = response;
                        }
                    });
            };

            $scope.GetRequirementVendorCodes();

            $scope.submitPOToSAP = function (isDraft) {
                let sapDataList = [];
                $scope.POServiceStatusText = '';
                if ($scope.formDetails && $scope.formDetails.items && $scope.formDetails.items.length > 0) {
                    $scope.formDetails.items.forEach(function (poItem, index) {
                        if (!poItem.isDeleted) {
                            let PRNumberList = [];
                            let PRItemListList = [];
                            let PRPlantsList = [];

                            let counter = 0;
                            if (poItem.PR_NUMBER.includes(',')) {
                                PRNumberList = poItem.PR_NUMBER.split(',');
                                PRItemListList = poItem.ITEM_OF_REQUESITION.split(',');
                                PRPlantsList = poItem.PLANT.split(',');
                            }
                            else {
                                PRNumberList.push(poItem.PR_NUMBER);
                                PRItemListList.push(poItem.ITEM_OF_REQUESITION);
                                PRPlantsList.push(poItem.PLANT);
                            }
                            
                            PRNumberList.forEach(function (prNumber, index) {
                                if (!poItem.QUANTITY) {
                                    poItem.quantityError = true;
                                    return;
                                } else {
                                    poItem.quantityError = false;
                                }

                                sapDataList.push({
                                    COMP_ID: $scope.currentUsercompanyId,
                                    REQ_ID: $scope.reqId,
                                    QCS_ID: $scope.qcsId,
                                    PO_TEMPLATE: $scope.documentType,
                                    VENDOR_CODE: $scope.formDetails.VENDOR_CODE,
                                    DOC_TYPE: $scope.formDetails.DOC_TYPE,
                                    PO_CREATOR: $scope.formDetails.PO_CREATOR,
                                    INCO_TERMS1: $scope.formDetails.INCO_TERMS1,
                                    INCO_TERMS2: $scope.formDetails.INCO_TERMS2,
                                    PURCHASE_GROUP_CODE: $scope.formDetails.PURCHASE_GROUP_CODE,
                                    CURRENCY: $scope.formDetails.PO_CURRENCY,
                                    VENDOR_ID: $scope.formDetails.VENDOR_ID,
                                    PLANT: PRPlantsList[counter],//poItem.PLANT,
                                    PLANT_NAME: poItem.PLANT_NAME,
                                    //MTART: poItem.MATERIAL_TYPE,
                                    PRODUCT_CODE: poItem.PRODUCT_CODE,
                                    PRODUCT_ID: poItem.PRODUCT_ID,
                                    CATEGORY_ID: poItem.CATEGORY_ID,
                                    PO_MATERIAL_DESC: poItem.PO_MATERIAL_DESC,
                                    PR_NUMBER: prNumber, //poItem.PR_NUMBER,
                                    PR_LINE_ITEM: PRItemListList[counter], //poItem.ITEM_OF_REQUESITION,
                                    UOM: poItem.PO_UOM,
                                    ORDER_PRICE_UNIT: 0, //PENDING/DOUBT
                                    PRICE_UNIT: poItem.PRICE_UNIT,
                                    TAX_CODE: poItem.TAX_CODE,
                                    PBXX: poItem.PO_PRICE ? $scope.precisionRound(parseFloat(poItem.PO_PRICE)) : 0,
                                    ITEM_DISCOUNT_VALUE: poItem.DISCOUNT_VALUE ? $scope.precisionRound(poItem.DISCOUNT_VALUE) : 0,
                                    ORDER_QTY: poItem.QUANTITY ? $scope.precisionRound(parseFloat(poItem.QUANTITY)) : 0,
                                    NET_PRICE: $scope.precisionRound(poItem.PO_NETPRICE),
                                    CGST: $scope.precisionRound(poItem.CGST),
                                    SGST: $scope.precisionRound(poItem.SGST),
                                    IGST: $scope.precisionRound(poItem.IGST),
                                    QUOT_NO: $scope.poQuotId ? $scope.poQuotId : '',
                                    SHORT_TEXT: poItem.SHORT_TEXT ? poItem.SHORT_TEXT : '',
                                    CREATED_BY: $scope.currentUserId,
                                    PAYMENT_TERMS: $scope.formDetails.PAYMENT_TERMS,
                                    CATEGORY_CODE: poItem.CATEGORY_CODE,
                                    ACCTASSCAT: $scope.formDetails.ACCTASSCAT,
                                    VENDOR_COMPANY: $scope.formDetails.VENDOR_COMPANY,
                                    STEUC: $scope.formDetails.STEUC,
                                    STGE_LOC: $scope.formDetails.STGE_LOC,
                                    sessionID: $scope.currentUserSessionId
                                });

                                counter++;
                            });
                        }
                    });

                    var params = {
                        "data": sapDataList,
                        //"rawJSON": JSON.stringify($scope.formDetails),
                        //"qcsVendorAssignmentJSON": JSON.stringify($scope.vendorAssignments),
                        //"qcsRequirementJSON": JSON.stringify($scope.requirementDetails),
                        "isDraft": isDraft ? true : false
                    };

                    PRMPOServices.SavePODetailsToSAP(params)
                        .then(function (response) {
                            $scope.GetRequirementPO();
                            if (!response.errorMessage) {
                                //swal("Thanks !", response.message, "success");
                                $scope.POServiceStatusText = "PO Number# " + response.message + " Created";

                                swal({
                                    title: "Thanks !",
                                    text: "PO Number# " + response.message + " Created",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },
                                    function () {
                                        $state.go("cost-comparisions-qcs", { 'reqID': $scope.reqId, 'qcsID': $scope.qcsId, "poNumber": '', "poItems": null });
                                    });
                            } else {
                                swal("Error!", response.errorMessage, "error");
                                $scope.POServiceStatusText = response.errorMessage;
                            }
                        });
                }
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return url;//'<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }
            };


            $scope.updateItemTaxCode = function (formDetails) {
                formDetails.items.forEach(function (item) {
                    item.TAX_CODE = formDetails.TAX_CODE;
                })
            }

            $scope.changeQty = function (item) {
                if (item.QUANTITY_TEMP < item.QUANTITY) {
                    swal({
                        title: "Quantity",
                        text: "Please enter Quantity less than or equal to " + item.QUANTITY_TEMP,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                        });
                    item.QUANTITY = item.QUANTITY_TEMP;
                }
            }

        }]);