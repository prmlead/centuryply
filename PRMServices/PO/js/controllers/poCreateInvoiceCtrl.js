﻿prmApp
    .controller('poCreateInvoiceCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService,
        poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService) {

        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.invoiceNumber = $stateParams.invoiceNumber;
        $scope.poNumber = $stateParams.poNumber;
        //$scope.asnCode = $stateParams.asnCode;
        $scope.invoiceID = $stateParams.invoiceID;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.compID = userService.getUserCompanyId();
        $scope.customerCompanyId = userService.getCustomerCompanyId();
        $scope.filteredPendingPOsList = {};
        $scope.selectedPODetails = [{
            itemAttachment: [],
            attachmentName: '',
            attachmentsArray: []
        }];

        $scope.pendingPOItems = [{
            itemAttachment: [],
            attachmentName: '',
            attachmentsArray: []
        }];

        $scope.billedQty = 0;
        $scope.receivedQty = 0;
        $scope.rejectedQty = 0;
        $scope.remainingQty = 0;
        $scope.removeQty = 0;
        $scope.isError = false;
        $scope.ALTERNATIVE_UOM = '';
        $scope.ALTERNATIVE_UOM_QTY = '';
        $scope.SERVICE_CODE = '';
        $scope.SERVICE_DESCRIPTION = '';
        $scope.MISC_CHARGES = '';
        $scope.maxDateMoment = moment();

        if ($scope.isCustomer) {
            $scope.SelectedDeptId = 0;
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if (userService.getSelectedUserDepartmentDesignation()) {
                $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
            }
            $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
            $scope.UserLocation = $scope.SelectedUserDepartmentDesignation.userLocation;
        }

        $scope.filters = {
            fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
        }
        $scope.checkInvoiceUniqueResult = false;
        $scope.isSaveDisabled = false;

        $scope.getPendingPOOverall = function () {
            let isExport = false;
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            var params = {
                "compid": $scope.isCustomer ? $scope.compID : 0,
                "uid": $scope.isCustomer ? 0 : +$scope.userID,
                "search": $scope.poNumber,
                "categoryid": '',
                "productid": '',
                "supplier": '',
                "postatus": '',
                "deliverystatus": '',
                "plant": '',
                "fromdate": '1970-01-01',
                "todate": '2100-01-01',
                "page": 0,
                "pagesize": 10,
                "onlycontracts": $scope.onlyContracts,
                "excludecontracts": $scope.excludeContracts,
                "ackStatus": '',
                "buyer": '',
                "purchaseGroup": '',
                "sessionid": userService.getUserToken()
            };

            PRMPOService.getPOScheduleList(params)
                .then(function (response) {
                    if (response && response.length > 0) {
                        response = _.orderBy(response, ['PO_DATE'], ['desc']);
                        response.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                            item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                            item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                            item.LAST_RECEIVED_DATE = item.LAST_RECEIVED_DATE ? moment(item.LAST_RECEIVED_DATE).format("DD-MM-YYYY") : '-';
                            item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? moment(item.VENDOR_EXPECTED_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.PO_DATE = item.PO_DATE ? moment(item.PO_DATE).format("DD-MM-YYYY") : '-';
                            item.multipleAttachments = [];
                            item.INVOICE_AMOUNT = 0;
                            item.INVOICE_NUMBER = '';
                            item.COMMENTS = '';
                            item.isAcknowledgeOverall = false;
                            item.fontStyle = {};
                            //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                        });
                    }

                    if (!isExport) {
                        $scope.pendingPOList = [];
                        $scope.filteredPendingPOsList = {};
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                $scope.pendingPOList.push(item);
                            });
                        }

                        if ($scope.pendingPOList && $scope.pendingPOList.length > 0) {
                            $scope.totalItems = $scope.pendingPOList[0].TOTAL_COUNT;
                            $scope.filteredPendingPOsList = $scope.pendingPOList[0];


                            $scope.filteredPendingPOsList = $scope.pendingPOList[0];

                            if ($scope.filteredPendingPOsList.PO_NUMBER !== '') {
                                var params1 = {
                                    "ponumber": $scope.poNumber,
                                    "moredetails": 1,
                                    "forasn": false
                                };
                                PRMPOService.getPOScheduleItems(params1)
                                    .then(function (response) {
                                        $scope.pendingPOItems = response;
                                        $scope.pendingPOItems.forEach(function (item, index) {
                                            let invQty = 0;
                                            if (item.GRNItems && item.GRNItems.length > 0) {
                                                item.GRNItems.forEach(function (grnItem, grnIndex) {
                                                    if (grnItem.INVOICE_NUMBER) {
                                                        invQty += grnItem.GRN_RECEIVED_QTY
                                                    }
                                                });
                                            }
                                            item.INVOICE_QTY = (item.ORDER_QTY - invQty);
                                        });
                                    });
                            }

                        }
                    }

                });
        };

        $scope.getPOScheduleItems = function () {
            var params1 = {
                "ponumber": $scope.poNumber,
                "moredetails": 0,
                "forasn": false
            };
            PRMPOService.getPOScheduleItems(params1)
                .then(function (response) {
                    $scope.pendingPOItems = response;
                    $scope.pendingPOItems.forEach(function (item, index) {
                        let invQty = 0;
                        if (item.GRNItems && item.GRNItems.length > 0) {
                            item.GRNItems.forEach(function (grnItem, grnIndex) {
                                if (grnItem.INVOICE_NUMBER) {
                                    invQty += grnItem.GRN_RECEIVED_QTY
                                }
                            });
                        }
                        item.INVOICE_QTY = (item.ORDER_QTY - invQty);
                    });
                });
        };


        $scope.getPendingPOOverall();

        //$scope.getPOScheduleItems();

        $scope.createInvoice = function (poNumber) {
            var url = $state.href('vendorPoInvoices', { "poNumber": poNumber });
            $window.open(url, '_blank');
        };

        $scope.saveInvoice = function (item) {
            $scope.invoiceDateError = false;
            $scope.refDocNumberError = false;

            if ($scope.filteredPendingPOsList.INVOICE_DATE_1 == '' || $scope.filteredPendingPOsList.INVOICE_DATE_1 == undefined) {
                $scope.invoiceDateError = true;
            }

            if ($scope.filteredPendingPOsList.REF_DOC_NUMBER == '' || $scope.filteredPendingPOsList.REF_DOC_NUMBER == undefined) {
                $scope.refDocNumberError = true;
            }

            if ($scope.invoiceDateError || $scope.refDocNumberError) {
                return;
            }

            $scope.mainArr = [];

            $scope.pendingPOItems.forEach(function (item, index) {

                var ts = moment($scope.filteredPendingPOsList.INVOICE_DATE_1, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                let INVOICE_DATE = "/Date(" + milliseconds + "000+0530)/";

                let newObj =
                {
                    "INVOICE_DATE": INVOICE_DATE,
                    "INVOICE_DATE_TEMP": $scope.filteredPendingPOsList.INVOICE_DATE_1,
                    "INVOICE_NUMBER": $scope.filteredPendingPOsList.REF_DOC_NUMBER,
                    "VENDOR_CODE": $scope.filteredPendingPOsList.VENDOR_CODE,
                    "VENDOR_COMPANY": $scope.filteredPendingPOsList.VENDOR_COMPANY,
                    "VENDOR_ID": $scope.filteredPendingPOsList.VENDOR_ID,
                    "PO_NUMBER": item.PO_NUMBER,
                    "PO_LINE_ITEM": item.PO_LINE_ITEM,
                    "U_ID": +$scope.userID,
                    "C_COMP_ID": +$scope.customerCompanyId,
                    "QUANTITY": item.INVOICE_QTY,
                    "V_COMP_ID": +$scope.compID,
                    "createdBy": $scope.userID,
                    "GRN_NUMBER": item.GRN_NUMBER,
                    "GRN_YEAR": item.PO_DATE ? moment(item.PO_DATE, "DD/MM/YYYY").year() : '2023',
                    "GRN_LINE_ITEM": item.GRN_LINE_ITEM,
                };

                $scope.mainArr.push(newObj);
            });

            var params =
            {
                "INVOICE_ARR": $scope.mainArr,
                "doSubmitToSAP": false,
                "sessionId": $scope.sessionID
            };


            PRMPOService.savePOInvoice(params)
                .then(function (response) {
                    if (response.errorMessage) {
                        swal("Error!", response.errorMessage, "error");
                    }
                    else {
                        swal({
                            title: "Thanks !",
                            text: "Saved Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                $state.go("listInvoices");
                            });
                    }
                });
        };


        $scope.checkUserUniqueResult = function (idtype, inputvalue) {
            if (!$scope.checkInvoiceUniqueResult) {
                $scope.checkInvoiceUniqueResult = false;
            }
            if (inputvalue == "" || inputvalue == undefined) {
                $scope.isSaveDisabled = false;
                return false;
            }

            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "INVOICE") {
                    if ($scope.checkInvoiceUniqueResult = !response) {
                        $scope.checkInvoiceUniqueResult = !response;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.isSaveDisabled = false;
                    }
                }
            });
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;
            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.totalASNSize = 0;
            if ($scope.filesTemp && $scope.filesTemp.length > 0) {
                $scope.filesTemp.forEach(function (item, index) {
                    $scope.totalASNSize = $scope.totalASNSize + item.size;
                });
            }
            if (($scope.totalASNSize + $scope.totalASNItemSize) > $scope.totalAttachmentMaxSize) {
                swal({
                    title: "Attachment size!",
                    text: "Total Attachments size cannot exceed 6MB",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        return;
                    });
                return;
            }

            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: 0,
                            fileSize: 0
                        };
                        var bytearray = new Uint8Array(result);
                        fileUpload.fileSize = result.byteLength;
                        fileUpload.fileStream = $.makeArray(bytearray);
                        fileUpload.fileName = attach.name;
                        if (!$scope.selectedPODetails[0].attachmentsArray) {
                            $scope.selectedPODetails[0].attachmentsArray = [];
                        }

                        var ifExists = _.findIndex($scope.selectedPODetails[0].attachmentsArray, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                        if (ifExists <= -1) {
                            $scope.selectedPODetails[0].attachmentsArray.push(fileUpload);
                        }

                    });
            });
        };

        $scope.removeAttach = function (index) {
            $scope.selectedPODetails[0].attachmentsArray.splice(index, 1);
        };

        $scope.cancelInvoice = function () {
            var url = $state.href("listInvoices");
            $window.open(url, '_self');
        };
    });