﻿using System;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;


namespace PRMServices.Models
{
    public class LPPEntity : Entity
    {
        [DataMember(Name = "COMP_ID")]
        [DataNames("COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember(Name = "CATALOGUE_ITEM_ID")]
        [DataNames("CATALOGUE_ITEM_ID")]
        public int CATALOGUE_ITEM_ID { get; set; }

        [DataMember(Name = "REV_UNIT_PRICE")]
        [DataNames("REV_UNIT_PRICE")]
        public decimal REV_UNIT_PRICE { get; set; }

        [DataMember(Name = "SELECTED_VENDOR_CURRENCY")]
        [DataNames("SELECTED_VENDOR_CURRENCY")]
        public string SELECTED_VENDOR_CURRENCY { get; set; }

        [DataMember(Name = "VENDOR_ID")]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember(Name = "VENDOR_NAME")]
        [DataNames("VENDOR_NAME")]
        public string VENDOR_NAME { get; set; }

        [DataMember(Name = "CURRENCY_FACTOR")]
        [DataNames("CURRENCY_FACTOR")]
        public decimal CURRENCY_FACTOR { get; set; }

        [DataMember(Name = "REQ_ID")]
        [DataNames("REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "REQ_CURRENCY")]
        [DataNames("REQ_CURRENCY")]
        public string REQ_CURRENCY { get; set; }

        [DataMember(Name = "REQ_CURRENCY_FACTOR")]
        [DataNames("REQ_CURRENCY_FACTOR")]
        public decimal REQ_CURRENCY_FACTOR { get; set; }

        [DataMember(Name = "RFQ_UNITS")]
        [DataNames("RFQ_UNITS")]
        public string RFQ_UNITS { get; set; }

        [DataMember(Name = "DATE_CREATED")]
        [DataNames("DATE_CREATED")]
        public DateTime? DATE_CREATED { get; set; }

        [DataMember(Name = "QUANTITY")]
        [DataNames("QUANTITY")]
        public decimal QUANTITY { get; set; }
    }
}