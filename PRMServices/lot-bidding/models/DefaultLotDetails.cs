﻿using PRM.Core.Common;
using PRM.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PRMServices.models
{
    [DataContract]
    public class DefaultLotDetails : Entity
    {
        [DataMember]
        [DataNames("LOT_ID")]
        public int LotId { get; set; }

        [DataMember]
        [DataNames("REQ_ID")]
        public int ReqId { get; set; }

        [DataMember]
        [DataNames("IS_DEFAULT_LOT")]
        public int IsDefaultLot { get; set; }

        [DataMember]
        [DataNames("DEFAULT_ITEMS")]
        public string DefaultItems { get; set; }

        [DataMember]
        [DataNames("DEFAULT_VENDORS")]
        public string DefaultVendors { get; set; }

        [DataMember]
        [DataNames("LOT_TITLE")]
        public string LotTitle { get; set; }

        [DataMember]
        [DataNames("LOT_DESC")]
        public string LotDesc { get; set; }

        [DataMember]
        [DataNames("START_TIME")]
        public DateTime? StartTime { get; set; }

        [DataMember]
        [DataNames("END_TIME")]
        public DateTime? EndTime { get; set; }

        [DataMember]
        [DataNames("REQ_POSTED_ON")]
        public DateTime? PostedOn { get; set; }

        [DataMember]
        [DataNames("POSTED_BY")]
        public string PostedBy { get; set; }

        [DataMember]
        [DataNames("DURATION")]
        public int Duration { get; set; }

        [DataMember]
        [DataNames("PROJECT_ID")]
        public int ProjectId { get; set; }

        [DataMember]
        [DataNames("NUMBER_OF_LOTS")]
        public int TotalLots { get; set; }

        [DataMember]
        [DataNames("STATUS")]
        public string Status { get; set; }
    }
}