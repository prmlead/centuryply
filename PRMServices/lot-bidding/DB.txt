﻿Added: 
	Procedures:
		1. lot_GetLotRequirements
		2. lot_MarkLotAsComplete
		3. lot_SaveLotDetailsByReq
		4. lot_SaveLotDetails
		5. lot_SaveLotRequirement
		6.
	Tables:
		1. RequirementDetails (Add Column LOT_ID);
		2. LOT_DETAILS table full

	Triggers: 
		1.TBD SHIVA WILL PROVIDE

Modified:
	1. cp_GetRequirementData
			note :
				1. (select COUNT(REQ_ID) from requirementdetails where REQ_ID in (SELECT REQ_ID FROM lotdetails where REQ_ID = P_REQ_ID))as LOT_COUNT line No: 84
				2. select REQ_ID, REQ_TITLE from requirementdetails where LOT_ID in (SELECT LOT_ID FROM lotdetails where REQ_ID = P_REQ_ID); last line of store procedure
	2. cp_UpdateBidTime
			Note: 
				1. DECLARE PARAM_TIME_DIFF int; line 37
				2. SELECT START_TIME, END_TIME, IS_STOPPED, LOT_ID INTO P_START_TIME, P_END_TIME, P_IS_STOPPED, P_LOT_ID from requirementdetails where REQ_ID = P_REQ_ID; line 39
				3. IF P_LOT_ID > 0 THEN
					BEGIN
						UPDATE `RequirementDetails` SET `END_TIME` = DATE_ADD(END_TIME, INTERVAL PARAM_TIME_DIFF SECOND),
						`START_TIME` = DATE_ADD(START_TIME, INTERVAL PARAM_TIME_DIFF SECOND) 
						WHERE REQ_ID <> P_REQ_ID AND LOT_ID = P_LOT_ID;
					END;
					END IF; line numbers 50-56

Removed:
	1.



Code Changes:

	Added :
		1. list-item.html
			a) line no : 207 - 209
			b) line no : 2290

		2. itemCtrl.js 
			a)functions
				-> Added :
					1. createLot()
					2. isValidLotBidding()
		
		3. PRMServices.svc
			a)functions
				-> added
					1. line 470
					2. line 789
					3. line 935
					4  lines 1116 - 1131 
