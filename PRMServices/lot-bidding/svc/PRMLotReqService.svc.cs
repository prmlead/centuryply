﻿using PRM.Core.Common;
using PRMServices.models;
using PRMServices.Models;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMLotReqService : IPRMLotReqService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        private PRMServices prmServices = new PRMServices();

        #region GetFunctions

        public bool isLotValid(int reqid, string sessionid)
        {
            bool isValid = false;
            Utilities.ValidateSession(sessionid);
            string query = $@"SELECT CLOSED FROM requirementdetails WHERE REQ_ID = {reqid};";
            var dataset = sqlHelper.ExecuteQuery(query);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                DataRow row = dataset.Tables[0].Rows[0];
                isValid = row["CLOSED"] != DBNull.Value ? Convert.ToBoolean(Convert.ToString(row["CLOSED"]) == "Negotiation Ended" ? false : true) : true;
            }
            return isValid;
        }

        public LotDetails GetLotDetails(int reqid, int lotid, int user, string sessionid)
        {
            LotDetails details = new LotDetails();
            try
            {
                lotid = lotid == 0 ? -1 : lotid;
                reqid = reqid == 0 ? -1 : reqid;
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_USER", user);
                DataSet ds = sqlHelper.SelectList("lot_SaveLotDetailsByReq", sd);

                PRM.Core.Common.DataNamesMapper<LotDetails> mapper = new PRM.Core.Common.DataNamesMapper<LotDetails>();
                string query = string.Format("SELECT * FROM LotDetails WHERE REQ_ID = {0} OR LOT_ID = {1};", reqid, lotid);
                var dataset = sqlHelper.ExecuteQuery(query);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(dataset.Tables[0]).ToList().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public List<LotDetails> GetCompanyLots(int compid, int vendorid, string sessionid)
        {
            List<LotDetails> details = new List<LotDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                PRM.Core.Common.DataNamesMapper<LotDetails> mapper = new PRM.Core.Common.DataNamesMapper<LotDetails>();
                string query = $@" Select LD.*,RD.*, (SELECT COUNT(LOT_ID) FROM RequirementDetails WHERE LOT_ID = LD.LOT_ID AND CLOSED <> 'DELETED') AS NUMBER_OF_LOTS, CONCAT(U_FNAME,' ',U_LNAME) AS POSTED_BY  
                                 from requirementdetails RD inner join LotDetails LD on LD.REQ_ID = RD.REQ_ID INNER JOIN User U ON U.U_ID = LD.CREATED_BY
                                 where RD.LOT_ID = 0 AND LD.COMP_ID = {compid}  ORDER BY LD.LOT_ID DESC;";

                if (vendorid > 0)
                {
                    query = $@"Select LD.*,RD.*, (SELECT COUNT(LOT_ID) FROM RequirementDetails WHERE LOT_ID = LD.LOT_ID  AND CLOSED <> 'DELETED') AS NUMBER_OF_LOTS, CONCAT(U_FNAME,' ',U_LNAME) AS POSTED_BY
                                 from requirementdetails RD inner join LotDetails LD on LD.REQ_ID = RD.REQ_ID 
								 INNER JOIN auctiondetails AD ON AD.REQ_ID = RD.REQ_ID INNER JOIN User U ON U.U_ID = LD.CREATED_BY
                                 where RD.LOT_ID = 0 AND AD.U_ID = {vendorid}  ORDER BY LD.LOT_ID DESC;";

         //           query = $@"Select LD.*,RD.*, (SELECT COUNT(LOT_ID) FROM RequirementDetails R inner join auctiondetails A on R.REQ_ID = A.REQ_ID  
         //                           WHERE A.U_ID = {vendorid} and LOT_ID = LD.LOT_ID AND CLOSED <> 'DELETED') AS NUMBER_OF_LOTS, 
         //                           CONCAT(U_FNAME,' ',U_LNAME) AS POSTED_BY
         //                        from requirementdetails RD inner join LotDetails LD on LD.REQ_ID = RD.REQ_ID 
								 //INNER JOIN auctiondetails AD ON AD.REQ_ID = RD.REQ_ID INNER JOIN User U ON U.U_ID = LD.CREATED_BY
         //                        where RD.LOT_ID = 0 AND AD.U_ID = {vendorid}  ORDER BY RD.REQ_ID DESC;";
                }
                
                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving log details");
            }

            return details;
        }

        public Requirement[] GetLotRequirements(int reqid, int lotid, int vendorid, string sessionid)
        {
            List<Requirement> details = new List<Requirement>();
            try
            {
                PRMServices prmservice = new PRMServices();
                Utilities.ValidateSession(sessionid);
                PRM.Core.Common.DataNamesMapper<Requirement> mapper = new PRM.Core.Common.DataNamesMapper<Requirement>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_LOT_ID", lotid);
                sd.Add("P_VENDOR_ID", vendorid);
                var dataset = sqlHelper.SelectList("lot_GetLotRequirements", sd);
                //details = mapper.Map(dataset.Tables[0]).ToList();
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        Requirement requirement = new Requirement();

                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        //requirement.CustomerID = userID;
                        requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }
                        requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        requirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (start < DateTime.UtcNow && RunPrice > 0)
                        {
                            requirement.Price = RunPrice;
                        }
                        string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt16(row["IS_DISCOUNT_QUOTATION"]) : 0;

                        requirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt16(row["NO_OF_VENDORS_INVITED"]) : 0;
                        requirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt16(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                        requirement.TotalCount = row["LOT_TOTAL_ITEMS"] != DBNull.Value ? Convert.ToInt16(row["LOT_TOTAL_ITEMS"]) : 0;
                        requirement.IsDefaultLot = row["IS_DEFAULT_LOT"] != DBNull.Value ? Convert.ToInt16(row["IS_DEFAULT_LOT"]) : 0;

                        requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.UtcNow;
                        requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                        requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.UtcNow;
                        requirement.UserName = row["USER_NAME"] != DBNull.Value ? Convert.ToString(row["USER_NAME"]) : string.Empty;

                        requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                        requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.UtcNow;
                        requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                        requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;
                        DateTime now = DateTime.UtcNow;
                        if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (requirement.EndTime < now)
                        {
                            requirement.TimeLeft = -1;
                            if ((Status == GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                            {

                                requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                prmservice.EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionid);
                            }
                            else
                            {
                                requirement.Status = Status;
                            }
                        }
                        else if (requirement.StartTime == DateTime.MaxValue)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        if (Status == GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = Status;
                        }

                        if (requirement.IS_CB_ENABLED)
                        {
                            if (requirement.CB_END_TIME > now)
                            {
                                requirement.Status = GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                        }

                        requirement.AuctionVendors = new List<VendorDetails>();
                        requirement.CustFirstName = string.Empty;
                        requirement.CustLastName = string.Empty;
                        //requirement.Status = string.Empty;
                        requirement.SessionID = string.Empty;
                        requirement.ErrorMessage = string.Empty;
                        if (vendorid > 0)
                        {
                            bool isVendorPart = row["IS_VENDOR_PART"] != DBNull.Value ? (Convert.ToInt32(row["IS_VENDOR_PART"]) == 1 ? true : false) : false;
                            var isEligible = row["IS_ELIGIBLE"] != DBNull.Value ? Convert.ToString(row["IS_ELIGIBLE"]) : string.Empty;
                            if (isVendorPart && row["IS_ELIGIBLE"] != DBNull.Value)
                            {
                                requirement.AuctionVendors = new List<VendorDetails>();
                                requirement.AuctionVendors.Add(new VendorDetails() {
                                    // QuotationUrl = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty,
                                    //IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt16(row["IS_QUOTATION_REJECTED"]) : -1,
                                    VendorID = row["VENDOR_ID"] != DBNull.Value ? Convert.ToInt16(row["VENDOR_ID"]) : -1,
                                    IS_ELIGIBLE = row["IS_ELIGIBLE"] != DBNull.Value ? Convert.ToString(row["IS_ELIGIBLE"]) : string.Empty,
                                    IS_VENDOR_PART = row["IS_VENDOR_PART"] != DBNull.Value ? (Convert.ToInt32(row["IS_VENDOR_PART"]) == 1 ? true : false) : false,
                            });
                            }
                            
                            if (!isVendorPart && isEligible == "YES")
                            {
                                requirement.AuctionVendors = new List<VendorDetails>();
                                requirement.AuctionVendors.Add(new VendorDetails()
                                {
                                    IS_VENDOR_PART = row["IS_VENDOR_PART"] != DBNull.Value ? (Convert.ToInt32(row["IS_VENDOR_PART"]) == 1 ? true : false) : false,
                                    // QuotationUrl = row["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row["QUOTATION_URL"]) : string.Empty,
                                    //IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt16(row["IS_QUOTATION_REJECTED"]) : -1,
                                    IS_ELIGIBLE = "NO",

                                });
                            }
                        }

                        details.Add(requirement);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot requirement details");
            }

            return details.ToArray();
        }

        public Requirement[] GetLotRequirementItems(int id, string sessionid)
        {
            List<Requirement> details = new List<Requirement>();
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = string.Format("select * from requirementitems RI inner join requirementdetails RD on RD.REQ_ID = RI.REQ_ID WHERE RD.LOT_ID = {0} AND IS_DEFAULT_LOT <> 1", id);
                var dataset = sqlHelper.ExecuteQuery(query);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        Requirement requirement = new Requirement();

                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        requirement.Title = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }

                        details.Add(requirement);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot requirement items");
            }

            return details.ToArray();
        }

        public Response MarkLotAsComplete(int lotid, int user, string sessionid)
        {
            Response response = new Response();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_LOT_ID", lotid);
                sd.Add("P_USER", user);
                DataSet ds = sqlHelper.SelectList("lot_MarkLotAsComplete", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    response.ObjectID = lotid;
                }
            }
            catch(Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public LotDetails GetLotDetailReport(int reqid, int lotid, int user, string sessionid)
        {
            LotDetails details = new LotDetails();
            Requirement req = new Requirement();
            try
            {
                req = prmServices.GetRequirementData(reqid, user, sessionid);

                if (req != null)
                {
                    foreach (LotAuctions LA in req.LotAuctions)
                    {
                        Requirement lotreq = new Requirement();
                        lotreq = prmServices.GetRequirementData(LA.ReqId, user, sessionid);
                        int i = 0;
                        //details.LotRequirements.Add(lotreq);
                    }
                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }
        #endregion

        #region SaveFunctions
        public Response SaveLotDetails(LotDetails details)
        {
            Response response = new Response();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_LOT_ID", details.LotId);
                sd.Add("P_COMP_ID", details.CompId);
                sd.Add("P_LOT_TITLE", details.LotTitle);    
                sd.Add("P_LOT_DESC", details.LotDesc);
                sd.Add("P_START_TIME", details.StartTime.Value);
                sd.Add("P_END_TIME", details.EndTime.Value);
                sd.Add("P_DURATION", (details.EndTime.Value - details.StartTime.Value).Minutes);
                sd.Add("P_PROJECT_ID", details.ProjectId);
                sd.Add("P_NUMBER_OF_LOTS", details.TotalLots);
                sd.Add("P_USER", details.ModifiedBy);
                DataSet ds = sqlHelper.SelectList("lot_SaveLotDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in saving log details");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateAuctionStart(List<Requirement> itemids, string sessionid, int user)
        {
            Response response = new Response();
            List<Requirement> details = new List<Requirement>();
            try
            {
                foreach (Requirement fd in itemids)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_REQ_ID", fd.RequirementID);
                    sd.Add("P_START_TIME", fd.StartTime);
                    sd.Add("P_END_TIME", fd.EndTime);
                    DataSet ds = sqlHelper.SelectList("lot_UpdatAuctionTime", sd);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in updating auction time");
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveLotRequirement(string itemids, string vendorids, int lotid, string title, string desc, DateTime start,
            DateTime end, string sessionid, int user, DefaultLotDetails defaultDetails)
        {
            Response response = new Response();
            Response response1 = new Response();
            PRMServices prm = new PRMServices();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_LOT_ID", lotid);
                sd.Add("P_ITEMS", itemids);
                sd.Add("P_TITLE", title);
                sd.Add("P_DESCRIPTION", desc);
                sd.Add("P_START_TIME", start);
                sd.Add("P_END_TIME", end);
                sd.Add("P_USER", user);
                sd.Add("P_VENDORS", vendorids);


                string[] vendorssList = vendorids.Split(',');

                DataSet dataset = sqlHelper.SelectList("lot_SaveLotRequirement", sd);

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    response.ObjectID = Convert.ToInt32(dataset.Tables[0].Rows[0][0]);
                    var time = Convert.ToString(dataset.Tables[0].Rows[0][1]);
                };

                int defReqID = 0;
                string defaultReqId = string.Format("SELECT REQ_ID FROM requirementdetails WHERE LOT_ID = {0} AND IS_DEFAULT_LOT = 1 LIMIT 1", lotid);
                DataSet R = sqlHelper.ExecuteQuery(defaultReqId);
                if (R != null && R.Tables.Count > 0 && R.Tables[0].Rows.Count > 0 && R.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = R.Tables[0].Rows[0];
                    defReqID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    
                }

                string delete1 = string.Format("DELETE FROM auctiondetails WHERE REQ_ID = {0};", defReqID);
                sqlHelper.ExecuteNonQuery_IUD(delete1);

                string delete2 = string.Format("DELETE FROM requirementitems WHERE REQ_ID = {0};", defReqID);
                sqlHelper.ExecuteNonQuery_IUD(delete2);

                string delete3 = string.Format("DELETE FROM quotations WHERE REQ_ID = {0};", defReqID);
                sqlHelper.ExecuteNonQuery_IUD(delete3);

                if (defaultDetails.DefaultItems == string.Empty)
                {
                    string query = string.Format("UPDATE requirementdetails SET CLOSED = 'DELETED' WHERE REQ_ID = {1} AND LOT_ID = {0} AND IS_DEFAULT_LOT = 1;", lotid, defReqID);
                    sqlHelper.ExecuteNonQuery_IUD(query);
                    //sd.Add("P_IS_DEFAULT", 0);
                };
                if (defaultDetails.DefaultItems != string.Empty)
                {
                    //string delete1 = string.Format("DELETE FROM auctiondetails WHERE REQ_ID = {0};", defReqID);
                    //sqlHelper.ExecuteNonQuery_IUD(delete1);

                    //string delete2 = string.Format("DELETE FROM requirementitems WHERE REQ_ID = {0};", defReqID);
                    //sqlHelper.ExecuteNonQuery_IUD(delete2);

                    //string delete3 = string.Format("DELETE FROM quotations WHERE REQ_ID = {0};", defReqID);
                    //sqlHelper.ExecuteNonQuery_IUD(delete3);

                 

                    SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                    sd1.Add("P_LOT_ID", defaultDetails.LotId);
                    sd1.Add("P_ITEMS", itemids);
                    sd1.Add("P_TITLE", defaultDetails.LotTitle);
                    sd1.Add("P_DESCRIPTION", defaultDetails.LotDesc);
                    sd1.Add("P_START_TIME", defaultDetails.StartTime);
                    sd1.Add("P_END_TIME", defaultDetails.EndTime);
                    sd1.Add("P_USER", user);
                    sd1.Add("P_VENDORS", vendorids);
                    sd1.Add("P_DEFAULT_ITEMS", defaultDetails.DefaultItems);
                    sd1.Add("P_DEFAULT_VENDORS", defaultDetails.DefaultVendors);
                    sd1.Add("P_IS_DEFAULT", 1);
                    sd1.Add("P_REQ_ID", defReqID);
                    string[] defaultvendorssList = defaultDetails.DefaultVendors.Split(',');
                    DataSet dataset1 = sqlHelper.SelectList("lot_SaveDefaultLotRequirement", sd1);
                    if (dataset1 != null && dataset1.Tables.Count > 0 && dataset1.Tables[0].Rows.Count > 0)
                    {
                        
                        response1.ObjectID = Convert.ToInt32(dataset1.Tables[0].Rows[0][0]);
                        
                        var time = Convert.ToString(dataset1.Tables[0].Rows[0][1]);
                    }

                    if (response1.ObjectID > 0)
                    {
                        foreach (var id in defaultvendorssList)
                        {
                            string body = string.Empty;
                            UserInfo user1 = prm.GetUserNew(Convert.ToInt32(id), sessionid);
                            UserInfo customerInfo = prm.GetUserNew(user, sessionid);
                            User altUser = prm.GetAlternateCommunications(response.ObjectID, Convert.ToInt32(id), sessionid);
                            string ScreenName = "ADD_REQUIREMENT";

                            body = prm.GenerateEmailBody("CustomeremailForLotAuctionStartUpdate");
                            body = String.Format(body, user1.FirstName, user1.LastName, response.ObjectID, prm.toLocal(start), title);
                            prm.SendEmail(user1.Email + "," + user1.AltEmail, "Negotiation Start time has been updated for RQID: " + response.ObjectID + " Title: " + title, body, response.ObjectID, Convert.ToInt32(user1.UserID), "LOT NEW REQ", sessionid, null, null, prm.toLocal(start), 15, "NEGOTATION_START").ConfigureAwait(false);

                            string body1 = prm.GenerateEmailBody("CustomersmsForLotAuctionStartUpdate");

                            body1 = String.Format(body1, user1.FirstName, user1.LastName, response.ObjectID, prm.toLocal(start), title);
                            body1 = body1.Replace("<br/>", "");
                           // prm.SendSMS(string.Empty, user1.PhoneNum + "," + user1.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, Convert.ToInt32(id), "NEW LOT REQ", sessionid).ConfigureAwait(false);

                            PushNotifications push = new PushNotifications();
                        }
                    }

                    
                };
                if (response.ObjectID > 0)
                {
                    foreach (var id in vendorssList)
                    {
                        string body = string.Empty;
                        UserInfo user1 = prm.GetUserNew(Convert.ToInt32(id), sessionid);
                        UserInfo customerInfo = prm.GetUserNew(user, sessionid);
                        User altUser = prm.GetAlternateCommunications(response.ObjectID, Convert.ToInt32(id), sessionid);
                        string ScreenName = "ADD_REQUIREMENT";

                        body = prm.GenerateEmailBody("CustomeremailForLotAuctionStartUpdate");
                        body = String.Format(body, user1.FirstName, user1.LastName, response.ObjectID, prm.toLocal(start), title);
                        prm.SendEmail(user1.Email + "," + user1.AltEmail, "Negotiation Start time has been updated for RQID: " + response.ObjectID + " Title: " + title, body, response.ObjectID, Convert.ToInt32(user1.UserID), "LOT NEW REQ", sessionid, null, null, prm.toLocal(start), 15, "NEGOTATION_START").ConfigureAwait(false);

                        string body1 = prm.GenerateEmailBody("CustomersmsForLotAuctionStartUpdate");

                        body1 = String.Format(body1, user1.FirstName, user1.LastName, response.ObjectID, prm.toLocal(start), title);
                        body1 = body1.Replace("<br/>", "");
                      //  prm.SendSMS(string.Empty, user1.PhoneNum + "," + user1.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, Convert.ToInt32(id), "NEW LOT REQ", sessionid).ConfigureAwait(false);

                        PushNotifications push = new PushNotifications();
                    }

                }

            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public DefaultLotDetails GetDefaultLotDetails(int lotid,string sessionid)
        {
            DefaultLotDetails details = new DefaultLotDetails();
            try
            {
                lotid = lotid == 0 ? -1 : lotid;
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_LOT_ID", lotid);
                DataSet ds = sqlHelper.SelectList("lot_getDefaultLotDetails", sd);

                PRM.Core.Common.DataNamesMapper<DefaultLotDetails> mapper = new PRM.Core.Common.DataNamesMapper<DefaultLotDetails>();
                
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    details = mapper.Map(ds.Tables[0]).ToList().FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving lot details");
            }

            return details;
        }

        public Response UpdateVendorRankComparision(int reqID, int userID, double minReduction, double rankComp, string sessionID)
        {
            Response response = new Response();

            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_REQ_ID", reqID);
            sd.Add("P_MIN_REDUCE_AMOUNT", minReduction);
            sd.Add("P_VENDOR_COMPARISION_MIN_AMOUNT", rankComp);
            DataSet ds = sqlHelper.SelectList("lot_UpdateVendorRankComparision", sd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
            }

            return response;
        }

        private string GetEnumDesc<T>(string value) where T : struct, IConvertible
        {
            return Utilities.GetEnumDesc<T>(value);
        }
        #endregion
    }
}