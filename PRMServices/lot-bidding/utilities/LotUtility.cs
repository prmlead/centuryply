﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using OfficeOpenXml;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;

namespace PRMServices.Common
{

    public class LotUtility
    {        
       public static DateTime toLocal(DateTime? date)
        {
            DateTime convertedDate = DateTime.SpecifyKind(DateTime.Parse(date.ToString()), DateTimeKind.Utc);

            var kind = convertedDate.Kind;

            DateTime dt = convertedDate.ToLocalTime();

            return dt;
        }
    }
}