prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('saveLotCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "techevalService", "catalogService", "PRMLotReqService", "SignalRFactory", "signalRHubName",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, techevalService, catalogService, PRMLotReqService, SignalRFactory, signalRHubName) {
            $scope.stateParamsLotId = $stateParams.Id;
            $scope.compid = userService.getUserCompanyId();
            $scope.DefaultRequirementDetails = {};
            $scope.defaultVendors = [];
            $scope.parentRequirementDetails = {};
            $scope.lotRequirementItems = [];
            $scope.RequirementVendors = [];
            $scope.itemSelectBeforeError = [];
            $scope.DefaultitemSelectBeforeError = [];
            $scope.groupItems = [];
            $scope.lotrequirements = [];
            $scope.lotDetails = {
                LotId: 0,
                CompId: userService.getUserCompanyId(),
                LotTitle: '',
                LotDesc: '',
                StartTime: '',
                EndTime: '',
                Duration: 0,
                ProjectId: 0,
                TotalLots: 0,
                Status: ''
            };

            $scope.DefaultlotDetailsTemp = {
                LotId: 0,
                CompId: userService.getUserCompanyId(),
                LotTitle: 'Default Lot',
                LotDesc: 'Default Lot Desc',
                StartTime: '',
                EndTime: '',
                Duration: 0,
                ProjectId: 0,
                TotalLots: 0,
                Status: ''
            };
            $scope.minDateMoment = moment();
            $scope.previousDate = '';
            $scope.DefaultlotDetails = {};
            $scope.lotID = parseInt($scope.stateParamsLotId);
            var tempDefaultItems = [];
            $scope.getDefaultlotDetails = function (val) {
                PRMLotReqService.GetDefaultLotDetails($scope.lotID)
                    .then(function (response1) {
                        if (response1) {
                            $scope.DefaultlotDetails = response1;
                            if (val) {
                                if ($scope.DefaultlotDetails.LotTitle == '' || $scope.DefaultlotDetails.LotTitle == null) {
                                    $scope.DefaultlotDetails.LotTitle = $scope.parentRequirementDetails.title;
                                    $scope.DefaultlotDetails.LotDesc = $scope.parentRequirementDetails.description;
                                }
                                if ($scope.DefaultlotDetails) {
                                    var items = $scope.DefaultlotDetails.DefaultItems.split(',');
                                    $scope.DefaultRequirementDetails.listRequirementItems = [];
                                    $scope.parentRequirementDetails.listRequirementItems.forEach(function (item, idx) {
                                        items.forEach(function (i) {
                                            i = parseInt(i);
                                            if (i == item.catalogueItemID) {
                                                item.defaultlotSelect = true;
                                                $scope.DefaultRequirementDetails.listRequirementItems.push(item);
                                                
                                            }
                                        })
                                    })


                                    //$scope.groupItems.forEach(function (item, idx) {
                                    //    items.forEach(function (i) {
                                    //        i = parseInt(i);
                                    //        if (i == item.catalogueItemID) {
                                    //            item.defaultlotSelect = true;
                                    //            $scope.DefaultRequirementDetails.listRequirementItems.push(item);

                                    //        }
                                    //    })
                                    //})

                                    tempDefaultItems = $scope.DefaultRequirementDetails.listRequirementItems;
                                    
                                }
                            }
                            
                        }
                        $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                    })
            }
            

            auctionsService.getrequirementdata({ "reqid": $stateParams.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
            .then(function (response) {
                if (response) {
                    var items = response.listRequirementItems.filter(function (item) {
                        return (item.isCoreProductCategory === 1);
                    });
                    response.listRequirementItems = items;

                    var vendors = response.auctionVendors.filter(function (v) {
                        return (v.isQuotationRejected === 0);
                    });
                    response.auctionVendors = vendors;
                    $scope.parentRequirementDetails = response;
                    $scope.DefaultRequirementDetails = angular.copy(response);

                   // $scope.groupItems = _.uniqBy($scope.parentRequirementDetails.listRequirementItems, 'productIDorName');
                   // $scope.DefaultRequirementDetails.listRequirementItems = $scope.groupItems;
                    $scope.DefaultRequirementDetails.listRequirementItems.forEach(function (item, itemIndexs) {
                        if (item) {
                            item.defaultlotSelect = true;
                        }
                    });

                    $scope.DefaultRequirementDetails.auctionVendors.forEach(function (vend, vendIndexs) {
                        if (vend) {
                            vend.defaultvendIsSelect = true;
                        }
                    });
                   tempDefaultItems = $scope.DefaultRequirementDetails.listRequirementItems;
                    $scope.getDefaultlotDetails(true);
                }
            });

            $scope.setEndTime = function () {
                $scope.NegotiationSettings = $scope.parentRequirementDetails.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                if ($scope.lotDetails.StartTime != '' || $scope.lotDetails.StartTime != 'undefined') {
                    $scope.lotDetails.EndTime = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.mins, 'minutes'));
                    $scope.lotDetails.EndTime = moment($scope.lotDetails.EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.hours, 'hours'));
                    $scope.lotDetails.EndTime = moment($scope.lotDetails.EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.days, 'days'));
                }
                $scope.DefaultlotDetails.StartTime = $scope.lotDetails.EndTime;

                if ($scope.DefaultlotDetails.StartTime != '' || $scope.DefaultlotDetails.StartTime != 'undefined') {
                    $scope.DefaultlotDetails.EndTime = moment($scope.DefaultlotDetails.StartTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.mins, 'minutes'));
                    $scope.DefaultlotDetails.EndTime = moment($scope.DefaultlotDetails.EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.hours, 'hours'));
                    $scope.DefaultlotDetails.EndTime = moment($scope.DefaultlotDetails.EndTime, "DD-MM-YYYY HH:mm").add(moment.duration($scope.days, 'days'));
                }
            }

            $scope.getLotRequirementItems = function () {
                PRMLotReqService.lotRequirementItems($stateParams.Id)
                    .then(function (response) {
                        if (response) {
                            $scope.lotRequirementItems = response;
                            if (response.length > 0) {
                                $scope.lotDetails.StartTime = userService.toLocalDate(response[response.length - 1].endTime);
                                if ($scope.lotDetails.StartTime != '') {
                                    $scope.minDateMoment = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm");
                                }
                                $scope.setEndTime();

                                $scope.validations.isDisabled = true;
                            } else {
                                $scope.lotDetails.StartTime = $scope.previousDate;
                                if ($scope.lotDetails.StartTime != '') {
                                    $scope.minDateMoment = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm");
                                }
                                $scope.setEndTime();
                            }
                        }
                    });
            };

            $scope.getLotRequirementItems();

            $scope.filteredWorders = [];

            $scope.isAlreadyInLot = function (requirementItem) {
                $scope.filteredWorders = [];
                $scope.lotRequirementItems.forEach(function ( obj, index) {
                    $scope.filteredWorders.push(obj.title);
                })
                var isValid = true;
                if (requirementItem && $scope.filteredWorders && $scope.filteredWorders.length > 0) {
                    if ($scope.filteredWorders.includes(requirementItem.productIDorName)) {
                        isValid = false;
                    }
                }
               
                return isValid;
            };

            $scope.allValidations = false;
            $scope.validations = {
                lotTitleValidation: '',
                currentTimeValidation: '',
                endTimeValidation: '',
                selectedItem: '',
                isDisabled : false

            }

            $scope.createLotRequirement = function () {
                $scope.getDefaultlotDetails(true);
                $scope.allValidations = false;
                $scope.validations.selectedItem = $scope.validations.lotTitleValidation = $scope.validations.currentTimeValidation = $scope.validations.EndTimeValidation = '';

                console.log($scope.parentRequirementDetails.listRequirementItems);
                var selectedItems = _.filter($scope.parentRequirementDetails.listRequirementItems, function (o) {
                   if (o && o.lotSelect) return o;
                });
                //var selectedItems = [];
                
                //$scope.groupItems.forEach(function (o) {
                //    if (o && o.lotSelect) {
                //        $scope.parentRequirementDetails.listRequirementItems.forEach(function (a) {
                //            if (o.productIDorName == a.productIDorName) {
                //                selectedItems.push(a);
                //            }
                //        });
                       
                //    }
                //})
                $scope.itemSelectBeforeError = selectedItems;

                var selectedItemIds = selectedItems.map(function (elem) {
                    return elem && elem.itemID;
                }).join(",");

                var selectedVendors = _.filter($scope.parentRequirementDetails.auctionVendors, function (v) {
                    if (v && v.vendIsSelect) return v;
                });
                
                var selectedVendorIds = selectedVendors.map(function (elem) {
                    return elem && elem.vendorID;
                }).join(",");


                //Default items&vendors Start
                //var defaultItems = [];
                var defaultItems = _.filter($scope.DefaultRequirementDetails.listRequirementItems, function (o) {
                    if (o && o.defaultlotSelect) return o;
                });
                //$scope.DefaultRequirementDetails.listRequirementItems.forEach(function (o) {
                //    if (o && o.defaultlotSelect) {
                //        $scope.parentRequirementDetails.listRequirementItems.forEach(function (a) {
                //            if (o.productIDorName == a.productIDorName) {
                //                defaultItems.push(a);
                //            }
                //        });

                //    }
                //})
                $scope.DefaultitemSelectBeforeError = defaultItems;
                var defaultItemIds = '';
                defaultItemIds = defaultItems.map(function (elem) {
                    return elem && elem.itemID;
                }).join(",");

                var defaultVendors = _.filter($scope.DefaultRequirementDetails.auctionVendors, function (v) {
                    if (v && v.defaultvendIsSelect) return v;
                });
                var defaultVendorIds = '';
                defaultVendorIds = defaultVendors.map(function (elem) {
                    return elem && elem.vendorID;
                }).join(",");

                //Default items&vendors End

                $scope.parentRequirementDetails.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item) {
                        item.lotSelect = false;
                    }
                });

                $scope.parentRequirementDetails.auctionVendors.forEach(function (vend, vendIndexs) {
                    if (vend) {
                        vend.vendIsSelect = false;
                    }
                });


                $scope.DefaultlotDetailsTemp = {
                    LotId: $scope.lotID,
                    CompId: userService.getUserCompanyId(),
                    LotTitle: $scope.DefaultlotDetails.LotTitle,
                    LotDesc: $scope.DefaultlotDetails.LotDesc,
                    StartTime: "/Date(" + userService.toUTCTicks($scope.DefaultlotDetails.StartTime) + "+0530)/",
                    EndTime: "/Date(" + userService.toUTCTicks($scope.DefaultlotDetails.EndTime) + "+0530)/",
                    DefaultItems: defaultItemIds,
                    DefaultVendors: defaultVendorIds,
                    Duration: 0,
                    ReqId: $scope.DefaultlotDetails.ReqId,
                    TotalLots: 0,
                    Status: ''
                };
               // params.defaultItems = defaultItemIds;
               // params.defaultVendors = defaultVendorIds;

                if ($scope.lotDetails.LotTitle == '' || $scope.lotDetails.LotTitle == 'undefined') {
                    $scope.validations.lotTitleValidation = 'Please Enter Lot Title';
                    $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                    //$scope.itemSelectBeforeError.forEach(function (item) {
                    //    $scope.parentRequirementDetails.listRequirementItems.forEach(function (item1, itemIndexs) {
                    //        if (item.itemID == item1.itemID) {
                    //            item1.lotSelect = true;
                    //        }
                    //    });
                    //})
                    //$scope.DefaultitemSelectBeforeError.forEach(function (item) {
                    //    $scope.DefaultRequirementDetails.listRequirementItems.forEach(function (vend, vendIndexs) {
                    //        if (item.itemID == vend.itemID) {
                    //            vend.defaultlotSelect = true;
                    //        } else {
                    //            vend.defaultlotSelect = false;
                    //        }
                    //    });
                    //})
                    $scope.allValidations = true;
                    return false;
                }

                if ($scope.DefaultlotDetails.LotTitle == '' || $scope.DefaultlotDetails.LotTitle == 'undefined') {
                    $scope.validations.lotDefaultTitleValidation = 'Please Enter Lot Title';
                    $scope.allValidations = true;
                    return false;
                }

                if ($scope.lotDetails.StartTime == '') {
                    $scope.validations.currentTimeValidation = 'Please Enter Start Time';
                    $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                    $scope.allValidations = true;
                    return false;
                }

                if ($scope.lotDetails.EndTime == '') {
                    $scope.validations.EndTimeValidation = 'Please Enter End Time';
                    $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                    $scope.allValidations = true;
                    return false;
                }

                var ts = moment($scope.lotDetails.StartTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var quotationFreezTime = new Date(m);

                var ts1 = moment($scope.lotDetails.EndTime, "DD-MM-YYYY HH:mm").valueOf();
                var m1 = moment(ts1);
                var expStartTime = new Date(m1);

                auctionsService.getdate()
                    .then(function (GetDateResponse) {

                        var CurrentDateToLocal = userService.toLocalDate(GetDateResponse);

                        var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                        var m = moment(ts);
                        var deliveryDate = new Date(m);
                        var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                        var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                        var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                        if (quotationFreezTime < CurrentDate) {
                            $scope.validations.currentTimeValidation = 'Start Time should be greater than Current Time';
                            $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                            $scope.allValidations = true;
                            return false;
                        }
                        else if (quotationFreezTime >= expStartTime) {
                            $scope.validations.EndTimeValidation = 'End Time should be greater than Start Time';
                            $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                            $scope.allValidations = true;
                            return false;
                        }

                        if (selectedItemIds == '') {
                            $scope.validations.selectedItem = 'Please Select Atleast One Item';
                            $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                            $scope.allValidations = true;
                            return false;
                        }

                        if (selectedVendorIds == '') {
                            $scope.validations.selectedItem = 'Please Select Vendors';
                            $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                            $scope.allValidations = true;
                            return false;
                        }
                        

                        if ($scope.allValidations) {
                            $scope.DefaultRequirementDetails.listRequirementItems = tempDefaultItems;
                            return false;
                        }



                        var params = {};
                        params.itemids = selectedItemIds;
                        params.vendorids = selectedVendorIds;
                        params.lotid = $stateParams.Id;
                        params.title = $scope.lotDetails.LotTitle;
                        params.desc = $scope.lotDetails.LotDesc;
                        params.start = "/Date(" + userService.toUTCTicks($scope.lotDetails.StartTime) + "+0530)/";
                        params.end = "/Date(" + userService.toUTCTicks($scope.lotDetails.EndTime) + "+0530)/";
                        params.sessionid = userService.getUserToken();
                        params.user = userService.getUserId();
                        params.defaultDetails = $scope.DefaultlotDetailsTemp;

                        PRMLotReqService.SaveLotRequirement(params)
                            .then(function (response) {
                                if (response.errorMessage !== '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    $scope.previousDate = $scope.lotDetails.EndTime;
                                    $scope.validations.isDisabled = true;
                                    growlService.growl("Details saved successfully.", "success");
                                }

                                $scope.lotDetails = {
                                    LotTitle: '',
                                    LotDesc: '',
                                };
                                $scope.getDefaultlotDetails(true);
                                $scope.getLotRequirementItems();
                                $log.info(response);
                            });
                    });
            };

            $scope.removeFromDefault = function (item) {
                if (item && item.lotSelect && $scope.DefaultRequirementDetails.listRequirementItems.length > 0) {
                    let defaultItemsArray = $scope.DefaultRequirementDetails.listRequirementItems;
                    var items = _.filter(defaultItemsArray, function (o) {
                        return o.itemID != item.itemID;
                    });
                    $scope.DefaultRequirementDetails.listRequirementItems = items;
                } else {
                    item.defaultlotSelect = true;
                    $scope.DefaultRequirementDetails.listRequirementItems.push(item);
                }
                $scope.setEndTime();
            };

            $scope.retriveVendors = function (reqitem) {
                if (reqitem && reqitem.lotSelect && $scope.parentRequirementDetails.listRequirementItems.length > 0) {
                    $scope.parentRequirementDetails.auctionVendors.forEach(function (vend, vendIdx) {
                        if (vend.quotationUrl != '') {
                            vend.listRequirementItems.forEach(function (item, itemIdx) {
                                if (item.itemID == reqitem.itemID && !item.isRegret) {
                                    $scope.RequirementVendors.push(vend);
                                }

                            })
                        }

                    })
                    $scope.setEndTime();
                } else {
                    $scope.parentRequirementDetails.auctionVendors.forEach(function (vend, vendIdx) {
                        if (vend.quotationUrl != '') {
                            vend.listRequirementItems.forEach(function (item, itemIdx) {
                                if (item.itemID == reqitem.itemID) {
                                  //  $scope.RequirementVendors.push(vend);
                                    _.remove($scope.RequirementVendors, vend.vendorID);
                                }

                            })
                        }

                    })
                    $scope.setEndTime();
                    //$scope.auctionItemVendor.listRequirementItems.splice(index, 1, obj);
                }
            };



        }]);