﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class QCSAudit : Entity
    {
        
        [DataMember] [DataNames("AUDIT_ID")] public int AUDIT_ID { get; set; }
        [DataMember] [DataNames("TABLE_NAME")] public string TABLE_NAME { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("QCS_CODE")] public int QCS_CODE { get; set; }
       
        [DataMember] [DataNames("CREATED_DATE")] public DateTime CREATED_DATE { get; set; }
        [DataMember] [DataNames("MODIFIED_DATE")] public DateTime MODIFIED_DATE { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("REQ_JSON")] public string REQ_JSON { get; set; }
        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("MODIFIED_BY")] public int MODIFIED_BY { get; set; }
        [DataMember] [DataNames("AUDIT_VERSION")] public int AUDIT_VERSION { get; set; }
        [DataMember] [DataNames("AUDIT_COMMENTS")] public string AUDIT_COMMENTS { get; set; }
        [DataMember] [DataNames("APPROVER_NAME")] public string APPROVER_NAME { get; set; }

    }
}