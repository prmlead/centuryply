﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ReportObj : Entity
    {

        [DataMember(Name = "auctionVendors")]
        public List<AuctionVendors> AuctionVendors { get; set; }

    }

    [DataContract]
    public class AuctionVendors : Entity
    {

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "vendorName")]
        public string VendorName { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "listItems")]
        public List<ListItems> ListItems { get; set; }

    }

    [DataContract]
    public class ListItems : Entity
    {

        [DataMember(Name = "catalogueItemID")]
        public int CatalogueItemID { get; set; }

        [DataMember(Name = "hsnCode")]
        public string HsnCode { get; set; }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        [DataMember(Name = "productQuantity")]
        public int ProductQuantity { get; set; }

        [DataMember(Name = "cGst")]
        public double CGst { get; set; }

        [DataMember(Name = "sGst")]
        public double SGst { get; set; }

        [DataMember(Name = "iGst")]
        public double IGst { get; set; }

        [DataMember(Name = "unitPrice")]
        public double UnitPrice { get; set; }

        [DataMember(Name = "revUnitPrice")]
        public double RevUnitPrice { get; set; }

        [DataMember(Name = "itemPrice")]
        public double ItemPrice { get; set; }

        [DataMember(Name = "revitemPrice")]
        public double RevitemPrice { get; set; }

    }
}