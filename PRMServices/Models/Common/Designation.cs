﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Designation : ResponseAudit
    {        
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "desigID")]
        public int DesigID { get; set; }

        string _desigCode = string.Empty;
        [DataMember(Name = "desigCode")]
        public string DesigCode
        {
            get
            {
                return _desigCode;
            }
            set
            {
                _desigCode = value;
            }
        }

        string _desigDesc = string.Empty;
        [DataMember(Name = "desigDesc")]
        public string DesigDesc {
            get
            {
                return _desigDesc;
            }
            set
            {
                _desigDesc = value;
            }
        }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "isAssignedToUser")]
        public bool IsAssignedToUser { get; set; }

        [DataMember(Name = "desigTypeID")]
        public int DesigTypeID { get; set; }
    }
}