﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementUsersByRank : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "uID")]
        public int UID { get; set; }

        

       

        


        //string l1Name = string.Empty;
        //[DataMember(Name = "L1Name")]
        //public string L1Name
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(l1Name))
        //        { return l1Name; }
        //        else
        //        { return ""; }
        //    }
        //    set
        //    {
        //        if (!string.IsNullOrEmpty(value))
        //        { l1Name = value; }
        //    }
        //}

        //string l2Name = string.Empty;
        //[DataMember(Name = "L2Name")]
        //public string L2Name
        //{
        //    get
        //    {
        //        if (!string.IsNullOrEmpty(l2Name))
        //        { return l2Name; }
        //        else
        //        { return ""; }
        //    }
        //    set
        //    {
        //        if (!string.IsNullOrEmpty(value))
        //        { l2Name = value; }
        //    }
        //}


        [DataMember(Name = "revBasePrice")]
        public double RevBasePrice { get; set; }


        [DataMember(Name = "initBasePrice")]
        public double InitBasePrice { get; set; }

        [DataMember(Name = "l1InitialBasePrice")]
        public double L1InitialBasePrice { get; set; }

        [DataMember(Name = "revVendTotalPrice")]
        public double RevVendTotalPrice { get; set; }

        [DataMember(Name = "vendTotalPrice")]
        public double VendTotalPrice { get; set; }
       


        [DataMember(Name = "rank")]
        public int Rank { get; set; }

        [DataMember(Name = "quantity")]
        public double Quantity { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "selectedVendorCurrency")]
        public string SelectedVendorCurrency { get; set; }

        [DataMember(Name = "PLANT_ADDRESS")]
        public string PLANT_ADDRESS { get; set; }
        
    }
}


//REQ_ID, U_ID, REQ_TITLE, REQ_POSTED_ON, START_TIME, END_TIME, 
//NEGOTIATION_DURATION,
//SAVINGS,
//REQ_IS_TABULAR, IS_UNIT_PRICE_BIDDING, 
//NO_OF_VENDORS_INVITED, NO_OF_VENDORS_PARTICIPATED