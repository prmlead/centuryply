﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models.Catalog;

namespace PRMServices.Models
{
    [DataContract]
    public class RequirementItemsLite : Entity
    {
        [DataNames("ITEM_ID")]
        [DataMember(Name = "itemID")]
        public int ITEM_ID { get; set; }

        [DataNames("PRICE")]
        [DataMember(Name = "itemPrice")]
        public decimal ItemPrice { get; set; }

        [DataNames("REVICED_PRICE")]
        [DataMember(Name = "revitemPrice")]
        public decimal RevItemPrice { get; set; }

        [DataNames("UNIT_PRICE")]
        [DataMember(Name = "unitPrice")]
        public decimal UnitPrice { get; set; }

        [DataNames("REV_UNIT_PRICE")]
        [DataMember(Name = "revUnitPrice")]
        public decimal RevUnitPrice { get; set; }

        [DataNames("C_GST")]
        [DataMember(Name = "cGst")]
        public decimal CGst { get; set; }

        [DataNames("S_GST")]
        [DataMember(Name = "sGst")]
        public decimal SGst { get; set; }

        [DataNames("I_GST")]
        [DataMember(Name = "iGst")]
        public decimal IGst { get; set; }

        [DataMember(Name = "Gst")]
        public decimal Gst
        {
            get
            {
                return IGst + SGst + CGst;
            }
        }

        [DataNames("UNIT_MRP")]
        [DataMember(Name = "unitMRP")]
        public decimal UnitMRP { get; set; }

        [DataNames("UNIT_DISCOUNT")]
        [DataMember(Name = "unitDiscount")]
        public decimal UnitDiscount { get; set; }

        [DataNames("REV_UNIT_DISCOUNT")]
        [DataMember(Name = "revUnitDiscount")]
        public decimal RevUnitDiscount { get; set; }

        [DataNames("ITEM_MIN_REDUCTION")]
        [DataMember(Name = "itemMinReduction")]
        public decimal ItemMinReduction { get; set; }

        [DataNames("LAST_ITEM_PRICE")]
        [DataMember(Name = "itemLastPrice")]
        public decimal ItemLastPrice { get; set; }

        [DataNames("ITEM_RANK")]
        [DataMember(Name = "itemRank")]
        public int ItemRank { get; set; }

        [DataNames("REVICED_PRICE_CB")]
        [DataMember(Name = "revitemPriceCB")]
        public decimal RevItemPriceCB { get; set; }

        [DataNames("REV_UNIT_PRICE_CB")]
        [DataMember(Name = "revUnitPriceCB")]
        public decimal RevUnitPriceCB { get; set; }

        [DataNames("REV_UNIT_DISCOUNT_CB")]
        [DataMember(Name = "revUnitDiscountCB")]
        public decimal RevUnitDiscountCB { get; set; }

        [DataNames("ITEM_LEVEL_LEAST_PRICE")]
        [DataMember(Name = "ITEM_LEVEL_LEAST_PRICE")]
        public decimal ITEM_LEVEL_LEAST_PRICE { get; set; }

        [DataNames("LAST_BID_REV_UNIT_PRICE")]
        [DataMember(Name = "LAST_BID_REV_UNIT_PRICE")]
        public decimal LAST_BID_REV_UNIT_PRICE { get; set; }

        [DataMember(Name = "LAST_BID_REDUCTION_PRICE")]
        public decimal LAST_BID_REDUCTION_PRICE
        {
            get
            {
                return LAST_BID_REV_UNIT_PRICE - RevUnitPrice;
            }
        }

        [DataNames("CATALOGUE_ITEM_ID")]
        [DataMember(Name = "catalogueItemID")]
        public int CatalogueItemID { get; set; }

        string productQuotationTemplateJson = string.Empty;
        [DataNames("ProductQuotationTemplateJson")]
        [DataMember(Name = "productQuotationTemplateJson")]
        public string ProductQuotationTemplateJson
        {
            get
            {
                if (!string.IsNullOrEmpty(productQuotationTemplateJson))
                { return productQuotationTemplateJson; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productQuotationTemplateJson = value; }
            }
        }

        [DataMember(Name = "productQuotationTemplateArray")]
        public List<ProductQuotationTemplate> ProductQuotationTemplateArray { get; set; }

        [DataNames("IS_CORE_CAT")]
        [DataMember(Name = "isCoreProductCategory")]
        public int IsCoreProductCategory { get; set; } = 1;

        //Below is temporaryVariable
        [DataNames("U_ID")]
        public int U_ID { get; set; }
    }
}