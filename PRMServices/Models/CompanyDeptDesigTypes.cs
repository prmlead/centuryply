﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Models;

namespace PRMServices.Models
{
    [DataContract]
    public class CompanyDeptDesigTypes : Entity
    {
        [DataMember(Name = "typeID")]
        public int TypeID { get; set; }

        [DataMember(Name = "typeName")]
        public string TypeName { get; set; }

    }
}