﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class RFQCreators : ResponseAudit
    {
        [DataMember(Name = "firstName")]
        public int NoOfReqClosed { get; set; }

        [DataMember(Name = "lastName")]
        public int LastName { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "isIndentAssigned")]
        public int IsIndentAssigned { get; set; }

        [DataMember(Name = "noOfNonCapexPending")]
        public int NoOfNonCapexPending { get; set; }

    }
}