﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class BidObject : Entity
    {
        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        //[DataMember(Name = "quotation")]
        //public byte[] Quotation { get; set; }

        [DataMember(Name = "quotationName")]
        public string QuotationName { get; set; }

        [DataMember(Name = "tax")]
        public int Tax { get; set; }

        [DataMember(Name = "quotation")]
        public byte[] Quotation { get; set; }

        [DataMember(Name = "freightcharges")]
        public double Freightcharges { get; set; }

        [DataMember(Name = "warranty")]
        public string Warranty { get; set; }

        [DataMember(Name = "payment")]
        public string Payment { get; set; }

        [DataMember(Name = "duration")]
        public string Duration { get; set; }

        [DataMember(Name = "validity")]
        public string Validity { get; set; }

        [DataMember(Name = "quotationObject")]
        public List<RequirementItems> QuotationObject { get; set; }

        [DataMember(Name = "fwdQuotationObject")]
        public List<FwdRequirementItems> FwdQuotationObject { get; set; }

        [DataMember(Name = "revised")]
        public int Revised { get; set; }

        [DataMember(Name = "priceWithoutTax")]
        public double PriceWithoutTax { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "discount")]
        public double Discount { get; set; }

        [DataMember(Name = "listRequirementTaxes")]
        public List<RequirementTaxes> ListRequirementTaxes { get; set; }

        [DataMember(Name = "otherProperties")]
        public string OtherProperties { get; set; }

        [DataMember(Name = "ignorevalidations")]
        public bool Ignorevalidations { get; set; }

        [DataMember(Name = "vendorBidPrice")]
        public double VendorBidPrice { get; set; }

        [DataMember(Name = "itemrevtotalprice")]
        public double Itemrevtotalprice { get; set; }

        [DataMember(Name = "basePrice")]
        public double BasePrice { get; set; }
    }

    [DataContract]
    public class BidObjectLite
    {
        [DataMember] [DataNames("reqID")] public int ReqID { get; set; }
        [DataMember] [DataNames("userID")] public int UserID { get; set; }
        [DataMember] [DataNames("custCompID")] public int CustCompID { get; set; }
        [DataMember] [DataNames("totalprice")] public double TotalPrice { get; set; }
        [DataMember] [DataNames("basePrice")] public double BasePrice { get; set; }
        [DataMember] [DataNames("quotationObject")] public BidItemObjectLite[] QuotationObject { get; set; }
        [DataMember] [DataNames("ignorevalidations")] public bool Ignorevalidations { get; set; }
        [DataMember] [DataNames("sessionID")] public string SessionID { get; set; }

    }

    [DataContract]
    public class BidItemObjectLite
    {
        [DataMember] [DataNames("itemID")] public int ItemID { get; set; }
        [DataMember] [DataNames("requirementID")] public int RequirementID { get; set; }
        [DataMember] [DataNames("vendorID")] public int VendorID { get; set; }
        [DataMember] [DataNames("itemRank")] public int ItemRank { get; set; }
        [DataMember] [DataNames("vendorRank")] public int VendorRank { get; set; }
        [DataMember] [DataNames("revitemPrice")] public double RevItemPrice { get; set; }
        [DataMember] [DataNames("revUnitPrice")] public double RevUnitPrice { get; set; }
    }
}