﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class AgingAnalysis : Entity
    {
         [DataMember(Name = "dateOfInstallation")]
        public int DateOfInstallation { get; set; }

        [DataMember(Name = "dateOfDeinstallation")]
        public int DateOfDeinstallation { get; set; }

        [DataMember(Name = "noOfYearsServed")]
        public string NoOfYearsServed { get; set; }
        
    }
}