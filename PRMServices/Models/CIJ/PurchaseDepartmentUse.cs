﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace PRMServices.Models
{
    [DataContract]
    public class PurchaseDepartmentUse : Entity
    {
        [DataMember(Name = "poNo")]
        public int PoNo { get; set; }

        [DataMember(Name = "poDate")]
        public int PoDate { get; set; }

        [DataMember(Name = "supplierName")]
        public int SupplierName { get; set; }

        [DataMember(Name = "otherInfo")]
        public int OtherInfo { get; set; }

        [DataMember(Name = "signature")]
        public int Signature { get; set; }
    }
}