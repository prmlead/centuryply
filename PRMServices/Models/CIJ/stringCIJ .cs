﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class stringCIJ : Entity
    {
        [DataMember(Name = "cijID")]
        public int CijID { get; set; }

        [DataMember(Name = "cijCode")]
        public string CijCode { get; set; }

        [DataMember(Name = "cij")]
        public string Cij { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "cijType")]
        public string CijType { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "status")]
        public string Status { get; set; }

        [DataMember(Name = "requestedBy")]
        public string RequestedBy { get; set; }

        [DataMember(Name = "createdBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "isValid")]
        public int IsValid { get; set; }

        [DataMember(Name = "deptID")]
        public int DeptID { get; set; }

        [DataMember(Name = "assetType")]
        public int AssetType { get; set; }

        //[DataMember(Name = "userPriority")]
        //public string UserPriority { get; set; }

        //[DataMember(Name = "userStatus")]
        //public string UserStatus{ get; set; }

        [DataMember(Name = "currentApproverName")]
        public string CurrentApproverName { get; set; }

        string userPriority = string.Empty;
        [DataMember(Name = "userPriority")]
        public string UserPriority
        {
            get
            {
                if (!string.IsNullOrEmpty(userPriority))
                { return userPriority; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { userPriority = value; }
            }
        }

        string userStatus = string.Empty;
        [DataMember(Name = "userStatus")]
        public string UserStatus
        {
            get
            {
                if (!string.IsNullOrEmpty(userStatus))
                { return userStatus; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { userStatus = value; }
            }
        }

        string expectedDelivery = string.Empty;
        [DataMember(Name = "expectedDelivery")]
        public string ExpectedDelivery
        {
            get
            {
                if (!string.IsNullOrEmpty(expectedDelivery))
                { return expectedDelivery; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { expectedDelivery = value; }
            }
        }


    }
}