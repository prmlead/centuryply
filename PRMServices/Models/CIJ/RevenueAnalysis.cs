﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class RevenueAnalysis : Entity
    {
        [DataMember(Name = "noOfProceduresDone")]
        public int NoOfProceduresDone { get; set; }

        [DataMember(Name = "incomeFormProcedures")]
        public int IncomeFormProcedures { get; set; }

        [DataMember(Name = "reqByName")]
        public string reqByName { get; set; }

        [DataMember(Name = "recmedByName")]
        public string RecmedByName { get; set; }

        [DataMember(Name = "reqBYSign")]
        public string ReqBYSign { get; set; }

        [DataMember(Name = "recmedBYSign")]
        public string RecmedBYSign { get; set; }

        [DataMember(Name = "reqByDate")]
        public string ReqByDate { get; set; }

        [DataMember(Name = "recmedByDate")]
        public string RecmedByDate { get; set; }

    }
}