﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class CBItemPrices : Entity
    {
        
        [DataMember] [DataNames("CB_AIH_ID")] public int CB_AIH_ID { get; set; }
        [DataMember] [DataNames("CB_AH_ID")] public int CB_AH_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("V_ID")] public int V_ID { get; set; }
        [DataMember] [DataNames("ITEM_ID")] public int ITEM_ID { get; set; }

        [DataMember] [DataNames("REV_UNIT_PRICE")] public decimal REV_UNIT_PRICE { get; set; }
        [DataMember] [DataNames("REV_FREIGHT_CHARGES")] public decimal REV_FREIGHT_CHARGES { get; set; }
        [DataMember] [DataNames("REV_ITEM_PRICE")] public decimal REV_ITEM_PRICE { get; set; }

        [DataMember] [DataNames("CREATED_BY")] public int CREATED_BY { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }

        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }

    }
}