﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Currencies
    {
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "vendorCurrency")]
        public string VendorCurrency { get; set; }


        string currency = "INR";
        [DataMember(Name = "currency")]
        public string Currency
        {
            get
            {
                if (!string.IsNullOrEmpty(currency))
                {
                    return currency;
                }
                else
                {
                    return "INR";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    currency = value;
                }
            }
        }



    }
}