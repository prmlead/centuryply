﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class Report : Entity
    {
        [DataMember(Name = "requirementList")]
        public List<Requirement> RequirementList { get; set; }

        [DataMember(Name = "dashboardStats")]
        public DashboardStats DashboardStats { get; set; }
    }
}