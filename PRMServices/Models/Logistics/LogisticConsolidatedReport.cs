﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class LogisticConsolidatedReport : Entity
    {
        

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        //[DataMember(Name = "uID")]
        //public int UID { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "reqPostedOn")]
        public DateTime? ReqPostedOn { get; set; }

        [DataMember(Name = "invoiceNumber")]
        public string InvoiceNumber { get; set; }

        [DataMember(Name = "invoiceDate")]
        public DateTime? InvoiceDate { get; set; }

        [DataMember(Name = "consigneeName")]
        public string ConsigneeName { get; set; }

        [DataMember(Name = "destination")]
        public string Destination { get; set; }


        [DataMember(Name = "productName")]
        public string ProductName { get; set; }


        [DataMember(Name = "shippingMode")]
        public string ShippingMode { get; set; }


        [DataMember(Name = "qty")]
        public int QTY { get; set; }

        [DataMember(Name = "chargeableWt")]
        public double ChargeableWt { get; set; }

        [DataMember(Name = "dgFee")]
        public double DGFee { get; set; }


        [DataMember(Name = "ams")]
        public string AMS { get; set; }

        [DataMember(Name = "misc")]
        public string MISC { get; set; }

        [DataMember(Name = "menzinesCharges")]
        public double MenzinesCharges { get; set; }

        [DataMember(Name = "chaCharges")]
        public double ChaCharges { get; set; }

        [DataMember(Name = "freightBidAmount")]
        public double FreightBidAmount { get; set; }

        [DataMember(Name = "grandTotal")]
        public double GrandTotal { get; set; }

        [DataMember(Name = "portOfLanding")]
        public string PortOfLanding { get; set; }

        [DataMember(Name = "isPalletize")]
        public int IsPalletize { get; set; }

        [DataMember(Name = "pallet")]
        public string Pallet { get; set; }

        [DataMember(Name = "netWeight")]
        public double NetWeight { get; set; }

        [DataMember(Name = "serviceCharges")]
        public decimal ServiceCharges { get; set; }

        [DataMember(Name = "customClearance")]
        public decimal CustomClearance { get; set; }

        [DataMember(Name = "terminalHandling")]
        public decimal TerminalHandling { get; set; }

        [DataMember(Name = "drawbackAmount")]
        public double DrawbackAmount { get; set; }

        [DataMember(Name = "warehouseStorages")]
        public double WarehouseStorages { get; set; }

        [DataMember(Name = "loadingUnloadingCharges")]
        public double LoadingUnloadingCharges { get; set; }

        [DataMember(Name = "additionalMiscExp")]
        public double AdditionalMiscExp { get; set; }

        [DataMember(Name = "narcoticSubcharges")]
        public double NarcoticSubcharges { get; set; }

        [DataMember(Name = "PalletizeNumber")]
        public double palletizeNumber { get; set; }

        [DataMember(Name = "PalletizeQty")]
        public double palletizeQty { get; set; }


    }
}


//REQ_ID, U_ID, REQ_TITLE, REQ_POSTED_ON, START_TIME, END_TIME, 
//NEGOTIATION_DURATION,
//SAVINGS,
//REQ_IS_TABULAR, IS_UNIT_PRICE_BIDDING, 
//NO_OF_VENDORS_INVITED, NO_OF_VENDORS_PARTICIPATED