﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Models
{
    public class ViewModel
    {
        public Requirement Requirement { get; set; }

        public UserInfo UserInfo { get; set; }
    }
}