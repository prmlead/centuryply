﻿using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCInput : Entity
    {
        [DataMember(Name = "apmcID")]
        public int APMCID { get; set; }

        [DataMember(Name = "apmcNegotiationID")]
        public int APMCNegotiationID { get; set; }

        [DataMember(Name = "inputPrice")]
        public decimal InputPrice { get; set; }

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "comment")]
        public string Comment { get; set; }

        [DataMember(Name = "company")]
        public Company Company { get; internal set; }

        [DataMember(Name = "isComplete")]
        public int IsComplete { get; internal set; }
    }
}