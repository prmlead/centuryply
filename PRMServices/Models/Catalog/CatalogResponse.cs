﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class CatalogResponse
    {

        [DataMember(Name = "pcId")]
        public int PcId { get; set; }

        [DataMember(Name = "repsonseId")]
        public int ResponseId { get; set; }

        string eMessage = string.Empty;
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this.eMessage;
            }
            set
            {
                this.eMessage = value;
            }
        }

        string sessionId = string.Empty;
        [DataMember(Name = "sessionID")]
        public string SessionID
        {
            get
            {
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
            }
        }

        [DataMember(Name = "objectId")]
        public int ObjectId { get; set; }
    }
}