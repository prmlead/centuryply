﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

using PRMServices.Models;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ERPPRStage : ResponseAudit
    {
        
        [DataNames("PLANT")]
        [DataMember(Name = "PLANT")]
        public string PLANT { get; set; }

        [DataNames("PLANT_CODE")]
        [DataMember(Name = "PLANT_CODE")]
        public string PLANT_CODE { get; set; }

        [DataNames("PURCHASE_GROUP_CODE")]
        [DataMember(Name = "PURCHASE_GROUP_CODE")]
        public string PURCHASE_GROUP_CODE { get; set; }

        [DataNames("PURCHASE_GROUP_NAME")]
        [DataMember(Name = "PURCHASE_GROUP_NAME")]
        public string PURCHASE_GROUP_NAME { get; set; }

        [DataNames("REQUESITION_DATE")]
        [DataMember(Name = "REQUESITION_DATE")]
        public string REQUESITION_DATE { get; set; }

        [DataNames("PR_RELEASE_DATE")]
        [DataMember(Name = "PR_RELEASE_DATE")]
        public string PR_RELEASE_DATE { get; set; }

        [DataNames("PR_CHANGE_DATE")]
        [DataMember(Name = "PR_CHANGE_DATE")]
        public string PR_CHANGE_DATE { get; set; }

        [DataNames("PR_NUMBER")]
        [DataMember(Name = "PR_NUMBER")]
        public string PR_NUMBER { get; set; }

        [DataNames("REQUISITIONER_NAME")]
        [DataMember(Name = "REQUISITIONER_NAME")]
        public string REQUISITIONER_NAME { get; set; }
        
        [DataNames("REQUISITIONER_EMAIL")]
        [DataMember(Name = "REQUISITIONER_EMAIL")]
        public string REQUISITIONER_EMAIL { get; set; }

        [DataNames("REQUISITION_ITEM")]
        [DataMember(Name = "REQUISITION_ITEM")]
        public string REQUISITION_ITEM { get; set; }

        [DataNames("MATERIAL_GROUP_CODE")]
        [DataMember(Name = "MATERIAL_GROUP_CODE")]
        public string MATERIAL_GROUP_CODE { get; set; }

        [DataNames("MATERIAL_GROUP_DESC")]
        [DataMember(Name = "MATERIAL_GROUP_DESC")]
        public string MATERIAL_GROUP_DESC { get; set; }

        [DataNames("MATERIAL_CODE")]
        [DataMember(Name = "MATERIAL_CODE")]
        public string MATERIAL_CODE { get; set; }

        [DataNames("MATERIAL_TYPE")]
        [DataMember(Name = "MATERIAL_TYPE")]
        public string MATERIAL_TYPE { get; set; }

        [DataNames("MATERIAL_HSN_CODE")]
        [DataMember(Name = "MATERIAL_HSN_CODE")]
        public string MATERIAL_HSN_CODE { get; set; }

        [DataNames("MATERIAL_DESCRIPTION")]
        [DataMember(Name = "MATERIAL_DESCRIPTION")]
        public string MATERIAL_DESCRIPTION { get; set; }

        [DataNames("SHORT_TEXT")]
        [DataMember(Name = "SHORT_TEXT")]
        public string SHORT_TEXT { get; set; }

        [DataNames("ITEM_TEXT")]
        [DataMember(Name = "ITEM_TEXT")]
        public string ITEM_TEXT { get; set; }

        [DataNames("UOM")]
        [DataMember(Name = "UOM")]
        public string UOM { get; set; }

        [DataNames("QTY_REQUIRED")]
        [DataMember(Name = "QTY_REQUIRED")]
        public string QTY_REQUIRED { get; set; }

        [DataNames("MATERIAL_DELIVERY_DATE")]
        [DataMember(Name = "MATERIAL_DELIVERY_DATE")]
        public string MATERIAL_DELIVERY_DATE { get; set; }

        [DataNames("LEAD_TIME")]
        [DataMember(Name = "LEAD_TIME")]
        public string LEAD_TIME { get; set; }

        [DataNames("PR_TYPE")]
        [DataMember(Name = "PR_TYPE")]
        public string PR_TYPE { get; set; }

        [DataNames("PR_TYPE_DESC")]
        [DataMember(Name = "PR_TYPE_DESC")]
        public string PR_TYPE_DESC { get; set; }

        [DataNames("FILE_NAME")]
        [DataMember(Name = "FILE_NAME")]
        public string FILE_NAME { get; set; }
    }
}