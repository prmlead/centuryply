﻿using Microsoft.AspNet.SignalR;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SignalR;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web;
using CATALOG = PRMServices.Models.Catalog;



namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMSAPIntegrationService : IPRMSAPIntegrationService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        private PRMServices prmServices = new PRMServices();

        public Response SaveERPPRStageData(ERPPRStage[] data)
        {
            Response response = new Response();
            logger.Debug("Service started:" + data.Length);
            try
            {
                int count = 0;
                int readingCount = 1;
                if (data != null && data.Length > 0)
                {
                    logger.Debug("Total records came:" + data.Length);
                    string failedRecords = "";
                    string queryValues = string.Empty;
                    string query = @"INSERT INTO erp_pr_stage (PLANT, PLANT_CODE, PURCHASE_GROUP_CODE, PURCHASE_GROUP_NAME, REQUESITION_DATE, 
                                    PR_RELEASE_DATE, PR_CHANGE_DATE, PR_NUMBER, REQUISITIONER_NAME, REQUISITIONER_EMAIL, REQUESITION_ITEM, 
                                    MATERIAL_GROUP_CODE, MATERIAL_GROUP_DESC, MATERIAL_CODE, MATERIAL_TYPE, MATERIAL_HSN_CODE, MATERIAL_DESCRIPTION, SHORT_TEXT, ITEM_TEXT, UOM, 
                                    QTY_REQUIRED, MATERIAL_DELIVERY_DATE, LEAD_TIME, PR_TYPE, PR_TYPE_DESC, FILE_NAME, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY)  Values";
                    string queryAudit = @"INSERT INTO erp_pr_stage_audit (PLANT, PLANT_CODE, PURCHASE_GROUP_CODE, PURCHASE_GROUP_NAME, REQUESITION_DATE, 
                                    PR_RELEASE_DATE, PR_CHANGE_DATE, PR_NUMBER, REQUISITIONER_NAME, REQUISITIONER_EMAIL, REQUESITION_ITEM, 
                                    MATERIAL_GROUP_CODE, MATERIAL_GROUP_DESC, MATERIAL_CODE, MATERIAL_TYPE, MATERIAL_HSN_CODE, MATERIAL_DESCRIPTION, SHORT_TEXT, ITEM_TEXT, UOM, 
                                    QTY_REQUIRED, MATERIAL_DELIVERY_DATE, LEAD_TIME, PR_TYPE, PR_TYPE_DESC, FILE_NAME, DATE_CREATED, DATE_MODIFIED, CREATED_BY, MODIFIED_BY)  Values";
                    foreach (var info in data)
                    {
                        try
                        {
                            DateTime REQUESITION_DATE;
                            DateTime.TryParse(trimValues(info.REQUESITION_DATE), out REQUESITION_DATE);

                            DateTime PR_RELEASED_DATE;
                            DateTime.TryParse(trimValues(info.PR_RELEASE_DATE),out PR_RELEASED_DATE);
                            DateTime PR_CHANGE_DATE;
                            DateTime.TryParse(trimValues(info.PR_CHANGE_DATE),out PR_CHANGE_DATE);
                            DateTime MATERIAL_DELIVERY_DATE;
                            DateTime.TryParse(trimValues(info.MATERIAL_DELIVERY_DATE),out MATERIAL_DELIVERY_DATE);
                            string columns = "(~$^{0}~$^,~$^{1}~$^,~$^{2}~$^,~$^{3}~$^,~$^{4}~$^," +
                                             "~$^{5}~$^,~$^{6}~$^,~$^{7}~$^,~$^{8}~$^,~$^{9}~$^," +
                                             "~$^{10}~$^,~$^{11}~$^,~$^{12}~$^,~$^{13}~$^,~$^{14}~$^," +
                                             "~$^{15}~$^,~$^{16}~$^,~$^{17}~$^,~$^{18}~$^,~$^{19}~$^," +
                                             "~$^{20}~$^,~$^{21}~$^,~$^{22}~$^,~$^{23}~$^,~$^{24}~$^, " +
                                             "~$^{25}~$^,utc_timestamp(), utc_timestamp(), 0, 0),";
                            queryValues = queryValues + 
                                string.Format(columns, trimValues(info.PLANT), trimValues(info.PLANT_CODE), trimValues(info.PURCHASE_GROUP_CODE), trimValues(info.PURCHASE_GROUP_NAME),REQUESITION_DATE.ToString("yyyy-MM-dd HH:mm:ss"),
                                PR_RELEASED_DATE.ToString("yyyy-MM-dd HH:mm:ss"),PR_CHANGE_DATE.ToString("yyyy-MM-dd HH:mm:ss"),trimValues(info.PR_NUMBER), trimValues(info.REQUISITIONER_NAME), trimValues(info.REQUISITIONER_EMAIL), trimValues(info.REQUISITION_ITEM), 
                                trimValues(info.MATERIAL_GROUP_CODE), trimValues(info.MATERIAL_GROUP_DESC),trimValues(info.MATERIAL_CODE), trimValues(info.MATERIAL_TYPE), trimValues(info.MATERIAL_HSN_CODE), trimValues(info.MATERIAL_DESCRIPTION),trimValues(info.SHORT_TEXT), trimValues(info.ITEM_TEXT),trimValues(info.UOM), 
                                info.QTY_REQUIRED.Replace(",",""),MATERIAL_DELIVERY_DATE.ToString("yyyy-MM-dd HH:mm:ss"),trimValues(info.LEAD_TIME), trimValues(info.PR_TYPE), trimValues(info.PR_TYPE_DESC), trimValues(info.FILE_NAME));

                            logger.Debug("Record Number:" + readingCount);
                            count++;
                            readingCount++;
                        }
                        catch (Exception ex)
                        {
                            failedRecords = failedRecords + info.PR_NUMBER + ";";
                            logger.Error("Error occured YPRMPR:" + ex.Message);
                        }
                    }

                    if (!string.IsNullOrEmpty(queryValues))
                    {
                        queryValues = queryValues.Replace(@"'", "\\'");
                        queryValues = queryValues.Replace("~$^", "'");
                        queryValues = queryValues.Substring(0, queryValues.Length - 1);
                        queryValues = queryValues + ";";
                        query = query + queryValues;
                        queryAudit = queryAudit + queryValues;
                        sqlHelper.ExecuteQuery("delete from erp_pr_stage");
                        var dataset = sqlHelper.ExecuteQuery(query);
                        sqlHelper.ExecuteQuery(queryAudit);
                    }

                    var dattable = sqlHelper.SelectQuery("SELECT COUNT(PR_NUMBER) AS TOTAL_RECORDS FROM ERP_PR_STAGE");
                    int totalRecords = 0;
                    if (dattable != null && dattable.Rows.Count > 0)
                    {
                        totalRecords = Convert.ToInt32(dattable.Rows[0]["TOTAL_RECORDS"]);
                    }

                    if (count > 0)
                    {
                        var sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_DATE", DateTime.Now);
                        ProcessPostIntegration("erp_ProcessPR", sd);
                    }
                    response.Message = "Records recieved:" + data.Length + ", processed:" + count + ", Current Total Records:" + totalRecords + ", Failed Records:" + failedRecords;
                    response.ObjectID = count;
                }
                else
                {
                    logger.Debug("No data found...");
                    throw new Exception("Empty data.");
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error occured YPRMPR:" + ex.Message);
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        public string trimValues(string value) {

            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            else {
                return value.Trim();
            }
        }

        private void ProcessPostIntegration(string spName, SortedDictionary<object, object> sd = null)
        {
            sd = sd ?? new SortedDictionary<object, object>() { };
            var dataset = sqlHelper.SelectList(spName, sd);
        }
    }
}