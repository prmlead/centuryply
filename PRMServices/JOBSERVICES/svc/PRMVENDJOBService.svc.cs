﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.SQLHelper;
using PRMServices.Models;
using System.Data;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Net.Mail;
using PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMVENDJOBService : IPRMVENDJOBService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        PRMServices prmServices = new PRMServices();

        public int RemindVendorsBeforeNeg30min(bool sendEmail, bool sendSms, int userID, DateTime currentTime, int timeGap)
        {

            int noOfRecords = new int();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_JOB_TIME", currentTime);
                sd.Add("P_U_ID", userID);
                sd.Add("P_TIME_GAP", timeGap);

                DataSet ds = sqlHelper.SelectList("cp_sendNegotiationReminders", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    
                    
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        noOfRecords = ds.Tables[0].Rows.Count;
                        string vendEmail = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        string vendPhone = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        string vendFullName = row["FULL_NAME"] != DBNull.Value ? Convert.ToString(row["FULL_NAME"]) : string.Empty;
                        string reqTitle = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        string reqID = row["REQ_ID"] != DBNull.Value ? Convert.ToString(row["REQ_ID"]) : string.Empty;
                        int vendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        int customerID = row["REQ_U_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_U_ID"]) : 0;
                        string customerName = row["CUSTOMER_NAME"] != DBNull.Value ? Convert.ToString(row["CUSTOMER_NAME"]) : string.Empty;
                        string customerEmail = row["CUSTOMER_EMAIL"] != DBNull.Value ? Convert.ToString(row["CUSTOMER_EMAIL"]) : string.Empty;
                        DateTime startTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
                        
                        if (sendEmail) {
                            
                            string body = prmServices.GenerateEmailBody("VendoremailForAuctionStartUpdate_reminder");
                            body = String.Format(body, vendFullName, reqID, prmServices.toLocal(startTime), reqTitle);
                           

                            prmServices.SendEmail(vendEmail, "Negotiation Start time has been updated for REQID: " + reqID + " Title: " + reqTitle, body, Convert.ToInt32(reqID), 
                                vendorID, "NEG_30MIN_REMINDER", null, null, null, prmServices.toLocal(startTime), 0, null).ConfigureAwait(false);

                            //string body1 = prmServices.GenerateEmailBody("CustomeremailForUpdateAuctionStartUpdate_reminder");
                            //body1 = String.Format(body1, customerName, req_id, prmServices.toLocal(start_time), req_title);
                            //prmServices.SendEmail(customerEmail, "Negotiation Start time has been updated for REQID: " + req_id + " Title: " + req_title, body1, Convert.ToInt32(req_id),
                            //            customerID, "NEG_30MIN_REMINDER", null, null, null, prmServices.toLocal(start_time), 0, null).ConfigureAwait(false);

                        }

                        if (sendSms) {

                        }
                    }
                    
                    
                }

            }
            catch (Exception ex)
            {
                logger.Error("Error in RemindVendorsBeforeNeg30min Job", ex.Message);
            }

            return noOfRecords;

        }


        public int GetPendingNegotiationsReport(string subjectFromJob, DateTime from, DateTime to, string userID,bool sendEmail, bool sendSms, string bodyContent,int dateDiff)
        {
            int noOfRecords = new int();
            try
            {
                #region Daily Report
                List<ConsolidatedReport> details = new List<ConsolidatedReport>();
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                
                sd.Add("P_DATE_DIFF", dateDiff);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_U_ID", userID);
                sd.Add("P_TO_DATE", to);
                DataSet ds = sqlHelper.SelectList("cp_sendNegotationPendingReport", sd);
                string emails = "";
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        noOfRecords = ds.Tables[0].Rows.Count;
                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.ReqDept = row["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row["DEPT_CODE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.UtcNow;
                        emails = row["EMAILS"] != DBNull.Value ? Convert.ToString(row["EMAILS"]) : string.Empty;
                        report.POSTED_BY_USER = row["POSTED_BY"] != DBNull.Value ? Convert.ToString(row["POSTED_BY"]) : string.Empty;

                        if (report.StartTime == DateTime.MaxValue)
                        {
                            report.Closed = prmServices.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }

                        details.Add(report);
                    }
                }
                   
                    

                if (details != null && details.Count > 0)
                {
                    string emailContent = prmServices.GenerateEmailBody("pendingRequirementsemail");
                    string bodyTemplate = prmServices.GenerateEmailBody("pendingRequirements_body");
                    string subject = subjectFromJob;
                    string body = string.Empty;
                    emailContent = string.Format(emailContent, bodyContent);
                    foreach (var item in details)
                    {
                        if (item.Closed == "UNCONFIRMED")
                        {
                            item.Closed = "Quotations pending";
                        }
                        else if (item.Closed == "NOTSTARTED")
                        {
                            item.Closed = "Negotiation scheduled";
                        }
                        else if (item.Closed == "STARTED")
                        {
                            item.Closed = "Negotiation started";
                        }
                        else if (item.Closed == "Negotiation Ended")
                        {
                            item.Closed = "Negotiation Closed";
                        }
                        else if (item.Closed == "DELETED")
                        {
                            item.Closed = "Negotiation Cancelled";
                        }
                        body = body + string.Format(bodyTemplate, item.RequirementID,item.Title,item.POSTED_BY_USER,item.ReqDept,prmServices.toLocal(item.ReqPostedOn),item.NoOfVendorsInvited,item.NoOfvendorsParticipated,item.Closed);
                    }

                    emailContent = emailContent.Replace("%%BODY%%", body);

                    if (!string.IsNullOrEmpty(emails))
                    {
                        prmServices.SendEmail(emails, subject, emailContent,0,0, "PENDING_NEGOTIATION", null, null, null).ConfigureAwait(false);
                    }
                }

                #endregion Daily Report
            }
            catch (Exception ex)
            {
                logger.Error("Error in GetPendingNegotiationsReport Job", ex.Message);
            }
           
            return noOfRecords;
        }


        

    }

}