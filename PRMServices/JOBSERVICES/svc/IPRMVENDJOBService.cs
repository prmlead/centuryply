﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMVENDJOBService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "remindVendorsBeforeNeg30min?sendEmail={sendEmail}&sendSms={sendSms}&userID={userID}&currentTime={currentTime}&timeGap={timeGap}")]
        int RemindVendorsBeforeNeg30min(bool sendEmail,bool sendSms,int userID, DateTime currentTime, int timeGap);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPendingNegotiationsReport?template={template}&from={from}&to={to}&userID={userID}&sendEmail={sendEmail}&sendSms={sendSms}&bodyContent={bodyContent}&dateDiff={dateDiff}")]
        int GetPendingNegotiationsReport(string template, DateTime from, DateTime to, string userID, bool sendEmail, bool sendSms, string bodyContent, int dateDiff);
    }
}
