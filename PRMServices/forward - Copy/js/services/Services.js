prmApp.constant('PRMFwdReqServiceDomain', 'forward/svc/PRMFwdReqService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMFwdReqServiceService', ["PRMFwdReqServiceDomain", "userService", "httpServices",
    function (PRMFwdReqServiceDomain, userService, httpServices) {
        var PRMFwdReqServiceService = this;

        PRMFwdReqServiceService.getrequirementdata = function (params) {
            let url = PRMFwdReqServiceDomain + 'getrequirementdata?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        PRMFwdReqServiceService.RestartNegotiation = function (params) {
            var url = PRMFwdReqServiceDomain + 'restartnegotiation';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.makeabid = function (params) {
            let url = PRMFwdReqServiceDomain + 'makeabid';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.uploadQuotation = function (params) {
            let url = PRMFwdReqServiceDomain + 'uploadquotation';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.revquotationupload = function (params) {
            let url = PRMFwdReqServiceDomain + 'revquotationupload';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.QuatationAprovel = function (params) {
            let url = PRMFwdReqServiceDomain + 'quatationapproval';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.postrequirementdata = function (params) {
            var myDate = new Date();
            var myEpoch = parseInt(myDate.getTime() / 1000);
            params.minBidAmount = 0;
            params.postedOn = "/Date(" + myEpoch + "000+0000)/";
            params.timeLeft = -1;
            params.price = -1;
            var requirement = {
                "requirement": {
                    "title": params.title,
                    "description": params.description,
                    "category": params.category,
                    "subcategories": params.subcategories,
                    "urgency": params.urgency,
                    "budget": params.budget,
                    "attachmentName": params.attachmentName,
                    "deliveryLocation": params.deliveryLocation,
                    "taxes": params.taxes,
                    "paymentTerms": params.paymentTerms,
                    "requirementID": params.requirementID,
                    "customerID": params.customerID,
                    "isClosed": params.isClosed,
                    "endTime": null,
                    "sessionID": params.sessionID,
                    "errorMessage": "",
                    "timeLeft": -1,
                    "price": -1,
                    "auctionVendors": params.auctionVendors,
                    "startTime": null,
                    "status": "",
                    "postedOn": "/Date(" + myEpoch + "000+0000)/",
                    "custLastName": params.customerLastname,
                    "custFirstName": params.customerFirstname,
                    "deliveryTime": params.deliveryTime,
                    "includeFreight": params.includeFreight,
                    "inclusiveTax": params.inclusiveTax,
                    "minBidAmount": 0,
                    "checkBoxEmail": params.checkBoxEmail,
                    "checkBoxSms": params.checkBoxSms,
                    "quotationFreezTime": params.quotationFreezTime,
                    "currency": params.currency,
                    "timeZoneID": params.timeZoneID,
                    "listFwdRequirementItems": params.listFwdRequirementItems,
                    "isTabular": params.isTabular,
                    "reqComments": params.reqComments,
                    "itemsAttachment": params.itemsAttachment,
                    "isSubmit": params.isSubmit,
                    "isQuotationPriceLimit": params.isQuotationPriceLimit,
                    "quotationPriceLimit": params.quotationPriceLimit,
                    "noOfQuotationReminders": params.noOfQuotationReminders,
                    "remindersTimeInterval": params.remindersTimeInterval,
                    "custCompID": params.custCompID,
                    "customerCompanyName": params.customerCompanyName,
                    "deleteQuotations": params.deleteQuotations,
                    "indentID": params.indentID,
                    "expStartTime": params.expStartTime,
                    "cloneID": params.cloneID,
                    "isDiscountQuotation": params.isDiscountQuotation,
                    "contactDetails": params.contactDetails,
                    "generalTC": params.generalTC,
                    "isRevUnitDiscountEnable": params.isRevUnitDiscountEnable,
                    "multipleAttachments": params.multipleAttachments,
                    "contractStartTime": params.contractStartTime,
                    "contractEndTime": params.contractEndTime,
                    "isContract": params.isContract,
                    "prCode": params.prCode,
                    "PR_ID": params.PR_ID
                }, "attachment": params.attachment
            };
            let url = PRMFwdReqServiceDomain + 'requirementsave';
            return httpServices.post(url, requirement);
        };

        PRMFwdReqServiceService.savepricecap = function (params) {
            let url = PRMFwdReqServiceDomain + 'savepricecap';
            return httpServices.post(url, params);
        };

        PRMFwdReqServiceService.GetBidHistory = function (params) {
            let url = PRMFwdReqServiceDomain + 'getbidhistory?reqid=' + params.reqid + "&sessionid=" + params.sessionid + "&userid=" + params.userid;
            return httpServices.get(url);
        };

        return PRMFwdReqServiceService;

}]);