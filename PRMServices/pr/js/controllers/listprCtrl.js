﻿prmApp
    .controller('listprCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices",
        "PRMCustomFieldService",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, PRMCustomFieldService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.PRStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };
            $scope.prExcelReport = [];

            /********  CONSOLIDATE PR ********/
            //$scope.prDet = {
            //    prLevel: 0
            //};
            /********  CONSOLIDATE PR ********/

            $scope.filtersList = {
                plantList: [],
                wbsCodeList: [],
                projectTypeList: [],
                projectNameList: [],
                sectionHeadList: [],
                purchaseGroupList1: [],
                statusList: [],
                profitCentreList: [],
                requisitionList: [],
                plantListGrp: []
            };

            $scope.filters = {
                status: '',
                plant: [],
                plantGrp: {},
                projectType: {},
                sectionHead: {},
                wbsCode: {},
                projectName: {},
                purcahseGroup: {},
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: '',
                profitCentre: {},
                requisitioners: {},
                newPrStatus: {},
                requisitionerName: {},
                searchRequirement: '',
                selectedRequirement: null,
                prToDate: '',
                prFromDate: '',
                dateFilter: 'REQUISITION'
            };
            $scope.filters1 = {

                status: '',
                plant: [],
                plantGrp: {},
                projectType: {},
                sectionHead: {},
                wbsCode: {},
                projectName: {},
                purcahseGroup: {},
                material: '',
                docType: '',
                purchaseGroup: '',
                searchKeyword: '',
                profitCentre: {},
                requisitioners: {},
                newPrStatus: {},
                requisitionerName: {},
                searchRequirement: '',
                selectedRequirement: null,
            };

            $scope.filters.prToDate = moment().format('YYYY-MM-DD');
            $scope.filters.prFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.PRItemStats = {
                totalPRs: 0,
                newPRs: 0,
                partialPRs: 0,
                inProgressPRs: 0,
                totalPRItems: 0,
                totalRFQPosted: 0
            };

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.PlantsList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.materialGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.purchaseGroupList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.docTypeList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];
            $scope.prStatusList = [{
                FIELD_NAME: 'All',
                FIELD_VALUE: ''
            }];

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getprlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.PRList = [];
            $scope.filteredPRList = [];
            $scope.PRItems = [];
            $scope.itemList1 = [];
            $scope.filtereditemList1 = [];
            $scope.PRItems1 = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function () {

                let selectedPlants = [];
                if ($scope.filters.plantGrp && $scope.filters.plantGrp.length > 0) {
                    $scope.filters.plantGrp.forEach(function (plant, index) {
                        var filteredPlants = $scope.filtersList.plantList.filter(function (item) {
                            return item.FILTER1 === plant;
                        });

                        if (filteredPlants && filteredPlants.length > 0) {
                            //filteredPlants.forEach(function (plant, index) {
                            //    if (!selectedPlants.includes(plant.id) && plant.id) {
                            //        selectedPlants.push(plant.id);
                            //    }
                            //});
                            $scope.filters.plant = $scope.filters.plant.concat(filteredPlants);
                        }
                    });
                }

                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs = 0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;
                $scope.PRItemStats.totalPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;


                    if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.newPrStatus) || !_.isEmpty($scope.filters.plant) || !_.isEmpty($scope.filters.purcahseGroup)) {
                        // $scope.getprlist(currentPage, 10, $scope.filters.searchKeyword);
                        $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                    } else {

                        if ($scope.initialPRPageArray && $scope.initialPRPageArray.length > 0) {
                            $scope.PRList = $scope.initialPRPageArray;
                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }

                        }
                    }
                } else {
                    $scope.filtereditemList1 = $scope.itemsList;
                    $scope.totalItems1 = $scope.filtereditemList1.length;

                    if ($scope.filters1.searchKeyword) {
                        $scope.getPRSBasedOnItem(0, 10, $scope.filters1.searchKeyword);
                    } else {

                        if ($scope.initialitemPageArray1 && $scope.initialitemPageArray1.length > 0) {
                            $scope.itemsList = $scope.initialitemPageArray1;
                            if ($scope.itemsList && $scope.itemsList.length > 0) {
                                $scope.totalItems1 = $scope.itemsList[0].TOTAL_PR_ITEM_COUNT;
                                $scope.PRItemStats.totalPRs = $scope.totalItems1;
                                $scope.filtereditemList1 = $scope.itemsList;
                            }

                        }
                    }
                }

            };

            $scope.filterByDate = function () {
                $scope.PRStats.totalPRs = 0;
                $scope.PRStats.newPRs = 0;
                $scope.PRStats.partialPRs = 0;
                $scope.PRStats.totalPRItems = 0;
                $scope.PRStats.totalRFQPosted = 0;
                $scope.PRStats.inProgressPRs = 0;

                if ($scope.prDet.prLevel) {
                    $scope.filteredPRList = $scope.PRList;
                    $scope.totalItems = $scope.filteredPRList.length;
                    $scope.getprlist(0, 10, $scope.filters.searchKeyword);
                } else {
                    $scope.getPRSBasedOnItem(0, 10, $scope.filters1.searchKeyword);
                }
            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPRPageArray = [];

            $scope.getprlist = function (recordsFetchFrom, pageSize, searchString) {

                var plant, projectType, sectionHead, wbsCode, profitCentre, purchaseCode, creatorName, clientName, prStatus, fromDate, toDate;

                if (_.isEmpty($scope.filters.prFromDate)) {
                    fromDate = '';
                } else {
                    fromDate = $scope.filters.prFromDate;
                }

                if (_.isEmpty($scope.filters.prToDate)) {
                    toDate = '';
                } else {
                    toDate = $scope.filters.prToDate;
                }

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    //var plants = _($scope.filters.plant)
                    //    .filter(item => item.id)
                    //    .map('id')
                    //    .value();
                    plant = $scope.filters.plant.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters.purcahseGroup && $scope.filters.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }

                if (_.isEmpty($scope.filters.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters.requisitionerName && $scope.filters.requisitionerName.length > 0) {
                    var creators = _($scope.filters.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }

                if (_.isEmpty($scope.filters.projectName)) {
                    clientName = '';
                } else if ($scope.filters.projectName && $scope.filters.projectName.length > 0) {
                    var clientNames = _($scope.filters.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                if (_.isEmpty($scope.filters.newPrStatus)) {
                    prStatus = '';
                } else if ($scope.filters.newPrStatus && $scope.filters.newPrStatus.length > 0) {
                    var statusFilters = _($scope.filters.newPrStatus)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    prStatus = statusFilters.join(',');
                }

                var params = {
                    "userid": userService.getUserId(),
                    "sessionid": userService.getUserToken(),
                    "deptid": userService.getSelectedUserDepartmentDesignation().deptID,
                    "desigid": userService.getSelectedUserDesigID(),
                    "depttypeid": userService.getSelectedUserDepartmentDesignation().deptTypeID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "prStatus": prStatus,
                    "searchString": searchString ? searchString : "",
                    "PageSize": recordsFetchFrom * pageSize,
                    'fromDate': fromDate,
                    'toDate': toDate,
                    'dateFilter': $scope.filters.dateFilter ? $scope.filters.dateFilter : 'REQUISITION',
                    "NumberOfRecords": pageSize
                };

                $scope.pageSizeTemp = (params.PageSize + 1);
                //$scope.NumberOfRecords = recordsFetchFrom > 0 ? ((recordsFetchFrom + 1) * pageSize) : $scope.PRStats.totalPRs;
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMPRServices.getprlist(params)
                    .then(function (response) {
                        //$scope.consolidatedReport = response;
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';
                                item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }
                        if (!$scope.downloadExcel) {
                            $scope.PRList = [];
                            $scope.filteredPRList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    item.selectPRSForRFQ = false;
                                    $scope.PRList.push(item);
                                    if ($scope.initialPRPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                                        $scope.initialPRPageArray.push(item);
                                    }
                                });

                            }

                            if ($scope.PRList && $scope.PRList.length > 0) {
                                $scope.totalItems = $scope.PRList[0].TOTAL_PR_COUNT;
                                $scope.PRStats.totalPRs = $scope.totalItems;
                                $scope.PRStats.newPRs = $scope.PRList[0].newPRs;
                                $scope.PRStats.partialPRs = $scope.PRList[0].partialPRs;
                                $scope.PRStats.totalPRItems = $scope.PRList[0].totalPRItems;
                                $scope.PRStats.totalRFQPosted = $scope.PRList[0].totalRFQPosted;
                                $scope.PRStats.inProgressPRs = $scope.PRList[0].inProgressPRs;
                                $scope.filteredPRList = $scope.PRList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.prExcelReport = response;
                                downloadPRExcel()
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }

                    });


            };


            $scope.getprlist(0, 10, $scope.searchString);
            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.compId,
                    'fromDate': $scope.filters.prFromDate,
                    'toDate': $scope.filters.prToDate
                };

                let plantListTemp = [];
                let wbsCodeTemp = [];
                let projectTypeTemp = [];
                let projectNameTemp = [];
                let sectionHeadTemp = [];
                let purchaseGroupTemp = [];
                let statusTemp = [];
                let profitCentreTemp = [];
                let requisitionListTemp = [];
                let plantListGrpTemp = [];

                PRMPRServices.getFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.TYPE === 'WBS_CODE') {
                                        wbsCodeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROFIT_CENTER') {
                                        profitCentreTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_TYPE') {
                                        projectTypeTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'SECTION_HEAD') {
                                        sectionHeadTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PROJECT_DESCRIPTION') {
                                        projectNameTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PLANTS') {
                                        plantListTemp.push({ id: +item.NAME, name: item.ID + ' - ' + item.NAME, FILTER1: item.FILTER1 });
                                        if (!plantListGrpTemp.includes(item.FILTER1)) {
                                            plantListGrpTemp.push(item.FILTER1);
                                        }
                                    } else if (item.TYPE === 'PURCHASE_GROUP') {
                                        purchaseGroupTemp.push({ id: item.NAME, name: item.ID });
                                    } else if (item.TYPE === 'PR_CREATOR_NAME') {
                                        requisitionListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.TYPE === 'PR_STATUS') {
                                        statusTemp.push({ id: item.ID, name: item.NAME });
                                    }

                                });

                                $scope.filtersList.requisitionList = requisitionListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.plantListGrp = plantListGrpTemp;
                                $scope.filtersList.wbsCodeList = wbsCodeTemp;
                                $scope.filtersList.projectTypeList = projectTypeTemp;
                                $scope.filtersList.sectionHeadList = sectionHeadTemp;
                                $scope.filtersList.projectNameList = projectNameTemp;
                                $scope.filtersList.purchaseGroupList1 = purchaseGroupTemp;
                                $scope.filtersList.statusList = statusTemp;
                                $scope.filtersList.profitCentreList = profitCentreTemp;
                            }
                        }
                    });

            };
            $scope.getFilterValues();


            $scope.selectedPRItems = [];

            $scope.getPrItems = function (prDetails, createRequirement) {
                if (prDetails) {
                    var params = {
                        "prid": prDetails.PR_ID
                    };
                    PRMPRServices.getpritemslist(params)
                        .then(function (response) {
                            $scope.PRItems = response;
                            $scope.PRItemsTemp = [];
                            //$scope.PRItemsTemp = $scope.PRItems;
                            //$scope.PRItems.forEach(function (item, index) {
                            //    //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE).split(' ')[0];
                            //    //item.DELIVERY_DATE = userService.toLocalDate(item.DELIVERY_DATE).split(' ')[0];
                            //    item.RFQS = [];
                            //    item.REQ_DETAILS = [];
                            //});
                            $scope.PRItems.forEach(function (item, index) {
                                if (item.REQ_ID <= 0) {
                                    item.RFQS = [];
                                    item.REQ_DETAILS = [];
                                    $scope.PRItemsTemp.push(item);
                                }
                            });

                            prDetails.PRItems = $scope.PRItems;

                            if (createRequirement) {
                                if (prDetails) {
                                    prDetails.isSelected = true;
                                    //$scope.selectedPRItems = [];
                                    if (prDetails.PRItems) {
                                        prDetails.PRItems.forEach(function (prItem) {
                                            prItem.isSelected = true;
                                        });
                                    }
                                    $scope.selectedPRItems.push(prDetails);
                                    //$scope.createRequirementMultiplePR();
                                }
                                $scope.selectedPRItems = _.uniqBy($scope.selectedPRItems, 'PR_ID');
                            }

                            if (prDetails.selectPRSForRFQ) {
                                prDetails.PRItems.forEach(function (item, index) {
                                    item.isCheckedPrItem = true;
                                });
                            }


                            $scope.PreviewItems = [];
                        });
                }
            };


            $scope.goToPrEdit = function (id) {
                var url = $state.href("save-pr-details", { "Id": id });
                window.open(url, '_self');
            };

            $scope.goToPrAction = function (id) {
                var url = $state.href("pr-actions", { "Id": id });
                window.open(url, '_blank');
            };

            $scope.createRequirement = function (prDetails) {
                $scope.getPrItems(prDetails, true);
            };

            $scope.createRequirementMultiplePR = function () {
                //let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                //    return prObj.isSelected;
                //});

                ////if (validSelectedPRs && validSelectedPRs.length > 0) {
                ////    $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs });
                ////}
            };

            $scope.addToRequirementList = function (prDetails, item, action) {
                if (action === 'ADD') {
                    item.isSelected = true;
                    prDetails.isSelected = true;
                    let index = _.findIndex($scope.selectedPRItems, function (pr) {
                        return pr.PR_ID === prDetails.PR_ID;
                    });

                    if (index < 0) {
                        $scope.selectedPRItems.push(prDetails);
                    }

                } else {
                    item.isSelected = false;
                    let selectedPRItems = _.filter(prDetails.PRItems, function (prItem) {
                        return prItem.isSelected;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        prDetails.isSelected = true;
                    } else {
                        prDetails.isSelected = false;
                    }
                }


            };

            $scope.showRequirementButton = function () {
                let isvisible = false;
                $scope.selectedPRNumbers = '';

                if ($scope.selectedPRItems && $scope.selectedPRItems.length > 0) {
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prObj) {
                        return prObj.isSelected;
                    });

                    if (validSelectedPRs && validSelectedPRs.length > 0) {
                        var prNumbers = _(validSelectedPRs)
                            .map('PR_NUMBER')
                            .value();
                        $scope.selectedPRNumbers = prNumbers.join(',');
                        isvisible = true;

                    }
                }

                return isvisible;
            };

            $scope.showRequirementItemsButton = function () {
                let isvisible = false;

                if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {

                    let selectedPRItems = _.filter($scope.selectedItemsForRFQ, function (prItem) {
                        return prItem.selectForRFQ && prItem.OVERALL_ITEM_QUANTITY != prItem.RFQ_QUANTITY;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        isvisible = true;
                    }
                }
                return isvisible;
            };

            $scope.showLinkToRFP = function (prDetails) {
                $scope.selectedPR = prDetails;
                $scope.getAuctions();
            };

            $scope.getAuctions = function () {
                auctionsService.SearchRequirements({ "search": '', "excludeprlinked": true, "compid": $scope.compId, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response) {
                            $scope.myAuctions1 = [];
                            $scope.myAuctionsFiltred = [];
                            $scope.myAuctions1 = response;
                            $scope.myAuctionsFiltred = _.orderBy($scope.myAuctions1, ['requirementId'], ['desc']);
                        }
                    });
            };

            $scope.isLast = function (last) {
                var a = '';
                if (last) {
                    a = '';
                } else {
                    a = ',';
                }
                return a;
            };

            $scope.searchRFPs = function (product) {
                product.RFQS = [];
                product.REQ_DETAILS = [];
                if (product.search) {
                    PRMPRServices.GetRFPS({ "SEARCH": product.search.toLowerCase(), "COMP_ID": $scope.compId, "U_ID": $scope.userID })
                        .then(function (response) {
                            if (response) {
                                product.RFQS = response;
                            }
                        });
                }
            };

            /********  CONSOLIDATE PR ********/
            $scope.prDet = {
                prLevel: 1
            };

            $scope.totalItems1 = 0;
            $scope.currentPage1 = 1;
            $scope.itemsPerPage1 = 10;
            $scope.maxSize1 = 5;

            $scope.setPage1 = function (pageNo) {
                $scope.currentPage1 = pageNo;
                $scope.getPRSBasedOnItem(($scope.currentPage1 - 1), 10, $scope.filters1.searchKeyword);
            };

            $scope.pageChanged1 = function () {

                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.search = '';
            $scope.itemsList = [];
            $scope.initialitemPageArray1 = [];
            $scope.getPRSBasedOnItem = function (recordsFetchFrom, pageSize, searchKeyword) {
                $scope.prDet.prLevel = 0;
                $scope.selectedPRNumbers = '';
                var plant, projectType, sectionHead, wbsCode, profitCentre = '', purchaseCode = '';

                if (_.isEmpty($scope.filters1.plant)) {
                    plant = '';
                } else if ($scope.filters1.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters1.projectType)) {
                    projectType = '';
                } else if ($scope.filters1.projectType && $scope.filters1.projectType.length > 0) {
                    var projectTypes = _($scope.filters1.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters1.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters1.sectionHead && $scope.filters1.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters1.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters1.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters1.wbsCode && $scope.filters1.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters1.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters1.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters1.profitCentre && $scope.filters1.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters1.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }

                if (_.isEmpty($scope.filters1.purcahseGroup)) {
                    purchaseCode = '';
                } else if ($scope.filters1.purcahseGroup && $scope.filters1.purcahseGroup.length > 0) {
                    var purchaseCodes = _($scope.filters1.purcahseGroup)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    purchaseCode = purchaseCodes.join(',');
                }
                if (_.isEmpty($scope.filters1.requisitionerName)) {
                    creatorName = '';
                } else if ($scope.filters1.requisitionerName && $scope.filters1.requisitionerName.length > 0) {
                    var creators = _($scope.filters1.requisitionerName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    creatorName = creators.join(',');
                }
                if (_.isEmpty($scope.filters1.projectName)) {
                    clientName = '';
                } else if ($scope.filters1.projectName && $scope.filters1.projectName.length > 0) {
                    var clientNames = _($scope.filters1.projectName)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    clientName = clientNames.join(',');
                }

                var params = {
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "purchaseCode": purchaseCode,
                    "creatorName": creatorName,
                    "clientName": clientName,
                    "search": searchKeyword ? searchKeyword : '',
                    "PageSize": recordsFetchFrom * pageSize,
                    "NumberOfRecords": pageSize
                };
                $scope.pageSizeTemp1 = (params.PageSize + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);


                PRMPRServices.GetIndividualItems(params)
                    .then(function (response) {
                        $scope.itemsList = [];
                        $scope.filtereditemList1 = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.ITEM_ID != 0;
                                item.selectForRFQ = false;
                                item.IsDisabled = false;
                                item.SELECTED_QUANTITY = 0;
                                item.RFQ_QUANTITY = 0;
                                item.FILTERED_PR_COUNT = 0;
                                $scope.itemsList.push(item);
                                if ($scope.initialitemPageArray1.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)
                                    $scope.initialitemPageArray1.push(item);
                                }
                            });

                        }

                        if ($scope.itemsList && $scope.itemsList.length > 0) {
                            $scope.totalItems1 = $scope.itemsList[0].TOTAL_PR_ITEM_COUNT;
                            $scope.PRItemStats.totalPRs = $scope.totalItems1;
                            $scope.filtereditemList1 = $scope.itemsList;
                        }

                    });

            };

            $scope.getPRLevelView = function () {

                $scope.selectedPRNumbers = '';
            };

            $scope.ItemPRS = [];
            $scope.ItemPRSTemp = [];
            $scope.ItemPRSPopUp = [];
            $scope.ItemPRSPopUpTemp = [];

            $scope.GetPRSbyItem = function (prItem, type, productid) {

                var plant, projectType, sectionHead, wbsCode, profitCentre = '';

                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }

                if (_.isEmpty($scope.filters.projectType)) {
                    projectType = '';
                } else if ($scope.filters.projectType && $scope.filters.projectType.length > 0) {
                    var projectTypes = _($scope.filters.projectType)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    projectType = projectTypes.join(',');
                }

                if (_.isEmpty($scope.filters.sectionHead)) {
                    sectionHead = '';
                } else if ($scope.filters.sectionHead && $scope.filters.sectionHead.length > 0) {
                    var sectionHeads = _($scope.filters.sectionHead)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    sectionHead = sectionHeads.join(',');
                }

                if (_.isEmpty($scope.filters.wbsCode)) {
                    wbsCode = '';
                } else if ($scope.filters.wbsCode && $scope.filters.wbsCode.length > 0) {
                    var wbsCodes = _($scope.filters.wbsCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    wbsCode = wbsCodes.join(',');
                }

                if (_.isEmpty($scope.filters.profitCentre)) {
                    profitCentre = '';
                } else if ($scope.filters.profitCentre && $scope.filters.profitCentre.length > 0) {
                    var profitCentres = _($scope.filters.profitCentre)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    profitCentre = profitCentres.join(',');
                }



                var params = {
                    "PRODUCT_ID": prItem.PRODUCT_ID,
                    "plant": plant,
                    "projectType": projectType,
                    "sectionHead": sectionHead,
                    "wbsCode": wbsCode,
                    "profitCentre": profitCentre,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ''
                };

                if (!prItem.ItemPRS) {
                    PRMPRServices.GetPRSbyItem(params)
                        .then(function (response) {
                            $scope.ItemPRS = response;
                            $scope.ItemPRSPopUp = response;
                            $scope.ItemPRSPopUpTemp = response;
                            $scope.ItemPRSTemp = response;
                            //prItem.FILTERED_PR_COUNT = $scope.ItemPRS.length;
                            var prids = _.uniqBy($scope.ItemPRS, 'PR_NUMBER');
                            prItem.FILTERED_PR_COUNT = prids.length;
                            /**** Disabling For All Items ****/
                            $scope.itemsList.forEach(function (item, index) {
                                item.IsDisabled = false;
                            });
                            /**** Disabling For All Items ****/

                            $scope.ItemPRS.forEach(function (item, index) {
                                //item.CREATED_DATE = moment(item.CREATED_DATE).format("YYYY-MM-DD");

                                var releaseDateTemp = item.RELEASE_DATE ? moment(item.RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.RELEASE_DATE = releaseDateTemp != '-' ? releaseDateTemp.contains("1970") ? '-' : releaseDateTemp : '-';

                                //if (item.COMPLETED_ITEMS === item.TOTAL_ITEMS) {
                                //    
                                //    item.NEW_PR_STATUS = "In Progess"
                                //} else if (item.COMPLETED_ITEMS > 0 && item.COMPLETED_ITEMS < item.TOTAL_ITEMS) {
                                //    item.NEW_PR_STATUS = "Partial"
                                //} else {
                                //    item.NEW_PR_STATUS = "New"
                                //}


                                prItem.isExpanded1 = true;

                                item.isChecked = prItem.isExpanded1;


                            });



                            $scope.ItemPRSPopUp = $scope.ItemPRS;
                            $scope.ItemPRSPopUpTemp = $scope.ItemPRS;
                            $scope.ItemPRSTemp = $scope.ItemPRS;

                            //$scope.ItemPRSPopUp.forEach(function (itemPopup, indexPopup) {
                            //    itemPopup.CREATED_DATE = userService.toLocalDate(itemPopup.CREATED_DATE).split(' ')[0];
                            //    itemPopup.RELEASE_DATE = userService.toLocalDate(itemPopup.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopup.isChecked = prItem.isExpanded1;
                            //});

                            //$scope.ItemPRSPopUpTemp.forEach(function (itemPopupTemp, indexPopup) {
                            //    itemPopupTemp.CREATED_DATE = userService.toLocalDate(itemPopupTemp.CREATED_DATE).split(' ')[0];
                            //    itemPopupTemp.RELEASE_DATE = userService.toLocalDate(itemPopupTemp.RELEASE_DATE).split(' ')[0];
                            //    prItem.isExpanded1 = true;
                            //    itemPopupTemp.isChecked = prItem.isExpanded1;
                            //});

                            $scope.totalItems2 = $scope.ItemPRSPopUp.length;


                            calculateQuantity($scope.ItemPRS, prItem);
                            $scope.ItemPRSTemp.forEach(function (item, index) {
                                //item.CREATED_DATE = userService.toLocalDate(item.CREATED_DATE);
                                prItem.isExpanded1 = true;
                                item.isChecked = prItem.isExpanded1;
                            });
                            prItem.ItemPRS = $scope.ItemPRS;

                            prItem.ItemPRSTemp = prItem.ItemPRS;

                            /********** RFQ Posting With Selected Items ***********/
                            if (prItem.selectForRFQ) {
                                $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                            }
                            /********** RFQ Posting With Selected Items ***********/

                            if (type && type === 'ITEM_DISPLAY') {
                                $scope.displayItems(productid);
                            }


                        });
                } else {
                    /**** Disabling For All Items ****/
                    //calculateQuantity($scope.ItemPRS, prItem);
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });
                    /**** Disabling For All Items ****/


                    /********** RFQ Posting With Selected Items ***********/
                    if (prItem.selectForRFQ) {
                        $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                    }
                    /********** RFQ Posting With Selected Items ***********/

                    if (type && type === 'ITEM_DISPLAY') {
                        $scope.displayItems(productid);
                    }
                }

            };

            $scope.searchPR = function (searchText, prItem) {
                var filterText = angular.lowercase(searchText);
                if (!filterText || filterText == '' || filterText == undefined || filterText == null) {
                    prItem.ItemPRS = prItem.ItemPRSTemp;
                    $scope.consolidate(prItem, filterText);
                }
                else {
                    prItem.ItemPRS = prItem.ItemPRSTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                }

                $scope.totalItems1 = prItem.ItemPRS.length;

            };


            $scope.unSelectAll = function (prItem, searchedPR) {

                if (!prItem.isExpanded1) {
                    prItem.ItemPRS.forEach(function (item, index) {
                        if (item.REQ_ID <= 0) {
                            item.isChecked = false;
                        }
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                } else {
                    prItem.ItemPRS.forEach(function (item, index) {
                        item.isChecked = true;
                    });
                    $scope.consolidate(prItem, searchedPR, prItem.isExpanded1, 'OVERALL');
                }

            };

            $scope.consolidate = function (pr, searchedPR, isChecked, type) {
                if (searchedPR == null || searchedPR == "undefined" || searchedPR == "") {
                    if (pr && pr.ItemPRS.length > 0) {
                        var consolidateQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; });
                        var getRFQQuantity = pr.ItemPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                        if (consolidateQuantity && consolidateQuantity.length > 0) {
                            pr.SELECTED_QUANTITY = _.sumBy(consolidateQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                            pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                        } else {
                            pr.SELECTED_QUANTITY = 0;
                        }
                    }
                }
                else {
                    searchedPR = angular.lowercase(searchedPR);
                    var searchedFilteredPRS = [];
                    if (pr.isExpanded1) {
                        if (type === 'OVERALL') {
                            searchedFilteredPRS = $scope.ItemPRSTemp;
                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }
                        } else {
                            if (!isChecked) {
                                var unCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != unCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    } else {
                        if (type === 'OVERALL') {

                            var prOverallIds = _(pr.ItemPRS)
                                .filter(item => !item.isChecked)
                                .map('PR_ID')
                                .value();

                            searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return !prOverallIds.includes(item.PR_ID); }); // remove the unchecked PR and calculate

                            if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                calculateQuantity(searchedFilteredPRS, pr);
                            }

                        } else {
                            if (!isChecked) {
                                var itemUnCheckedPRID = _.result(_.find(pr.ItemPRS, function (items) {
                                    return !items.isChecked;
                                }), 'PR_ID'); // get Unchecked PR IR's

                                searchedFilteredPRS = $scope.ItemPRSTemp.filter(function (item) { return item.PR_ID != itemUnCheckedPRID; }); // remove the unchecked PR and calculate

                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }

                            } else {
                                searchedFilteredPRS = $scope.ItemPRSTemp; // Assign All The PR's from DB
                                if (searchedFilteredPRS && searchedFilteredPRS.length > 0) {
                                    calculateQuantity(searchedFilteredPRS, pr);
                                }
                            }
                        }
                    }

                }
            };


            function calculateQuantity(searchedFilteredPRS, pr) {
                var searchConsolidatedQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID <= 0; }); // calculate all the checked PR's Quantity
                var getRFQQuantity = searchedFilteredPRS.filter(function (items) { return items.isChecked && items.REQ_ID > 0; });
                if (searchConsolidatedQuantity && searchConsolidatedQuantity.length > 0) {
                    pr.SELECTED_QUANTITY = _.sumBy(searchConsolidatedQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else if (getRFQQuantity && getRFQQuantity.length > 0) {
                    pr.RFQ_QUANTITY = _.sumBy(getRFQQuantity, 'TOTAL_PR_ITEM_QUANTITY');
                } else {
                    pr.SELECTED_QUANTITY = 0;
                }
            }

            function addCategoryToFilters(catArray) {
                var categoryListresp = catArray;
                categoryListresp = _.uniqBy(categoryListresp, 'CATEGORY_ID');
                categoryListresp.forEach(function (item, index) {
                    var catObj = {
                        FIELD_NAME: '',
                        FIELD_VALUE: ''
                    };
                    catObj.FIELD_NAME = item.CategoryName;
                    catObj.FIELD_VALUE = item.CATEGORY_ID;
                    $scope.categoryList.push(catObj);
                });

                $scope.categoryList = _.uniqBy($scope.categoryList, 'FIELD_VALUE');

            }


            $scope.PostRequirement = function (prItem) {
                $scope.itemsList.forEach(function (item, index) {
                    item.IsDisabled = true;
                });
                if (prItem.selectForRFQ) {
                    $scope.GetPRSbyItem(prItem);
                } else {
                    $scope.itemsList.forEach(function (item, index) {
                        item.IsDisabled = false;
                    });

                    $scope.AddItemToRequirement(prItem, prItem.selectForRFQ);
                }
            };

            $scope.selectedItemsForRFQ = [];
            $scope.AddItemToRequirement = function (prItem, type) {

                if (type) {
                    var checkedPRIDs = _(prItem.ItemPRS)
                        .filter(item => item.isChecked)
                        .map('PR_ID')
                        .value();

                    prItem.PR_ID = checkedPRIDs.join(',');
                    $scope.selectedItemsForRFQ.push(prItem);
                } else {
                    if ($scope.selectedItemsForRFQ && $scope.selectedItemsForRFQ.length > 0) {
                        var itemIndex = _.findIndex($scope.selectedItemsForRFQ, function (item) {
                            return item.PRODUCT_ID === prItem.PRODUCT_ID;
                        });

                        if (itemIndex >= 0) {
                            $scope.selectedItemsForRFQ.splice(itemIndex, 1);
                        }
                    }
                }

                $scope.selectedItemsForRFQ = _.uniqBy($scope.selectedItemsForRFQ, 'PRODUCT_ID');

            };

            $scope.createRFQWithConsolidatedItems = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                var rfqPRSList = $scope.selectedItemsForRFQ.filter(function (item) {
                    return item.selectForRFQ && item.RFQ_QUANTITY != item.OVERALL_ITEM_QUANTITY;
                });

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    rfqPRSList.forEach(function (item, index) {
                        if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID) {
                            isEmptyItemCode = true;
                        }
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (rfqPRSList && rfqPRSList.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prItemsList': rfqPRSList, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prItemsList': rfqPRSList, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };

            /********  CONSOLIDATE PR ********/

            $scope.navigateToRequirement = function () {
                angular.element('#templateSelection').modal('hide');
                if ($scope.prDet.prLevel) {
                    //$scope.GetPRMTemplateFields();
                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    if (validSelectedPRs && validSelectedPRs.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }

                } else {
                    $scope.createRFQWithConsolidatedItems();
                }
            };

            $scope.navigateToRequirements1 = function () {
                angular.element('#templateSelection').modal('hide');
                if (!$scope.prDet.prLevel) {
                    // $scope.GetPRMTemplateFields();
                    let selectedPRItems = _.filter($scope.selectedItemsForRFQ, function (prItem) {
                        return prItem.selectForRFQ;
                    });

                    if (selectedPRItems && selectedPRItems.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prItemsList': selectedPRItems, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prItemsList': selectedPRItems, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }

                } else {
                    $scope.createRFQWithConsolidatedItems();
                }
            };


            $scope.GetPRMTemplates = function () {
                PRMCustomFieldService.GetTemplates().then(function (response) {
                    $scope.prmTemplates = response;
                    if ($scope.prmTemplates && $scope.prmTemplates.length > 0) {
                        $scope.selectedTemplate = $scope.prmTemplates[0];
                    }
                });
            };

            //$scope.GetPRMTemplates();


            $scope.GetPRMTemplateFields = function () {
                let prmFieldMappingDetails = {};
                let isServiceRelatedITems = false;
                let isEmptyItemCode = false;
                var template = $scope.selectedTemplate && $scope.selectedTemplate.TEMPLATE_NAME ? $scope.selectedTemplate.TEMPLATE_NAME : 'PRM-DEFAULT';
                var params = {
                    "templateid": 0,
                    "templatename": template,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplateFields(params).then(function (mappingDetails) {
                    mappingDetails.forEach(function (item, index) {
                        prmFieldMappingDetails[item.FIELD_NAME] = item;
                    });

                    if (prmFieldMappingDetails) {
                        prmFieldMappingDetails.IS_SEARCH_FOR_CAS = prmFieldMappingDetails.PRODUCT_CODE.FIELD_LABEL.toLowerCase().contains("cas number");
                        prmFieldMappingDetails.IS_SEARCH_FOR_MFCD = prmFieldMappingDetails.PRODUCT_NUMBER.FIELD_LABEL.toLowerCase().contains("mfcd code");
                        if (!isServiceRelatedITems && prmFieldMappingDetails.IS_SEARCH_FOR_CAS) {
                            isServiceRelatedITems = true;
                        }
                    }

                    let validSelectedPRs = _.filter($scope.selectedPRItems, function (prItemObj) {
                        return prItemObj.isSelected;
                    });

                    validSelectedPRs.forEach(function (prObj, index) {
                        prObj.PRItems.forEach(function (item, index1) {
                            if (prmFieldMappingDetails.IS_SEARCH_FOR_CAS && (item.ITEM_CODE_CAS || item.CASNR)) {
                                item.ITEM_CODE = item.ITEM_CODE_CAS || item.CASNR;
                            }

                            if (prmFieldMappingDetails.IS_SEARCH_FOR_MFCD && (item.ITEM_CODE_MFCD || item.MFCD_NUMBER)) {
                                item.ITEM_NUM = item.ITEM_CODE_MFCD || item.MFCD_NUMBER;
                            }

                            if (!isEmptyItemCode && !(item.ITEM_CODE_CAS || item.CASNR) && !item.REQ_ID && item.isCheckedPrItem) {
                                isEmptyItemCode = true;
                            }
                        });
                    });

                    if (isServiceRelatedITems && isEmptyItemCode && false) {
                        swal("Error!", 'Selected template is not Valid as empty CAS number');
                    }
                    else if (validSelectedPRs && validSelectedPRs.length > 0) {
                        if ($scope.filters.selectedRequirement && $scope.filters.selectedRequirement.requirementId) {
                            $state.go('save-requirementAdv', { 'Id': $scope.filters.selectedRequirement.requirementId, 'prDetailsList': validSelectedPRs, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        } else {
                            $state.go('save-requirementAdv', { 'prDetailsList': validSelectedPRs, 'selectedTemplate': $scope.selectedTemplate, 'selectedPRNumbers': $scope.prDet.prLevel ? $scope.selectedPRNumbers : '' });
                        }
                    }
                });
            };


            $scope.convertDate = function (date) {
                if (date) {
                    var convertedDateTemp = moment(date).format("DD-MM-YYYY");
                    return convertedDateTemp.contains("1970") ? '-' : convertedDateTemp;
                } else {
                    return '-';
                }
            };


            $scope.ItemsListPopup = [];

            $scope.showPRItemlevelDetails = function (prItem) {
                $scope.GetPRSbyItem(prItem, 'ITEM_DISPLAY', prItem.PRODUCT_ID);
            };

            $scope.displayItems = function (productId) {

                var PR_IDS = '';

                var pridspop = _($scope.ItemPRSPopUp)
                    .filter(item => item.PR_ID)
                    .map('PR_ID')
                    .value();
                PR_IDS = pridspop.join(',');

                var params = {
                    "prIds": PR_IDS
                };

                PRMPRServices.GetItemDetails(params)
                    .then(function (response) {
                        $scope.ItemsListPopup = response;

                        $scope.ItemsListPopup = $scope.ItemsListPopup.filter(function (item) {
                            return item.PRODUCT_ID === productId;
                        });

                    });
            };

            $scope.filterPRsPopup = function (searchText) {

                var filterText = angular.lowercase(searchText);

                if (filterText) {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp.filter(function (pr) {
                        return (String(angular.lowercase(pr.PR_NUMBER)).includes(filterText) == true);
                    });
                } else {
                    $scope.ItemPRSPopUp = $scope.ItemPRSPopUpTemp;
                }

                $scope.totalItems2 = $scope.ItemPRSPopUp.length;
            };

            $scope.selectMultiplePRS = [];

            $scope.createRFQWithMultiplePR = function (prDet) {
                if (prDet.selectPRSForRFQ) {
                    var getCheckedPRIDs = _($scope.filteredPRList)
                        .filter(pr => pr.selectPRSForRFQ)
                        .map('PR_ID')
                        .value(); // GET checked PR_ID's

                    if (getCheckedPRIDs && getCheckedPRIDs.length > 0) {

                        var checkedPRObjects = $scope.filteredPRList.filter(function (prItem, prIndex) { return getCheckedPRIDs[0] === prItem.PR_ID; });

                        if (checkedPRObjects && checkedPRObjects.length > 0) {
                            $scope.createRequirement(prDet, true);
                        }
                    }
                } else {

                    var getUnCheckedPRIDs = _($scope.selectedPRItems)
                        .filter(pr => !pr.selectPRSForRFQ)
                        .map('PR_ID')
                        .value(); // GET un checked PR_ID's 

                    prDet.PRItems.forEach(function (prItem, prIndex) {
                        prItem.isCheckedPrItem = false;
                    });

                    var prDetIndex = _.findIndex($scope.selectedPRItems, function (item) {
                        return item.PR_ID === getUnCheckedPRIDs[0];
                    }); // filter the PR in the RFQ posting List and get the Index

                    if (prDetIndex >= 0) { // if PR Found
                        $scope.selectedPRItems.splice(prDetIndex, 1); //remove the PR  and PR Items
                    }
                }
            };

            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.PRList = [];
                $scope.downloadExcel = true;
                $scope.getprlist(0, 0, $scope.searchString);
            };

            //$scope.searchRequirement = function () {
            //    $scope.filteredRequirements = [];
            //    auctionsService.SearchRequirements({ "search": $scope.filters.searchRequirement, "compid": $scope.compId, "sessionid": userService.getUserToken() })
            //        .then(function (response) {
            //            if (response) {
            //                $scope.filteredRequirements = response;
            //            }
            //        });
            //};

            $scope.selectRequirement = function () {
                //console.log($scope.filters.selectedRequirement);
            };



            function downloadPRExcel() {
                alasql('SELECT PR_NUMBER as [PR Number],PLANT as [Plant],PLANT_NAME as [Plant Name], ' +
                    'PR_CREATOR_NAME as [PR Creator Name], NEW_PR_STATUS as [Status],RELEASE_DATE as [Release Date], ' +
                    'GMP as [GMP],PURCHASE_GROUP_CODE as [Purchase Group Code], ' +
                    'PURCHASE_GROUP_NAME as [Purchase Group Name]' +

                    'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["PR Details.xlsx", $scope.prExcelReport]);
                $scope.downloadExcel = false;
            };

            $scope.selectedRfpWithItem = [];
            $scope.linkRFPDetails = {
                ITEM_CODE: '',
                ITEM_NAME: '',
                ITEM_TEXT: '',
                PROD_ID: '',
                PROD_NO: '',
                PROD_CODE: '',
                REQ_ID: '',
                PR_ID: '',
                PR_ITEM_ID: '',
                REQ_ITEM_ID: '',
                PRODUCT_ID: '',
                REQ_TITLE: ''
            };
            $scope.isValid = true;
            $scope.isValidToSelect = function () {
                if ($scope.selectedRfpWithItem && $scope.selectedRfpWithItem.length > 0) {
                    $scope.isValid = _.find($scope.selectedRfpWithItem, function (item) { return item.ITEM_CODE == $scope.linkRFPDetails.ITEM_CODE; }) ? false : true;
                    return $scope.isValid;
                }
            };

            var tempArr = [];
            $scope.prItemsNotUsedInRfp = [];
            $scope.addToTable = function () {
                if ($scope.selectedRfpWithItem && $scope.selectedRfpWithItem.length > 0) {
                    $scope.linkRFPDetails.REQ_ID = $scope.selectedRfpWithItem[0].REQ_ID;
                    $scope.linkRFPDetails.REQ_TITLE = $scope.selectedRfpWithItem[0].REQ_TITLE;
                }
                if ($scope.linkRFPDetails.PR_ITEM_ID == null || $scope.linkRFPDetails.PR_ITEM_ID == '') {
                    $scope.PR_ITEM_ID_VALIDATE = true;
                    return false;
                };
                if ($scope.linkRFPDetails.REQ_ID == null || $scope.linkRFPDetails.REQ_ID == '') {
                    $scope.REQ_ID_VALIDATE = true;
                    return false;
                };
                if ($scope.linkRFPDetails.PROD_ID == null || $scope.linkRFPDetails.PROD_ID == '') {
                    $scope.PROD_ID_VALIDATE = true;
                    return false;
                };
                if ($scope.isValid) {
                    if (!_.isEmpty($scope.linkRFPDetails)) {
                        tempArr.push($scope.linkRFPDetails);
                        $scope.selectedRfpWithItem = tempArr;
                        $scope.PRItems.forEach(function (prItem, prIndex) {
                            if (prItem.PRODUCT_ID == $scope.linkRFPDetails.PRODUCT_ID) {
                                if (!prItem.IS_SELECTED) {
                                    prItem.IS_SELECTED = true;
                                }
                            }
                        });
                        /*$scope.PRItems = [...new Set($scope.PRItems)];*/
                        $scope.linkRFPDetails = {};
                        $scope.linkRFPDetails.REQ_ID = $scope.selectedRfpWithItem[0].REQ_ID;
                        $scope.linkRFPDetails.REQ_TITLE = $scope.selectedRfpWithItem[0].REQ_TITLE;
                    }
                } else {
                    swal("Not Allowed", "Already in use.", "error");
                }
            };

            $scope.getStyles = function () {
                return 'width:100%;height: ' + angular.element('#prItemTable')[0].offsetHeight + 'px;max-height: 400px; overflow-y: auto;';
            };
            $scope.getStylesRfp = function () {
                return 'width:100%;height: ' + angular.element('#rfpTable')[0].offsetHeight + 'px;max-height: 200px; overflow-y: auto;';
            };
            $scope.getTrStyles = function () {
                return 'color:red;';
            };

            $scope.populateItemName = function (value) {
                $scope.PRItems.forEach(function (item, index) {
                    if (item.PRODUCT_ID == value) {
                        $scope.linkRFPDetails.PR_ITEM_ID = item.ITEM_ID;
                        $scope.linkRFPDetails.ITEM_CODE = item.ITEM_CODE;
                        $scope.linkRFPDetails.REQ_ID = item.REQ_ID;
                        $scope.linkRFPDetails.PR_ID = item.PR_ID;
                        $scope.linkRFPDetails.PRODUCT_ID = item.PRODUCT_ID;
                        $scope.linkRFPDetails.ITEM_NAME = item.ITEM_NAME;
                        $scope.linkRFPDetails.ITEM_TEXT = item.ITEM_TEXT;
                    }
                });
            };
            $scope.populateRFPTitle = function (value) {
                $scope.myAuctionsFiltred.forEach(function (item, index) {
                    if (item.requirementId == value) {
                        $scope.linkRFPDetails.REQ_TITLE = item.REQ_TITLE;
                        $scope.linkRFPDetails.REQ_ID = item.requirementId;
                        $scope.getNotUsedPRItems();
                    }
                });
            };
            $scope.populateRFPItems = function (value) {
                $scope.prItemsNotUsedInRfp.forEach(function (item, index) {
                    if (item.PROD_ID == value) {
                        $scope.linkRFPDetails.PROD_ID = item.PROD_ID;
                        $scope.linkRFPDetails.PROD_CODE = item.PROD_CODE;
                        $scope.linkRFPDetails.PROD_NO = item.PROD_NO;
                        $scope.linkRFPDetails.REQ_ITEM_ID = item.REQ_ITEM_ID;
                    }
                });
            };
            $scope.showSelectedPRItems = true;
            $scope.showRFPTables = true;
            $scope.fillValue = function (rfqObj, product) {
                if (rfqObj) {
                    product.search = rfqObj.REQ_TITLE;
                    product.RFQS = [];
                    product.REQ_ID = rfqObj.requirementId;
                    $scope.getNotUsedPRItems(rfqObj.requirementId, product);
                }
            };

            $scope.removeArr = function (sNo) {
                $scope.PRItems.forEach(function (prItem, prIndex) {
                    if (prItem.PRODUCT_ID == $scope.selectedRfpWithItem[sNo].PRODUCT_ID) {
                        prItem.IS_SELECTED = false;
                    }
                })
                $scope.selectedRfpWithItem.splice(sNo, 1);
            };

            $scope.emptyMessage = false;
            $scope.getNotUsedPRItems = function (rfqId, product) {
                product.REQ_DETAILS = [];
                var params = {
                    "prId": product.PR_ID,
                    "reqId": rfqId,
                    "sessionid": $scope.sessionID,
                };
                PRMPRServices.getNotUsedPRItems(params)
                    .then(function (response) {
                        if (response) {
                            product.REQ_DETAILS = response;
                            if ($scope.prItemsNotUsedInRfp.length <= 0) {
                                $scope.emptyMessage = true;
                            }
                        } else {
                            growlService.growl("Error in getting Items.", "inverse");
                        }
                    });
            };


            $scope.LinkToRFP = function () {
                if ($scope.PreviewItems && $scope.PreviewItems.length > 0) {
                    var params = {
                        "listLinkToRFP": $scope.PreviewItems,
                        "sessionid": $scope.sessionID,
                    };
                    PRMPRServices.linkRFPToPR(params)
                        .then(function (response) {
                            if (response && response.errorMessage === '') {
                                growlService.growl("Successfully link to RFP", "success");
                                angular.element('#linkRFP').modal('hide');
                                $scope.myAuctionsFiltred = [];
                                $scope.selectedRfpWithItem = [];
                                $scope.linkRFPDetails = {};
                            } else {
                                angular.element('#linkRFP').modal('hide');
                                $scope.selectedRfpWithItem = [];
                                $scope.linkRFPDetails = {};
                                growlService.growl("Error linking PR to RFP, please contact support team.", "inverse");
                            };
                        });
                }
            };

            $scope.PreviewItems = [];

            $scope.MapToPreview = function (product) {
                product.ERR_MESSAGE = '';
                var previewObj =
                {
                    "ITEM_NAME": product.ITEM_NAME,
                    "ITEM_TEXT": product.ITEM_TEXT,
                    "ITEM_CODE": product.ITEM_CODE,
                    "PROD_ID": product.RFP_PROD_ID,
                    "PROD_CODE": product.RFP_PROD_CODE,
                    "PROD_NO": product.RFP_PROD_NO,
                    "PROD_CODE_EMPTY": product.RFP_PROD_CODE_EMPTY,
                    "PROD_NO_EMPTY": product.RFP_PROD_NO_EMPTY,
                    "PR_PRODUCT_ID": product.PRODUCT_ID,
                    "RFP_PRODUCT_ID": product.SELECTED_RFP_PRODUCT_ID,
                    "PR_ID": product.PR_ID,
                    "PR_ITEM_ID": product.ITEM_ID,
                    "PRODUCT_ID": product.PRODUCT_ID,
                    "REQ_ID": product.REQ_ID,
                    "REQ_ITEM_ID": product.REQ_ITEM_ID
                };

                var itemFound = _.findIndex($scope.PreviewItems, function (item) { return item.RFP_PRODUCT_ID === previewObj.RFP_PRODUCT_ID });
                if (itemFound <= -1) {
                    $scope.PreviewItems.push(previewObj);
                    product.IS_DISABLE = true;
                } else {
                    product.ERR_MESSAGE = 'Item already mapped.';
                }
            };

            $scope.selectedRFPDetails = function (product)
            {
                product.RFP_PROD_CODE_EMPTY = '';
                product.RFP_PROD_NO_EMPTY = '';
                product.RFP_PROD_CODE_EMPTY = !_.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_CODE ? 'red' : '';
                product.RFP_PROD_NO_EMPTY = !_.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_NO ? 'red' : '';
                product.REQ_ITEM_ID = _.find(product.REQ_DETAILS, { PRODUCT_ID : product.SELECTED_RFP_PRODUCT_ID }).REQ_ITEM_ID;
                product.RFP_PROD_ID = _.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_ID;
                product.RFP_PROD_CODE = _.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_CODE ? _.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_CODE : 'Product code is not mapped in RFP creation.';
                product.RFP_PROD_NO = _.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_NO ? _.find(product.REQ_DETAILS, { PRODUCT_ID: product.SELECTED_RFP_PRODUCT_ID }).PROD_NO : 'Product no is not mapped in RFP creation.';
            };

            $scope.editItem = function (index, product) {
                product.ERR_MESSAGE = '';
                $scope.PRItemsTemp[index].IS_DISABLE = false;
                var index = _.findIndex($scope.PreviewItems, function (item) { return item.PR_PRODUCT_ID === product.PRODUCT_ID && item.RFP_PRODUCT_ID === product.SELECTED_RFP_PRODUCT_ID });
                if (index >= 0) {
                    $scope.PreviewItems.splice(index, 1);
                }
            };

            $scope.removeArr = function (index,obj) {
                $scope.PreviewItems.splice(index, 1);
                var itemFound = _.findIndex($scope.PRItemsTemp, function (item) { return item.PRODUCT_ID === obj.PR_PRODUCT_ID && item.SELECTED_RFP_PRODUCT_ID === obj.RFP_PRODUCT_ID });
                if (itemFound >= 0) {
                    $scope.PRItemsTemp[itemFound].IS_DISABLE = false;
                    $scope.PRItemsTemp[itemFound].ERR_MESSAGE = '';
                }
            };

        }]);