﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPRService
    {

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprlist?userid={userid}&sessionid={sessionid}&deptid={deptID}&desigid={desigID}&depttypeid={deptTypeID}&onlyopen={onlyopen}" +
            "&plant={plant}&projectType={projectType}&sectionHead={sectionHead}&wbsCode={wbsCode}&profitCentre={profitCentre}&purchaseCode={purchaseCode}&creatorName={creatorName}&clientName={clientName}&prStatus={prStatus}&search={search}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}&fromdate={fromdate}" +
            "&todate={todate}&datefilter={datefilter}")]
        List<PRDetails> GetPRList(int userid, string sessionid, int deptID, int desigID, int deptTypeID, int onlyopen,
            string plant, string projectType, string sectionHead, string wbsCode, string profitCentre, string purchaseCode, string creatorName, string clientName, string prStatus, string search,
            string fromdate, string todate, string datefilter, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetIndividualItems?userId={userId}&compId={compId}&sessionid={sessionid}&plant={plant}&projectType={projectType}&sectionHead={sectionHead}&wbsCode={wbsCode}&profitCentre={profitCentre}&purchaseCode={purchaseCode}&creatorName={creatorName}&clientName={clientName}&search={search}&PageSize={PageSize}&NumberOfRecords={NumberOfRecords}")]
        List<PRItems> GetIndividualItems(int userId, int compId, string sessionid, string plant, string projectType, string sectionHead, string wbsCode, string profitCentre, string purchaseCode, string creatorName, string clientName, string search, int PageSize = 0, int NumberOfRecords = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPRSbyItem?productId={productId}&userId={userId}&compId={compId}&sessionid={sessionid}&plant={plant}&projectType={projectType}&sectionHead={sectionHead}&wbsCode={wbsCode}&profitCentre={profitCentre}&search={search}")]
        List<PRDetails> GetPRSbyItem(int productId, int userId, int compId, string sessionid, string plant, string projectType, string sectionHead, string wbsCode, string profitCentre, string search);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getprdetails?prid={prid}&sessionid={sessionid}")]
        PRDetails GetPRDetails(int prid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetItemDetails?prIDS={prIDS}&sessionid={sessionID}")]
        List<PRItems> GetItemDetails(string prIDS, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getpritemslist?prid={prid}&sessionid={sessionid}")]
        List<PRItems> GetPRItemsList(int prid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
           ResponseFormat = WebMessageFormat.Json,
           UriTemplate = "getrequirementprstatus?reqid={reqid}&prid={prid}&sessionid={sessionid}")]
        RequirementPRStatus GetRequirementPRStatus(int reqid, int prid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getprfieldmapping?type={type}&sessionid={sessionid}")]
        List<PRFieldMapping> GetPRFieldMapping(string type, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getplantfieldmapping?type={type}&sessionid={sessionid}")]
        List<PRFieldMapping> GetPlantFieldMapping(string type, string sessionid);

        //[WebGet(RequestFormat = WebMessageFormat.Json,
        //    ResponseFormat = WebMessageFormat.Json,
        //    UriTemplate = "linktorfp?reqid={reqid}&prid={prid}&user={user}&sessionid={sessionid}")]
        //Response LinkToRFP(int reqid, int prid, int user, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveprdetails")]
        Response SavePRDetails(PRDetails prdetails, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savepractions")]
        Response SavePRActions(PRDetails prdetails, bool sendcommunication, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getseries?series={series}&seriestype={seriesType}&compid={compID}&deptid={deptID}&sessionid={sessionID}")]
        Response GetSeries(string series, string seriesType, string sessionID, int compID, int deptID);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getreqprlist?userid={userid}&sessionid={sessionid}")]
        List<PRDetails> GetReqPRList(int userid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "getcompanyrfqcreators?u_id={U_ID}&pr_id={PR_ID}&sessionid={sessionid}")]//&dept_id={dept_id}
        List<PRRFQCreator> GetCompanyRFQCreators(int U_ID, int PR_ID, string sessionid);//, int dept_id


        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "savecompanyrfqcreators")]
        Response SaveCompanyRFQCreators(List<PRRFQCreator> listPRRFQCreator, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getFilterValues?compid={compID}&sessionid={sessionID}")]
        List<PRFieldMapping> GetFilterValues(int compID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "getpritemsbyreq?reqid={reqid}&sessionid={sessionid}")]
        List<PRItems> GetPRItemsByReq(int reqid, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "linkRFPToPR")]
        Response LinkToRFP(List<LinkToRFP> listLinkToRFP, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "getNotUsedPRItems?prId={prId}&reqId={reqId}&sessionid={sessionid}")]
        List<LinkToRFP> GetNotUsedPrItems(int prId, int reqId, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRFPS?SEARCH={SEARCH}&COMP_ID={COMP_ID}&U_ID={U_ID}&sessionid={sessionID}")]
        List<RequirementLite> GetRFPS(string SEARCH, int COMP_ID, int U_ID, string sessionID);

    }
}
