﻿using PRM.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRMServices.Infrastructure
{
    public class WebWorkContext : IWorkContext
    {

        public WebWorkContext(
            HttpContextBase httpContextBase


            )
        {
            _httpContextBase = httpContextBase;
        }



        private readonly HttpContextBase _httpContextBase;
        private  int _cacheCompnay;
        private int _cacheUser;

        private  string GetCompany()
        {
            if (_httpContextBase == null || _httpContextBase.Request == null || _httpContextBase.Request.Headers["cid"] == "")
                return null;

            return _httpContextBase.Request.Headers["cid"];
        }
        private string GetUser()
        {
            if (_httpContextBase == null || _httpContextBase.Request == null || _httpContextBase.Request.Headers["uid"] == "")
                return null;

            return _httpContextBase.Request.Headers["uid"];
        }

        public int CurrentCompany
        {
            get
            {
                if(_cacheCompnay == 0)
                {
                    var companyId = GetCompany();
                    _cacheCompnay = Convert.ToInt32(companyId);
                }
                return _cacheCompnay;

            }

            set
            {
                _cacheCompnay = value;
            }
        }

        public int CurrentUser
        {
            get
            {
                if(_cacheUser == 0)
                {
                    var userId = GetCompany();
                    _cacheUser = Convert.ToInt32(userId);
                }

                return _cacheUser;
            }

            set
            {
                _cacheUser = value;
            }
        }
    }
}