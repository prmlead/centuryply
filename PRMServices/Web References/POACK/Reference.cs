﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace PRMServices.POACK {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.4084.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="SI_PO_ACKN_OBBinding", Namespace="urn:com:centurply:PRM360")]
    public partial class SI_PO_ACKN_OBService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback SI_PO_ACKN_OBOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public SI_PO_ACKN_OBService() {
            this.Url = global::PRMServices.Properties.Settings.Default.PRMServices_POACK_SI_PO_ACKN_OBService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event SI_PO_ACKN_OBCompletedEventHandler SI_PO_ACKN_OBCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://sap.com/xi/WebService/soap1.1", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlArrayAttribute("MT_PRM360_PO_ACKN_Response", Namespace="urn:com:centurply:PRM360")]
        [return: System.Xml.Serialization.XmlArrayItemAttribute("RETURN", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public DT_PRM360_PurchaseOrder_ResponseRETURN[] SI_PO_ACKN_OB([System.Xml.Serialization.XmlArrayAttribute(Namespace="urn:com:centurply:PRM360")] [System.Xml.Serialization.XmlArrayItemAttribute("PO_INFO", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)] DT_PRM360_PO_ACKNPO_INFO[] MT_PRM360_PO_ACKN_Request) {
            object[] results = this.Invoke("SI_PO_ACKN_OB", new object[] {
                        MT_PRM360_PO_ACKN_Request});
            return ((DT_PRM360_PurchaseOrder_ResponseRETURN[])(results[0]));
        }
        
        /// <remarks/>
        public void SI_PO_ACKN_OBAsync(DT_PRM360_PO_ACKNPO_INFO[] MT_PRM360_PO_ACKN_Request) {
            this.SI_PO_ACKN_OBAsync(MT_PRM360_PO_ACKN_Request, null);
        }
        
        /// <remarks/>
        public void SI_PO_ACKN_OBAsync(DT_PRM360_PO_ACKNPO_INFO[] MT_PRM360_PO_ACKN_Request, object userState) {
            if ((this.SI_PO_ACKN_OBOperationCompleted == null)) {
                this.SI_PO_ACKN_OBOperationCompleted = new System.Threading.SendOrPostCallback(this.OnSI_PO_ACKN_OBOperationCompleted);
            }
            this.InvokeAsync("SI_PO_ACKN_OB", new object[] {
                        MT_PRM360_PO_ACKN_Request}, this.SI_PO_ACKN_OBOperationCompleted, userState);
        }
        
        private void OnSI_PO_ACKN_OBOperationCompleted(object arg) {
            if ((this.SI_PO_ACKN_OBCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.SI_PO_ACKN_OBCompleted(this, new SI_PO_ACKN_OBCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:com:centurply:PRM360")]
    public partial class DT_PRM360_PO_ACKNPO_INFO {
        
        private string eBELNField;
        
        private string eBELPField;
        
        private string pOCONFIRMATIONField;
        
        private string cONF_SERField;
        
        private string cONF_TYPEField;
        
        private string dELIV_DATEField;
        
        private string dEL_DATCAT_EXTField;
        
        private string dISPO_RELField;
        
        private string qUANTITYField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EBELN {
            get {
                return this.eBELNField;
            }
            set {
                this.eBELNField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EBELP {
            get {
                return this.eBELPField;
            }
            set {
                this.eBELPField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string POCONFIRMATION {
            get {
                return this.pOCONFIRMATIONField;
            }
            set {
                this.pOCONFIRMATIONField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CONF_SER {
            get {
                return this.cONF_SERField;
            }
            set {
                this.cONF_SERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string CONF_TYPE {
            get {
                return this.cONF_TYPEField;
            }
            set {
                this.cONF_TYPEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string DELIV_DATE {
            get {
                return this.dELIV_DATEField;
            }
            set {
                this.dELIV_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string DEL_DATCAT_EXT {
            get {
                return this.dEL_DATCAT_EXTField;
            }
            set {
                this.dEL_DATCAT_EXTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string DISPO_REL {
            get {
                return this.dISPO_RELField;
            }
            set {
                this.dISPO_RELField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string QUANTITY {
            get {
                return this.qUANTITYField;
            }
            set {
                this.qUANTITYField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.4084.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:com:centurply:PRM360")]
    public partial class DT_PRM360_PurchaseOrder_ResponseRETURN {
        
        private string pREQ_NOField;
        
        private string eBELNField;
        
        private string gRN_NOField;
        
        private string mSGTYField;
        
        private string mESSAGEField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string PREQ_NO {
            get {
                return this.pREQ_NOField;
            }
            set {
                this.pREQ_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string EBELN {
            get {
                return this.eBELNField;
            }
            set {
                this.eBELNField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string GRN_NO {
            get {
                return this.gRN_NOField;
            }
            set {
                this.gRN_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MSGTY {
            get {
                return this.mSGTYField;
            }
            set {
                this.mSGTYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string MESSAGE {
            get {
                return this.mESSAGEField;
            }
            set {
                this.mESSAGEField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.4084.0")]
    public delegate void SI_PO_ACKN_OBCompletedEventHandler(object sender, SI_PO_ACKN_OBCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.4084.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SI_PO_ACKN_OBCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal SI_PO_ACKN_OBCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public DT_PRM360_PurchaseOrder_ResponseRETURN[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((DT_PRM360_PurchaseOrder_ResponseRETURN[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591