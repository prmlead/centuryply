﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('customfields', {
                    url: '/customfields',
                    templateUrl: 'custom-fields/views/custom-fields.html',
                    params: {
                        detailsObj: null
                    }
                }).state('savefield', {
                    url: '/savefield/:Id',
                    templateUrl: 'custom-fields/views/save-field.html',
                    params: {
                        detailsObj: null
                    }
                });
        }]);