﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
                .state('supplier', {
                    url: '/supplier',
                    templateUrl: 'Reports/views/supplier.html'
                })
                .state('savingsDashBoard', {
                    url: '/savingsDashBoard',
                    templateUrl: 'Reports/views/savingsDashBoard.html'
                })
                .state('contractSpend', {
                    url: '/contractSpend',
                    templateUrl: 'Reports/views/contractSpend.html'
                })
                .state('averageProcure', {
                    url: '/averageProcure',
                    templateUrl: 'Reports/views/averageProcure.html'
                })
                .state('purchaseTurnOver', {
                    url: '/purchaseTurnOver',
                    templateUrl: 'Reports/views/purchaseTurnOver.html'
                })
                .state('purchaserAnalysis', {
                    url: '/purchaserAnalysis',
                    templateUrl: 'Reports/views/purchaserAnalysis.html'
                })
                .state('utilizationReport', {
                    url: '/utilizationReport',
                    templateUrl: 'Reports/views/utilizationReport.html'
                })

        }]);