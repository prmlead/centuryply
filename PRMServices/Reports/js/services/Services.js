﻿prmApp.constant('PRMAnalysisServicesDomain', 'Reports/svc/PRMAnalysisService.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMAnalysisServices', ["PRMAnalysisServicesDomain", "userService", "httpServices", "$window",
    function (PRMAnalysisServicesDomain, userService, httpServices, $window) {


        var PRMAnalysisServices = this;

        PRMAnalysisServices.GetPurchaserAnalysis = function (params) {
            let url = PRMAnalysisServicesDomain + 'GetPurchaserAnalysis?userID=' + params.userID + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        
       
        PRMAnalysisServices.GetTemplates = function (name, domain, from, to) {
            //let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&userType=' + userType + '&sessionid=' + userService.getUserToken();
            let url = PRMAnalysisServicesDomain + 'GetTemplates?name=' + name + '&domain=' + domain + '&from=' + from + ' &to=' + to + '&sessionID=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", name + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }


        PRMAnalysisServices.GetReports = function (domain, from, to) {
            let url = PRMAnalysisServicesDomain + 'GetReports?domain=' + domain + '&from=' + from + '&to=' + to + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        
        return PRMAnalysisServices;

    }]);