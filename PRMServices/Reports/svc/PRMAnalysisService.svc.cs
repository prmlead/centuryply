﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;

using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;
using System.Xml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMAnalysisService : IPRMAnalysisService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        public PRMServices prm = new PRMServices();

        #region Services

        public PurchaserAnalysis GetPurchaserAnalysis(int userID, DateTime fromDate, DateTime toDate, string sessionid)
        {
            PurchaserAnalysis details = new PurchaserAnalysis();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                
                var dataset = sqlHelper.SelectList("cp_GetPurchaserAnalysis", sd);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        DataRow row = dataset.Tables[0].Rows[0];
                        details.USER_SAVINGS = row["PARAM_USER_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["PARAM_USER_SAVINGS"]) : 0;
                        details.TOTAL_VOLUME = row["PARAM_TOTAL_VOLUME"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_VOLUME"]) : 0;
                        details.TOTAL_MATERIAL = row["PARAM_TOTAL_MATERIAL"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_MATERIAL"]) : 0;
                        details.TOTAL_RFQ = row["PARAM_TOTAL_RFQ"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_RFQ"]) : 0;
                        details.TOTAL_VENDORS = row["PARAM_TOTAL_VENDORS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_VENDORS"]) : 0;
                        details.ADDED_VENDORS = row["PARAM_ADDED_VENDORS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_ADDED_VENDORS"]) : 0;
                        details.RFQ_TAT = row["RFQTURNAROUNDTIME"] != DBNull.Value ? Convert.ToDouble(row["RFQTURNAROUNDTIME"]) : 0;
                        details.QCS_TAT = row["QCSTURNAROUNDTIME"] != DBNull.Value ? Convert.ToDouble(row["QCSTURNAROUNDTIME"]) : 0;
                        details.TOTAL_ITEM_SUM = row["PARAM_TOTAL_ITEM_SUM"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_ITEM_SUM"]) : 0;
                    }

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public OpsReports GetReports(string domain, string from, string to, string sessionID)
        {
            OpsReports reports = new OpsReports();
            reports.ReqTransactions = new List<OpsRequirement>();
            reports.SubUsersRegistered = new List<OpsUserDetails>();
            reports.VendorsRegistered = new List<OpsUserDetails>();

            List<OpsRequirement> requirements = GetRegularRfqReport(domain, from, to, sessionID);
            foreach (OpsRequirement req in requirements)
            {
                reports.ReqTransactions.Add(req);
            }
            List<OpsUserDetails> subUsers = GetSubUserDataReport(domain, from, to, sessionID);
            foreach (OpsUserDetails U in subUsers)
            {
                reports.SubUsersRegistered.Add(U);
            }
            List<OpsUserDetails> vendors = GetVendorRegisteredReport(domain, from, to, sessionID);
            foreach (OpsUserDetails vend in vendors)
            {
                reports.VendorsRegistered.Add(vend);
            }
            return reports;
        }

        public List<OpsRequirement> GetRegularRfqReport(string domain, string from, string to, string sessionID)
        {
            List<OpsRequirement> requirements = new List<OpsRequirement>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                string body = GetOpsQueries("GetRegularRfqReport");
                body = String.Format(body, Int32.Parse(domain), from, to);
                string query = body;
                DataSet ds = sqlHelper.ExecuteQuery(query);

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsRequirement req = new OpsRequirement();

                    req.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    req.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    req.REQ_POSTED_ON = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]).AddMinutes(330) : DateTime.Now;
                    req.UserName = row["Posted_By"] != DBNull.Value ? Convert.ToString(row["Posted_By"]) : string.Empty;
                    req.CLOSED = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    req.CLOSED_EXCEL = NegotiationStatus("CUSTOMER", req.CLOSED);
                    req.VendorsInvited = row["VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_INVITED"]) : 0;
                    req.VendorsParticipated = row["VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_PARTICIPATED"]) : 0;
                    req.MinIbitialQuotationPrice = row["MIN_INITIAL_QUOTATION_PRICE"] != DBNull.Value ? Convert.ToDouble(row["MIN_INITIAL_QUOTATION_PRICE"]) : 0;
                    req.SAVINGS = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    req.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirements.Add(req);
                }
            }

            return requirements;
        }



        public string NegotiationStatus(string type, string status) {

            if (type == "CUSTOMER")
            {
                if (status == "UNCONFIRMED")
                {
                    status = "Quotations pending";
                }
                else if (status == "NOTSTARTED")
                {
                    status = "Negotiation scheduled";
                }
                else if (status == "STARTED")
                {
                    status = "Negotiation started";
                }
                else if (status == "Negotiation Ended")
                {
                    status = "Negotiation Closed";
                }
                else if (status == "DELETED")
                {
                    status = "Requirement Cancelled";
                }
                else if (status == "Qcs Pending")
                {
                    status = "QCS Pending";
                }
                else if (status == "Qcs Created")
                {
                    status = "QCS Created";
                }
                else if (status == "Qcs Approved")
                {
                    status = "QCS Approved";
                }
                else if (status == "Qcs Approval Pending")
                {
                    status = "QCS Approval Pending";
                }
                else if (status == "Qcs Rejected")
                {
                    status = "QCS Rejected";
                }
                else if (status == "Negotiation Completed")
                {
                    status = "Negotiation Completed";
                }
                else if (status == "Saved_As_Draft")
                {
                    status = "Saved_As_Draft";
                }
                else if (status == "Closed")
                {
                    status = "Closed";
                }
                else if (status == "Quotations Received")
                {
                    status = "Quotations Received";
                }
            }
            else if (type == "VENDOR")
            {
                if (status == "NEW")
                {
                    status = "NEW"; // / WIP
                }
                else if (status == "NOTSTARTED")
                {
                    status = "Negotiation scheduled";
                }
                else if (status == "STARTED")
                {
                    status = "Negotiation started";
                }
                else if (status == "Negotiation Ended")
                {
                    status = "Negotiation Closed";
                }
                else if (status == "DELETED")
                {
                    status = "Requirement Cancelled";
                }
                else if (status == "WIP")
                {
                    status = "WIP";
                }
                else if (status == "UNCONFIRMED")
                {
                    status = "NEW";
                }
            }

            return status;
        }

        public List<OpsUserDetails> GetSubUserDataReport(string domain, string from, string to, string sessionID)
        {
            List<OpsUserDetails> users = new List<OpsUserDetails>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                string body = GetOpsQueries("GetSubUserDataReport");
                body = String.Format(body, Int32.Parse(domain), from, to);
                string query = body;
                DataSet ds = sqlHelper.ExecuteQuery(query);


                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsUserDetails req = new OpsUserDetails();

                    var FNAME = "";
                    var LNAME = "";
                    FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    req.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    req.UserName = FNAME + " " + LNAME;
                    req.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    req.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    req.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                    users.Add(req);
                }
            }

            return users;
        }

        public List<OpsUserDetails> GetVendorRegisteredReport(string domain, string from, string to, string sessionID)
        {
            List<OpsUserDetails> users = new List<OpsUserDetails>();
            using (MySqlCommand cmd = new MySqlCommand())
            {

                string body = GetOpsQueries("GetVendorRegisteredReport");
                body = String.Format(body, Int32.Parse(domain), from, to);
                string query = body;
                DataSet ds = sqlHelper.ExecuteQuery(query);


                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    OpsUserDetails req = new OpsUserDetails();

                    var FNAME = "";
                    var LNAME = "";
                    FNAME = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    req.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    req.UserName = FNAME + " " + LNAME;
                    req.U_EMAIL = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    req.U_PHONE = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    req.DateCreated = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.Now;
                    req.CreatedByName = row["CREATED_BY_NAME"] != DBNull.Value ? Convert.ToString(row["CREATED_BY_NAME"]) : string.Empty;

                    users.Add(req);
                }
            }

            return users;
        }

        public string GetOpsQueries(string TemplateName)
        {
            string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/OpsDataQueries.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;

            return body;
        }


        public string GetTemplates(string name, string domain, string from, string to, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Report
            if (name.ToUpper().Contains("REG_RFQ_REPORT"))
            {
                List<OpsRequirement> requirement = new List<OpsRequirement>();
                List<OpsRequirement> requirementList = new List<OpsRequirement>();
                
                requirement = GetRegularRfqReport(domain, from, to, sessionID);
                foreach (OpsRequirement R in requirement)
                {
                    requirementList.Add(R);
                }
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("TRANSACTION DATA");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "RFQ TRANSACTION REPORT";
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Req Title";
                wsSheet1.Cells["C" + lineNumber].Value = "Created Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Created By";
                wsSheet1.Cells["E" + lineNumber].Value = "Req Status";
                wsSheet1.Cells["F" + lineNumber].Value = "Vendors Invited";
                wsSheet1.Cells["G" + lineNumber].Value = "Vendors Participated";
                wsSheet1.Cells["H" + lineNumber].Value = "Min Initial Quotation Price";
                wsSheet1.Cells["I" + lineNumber].Value = "Savings";




                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":I" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:I"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;


                foreach (OpsRequirement req in requirementList)
                {


                    wsSheet1.Cells["A" + lineNumber].Value = req.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = req.Title;
                    wsSheet1.Cells["C" + lineNumber].Value = Convert.ToString(req.REQ_POSTED_ON);
                    wsSheet1.Cells["D" + lineNumber].Value = req.UserName;
                    wsSheet1.Cells["E" + lineNumber].Value = req.CLOSED_EXCEL;
                    wsSheet1.Cells["F" + lineNumber].Value = req.VendorsInvited;
                    wsSheet1.Cells["G" + lineNumber].Value = req.VendorsParticipated;
                    wsSheet1.Cells["H" + lineNumber].Value = req.MinIbitialQuotationPrice;
                    wsSheet1.Cells["I" + lineNumber].Value = req.SAVINGS;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;
                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }

            }

            if (name.ToUpper().Contains("SUB_USERS"))
            {
                List<OpsUserDetails> SubUsers = new List<OpsUserDetails>();
                List<OpsUserDetails> SubUsersList = new List<OpsUserDetails>();
                SubUsers = GetSubUserDataReport(domain, from, to, sessionID);
                foreach (OpsUserDetails sub in SubUsers)
                {
                    SubUsersList.Add(sub);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("SUB USERS");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "SUB USERS";
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                wsSheet1.Cells["B" + lineNumber].Value = "User";
                wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                wsSheet1.Cells["F" + lineNumber].Value = "Created By";

                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                wsSheet1.Cells["A:G"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (OpsUserDetails user in SubUsersList)
                {
                    var FNAME = "";
                    var LNAME = "";
                    //FNAME = user.U_LNAME
                    //LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    wsSheet1.Cells["A" + lineNumber].Value = user.U_ID;
                    wsSheet1.Cells["B" + lineNumber].Value = user.UserName;
                    wsSheet1.Cells["C" + lineNumber].Value = user.U_EMAIL;
                    wsSheet1.Cells["D" + lineNumber].Value = user.U_PHONE;
                    wsSheet1.Cells["E" + lineNumber].Value = user.DateCreated.ToString();
                    wsSheet1.Cells["F" + lineNumber].Value = user.CreatedByName;

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;
                }
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (name.ToUpper().Contains("VENDORS_REGISTERED"))
            {
                List<OpsUserDetails> Vendors = new List<OpsUserDetails>();
                List<OpsUserDetails> VendorsList = new List<OpsUserDetails>();
                Vendors = GetVendorRegisteredReport(domain, from, to, sessionID);
                foreach (OpsUserDetails vend in Vendors)
                {
                    VendorsList.Add(vend);
                }

                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("VENDORS REGISTERED");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "VENDORS REGISTERED";
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));

                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                wsSheet1.Cells["B" + lineNumber].Value = "User";
                wsSheet1.Cells["C" + lineNumber].Value = "E-Mail";
                wsSheet1.Cells["D" + lineNumber].Value = "Phone";
                wsSheet1.Cells["E" + lineNumber].Value = "Created On";
                wsSheet1.Cells["F" + lineNumber].Value = "Created By";

                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":F" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                wsSheet1.Cells["A:G"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (OpsUserDetails user in VendorsList)
                {
                    var FNAME = "";
                    var LNAME = "";
                    //FNAME = user.U_LNAME
                    //LNAME = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    wsSheet1.Cells["A" + lineNumber].Value = user.U_ID;
                    wsSheet1.Cells["B" + lineNumber].Value = user.UserName;
                    wsSheet1.Cells["C" + lineNumber].Value = user.U_EMAIL;
                    wsSheet1.Cells["D" + lineNumber].Value = user.U_PHONE;
                    wsSheet1.Cells["E" + lineNumber].Value = user.DateCreated.ToString();
                    wsSheet1.Cells["F" + lineNumber].Value = user.CreatedByName;

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;
                }
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Report
            var str = Convert.ToBase64String(ms.ToArray());
            return Convert.ToBase64String(ms.ToArray());
        }




        #endregion Services


    }

}