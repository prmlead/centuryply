﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMAnalysisService
    {

        

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetPurchaserAnalysis?userID={userID}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        PurchaserAnalysis GetPurchaserAnalysis(int userID, DateTime fromDate, DateTime toDate, string sessionid);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetReports?domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        OpsReports GetReports(string domain, string from, string to, string sessionID);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetTemplates?name={name}&domain={domain}&from={from}&to={to}&sessionID={sessionID}")]
        string GetTemplates(string name, string domain, string from, string to, string sessionID);









    }
}
