angular.module('techevalModule')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/techeval-home');

    $stateProvider
        .state('techeval-home', {
            url: '/techeval-home',
            templateUrl: '/partials/techeval-home.html',
            controller: 'techevalCtrl'
        })
}]);