angular = require('angular');
require('angular-ui-router');
require('loginmodule');
require('../dist/templateCachePartials');


var app = angular.module('commonModule', ['ui.router', 'commonPartials', 'loginModule']);

require('./config/index.js');
require('./services/index.js');
//require('./directives/index.js');
require('./controllers/index.js');