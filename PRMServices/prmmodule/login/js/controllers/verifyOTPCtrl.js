angular = require('angular');

angular.module('loginModule')
    .controller('verifyOTPCtrl', ["$scope", "$state", "$stateParams", "loginService", function LoginCtrl($scope, $state, $stateParams, loginService) {
        'use strict';
        $scope.verifyOTP = function () {
            $scope.otpvalue = $scope.otpobj.otp;
            $scope.otpvalue.phone = $stateParams.phoneNum;
            // if ($scope.otpvalue == "") {
            //     $scope.otpvalueValidation = true;
            //     $scope.otpvalueValidationEmpty = true;
            // } else {
            //     $scope.otpvalueValidationEmpty = false;
            //     $scope.otpvalueValidation = false;
            // }
            // if (isNaN($scope.otpvalue)) {
            //     $scope.otpvalueValidationError = true;
            //     $scope.otpvalueValidation = true;
            // } else {
            //     $scope.otpvalueValidationError = false;
            //     $scope.otpvalueValidation = false;
            // }
            // if (!$scope.otpvalueValidation) {
            loginService.verifyOTP($scope.otpvalue)
                .then(function (response) {
                    if (response.errorMessage == "") {
                        if (response.userInfo.isOTPVerified == 1) {
                            $scope.isOTPVerified = 1;

                            swal("Done!", "Mobile OTP Verified successfully.", "success");
                            $state.go('profile-home');
                        } else {
                            $scope.isOTPVerified = 0;
                            swal("Warning", "Please enter valid OTP", "warning");
                        }
                    } else {
                        swal("Warning", response.errorMessage, "warning");
                    }
                });
            //}
        };
    }]);
