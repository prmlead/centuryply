angular = require('angular');

angular.module('loginModule')
.service('growlService',[ 'notify', function(notify){
    var gs = {};
    gs.growl = function(message, type) {
        function onClick () {
        ////console.log('clicked notification!')
        }

        notify.show(message, 'img/logos/acadslogo_black.png', 5000, onClick);

        //growl('5 new messages', { sticky: true })

        // $.growl({
        //     message: message
        // },{
        //     type: type,
        //     allow_dismiss: false,
        //     label: 'Cancel',
        //     className: 'btn-xs btn-inverse',
        //     placement: {
        //         from: 'top',
        //         align: 'right'
        //     },
        //     delay: 2500,
        //     animate: {
        //             enter: 'animated bounceIn',
        //             exit: 'animated bounceOut'
        //     },
        //     offset: {
        //         x: 20,
        //         y: 85
        //     }
        // });
    }
    
    return gs;
}]);