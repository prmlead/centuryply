angular = require('angular');

angular.module('profileModule')
.controller('profilePasswordCtrl', ["$scope", "growlService", "loginService", "profileService", function ($scope, growlService, loginService, profileService) {

	$scope.pwdObj = {
		username: loginService.getUserObj().username,
		oldPass: '',
		newPass: ''
	};

	$scope.updatePwd = function () {
	    if ($scope.pwdObj.newPass.length < 6) {
	        growlService.growl("Password should be at least 6 characters long.", "inverse");
	        return false;
	    } else if ($scope.pwdObj.newPass != $scope.pwdObj.confirmNewPass) {
	        growlService.growl('Passwords do not match, please enter the same password in both new and Confirm Password fields', 'inverse');
	        return false;
	    }
	    $scope.pwdObj.userID = parseInt(loginService.getUserId());
	    profileService.updatePassword($scope.pwdObj)
			.then(function (response) {
			    if (response.errorMessage == "") {
			        $scope.pwdObj = {
			            username: loginService.getUserObj().username
			        };
			        swal("Done!", 'Your password has been successfully updated.', 'success');
			    } else {
			        swal("Error!", response.errorMessage, 'error');
			    }
			})
	}
}]);
		