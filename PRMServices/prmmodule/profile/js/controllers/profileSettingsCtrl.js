angular = require('angular');

angular.module('profileModule')
.controller('profileSettingsCtrl', ["$scope", "$http", "domain", "$log", "growlService", "loginService", "profileService", function ($scope, $http, domain, $log, growlService, loginService, profileService)
{

    $scope.userStatus ='';

    $scope.userDetails = {
            achievements: "",
            assocWithOEM: false,
            clients: "",
            establishedDate: "01-01-1970",
            aboutUs: "",
            logoFile: "",
            logoURL: "",
            products: "",
            strengths: "",
            responseTime: "",
            oemCompanyName: "",
            oemKnownSince: "",
            workingHours: "",
            files: [],
            directors: "",
            address: "",
            dateshow: 0

        };

		$scope.NegotiationSettings = [];
		
		$scope.days = 0;
		$scope.hours = 0;
		$scope.mins = 0;
		 
        $scope.NegotiationSettingsValidationMessage = '';
		  
        $scope.isShow = false;
		 
        $scope.callGetUserDetails = function () {
            $log.info("IN GET USER DETAILS");
            profileService.getProfileDetails({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
                .then(function (response) {
                    $scope.userStatus = "registered";
                    if (response != undefined) {
                        $scope.userDetails = response;

                        $scope.NegotiationSettings = $scope.userDetails.NegotiationSettings;
                        var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                        $scope.days = parseInt(duration[0]);
                        duration = duration[1];
                        duration = duration.split(":", 4);
                        $scope.hours = parseInt(duration[0]);
                        $scope.mins = parseInt(duration[1]); 
                    }
                });
        }

        $scope.callGetUserDetails();
		
	    $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {
        $scope.NegotiationSettingsValidationMessage = '';

        if (minReduction < 10 || minReduction == undefined) {
            $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Min.Amount reduction';
            return;
        }

        if (rankComparision < 10 || rankComparision == undefined) {
            $scope.NegotiationSettingsValidationMessage = 'Set Min 10 for Rank Comparision price';
            return;
        }

        if (minReduction >= 10 && rankComparision > minReduction) {
            $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
            return;
        }
        if (days == undefined || days < 0) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
            return;
        }
        if (hours < 0) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
            return;
        }
        if (mins < 0) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
            return;
        }
        if (mins > 60 || mins == undefined) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minunts';
            return;
        }
        if (hours > 24 || hours == undefined) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
            return;
        }
        if (hours == 24 && mins > 0) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minuts';
            return;
        }
        if (mins < 5 && hours == 0 && days == 0) {
            $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minunts';
            return;
        }
    }
		
	$scope.saveNegotiationSettings = function () {
    $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);

    if ($scope.NegotiationSettingsValidationMessage == '') {
        $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
        var params = {
            "NegotiationSettings": {
                "userID": loginService.getUserId(),
                "minReductionAmount": $scope.NegotiationSettings.minReductionAmount,
                "rankComparision": $scope.NegotiationSettings.rankComparision,
                "negotiationDuration": $scope.NegotiationSettings.negotiationDuration,
                "sessionID": loginService.getUserToken()
            }
        };
        $http({
            method: 'POST',
            url: domain + 'savenegotiationsettings',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
                $scope.NegotiationSettings = response.data;
                $scope.isShow = false; 
                window.location.reload();               
                growlService.growl('Successfully Saved', 'success');                
            }
            else {
                growlService.growl('Not Saved', 'inverse');
            }
        });
    }
}

    $scope.isShowFunction = function (value) {            
        if (value == true) {
            $scope.callGetUserDetails();
            $scope.isShow = true;
        }
        if (value == false) {
            $scope.callGetUserDetails();
            $scope.isShow = false;                
        }
        
    }
	  
}]);
	  
        