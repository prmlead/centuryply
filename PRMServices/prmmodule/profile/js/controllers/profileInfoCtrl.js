angular = require('angular');

angular.module('profileModule')
.controller('profileInfoCtrl', ["$scope", "$state", "$http", "domain", "$filter", "growlService", "fileReader", "loginService", "profileService", function ($scope, $state, $http, domain, $filter, growlService, fileReader, loginService, profileService) {	

	$scope.userDetails = {
		achievements: "",
		assocWithOEM: false,
		clients: "",
		establishedDate: "01-01-1970",
		aboutUs: "",
		logoFile: "",
		logoURL: "",
		products: "",
		strengths: "",
		responseTime: "",
		oemCompanyName: "",
		oemKnownSince: "",
		workingHours: "",
		files: [],
		directors: "",
		address: "",
		dateshow: 0
	};
	
	$scope.userStatus = '';
	
	$scope.editPro = 0;
	$scope.imagefilesonlyforlogo = false;
	
	$scope.logoFile = { "fileName": '', 'fileStream': "" };

	$scope.userObj = {};
	
	profileService.getUserDataNoCache()
	.then(function(response){
		$scope.userObj = response;
	})

	$scope.editMode = function (value) {
		$scope.editPro = value;
	}

	$scope.stateChangeFunction = function()
    {
        $state.go('profile-home');
        window.location.reload;
    }
		
	$scope.callGetUserDetails = function () {
		profileService.getProfileDetails({ "userid": loginService.getUserId(), "sessionid": loginService.getUserToken() })
			.then(function (response) {
				$scope.userStatus = "registered";
				if (response != undefined) {
					$scope.userDetails = response;

					var data = response.establishedDate;
					var date = new Date(parseInt(data.substr(6)));
					$scope.userDetails.establishedDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
					
					var today = new Date();
					var todayDate = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
					$scope.userDetails.dateshow = 0;
					
					if($scope.userDetails.establishedDate == todayDate){
						$scope.userDetails.dateshow = 1;
					}
					
					$http({
						method: 'GET',
						url: domain + 'getcategories?userid=' + loginService.getUserId() + '&sessionid=' + loginService.getUserToken(),
						encodeURI: true,
						headers: { 'Content-Type': 'application/json' }
					}).then(function (response) {
						if (response && response.data) {
							if (response.data.length > 0) {
								$scope.totalSubcats = _.filter(response.data, ['category', $scope.userDetails.category]);
								//$scope.totalSubcats = $filter('filter')(response.data, { category: $scope.userDetails.category });
								$scope.selectedCurrency = _.filter($scope.currencies, ['value', response.currency]);
								//$scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
								$scope.selectedCurrency = $scope.selectedCurrency[0];
								if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
									for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
										for (j = 0; j < $scope.totalSubcats.length; j++) {
											if ($scope.userDetails.subcategories[i].id == $scope.totalSubcats[j].id) {
												$scope.totalSubcats[j].ticked = true;
											}
										}
									}
								}
							}
						} else {
							//console.log(response.data[0].errorMessage);
						}
					}, function (result) {
						//console.log("there is no current auctions");
					});
					if ($scope.userDetails.subcategories && $scope.userDetails.subcategories.length > 0) {
						for (i = 0; i < $scope.userDetails.subcategories.length; i++) {
							$scope.subcategories += $scope.userDetails.subcategories[i].subcategory + ";";
						}
					}						
				}
			});       
        }
		
		$scope.updateUserInfo = function () {                      
            var params = {};
            if($scope.userDetails.assocWithOEM){
                if($scope.userDetails.oemCompanyName == "" || $scope.userDetails.oemKnownSince == "" || $scope.userDetails.assocWithOEMFileName == ""){
                    growlService.growl("If Associated with OEM, please provide further details.", "inverse");
                    return false;
                }
            }
            
            if($scope.editPro == 1 && $scope.userDetails.aboutUs == ""){
                growlService.growl("Please update your about us section", "inverse");
                return false;
            }

            var ts = moment($scope.userDetails.establishedDate, "DD-MM-YYYY").valueOf();
            var m = moment(ts);
            var auctionStartDate = new Date(m);
            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
            $scope.userDetails.establishedDate = "/Date(" + milliseconds + "000+0530)/";

            params = $scope.userDetails;
            
            if($scope.logoFile != '' && $scope.logoFile != null){
                params.logoFile = $scope.logoFile;
                params.logoURL += $scope.logoFile.fileName;
            }
            params.sessionID = loginService.getUserToken();
            params.userID = loginService.getUserId();
            params.errorMessage = "";
            params.subcategories = $scope.userDetails.subcategories;

			var params1 = {
				"user": {
					"userID": params.userID,
					"sessionID": params.sessionID,
					"isOTPVerified": params.isOTPVerified,
					"credentialsVerified": params.credentialsVerified,
					"errorMessage": params.errorMessage,
					"logoFile": params.logoFile ? params.logoFile : null,
					"logoURL": params.logoURL ? params.logoURL : "",
					"aboutUs": params.aboutUs ? params.aboutUs : "",
					"achievements": params.achievements ? params.achievements : "",
					"assocWithOEM": params.assocWithOEM ? params.assocWithOEM : false ,
					"assocWithOEMFile": params.assocWithOEMFile ? params.assocWithOEMFile : null ,                
					"assocWithOEMFileName": params.assocWithOEMFileName ? params.assocWithOEMFileName : "" ,                
					"clients": params.clients ? params.clients : "",
					"establishedDate": ("establishedDate" in params) ? params.establishedDate :'/Date(634334872000+0000)/',
					"products": params.products ? params.products : "",
					"strengths": params.strengths ? params.strengths : "",
					"responseTime": params.responseTime ? params.responseTime : "",
					"oemCompanyName": params.oemCompanyName ? params.oemCompanyName : "",
					"oemKnownSince": params.oemKnownSince? params.oemKnownSince : "",
					"files": [],
					"profileFile": params.profileFile ? params.profileFile : null ,                
					"profileFileName": params.profileFileName ? params.profileFileName : "",
					"workingHours": params.workingHours ? params.workingHours : "",
					"directors": params.directors ? params.directors : "",
					"address": params.address ? params.address : "",
					"subcategories": params.subcategories
				}
			}       

            profileService.updateUserProfileInfo(params1)
            .then(function (response) {
                if(response.toLowerCase().indexOf('already exists') > 0){
                    profileService.getUserDataNoCache().then(function(response){
                    
                    var loginUserData = loginService.getUserObj();                                                        
                    $scope.userObj = loginUserData;
                    $scope.callGetUserDetails();
                    $state.reload();
                });
                }
                $state.reload();
            });
            
            
            $scope.editPro = 0;
            $scope.imagefilesonlyforlogo = false;
            $scope.callGetUserDetails();            
        };
		
	$scope.callGetUserDetails();
		
	$scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "storeLogo") 
					{
                        var bytearray = new Uint8Array(result);
                        $scope.logoFile.fileStream = $.makeArray(bytearray);
                        $scope.logoFile.fileName = $scope.file.name;
                    }                    
                    else 
					{
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.assocWithOEMFile = $.makeArray(bytearray);                        
                        $scope.userDetails.assocWithOEMFileName = $scope.file.name;
                    }                   
                });
        };
		
}]);