﻿using System;
using System.Data;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using OfficeOpenXml;
using OfficeOpenXml.DataValidation;
using System.IO;
using System.Linq;
using System.Drawing;
using OfficeOpenXml.Style;
using PRMServices.SQLHelper;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;
using CATALOG = PRMServices.Models.Catalog;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using PRMServices.Models.Catalog;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMReportService : IPRMReportService
    {
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        private PRMServices prmservices = new PRMServices();
        private PRMWFService WF = new PRMWFService();
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #region Get

        public LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (count <= 0)
            {
                count = 10;
            }

            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_COUNT", count);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject(row, count);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public LiveBidding[] GetLiveBiddingReport2(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LiveBidding> details = new List<LiveBidding>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                ReportsRequirement reportsrequirement = new ReportsRequirement();
                reportsrequirement = GetReqDetails(reqID, sessionID);
                TimeSpan duration = reportsrequirement.EndTime.Subtract(reportsrequirement.StartTime);
                int interval = 0;
                for (int a = 10; a < duration.Minutes; a = a + 10)
                {
                    interval = duration.Minutes - 10;
                }

                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetLiveBiddingReport2", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LiveBidding detail = ReportUtility.GetLiveBiddingReportObject2(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                LiveBidding error = new LiveBidding();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ItemWiseReport> details = new List<ItemWiseReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetItemWiseReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ItemWiseReport detail = ReportUtility.GetItemWiseReportObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                ItemWiseReport error = new ItemWiseReport();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetDeliveryTimeLineReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID)
        {
            List<DeliveryTimeLine> details = new List<DeliveryTimeLine>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetPaymentTermsReport", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DeliveryTimeLine detail = ReportUtility.GetIDeliveryTimelineObject(row);
                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                details.Clear();
                DeliveryTimeLine error = new DeliveryTimeLine();
                error.ErrorMessage = ex.Message;
                details.Add(error);
            }

            return details.ToArray();
        }

        public ReportsRequirement GetReqDetails(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ReportsRequirement reportsrequirement = new ReportsRequirement();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqDetails", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    reportsrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    reportsrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    reportsrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    reportsrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    reportsrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    reportsrequirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    reportsrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    reportsrequirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    reportsrequirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    reportsrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_INVITED"]) : 0;
                    reportsrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                }
            }
            catch (Exception ex)
            {

            }

            return reportsrequirement;
        }

        public string GetTemplates(string template, int compID, int userID, int reqID,string fromDate,string toDate, string sessionID)
        {
            PRMServices service = new PRMServices();
            int maxRows = 500;
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region addvendors
            if (template.ToLower().Contains("addvendors"))
            {
                List<CategoryObj> categories = service.GetCategories(userID);
                var uniqueCategories = categories.Select(o => o.Category).Distinct().Where(o=>o.Trim()!=string.Empty).ToList();
                List<string> currencies = new List<string>(new string[] { "INR", "USD", "JPY", "GBP", "CAD", "AUD", "HKD", "EUR", "CNY" });
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("AddVendor");
                wsSheet1.Cells["A1"].Value = "FirstName";
                wsSheet1.Cells["B1"].Value = "LasName";
                wsSheet1.Cells["C1"].Value = "Email";
                wsSheet1.Cells["D1"].Value = "PhoneNumber";
                wsSheet1.Cells["E1"].Value = "CompanyName";
                wsSheet1.Cells["F1"].Value = "Category";
                wsSheet1.Cells["G1"].Value = "Currency";
                wsSheet1.Cells["H1"].Value = "KnownSince";
                var validation = wsSheet1.DataValidations.AddListValidation("G2:G100000");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Currency";
                validation.Error = "Invalid currency selected";
                foreach(var currency in currencies)
                {
                    validation.Formula.Values.Add(currency);
                }

                var catValidation = wsSheet1.DataValidations.AddListValidation("F2:F100000");
                catValidation.ShowErrorMessage = true;
                catValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                catValidation.ErrorTitle = "Invalid Category";
                catValidation.Error = "Invalid category entered";
                int count = 1;
                foreach (var cat in uniqueCategories)
                {
                    wsSheet1.Cells["AA" + count.ToString()].Value = cat;
                    count++;
                }

                catValidation.Formula.ExcelFormula = "AA1:AA" + (count-1).ToString();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }                    
            }
            #endregion

            #region vendorquotation
            if (template.ToUpper().Contains("MARGIN_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";                
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors) {

                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);                  
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);

                    string vendorRemarks = "";

                    foreach (var vendor in entry.Value.AuctionVendors) {
                        if (vendor.VendorID == entry.Key) {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems) {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString();
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region revvendorquotation
            if (template.ToUpper().Contains("MARGIN_REV_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("MARGIN_REV_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";

                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;                
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    Requirement req = service.GetRequirementData(entry.Value.RequirementID, entry.Key, sessionID);
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["L" + index].Value = item.ItemID;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["M" + index].Value = 0;
                        wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["P" + index].Value = vendorRemarks;
                        wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                        wsSheet1.Cells["R" + index].Value = req.Title;
                        wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                        wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                        var costPrice = 100 * item.UnitMRP / (100 + gst) * (1 + (item.UnitDiscount / 100));
                        wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region unitprice
            #region vendorquotation
            if (template.ToUpper().Contains("UNIT_PRICE_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRICE";
                wsSheet1.Cells["D1"].Value = "GST";
                wsSheet1.Cells["E1"].Value = "FREIGHT";
                wsSheet1.Cells["F1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["G1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["H1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["I1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["J1"].Value = "PRODUCT_NO";
                wsSheet1.Cells["K1"].Value = "BRAND";
                wsSheet1.Cells["L1"].Value = "QUANTITY";
                wsSheet1.Cells["M1"].Value = "UNITS";
                wsSheet1.Cells["N1"].Value = "DESCRIPTION";
                wsSheet1.Cells["O1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["F2:F6000"].Formula = "IF(A2 > 0, L2*C2*(1+(D2/100)), \"\")";
                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["D" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["E" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["F" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["G" + index].Value = 0;
                        wsSheet1.Cells["H" + index].Value = item.ItemID;
                        wsSheet1.Cells["I" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["J" + index].Value = item.ProductNo;
                        wsSheet1.Cells["K" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["L" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["M" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["N" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["O" + index].Value = item.RequirementID;
                        index++;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            if (template.ToUpper().Contains("UNIT_QUOTATION"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_QUOTATION_"+ reqID);
                List<string> gstVals = new List<string>(new string[] { "0", "2.5", "6", "9", "14"});
                List<string> IgstVals = new List<string>(new string[] { "0", "5", "12", "18", "28"});

                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["D1"].Value = "ProductName";
                wsSheet1.Cells["E1"].Value = "ProductNumber";
                wsSheet1.Cells["F1"].Value = "HsnCode";
                wsSheet1.Cells["G1"].Value = "Description";
                //wsSheet1.Cells["H1"].Value = "OtherBrands";
                wsSheet1.Cells["H1"].Value = "QTY";
                wsSheet1.Cells["I1"].Value = "Units";
                wsSheet1.Cells["J1"].Value = "Make";
                wsSheet1.Cells["K1"].Value = "IsRegret";
                wsSheet1.Cells["L1"].Value = "RegretComments";
                wsSheet1.Cells["M1"].Value = "Remarks";
                wsSheet1.Cells["N1"].Value = "PRICE";
                wsSheet1.Cells["O1"].Value = "SGST";
                wsSheet1.Cells["P1"].Value = "CGST";
                wsSheet1.Cells["Q1"].Value = "IGST";
                wsSheet1.Cells["R1"].Value = "TOTAL_PRICE";
                wsSheet1.Cells["S1"].Value = "DeliveryLocation";
                
                wsSheet1.Cells["T1"].Value = "REQUIREMENT_ID";
                //wsSheet1.Cells["U1"].Value = "DeliveryDate";
                int maxRows1 = 0;
                maxRows1 = vendorsDictionary[userID].ListRequirementItems.Count + 1;

                wsSheet1.Cells[$"R2:R{maxRows}"].Formula = "IF(A2 > 0, N2*H2*(1+((O2 + P2)/100)), \"\")";
           
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#377BA0");
                wsSheet1.Cells["A1:T1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["A1:T1"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["A1:T1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells["A1:T1"].Style.Font.Bold = true;
                wsSheet1.Cells["A1:T1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A1:T1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 40;
                wsSheet1.Cells["A:T"].AutoFitColumns();
                wsSheet1.Cells["A:T"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:T"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:T"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:T"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
               

                // wsSheet1.Cells[$"I2:I{maxRows1}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                // wsSheet1.Cells[$"I2:I{maxRows1}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                //wsSheet1.Cells[$"U2:U{maxRows}"].Style.Numberformat.Format = "dd-MM-yyyy HH:mm";

                //var DeliveryDateValidation = wsSheet1.DataValidations.AddDateTimeValidation($"U2:U{maxRows}");//daliverydate
                //DeliveryDateValidation.ShowErrorMessage = true;
                //DeliveryDateValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                //DeliveryDateValidation.ErrorTitle = "Invalid Date";
                //DeliveryDateValidation.Error = "Enter Valid Delivery date.";
                //DeliveryDateValidation.Formula.Value = DateTime.Now;
                //DeliveryDateValidation.Operator = ExcelDataValidationOperator.notEqual;

                var unitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"N2:N{maxRows}");//PRICE
                unitPriceValidation.ShowErrorMessage = true;
                unitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                unitPriceValidation.ErrorTitle = "Invalid Unit Price";
                unitPriceValidation.Error = "Enter Value greater than zero.";
                unitPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                unitPriceValidation.Formula.Value = 0;

                var sgstValidation = wsSheet1.DataValidations.AddListValidation($"O2:O{maxRows}");//SGST
                sgstValidation.ShowErrorMessage = true;
                sgstValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                sgstValidation.ErrorTitle = "Invalid SGST";
                sgstValidation.Error = "Invalid SGST value should 0, 2.5, 6, 9, 14.";
                //sgstValidation.Operator = ExcelDataValidationOperator.between;
                //sgstValidation.Formula.Value = 0;
                //sgstValidation.Formula2.Value = 99;

                foreach (var gst in gstVals)
                {
                    sgstValidation.Formula.Values.Add(gst);
                }

                var cgstValidation = wsSheet1.DataValidations.AddListValidation($"P2:P{maxRows}");//CGST
                cgstValidation.ShowErrorMessage = true;
                cgstValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                cgstValidation.ErrorTitle = "Invalid CGST";
                cgstValidation.Error = "Invalid CGST value should 0, 2.5, 6, 9, 14.";
                //cgstValidation.Operator = ExcelDataValidationOperator.between;
                //cgstValidation.Formula.Value = 0;
                //cgstValidation.Formula2.Value = 99;
                foreach (var gst in gstVals)
                {
                    cgstValidation.Formula.Values.Add(gst);
                }

                var igstValidation = wsSheet1.DataValidations.AddListValidation($"Q2:Q{maxRows}");//IGST
                igstValidation.ShowErrorMessage = true;
                igstValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                igstValidation.ErrorTitle = "Invalid IGST";
                igstValidation.Error = "Invalid IGST value should 0, 5, 12, 18, 28.";
                //cgstValidation.Operator = ExcelDataValidationOperator.between;
                //cgstValidation.Formula.Value = 0;
                //cgstValidation.Formula2.Value = 99;
                foreach (var gst in IgstVals)
                {
                    igstValidation.Formula.Values.Add(gst);
                }

                //vendor dic

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }
                    int parentRowNumber = 2;
                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(item.ProductQuotationTemplateJson);
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        wsSheet1.Cells["C" + index].Value = item.ItemID;
                        wsSheet1.Cells["D" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["E" + index].Value = item.ProductNo;
                        wsSheet1.Cells["F" + index].Value = item.HsnCode;
                        wsSheet1.Cells["G" + index].Value = item.ProductDescription;
                        //wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["H" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["I" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["J" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["K" + index].Value = item.IsRegret;
                        wsSheet1.Cells["L" + index].Value = item.RegretComments;
                        wsSheet1.Cells["M" + index].Value = item.ItemLevelInitialComments;
                        wsSheet1.Cells["N" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["O" + index].Value = item.SGst;
                        wsSheet1.Cells["P" + index].Value = item.CGst;
                        wsSheet1.Cells["Q" + index].Value = item.IGst;
                        //wsSheet1.Cells["J" + index].Value = item.ItemPrice;
                        
                        wsSheet1.Cells["S" + index].Value = item.PlantAddress;
                        wsSheet1.Cells["T" + index].Value = item.RequirementID;
                        //wsSheet1.Cells["U" + index].Value = item.ITEM_DELV_DATE1.ToString();


                        
                        index++;

                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            foreach (var subItem in jsonToList)
                            {
                                if (!subItem.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    wsSheet1.Cells[$"A{parentRowNumber}:T{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[$"A{parentRowNumber}:T{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#f2f2f2"));
                                    wsSheet1.Cells[$"K{parentRowNumber}:Q{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[$"K{parentRowNumber}:Q{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    if (item.HsnCode == "" || item.HsnCode == null)
                                    {
                                        wsSheet1.Cells[$"F{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells[$"F{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    }
                                    
                                    wsSheet1.Cells["C" + index].Value = $"{item.ItemID}-{subItem.T_ID}";
                                    wsSheet1.Cells["D" + index].Value = subItem.NAME;
                                    wsSheet1.Cells["G" + index].Value = subItem.SPECIFICATION;
                                    //wsSheet1.Cells["Q" + index].Value = subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1;

                                    if (subItem.HAS_SPECIFICATION > 0)
                                    {
                                        wsSheet1.Cells["G" + index].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        wsSheet1.Cells["G" + index].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    }

                                    index++;
                                }
                                
                            }
                        }
                        
                        parentRowNumber = index;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            if (template.ToUpper().Contains("UNIT_BIDDING"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_BIDDING_" + reqID);
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "REQUIREMENT_ID";
                wsSheet1.Cells["C1"].Value = "ProductName";
                wsSheet1.Cells["D1"].Value = "ProductNumber";
                wsSheet1.Cells["E1"].Value = "Description";
                wsSheet1.Cells["F1"].Value = "Make";
                wsSheet1.Cells["G1"].Value = "OtherBrands";
                wsSheet1.Cells["H1"].Value = "Revised Unit Price";
                wsSheet1.Cells["I1"].Value = "QTY";
                wsSheet1.Cells["J1"].Value = "SGST";
                wsSheet1.Cells["K1"].Value = "CGST";
                wsSheet1.Cells["L1"].Value = "IGST";
                wsSheet1.Cells["M1"].Value = "Revised Item Price";
                wsSheet1.Cells["N1"].Value = "PRODUCT_ID";
                wsSheet1.Cells["O1"].Value = "Unit Price";

                int maxRows1 = 0;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }
                maxRows1 = vendorsDictionary[userID].ListRequirementItems.Count + 1;
                wsSheet1.Cells[$"M2:M{maxRows}"].Formula = "IF(H2 > 0, (H2*I2)*(1 + (J2 + K2)/100), \"\")";
                wsSheet1.Cells["A1:O1"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["A1:O1"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#377BA0"));//blue
                wsSheet1.Cells["A1:O1"].Style.Font.Color.SetColor(Color.White);
                wsSheet1.Cells["A1:O1"].Style.Font.Bold = true;
                wsSheet1.Cells["A1:O1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A1:O1"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Row(1).Height = 40;
                wsSheet1.Cells["A:O"].AutoFitColumns();
                wsSheet1.Cells["A:O"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:O"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:O"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                wsSheet1.Cells["A:O"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
           
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                var revUnitPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"M2:M{maxRows}");
                revUnitPriceValidation.ShowErrorMessage = true;
                revUnitPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                revUnitPriceValidation.ErrorTitle = "Invalid Revised Unit Price";
                revUnitPriceValidation.Error = "Enter Value greater than zero.";
                revUnitPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                revUnitPriceValidation.Formula.Value = 0;

                

                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    int parentRowNumber = 2;
                    string vendorRemarks = string.Empty;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                        }
                    }

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        if (item.IsItemQuotationRejected == 1 || item.IsRegret || item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }
                        List<CATALOG.ProductQuotationTemplate> jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(item.ProductQuotationTemplateJson);

                        var gst = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["A" + index].Value = entry.Key;
                        wsSheet1.Cells["B" + index].Value = item.RequirementID;
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductNo;
                        wsSheet1.Cells["E" + index].Value = item.ProductDescription;
                        wsSheet1.Cells["F" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["G" + index].Value = item.OthersBrands;
                        wsSheet1.Cells["H" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["I" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["J" + index].Value = (item.SGst);
                        wsSheet1.Cells["K" + index].Value = (item.CGst);
                        wsSheet1.Cells["L" + index].Value = (item.IGst);
                        wsSheet1.Cells["N" + index].Value = item.ItemID;
                        wsSheet1.Cells["O" + index].Value = item.UnitPrice;
                        index++;

                        if (jsonToList != null && jsonToList.Count > 0)
                        {
                            foreach (var subItem in jsonToList)
                            {
                                if (!subItem.NAME.Equals("Total", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    wsSheet1.Cells[$"A{parentRowNumber}:O{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[$"A{parentRowNumber}:O{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#f2f2f2"));
                                    wsSheet1.Cells[$"H{parentRowNumber}"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    wsSheet1.Cells[$"H{parentRowNumber}"].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#F2DCDB"));//Editable
                                    wsSheet1.Cells["N" + index].Value = $"{item.ItemID}-{subItem.T_ID}";
                                    wsSheet1.Cells["C" + index].Value = subItem.NAME;
                                    wsSheet1.Cells["E" + index].Value = subItem.SPECIFICATION;
                                    //wsSheet1.Cells["I" + index].Value = subItem.HAS_QUANTITY > 0 ? subItem.CONSUMPTION : 1;

                                    index++;
                                }
                            }
                        }
                        parentRowNumber = index;
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #region 
            if (template.ToUpper().Contains("UNIT_PRICE_BIDDING_DATA"))
            {
                Requirement requirement = service.GetRequirementData(reqID, userID, sessionID);
                requirement.AuctionVendors = requirement.AuctionVendors.Where(v => v.CompanyName != "PRICE_CAP" && v.IsQuotationRejected != 1).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("UNIT_PRICE_BIDDING_DATA");

                //wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                //wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                //wsSheet1.Cells["C1"].Value = "PRICE";
                //wsSheet1.Cells["D1"].Value = "REVISED_PRICE";
                //wsSheet1.Cells["E1"].Value = "GST";
                //wsSheet1.Cells["F1"].Value = "FREIGHT";
                //wsSheet1.Cells["G1"].Value = "REVISED_FREIGHT";
                //wsSheet1.Cells["H1"].Value = "TOTAL_PRICE";
                //wsSheet1.Cells["I1"].Value = "REVISED_TOTAL_PRICE";
                //wsSheet1.Cells["J1"].Value = "ITEM_ID";
                //wsSheet1.Cells["K1"].Value = "PRODUCT_ID";
                //wsSheet1.Cells["L1"].Value = "PRODUCT_NO";
                //wsSheet1.Cells["M1"].Value = "BRAND";
                //wsSheet1.Cells["N1"].Value = "QUANTITY";
                //wsSheet1.Cells["O1"].Value = "UNITS";
                //wsSheet1.Cells["P1"].Value = "DESCRIPTION";

                //VENDOR_ID VENDOR_NAME PRICE REVISED_PRICE   GST FREIGHT REVISED_FREIGHT TOTAL_PRICE 
                //REVISED_TOTAL_PRICE ITEM_ID PRODUCT_ID 
                //PRODUCT_NO  BRAND QUANTITY    UNITS DESCRIPTION

                //VENDOR_NAME Vendor Rank, Product Name, UNIT,    QUANTITY, Quoted Unit Price,   
                //Quoted GST,  Final Bid Price, Total Quoted Price(inc GST),    Total Bid Price(inc GST),   
                //Savings %, Saving Value, Vendor wise total saving %, Vendor wise total saving Value

                wsSheet1.Cells["A1"].Value = "VENDOR NAME";
                wsSheet1.Cells["B1"].Value = "Vendor Rank";
                wsSheet1.Cells["C1"].Value = "Product Name";
                wsSheet1.Cells["D1"].Value = "UNIT";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "Quoted Unit Price";
                wsSheet1.Cells["G1"].Value = "Quoted GST";
                wsSheet1.Cells["H1"].Value = "Final Bid Price";
                wsSheet1.Cells["I1"].Value = "Total Quoted Price(inc GST)";
                wsSheet1.Cells["J1"].Value = "Total Bid Price(inc GST)";
                wsSheet1.Cells["K1"].Value = "Savings %";
                wsSheet1.Cells["L1"].Value = "Saving Value";
                wsSheet1.Cells["M1"].Value = "Vendor wise total saving %";
                wsSheet1.Cells["N1"].Value = "Vendor wise total saving Value";
                //wsSheet1.Cells["O1"].Value = "IT";
                //wsSheet1.Cells["P1"].Value = "RT";
               

                var calcColumns = wsSheet1.Cells["A1:N1"];
                wsSheet1.Cells.AutoFitColumns();
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumns.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                //calcColumns.Style.Border.Bottom.Color.SetColor(Color.Red);
                calcColumns.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                calcColumns.Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                calcColumnsFont.Bold = true;
                //calcColumnsFont.Size = 16;
                calcColumnsFont.Color.SetColor(Color.White);
                
                //calcColumnsFont.Italic = true;


                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                Dictionary<int, Requirement> vendorsDictionary = new Dictionary<int, Requirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    Requirement vendorReq = service.GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }


                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    entry.Value.AuctionVendors[0].savingsPercentage = 0;
                    entry.Value.AuctionVendors[0].savings = 0;

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        //K
                        item.savingsPercentage = Math.Round(
                            (((item.ItemPrice - item.RevItemPrice)
                            / (item.ItemPrice)) * 100),
                            2);
                        //L
                        item.savings = Math.Round((item.ItemPrice - item.RevItemPrice), 2);

                    }

                    //M
                    entry.Value.AuctionVendors[0].savingsPercentage = Math.Round(
                        (((entry.Value.AuctionVendors[0].TotalInitialPrice - entry.Value.AuctionVendors[0].RevVendorTotalPrice)
                        / (entry.Value.AuctionVendors[0].TotalInitialPrice)) * 100),
                        2);

                    //N
                    entry.Value.AuctionVendors[0].savings = Math.Round((entry.Value.AuctionVendors[0].TotalInitialPrice - entry.Value.AuctionVendors[0].RevVendorTotalPrice), 2);

                }




                int index = 2;
                foreach (KeyValuePair<int, Requirement> entry in vendorsDictionary)
                {
                    string vendorRemarks = string.Empty;
                    double freightcharges = 0;
                    double revfreightcharges = 0;
                    foreach (var vendor in entry.Value.AuctionVendors)
                    {
                        if (vendor.VendorID == entry.Key)
                        {
                            vendorRemarks = vendor.OtherProperties;
                            freightcharges = vendor.VendorFreight;
                            revfreightcharges = vendor.RevVendorFreight;
                        }
                    }

                    var from = index;
                    var to = index;

                    foreach (var item in entry.Value.ListRequirementItems)
                    {
                        var gst = item.CGst + item.SGst + item.IGst;

                        wsSheet1.Cells["A" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        if(from == to)
                        {
                        wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].Rank;
                        }
                        wsSheet1.Cells["C" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["F" + index].Value = item.UnitPrice;
                        wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                        wsSheet1.Cells["H" + index].Value = item.RevUnitPrice;
                        wsSheet1.Cells["I" + index].Value = item.ItemPrice;
                        wsSheet1.Cells["J" + index].Value = item.RevItemPrice;
                        wsSheet1.Cells["K" + index].Value = item.savingsPercentage;
                        wsSheet1.Cells["L" + index].Value = item.savings;
                        wsSheet1.Cells["M" + index].Value = entry.Value.AuctionVendors[0].savingsPercentage;
                        wsSheet1.Cells["N" + index].Value = entry.Value.AuctionVendors[0].savings;
                        //wsSheet1.Cells["O" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        //wsSheet1.Cells["P" + index].Value = entry.Value.AuctionVendors[0].RevVendorTotalPrice;


                        //wsSheet1.Cells["A" + index].Value = entry.Key;
                        //wsSheet1.Cells["B" + index].Value = entry.Value.AuctionVendors[0].CompanyName;
                        //wsSheet1.Cells["C" + index].Value = item.UnitPrice;// entry.Value.AuctionVendors[0].RunningPrice;
                        //wsSheet1.Cells["D" + index].Value = item.RevUnitPrice; // entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        //wsSheet1.Cells["E" + index].Value = item.CGst + item.SGst + item.IGst;
                        //wsSheet1.Cells["F" + index].Value = entry.Value.AuctionVendors[0].VendorFreight;
                        //wsSheet1.Cells["G" + index].Value = entry.Value.AuctionVendors[0].RevVendorFreight;
                        //wsSheet1.Cells["H" + index].Value = entry.Value.AuctionVendors[0].TotalInitialPrice;
                        //wsSheet1.Cells["I" + index].Value = entry.Value.AuctionVendors[0].RevVendorTotalPrice;
                        //wsSheet1.Cells["J" + index].Value = item.ItemID;
                        //wsSheet1.Cells["K" + index].Value = item.ProductIDorName;
                        //wsSheet1.Cells["L" + index].Value = item.ProductNo;
                        //wsSheet1.Cells["M" + index].Value = item.ProductBrand;
                        //wsSheet1.Cells["N" + index].Value = item.ProductQuantity;
                        //wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                        //wsSheet1.Cells["P" + index].Value = item.ProductDescription;

                        

                        wsSheet1.Cells.AutoFitColumns();

                        index++;
                        to = index;
                    }
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Merge = true;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["B" + from + ":B" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Merge = true;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["M" + from + ":M" + (to - 1)].Style.Font.Size = 16;

                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Merge = true;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    wsSheet1.Cells["N" + from + ":N" + (to - 1)].Style.Font.Size = 16;
                    

                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion
            #endregion

            #region bulk margin quotations
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_QUOTATION"))
            {
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_QUOTATION");
                wsSheet1.Cells["A1"].Value = "VENDOR_ID";
                wsSheet1.Cells["B1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["C1"].Value = "HSN_CODE";
                wsSheet1.Cells["D1"].Value = "BRAND";
                wsSheet1.Cells["E1"].Value = "QUANTITY";
                wsSheet1.Cells["F1"].Value = "COST_PRICE";
                wsSheet1.Cells["G1"].Value = "GST";
                wsSheet1.Cells["H1"].Value = "MRP";
                wsSheet1.Cells["I1"].Value = "NET_PRICE";
                wsSheet1.Cells["J1"].Value = "MARGIN_AMOUNT";
                wsSheet1.Cells["K1"].Value = "MARGIN_PERC";
                wsSheet1.Cells["L1"].Value = "ITEM_ID";
                wsSheet1.Cells["M1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["N1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["O1"].Value = "UNITS";
                wsSheet1.Cells["P1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Q1"].Value = "REQ_ID";
                wsSheet1.Cells["R1"].Value = "TITLE";
                wsSheet1.Cells["S1"].Value = "POSTED_DATE";
                wsSheet1.Cells["T1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["U1"].Value = "QUANTITY_ORIG";
                wsSheet1.Cells["V1"].Value = "QUOTATION_STATUS";
                #region excelStyling
                wsSheet1.Cells["K2:K6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Column(1).Hidden = true;
                wsSheet1.Column(12).Hidden = true;
                wsSheet1.Column(13).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Cells["A:K"].AutoFitColumns();
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                wsSheet1.Cells["I:I"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["I:I"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["J:J"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["J:J"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["K:K"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["K:K"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                var calcColumns = wsSheet1.Cells["I:K"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling

                wsSheet1.Cells["I2:I6000"].Formula = "IF(A2 > 0, F2*(1+(G2/100)), \"\")";
                wsSheet1.Cells["J2:J6000"].Formula = "IF(A2 > 0, H2 - I2, \"\")";
                wsSheet1.Cells["K2:K6000"].Formula = "IFERROR(J2/I2*100,\"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                int index = 2;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 14, sessionID);
                foreach (Requirement req in requirementList)
                {
                    if (req.AuctionVendors.Count == 0)
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime == null || req.StartTime > DateTime.UtcNow))
                    {
                        if(req.RequirementID > 0 && req.PostedOn >= DateTime.UtcNow.AddMonths(-1))
                        {
                            foreach (var item in req.ListRequirementItems)
                            {
                                var gst = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["A" + index].Value = userID;
                                wsSheet1.Cells["B" + index].Value = req.AuctionVendors[0].CompanyName;
                                wsSheet1.Cells["L" + index].Value = item.ItemID;
                                wsSheet1.Cells["C" + index].Value = item.ProductNo;
                                wsSheet1.Cells["D" + index].Value = item.ProductBrand;
                                wsSheet1.Cells["E" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["H" + index].Value = item.UnitMRP;
                                wsSheet1.Cells["G" + index].Value = item.CGst + item.SGst + item.IGst;
                                wsSheet1.Cells["M" + index].Value = 0;
                                wsSheet1.Cells["N" + index].Value = item.ProductIDorName;
                                wsSheet1.Cells["O" + index].Value = item.ProductQuantityIn;
                                wsSheet1.Cells["P" + index].Value = req.AuctionVendors[0].OtherProperties;
                                wsSheet1.Cells["Q" + index].Value = req.RequirementID;
                                wsSheet1.Cells["R" + index].Value = req.Title;
                                wsSheet1.Cells["S" + index].Value = req.PostedOn.ToString(); ;
                                wsSheet1.Cells["T" + index].Value = item.OthersBrands;
                                wsSheet1.Cells["U" + index].Value = item.ProductQuantity;
                                wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].IsQuotationRejected == -1 ? "Pending/Not Uploaded" : (req.AuctionVendors[0].IsQuotationRejected == 0 ? "Approved" : "Rejected");
                                var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                                wsSheet1.Cells["F" + index].Value = Math.Round(costPrice, 2);
                                index++;
                            }
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            #region bulk margin bidding
            if (template.ToUpper().Contains("BULK_MARGIN_VEND_BIDDING"))
            {
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("BULK_MARGIN_VEND_BIDDING");
                wsSheet1.Cells["A1"].Value = "REQ_ID";
                wsSheet1.Cells["B1"].Value = "REQ_TITLE";
                wsSheet1.Cells["C1"].Value = "POSTED_DATE";
                wsSheet1.Cells["D1"].Value = "NEGOTIATION_DATE";
                wsSheet1.Cells["E1"].Value = "VENDOR_ID";
                wsSheet1.Cells["F1"].Value = "VENDOR_NAME";
                wsSheet1.Cells["G1"].Value = "HSN_CODE";
                wsSheet1.Cells["H1"].Value = "MANUFACTURER_NAME";
                wsSheet1.Cells["I1"].Value = "BRAND";
                wsSheet1.Cells["J1"].Value = "QUANTITY";
                wsSheet1.Cells["K1"].Value = "COST_PRICE";
                wsSheet1.Cells["L1"].Value = "REV_COST_PRICE";
                wsSheet1.Cells["M1"].Value = "DIFFERENCE";
                wsSheet1.Cells["N1"].Value = "GST";
                wsSheet1.Cells["O1"].Value = "MRP";
                wsSheet1.Cells["P1"].Value = "REV_NET_PRICE";
                wsSheet1.Cells["Q1"].Value = "REV_MARGIN_AMOUNT";
                wsSheet1.Cells["R1"].Value = "REV_MARGIN_PERC";
                wsSheet1.Cells["S1"].Value = "ITEM_ID";
                wsSheet1.Cells["T1"].Value = "QUOTATION_ID";
                wsSheet1.Cells["U1"].Value = "TOTAL_INIT_PRICE";
                wsSheet1.Cells["V1"].Value = "REV_TOTAL_PRICE";
                wsSheet1.Cells["W1"].Value = "PRODUCT_NAME";
                wsSheet1.Cells["X1"].Value = "UNITS";
                wsSheet1.Cells["Y1"].Value = "VENDOR_REMARK";
                wsSheet1.Cells["Z1"].Value = "RANK";
                wsSheet1.Cells["AA1"].Value = "REV_COST_PRICE_ORIG";

                #region excelStyling                

                wsSheet1.Cells["R2:R6000"].Style.Numberformat.Format = "0.00000000";
                wsSheet1.Cells["A:AA"].AutoFitColumns();
                wsSheet1.Column(10).Hidden = true;
                wsSheet1.Column(11).Hidden = true;
                wsSheet1.Column(14).Hidden = true;
                wsSheet1.Column(15).Hidden = true;
                wsSheet1.Column(21).Hidden = true;
                wsSheet1.Column(27).Hidden = true;
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml("#F2DCDB");
                Color colFromHex1 = System.Drawing.ColorTranslator.FromHtml("#DAF7A6");
                wsSheet1.Cells["P:P"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["P:P"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["Q:Q"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["Q:Q"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["R:R"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["R:R"].Style.Fill.BackgroundColor.SetColor(colFromHex);
                wsSheet1.Cells["L:L"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                wsSheet1.Cells["L:L"].Style.Fill.BackgroundColor.SetColor(colFromHex1);
                var calcColumns = wsSheet1.Cells["P:R"];
                var calcColumnsFont = calcColumns.Style.Font;
                calcColumnsFont.Bold = true;
                calcColumnsFont.Italic = true;
                #endregion excelStyling
                
                wsSheet1.Cells["M2:M6000"].Formula = "IF(E2 > 0, ABS(K2 - L2), \"\")";
                wsSheet1.Cells["P2:P6000"].Formula = "IF(E2 > 0, L2*(1+(N2/100)), \"\")";
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                List<Requirement> requirementList = service.GetVendorRequirements(userID, 1, sessionID);
                int index = 2;
                foreach (Requirement req in requirementList)
                {
                    if(req.AuctionVendors.Count > 0 && (req.AuctionVendors[0].IsQuotationRejected == 1 || req.AuctionVendors[0].IsRevQuotationRejected == 1))
                    {
                        continue;
                    }
                    if (req.PostedOn >= DateTime.UtcNow.AddMonths(-1) && req.IsDiscountQuotation == 2 && (req.StartTime < DateTime.UtcNow && req.EndTime > DateTime.UtcNow))
                    {
                        if(req.AuctionVendors == null)
                        {
                            continue;
                        }
                        foreach (var item in req.ListRequirementItems)
                        {
                            var gst = item.CGst + item.SGst + item.IGst;
                            var revCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["A" + index].Value = req.RequirementID; // entry.Key;
                            wsSheet1.Cells["B" + index].Value = req.Title;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["C" + index].Value = req.PostedOn.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["D" + index].Value = req.StartTime.ToString();  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["E" + index].Value = userID;  // entry.Value.AuctionVendors[0].CompanyName;
                            wsSheet1.Cells["F" + index].Value = req.AuctionVendors[0].CompanyName;  // entry.Value.AuctionVendors[0].CompanyName;

                            wsSheet1.Cells["G" + index].Value = item.ProductNo;
                            wsSheet1.Cells["H" + index].Value = item.OthersBrands;
                            wsSheet1.Cells["I" + index].Value = item.ProductBrand;
                            wsSheet1.Cells["J" + index].Value = item.ProductQuantity;
                            wsSheet1.Cells["L" + index].Value = Math.Round(revCostPrice, 2);

                            wsSheet1.Cells["N" + index].Value = item.CGst + item.SGst + item.IGst;
                            wsSheet1.Cells["O" + index].Value = item.UnitMRP;
                            wsSheet1.Cells["S" + index].Value = item.ItemID;
                            wsSheet1.Cells["T" + index].Value = 0;
                            wsSheet1.Cells["U" + index].Value = req.AuctionVendors[0].TotalInitialPrice;
                            wsSheet1.Cells["V" + index].Value = req.AuctionVendors[0].RevVendorTotalPrice;
                            wsSheet1.Cells["W" + index].Value = item.ProductIDorName;
                            wsSheet1.Cells["X" + index].Value = item.ProductQuantityIn;
                            wsSheet1.Cells["Y" + index].Value = req.AuctionVendors[0].OtherProperties;
                            wsSheet1.Cells["Z" + index].Value = req.AuctionVendors[0].Rank > 0 ? req.AuctionVendors[0].Rank.ToString() : "NA";
                            wsSheet1.Cells["AA" + index].Value = Math.Round(revCostPrice, 2);

                            if (req.IsRevUnitDiscountEnable == 0)
                            {
                                wsSheet1.Cells["Q" + index].Value = 0;
                                wsSheet1.Cells["R" + index].Value = 0;
                            }
                            if (req.IsRevUnitDiscountEnable == 1)
                            {
                                wsSheet1.Cells["Q" + index].Formula = "IF(E" + index + " > 0, ABS(O" + index + " - P" + index + "), \"\")";
                                wsSheet1.Cells["R" + index].Formula = "IFERROR(Q" + index + "/P" + index + "*100,\"\")";
                            }

                            var costPrice = (item.UnitMRP * 100 * 100) / ((item.UnitDiscount * 100) + 10000 + (item.UnitDiscount * gst) + (gst * 100));
                            wsSheet1.Cells["K" + index].Value = Math.Round(costPrice, 2);
                            index++;
                        }
                    }
                }

                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion


            #region REQUIREMENT_SAVE
            if (template.ToUpper().Contains("REQUIREMENT_SAVE"))
            {
                Requirement requirement = new Requirement();
                if (reqID > 0)
                {
                    requirement = service.GetRequirementData(reqID, userID, sessionID);
                }

                PRMCatalogService catalogService = new PRMCatalogService();
                var companyUnits = service.GetCompanyConfiguration(compID, "ITEM_UNITS", sessionID);
                List<Product> products = catalogService.GetProducts(compID, sessionID);
                products = products.Where(p => (p.IsValid > 0 && !string.IsNullOrEmpty(p.ProductCode))).ToList();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE");
                ExcelWorksheet wsSheet2 = ExcelPkg.Workbook.Worksheets.Add("REQUIREMENT_SAVE_CONFIG");
                wsSheet1.Cells["A1"].Value = "ProductCode";
                wsSheet1.Cells["B1"].Value = "ProductName";
                wsSheet1.Cells["C1"].Value = "ProductNumber";
                wsSheet1.Cells["D1"].Value = "Quantity";
                wsSheet1.Cells["E1"].Value = "Units";
                wsSheet1.Cells["F1"].Value = "Make";
                wsSheet1.Cells["G1"].Value = "DeliveryLocation";
                wsSheet1.Cells["H1"].Value = "HsnCode";
                wsSheet1.Cells["A:H"].AutoFitColumns();
                var validation = wsSheet1.DataValidations.AddListValidation($"E2:E{maxRows}");
                validation.ShowErrorMessage = true;
                validation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                validation.ErrorTitle = "Invalid Units";
                validation.Error = "Invalid Units Selected";

                var quantityPriceValidation = wsSheet1.DataValidations.AddDecimalValidation($"D2:D{maxRows}");
                quantityPriceValidation.ShowErrorMessage = true;
                quantityPriceValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                quantityPriceValidation.ErrorTitle = "Invalid Quantity";
                quantityPriceValidation.Error = "Enter Value greater than zero.";
                quantityPriceValidation.Operator = ExcelDataValidationOperator.greaterThan;
                quantityPriceValidation.Formula.Value = 0;

                int count = 1;
                foreach (var unit in companyUnits)
                {
                    wsSheet2.Cells["A" + count.ToString()].Value = unit.ConfigValue;
                    count++;
                }

                validation.Formula.ExcelFormula = "REQUIREMENT_SAVE_CONFIG!$A1:$A" + (count - 1).ToString();

                for (int i = 2; i <= maxRows; i++)
                {
                    wsSheet1.Cells["B" + (i)].Formula = $"IFERROR(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!C\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                    wsSheet1.Cells["C" + (i)].Formula = $"IFERROR(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!D\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                    wsSheet1.Cells["H" + (i)].Formula = $"IFERROR(INDIRECT(\"REQUIREMENT_SAVE_CONFIG!E\" & MATCH(A{i}, REQUIREMENT_SAVE_CONFIG!$B:$B,0 )),\"\")";
                }


                if (requirement != null && requirement.ListRequirementItems != null && requirement.ListRequirementItems.Count > 0)
                {
                    int index = 2;
                    foreach (var item in requirement.ListRequirementItems)
                    {
                        if (item.IsCoreProductCategory <= 0)
                        {
                            continue;
                        }

                        wsSheet1.Cells["A" + index].Value = item.ProductCode;
                        wsSheet1.Cells["B" + index].Value = item.ProductIDorName;
                        wsSheet1.Cells["C" + index].Value = item.ProductNo;
                        wsSheet1.Cells["D" + index].Value = item.ProductQuantity;
                        wsSheet1.Cells["E" + index].Value = item.ProductQuantityIn;
                        wsSheet1.Cells["F" + index].Value = item.ProductBrand;
                        wsSheet1.Cells["G" + index].Value = item.PlantAddress;
                        wsSheet1.Cells["H" + index].Value = item.HsnCode;
                        index++;
                    }
                }


                var productValidation = wsSheet1.DataValidations.AddListValidation($"A2:A{maxRows}");
                productValidation.ShowErrorMessage = true;
                productValidation.ErrorStyle = ExcelDataValidationWarningStyle.stop;
                productValidation.ErrorTitle = "Invalid Product";
                productValidation.Error = "Invalid Product entered";
                count = 1;
                foreach (var product in products)
                {
                    wsSheet2.Cells["B" + count.ToString()].Value = product.ProductCode;
                    wsSheet2.Cells["C" + count.ToString()].Value = product.ProductName;
                    wsSheet2.Cells["D" + count.ToString()].Value = product.ProductNo;
                    wsSheet2.Cells["E" + count.ToString()].Value = product.ProductHSNCode;
                    count++;
                }

                productValidation.Formula.ExcelFormula = "=REQUIREMENT_SAVE_CONFIG!$B1:$B" + (count - 1).ToString();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;
                wsSheet2.Protection.IsProtected = false;
                wsSheet2.Protection.AllowSelectLockedCells = false;
                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }
            #endregion

            return Convert.ToBase64String(ms.ToArray());
        }

        public ExcelRequirement GetReqReportForExcel(int reqID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            ExcelRequirement excelrequirement = new ExcelRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[3].Rows[0];
                    excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
                    excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
                    excelrequirement.CurrentDate = prmservices.GetDate();
                    excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
                    excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
                    excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    excelrequirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    excelrequirement.IsTechScoreReq = row["TECH_SCORE_REQ"] != DBNull.Value ? Convert.ToInt16(row["TECH_SCORE_REQ"]) : 0;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[4].Rows[0];
                    excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
                    excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
                }

                List<ExcelVendorDetails> ListRV = new List<ExcelVendorDetails>();
                int rank = 1;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ExcelVendorDetails RV = new ExcelVendorDetails();

                        #region VENDOR DETAILS
                        RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        RV.TechnicalScore = row["TECHNICAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row["TECHNICAL_SCORE"]) : 0;
                        RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
                        RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
                        RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
                        RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
                        RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                        RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
                        RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
                        RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
                        RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
                        RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
                        RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        
                        RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0; //*
                        RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                        RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
                        RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0; //*
                        RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

                        RV.InstallationCharges = row["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES"]) : 0; //*
                        RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationCharges = row["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES"]) : 0; //*
                        RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                        RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
                        RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));
                        
                        RV.PackingCharges = row["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES"]) : 0; //*
                        RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                        RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingCharges = row["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES"]) : 0; //*
                        RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                        RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
                        RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

                        RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

                        RV.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;
                        RV.PayLoadFactor = 0;
                        RV.RevPayLoadFactor = 0;
                        RV.RevChargeAny = 0;
                        RV.IsQuotationRejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        RV.IsRevQuotationRejected = row["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_QUOTATION_REJECTED"]) : 0;

                        #endregion VENDOR DETAILS

                        ListRV.Add(RV);
                    }

                    foreach (ExcelVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
                    {
                        vendor.Rank = ListRV.IndexOf(vendor) + 1;
                    }
                }

                List<ExcelRequirementItems> ListRITemp = new List<ExcelRequirementItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        ExcelRequirementItems RI = new ExcelRequirementItems();
                        #region ITEM DETAILS
                        RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
                        RI.HsnCode = row["HSN_CODE"] != DBNull.Value ? Convert.ToString(row["HSN_CODE"]) : string.Empty;
                        RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
                        RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
                        RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;
                        RI.I_LLP_DETAILS = row["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row["I_LLP_DETAILS"]) : string.Empty;
                        RI.ITEM_L1_PRICE = row["ITEM_L1_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L1_PRICE"]) : 0;
                        RI.ITEM_L1_COMPANY_NAME = row["ITEM_L1_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L1_COMPANY_NAME"]) : string.Empty;
                        RI.ITEM_L2_PRICE = row["ITEM_L2_PRICE"] != DBNull.Value ? Convert.ToDouble(row["ITEM_L2_PRICE"]) : 0;
                        RI.ITEM_L2_COMPANY_NAME = row["ITEM_L2_COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["ITEM_L2_COMPANY_NAME"]) : string.Empty;
                        RI.ProductQuotationTemplateJson = row["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                        RI.IsCore = row["IS_CORE"] != DBNull.Value ? Convert.ToInt32(row["IS_CORE"]) : 0;
                        RI.QtyDistributed = 0;
                        RI.CatalogueItemID = row["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["CATALOGUE_ITEM_ID"]) : 0;
                        if (!string.IsNullOrEmpty(RI.ProductQuotationTemplateJson))
                        {
                            RI.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(RI.ProductQuotationTemplateJson);
                        }

                        RI.ReqVendors = ListRV.Select(item => (ExcelVendorDetails)item.Clone()).ToList().ToArray();
                        #endregion ITEM DETAILS

                        ListRITemp.Add(RI);
                    }
                }

                List<ExcelQuotationPrices> ListQ = new List<ExcelQuotationPrices>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[2].Rows)
                    {
                        ExcelQuotationPrices Q = new ExcelQuotationPrices();

                        #region QUOTATION DETAILS
                        Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
                        Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
                        Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
                        Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
                        Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
                        Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
                        Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
                        Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
                        Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
                        Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                        Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                        Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
                        Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
                        Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
                        Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
                        Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
                        Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
                        Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
                        Q.ProductQuotationTemplateJson = row["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row["ProductQuotationTemplateJson"]) : string.Empty;
                        if (!string.IsNullOrEmpty(Q.ProductQuotationTemplateJson))
                        {
                            Q.ProductQuotationTemplateArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(Q.ProductQuotationTemplateJson);
                        }


                        // ITEM LEVEL RANKING //
                        int is_quotation_rejected = row["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row["IS_QUOTATION_REJECTED"]) : 0;
                        Q.ItemRank = 0;
                        if (is_quotation_rejected <= 0 && Q.RevUnitPrice > 0)
                        {
                            Q.ItemRank = 1;// row2["ITEM_RANK"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_RANK"]) : 0;
                        }
                        // ITEM LEVEL RANKING //

                        #endregion QUOTATION DETAILS

                        ListQ.Add(Q);

                    }
                }

                // ITEM LEVEL RANKING //
                ListQ = ListQ.OrderBy(q => q.ItemID).ThenByDescending(q => q.ItemRank).ThenBy(q => q.RevUnitPrice).ToList();
                int rank1 = 1;
                int prevItemId = 0;
                double prevRevUnitPrice = 0;
                foreach (var temp in ListQ)
                {
                    if (temp.ItemRank > 0)
                    {
                        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && prevRevUnitPrice == temp.RevUnitPrice)
                        {
                            temp.ItemRank = rank1;
                        }

                        if (prevItemId == temp.ItemID && temp.RevUnitPrice > 0 && temp.RevUnitPrice > prevRevUnitPrice)
                        {
                            temp.ItemRank = rank1 + 1;
                            rank1 = rank1 + 1;
                        }

                        if (prevItemId != temp.ItemID)
                        {
                            temp.ItemRank = 1;
                            rank1 = 1;
                        }
                    }

                    prevItemId = temp.ItemID;
                    prevRevUnitPrice = temp.RevUnitPrice;
                }
                // ITEM LEVEL RANKING //

                for (int i = 0; i < ListRITemp.Count; i++)
                {
                    for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
                    {
                        ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        try
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
                        }
                        catch
                        {
                            ListRITemp[i].ReqVendors[v].QuotationPrices = new ExcelQuotationPrices();
                        }
                    }
                }

                excelrequirement.ReqItems = ListRITemp.ToArray();

                foreach (ExcelRequirementItems item in excelrequirement.ReqItems)
                {
                    if (item.ReqVendors != null)
                    {
                        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
                        vendors = item.ReqVendors.Where(v => v.RevVendorTotalPrice > 0).OrderBy(v => v.RevVendorTotalPrice).ToList();
                        item.ReqVendors = vendors.ToArray();
                    }
                }

            }
            catch (Exception ex)
            {
                excelrequirement.ErrorMessage = ex.Message;
            }

            return excelrequirement;
        }

        //public MACRequirement GetReqItemWiseVendors(int reqID, string sessionID)
        //{
        //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //    MACRequirement excelrequirement = new MACRequirement();
        //    try
        //    {
        //        int isValidSession = Utilities.ValidateSession(sessionID, null);
        //        sd.Add("P_REQ_ID", reqID);
        //        DataSet ds = sqlHelper.SelectList("rp_GetReqReportForExcel", sd);
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[3].Rows[0];
        //            excelrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
        //            excelrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
        //            excelrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.UtcNow;
        //            excelrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.UtcNow;
        //            excelrequirement.CurrentDate = prmservices.GetDate();
        //            excelrequirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
        //            excelrequirement.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
        //            excelrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_ENABLED"]) : 0;
        //            excelrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["IS_CB_COMPLETED"]) : 0;
        //            excelrequirement.PostedByFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
        //            excelrequirement.PostedByLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
        //        }

        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[4].Rows[0];
        //            excelrequirement.PreSavings = row["PRE_SAVING"] != DBNull.Value ? Convert.ToDouble(row["PRE_SAVING"]) : 0;
        //            excelrequirement.PostSavings = row["REV_SAVING"] != DBNull.Value ? Convert.ToDouble(row["REV_SAVING"]) : 0;
        //        }

        //        List<MACVendorDetails> ListRV = new List<MACVendorDetails>();
        //        int rank = 1;
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[0].Rows)
        //            {
        //                MACVendorDetails RV = new MACVendorDetails();

        //                #region VENDOR DETAILS
        //                RV.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
        //                RV.VendorName = row["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_NAME"]) : string.Empty;
        //                RV.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
        //                RV.VendorFreight = row["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["VEND_FREIGHT"]) : 0;
        //                RV.RevVendorFreight = row["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_FREIGHT"]) : 0;
        //                RV.TotalInitialPrice = row["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_TOTAL_PRICE"]) : 0;
        //                RV.RevVendorTotalPrice = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
        //                RV.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
        //                RV.Validity = row["VALIDITY"] != DBNull.Value ? Convert.ToString(row["VALIDITY"]) : string.Empty;
        //                RV.OtherProperties = row["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row["OTHER_PROPERTIES"]) : string.Empty;
        //                RV.Duration = row["DURATION"] != DBNull.Value ? Convert.ToString(row["DURATION"]) : string.Empty;
        //                RV.Payment = row["PAYMENT"] != DBNull.Value ? Convert.ToString(row["PAYMENT"]) : string.Empty;
        //                RV.GSTNumber = row["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row["GST_NUMBER"]) : string.Empty;
        //                RV.SelectedVendorCurrency = row["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

        //                RV.PackingChargesTaxPercentage = row["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.PackingChargesWithTax = row["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["PACKING_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevpackingChargesWithTax = row["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevpackingChargesCB = row["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_PACKING_CHARGES_CB"]) : 0;
        //                RV.RevpackingChargesWithTaxCB = RV.RevpackingChargesCB + ((RV.RevpackingChargesCB / 100) * (RV.PackingChargesTaxPercentage));

        //                RV.InstallationChargesTaxPercentage = row["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.InstallationChargesWithTax = row["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevinstallationChargesWithTax = row["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevinstallationChargesCB = row["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_INSTALLATION_CHARGES_CB"]) : 0;
        //                RV.RevinstallationChargesWithTaxCB = RV.RevinstallationChargesCB + ((RV.RevinstallationChargesCB / 100) * (RV.InstallationChargesTaxPercentage));

        //                RV.FREIGHT_CHARGES_TAX_PERCENTAGE = row["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
        //                RV.FREIGHT_CHARGES = row["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES"]) : 0;
        //                RV.FREIGHT_CHARGES_WITH_TAX = row["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.REV_FREIGHT_CHARGES = row["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES"]) : 0;
        //                RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.REV_FREIGHT_CHARGES_WITH_TAX = row["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;
        //                RV.RevVendorFreightCB = row["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_FREIGHT_CHARGES_CB"]) : 0;

        //                RV.RevVendorTotalPriceCB = row["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE_CB"]) : 0;

        //                #endregion VENDOR DETAILS

        //                ListRV.Add(RV);
        //            }

        //            foreach (MACVendorDetails vendor in ListRV.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.TotalInitialPrice).ToList())
        //            {
        //                vendor.Rank = ListRV.IndexOf(vendor) + 1;
        //            }
        //        }

        //        List<MACRequirementItems> ListRITemp = new List<MACRequirementItems>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[1].Rows)
        //            {
        //                MACRequirementItems RI = new MACRequirementItems();
        //                #region ITEM DETAILS
        //                RI.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
        //                RI.ProductIDorName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
        //                RI.ProductNo = row["PROD_NO"] != DBNull.Value ? Convert.ToString(row["PROD_NO"]) : string.Empty;
        //                RI.ProductDescription = row["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row["DESCRIPTION"]) : string.Empty;
        //                RI.ProductQuantity = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
        //                RI.ProductQuantityIn = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
        //                RI.ProductBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
        //                RI.OthersBrands = row["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row["OTHER_BRAND"]) : string.Empty;
        //                RI.ItemLastPrice = row["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row["LAST_ITEM_PRICE"]) : 0;

        //                RI.ReqVendors = ListRV.Select(item => (MACVendorDetails)item.Clone()).ToList().ToArray();
        //                #endregion ITEM DETAILS

        //                ListRITemp.Add(RI);
        //            }
        //        }

        //        List<MACQuotationPrices> ListQ = new List<MACQuotationPrices>();
        //        if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
        //        {
        //            foreach (DataRow row in ds.Tables[2].Rows)
        //            {
        //                MACQuotationPrices Q = new MACQuotationPrices();

        //                #region QUOTATION DETAILS
        //                Q.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
        //                Q.ItemID = row["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row["ITEM_ID"]) : 0;
        //                Q.UnitPrice = row["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["UNIT_PRICE"]) : 0;
        //                Q.RevUnitPrice = row["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE"]) : 0;
        //                Q.CGst = row["C_GST"] != DBNull.Value ? Convert.ToDouble(row["C_GST"]) : 0;
        //                Q.SGst = row["S_GST"] != DBNull.Value ? Convert.ToDouble(row["S_GST"]) : 0;
        //                Q.IGst = row["I_GST"] != DBNull.Value ? Convert.ToDouble(row["I_GST"]) : 0;
        //                Q.ItemPrice = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
        //                Q.RevItemPrice = row["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE"]) : 0;
        //                Q.IsRegret = row["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row["IS_REGRET"]) > 0 ? true : false) : false;
        //                Q.RegretComments = row["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REGRET_COMMENTS"]) : string.Empty;
        //                Q.ItemLevelInitialComments = row["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
        //                Q.ItemLevelRevComments = row["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
        //                Q.VendorUnits = row["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row["VENDOR_UNITS"]) : string.Empty;
        //                Q.VendorBrand = row["BRAND"] != DBNull.Value ? Convert.ToString(row["BRAND"]) : string.Empty;
        //                Q.ItemFreightCharges = row["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_CHARGES"]) : 0;
        //                Q.ItemRevFreightCharges = row["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row["ITEM_REV_FREIGHT_CHARGES"]) : 0;
        //                Q.ItemFreightTAX = row["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row["ITEM_FREIGHT_TAX"]) : 0;
        //                Q.RevItemPriceCB = row["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REVICED_PRICE_CB"]) : 0;
        //                Q.RevUnitPriceCB = row["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row["REV_UNIT_PRICE_CB"]) : 0;
        //                #endregion QUOTATION DETAILS

        //                ListQ.Add(Q);

        //            }
        //        }

        //        for (int i = 0; i < ListRITemp.Count; i++)
        //        {
        //            for (int v = 0; v < ListRITemp[i].ReqVendors.Length; v++)
        //            {
        //                ListRITemp[i].ReqVendors[v].QuotationPrices = new MACQuotationPrices();
        //                try
        //                {
        //                    ListRITemp[i].ReqVendors[v].QuotationPrices = ListQ.Where(quotation => quotation.VendorID == ListRITemp[i].ReqVendors[v].VendorID && quotation.ItemID == ListRITemp[i].ItemID).FirstOrDefault();
        //                }
        //                catch
        //                {
        //                    ListRITemp[i].ReqVendors[v].QuotationPrices = new MACQuotationPrices();
        //                }
        //            }
        //        }

        //        excelrequirement.ReqItems = ListRITemp.ToArray();
        //        excelrequirement.ReqVendors = ListRV.ToArray();


        //        //foreach (ExcelRequirementItems item in excelrequirement.ReqItems) {
        //        //    if (item.ReqVendors != null)
        //        //    {
        //        //        List<ExcelVendorDetails> vendors = new List<ExcelVendorDetails>();
        //        //        vendors = item.ReqVendors.OrderBy(v => v.RevVendorTotalPrice).ToList();
        //        //        item.ReqVendors = vendors.ToArray();
        //        //    }
        //        //}

        //    }
        //    catch (Exception ex)
        //    {
        //        excelrequirement.ErrorMessage = ex.Message;
        //    }

        //    return excelrequirement;
        //}

        public List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedReport> consolidatedReports = new List<ConsolidatedReport>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);

                //sd.Add("P_FROM_DATE", Convert.ToDateTime(from).ToUniversalTime());
                //sd.Add("P_TO_DATE", Convert.ToDateTime(to).ToUniversalTime());

                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.RevVendTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                        revised.PLANT_ADDRESS = row1["PLANT_ADDRESS"] != DBNull.Value ? Convert.ToString(row1["PLANT_ADDRESS"]) : string.Empty;
                        // revised.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.VendTotalPrice = row2["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["VEND_TOTAL_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        initial.PLANT_ADDRESS = row2["PLANT_ADDRESS"] != DBNull.Value ? Convert.ToString(row2["PLANT_ADDRESS"]) : string.Empty;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedReport report = new ConsolidatedReport();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                       // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        report.POSTED_BY_USER = row["POSTED_BY"] != DBNull.Value ? Convert.ToString(row["POSTED_BY"]) : string.Empty;
                        
                        

                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null) {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_VendTotalPrice = InitialUser.VendTotalPrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                            report.IL1_SelectedVendorCurrency = InitialUser.SelectedVendorCurrency;
                            report.PLANT_LOCATION = InitialUser.PLANT_ADDRESS;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_RevVendTotalPrice = RevisedUserL1.RevVendTotalPrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                            report.RL1_SelectedVendorCurrency = RevisedUserL1.SelectedVendorCurrency;
                            report.PLANT_LOCATION = RevisedUserL1.PLANT_ADDRESS;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_RevVendTotalPrice = RevisedUserL2.RevVendTotalPrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            report.RL2_SelectedVendorCurrency = RevisedUserL2.SelectedVendorCurrency;
                            report.PLANT_LOCATION = RevisedUserL1.PLANT_ADDRESS;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_VendTotalPrice - report.RL1_RevVendTotalPrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, 2);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevVendTotalPrice / report.IL1_VendTotalPrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedReport recuirementerror = new ConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<LogisticConsolidatedReport> consolidatedReports = new List<LogisticConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetLogisticConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        LogisticConsolidatedReport report = new LogisticConsolidatedReport();

                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.InvoiceDate = row["INVOICE_DATE"] != DBNull.Value ? Convert.ToDateTime(row["INVOICE_DATE"]) : DateTime.MaxValue;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        // report.QTY = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToInt32(row["NET_WEIGHT"]) : 0;
                        report.ChargeableWt = row["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row["QUANTITY"]) : 0;
                        report.DGFee = row["FORWORD_DG_FEE"] != DBNull.Value ? Convert.ToDouble(row["FORWORD_DG_FEE"]) : 0;
                        report.AMS = row["AMS"] != DBNull.Value ? Convert.ToString(row["AMS"]) : string.Empty;
                        report.MISC = row["MISC"] != DBNull.Value ? Convert.ToString(row["MISC"]) : string.Empty;
                        // report.MenzinesCharges = row["L2_NAME"] != DBNull.Value ? Convert.ToDouble(row["L2_NAME"]) : 0;
                        // report.ChaCharges = row["L1_INITIAL_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row["L1_INITIAL_BASE_PRICE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        // report.GrandTotal = row["BASE_PRICE_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["BASE_PRICE_SAVINGS"]) : 0;
                        report.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        report.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDecimal(row["SERVICE_CHARGES"]) : 0;
                        report.CustomClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_CLEARANCE"]) : 0;
                        report.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDecimal(row["TERMINAL_HANDLING"]) : 0;
                        report.DrawbackAmount = row["DRAWBACK_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["DRAWBACK_AMOUNT"]) : 0;
                        report.LoadingUnloadingCharges = row["LOADING_UNLOADING"] != DBNull.Value ? Convert.ToDouble(row["LOADING_UNLOADING"]) : 0;
                        report.WarehouseStorages = row["WAREHOUSE_STORAGES"] != DBNull.Value ? Convert.ToDouble(row["WAREHOUSE_STORAGES"]) : 0;
                        report.AdditionalMiscExp = row["ADDITIONAL_MISC_EXP"] != DBNull.Value ? Convert.ToDouble(row["ADDITIONAL_MISC_EXP"]) : 0;
                        report.NarcoticSubcharges = row["NARCOTIC_SUBCHARGES"] != DBNull.Value ? Convert.ToDouble(row["NARCOTIC_SUBCHARGES"]) : 0;
                        report.palletizeNumber = row["PALLETIZE_NUMBER"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_NUMBER"]) : 0;
                        report.palletizeQty = row["PALLETIZE_QTY"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
                        report.QTY = row["QUANTITY"] != DBNull.Value ? Convert.ToInt32(row["QUANTITY"]) : 0;
                        report.IsPalletize = row["IS_PALLETIZE"] != DBNull.Value ? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;

                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                LogisticConsolidatedReport recuirementerror = new LogisticConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<ConsolidatedBasePriceReports> consolidatedReports = new List<ConsolidatedBasePriceReports>();
            List<RequirementUsersByRank> revisedReport = new List<RequirementUsersByRank>();
            List<RequirementUsersByRank> initialReport = new List<RequirementUsersByRank>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.UtcNow.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.UtcNow.ToString();
                }

                //DateTime fromDate = Convert.ToDateTime(from);
                //DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", from);
                sd.Add("P_TO_DATE", to);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetConsolidatedBasePriceReports", sd);


                #region Revised Details (Table 1)

                //List<RevisedDetails> revisedDetails = new List<RevisedDetails>();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        RequirementUsersByRank revised = new RequirementUsersByRank();
                        revised.UID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                        revised.RequirementID = row1["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row1["REQ_ID"]) : 0;
                        revised.RevBasePrice = row1["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_BASE_PRICE"]) : 0;
                        revised.InitBasePrice = row1["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["INIT_BASE_PRICE"]) : 0;
                        revised.Rank = row1["RN"] != DBNull.Value ? Convert.ToInt32(row1["RN"]) : 0;
                        revised.Quantity = row1["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row1["QUANTITY"]) : 0;
                        revised.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                        revisedReport.Add(revised);
                    }
                }

                #endregion Revised Details

                #region initial details (table 2)

                // list<initialdetails> initialdetails = new list<initialdetails>();

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        RequirementUsersByRank initial = new RequirementUsersByRank();

                        //U_ID, REQ_ID, REV_BASE_PRICE, INIT_BASE_PRICE, RN, QUANTITY, COMPANY_NAME
                        initial.UID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        initial.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                        initial.RevBasePrice = row2["REV_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_BASE_PRICE"]) : 0;
                        initial.InitBasePrice = row2["INIT_BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["INIT_BASE_PRICE"]) : 0;
                        initial.Rank = row2["RN"] != DBNull.Value ? Convert.ToInt32(row2["RN"]) : 0;
                        initial.Quantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                        // initial.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                        initialReport.Add(initial);
                    }
                }

                #endregion requirement items

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {

                        ConsolidatedBasePriceReports report = new ConsolidatedBasePriceReports();
                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.UID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        report.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        report.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                        // report.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                        report.Closed = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        report.NoOfVendorsInvited = row["NoOfVendorsInvited"] != DBNull.Value ? Convert.ToInt32(row["NoOfVendorsInvited"]) : 0;
                        report.NoOfvendorsParticipated = row["NoOfvendorsParticipated"] != DBNull.Value ? Convert.ToInt32(row["NoOfvendorsParticipated"]) : 0;
                        report.ReqCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;


                        RequirementUsersByRank InitialUser = initialReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL1 = revisedReport.Where(a => a.Rank == 1 && a.RequirementID == report.RequirementID).FirstOrDefault();
                        RequirementUsersByRank RevisedUserL2 = revisedReport.Where(a => a.Rank == 2 && a.RequirementID == report.RequirementID).FirstOrDefault();

                        #region Initial L1 START
                        if (InitialUser != null)
                        {
                            report.IL1_UID = InitialUser.UID;
                            report.IL1_RevBasePrice = InitialUser.RevBasePrice;
                            report.IL1_InitBasePrice = InitialUser.InitBasePrice;
                            report.IL1_L1InitialBasePrice = InitialUser.L1InitialBasePrice;
                            report.IL1_Rank = InitialUser.Rank;
                            report.IL1_Quantity = InitialUser.Quantity;
                            report.IL1_CompanyName = InitialUser.CompanyName;
                        }

                        #endregion Initial L1 END

                        #region Revised RL1 START
                        if (RevisedUserL1 != null)
                        {
                            report.RL1_UID = RevisedUserL1.UID;
                            report.RL1_RevBasePrice = RevisedUserL1.RevBasePrice;
                            report.RL1_InitBasePrice = RevisedUserL1.InitBasePrice;
                            report.RL1_L1InitialBasePrice = RevisedUserL1.L1InitialBasePrice;
                            report.RL1_Rank = RevisedUserL1.Rank;
                            report.RL1_Quantity = RevisedUserL1.Quantity;
                            report.RL1_CompanyName = RevisedUserL1.CompanyName;
                        }

                        #endregion Revised RL1 END

                        #region Revised RL2 START
                        if (RevisedUserL2 != null)
                        {
                            report.RL2_UID = RevisedUserL2.UID;
                            report.RL2_RevBasePrice = RevisedUserL2.RevBasePrice;
                            report.RL2_InitBasePrice = RevisedUserL2.InitBasePrice;
                            report.RL2_L1InitialBasePrice = RevisedUserL2.L1InitialBasePrice;
                            report.RL2_Rank = RevisedUserL2.Rank;
                            report.RL2_Quantity = RevisedUserL2.Quantity;
                            report.RL2_CompanyName = RevisedUserL2.CompanyName;
                            #endregion Revised RL2 END
                        }

                        report.BasePriceSavings = report.IL1_InitBasePrice - report.RL1_RevBasePrice;
                        if (report.BasePriceSavings > 0)
                        {
                            report.BasePriceSavings = Math.Round(report.BasePriceSavings, 2);
                        }
                        else
                        {
                            report.BasePriceSavings = 0;
                        }

                        report.SavingsPercentage = (100 - (report.RL1_RevBasePrice / report.IL1_InitBasePrice) * 100);
                        if (report.SavingsPercentage > 0)
                        {
                            report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        }
                        else
                        {
                            report.SavingsPercentage = 0;
                        }


                        //report.SavingsPercentage = Math.Round(report.SavingsPercentage, 2);
                        //report.RevisedUserL1 = RevisedUserL1;
                        //report.RevisedUserL2 = RevisedUserL2;

                        consolidatedReports.Add(report);
                    }

                    consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();





                }
            }
            catch (Exception ex)
            {
                ConsolidatedBasePriceReports recuirementerror = new ConsolidatedBasePriceReports();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetUserLoginTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Users Login Report

            if (template.ToUpper().Contains("USERS_LOGIN_REPORT"))
            {

                List<ActiveUsers> usersReport = prmservices.GetUsersLoginStatus(userID, from, to, sessionID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("USERS_LOGIN_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "USERS LOGIN REPORT";
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "User ID";
                wsSheet1.Cells["B" + lineNumber].Value = "User Name";
                wsSheet1.Cells["C" + lineNumber].Value = "User E-mail";
                wsSheet1.Cells["D" + lineNumber].Value = "User Phone Number";
                wsSheet1.Cells["E" + lineNumber].Value = "User Login Time";


                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":E" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:E"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in usersReport)
                {


                    wsSheet1.Cells["A" + lineNumber].Value = item.UserID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.FirstName + ' ' + item.LastName;
                    wsSheet1.Cells["C" + lineNumber].Value = item.Email;
                    wsSheet1.Cells["D" + lineNumber].Value = item.PhoneNum;
                    wsSheet1.Cells["E" + lineNumber].Value = item.DateCreated.ToString();

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Users Login Report



            return Convert.ToBase64String(ms.ToArray());
        }

        public string GetLogsTemplates(string template, string from, string to, int companyID, string sessionid)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Email Logs Report

            if (template.ToUpper().Contains("EMAIL_LOGS_REPORT"))
            {

                List<EmailLogs> emailLogs = prmservices.GetEmailLogs(from,to,companyID, sessionid);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("EMAIL_LOGS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "EMAIL LOGS REPORT";
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "REQ ID";
                wsSheet1.Cells["B" + lineNumber].Value = "REQ TYPE";
                wsSheet1.Cells["C" + lineNumber].Value = "REQ STATUS";
                wsSheet1.Cells["D" + lineNumber].Value = "MESSAGE TYPE";
                wsSheet1.Cells["E" + lineNumber].Value = "NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "EMAIL";
                wsSheet1.Cells["G" + lineNumber].Value = "ATL.EMAIL";
                wsSheet1.Cells["H" + lineNumber].Value = "SUBJECT";
                wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Merge = true;
                wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Value = "MESSAGE";
                wsSheet1.Cells["K" + lineNumber].Value = "MESSAGE DATE & TIME";


                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);


                lineNumber++;

                wsSheet1.Cells["A:K"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in emailLogs)
                {
                    if (item.ReqStatus == "UNCONFIRMED")
                    {
                        item.ReqStatus = "Quotations pending";
                    }
                    else if (item.ReqStatus == "NOTSTARTED")
                    {
                        item.ReqStatus = "Negotiation scheduled";
                    }
                    else if (item.ReqStatus == "STARTED")
                    {
                        item.ReqStatus = "Negotiation started";
                    }
                    else if (item.ReqStatus == "Negotiation Ended")
                    {
                        item.ReqStatus = "Negotiation Closed";
                    }
                    else if (item.ReqStatus == "DELETED")
                    {
                        item.ReqStatus = "Negotiation Cancelled";
                    }

                    if (item.Requirement == "VENDOR_REGISTER" || item.Requirement == "COMMUNICATIONS" || item.Requirement == "USER_REGISTER")
                    {

                        item.ReqStatus = "--";
                        item.ReqType = -1;

                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item.ReqID;
                    if (item.ReqType == 0)
                    {
                        wsSheet1.Cells["B" + lineNumber].Value = "PRICE";
                    }
                    // wsSheet1.Cells["B" + lineNumber].Value = item.;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqStatus;
                    wsSheet1.Cells["D" + lineNumber].Value = item.Requirement;
                    wsSheet1.Cells["E" + lineNumber].Value = item.FirstName + ' ' + item.LastName;
                    wsSheet1.Cells["F" + lineNumber].Value = item.Email;
                    wsSheet1.Cells["G" + lineNumber].Value = item.AltEmail;
                    wsSheet1.Cells["H" + lineNumber].Value = item.Subject;
                    wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Merge = true;
                    wsSheet1.Cells["I" + lineNumber + ":J" + lineNumber].Value = item.Message;
                    item.MessageDate = prmservices.toLocal(item.MessageDate);
                    wsSheet1.Cells["K" + lineNumber].Value = item.MessageDate.ToString();

                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;

                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Email Logs Report


            #region SMS Logs Report

            if (template.ToUpper().Contains("SMS_LOGS_REPORT"))
            {

                List<EmailLogs> emailLogs = prmservices.GetSMSLogs(from, to, companyID, sessionid);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("SMS_LOGS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "SMS LOGS REPORT";
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "REQ ID";
                wsSheet1.Cells["B" + lineNumber].Value = "REQ TYPE";
                wsSheet1.Cells["C" + lineNumber].Value = "REQ STATUS";
                wsSheet1.Cells["D" + lineNumber].Value = "MESSAGE TYPE";
                wsSheet1.Cells["E" + lineNumber].Value = "NAME";
                wsSheet1.Cells["F" + lineNumber].Value = "EMAIL";
                wsSheet1.Cells["G" + lineNumber].Value = "ALT.EMAIL";
                wsSheet1.Cells["H" + lineNumber].Value = "PHONE NO.";
                wsSheet1.Cells["I" + lineNumber].Value = "ALT.PHONE NO.";
                // wsSheet1.Cells["G" + lineNumber].Value = "SUBJECT";
                wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Value = "MESSAGE";
                wsSheet1.Cells["L" + lineNumber].Value = "MESSAGE DATE & TIME";


                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":L" + lineNumber].Style.Font.Color.SetColor(Color.White);


                lineNumber++;

                wsSheet1.Cells["A:L"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                var item = emailLogs;

               // foreach (var item in emailLogs)
                    for (int i = 0; i < item.Count; i++)
                    {
                        if (item[i].ReqStatus == "UNCONFIRMED")
                        {
                            item[i].ReqStatus = "Quotations pending";
                        }
                        else if (item[i].ReqStatus == "NOTSTARTED")
                        {
                            item[i].ReqStatus = "Negotiation scheduled";
                        }
                        else if (item[i].ReqStatus == "STARTED")
                        {
                            item[i].ReqStatus = "Negotiation started";
                        }
                        else if (item[i].ReqStatus == "Negotiation Ended")
                        {
                            item[i].ReqStatus = "Negotiation Closed";
                        }
                        else if (item[i].ReqStatus == "DELETED")
                        {
                            item[i].ReqStatus = "Negotiation Cancelled";
                        }

                        wsSheet1.Cells["A" + lineNumber].Value = item[i].ReqID;
                        if (item[i].ReqType == 0)
                        {
                            wsSheet1.Cells["B" + lineNumber].Value = "PRICE";
                        }

                        wsSheet1.Cells["C" + lineNumber].Value = item[i].ReqStatus;
                        wsSheet1.Cells["D" + lineNumber].Value = item[i].Requirement;
                        wsSheet1.Cells["E" + lineNumber].Value = item[i].FirstName + ' ' + item[i].LastName;
                        wsSheet1.Cells["F" + lineNumber].Value = item[i].Email;
                        wsSheet1.Cells["G" + lineNumber].Value = item[i].AltEmail;
                        wsSheet1.Cells["H" + lineNumber].Value = item[i].PhoneNum;
                        wsSheet1.Cells["I" + lineNumber].Value = item[i].AltPhoneNum;
                        //wsSheet1.Cells["G" + lineNumber].Value = item.Subject;
                        wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Merge = true;
                        wsSheet1.Cells["J" + lineNumber + ":K" + lineNumber].Value = item[i].Message;

                        item[i].MessageDate = prmservices.toLocal(item[i].MessageDate);
                        wsSheet1.Cells["L" + lineNumber].Value = item[i].MessageDate.ToString();


                        wsSheet1.Cells.AutoFitColumns();

                        lineNumber++;

                    }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion SMS Logs Report


            return Convert.ToBase64String(ms.ToArray());
        }

        public List<AccountingConsolidatedReport> GetAccountingConsolidatedReports(string sessionID, string from, string to, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<AccountingConsolidatedReport> consolidatedReports = new List<AccountingConsolidatedReport>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID, null);
                if (string.IsNullOrEmpty(from))
                {
                    //from = DateTime.MinValue.ToString();
                    from = DateTime.Now.AddDays(-30).ToString();
                }

                if (string.IsNullOrEmpty(to))
                {
                    //to = DateTime.MaxValue.ToString();
                    to = DateTime.Now.ToString();
                }

                DateTime fromDate = Convert.ToDateTime(from);
                DateTime toDate = Convert.ToDateTime(to);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("rp_GetAccountingConsolidatedReports", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        AccountingConsolidatedReport report = new AccountingConsolidatedReport();

                        report.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        report.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        report.ReqPostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        report.InvoiceNumber = row["INVOICE_NUMBER"] != DBNull.Value ? Convert.ToString(row["INVOICE_NUMBER"]) : string.Empty;
                        report.ConsigneeName = row["CONSIGNEE_NAME"] != DBNull.Value ? Convert.ToString(row["CONSIGNEE_NAME"]) : string.Empty;
                        report.Destination = row["FINAL_DESTINATION"] != DBNull.Value ? Convert.ToString(row["FINAL_DESTINATION"]) : string.Empty;
                        report.ProductName = row["PROD_ID"] != DBNull.Value ? Convert.ToString(row["PROD_ID"]) : string.Empty;
                        report.ShippingMode = row["MODE_OF_SHIPMENT"] != DBNull.Value ? Convert.ToString(row["MODE_OF_SHIPMENT"]) : string.Empty;
                        report.PortOfLanding = row["PORT_OF_LANDING"] != DBNull.Value ? Convert.ToString(row["PORT_OF_LANDING"]) : string.Empty;
                        report.IsPalletize = row["IS_PALLETIZE"] != DBNull.Value ? Convert.ToInt32(row["IS_PALLETIZE"]) : 0;
                        report.FreightBidAmount = row["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_PRICE"]) : 0;
                        report.NetWeight = row["NET_WEIGHT"] != DBNull.Value ? Convert.ToDouble(row["NET_WEIGHT"]) : 0;
                        report.ServiceCharges = row["SERVICE_CHARGES"] != DBNull.Value ? Convert.ToDecimal(row["SERVICE_CHARGES"]) : 0;
                        report.CustomClearance = row["CUSTOM_CLEARANCE"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_CLEARANCE"]) : 0;
                        report.TerminalHandling = row["TERMINAL_HANDLING"] != DBNull.Value ? Convert.ToDecimal(row["TERMINAL_HANDLING"]) : 0;
                        report.DrawbackAmount = row["DRAWBACK_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["DRAWBACK_AMOUNT"]) : 0;
                        report.LoadingUnloadingCharges = row["LOADING_UNLOADING"] != DBNull.Value ? Convert.ToDouble(row["LOADING_UNLOADING"]) : 0;
                        report.WarehouseStorages = row["WAREHOUSE_STORAGES"] != DBNull.Value ? Convert.ToDouble(row["WAREHOUSE_STORAGES"]) : 0;
                        report.AdditionalMiscExp = row["ADDITIONAL_MISC_EXP"] != DBNull.Value ? Convert.ToDouble(row["ADDITIONAL_MISC_EXP"]) : 0;
                        report.NarcoticSubcharges = row["NARCOTIC_SUBCHARGES"] != DBNull.Value ? Convert.ToDouble(row["NARCOTIC_SUBCHARGES"]) : 0;
                        report.palletizeNumber = row["PALLETIZE_NUMBER"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_NUMBER"]) : 0;
                        report.palletizeQty = row["PALLETIZE_QTY"] != DBNull.Value ? Convert.ToDouble(row["PALLETIZE_QTY"]) : 0;
                        report.QTY = row["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row["QUANTITY_IN"]) : string.Empty;
                        if (report.IsPalletize == 1)
                        {
                            report.Pallet = "Palletize";
                        }
                        else if (report.IsPalletize == 0)
                        {
                            report.Pallet = "Non - Palletize";
                        }

                        consolidatedReports.Add(report);
                    }

                    // consolidatedReports = consolidatedReports.Where(r => r.NoOfVendorsInvited > 0).ToList();
                }
            }
            catch (Exception ex)
            {
                AccountingConsolidatedReport recuirementerror = new AccountingConsolidatedReport();
                recuirementerror.ErrorMessage = ex.Message;
                consolidatedReports.Add(recuirementerror);
            }

            return consolidatedReports;
        }

        public string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Accounting Consolidated Report

            if (template.ToUpper().Contains("ACCOUNTING_CONSOLIDATED_REPORT"))
            {

                List<AccountingConsolidatedReport> accountingconsolidateReport = GetAccountingConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("ACCOUNTING_CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "ACCOUNTING CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Invoice No.";
                wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Consignee Name";
                wsSheet1.Cells["E" + lineNumber].Value = "Final Destination";
                wsSheet1.Cells["F" + lineNumber].Value = "Port of Destination";
                wsSheet1.Cells["G" + lineNumber].Value = "Product Name";
                wsSheet1.Cells["H" + lineNumber].Value = "Shipping Mode";
                wsSheet1.Cells["I" + lineNumber].Value = "Division";
                wsSheet1.Cells["J" + lineNumber].Value = "Net Weight";
                wsSheet1.Cells["K" + lineNumber].Value = "Grand Total";

                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":K" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:N"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in accountingconsolidateReport)
                {
                    if (item.IsPalletize == 1)
                    {
                        item.Pallet = "Palletize";
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.Pallet = "Non - Palletize";
                    }

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.InvoiceNumber;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["D" + lineNumber].Value = item.ConsigneeName;
                    wsSheet1.Cells["E" + lineNumber].Value = item.Destination;
                    wsSheet1.Cells["F" + lineNumber].Value = item.PortOfLanding;
                    wsSheet1.Cells["G" + lineNumber].Value = item.ProductName;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ShippingMode;
                    wsSheet1.Cells["I" + lineNumber].Value = item.Pallet;
                    wsSheet1.Cells["J" + lineNumber].Value = item.NetWeight + " " + item.QTY;

                    if (item.IsPalletize == 1)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges + (item.palletizeQty * item.palletizeNumber));
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges);
                    }

                    wsSheet1.Cells["K" + lineNumber].Value = item.GrandTotal;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Accounting Consolidated Report

            #region Consolidated Report

            if (template.ToUpper().Contains("CONSOLIDATED_REPORT"))
            {

                List<LogisticConsolidatedReport> consolidateReport = GetLogisticConsolidatedReports(sessionID, from, to, userID);
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("CONSOLIDATED_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "CONSOLIDATED REPORT";
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Req ID";
                wsSheet1.Cells["B" + lineNumber].Value = "Requirement Title";
                wsSheet1.Cells["C" + lineNumber].Value = "Posted Date";
                wsSheet1.Cells["D" + lineNumber].Value = "Invoice Number";
                wsSheet1.Cells["E" + lineNumber].Value = "Invoice Date";
                wsSheet1.Cells["F" + lineNumber].Value = "Consignee Name";
                wsSheet1.Cells["G" + lineNumber].Value = "Destination";
                wsSheet1.Cells["H" + lineNumber].Value = "Product Name";
                wsSheet1.Cells["I" + lineNumber].Value = "Shipping Mode AIR/SEA";
                wsSheet1.Cells["J" + lineNumber].Value = "Quantity";
                wsSheet1.Cells["K" + lineNumber].Value = "Chargeable Wt";
                wsSheet1.Cells["L" + lineNumber].Value = "DG Fee";
                wsSheet1.Cells["M" + lineNumber].Value = "AMS";
                wsSheet1.Cells["N" + lineNumber].Value = "MISC";
                wsSheet1.Cells["O" + lineNumber].Value = "Menzines Charges";
                wsSheet1.Cells["P" + lineNumber].Value = "CHA Charges";
                wsSheet1.Cells["Q" + lineNumber].Value = "Freight/Bid Amount";
                wsSheet1.Cells["R" + lineNumber].Value = "Grand Total";

                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(119, 136, 153));
                wsSheet1.Cells["A" + lineNumber + ":R" + lineNumber].Style.Font.Color.SetColor(Color.White);

                lineNumber++;

                wsSheet1.Cells["A:R"].AutoFitColumns();
                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;

                foreach (var item in consolidateReport)
                {
                    //if (item.IsPalletize == 1)
                    //{
                    //    item.Pallet = "Palletize";
                    //}
                    //else if (item.IsPalletize == 0)
                    //{
                    //    item.Pallet = "Non - Palletize";
                    //}

                    wsSheet1.Cells["A" + lineNumber].Value = item.RequirementID;
                    wsSheet1.Cells["B" + lineNumber].Value = item.Title;
                    wsSheet1.Cells["C" + lineNumber].Value = item.ReqPostedOn.ToString();
                    wsSheet1.Cells["D" + lineNumber].Value = item.InvoiceNumber;
                    wsSheet1.Cells["E" + lineNumber].Value = item.InvoiceDate;
                    wsSheet1.Cells["F" + lineNumber].Value = item.ConsigneeName;
                    wsSheet1.Cells["G" + lineNumber].Value = item.Destination;
                    wsSheet1.Cells["H" + lineNumber].Value = item.ProductName;
                    wsSheet1.Cells["I" + lineNumber].Value = item.ShippingMode;
                    wsSheet1.Cells["J" + lineNumber].Value = item.QTY;
                    wsSheet1.Cells["K" + lineNumber].Value = item.ChargeableWt;
                    wsSheet1.Cells["L" + lineNumber].Value = item.DGFee;
                    wsSheet1.Cells["M" + lineNumber].Value = item.AMS;
                    wsSheet1.Cells["N" + lineNumber].Value = item.MISC;
                    wsSheet1.Cells["O" + lineNumber].Value = item.MenzinesCharges;
                    wsSheet1.Cells["P" + lineNumber].Value = 2500;
                    wsSheet1.Cells["Q" + lineNumber].Value = item.FreightBidAmount;


                    if (item.IsPalletize == 1)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges + (item.palletizeQty * item.palletizeNumber));
                    }
                    else if (item.IsPalletize == 0)
                    {
                        item.GrandTotal = (item.FreightBidAmount + Convert.ToDouble(item.ServiceCharges) + Convert.ToDouble(item.CustomClearance) + Convert.ToDouble(item.TerminalHandling) + item.DrawbackAmount + item.LoadingUnloadingCharges +
                            item.WarehouseStorages + item.AdditionalMiscExp + item.NarcoticSubcharges);
                    }

                    wsSheet1.Cells["R" + lineNumber].Value = item.GrandTotal;


                    wsSheet1.Cells.AutoFitColumns();

                    lineNumber++;





                }


                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Consolidated Report

            return Convert.ToBase64String(ms.ToArray());
        }

        public CArrayKeyValue[] GetCompanySavingStats(int compid, DateTime fromDate, DateTime toDate, string sessionid)
        {
            List<CArrayKeyValue> list = new List<CArrayKeyValue>();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_COMP_ID", compid);
            sd.Add("P_FROM_DATE", fromDate);
            sd.Add("P_TO_DATE", toDate);

            DataSet ds = sqlHelper.SelectList("cp_GetCompanySavingStats", sd);
            if (ds != null && ds.Tables.Count >= 0)
            {
                if (ds.Tables.Count > 0)
                {
                    //Category Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "CATEGORY";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[0].AsEnumerable())
                    {
                        var key = row["CATEGORY"] != DBNull.Value ? Convert.ToString(row["CATEGORY"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend
                            });
                        }
                    }

                    list.Add(categorySavings);
                }

                if (ds.Tables.Count > 1)
                {
                    //Monthly Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "MONTHLY";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[1].AsEnumerable())
                    {
                        var key = row["MONTH_NAME"] != DBNull.Value ? Convert.ToString(row["MONTH_NAME"]) : string.Empty;
                        var year = row["YEAR1"] != DBNull.Value ? Convert.ToString(row["YEAR1"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend,
                                Year = year
                            });
                        }
                    }

                    list.Add(categorySavings);
                }

                if (ds.Tables.Count > 2)
                {
                    //Monthly Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "DEPARTMENT";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[2].AsEnumerable())
                    {
                        var key = row["DEPARTMENT"] != DBNull.Value ? Convert.ToString(row["DEPARTMENT"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend
                            });
                        }
                    }

                    list.Add(categorySavings);
                }

                if (ds.Tables.Count > 3)
                {
                    //Monthly Savings
                    CArrayKeyValue categorySavings = new CArrayKeyValue();
                    categorySavings.Name = "PLANT_NAME";
                    categorySavings.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in ds.Tables[3].AsEnumerable())
                    {
                        var key = row["PLANT_NAME"] != DBNull.Value ? Convert.ToString(row["PLANT_NAME"]) : string.Empty;
                        var savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDecimal(row["SAVINGS"]) : 0;
                        var spend = row["SPEND"] != DBNull.Value ? Convert.ToDecimal(row["SPEND"]) : 0;
                        if (!string.IsNullOrEmpty(key))
                        {
                            categorySavings.ArrayPair.Add(new KeyValuePair()
                            {
                                Key1 = key,
                                DecimalVal = savings,
                                DecimalVal1 = spend
                            });
                        }
                    }

                    list.Add(categorySavings);
                }
            }

            return list.ToArray();
        }

        #endregion

        #region Post

        #endregion


        #region Private

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        private int ValidateSession(string sessionId)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            if (string.IsNullOrEmpty(sessionId))
            {
                throw new Exception("Session ID is null");
            }
            
            int isValid = 1;
            try
            {
                sd.Add("P_SESSION_ID", sessionId);
                DataSet ds = sqlHelper.SelectList("cp_ValidateSession", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            //catch (MySqlException ex)
            //{
            //    HttpContext.Current.Server.Transfer("/prm360.html#/login");
            //}
            catch (Exception ex)
            {
                isValid = 0;
            }

            if (isValid <= 0)
            {
                throw new Exception("Invalid Session ID passed");
            }

            return isValid;
        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            //PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            //ptmClient.SaveFileBytes(fileContent, fileName);
            Utilities.SaveFile(fileName, fileContent);
        }

        private Response SaveAttachment(string path)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {   
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion

        #region QCS

        public List<QCSDetails> GetQCSList(int uid, int reqid, string sessionid)
        {
            List<QCSDetails> details = new List<QCSDetails>();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                string query = string.Format("SELECT * FROM qcs_details WHERE REQ_ID = {0} ORDER BY QCS_ID DESC", reqid);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;

        }
        public List<QCSDetails> GetQCSIDS(int reqid, string sessionid, int qcsid = 0)
        {
            List<QCSDetails> details = new List<QCSDetails>();

            try
            {
                Utilities.ValidateSession(sessionid);
                var inlineQuery = $@"select * from qcs_details 
                                     WHERE (case when {reqid} > 0 and {qcsid} <= 0 and REQ_ID = {reqid} then 1
                                                 when {qcsid} > 0 and QCS_ID = {qcsid} then 1
                                                 else 0 end) = 1";
                string query = inlineQuery;
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;

        }


        public QCSDetails GetQCSDetails(int qcsid, string sessionid)
        {
            QCSDetails detail = new QCSDetails();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                string query = string.Format("SELECT * FROM qcs_details WHERE QCS_ID = {0}", qcsid);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSDetails> mapper = new CORE.DataNamesMapper<QCSDetails>();
                detail = mapper.Map(dt).FirstOrDefault();
            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }

        public Response SaveQCSDetails(QCSDetails qcsdetails, string sessionid)
        {

            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_QCS_ID", qcsdetails.QCS_ID);
                sd.Add("P_U_ID", qcsdetails.U_ID);
                sd.Add("P_REQ_ID", qcsdetails.REQ_ID);
                sd.Add("P_QCS_CODE", qcsdetails.QCS_CODE);
                sd.Add("P_QCS_TYPE", qcsdetails.QCS_TYPE);
                sd.Add("P_PO_CODE", qcsdetails.PO_CODE);
                sd.Add("P_RECOMMENDATIONS", qcsdetails.RECOMMENDATIONS);
                sd.Add("P_UNIT_CODE", qcsdetails.UNIT_CODE);
                sd.Add("P_WF_ID", qcsdetails.WF_ID);
                sd.Add("P_REQ_JSON", qcsdetails.REQ_JSON);
                sd.Add("P_IS_AUDIT", qcsdetails.IS_AUDIT);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("qcs_SaveQCSDetails", sd);
                //details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                if (qcsdetails.WF_ID > 0 && details.ErrorMessage == string.Empty)
                {
                    SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                    sd1.Clear();
                    sd1.Add("P_REQ_ID", qcsdetails.REQ_ID);
                    sd1.Add("P_U_ID", qcsdetails.U_ID);
                    sd1.Add("P_WF_ID", qcsdetails.WF_ID);
                    sd1.Add("P_IS_NEW_QUOTATION", 0);
                    DataSet ds = sqlHelper.SelectList("cp_QCSMarkAsComplete", sd1);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        
                    }
                }

                /*********** This is for calculation of ranks based on including taxes or not ************/
                //string query = string.Format("UPDATE requirementdetails SET REQ_INCLUSIVE_TAX = {0} WHERE REQ_ID = {1};", qcsdetails.IS_TAX_INCLUDED ? 1 : 0, qcsdetails.REQ_ID);
                //sqlHelper.ExecuteNonQuery_IUD(query);
                /*********** This is for calculation of ranks based on including taxes or not ************/

                if (details.ObjectID > 0 && qcsdetails.WF_ID > 0)
                {
                    PRMWFService pRMWF = new PRMWFService();
                    Response res2 = pRMWF.AssignWorkflow(qcsdetails.WF_ID, Convert.ToInt32(details.ObjectID), qcsdetails.U_ID, sessionid, qcsdetails.REQ_TITLE, qcsdetails.REQ_ID);
                }



            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public string GetQcsReport(string template, int reqid, int qcsID,int userID, string sessionID)
        {
            MemoryStream ms = new MemoryStream();
            byte[] contents = null;

            #region Email Logs Report

            if (template.ToUpper().Contains("QCS_REPORT"))
            {

                QCSDetails qcsDetails = GetQCSDetails(qcsID, sessionID);
                Requirement req = prmservices.GetReportRequirementData(reqid, userID, 1, sessionID);
                Workflow workflow = WF.GetItemWorkflows(0, qcsID, "QCS", sessionID, userID).FirstOrDefault();
                ExcelPackage ExcelPkg = new ExcelPackage();
                ExcelWorksheet wsSheet1 = ExcelPkg.Workbook.Worksheets.Add("QCS_REPORT");

                int lineNumber = 1;

                wsSheet1.Cells["A" + lineNumber].Value = "QUOTATIONS COMPARATIVE STATEMENT";
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Style.Font.Size = 16;
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(51, 122, 183));
                wsSheet1.Cells["A" + lineNumber + ":Z" + lineNumber].Style.Font.Color.SetColor(Color.White);
                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "RFQ ID:";
                wsSheet1.Cells["A" + lineNumber + ":D" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":D" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["E" + lineNumber].Value = req.RequirementID;
                wsSheet1.Cells["E" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                wsSheet1.Cells["E" + lineNumber + ":H" + lineNumber].Merge = true;

                wsSheet1.Cells["I" + lineNumber].Value = "TITLE:";
                wsSheet1.Cells["I" + lineNumber + ":L" + lineNumber].Merge = true;
                wsSheet1.Cells["I" + lineNumber + ":L" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["M".ToString() + lineNumber].Value = req.Title;
                wsSheet1.Cells["M" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                wsSheet1.Cells["M" + lineNumber + ":P" + lineNumber].Merge = true;

                lineNumber++;

                wsSheet1.Cells["A" + lineNumber].Value = "Plant";
                wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Merge = true;
                wsSheet1.Cells["A" + lineNumber + ":B" + lineNumber].Style.Font.Bold = true;

              
                wsSheet1.Cells["C" + lineNumber].Value = qcsID;
                wsSheet1.Cells["C" + lineNumber + ":E" + lineNumber].Merge = true;
                wsSheet1.Cells["C" + lineNumber + ":E" + lineNumber].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                wsSheet1.Cells["C" + lineNumber].Style.Font.Bold = true;

                char cl = 'F';

                foreach (var vend in req.AuctionVendors)
                {
                    

                    char subcl = cl;


                    wsSheet1.Cells[subcl.ToString() + 4].Value = "QTY DIST.";
                   
                    subcl++;
                    wsSheet1.Cells[subcl.ToString() + 4].Value = "QTD RATE";
                    subcl++;
                    wsSheet1.Cells[subcl.ToString() + 4].Value = "DIS (%)";
                    subcl++;
                    wsSheet1.Cells[subcl.ToString() + 4].Value = "Final Rate/Unit";
                    subcl++;
                    wsSheet1.Cells[subcl.ToString() + 4].Value = "Init Total Amount";
                    subcl++;
                    wsSheet1.Cells[subcl.ToString() + 4].Value = "Rev Total Amount";
                    subcl++;
                    wsSheet1.Cells[cl.ToString() + 3].Value = vend.CompanyName;
                    wsSheet1.Cells[cl.ToString() + subcl + 3].Merge = true;
                    wsSheet1.Cells[cl.ToString() + 3].Style.Font.Bold = true;

                    cl = subcl;

                }

                lineNumber++;
                wsSheet1.Cells["A" + lineNumber].Value = "S.NO";
                wsSheet1.Cells["A" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["B" + lineNumber].Value = "HSN / SAC (HSN from RFQ)";
                wsSheet1.Cells["B" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["C" + lineNumber].Value = "ITEM DESCRIPTION (Product name)";
                wsSheet1.Cells["C" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["D" + lineNumber].Value = "QTY UOM";
                wsSheet1.Cells["D" + lineNumber].Style.Font.Bold = true;
                wsSheet1.Cells["E" + lineNumber].Value = "DELIVERY LOCATION";
                wsSheet1.Cells["E" + lineNumber].Style.Font.Bold = true;

                wsSheet1.Protection.IsProtected = false;
                wsSheet1.Protection.AllowSelectLockedCells = false;




                ExcelPkg.SaveAs(ms);
                if (ms != null)
                {
                    contents = ms.ToArray();
                }
            }

            #endregion Email Logs Report


           


            return Convert.ToBase64String(ms.ToArray());
        }
        public List<QCSAudit> GeQcsAudit(int reqID, string sessionid)
        {
            List<QCSAudit> details = new List<QCSAudit>();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                string query = string.Format("SELECT Q.*,concat(U.U_FNAME, ' ',U.U_LNAME) AS APPROVER_NAME FROM qcs_audit_log Q inner join user U on U.U_ID = Q.MODIFIED_BY WHERE REQ_ID = {0}  ORDER BY AUDIT_ID DESC", reqID);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSAudit> mapper = new CORE.DataNamesMapper<QCSAudit>();
                details = mapper.Map(dt).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public QCSAudit GetQcsAuditDetails(int reqID,int qcsID,int verID, string sessionid)
        {
            QCSAudit details = new QCSAudit();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                string query = string.Format("SELECT * FROM qcs_audit_log WHERE REQ_ID = {0} AND QCS_CODE = {1} AND AUDIT_VERSION = {2} ORDER BY AUDIT_ID DESC", reqID, qcsID,verID);
                var dt = sqlHelper.SelectQuery(query);
                CORE.DataNamesMapper<QCSAudit> mapper = new CORE.DataNamesMapper<QCSAudit>();
                details = mapper.Map(dt).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response GetNewQuotations(QCSDetails qcsdetails,bool isNewQuotation, string sessionid)
        {
            Response details = new Response();

            try
            {
                Utilities.ValidateSession(sessionid, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int newQuot = 0;
                if (isNewQuotation == true)
                {
                    newQuot = 1;
                }

                sd.Add("P_U_ID", qcsdetails.U_ID);
                sd.Add("P_REQ_ID", qcsdetails.REQ_ID);
                sd.Add("P_WF_ID", qcsdetails.WF_ID);
                sd.Add("P_IS_NEW_QUOTATION", newQuot);
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("cp_QCSMarkAsComplete", sd);
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    details.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
            }


            return details;
        }


        public Response DeleteQcs(int reqID, int qcsID, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_QCS_ID", qcsID);
                sd.Add("P_IS_ACTIVE", 0);
                DataSet ds = sqlHelper.SelectList("qcs_ActiveDeActiveQcs", sd);
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
                logger.Error("Error in DeleteQcs " + ex.Message);
            }
            return details;
        }

        public Response ActivateQcs(int reqID, int qcsID, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_QCS_ID", qcsID);
                sd.Add("P_IS_ACTIVE", 1);
                DataSet ds = sqlHelper.SelectList("qcs_ActiveDeActiveQcs", sd);
            }
            catch (Exception ex)
            {
                details.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in ActivateQcs " + ex.Message);
            }
            return details;
        }

        public QCSPRDetails GetQcsPRDetails(int reqID, string sessionid)
        {
            QCSPRDetails detail = new QCSPRDetails();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionid);

                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("rp_getPRDetailsByReqID", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];

                    detail.REQ_ID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    detail.RFQ_CREATOR = row["RFQ_CREATOR"] != DBNull.Value ? Convert.ToInt32(row["RFQ_CREATOR"]) : 0;
                    detail.REQ_STATUS = row["REQ_STATUS"] != DBNull.Value ? Convert.ToString(row["REQ_STATUS"]) : string.Empty;
                    detail.REQ_CURRENCY = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;

                }
                detail.VENDOR_DETAILS = new List<vendorCurrency>();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        vendorCurrency vd = new vendorCurrency();
                        vd.U_ID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        vd.VENDOR_NAME = row2["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row2["VENDOR_NAME"]) : string.Empty;
                        vd.VENDOR_CURRENCY = row2["VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row2["VENDOR_CURRENCY"]) : string.Empty;
                        vd.SELECTED_VENDOR_CURRENCY = row2["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row2["SELECTED_VENDOR_CURRENCY"]) : string.Empty;
                        detail.VENDOR_DETAILS.Add(vd);
                    }

                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];

                    detail.PURCHASE_GROUP = row1["PURCHASE_GROUP"] != DBNull.Value ? Convert.ToString(row1["PURCHASE_GROUP"]) : string.Empty;
                    detail.PR_NUMBER = row1["PR_NUMBER"] != DBNull.Value ? Convert.ToString(row1["PR_NUMBER"]) : string.Empty;
                }


            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message;
                logger.Error(ex, "Error in retrieving Data");
            }

            return detail;

        }
        #endregion QCS
    }
}
