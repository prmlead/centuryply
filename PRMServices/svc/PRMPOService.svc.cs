﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
using PRMServices.POPDF;
using PRMServices.POACK;
using System.Net;
using CORE = PRM.Core.Common;
using PRMServices.POCreate;
using Newtonsoft.Json;
using PRMServices.POInvoice;
using test = PRMServices.Models;
using System.Text.RegularExpressions;
using System.Dynamic;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMPOService : IPRMPOService
    {
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        #region DESC

        public POVendor GetDesPoInfo(int reqID, int userID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            POVendor PoObject = new POVendor();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("po_GetDesPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    PoObject = POUtility.GetDescPoObject(row);
                }
            }
            catch (Exception ex)
            {
                PoObject.ErrorMessage = ex.Message;
            }

            return PoObject;
        }

        public List<UserDetails> GetVendors(int reqID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<UserDetails> listUser = new List<UserDetails>();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("po_getVendors", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserDetails user = new UserDetails();
                        user.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        user.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        user.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        user.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        user.Price = row["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["REV_VEND_TOTAL_PRICE"]) : 0;
                        listUser.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDetails user = new UserDetails();
                user.ErrorMessage = ex.Message;
                listUser.Add(user);
            }

            return listUser;
        }

        public DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            DispatchTrack dispatchtrack = new DispatchTrack();
            try
            {
                sd.Add("P_PO_ID", poID);
                sd.Add("P_DT_ID", dtID);
                DataSet ds = sqlHelper.SelectList("po_GetDesDispatchInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    dispatchtrack = POUtility.GetDescDispatchObject(row);
                }
            }
            catch (Exception ex)
            {
                dispatchtrack.ErrorMessage = ex.Message;
            }

            return dispatchtrack;
        }

        public Response SaveDescPoInfo(POVendor povendor)
        {
            Utilities.ValidateSession(povendor.SessionID, null);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (povendor.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName);
                    SaveFile(fileName, povendor.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + povendor.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    povendor.POLink = fileName;
                }
                else
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDescPoInfoEntity(povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdatePOStatus(int poID, string status, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            Response response = new Response();
            try
            {
                string query = string.Format("UPDATE poinformation SET PO_STATUS = '{0}' WHERE PO_ID = {1};", status, poID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                response.ObjectID = 0;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }
        
        public Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID, null);
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (dispatchtrack.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName);
                    SaveFile(fileName, dispatchtrack.POFile.FileStream);
                    fileName = "req" + povendor.ReqID + "_user" + povendor.VendorID + "_" + dispatchtrack.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }
                else if (1 == 0)
                {
                    PRMServices prm = new PRMServices();
                    Requirement req = prm.GetRequirementData(povendor.ReqID, povendor.CreatedBy, povendor.SessionID);
                    Requirement reqVendor = prm.GetRequirementData(povendor.ReqID, povendor.VendorID, povendor.SessionID);
                    UserDetails Vendor = prm.GetUserDetails(povendor.VendorID, povendor.SessionID);
                    UserDetails Customer = prm.GetUserDetails(povendor.CreatedBy, povendor.SessionID);
                    int margin = 12;
                    long tick = DateTime.Now.Ticks;
                    PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateDesPO(povendor, req, reqVendor, Vendor, Customer), PdfSharp.PageSize.A4, margin);
                    pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf"));
                    fileName = "PO_" + povendor.ReqID + "_" + povendor.VendorID + "_" + tick + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    povendor.POLink = fileName;
                }

                DataSet ds = POUtility.SaveDesDispatchTrackObject(dispatchtrack, povendor);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion DESC

        #region ITEM

        public VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPoList", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            VendorPO vendorpo = new VendorPO();
            vendorpo.Vendor = new UserDetails();
            vendorpo.Req = new Requirement();
            vendorpo.ListPOItems = new List<POItems>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetVendorPoInfo", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    vendorpo.Vendor = POUtility.GetVendorPoObject(row);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    vendorpo.Req = POUtility.GetVendorPoReqObject(row1);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        POItems poitems = new POItems();
                        poitems = POUtility.GetVendorPoItemsObject(row2);
                        vendorpo.ListPOItems.Add(poitems);
                    }
                }
            }
            catch (Exception ex)
            {
                vendorpo.ErrorMessage = ex.Message;
            }

            return vendorpo;
        }

        public Response SaveVendorPOInfo(VendorPO vendorpo)
        {
            Utilities.ValidateSession(vendorpo.SessionID, null);
            Response response = new Response();
            Requirement req = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            PRMPRService prmpr = new PRMPRService();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                req = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Req.CustomerID, vendorpo.SessionID);
                vendorreq = prm.GetRequirementData(vendorpo.Req.RequirementID, vendorpo.Vendor.UserID, vendorpo.SessionID);
                vendor = prm.GetUserDetails(vendorpo.Vendor.UserID, vendorpo.SessionID);
                customer = prm.GetUserDetails(vendorpo.Req.CustomerID, vendorpo.SessionID);
                string fileName = string.Empty;


                int poSeries = 0;
                if (vendorpo.ListPOItems[0].POID == 0)
                {
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_MODIFIED_BY", vendorpo.Req.CustomerID);
                    DataSet ds1 = sqlHelper.SelectList("po_genSeries", sd);
                    if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                    {
                        poSeries = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                        // response.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    }
                    vendorpo.PurchaseOrderID = prmpr.generatePRNumber("", "PO", vendorpo.SessionID, vendorpo.ListPOItems[0].COMP_ID, vendorpo.ListPOItems[0].DEPT_ID, vendorpo.ListPOItems[0].PurchaseID);
                }
                else
                {
                    vendorpo.PurchaseOrderID =  vendorpo.ListPOItems[0].PurchaseID;
                }


                if (vendorpo.POFile != null)
                {
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName);
                    SaveFile(fileName, vendorpo.POFile.FileStream);
                    fileName = "req" + req.RequirementID + "_user" + vendor.UserID + "_" + vendorpo.POFile.FileName;
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }
                else
                {
                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateItemizedPO(vendorpo, req, vendorreq, customer, vendor), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf"));
                    //fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTablePo(vendorpo, req, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTablePoToPdf(poTbl, @folderPath + "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, vendorpo);
                    fileName = "vendorPO" + req.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response res = SaveAttachment(fileName);
                    fileName = res.ObjectID.ToString();
                    vendorpo.POLink = fileName;
                }


                foreach (POItems POItem in vendorpo.ListPOItems)
                {
                    POItem.PurchaseID = vendorpo.PurchaseOrderID;

                    DataSet ds = POUtility.SavePOItemEntity(POItem, vendorpo, req, vendor);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }




        //public Response SaveMACVendorPOInfo(MACRequirement vendorpo)
        //{
        //    Utilities.ValidateSession(vendorpo.SessionID, null);
        //    Response response = new Response();
        //    //Requirement req = new Requirement();
        //    //Requirement vendorreq = new Requirement();
        //    //UserDetails customer = new UserDetails();
        //    //UserDetails vendor = new UserDetails();
        //    try
        //    {
        //        foreach (MACRequirementItems macItem in vendorpo.ReqItems)
        //        {
        //            //DataSet ds = POUtility.SaveMACPOItemEntity(macItem);
        //            //if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
        //            //{
        //            //    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
        //            //}
        //            //public static DataSet SaveMACPOItemEntity(MACRequirementItems poitems, int reqID)
        //            //{
        //            //    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //            //    sd.Add("P_PO_ID", 0);
        //            //    sd.Add("P_REQ_ID", reqID);
        //            //    sd.Add("P_VENDOR_ID", poitems.VendorID);
        //            //    sd.Add("P_ITEM_ID", poitems.ItemID);
        //            //    sd.Add("P_VENDOR_PO_QUANTITY", 0);
        //            //    sd.Add("P_PO_QUANTITY", poitems.ProductQuantity);
        //            //    sd.Add("P_EXPECTED_DELIVERY_DATE", poitems.ExpectedDeliveryDate);
        //            //    sd.Add("P_PO_PRICE", poitems.RevVendorUnitPrice);
        //            //    sd.Add("P_PO_COMMENTS", "");
        //            //    sd.Add("P_PO_STATUS", "");
        //            //    sd.Add("P_MODIFIED_BY", 0);
        //            //    sd.Add("P_CREATED_BY", 0);
        //            //    sd.Add("P_PURCHASE_ORDER_ID", poitems.PurchaseID);
        //            //    sd.Add("P_PO_LINK", vendorpo.POLink);
        //            //    sd.Add("P_DELIVERY_ADDR", poitems.DeliveryAddress);
        //            //    sd.Add("P_INDENT_ID", '');
        //            //    sd.Add("P_PO_TOTAL_PRICE", poitems.PoTotalPrice);
        //            //    DataSet ds = sqlHelper.SelectList("po_SavePoInfo", sd);
        //            //    return ds;
        //            //}
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        response.ErrorMessage = ex.Message;
        //    }

        //    return response;
        //}


        public List<DispatchTrack> GetDispatchTrackList(int poID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Utilities.ValidateSession(sessionID, null);
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            Requirement req = new Requirement();
            UserDetails vendor = new UserDetails();
            try
            {
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrackList", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    DataRow row1 = ds.Tables[1].Rows[0];
                    req = POUtility.GetRequirementDetails(row1);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    DataRow row2 = ds.Tables[2].Rows[0];
                    vendor = POUtility.GetVendorDetails(row2);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.VendorPOObject = new VendorPO();
                        dispatchtrack.VendorPOObject.Req = new Requirement();
                        dispatchtrack.VendorPOObject.Req = req;
                        dispatchtrack.VendorPOObject.Vendor = vendor;
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public List<DispatchTrack> GetDispatchTrack(int poID, int dispatchTrackID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<DispatchTrack> listDispatchTrack = new List<DispatchTrack>();
            try
            {
                sd.Add("P_PO_ID", poID);
                sd.Add("P_DT_ID", dispatchTrackID);
                DataSet ds = sqlHelper.SelectList("po_GetDispatchTrack", sd);
                List<POItems> listDispatcPOItems = new List<POItems>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[1].Rows)
                    {
                        POItems dispatchPoItem = new POItems();
                        dispatchPoItem = POUtility.GetDispatchPoItem(row);
                        listDispatcPOItems.Add(dispatchPoItem);
                    }
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DispatchTrack dispatchtrack = new DispatchTrack();
                        dispatchtrack = POUtility.GetDispatchTrackObject(row);
                        dispatchtrack.POItemsEntity = listDispatcPOItems.Where(p => p.PurchaseID == dispatchtrack.PurchaseID).ToList();
                        listDispatchTrack.Add(dispatchtrack);
                    }
                }
            }
            catch (Exception ex)
            {
                DispatchTrack dispatchtrack = new DispatchTrack();
                dispatchtrack.ErrorMessage = ex.Message;
                listDispatchTrack.Add(dispatchtrack);
            }

            return listDispatchTrack;
        }

        public Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType)
        {
            Utilities.ValidateSession(dispatchtrack.SessionID, null);
            PRMNotifications notifications = new PRMNotifications();
            Response response = new Response();
            Requirement newreq = new Requirement();
            Requirement vendorreq = new Requirement();
            UserDetails customer = new UserDetails();
            UserDetails vendor = new UserDetails();
            try
            {
                PRMServices prm = new PRMServices();
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                string fileName = string.Empty;
                newreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, 0, dispatchtrack.SessionID);
                vendorreq = prm.GetRequirementData(dispatchtrack.POItemsEntity[0].ReqID, dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                customer = prm.GetUserDetails(newreq.CustomerID, dispatchtrack.SessionID);
                vendor = prm.GetUserDetails(dispatchtrack.POItemsEntity[0].VendorID, dispatchtrack.SessionID);
                if (requestType == "DISPATCH")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrDispatchXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.DispatchQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPO(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_DispatchCode_" + dispatchtrack.DispatchCode + ".pdf"));


                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableDispatch(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableDispatchToPdf(poTbl, @folderPath + "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "MaterialDispatch" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.DispatchLink = fileName;
                }

                if (requestType == "RECEIVE")
                {
                    string mrrTable = string.Empty;
                    foreach (POItems poItem in dispatchtrack.POItemsEntity)
                    {
                        string xml = string.Empty;
                        xml = notifications.GenerateEmailBody("MrrReceiveXML");
                        xml = String.Format(xml, poItem.ProductIDorName, poItem.VendorPOQuantity, poItem.SumDispatchQuantity, poItem.SumRecivedQuantity, poItem.SumReturnQuantity, poItem.DispatchQuantity, poItem.RecivedQuantity, poItem.ReturnQuantity);
                        mrrTable += xml;

                    }

                    long nowTicks = DateTime.Now.Ticks;
                    int margin = 16;
                    List<KeyValuePair<string, DataTable>> poTbl = new List<KeyValuePair<string, DataTable>>();
                    poTbl = PdfUtilities.MakeDataTableMRR(dispatchtrack, newreq, vendorreq, customer, vendor);
                    PdfUtilities.MakeDataTableMRRToPdf(poTbl, @folderPath + "MRR" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf", customer, vendor, dispatchtrack);
                    fileName = "MRR" + newreq.RequirementID + "_" + vendor.UserID + "_" + nowTicks.ToString() + ".pdf";
                    //PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateMRRPOReportpdf(dispatchtrack, newreq, mrrTable, customer), PdfSharp.PageSize.A4, margin);
                    //pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "PurchaseID_" + dispatchtrack.PurchaseID + "_ReceivedCode_" + dispatchtrack.RecivedCode + ".pdf"));
                    Response responce = SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    dispatchtrack.RecivedLink = Convert.ToInt32(fileName);
                }

                Response res = SaveAttachment(fileName);
                fileName = res.ObjectID.ToString();
                foreach (POItems poItem in dispatchtrack.POItemsEntity)
                {
                    DataSet ds = POUtility.SaveDispatchTrackObject(dispatchtrack, poItem, requestType);
                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            PaymentTrack paymenttrack = new PaymentTrack();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_PO_ID", poID);
                DataSet ds = sqlHelper.SelectList("po_GetPaymentTrack", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    UserDetails Vendor = new UserDetails();
                    DataRow row = ds.Tables[0].Rows[0];
                    Vendor = POUtility.GetVendorPoObject(row);
                    paymenttrack.Vendor = Vendor;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0)
                {
                    Requirement Req = new Requirement();
                    DataRow row1 = ds.Tables[1].Rows[0];
                    Req = POUtility.GetVendorPoReqObject(row1);
                    paymenttrack.Req = Req;
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[2].Rows.Count > 0)
                {
                    POItems PO = new POItems();
                    DataRow row2 = ds.Tables[2].Rows[0];
                    PO = POUtility.GetPoObject(row2);
                    paymenttrack.PO = PO;
                }

                List<DispatchTrack> ListDispatchObject = new List<DispatchTrack>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[3].Rows.Count > 0 && ds.Tables[3].Rows[0][0] != null)
                {
                    foreach (DataRow row3 in ds.Tables[3].Rows)
                    {
                        DispatchTrack DispatchObject = new DispatchTrack();
                        DispatchObject = POUtility.GetPaymentTrackObject(row3);
                        ListDispatchObject.Add(DispatchObject);
                    }

                    paymenttrack.DispatchObject = ListDispatchObject;
                }

                List<PaymentInfo> ListPaymentInfo = new List<PaymentInfo>();
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[4].Rows.Count > 0 && ds.Tables[4].Rows[0][0] != null)
                {
                    foreach (DataRow row4 in ds.Tables[4].Rows)
                    {
                        PaymentInfo paymentinfo = new PaymentInfo();
                        paymentinfo = POUtility.GetPaymentInfoObject(row4);
                        ListPaymentInfo.Add(paymentinfo);
                    }

                    paymenttrack.PaymentInfoObject = ListPaymentInfo;
                }
            }
            catch (Exception ex)
            {
                paymenttrack.ErrorMessage = ex.Message;
            }

            return paymenttrack;
        }

        public Response SavePaymentInfo(PaymentInfo paymentinfo)
        {
            Utilities.ValidateSession(paymentinfo.SessionID, null);
            Response response = new Response();
            try
            {
                DataSet ds = POUtility.SavePaymentInfoObject(paymentinfo);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public bool CheckUniqueIfExists(string param, string idtype, string sessionID)
        {
            bool response = false;
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PARAM", param);
                sd.Add("P_ID_TYPE", idtype);
                DataSet ds = sqlHelper.SelectList("cp_CheckUniqueIfExists", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    int result = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (result > 0)
                    {
                        response = true;
                    }
                    else
                    {
                        response = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return response;
        }

        #endregion ITEM

        #region Private
        
        private void SaveFile(string fileName, byte[] fileContent)
        {
            //PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            //ptmClient.SaveFileBytes(fileContent, fileName);
            //File.WriteAllBytes(fileName, attachment);
            Utilities.SaveFile(fileName, fileContent);
        }

        private Response SaveAttachment(string path)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PATH", path);
                DataSet ds = sqlHelper.SelectList("cp_SaveAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private string GenerateItemizedPO(Requirement req, List<POVendor> poVendors, VendorDetails vendor, string sessionID)
        {
            PRMServices prm = new PRMServices();
            UserDetails customer = prm.GetUserDetails(req.CustomerID, sessionID);
            UserDetails vendorObj = prm.GetUserDetails(vendor.VendorID, sessionID);
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;
            int[] itemsArray = poVendors.Select(p => p.ItemID).ToArray();
            List<RequirementItems> items = req.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();
            string POID = string.Empty;
            string Comments = string.Empty;
            foreach (POVendor item in poVendors)
            {
                Requirement reqForVendor = prm.GetRequirementData(req.RequirementID, vendor.VendorID, sessionID);
                RequirementItems currentItem = reqForVendor.ListRequirementItems.Where(it => it.ItemID == item.ItemID).FirstOrDefault();
                tax = vendor.Taxes;
                RequirementItems selectedItem = items.Where(i => i.ItemID == item.ItemID).FirstOrDefault();
                string tableRows = "<tr>";
                tableRows += "<td>" + item.ProductIDorName + "</td>";
                tableRows += "<td>" + selectedItem.ProductNo + "</td>";
                tableRows += "<td>" + selectedItem.ProductDescription + "</td>";
                tableRows += "<td>" + selectedItem.ProductBrand + "</td>";
                tableRows += "<td>" + (item.Price * item.VendorPOQuantity).ToString() + "</td>";
                tableRows += "</tr>";
                totalPriceRev += Convert.ToDouble(item.Price * item.VendorPOQuantity);
                itemRows += tableRows;
                POID += item.POID;
                Comments += item.Comments + "<br/>";
            }

            Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += vendor.RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, sessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "ItemizedPOText.html")),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                POID.ToString(),
                vendorObj.CompanyName.ToString(), // 7
                vendorObj.Address.ToString(),
                vendorObj.PhoneNum.ToString(),
                vendorObj.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendorObj.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendorObj.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                poVendors[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",              
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendorObj.FirstName.ToString() + " " + vendorObj.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows,
                vendor.RevVendorFreight
                );
            return html1;
        }

        private string GenerateDesPO(POVendor povendor, Requirement req, Requirement reqVendor, UserDetails vendor, UserDetails customer)
        {
            string taxRows = string.Empty;
            double totalPrice = Convert.ToDouble(povendor.Price);
            foreach (RequirementTaxes tax in reqVendor.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=1>" + tax.TaxName + "</td>";
                tableRows += "<td>" + tax.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;                
            }

            string html = string.Empty;
            html = "DescPO.html";

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                povendor.PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),
                req.Description.ToString(), // 13
                reqVendor.AuctionVendors[0].RevPrice.ToString(),
                taxRows,
                reqVendor.AuctionVendors[0].RevVendorFreight.ToString(),
                totalPrice.ToString(),
                povendor.ExpectedDeliveryDate.ToString(),
                povendor.DeliveryAddress.ToString(),
                povendor.Comments.ToString(),
                customer.FirstName.ToString() + " " + customer.LastName.ToString()
               );
            }
            catch
            {

            }

            return htmlRows;
        }

        private string GenerateItemizedPO(VendorPO vendorpo, Requirement req, Requirement vendorreq, UserDetails customer, UserDetails vendor)
        {

            PRMServices prm = new PRMServices();
            string itemRows = string.Empty;
            double tax = 0;
            double totalPriceRev = 0;

            List<POItems> poItems = vendorpo.ListPOItems.Where(i => i.VendorPOQuantity > 0).ToList();                
            int[] itemsArray = poItems.Select(p => p.ItemID).ToArray();

            List<RequirementItems> items = vendorreq.ListRequirementItems.Where(i => itemsArray.Contains(i.ItemID)).ToList();

            DateTime? commonExpectedDeliveryDate = DateTime.Now;
            string commmonDeliveryAddress = "";

            foreach (RequirementItems ri in items)
            {
                 List<POItems> poitems = poItems.Where(i => i.ItemID == ri.ItemID).ToList();

                 if(poitems.Count > 0){

                     POItems poitem = poitems[0];

                     var GST = Convert.ToDouble(poitem.CGst) + Convert.ToDouble(poitem.SGst) + Convert.ToDouble(poitem.IGst);
                     var priceQuantity = Convert.ToDouble(poitem.POPrice) * Convert.ToDouble(poitem.VendorPOQuantity);

                     string tableRows = "<tr>";
                     tableRows += "<td>" + ri.ProductIDorName + "</td>";
                     tableRows += "<td>" + ri.ProductNo + "</td>";
                     tableRows += "<td>" + ri.ProductDescription + "</td>";
                     tableRows += "<td>" + ri.ProductBrand + "</td>";
                     tableRows += "<td>" + poitem.POPrice + "</td>";
                     tableRows += "<td>" + poitem.VendorPOQuantity + "</td>";
                     tableRows += "<td>" + GST + "</td>";
                     tableRows += "<td>" + Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST))) + "</td>";

                     if (!vendorpo.Common)
                     {
                         tableRows += "<td>" + poitem.ExpectedDeliveryDate + "</td>";
                         tableRows += "<td>" + poitem.DeliveryAddress + "</td>";
                     }
                     else
                     {
                         commonExpectedDeliveryDate = poitem.ExpectedDeliveryDate;
                         commmonDeliveryAddress = poitem.DeliveryAddress;
                     }

                     
                     tableRows += "</tr>";



                     totalPriceRev += Convert.ToDouble(priceQuantity + ((priceQuantity / 100) * (GST)));
                     itemRows += tableRows;
                 }               

            }


            string POID = string.Empty;
            string Comments = string.Empty;            

            //Requirement reqVendor = prm.GetRequirementData(req.RequirementID, vendorObj.UserID, sessionID);
            string taxRows = string.Empty;
            double totalPrice = totalPriceRev;
            foreach (RequirementTaxes taxItem in vendorreq.ListRequirementTaxes)
            {
                string tableRows = "<tr>";
                tableRows += "<td colspan=4>" + taxItem.TaxName + "</td>";
                tableRows += "<td>" + taxItem.TaxPercentage + "%</td>";
                tableRows += "</tr>";
                taxRows += tableRows;
                totalPrice += (totalPriceRev * taxItem.TaxPercentage) / 100;
            }
            totalPrice += vendorreq.AuctionVendors[0].RevVendorFreight;

            Credentials tinCred = prm.GetUserCredentials(req.CustomerID, vendorpo.SessionID).FirstOrDefault(v => v.FileType == "TIN");

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");

            //DateTime dateTimeObj = new DateTime();
            //if (req.DeliveryTime != null)
            //{
            //    dateTimeObj = (DateTime)req.DeliveryTime;
            //}

            string pageName = "ItemizedPOText.html";

            if (!vendorpo.Common)
            {
                pageName = "ItemizedPOText.html";
            }
            else
            {
                pageName = "ItemizedPOTextCommon.html";
            }
            


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + pageName)),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString(),
                customer.Address.ToString(),
                customer.PhoneNum.ToString(),
                customer.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                vendorpo.ListPOItems[0].PurchaseID.ToString(),
                vendor.CompanyName.ToString(), // 7
                vendor.Address.ToString(),
                vendor.PhoneNum.ToString(),
                vendor.Email.ToString(),
                DateTime.Now.ToShortDateString(),
                req.Title.ToString(),


                itemRows,
                totalPriceRev.ToString(),
                totalPriceRev.ToString(),
                tax,
                totalPrice,
                !string.IsNullOrEmpty(vendor.Address) ? customer.FirstName + " " + customer.LastName + " - " + vendor.Address.ToString() : customer.FirstName + " " + customer.LastName + " - " + customer.Address.ToString(),
                vendorpo.ListPOItems[0].DeliveryAddress.ToString(),
                //dateTimeObj != null ? dateTimeObj.ToShortDateString() : "No Delivery Date Specified by the Customer",
                req.DeliveryTime != "" ? req.DeliveryTime.ToString() : "No Delivery Date Specified by the Customer",
                "",
                tinCred.CredentialID,
                customer.FirstName.ToString() + " " + customer.LastName.ToString(),
                vendor.FirstName.ToString() + " " + vendor.LastName.ToString(), // 24
                string.IsNullOrEmpty(Comments) ? "" : Comments,
                taxRows,
                vendorreq.AuctionVendors[0].RevVendorFreight,
                vendorpo.ListPOItems[0].IndentID.ToString(), //28
                commonExpectedDeliveryDate.ToString(),
                commmonDeliveryAddress.ToString()
                );
            return html1;
        }

        private string GenerateMRRPO(DispatchTrack dispatchdetails , Requirement newreq , string mrrTable , UserDetails customer)
        {
            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "MRRPDF.html")),
                dispatchdetails.PurchaseID,
                dispatchdetails.IndentID,
                dispatchdetails.DispatchType,
                dispatchdetails.DeliveryTrackID,
                dispatchdetails.DispatchCode,
                dispatchdetails.DispatchMode,
                dispatchdetails.DispatchDate,
                dispatchdetails.DispatchComments,
                newreq.Title,
                mrrTable.ToString(),
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()

                );
            return html1;
        }

        private string GenerateMRRPOReportpdf(DispatchTrack dispatchdetails, Requirement newreq, string mrrTable, UserDetails customer)
        {

            if (string.IsNullOrEmpty(customer.LogoURL))
            {
                customer.LogoURL = "/img/logo.png";
            }

            customer.LogoURL = customer.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");


            string html1 = String.Format(System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "MaterialReceivedReportpdf.html")),
                dispatchdetails.PurchaseID,
                dispatchdetails.IndentID,
                dispatchdetails.DispatchType,
                dispatchdetails.DeliveryTrackID,
                dispatchdetails.DispatchCode,
                dispatchdetails.DispatchMode,
                dispatchdetails.DispatchDate,
                dispatchdetails.DispatchComments,
                newreq.Title,
                mrrTable.ToString(),
                dispatchdetails.RecivedCode,
                dispatchdetails.RecivedBy,
                dispatchdetails.RecivedComments,
                dispatchdetails.RecivedDate,
                "http://prm360.com" + customer.LogoURL,
                customer.CompanyName.ToString()
                );
            return html1;
        }

        #endregion

        public List<GRNDetails> GetGRNDetailsList(int compid, string uid, string search, string supplier, string fromdate, string todate, int page, int pagesize, string sessionid)
        {
            List<GRNDetails> details = new List<GRNDetails>();
            List<GRNDetails> details1 = new List<GRNDetails>();


            try
            {
                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_SUPPLIER", supplier);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                CORE.DataNamesMapper<GRNDetails> mapper = new CORE.DataNamesMapper<GRNDetails>();
                var dataset = sqlHelper.SelectList("po_GetGRNDetailsList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
                details1 = mapper.Map(dataset.Tables[1]).ToList();


                foreach (GRNDetails item in details)
                {

                    if (details1.Count > 0)
                    {
                        GRNDetails matchingItem = details1.FirstOrDefault(i => i.GRN_NUMBER == item.GRN_NUMBER && i.GRN_LINE_ITEM == item.GRN_LINE_ITEM);


                        if (matchingItem != null)
                        {
                            item.IS_AUDIT = 1;
                        }
                        else
                        {
                            item.IS_AUDIT = 0;
                        }
                    }

                   
                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public string GeneratePOPDF(string ponumber, string sessionid)
        {
            string base64String = string.Empty;
            try
            {
                string pdfURL = ConfigurationManager.AppSettings["POPDFURL"].ToString();
                var url = new Uri(pdfURL);
                Utilities.ValidateSession(sessionid);

                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;
                SI_PO_PDF_OBService zMM_PO_PDF = new SI_PO_PDF_OBService();
                DT_PRM360_PO_PDF_REQUEST POInput = new DT_PRM360_PO_PDF_REQUEST();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                ICredentials credentials = netCredential.GetCredential(url, "Basic");
                zMM_PO_PDF.Url = pdfURL;
                zMM_PO_PDF.Credentials = credentials;
                POInput.EBELN = ponumber;
                DT_PRM360_PO_PDF_Response result = zMM_PO_PDF.SI_PO_PDF_OB(POInput);

                if (result != null && result.POFORM != null && result.POFORM.Count() > 0)
                {
                    int count = 1;
                    foreach (var output in result.POFORM)
                    {
                        if (output.POBinary != null && output.POBinary.Length > 0)
                        {
                            logger.Info("output.FORMNAME:" + output.Formname);
                            logger.Info("output.FORMNAME:" + output.POBinary[0]);

                            byte[] byteArray = Enumerable.Range(0, output.POBinary[0].Length)
                               .Where(x => x % 2 == 0)
                               .Select(x => Convert.ToByte(output.POBinary[0].Substring(x, 2), 16))
                               .ToArray();

                            if (count == 1)
                            {
                                base64String = Convert.ToBase64String(byteArray.ToArray());
                            }

                            count++;
                        }
                    }
                }
                else
                {
                    if (result != null && result.POFORM != null)
                    {
                        logger.Info("result.POFORM.Count():" + result.POFORM.Count().ToString());
                    }
                    else
                    {
                        logger.Info("NULL RESPONSE");
                    }
                }

                //logger.Info("POFORM001:" + result.POFORM001);
                //logger.Info("POFORM002:" + result.POFORM002);
                //logger.Info("POFORM002_TC:" + result.POFORM002_TC);
                //logger.Info("POFORM003:" + result.POFORM003);
                //logger.Info("Return:" + result.Return);

                logger.Info("Return:" + base64String);
                return base64String;
                //List<byte> bytes = new List<byte>();
                //foreach (var res in result..T_POPDF_OUTPUT)
                //{
                //    bytes.AddRange(res.LINE);
                //}
                //var temp = Convert.FromBase64String("JVBERsnuSBoUMwMc1VALE68loD2qZlVkST2frDOxegJhEz3DoZ/AYcug9Qxjb2RpRm9udCAgICA1NjEgMjggNTcyIDFuZG9idA0KLzkgMzMzODkgIDcyMnRyZWE0MzUwIEJUICA3MzU1MDMuNjU0MCBtIDMzLjIwNSA2ODAgVGQgMCBnNjQyMDc5NDA1IHcgMDAgVCA0Ljl3IDwzMCBtIDQuNjAxMzQzIDY0NyA2NzEzLjMwNkY3MiAwIGcxNzQ3LjA1IEM0ODIwIDUyMj5UajEzMDNDMzQzNC43NWcNCiAwPlRqIDw1MzAzMDM5NC45IEVUIDcyRTMwIFJHdyAxMTUgdyAgMC43MCBSRzAgMCBRIHENMCBsIHEgMCBxIDAgUyBRIDUgbCA3Ny4wMzAgNDU2My4wIDQ3MTQuNzY1IDQzMDAuMCBtIDE0Ljc4NSA0MCA0MTQuNzUgMzUyOS42NXcgMzUuNzUgUkcgMDAgMCA1IHcgLjc1IFJHIDAwIDAgcSAwIFMgUSAwIGwgcSAwIHEgMCBTIFEgNSBsICAyMjg0LjMwbSA1Ni4xMCAgMjEzMi42NW0gMzAuMDAgIDIxMzEuODUyRDJEdw0KIDUgNjRUIDAgPDIwNTwNCi8gOTQ124WmvPg5XDe+shIXiMpF0bZbB9/AcUUwR0LVXuNE+y5c34KvzweTzzI2RjZUIDUuRT5Uajc0My4gRVQgMkQyRDwyRDI1NkU3MjIwMyBtIDQwIG0gODEuNTMwIDczNTQuNSB3ICAwLjcgdyAxMTE4LjUgdyAgMC43MCBSRzAgMCBRIHENMkQyRDc3LjIgOC4wZCAwIEQyRDJUIDAgDQogNTEwNi4yRDJENjUgMSBFVCAwMjAyNTIuOCBFVA0KL0NvJUFSXyAgICBwZSAvDQowMDQ1MTI=");

                //string base64String;
                //base64String = System.Convert.ToBase64String(bytes.ToArray(), 0, bytes.ToArray().Length);
                //string s = Encoding.UTF8.GetString(bytes.ToArray());
                //string attachName = HttpContext.Current.Server.MapPath("/Services/poattachments/".ToString() + ponumber + ".pdf");
                //SaveFile(attachName, bytes.ToArray());
                ////var byt = File.ReadAllBytes(attachName);
                //return Convert.ToBase64String(bytes.ToArray());
            }

            catch (Exception ex)
            {
                //detail.ErrorMessage = ex.Message + ", URL: " + url;
                logger.Error(ex, ex.Message);
            }

            return null;
        }

        public Response SavePOVendorQuantityAck(string ponumber, string poitemline, string status, string comments, int isVendPoAck, decimal quantity, int user, string sessionid)
        {
            bool isOverall = true;
            Response detail = new Response();
            var url = new Uri(ConfigurationManager.AppSettings["POACK"].ToString());
            try
            {
                Utilities.ValidateSession(sessionid);
                string query = string.Empty;
                query = $@"select * from POScheduleDetails WHERE PO_NUMBER = '{ponumber}'";
                if (!string.IsNullOrEmpty(poitemline))
                {
                    isOverall = false;
                    query += $@" AND PO_LINE_ITEM = '{poitemline}'";
                }

                DataSet dataSet = sqlHelper.ExecuteQuery(query);
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;
                SI_PO_ACKN_OBService z_MM_PO_ACKNWLDGMNT_PRM = new SI_PO_ACKN_OBService();
                List<DT_PRM360_PO_ACKNPO_INFO> acknowledgeInput = new List<DT_PRM360_PO_ACKNPO_INFO>();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                ICredentials credentials = netCredential.GetCredential(url, "Basic");
                z_MM_PO_ACKNWLDGMNT_PRM.Url = ConfigurationManager.AppSettings["POACK"].ToString();
                z_MM_PO_ACKNWLDGMNT_PRM.Credentials = credentials;
                
                if (dataSet != null && dataSet.Tables.Count > 0)
                {
                    foreach (var row in dataSet.Tables[0].AsEnumerable())
                    {
                        DT_PRM360_PO_ACKNPO_INFO ackItem = new DT_PRM360_PO_ACKNPO_INFO();
                        if (isOverall && quantity <= 0 && status.Equals("ACKNOWLEDGE", StringComparison.InvariantCultureIgnoreCase))
                        {
                            quantity = row["ORDER_QTY"] != DBNull.Value ? Convert.ToDecimal(row["ORDER_QTY"]) : 0;
                        }
                        string poNumber = row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                        string poLine = row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PO_LINE_ITEM"]) : string.Empty;
                        string ackStatus = status;// row["VENDOR_ACK_STATUS"] != DBNull.Value ? Convert.ToString(row["VENDOR_ACK_STATUS"]) : string.Empty;
                        DateTime deliveryDate = row["DELIVERY_DATE"] != DBNull.Value ? Convert.ToDateTime(row["DELIVERY_DATE"]) : DateTime.UtcNow;
                        decimal ackQty = Math.Round(quantity, 3);// row["ACK_QTY"] != DBNull.Value ? Convert.ToDecimal(row["ACK_QTY"]) : 0;

                        ackItem.EBELN = poNumber;
                        ackItem.EBELP = poLine;
                        ackItem.DELIV_DATE = deliveryDate.ToString("yyyyMMdd");
                        ackItem.QUANTITY = ackQty.ToString();
                        ackItem.DEL_DATCAT_EXT = (ackStatus == "ACKNOWLEDGE" || ackStatus == "EDIT") ? "1" : "";
                        ackItem.CONF_SER = (ackStatus == "ACKNOWLEDGE" || ackStatus == "EDIT") ? "0001" : "";
                        ackItem.CONF_TYPE = (ackStatus == "ACKNOWLEDGE" || ackStatus == "EDIT") ? "LA" : "";
                        ackItem.DISPO_REL = (ackStatus == "ACKNOWLEDGE" || ackStatus == "EDIT") ? "X" : "";
                        ackItem.POCONFIRMATION = ackStatus == "REJECTED" ? "REJECTED" : "APPROVED" ;
                        acknowledgeInput.Add(ackItem);
                    }
                }

                var result = z_MM_PO_ACKNWLDGMNT_PRM.SI_PO_ACKN_OB(acknowledgeInput.ToArray());

                if (result != null)
                {
                    foreach (var ouput in result)
                    {
                        logger.Info($"Successfully Saved Ack PO NUMBER: {ponumber}, SAP Message {ouput.MESSAGE}");
                    }
                    
                    query = string.Empty;

                    if (status == "ACKNOWLEDGE" || status == "EDIT")
                    {
                        query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', IS_PO_ACK = {isVendPoAck}, ACK_QTY = ORDER_QTY, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}'";
                    }
                    else
                    {
                        query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', IS_PO_ACK = {isVendPoAck}, ACK_QTY = 0, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}'";
                    }

                    if (!string.IsNullOrEmpty(poitemline))
                    {
                        if (status == "ACKNOWLEDGE" || status == "EDIT")
                        {
                            query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', IS_PO_ACK = {isVendPoAck}, ACK_QTY = {quantity}, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}' AND PO_LINE_ITEM = '{poitemline}'";
                        }
                        else
                        {
                            query = $@"UPDATE POScheduleDetails SET VENDOR_ACK_STATUS = '{status}', VENDOR_ACK_COMMENTS = '{comments}', IS_PO_ACK = {isVendPoAck}, ACK_QTY = 0, ACK_DATE = utc_timestamp,
                            DATE_MODIFIED = utc_timestamp, MODIFIED_BY = {user} WHERE PO_NUMBER = '{ponumber}' AND PO_LINE_ITEM = '{poitemline}'";
                        }
                    }

                    logger.Debug("query >>>>>" + query);
                    sqlHelper.ExecuteNonQuery_IUD(query);
                    detail.ObjectID = 1;
                    //detail.Message = result.EX_MESSAGE;
                }

                if (result != null)
                {
                    logger.Info($"Error savingAck PO NUMBER: {ponumber}, SAP Message ");
                    //detail.ErrorMessage = result.EX_MESSAGE;
                    //throw new Exception(result.EX_MESSAGE);
                }
            }
            catch (Exception ex)
            {
                detail.ErrorMessage = ex.Message + ", URL: " + url;
                logger.Error(ex, ex.Message);
            }

            return detail;
        }

        public List<POScheduleDetails> GetPOScheduleList(int compid, int uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
            string fromdate, string todate, int page, int pagesize, int onlycontracts, int excludecontracts, string ackStatus, string buyer, string purchaseGroup, string sessionid)
        {
            List<POScheduleDetails> details = new List<POScheduleDetails>();
            try
            {
                //if (!string.IsNullOrWhiteSpace(supplier) && supplier.Contains(','))
                //{
                //    supplier = string.Join(",", supplier.Split(',').Select(a => "'" + a + "'"));
                //}

                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_CATEGORY_ID", categoryid);
                sd.Add("P_PRODUCT_ID", productid);
                sd.Add("P_SUPPLIER", supplier);
                sd.Add("P_PO_STATUS", postatus);
                sd.Add("P_DELIVERY_STATUS", deliverystatus);
                sd.Add("P_PLANT", plant);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_ONLY_CONTRACTS", onlycontracts);
                sd.Add("P_EXCLUDE_CONTRACTS", excludecontracts);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);
                sd.Add("P_VEND_ACK_STATUS", ackStatus);
                sd.Add("P_BUYER", buyer);
                sd.Add("P_PURCHASE_GROUP", purchaseGroup);
                CORE.DataNamesMapper<POScheduleDetails> mapper = new CORE.DataNamesMapper<POScheduleDetails>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (details != null && details.Count > 0 && dataset.Tables[1].Rows.Count > 0)
                {
                    var row = dataset.Tables[1].Rows[0];
                    details[0].STATS_TOTAL_COUNT = row["TOTAL_COUNT"] != DBNull.Value ? Convert.ToInt32(row["TOTAL_COUNT"]) : 0;
                    details[0].STATS_PO_AWAITING_RECEIPT = row["PO_AWAITING_RECEIPT"] != DBNull.Value ? Convert.ToInt32(row["PO_AWAITING_RECEIPT"]) : 0;
                    details[0].STATS_PO_NOT_INITIATED = row["PO_NOT_INITIATED"] != DBNull.Value ? Convert.ToInt32(row["PO_NOT_INITIATED"]) : 0;
                    details[0].STATS_PO_PARTIAL_DELIVERY = row["PO_PARTIAL_DELIVERY"] != DBNull.Value ? Convert.ToInt32(row["PO_PARTIAL_DELIVERY"]) : 0;
                }

                if (details != null && details.Count > 0)
                {
                    CORE.DataNamesMapper<CompanyGST> gstMapper = new CORE.DataNamesMapper<CompanyGST>();
                    List<CompanyGST> companyGstInfo = new List<CompanyGST>();
                    if (dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                    {
                        companyGstInfo = gstMapper.Map(dataset.Tables[2]).ToList();
                    }

                    CORE.DataNamesMapper<POScheduleDetails> vendorInfoMapper = new CORE.DataNamesMapper<POScheduleDetails>();
                    List<POScheduleDetails> vendorInfo = new List<POScheduleDetails>();
                    if (dataset.Tables.Count > 3 && dataset.Tables[3].Rows.Count > 0)
                    {
                        vendorInfo = vendorInfoMapper.Map(dataset.Tables[3]).ToList();
                    }


                    CORE.DataNamesMapper<POScheduleDetails> asnMapper = new CORE.DataNamesMapper<POScheduleDetails>();
                    List<POScheduleDetails> asnInfo = new List<POScheduleDetails>();
                    if (dataset.Tables.Count > 4 && dataset.Tables[4].Rows.Count > 0)
                    {
                        asnInfo = asnMapper.Map(dataset.Tables[4]).ToList();
                    }

                    foreach (var detail in details)
                    {
                        if (!string.IsNullOrEmpty(detail.ATTACHMENTS))
                        {
                            DataSet ds = sqlHelper.ExecuteQuery($"SELECT * FROM attachmentdetails WHERE ATT_ID IN ({detail.ATTACHMENTS})");
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                detail.AttachmentsArray = new List<FileUpload>();
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    detail.AttachmentsArray.Add(new FileUpload()
                                    {
                                        FileID = Convert.ToInt32(row["ATT_ID"]),
                                        FileName = Convert.ToString(row["ATT_PATH"]),
                                    }); ;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(detail.VENDOR_ATTACHEMNTS))
                        {
                            DataSet ds = sqlHelper.ExecuteQuery($"SELECT * FROM attachmentdetails WHERE ATT_ID IN ({detail.VENDOR_ATTACHEMNTS})");
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                detail.VendorAttachmentsArray = new List<FileUpload>();
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    detail.VendorAttachmentsArray.Add(new FileUpload()
                                    {
                                        FileID = Convert.ToInt32(row["ATT_ID"]),
                                        FileName = Convert.ToString(row["ATT_PATH"]),
                                    }); ;
                                }
                            }
                        }

                        if (companyGstInfo != null && companyGstInfo.Count > 0)
                        {
                            var currentVendorGST = companyGstInfo.Where(g => (!string.IsNullOrEmpty(g.VENDOR_CODE) && g.VENDOR_CODE.Equals(detail.VENDOR_CODE, StringComparison.InvariantCultureIgnoreCase)));
                            if (currentVendorGST != null && currentVendorGST.Count() > 0)
                            {
                                detail.GST_ADDR = currentVendorGST.First().GST_ADDR;
                                detail.GST_NUMBER = currentVendorGST.First().GST_NUMBER;
                            }
                        }

                        if (vendorInfo != null && vendorInfo.Count > 0)
                        {
                            var vendor = vendorInfo.Where(g => (!string.IsNullOrEmpty(g.VENDOR_CODE) && g.VENDOR_CODE.Equals(detail.VENDOR_CODE, StringComparison.InvariantCultureIgnoreCase)));
                            if (vendor == null || vendor.Count() <= 0)
                            {
                                vendor = vendorInfo.Where(g => (g.VENDOR_ID > 0 && g.VENDOR_ID == detail.VENDOR_ID));
                            }

                            if (vendor != null && vendor.Count() > 0)
                            {
                                detail.VENDOR_PRIMARY_PHONE_NUMBER = vendor.ToList()[0].VENDOR_PRIMARY_PHONE_NUMBER;
                                detail.VENDOR_PRIMARY_EMAIL = vendor.ToList()[0].VENDOR_PRIMARY_EMAIL;
                                detail.PAYMENT_TERMS = vendor.ToList()[0].PAYMENT_TERMS;
                                detail.PAYMENT_TERMS_DESC = vendor.ToList()[0].PAYMENT_TERMS_DESC;
                                detail.ADDRESS = vendor.ToList()[0].ADDRESS;
                                detail.VENDOR_CODE = !string.IsNullOrEmpty(detail.VENDOR_CODE) ? detail.VENDOR_CODE : vendor.ToList()[0].VENDOR_CODE;
                            }
                        }

                        if (asnInfo != null && asnInfo.Count > 0)
                        {
                            var asnDetail = asnInfo.Where(g => (!string.IsNullOrEmpty(g.PO_NUMBER) && !string.IsNullOrEmpty(g.ASN_CODE) && g.PO_NUMBER.Equals(detail.PO_NUMBER, StringComparison.InvariantCultureIgnoreCase)));
                            if (asnDetail != null && asnDetail.Count() > 0)
                            {
                                detail.ASN_CODE = asnDetail.ToList()[0].ASN_CODE;
                                detail.ASN_ID = asnDetail.ToList()[0].ASN_ID;

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<POScheduleDetailsItems> GetPOScheduleList_Export(int compid, int uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
            string fromdate, string todate, int page, int pagesize, int onlycontracts, int excludecontracts, string ackStatus, string buyer, string purchaseGroup, string sessionid)
        {
            List<POScheduleDetailsItems> details = new List<POScheduleDetailsItems>();
            try
            {
                search = "%" + search + "%";
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_U_ID", uid);
                sd.Add("P_SEARCH", search);
                sd.Add("P_CATEGORY_ID", categoryid);
                sd.Add("P_PRODUCT_ID", productid);
                sd.Add("P_SUPPLIER", supplier);
                sd.Add("P_PO_STATUS", postatus);
                sd.Add("P_DELIVERY_STATUS", deliverystatus);
                sd.Add("P_PLANT", plant);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_ONLY_CONTRACTS", onlycontracts);
                sd.Add("P_EXCLUDE_CONTRACTS", excludecontracts);
                sd.Add("P_VEND_ACK_STATUS", ackStatus);
                sd.Add("P_BUYER", buyer);
                sd.Add("P_PURCHASE_GROUP", purchaseGroup);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleList_Export", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<CArrayKeyValue> GetPOScheduleFilters(int compid, string sessionid)
        {
            List<CArrayKeyValue> details = new List<CArrayKeyValue>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleFilters", sd);

                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "CATEGORY";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[0].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["CATEGORY_ID"] != DBNull.Value ? Convert.ToInt32(row["CATEGORY_ID"]) : 0;
                        keyValuePair.Value = row["CategoryCode"] != DBNull.Value ? Convert.ToString(row["CategoryCode"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PRODUCT";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[1].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key = row["PRODUCT_ID"] != DBNull.Value ? Convert.ToInt32(row["PRODUCT_ID"]) : 0;
                        keyValuePair.Key1 = row["ProductCode"] != DBNull.Value ? Convert.ToString(row["ProductCode"]) : "";
                        keyValuePair.Value = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 2 && dataset.Tables[2].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "VENDORS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[2].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        keyValuePair.Value = row["VENDOR_COMPANY"] != DBNull.Value ? Convert.ToString(row["VENDOR_COMPANY"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 3 && dataset.Tables[3].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PLANT";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[3].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PLANT"] != DBNull.Value ? Convert.ToString(row["PLANT"]) : "";
                        keyValuePair.Value = row["PLANT_NAME"] != DBNull.Value ? Convert.ToString(row["PLANT_NAME"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 4 && dataset.Tables[4].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PO_STATUS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[4].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PO_STATUS"] != DBNull.Value ? Convert.ToString(row["PO_STATUS"]) : "";
                        keyValuePair.Value = row["PO_STATUS"] != DBNull.Value ? Convert.ToString(row["PO_STATUS"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 5 && dataset.Tables[5].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PO_CREATOR";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[5].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PO_CREATOR"] != DBNull.Value ? Convert.ToString(row["PO_CREATOR"]) : "";
                        keyValuePair.Value = row["PO_CREATOR"] != DBNull.Value ? Convert.ToString(row["PO_CREATOR"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 6 && dataset.Tables[6].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "VENDOR_ACK_STATUS";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[6].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["VENDOR_ACK_STATUS"] != DBNull.Value ? Convert.ToString(row["VENDOR_ACK_STATUS"]) : "";
                        keyValuePair.Value = row["VENDOR_ACK_STATUS"] != DBNull.Value ? Convert.ToString(row["VENDOR_ACK_STATUS"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }

                if (dataset != null && dataset.Tables.Count > 7 && dataset.Tables[7].Rows.Count > 0)
                {
                    CArrayKeyValue key = new CArrayKeyValue();
                    key.Name = "PURCHASE_GROUP";
                    key.ArrayPair = new List<KeyValuePair>();
                    foreach (var row in dataset.Tables[7].AsEnumerable())
                    {
                        KeyValuePair keyValuePair = new KeyValuePair();
                        keyValuePair.Key1 = row["PURCHASE_GROUP"] != DBNull.Value ? Convert.ToString(row["PURCHASE_GROUP"]) : "";
                        keyValuePair.Value = row["PURCHASE_GROUP"] != DBNull.Value ? Convert.ToString(row["PURCHASE_GROUP"]) : "";
                        key.ArrayPair.Add(keyValuePair);
                    }

                    details.Add(key);
                }


            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<POScheduleDetailsItems> GetPOScheduleItems(string ponumber, int moredetails, bool forasn, string sessionid)
        {
            List<POScheduleDetailsItems> details = new List<POScheduleDetailsItems>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", ponumber);
                sd.Add("P_DETAILS", moredetails);
                CORE.DataNamesMapper<POScheduleDetailsItems> mapper = new CORE.DataNamesMapper<POScheduleDetailsItems>();
                var dataset = sqlHelper.SelectList("po_GetPOScheduleItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

                if (dataset.Tables.Count > 1 && dataset.Tables[1].Rows.Count > 0)
                {
                    CORE.DataNamesMapper<GRNItem> grnMapper = new CORE.DataNamesMapper<GRNItem>();
                    List<GRNItem> grnList = grnMapper.Map(dataset.Tables[1]).ToList();
                    if (grnList != null && grnList.Count > 0)
                    {
                        foreach (var po in details)
                        {
                            po.GRNItems = new List<GRNItem>();
                            po.GRNItems = grnList.Where(g => (g.PO_NUMBER == po.PO_NUMBER && g.PO_LINE_ITEM == po.PO_LINE_ITEM)).ToList();
                        }
                    }
                }

                if (forasn)
                {
                    details = details.Where(d => !string.IsNullOrEmpty(d.VENDOR_ACK_STATUS) && d.REMAINING_NET_QTY > 0 && (d.VENDOR_ACK_STATUS == "ACKNOWLEDGE" || d.VENDOR_ACK_STATUS == "APPROVED" || d.VENDOR_ACK_STATUS == "EDIT")).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response PostPOData(POScheduleDetailsItems[] data, bool isDraft)
        {
            Response response = new Response();
            string sapResponse = string.Empty;
            string sapErrorResponse = string.Empty;
            string populatePONumbers = string.Empty;
            SI_PurchaseOrder_OBService poCreateRequest = new SI_PurchaseOrder_OBService();
            DT_PRM360_PurchaseOrder_Request poSAPRequestInput = new DT_PRM360_PurchaseOrder_Request();
            poSAPRequestInput.Header = new DT_PRM360_PurchaseOrder_RequestHeader();
            List<DT_PRM360_PurchaseOrder_RequestHeaderItem> poSAPItems = new List<DT_PRM360_PurchaseOrder_RequestHeaderItem>();
            List<DT_PRM360_PurchaseOrder_RequestHeaderSitem> poSAPSItems = new List<DT_PRM360_PurchaseOrder_RequestHeaderSitem>();

            //logger.Debug("Service started:" + inputJSON);
            Random generator = new Random();
            int requirementId = 0;
            int qcsId = 0;
            int userId = 0;
            int vendorId = 0;
            string quotId = "360-" + generator.Next(0, 999999).ToString("D6");
            string poStatus = isDraft ? "DRAFT" : "SUBMITTING";
            string poNumber = $"CEN-{data[0].VENDOR_ID}-" + DateTime.Now.Ticks.ToString();
            response.Message = poNumber;
            response.ObjectID = 1;

            if (data != null && data.Length > 0)
            {
                requirementId = data[0].REQ_ID;
                qcsId = data[0].QCS_ID;
                userId = data[0].CREATED_BY;
                vendorId = data[0].VENDOR_ID;
            }

            try
            {
                if (data != null && data.Length > 0)
                {
                    logger.Debug("TOTAL PO ENTRIES:" + data.Length.ToString());
                    logger.Debug("UI DATA:" + JsonConvert.SerializeObject(data));

                    DT_PRM360_PurchaseOrder_RequestHeader poHeader = new DT_PRM360_PurchaseOrder_RequestHeader();
                    poHeader.DOC_TYPE = data[0].DOC_TYPE;
                    poHeader.COMP_CODE = "CPIL";
                    poHeader.VENDOR = data[0].VENDOR_CODE;
                    poHeader.PURCH_ORG = "1500";
                    poHeader.PUR_GROUP = data[0].PURCHASE_GROUP_CODE;
                    poHeader.PMNTTRMS = data[0].PAYMENT_TERMS; //"Z005";
                    poHeader.INCOTERMS1 = data[0].INCO_TERMS1; //"FOR";
                    poHeader.INCOTERMS2 = data[0].INCO_TERMS2; //"Free for goods";
                    poHeader.CURRENCY = data[0].CURRENCY;
                    poHeader.HEADER_TEXT = "";
                    poHeader.PO_REL_IND = "X";

                    int poLineItem = 10;
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                    foreach (var dataEntry in data)
                    {
                        DT_PRM360_PurchaseOrder_RequestHeaderItem poEntry = new DT_PRM360_PurchaseOrder_RequestHeaderItem();
                        DT_PRM360_PurchaseOrder_RequestHeaderSitem poEntry1 = new DT_PRM360_PurchaseOrder_RequestHeaderSitem();

                        poEntry.ACCTASSCAT = dataEntry.ACCTASSCAT;
                        poEntry.DELIVERY_DATE = DateTime.Now.ToString("yyyyMMdd"); //"20230103";
                        poEntry.ITEM_CAT = "";
                        poEntry.MATERIAL = dataEntry.PRODUCT_CODE;
                        poEntry.MATL_GROUP = dataEntry.CATEGORY_CODE;
                        poEntry.NET_PRICE = dataEntry.NET_PRICE.ToString();
                        poEntry.PLANT = dataEntry.PLANT; //"5001";
                        poEntry.PO_ITEM = poLineItem.ToString("00000");
                        poEntry.PO_UNIT = dataEntry.UOM;
                        poEntry.PREQ_ITEM = dataEntry.PR_LINE_ITEM;
                        poEntry.PREQ_NO = dataEntry.PR_NUMBER;
                        poEntry.QUANTITY = dataEntry.ORDER_QTY.ToString();
                        poEntry.SHORT_TEXT = "HEADER TXT";
                        poEntry.STEUC = dataEntry.STEUC ?? "";// "17019100";
                        poEntry.STGE_LOC = dataEntry.STGE_LOC ?? "";
                        poEntry.TAX_CODE = string.IsNullOrWhiteSpace(dataEntry.TAX_CODE) ? "V0" : dataEntry.TAX_CODE;

                        //poEntry1.PO_ITEM = poLineItem.ToString("00000");
                        //poEntry1.PREQ_NO = dataEntry.PR_NUMBER;
                        //poEntry1.EXT_LINE = "";
                        //poEntry1.QUANTITY = dataEntry.ORDER_QTY.ToString();
                        //poEntry1.SHORT_TEXT = "HEADER TXT";
                        //poEntry1.BASE_UOM = dataEntry.UOM;
                        //poEntry1.GR_PRICE = dataEntry.NET_PRICE.ToString();
                        //poEntry1.COSTCENTER = "5001COFA";
                        //poEntry1.SERVICE = "3000902";
                        //poEntry1.GL_ACCOUNT = "40600020";
                        //poEntry1.CO_AREA = "";
                        //poEntry1.PRICE_UNIT = "";
                        //poEntry1.ASSET_NO = "";
                        //poEntry1.RES_DOC = "";
                        //poEntry1.RES_ITEM = "";
                        //poEntry1.CMMT_ITEM = "";

                        poLineItem = poLineItem + 10;
                        poSAPItems.Add(poEntry);
                        poSAPSItems.Add(poEntry1);

                        sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_COMP_ID", dataEntry.COMP_ID);
                        sd.Add("P_PLANT", dataEntry.PLANT);
                        sd.Add("P_PRODUCT_ID", dataEntry.PRODUCT_ID);
                        sd.Add("P_DESCRIPTION", dataEntry.PO_MATERIAL_DESC);
                        sd.Add("P_CATEGORY_ID", dataEntry.CATEGORY_ID);
                        sd.Add("P_CATEGORY_CODE", dataEntry.CATEGORY_CODE);// need to pass this value not setting in js
                        sd.Add("P_VENDOR_CODE", dataEntry.VENDOR_CODE);
                        sd.Add("P_VENDOR_ID", dataEntry.VENDOR_ID);
                        sd.Add("P_PO_NUMBER", poNumber);
                        sd.Add("P_PO_LINE_ITEM", poEntry.PO_ITEM);
                        //sd.Add("P_PO_DATE", DateTime.Now);
                        sd.Add("P_PO_CREATOR", dataEntry.PO_CREATOR);
                        sd.Add("P_PAYMENT_TERMS", dataEntry.PAYMENT_TERMS);
                        sd.Add("P_TAX_CODE", dataEntry.TAX_CODE);
                        //sd.Add("P_PO_RELEASE_DATE", DateTime.Now);
                        //sd.Add("P_DELIVERY_DATE", DateTime.Now);
                        sd.Add("P_MATERIAL", dataEntry.PRODUCT_CODE);
                        sd.Add("P_ORDER_QTY", dataEntry.ORDER_QTY);
                        sd.Add("P_UOM", dataEntry.UOM);
                        sd.Add("P_NET_PRICE", dataEntry.NET_PRICE);
                        sd.Add("P_VALUE", dataEntry.NET_PRICE);
                        sd.Add("P_CURRENCY", dataEntry.CURRENCY);
                        sd.Add("P_DOC_TYPE", dataEntry.DOC_TYPE);
                        sd.Add("P_CREATED_BY", dataEntry.CREATED_BY);
                        sd.Add("P_CGST", dataEntry.CGST);
                        sd.Add("P_SGST", dataEntry.SGST);
                        sd.Add("P_IGST", dataEntry.IGST);
                        sd.Add("P_PR_QTY", dataEntry.PR_QTY);
                        sd.Add("P_PR_NUMBER", dataEntry.PR_NUMBER);
                        sd.Add("P_PR_LINE_ITEM", dataEntry.PR_LINE_ITEM);
                        sd.Add("P_INCO_TERMS", dataEntry.INCO_TERMS1);
                        sd.Add("P_VENDOR_COMPANY", dataEntry.VENDOR_COMPANY);
                        sd.Add("P_REQ_ID", dataEntry.REQ_ID);
                        DataSet ds = sqlHelper.SelectList("po_SavePOData", sd);
                    }


                    if (!isDraft || true)
                    {
                        string newURL = ConfigurationManager.AppSettings["POCREATE"].ToString();
                        logger.Debug("SAP PO URL:" + newURL);
                        NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                        Uri uri = new Uri(newURL);
                        ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                        poSAPRequestInput.Header = poHeader;
                        poSAPRequestInput.Header.Item = poSAPItems.ToArray();
                        poSAPRequestInput.Header.Sitem = poSAPSItems.ToArray();
                        List<DT_PRM360_PurchaseOrder_RequestHeaderCondition> conditions = new List<DT_PRM360_PurchaseOrder_RequestHeaderCondition>();
                        poSAPRequestInput.Header.Condition = conditions.ToArray();
                        poCreateRequest.Url = newURL;
                        poCreateRequest.Credentials = credentials;
                        poCreateRequest.PreAuthenticate = true;
                        poStatus = "SUBMITTED";
                        logger.Debug("Output:" + JsonConvert.SerializeObject(poSAPRequestInput));
                        var poOutput = poCreateRequest.SI_PurchaseOrder_OB(poSAPRequestInput);
                        sapResponse = poOutput != null ? JsonConvert.SerializeObject(poOutput) : string.Empty;
                        if (poOutput != null)
                        {
                            bool sendEmail = false;
                            List<POScheduleDetailsItems> prs = new List<POScheduleDetailsItems>();
                            string prmPoNumber = poNumber;

                            foreach (var item in poOutput)
                            {
                                if (!string.IsNullOrEmpty(item.MESSAGE))
                                {
                                    logger.Debug("PRM PO NUMBER:" + poNumber);
                                    logger.Debug("SAP MESSAGE:" + item.MESSAGE);
                                    logger.Debug("SAP EBELN:" + item.EBELN);
                                    logger.Debug("SAP PREQ_NO:" + item.PREQ_NO);
                                    if (item.MSGTY == "E")
                                    {
                                        sapErrorResponse += item.MESSAGE + ",";
                                    }
                                }

                                if (!string.IsNullOrEmpty(item.EBELN) && !string.IsNullOrWhiteSpace(item.PREQ_NO))
                                {
                                    string poNumber1 = item.EBELN.Split('/')[0];
                                    string poNumberLineItem = item.EBELN.Split('/')[1];
                                    string prNumber = item.PREQ_NO.Split('/')[0];
                                    string prLineItem = item.PREQ_NO.Split('/')[1];
                                    POScheduleDetailsItems temp = new POScheduleDetailsItems();
                                    temp.PO_NUMBER = poNumber1;
                                    temp.PO_LINE_ITEM = poNumberLineItem;
                                    temp.PR_NUMBER = prNumber;
                                    temp.PR_LINE_ITEM = prLineItem;
                                    prs.Add(temp);

                                    foreach (var pr in prs)
                                    {
                                        string query1 = $@"UPDATE POSCHEDULEDETAILS SET PO_NUMBER = '{pr.PO_NUMBER}', PO_LINE_ITEM = '{pr.PO_LINE_ITEM}' WHERE PO_NUMBER = '{prmPoNumber}' AND PR_NUMBER = '{pr.PR_NUMBER}' AND PR_LINE_ITEM = '{pr.PR_LINE_ITEM}'";
                                        sqlHelper.ExecuteQuery(query1);
                                        logger.Debug(query1);
                                    }

                                    //sqlHelper.ExecuteQuery($@"UPDATE POSCHEDULEDETAILS SET PO_NUMBER = '{poNumber1}', PO_LINE_ITEM = '{poNumberLineItem}' WHERE PO_NUMBER = '{poNumber}' AND PR_NUMBER = '{prNumber}' AND PR_LINE_ITEM = '{prLineItem}'");
                                    sqlHelper.ExecuteQuery($@"UPDATE requirementdetails SET REQ_STATUS = 'PO Created' WHERE REQ_ID = '{data[0].REQ_ID}'");
                                    sendEmail = true;
                                    poNumber = item.EBELN;
                                    populatePONumbers += item.EBELN + ",";
                                    response.ObjectID = 1;
                                }
                            }
                            //UPdate vendor assign ments
                            if (prs.Count > 0)
                            {
                                DataSet prDetails = sqlHelper.ExecuteQuery($"select pr_number, item_num, product_id From prdetails PR INNER JOIN pritems PRI ON PRI.PR_ID = PR.PR_ID where pr_number in ({string.Join(",", prs.Select(p => "'" + p.PR_NUMBER + "'"))})");
                                if (prDetails != null && prDetails.Tables.Count > 0 && prDetails.Tables[0].Rows.Count > 0)
                                {
                                    foreach (var row in prDetails.Tables[0].AsEnumerable())
                                    {
                                        string prNumberTemp = row["pr_number"] != null && row["pr_number"] != DBNull.Value ? Convert.ToString(row["pr_number"]) : string.Empty;
                                        string prNumberLineTemp = row["item_num"] != null && row["item_num"] != DBNull.Value ? Convert.ToString(row["item_num"]) : string.Empty;
                                        int productId = row["product_id"] != null && row["product_id"] != DBNull.Value ? Convert.ToInt32(row["product_id"]) : 0;
                                        var temp = prs.FirstOrDefault(p => p.PR_NUMBER == prNumberTemp && p.PR_LINE_ITEM == prNumberLineTemp);
                                        if (temp != null)
                                        {
                                            temp.PRODUCT_ID = productId;
                                        }
                                    }

                                    foreach (var pr in prs)
                                    {
                                        string query = $@"update qcs_vendor_item_assignment set PO_NUMBER = '{pr.PO_NUMBER}' where QCS_ID = {qcsId} AND REQ_ID = {requirementId} 
                                                        AND VENDOR_ID = {vendorId} AND PRODUCT_ID = {pr.PRODUCT_ID};";
                                        sqlHelper.ExecuteQuery(query);
                                    }
                                }
                            }


                            if (!string.IsNullOrEmpty(sapErrorResponse))
                            {
                                throw new Exception(sapErrorResponse);
                            }

                            if (!string.IsNullOrEmpty(populatePONumbers))
                            {
                                response.Message = populatePONumbers;
                            }

                            if (sendEmail && data != null && data.Length > 0)
                            {
                                var query = $@"SELECT * FROM watchers WHERE REQ_ID = {data[0].REQ_ID} AND status = 1";
                                var watchers = sqlHelper.ExecuteQuery(query);
                                List<string> cc = new List<string>();
                                if (watchers != null && watchers.Tables.Count > 0 && watchers.Tables[0].Rows.Count > 0)
                                {
                                    foreach (var row in watchers.Tables[0].AsEnumerable())
                                    {
                                        string email = row["email_id"] != null && row["email_id"] != DBNull.Value ? Convert.ToString(row["email_id"]) : string.Empty;
                                        if (!string.IsNullOrEmpty(email) && !cc.Contains(email))
                                        {
                                            cc.Add(email);
                                        }
                                    }
                                }

                                query = $@"select U.U_EMAIL from requirementdetails RD INNER JOIN User U ON U.U_ID = RD.U_ID AND RD.REQ_ID = {data[0].REQ_ID}";
                                var ReqCreator = sqlHelper.ExecuteQuery(query);
                                string reqCreatorEmail = string.Empty;
                                if (ReqCreator != null && ReqCreator.Tables.Count > 0 && ReqCreator.Tables[0].Rows.Count > 0)
                                {
                                    var row = ReqCreator.Tables[0].Rows[0];
                                    reqCreatorEmail = row["U_EMAIL"] != null && row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                                }

                                PRMServices services = new PRMServices();
                                services.SendEmail(reqCreatorEmail, "PO Created successfully: " + poNumber, "PO Created successfully:" + poNumber, data[0].REQ_ID, 0, "", data[0].SessionID, null, null, null, 0, "", string.Join(";", cc));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                poStatus = "ERROR_OCCURED";
                logger.Error("Error occured:" + ex.Message + ", FOR QUOT ID:" + quotId);
                logger.Error("Error occured:" + ex.StackTrace + ", FOR QUOT ID:" + quotId);
                response.ErrorMessage = ex.Message;
                string message = ex.Message.Length < 510 ? ex.Message : ex.Message.Substring(0, 510);
                message = message.Replace("'", "''");

                string temp = ex.Message.Length < 10000 ? ex.Message : ex.Message.Substring(0, 10000);
                temp = temp.Replace("'", "''");
                sqlHelper.ExecuteNonQuery_IUD($@"UPDATE poscheduledetails SET SAP_RESPONSE ='{message}', PO_STATUS = '{poStatus}' WHERE PO_NUMBER = '{poNumber}';");
                sqlHelper.ExecuteNonQuery_IUD($@"INSERT INTO SAPIntegrationErrors SELECT 'PO_CREATE', '{poNumber}', {requirementId}, {qcsId}, {userId}, '{temp}', utc_timestamp(), utc_timestamp(), {userId}, {userId};");
            }

            return response;
        }
        public SAPOEntity[] GetPOGenerateDetails(int compid, string template, int vendorid, string status, string creator,
           string plant, string purchasecode, string search, string sessionid, int page = 0, int pagesize = 0)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_PO_TEMPLATE", template);
                sd.Add("P_VENDOR", vendorid);
                sd.Add("P_STATUS", status);
                sd.Add("P_PLANT", plant);
                sd.Add("P_PURCHASE_GROUP", purchasecode);
                sd.Add("P_CREATOR", creator);
                sd.Add("P_SEARCH", search);
                sd.Add("P_PAGE", page);
                sd.Add("P_PAGE_SIZE", pagesize);

                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.SelectList("PO_GetPOGenerateList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }

        public SAPOEntity[] GetPOItems(string ponumber, string quotno, string sessionid)
        {
            List<SAPOEntity> details = new List<SAPOEntity>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PO_NUMBER", ponumber);
                sd.Add("P_QUOT_NO", quotno);

                CORE.DataNamesMapper<SAPOEntity> mapper = new CORE.DataNamesMapper<SAPOEntity>();
                var dataset = sqlHelper.SelectList("PO_GetPOItems", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

            return details.ToArray();
        }


        #region ASN

        public List<ASNDetails> GetASNDetails(int compid, int asnid, string asncode, string ponumber, string grncode, int vendorid, string sessionid)
        {
            List<ASNDetails> details = new List<ASNDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                asncode = asncode == "0" ? string.Empty : asncode;
                ponumber = ponumber == "0" ? string.Empty : ponumber;
                grncode = grncode == "0" ? string.Empty : grncode;
                string query = $"SELECT * FROM ASNDetails WHERE ";
                string query1 = @"SELECT PO_NUMBER, PO_LINE_ITEM,  ORDER_QTY, SUM(NET_WT) AS NET_WT, ((ORDER_QTY) - SUM(NET_WT)) REMAINING_NET_QTY 
                                FROM ASNDetails WHERE ";

                bool conditions = false;
                if (vendorid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} VENDOR_ID = {vendorid}";
                    query1 += $" VENDOR_ID = {vendorid}";
                    conditions = true;
                }
                if (asnid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} ASN_ID = {asnid}";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(asncode))
                {
                    query += $" {(conditions ? " AND " : "")} ASN_CODE = '{asncode}'";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(ponumber))
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER = '{ponumber}'";
                    query1 += $" {(conditions ? " AND " : "")} PO_NUMBER = '{ponumber}'";
                    conditions = true;
                }

                if (!string.IsNullOrWhiteSpace(grncode))
                {
                    query += $" {(conditions ? " AND " : "")} GRN_CODE = '{grncode}'";
                    conditions = true;
                }

                if (compid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER IN (SELECT PO_NUMBER FROM POScheduleDetails WHERE COMP_ID = {compid})";
                    conditions = true;
                }

                query += $" ORDER BY DATE_MODIFIED DESC";
                query1 += $" GROUP BY PO_NUMBER, PO_LINE_ITEM, ORDER_QTY";

                CORE.DataNamesMapper<ASNDetails> mapper = new CORE.DataNamesMapper<ASNDetails>();
                var dataTable = sqlHelper.SelectQuery(query);
                var dataTable1 = sqlHelper.SelectQuery(query1);
                details = mapper.Map(dataTable).ToList();

                if (!string.IsNullOrWhiteSpace(asncode) && details != null && details.Count > 0)
                {
                    foreach (var detail in details)
                    {
                        if (!string.IsNullOrEmpty(detail.ATTACHMENTS))
                        {
                            DataSet ds = sqlHelper.ExecuteQuery($"SELECT * FROM attachmentdetails WHERE ATT_ID IN ({detail.ATTACHMENTS})");
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                detail.AttachmentsArray = new List<FileUpload>();
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    detail.AttachmentsArray.Add(new FileUpload()
                                    {
                                        FileID = Convert.ToInt32(row["ATT_ID"]),
                                        FileName = Convert.ToString(row["ATT_PATH"]),
                                    }); ;
                                }
                            }
                        }
                    }
                }

                if (dataTable1 != null && dataTable1.Rows.Count > 0 && details != null && details.Count > 0)
                {
                    foreach (var row in dataTable1.AsEnumerable())
                    {
                        decimal remainingQty = row["REMAINING_NET_QTY"] != null && row["REMAINING_NET_QTY"] != DBNull.Value ? Convert.ToDecimal(row["REMAINING_NET_QTY"]) : 0;
                        string poNumber = row["PO_NUMBER"] != null && row["PO_NUMBER"] != DBNull.Value ? Convert.ToString(row["PO_NUMBER"]) : string.Empty;
                        string poLineItem = row["PO_LINE_ITEM"] != null && row["PO_LINE_ITEM"] != DBNull.Value ? Convert.ToString(row["PO_LINE_ITEM"]) : string.Empty;
                        //string asnCode = row["ASN_CODE"] != null && row["ASN_CODE"] != DBNull.Value ? Convert.ToString(row["ASN_CODE"]) : string.Empty;

                        var item = details.Where(d => d.PO_NUMBER == poNumber && d.PO_LINE_ITEM == poLineItem).ToList();
                        if (item != null && item.Count > 0)
                        {
                            item[0].REMAINING_NET_QTY = remainingQty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<ASNDetails> GetASNDetailsList(string ponumber, int vendorid, string sessionid)
        {
            List<ASNDetails> details = new List<ASNDetails>();
            try
            {
                Utilities.ValidateSession(sessionid);
                ponumber = ponumber == "0" ? string.Empty : ponumber;
                string query = $"SELECT ASN_CODE, PO_NUMBER FROM ASNDetails WHERE";

                bool conditions = false;
                if (vendorid > 0)
                {
                    query += $" {(conditions ? " AND " : "")} VENDOR_ID = {vendorid}";
                    conditions = true;
                }
                if (!string.IsNullOrWhiteSpace(ponumber))
                {
                    query += $" {(conditions ? " AND " : "")} PO_NUMBER = '{ponumber}'";
                    conditions = true;
                }

                query += $" GROUP BY ASN_CODE, PO_NUMBER";

                CORE.DataNamesMapper<ASNDetails> mapper = new CORE.DataNamesMapper<ASNDetails>();
                var dataTable = sqlHelper.SelectQuery(query);
                details = mapper.Map(dataTable).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public Response SaveASNDetails(ASNDetails[] detailsarray)
        {
            Response response = new Response();
           
            return response;
        }

        public Response SavePOInvoice(List<POInvoiceDet> INVOICE_ARR, bool doSubmitToSAP,int rowId, string sessionId)
        {
            Response response = new Response();
            SI_Invoice_OBService invReq = new SI_Invoice_OBService();
            DT_PRM360_Invoice_Request poSAPRequestInput = new DT_PRM360_Invoice_Request();
            poSAPRequestInput.Header = new DT_PRM360_Invoice_RequestHeader();
            List<DT_PRM360_Invoice_RequestHeaderItem> poInvItems = new List<DT_PRM360_Invoice_RequestHeaderItem>();
            int userId = 0;
            var query = string.Empty;
            var query1 = string.Empty;
            var invoiceCreatorquery = string.Empty;
            string body = string.Empty;
            string subject = string.Empty;
            PRMServices services = new PRMServices();

            string prmInvoiceNumber = string.Empty;

            if (INVOICE_ARR != null && INVOICE_ARR.Count > 0)
            {
                userId = INVOICE_ARR[0].CreatedBy;
                prmInvoiceNumber = INVOICE_ARR[0].INVOICE_NUMBER;
            }

            try
            {
                Utilities.ValidateSession(sessionId);
                if (INVOICE_ARR != null && INVOICE_ARR.Count > 0 && !doSubmitToSAP)
                {
                    var details = INVOICE_ARR[0];
                    if (details.AttachmentsArray != null && details.AttachmentsArray.Count > 0)
                    {
                        string fileName = string.Empty;
                        foreach (FileUpload fd in details.AttachmentsArray)
                        {
                            if (fd.FileStream != null && !string.IsNullOrWhiteSpace(fd.FileName) && fd.FileID <= 0)
                            {
                                fd.FileName = Regex.Replace(fd.FileName, @"[^0-9a-zA-Z.]+", "PRM_");
                                if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                                {
                                    var attachName = string.Empty;
                                    long tick = DateTime.UtcNow.Ticks;
                                    attachName = HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName);
                                    SaveFile(attachName, fd.FileStream);
                                    attachName = "POINVOICE_" + tick + "_VENDOR_" + details.VENDOR_ID + "_" + fd.FileName;

                                    Response res = SaveAttachment(attachName);
                                    if (res.ErrorMessage != "")
                                    {
                                        response.ErrorMessage = res.ErrorMessage;
                                    }

                                    fd.FileID = res.ObjectID;
                                }
                            }

                            fileName += Convert.ToString(fd.FileID) + ",";
                        }

                        fileName = fileName.Substring(0, fileName.Length - 1);
                        details.ATTACHMENTS = fileName;
                        logger.Info("details.ATTACHMENTS: "  + details.ATTACHMENTS);
                    }
                }

                Response response2 = validateInvoice(INVOICE_ARR);

                if (!string.IsNullOrEmpty(response2.ErrorMessage)) 
                {
                    return response2;
                }

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                DT_PRM360_Invoice_ResponseRETURN[] sapOutput;
                if (doSubmitToSAP)
                {
                    DT_PRM360_Invoice_RequestHeader invHeader = new DT_PRM360_Invoice_RequestHeader();
                    invHeader.DOC_DATE = INVOICE_ARR[0].INVOICE_DATE.HasValue ? INVOICE_ARR[0].INVOICE_DATE.Value.ToString("yyyyMMdd") : DateTime.UtcNow.ToString("yyyyMMdd");
                    invHeader.REF_DOC_NO = INVOICE_ARR[0].INVOICE_NUMBER;
                    invHeader.DIFF_INV = INVOICE_ARR[0].VENDOR_CODE;
                    foreach (var dataEntry in INVOICE_ARR)
                    {
                        DT_PRM360_Invoice_RequestHeaderItem item = new DT_PRM360_Invoice_RequestHeaderItem();
                        item.PO_NUMBER = dataEntry.PO_NUMBER;
                        item.PO_ITEM = dataEntry.PO_LINE_ITEM;
                        item.QUANTITY = dataEntry.QUANTITY.ToString();
                        item.REF_DOC = dataEntry.GRN_NUMBER;
                        item.REF_DOC_YEAR = dataEntry.GRN_YEAR;
                        item.REF_DOC_IT = dataEntry.GRN_LINE_ITEM;
                        poInvItems.Add(item);
                    }

                    logger.Info("Header Values>>>" + invHeader);
                    logger.Info("Item Values>>>" + poInvItems[0]);

                    string newURL = ConfigurationManager.AppSettings["POINVCREATE"].ToString();
                    logger.Debug("SAP INVOICE URL:" + newURL);

                    NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                    Uri uri = new Uri(newURL);
                    ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                    poSAPRequestInput.Header = invHeader;
                    poSAPRequestInput.Header.Item = poInvItems.ToArray();
                    invReq.Url = newURL;
                    invReq.Credentials = credentials;
                    invReq.PreAuthenticate = true;
                    logger.Debug("SAP DATA:" + JsonConvert.SerializeObject(poSAPRequestInput));
                    sapOutput = invReq.SI_Invoice_OB(poSAPRequestInput);

                    if (sapOutput != null)
                    {
                        foreach (var item in sapOutput)
                        {
                            logger.Debug("PO NUMBER" + item.EBELN);
                            logger.Debug("PO LINE ITEM" + item.EBELP);
                            logger.Debug("GRN NUMBER" + item.GRN_NO);
                            logger.Debug("GRN LINE ITEM" + item.GRN_ITEM);
                            logger.Debug("INVOICE NUMBER" + item.INVOICE_NO);
                            logger.Debug("INVOICE LINE ITEM" + item.INVOICE_ITEM);
                        }

                        foreach (var item in INVOICE_ARR)
                        {
                            item.INVOICE_NUMBER = sapOutput[0].INVOICE_NO;
                        }
                    }
                }                

                if (INVOICE_ARR != null && !doSubmitToSAP)
                {
                    foreach (var item in INVOICE_ARR)
                    {
                        
                        sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_VENDOR_CODE", INVOICE_ARR[0].VENDOR_CODE);
                        sd.Add("P_VENDOR_COMPANY", INVOICE_ARR[0].VENDOR_COMPANY);
                        sd.Add("P_VENDOR_ID", INVOICE_ARR[0].VENDOR_ID);
                        sd.Add("P_PRM_INVOICE_NUMBER", prmInvoiceNumber);
                        sd.Add("P_INVOICE_NUMBER", item.INVOICE_NUMBER);
                        sd.Add("P_INVOICE_LINE_ITEM", item.INVOICE_LINE_ITEM ?? item.PO_LINE_ITEM);
                        sd.Add("P_PO_NUMBER", item.PO_NUMBER);
                        sd.Add("P_PO_LINE_ITEM", item.PO_LINE_ITEM);
                        sd.Add("P_GRN_NUMBER", item.GRN_NUMBER);
                        sd.Add("P_GRN_LINE_ITEM", item.GRN_LINE_ITEM);
                        sd.Add("P_CREATED_BY", INVOICE_ARR[0].U_ID);
                        sd.Add("P_INVOICE_DATE", INVOICE_ARR[0].INVOICE_DATE.Value.ToString("yyyy-MM-dd 00:00:00"));
                        sd.Add("P_C_COMP_ID", INVOICE_ARR[0].C_COMP_ID);
                        sd.Add("P_ATTACHMENTS", INVOICE_ARR[0].ATTACHMENTS);
                        sd.Add("P_ROW_ID", rowId);
                        sd.Add("P_INVOICE_ID", item.INVOICE_ID);
                        sd.Add("P_QUANTITY", item.QUANTITY);
                        var dataset = sqlHelper.SelectList("po_SaveInvoiceDetails", sd);

                        if (rowId <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                        {
                            rowId = Convert.ToInt32(dataset.Tables[0].Rows[0]["ROW_ID"]);
                        }
                    }


                    query = $@"Select U.U_EMAIL,U.U_FNAME,U.U_LNAME,U1.U_EMAIL AS VENDOR_EMAIL,U1.U_FNAME AS VENDOR_FIRST_NAME
                            ,U1.U_LNAME AS VENDOR_LAST_NAME from poscheduledetails POSD
                            INNER JOIN USER U on U.U_ID = POSD.CREATED_BY
                            INNER JOIN USER U1 on U1.U_ID = {INVOICE_ARR[0].U_ID}
                            where PO_NUMBER = '{INVOICE_ARR[0].PO_NUMBER}' LIMIT 1";
                    var invoiceCreator = sqlHelper.ExecuteQuery(query);
                    string invoiceVendorEmail = string.Empty;
                    string vendorFirstName = string.Empty;
                    string vendorLastName = string.Empty;
                    string poCreatorEmail = string.Empty;
                    string userFirstName = string.Empty;
                    string userLastName = string.Empty;

                    if (invoiceCreator != null && invoiceCreator.Tables.Count > 0 && invoiceCreator.Tables[0].Rows.Count > 0)
                    {
                        var row = invoiceCreator.Tables[0].Rows[0];
                        invoiceVendorEmail = row["VENDOR_EMAIL"] != null && row["VENDOR_EMAIL"] != DBNull.Value ? Convert.ToString(row["VENDOR_EMAIL"]) : string.Empty;
                        vendorFirstName = row["VENDOR_FIRST_NAME"] != null && row["VENDOR_FIRST_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_FIRST_NAME"]) : string.Empty;
                        vendorLastName = row["VENDOR_LAST_NAME"] != null && row["VENDOR_LAST_NAME"] != DBNull.Value ? Convert.ToString(row["VENDOR_LAST_NAME"]) : string.Empty;
                        poCreatorEmail = row["U_EMAIL"] != null && row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        userFirstName = row["U_FNAME"] != null && row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        userLastName = row["U_LNAME"] != null && row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;

                    }

                    body = services.GenerateEmailBody("InvoiceCreatedEndUserEmail");
                    subject = "Invoice No:-" + INVOICE_ARR[0].INVOICE_NUMBER + " has been Created";

                    logger.Info("Send emailuser1;");
                    body = String.Format(body, userFirstName, userLastName, INVOICE_ARR[0].PRM_INVOICE_NUMBER, vendorFirstName, vendorLastName);
                    services.SendEmail(poCreatorEmail, subject, body, 0, INVOICE_ARR[0].VENDOR_ID, "", sessionId, null, null, null, 0, "", "").ConfigureAwait(false); ;
                    logger.Info("Send emailuser2;");

                }

                if ((INVOICE_ARR[0].INVOICE_STATUS == "REJECTED" || INVOICE_ARR[0].INVOICE_STATUS == "APPROVED"))
                {
                    if (!string.IsNullOrWhiteSpace(INVOICE_ARR[0].PRM_INVOICE_NUMBER))
                    {
                        sd = new SortedDictionary<object, object>() { };
                        sd.Add("P_ROW_ID", rowId);
                        sd.Add("P_U_ID", INVOICE_ARR[0].U_ID);
                        sd.Add("P_INVOICE_STATUS", INVOICE_ARR[0].INVOICE_STATUS);
                        sd.Add("P_INVOICE_COMMENTS", INVOICE_ARR[0].INVOICE_STATUS_COMMENTS);
                        sd.Add("P_INVOICE_NUMBER", INVOICE_ARR[0].INVOICE_NUMBER);

                        var dataset = sqlHelper.SelectList("po_UpdateInvoiceStatus", sd);
                        if (dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                        {
                            response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        }

                       

                        query = $@"select U.U_EMAIL,U.U_FNAME,U.U_LNAME,U1.U_EMAIL AS USER_EMAIL,U1.U_FNAME AS USER_FIRST_NAME
                           ,U1.U_LNAME AS USER_LAST_NAME from User U 
                        INNER JOIN User U1 ON U1.U_ID = {INVOICE_ARR[0].U_ID}
                        where  U.U_ID ={INVOICE_ARR[0].VENDOR_ID} LIMIT 1";
                        var invoiceUser = sqlHelper.ExecuteQuery(query);
                        string invoiceVendorEmail = string.Empty;
                        string vendorFirstName = string.Empty;
                        string vendorLastName = string.Empty;
                        string invoiceUserEmail = string.Empty;
                        string userFirstName = string.Empty;
                        string userLastName = string.Empty;

                        if (invoiceUser != null && invoiceUser.Tables.Count > 0 && invoiceUser.Tables[0].Rows.Count > 0)
                        {
                            var row = invoiceUser.Tables[0].Rows[0];
                            invoiceVendorEmail = row["U_EMAIL"] != null && row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                            vendorFirstName = row["U_FNAME"] != null && row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                            vendorLastName = row["U_LNAME"] != null && row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                            invoiceUserEmail = row["USER_EMAIL"] != null && row["USER_EMAIL"] != DBNull.Value ? Convert.ToString(row["USER_EMAIL"]) : string.Empty;
                            userFirstName = row["USER_FIRST_NAME"] != null && row["USER_FIRST_NAME"] != DBNull.Value ? Convert.ToString(row["USER_FIRST_NAME"]) : string.Empty;
                            userLastName = row["USER_LAST_NAME"] != null && row["USER_LAST_NAME"] != DBNull.Value ? Convert.ToString(row["USER_LAST_NAME"]) : string.Empty;

                        }

                        if (INVOICE_ARR[0].INVOICE_STATUS == "APPROVED")
                        {
                            body = services.GenerateEmailBody("InvoiceApprovedEndUserEmail");
                            subject = "Invoice No:-" + INVOICE_ARR[0].INVOICE_NUMBER + " has been approved";
                        }
                        else if (INVOICE_ARR[0].INVOICE_STATUS == "REJECTED")
                        {
                            body = services.GenerateEmailBody("InvoiceRejectedEndUserEmail");
                            subject = "Invoice No:-" + INVOICE_ARR[0].INVOICE_NUMBER + " has been Rejected";
                        }
                        logger.Info("Send email1;");
                        body = String.Format(body, vendorFirstName, vendorLastName, INVOICE_ARR[0].PRM_INVOICE_NUMBER, userFirstName, userLastName);
                        services.SendEmail(invoiceVendorEmail, subject, body, 0, INVOICE_ARR[0].VENDOR_ID, "", sessionId, null, null, null, 0, "", "").ConfigureAwait(false); ;
                        logger.Info("Send email2;");

                        //string query = $@"UPDATE poinvoicedetails SET INVOICE_STATUS = '{INVOICE_ARR[0].INVOICE_STATUS}', INVOICE_STATUS_COMMENTS = '{INVOICE_ARR[0].INVOICE_STATUS_COMMENTS}' WHERE INVOICE_NUMBER = '{INVOICE_ARR[0].PRM_INVOICE_NUMBER}'";
                        //sqlHelper.ExecuteNonQuery_IUD(query);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error($@"error in SavePOInvoice on {DateTime.Now}" + ex.Message);
                response.ErrorMessage = "Error in saving";
                string temp = ex.Message.Length < 10000 ? ex.Message : ex.Message.Substring(0, 10000);
                temp = temp.Replace("'", "''");
                sqlHelper.ExecuteNonQuery_IUD($@"INSERT INTO SAPIntegrationErrors SELECT 'INVOICE_CREATE', '', 0, 0, {userId}, '{temp}', utc_timestamp(), utc_timestamp(), {userId}, {userId};");
            }

            return response;
        }


        private Response validateInvoice(List<POInvoiceDet> INVOICE_ARR) 
        {
            Response response = new Response();

            foreach (var obj in INVOICE_ARR)
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_GRN_NUMBER", obj.GRN_NUMBER);
                sd.Add("P_GRN_LINE_ITEM", obj.GRN_LINE_ITEM);
                sd.Add("P_U_ID", INVOICE_ARR[0].U_ID);


                var dataset = sqlHelper.SelectList("po_ValidateInvoice", sd);
                if (dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    response.ObjectID = dataset.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dataset.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = dataset.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(dataset.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }

                if (!string.IsNullOrEmpty(response.ErrorMessage))
                {
                    return response;
                }
            }


            return response;
        }


        public Response savePOInvoiceStatus(string invoiceNumber, string invoiceStatus, string invoiceStatusComments, string sessionId)
        {
            Response response = new Response();
            try
            {
                Utilities.ValidateSession(sessionId);
                if (!string.IsNullOrWhiteSpace(invoiceNumber))
                {
                    string query = $@"UPDATE poinvoicedetails SET INVOICE_STATUS = '{invoiceStatus}', INVOICE_STATUS_COMMENTS = '{invoiceStatusComments}' WHERE INVOICE_NUMBER = '{invoiceNumber}'";
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
                else
                {
                    throw new Exception("Invalid Invoice Number.");
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public List<POInvoiceDet> GetInvoiceList(int COMP_ID, int U_ID, string sessionid, string poNumber, string invoiceNumber, string supplier,  string grnNumber, string fromdate, string todate, string search) 
        {
            List<POInvoiceDet> details = new List<POInvoiceDet>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", U_ID);
                sd.Add("P_PO_NUMBER", poNumber);
                sd.Add("P_INVOICE_NUMBER", invoiceNumber);
                sd.Add("P_SUPPLIER_NAME", supplier);
                sd.Add("P_GRN_NUMBER", grnNumber);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_SEARCH", search);
                CORE.DataNamesMapper<POInvoiceDet> mapper = new CORE.DataNamesMapper<POInvoiceDet>();
                var dataset = sqlHelper.SelectList("po_GetInvoicesList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error("error in GetPOInvoice" + ex.Message);
            }
            return details;
        }


        public List<PaymentTracking> GetPaymentInvoiceDetails(string sessionid, int compId, int uId, string poNumber, string grnNumber, string invoiceNumber, string vendorCode, string fromdate, string todate, string search) 
        {
            List<PaymentTracking> details = new List<PaymentTracking>();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_U_ID", uId);
                sd.Add("P_PO_NUMBER", poNumber);
                sd.Add("P_GRN_NUMBER", grnNumber);
                sd.Add("P_INVOICE_NUMBER", invoiceNumber);
                sd.Add("P_VENDOR_CODE", vendorCode);
                sd.Add("P_FROM_DATE", fromdate);
                sd.Add("P_TO_DATE", todate);
                sd.Add("P_SEARCH", search);
                CORE.DataNamesMapper<PaymentTracking> mapper = new CORE.DataNamesMapper<PaymentTracking>();
                var dataset = sqlHelper.SelectList("po_GetPaymentTrackingList", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error("error in GetPaymentInvoiceDetails" + ex.Message);
            }


            return details;
        }



        public List<PaymentTracking> getPaymentTrackingListByFilter(int COMP_ID, int USER_ID, string sessionid)
        {
            List<PaymentTracking> details = new List<PaymentTracking>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", USER_ID);
                CORE.DataNamesMapper<PaymentTracking> mapper = new CORE.DataNamesMapper<PaymentTracking>();
                var dataset = sqlHelper.SelectList("po_GetPaymentTrackingListFilter", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public List<POInvoiceDet> getInvoiceListByFilter(int COMP_ID, int USER_ID, string sessionid)
        {
            List<POInvoiceDet> details = new List<POInvoiceDet>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                sd.Add("P_COMP_ID", COMP_ID);
                sd.Add("P_U_ID", USER_ID);
                CORE.DataNamesMapper<POInvoiceDet> mapper = new CORE.DataNamesMapper<POInvoiceDet>();
                var dataset = sqlHelper.SelectList("po_GetInvoicesListFilter", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SavePOInvoiceForm(test.POInvoice[] poInvDet, int invoiceId)
        {
            Response response = new Response();
            response.ObjectID = 1;
            //string configURL = ConfigurationManager.AppSettings["NEULAND_SAP_INVOICE_URL"].ToString();
            //var url = new Uri(configURL);
            try
            {
                //Utilities.ValidateSession(poInvDet[0].SessionID);

                if (poInvDet != null && poInvDet.Length > 0)
                {
                    //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12 | System.Net.SecurityProtocolType.Tls11 | System.Net.SecurityProtocolType.Tls;
                    //Invoice.Z_INVOICE_SRV_WS _ZMM_Invoice = new Invoice.Z_INVOICE_SRV_WS();
                    //NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                    //ICredentials credentials = netCredential.GetCredential(url, "Basic");
                    //_ZMM_Invoice.Credentials = credentials;
                    //_ZMM_Invoice.Url = configURL;
                    //Invoice.ZFM_INVOICE_CREATE invoiceInput = new Invoice.ZFM_INVOICE_CREATE();
                    //Invoice.ZOPS_MIRO_HEADER inputList1 = new Invoice.ZOPS_MIRO_HEADER();
                    //List<Invoice.ZOPS_MIRO_ITEMS> inputList2 = new List<Invoice.ZOPS_MIRO_ITEMS>();


                    //foreach (var details in poInvDet)
                    //{
                    //    inputList2.Add(new Invoice.ZOPS_MIRO_ITEMS()
                    //    {

                    //        PO_NUMBER = details.PO_NUMBER,
                    //        PO_ITEM = details.PO_LINE_ITEM,
                    //        REF_DOC = details.GRN_NUMBER,
                    //        REF_DOC_YEAR = details.GRN_YEAR,
                    //        REF_DOC_IT = details.GRN_LINE_ITEM,
                    //        QUANTITY = details.INVOICE_QTY
                    //    });
                    //}

                    //inputList1.DOC_DATE = poInvDet[0].INVOICE_DATE.HasValue ? poInvDet[0].INVOICE_DATE.Value.ToString("yyyyMMdd") : DateTime.UtcNow.ToString("yyyyMMdd");
                    //inputList1.REF_DOC_NO = poInvDet[0].INVOICE_NUMBER;
                    //invoiceInput.IM_HEADER = inputList1;
                    //invoiceInput.IM_ITEM = inputList2.ToArray();
                    //var json1 = JsonConvert.SerializeObject(invoiceInput);
                    //System.Xml.XmlDocument items = JsonConvert.DeserializeXmlNode(json1, "root");
                    //logger.Info("invoice payload>>>>" + items.InnerXml);

                    //var result = _ZMM_Invoice.ZFM_INVOICE_CREATE(invoiceInput);
                    //var message = string.Empty;
                    //if (result != null)
                    //{
                    //    if (result.EX_RETURN.Length > 0)//&& result.EX_RETURN[0].MSGTY != "E"
                    //    {
                    //        foreach (var value in result.EX_RETURN)
                    //        {
                    //            if (value.MSGTY != "E")
                    //            {
                    //                try
                    //                {
                    //                    foreach (var details in poInvDet.Where(a => a.GRN_NUMBER == value.GRN_NO && a.GRN_LINE_ITEM == value.GRN_ITEM.ToString().Substring(1) && a.PO_NUMBER == value.EBELN && a.PO_LINE_ITEM == value.EBELP))
                    //                    {
                    //                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    //                        sd.Add("P_INVOICE_ROW_ID", invoiceId);
                    //                        sd.Add("P_V_COMP_ID", details.V_COMP_ID);
                    //                        sd.Add("P_C_COMP_ID", details.C_COMP_ID);
                    //                        sd.Add("P_INVOICE_DATE", details.INVOICE_DATE);
                    //                        sd.Add("P_PRM_INVOICE_NUMBER", details.INVOICE_NUMBER);
                    //                        sd.Add("P_PO_NUMBER", details.PO_NUMBER);
                    //                        sd.Add("P_PO_LINE_ITEM", details.PO_LINE_ITEM);
                    //                        sd.Add("P_GRN_NUMBER", details.GRN_NUMBER);
                    //                        sd.Add("P_GRN_LINE_ITEM", details.GRN_LINE_ITEM);
                    //                        sd.Add("P_INVOICE_QTY", details.INVOICE_QTY);
                    //                        sd.Add("P_SAP_INVOICE_NUMBER", result.EX_RETURN[0].INVOICE_NO);
                    //                        sd.Add("P_SAP_INVOICE_LINE_ITEM", result.EX_RETURN[0].INVOICE_ITEM);
                    //                        sd.Add("P_CREATED_BY", details.U_ID);

                    //                        CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                    //                        var dataset = sqlHelper.SelectList("po_SaveInvoiceDetails", sd);
                    //                        if (invoiceId <= 0 && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                    //                        {
                    //                            invoiceId = Convert.ToInt32(dataset.Tables[0].Rows[0]["INVOICE_ID"]);
                    //                        }
                    //                    }

                    //                }
                    //                catch (Exception ex)
                    //                {
                    //                    logger.Error("error while saving each invoice line item >> " + ex.Message);
                    //                }
                    //            }
                    //            else
                    //            {
                    //                message += value.MESSAGE + Environment.NewLine;
                    //            }
                    //        }
                    //        response.ObjectID = invoiceId;
                    //    }
                    //    else
                    //    {
                    //        response.ErrorMessage = result.EX_RETURN[0].MESSAGE;
                    //        response.ObjectID = -1;
                    //        logger.Error("error Message in saving invoice>> " + result.EX_RETURN[0].MESSAGE);
                    //        return response;
                    //    }

                    //    if (!string.IsNullOrEmpty(message))
                    //    {
                    //        response.ErrorMessage = message;
                    //        response.ObjectID = -2;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
                response.ObjectID = -1;
                logger.Error("error in saving invoice details>>>" + ex.Message);
            }

            return response;
        }

        public string GetInvoiceByID()
        {
            string json = null;

            try
            {
                var parameters = HttpContext.Current.Request.QueryString;

                if (parameters == null && parameters.Count <= 0)
                {
                    throw new Exception($@"Data is empty..");
                }

                if (!string.IsNullOrEmpty(parameters["SESSION_ID"]))
                {
                    Utilities.ValidateSession(parameters["SESSION_ID"]);
                }

                IDictionary<string, object> newObj = new Dictionary<string, object>();
                IDictionary<string, object> newObj1 = new ExpandoObject();
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();


                foreach (string key in parameters.Keys)
                {
                    if (key.ToUpper() != "SESSION_ID" && key.ToUpper() != "TOKEN")
                    {
                        string value = parameters[key];
                        newObj1.Add($"P_{key}", value);
                    }
                }

                var dataset1 = sqlHelper.SelectList("po_GetIndividualInvoice", newObj1);
                if (dataset1 != null && dataset1.Tables.Count > 0 && dataset1.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataset1, Formatting.Indented);
                }


            }
            catch (Exception ex) 
            {
                logger.Error("error in GetInvoiceByID()" + ex.Message);
            }

            return json;

        }


        public string GetInvoiceAudit()
        {
            string json = null;

            try
            {
                var parameters = HttpContext.Current.Request.QueryString;

                if (parameters == null && parameters.Count <= 0)
                {
                    throw new Exception($@"Data is empty..");
                }

                if (!string.IsNullOrEmpty(parameters["SESSION_ID"]))
                {
                    Utilities.ValidateSession(parameters["SESSION_ID"]);
                }

                IDictionary<string, object> newObj = new Dictionary<string, object>();
                IDictionary<string, object> newObj1 = new ExpandoObject();
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();


                foreach (string key in parameters.Keys)
                {
                    if (key.ToUpper() != "SESSION_ID" && key.ToUpper() != "TOKEN")
                    {
                        string value = parameters[key];
                        newObj1.Add($"P_{key}", value);
                    }
                }

                var dataset1 = sqlHelper.SelectList("po_GetInvoiceAudit", newObj1);
                if (dataset1 != null && dataset1.Tables.Count > 0 && dataset1.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataset1, Formatting.Indented);
                }


            }
            catch (Exception ex)
            {
                logger.Error("error in GetInvoiceAudit()" + ex.Message);
            }

            return json;

        }


       
         public string GetInvoiceItems()
          {
            string json = null;

            try
            {
                var parameters = HttpContext.Current.Request.QueryString;

                if (parameters == null && parameters.Count <= 0)
                {
                    throw new Exception($@"Data is empty..");
                }

                if (!string.IsNullOrEmpty(parameters["SESSION_ID"]))
                {
                    Utilities.ValidateSession(parameters["SESSION_ID"]);
                }

                IDictionary<string, object> newObj = new Dictionary<string, object>();
                IDictionary<string, object> newObj1 = new ExpandoObject();
                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();


                foreach (string key in parameters.Keys)
                {
                    if (key.ToUpper() != "SESSION_ID" && key.ToUpper() != "TOKEN")
                    {
                        string value = parameters[key];
                        newObj1.Add($"P_{key}", value);
                    }
                }

                var dataset1 = sqlHelper.SelectList("po_GetInvoiceItemsList", newObj1);
                if (dataset1 != null && dataset1.Tables.Count > 0 && dataset1.Tables[0].Rows.Count > 0)
                {
                    json = JsonConvert.SerializeObject(dataset1, Formatting.Indented);
                }


            }
            catch (Exception ex)
            {
                logger.Error("error in GetInvoiceItems()" + ex.Message);
            }

            return json;

        }




        #endregion
    }
}