﻿using PushSharp.Google;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using System.Text;
using System.Security.Cryptography;
using PRMServices.Models;
using PRMServices.Common;
using MySql.Data.MySqlClient;
using PRMServices;
using PdfSharp.Pdf;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCijIndentService : IPRMCijIndentService
    {
        PRMServices prm = new PRMServices();

        public List<CompanyDeptDesigTypes> GetCompanyDeptDesigTypes(int userId, string type, string sessionID)
        {
            List<CompanyDeptDesigTypes> listTypes = new List<CompanyDeptDesigTypes>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_GetCompanyDeptDesigTypes";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userId));
                        cmd.Parameters.Add(new MySqlParameter("P_TYPE", type));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                CompanyDeptDesigTypes companydeptdesigtypes = new CompanyDeptDesigTypes();

                                companydeptdesigtypes.TypeID = row["T_ID"] != DBNull.Value ? Convert.ToInt32(row["T_ID"]) : 0;
                                companydeptdesigtypes.TypeName = row["T_NAME"] != DBNull.Value ? Convert.ToString(row["T_NAME"]) : string.Empty;

                                listTypes.Add(companydeptdesigtypes);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CompanyDeptDesigTypes companydeptdesigtypes = new CompanyDeptDesigTypes();
                        companydeptdesigtypes.ErrorMessage = ex.Message;
                        listTypes.Add(companydeptdesigtypes);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listTypes;
        }

        public Response SaveWorkCategory(List<BudgetCodeEntity> listWorkCategories, string sessionID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        foreach (BudgetCodeEntity WC in listWorkCategories)
                        {
                            cmd.CommandText = "cp_SaveWorkCategory";

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new MySqlParameter("P_U_ID", WC.UserID));
                            cmd.Parameters.Add(new MySqlParameter("P_WC_ID", WC.WorkCatID));
                            cmd.Parameters.Add(new MySqlParameter("P_CAT_CODE", WC.WorkCatCode));
                            cmd.Parameters.Add(new MySqlParameter("P_CAT_DESC", WC.WorkCatDesc));
                            cmd.Parameters.Add(new MySqlParameter("P_DEPT_ID", WC.DeptID));
                            cmd.Parameters.Add(new MySqlParameter("P_IS_VALID", WC.IsValid));

                            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                            DataSet ds = new DataSet();
                            myDA.Fill(ds);
                            cmd.Connection.Close();

                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                            }
                            cmd.Parameters.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public Response SaveBudgetCode(List<BudgetCodeEntity> listBudgetCodes, string sessionID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        foreach (BudgetCodeEntity BC in listBudgetCodes)
                        {
                            cmd.CommandText = "cp_SaveBudgetCode";

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new MySqlParameter("P_U_ID", BC.UserID));
                            cmd.Parameters.Add(new MySqlParameter("P_BC_ID", BC.BudgetID));
                            cmd.Parameters.Add(new MySqlParameter("P_BUDGET_CODE", BC.BudgetCode));
                            cmd.Parameters.Add(new MySqlParameter("P_BUDGET_DESC", BC.BudgetDesc));
                            cmd.Parameters.Add(new MySqlParameter("P_DEPT_ID", BC.DeptID));
                            cmd.Parameters.Add(new MySqlParameter("P_WC_ID", BC.WorkCatID));
                            cmd.Parameters.Add(new MySqlParameter("P_IS_VALID", BC.IsValid));

                            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                            DataSet ds = new DataSet();
                            myDA.Fill(ds);
                            cmd.Connection.Close();

                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                            }
                            cmd.Parameters.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public List<BudgetCodeEntity> GetWorkCategories(int userID, string sessionID)
        {
            List<BudgetCodeEntity> listWorkCategories = new List<BudgetCodeEntity>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        cmd.CommandText = "cp_GetWorkCategories";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                BudgetCodeEntity workCategory = new BudgetCodeEntity();

                                workCategory.WorkCatID = row["WC_ID"] != DBNull.Value ? Convert.ToInt32(row["WC_ID"]) : 0;
                                workCategory.WorkCatCode = row["CAT_CODE"] != DBNull.Value ? Convert.ToString(row["CAT_CODE"]) : string.Empty;
                                workCategory.WorkCatDesc = row["CAT_DESC"] != DBNull.Value ? Convert.ToString(row["CAT_DESC"]) : string.Empty;
                                workCategory.IsValid = row["IS_VALID"] != DBNull.Value ? Convert.ToInt32(row["IS_VALID"]) : 0;
                                workCategory.DeptID = row["DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["DEPT_ID"]) : 0;
                                workCategory.DeptTypeID = row["TYPE_ID"] != DBNull.Value ? Convert.ToInt32(row["TYPE_ID"]) : 0;
                                workCategory.DeptCode = row["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row["DEPT_CODE"]) : string.Empty;

                                listWorkCategories.Add(workCategory);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        BudgetCodeEntity workCategory = new BudgetCodeEntity();
                        workCategory.ErrorMessage = ex.Message;
                        listWorkCategories.Add(workCategory);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listWorkCategories;
        }

        public List<BudgetCodeEntity> GetBudgetCodes(int userID, string sessionID)
        {
            List<BudgetCodeEntity> listBudgetCodes = new List<BudgetCodeEntity>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        cmd.CommandText = "cp_GetBudgetCodes";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                BudgetCodeEntity BudgetCode = new BudgetCodeEntity();

                                BudgetCode.BudgetID = row["BC_ID"] != DBNull.Value ? Convert.ToInt32(row["BC_ID"]) : 0;
                                BudgetCode.BudgetCode = row["BUDGET_CODE"] != DBNull.Value ? Convert.ToString(row["BUDGET_CODE"]) : string.Empty;
                                BudgetCode.BudgetDesc = row["BUDGET_DESC"] != DBNull.Value ? Convert.ToString(row["BUDGET_DESC"]) : string.Empty;
                                BudgetCode.IsValid = row["IS_VALID"] != DBNull.Value ? Convert.ToInt32(row["IS_VALID"]) : 0;
                                BudgetCode.DeptID = row["DEPT_ID"] != DBNull.Value ? Convert.ToInt32(row["DEPT_ID"]) : 0;
                                BudgetCode.DeptTypeID = row["TYPE_ID"] != DBNull.Value ? Convert.ToInt32(row["TYPE_ID"]) : 0;
                                BudgetCode.DeptCode = row["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row["DEPT_CODE"]) : string.Empty;
                                BudgetCode.WorkCatID = row["WC_ID"] != DBNull.Value ? Convert.ToInt32(row["WC_ID"]) : 0;
                                BudgetCode.WorkCatCode = row["CAT_CODE"] != DBNull.Value ? Convert.ToString(row["CAT_CODE"]) : string.Empty;
                                BudgetCode.WorkCatDesc = row["CAT_DESC"] != DBNull.Value ? Convert.ToString(row["CAT_DESC"]) : string.Empty;

                                listBudgetCodes.Add(BudgetCode);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        BudgetCodeEntity BudgetCode = new BudgetCodeEntity();
                        BudgetCode.ErrorMessage = ex.Message;
                        listBudgetCodes.Add(BudgetCode);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listBudgetCodes;
        }

        public List<Requirement> GetAssignedIndents(int userID, string sessionID)
        {
            List<Requirement> listRequirements = new List<Requirement>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        cmd.CommandText = "cp_GetAssignedIndents";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                Requirement requirement = new Requirement();

                                requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                                requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                                //requirement.IndentDetails = new MPIndent();
                                //requirement.IndentDetails.IndID = row["INDENT_ID"] != DBNull.Value ? Convert.ToInt32(row["INDENT_ID"]) : 0;
                                //requirement.IndentDetails.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                                //requirement.IndentDetails.IsCapex = row["IS_CAPEX"] != DBNull.Value ? Convert.ToInt32(row["IS_CAPEX"]) : 0;
                                //requirement.IndentDetails.CIJ = new stringCIJ();
                                //requirement.IndentDetails.CIJ.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                                //requirement.IndentDetails.CIJ.CijCode = row["CIJ_CODE"] != DBNull.Value ? Convert.ToString(row["CIJ_CODE"]) : string.Empty;
                                //requirement.IndentDetails.Status = row["DIRECTOR_STATUS"] != DBNull.Value ? Convert.ToString(row["DIRECTOR_STATUS"]) : string.Empty;



                                listRequirements.Add(requirement);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Requirement requirement = new Requirement();
                        requirement.ErrorMessage = ex.Message;
                        listRequirements.Add(requirement);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listRequirements;
        }

        public Response EditCijIndentDetails(int id, string type, int val, string sessionID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        string query = string.Empty;

                        if (type == "CIJ")
                        {
                            query = string.Format("UPDATE cij " +
                            " SET CIJ_STRING = REPLACE(CIJ_STRING, 'assetType\":0,', 'assetType\":{0},'), " +
                            " CIJ_STRING = REPLACE(CIJ_STRING, 'assetType\":1,', 'assetType\":{0},'), " +
                            " ASSEET_TYPE = {0} " +
                            " WHERE CIJ_ID = {1}; ", val, id);
                        }
                        else if (type == "INDENT")
                        {
                            query = string.Format("UPDATE indent " +
                            " SET IS_MEDICAL = {0} " +
                            " WHERE I_ID = {1}; ", val, id);
                        }

                        if (!string.IsNullOrEmpty(query))
                        {
                            cmd.CommandText = query;
                            cmd.CommandType = CommandType.Text;

                            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                            DataSet ds = new DataSet();
                            myDA.Fill(ds);
                            cmd.Connection.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return response;
        }

        public List<IndentItems> GetItemsList(int moduleID, string module, string sessionID)
        {
            List<IndentItems> listCijItems = new List<IndentItems>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_GetItemsList";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_ID", moduleID));
                        cmd.Parameters.Add(new MySqlParameter("P_MODULE", module));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row1 in ds.Tables[0].Rows)
                            {
                                IndentItems indentitems = new IndentItems();

                                indentitems.IndentItemID = row1["II_ID"] != DBNull.Value ? Convert.ToInt32(row1["II_ID"]) : 0;
                                indentitems.MaterialDescription = row1["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row1["DESCRIPTION"]) : string.Empty;
                                indentitems.ExistFunctional = row1["FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["FUNCTIONAL_QTY"]) : 0;
                                indentitems.ExistNonFunctional = row1["NON_FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["NON_FUNCTIONAL_QTY"]) : 0;
                                indentitems.RequiredQuantity = row1["REQUIRED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REQUIRED_QTY"]) : 0;
                                indentitems.PurposeOfMaterial = row1["PURPOSE"] != DBNull.Value ? Convert.ToString(row1["PURPOSE"]) : string.Empty;
                                indentitems.MakeModel = row1["MAKE_MODEL"] != DBNull.Value ? Convert.ToString(row1["MAKE_MODEL"]) : string.Empty;
                                indentitems.Units = row1["UNITS"] != DBNull.Value ? Convert.ToString(row1["UNITS"]) : string.Empty;
                                indentitems.FileName = new FileUpload();
                                indentitems.FileName.FileName = row1["ATTACH_ID"] != DBNull.Value ? Convert.ToString(row1["ATTACH_ID"]) : string.Empty;

                                indentitems.IsRejected = row1["IS_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REJECTED"]) : 0;
                                indentitems.RejectedQuantity = row1["REJECTED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REJECTED_QTY"]) : 0;
                                indentitems.RejectedBy = row1["REJECTED_BY"] != DBNull.Value ? Convert.ToInt32(row1["REJECTED_BY"]) : 0;
                                indentitems.InitialQty = row1["INITIAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["INITIAL_QTY"]) : 0;
                                indentitems.RejectReason = row1["REJECT_REASON"] != DBNull.Value ? Convert.ToString(row1["REJECT_REASON"]) : string.Empty;

                                indentitems.BudgetID = row1["BC_ID"] != DBNull.Value ? Convert.ToInt32(row1["BC_ID"]) : 0;

                                indentitems.BCInfo = new BudgetCodeEntity();

                                indentitems.BCInfo.DeptCode = row1["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row1["DEPT_CODE"]) : string.Empty;
                                indentitems.BCInfo.WorkCatCode = row1["CAT_CODE"] != DBNull.Value ? Convert.ToString(row1["CAT_CODE"]) : string.Empty;
                                indentitems.BCInfo.WorkCatDesc = row1["CAT_DESC"] != DBNull.Value ? Convert.ToString(row1["CAT_DESC"]) : string.Empty;
                                indentitems.BCInfo.BudgetID = row1["BC_ID"] != DBNull.Value ? Convert.ToInt32(row1["BC_ID"]) : 0;
                                indentitems.BCInfo.BudgetCode = row1["BUDGET_CODE"] != DBNull.Value ? Convert.ToString(row1["BUDGET_CODE"]) : string.Empty;
                                indentitems.BCInfo.BudgetDesc = row1["BUDGET_DESC"] != DBNull.Value ? Convert.ToString(row1["BUDGET_DESC"]) : string.Empty;

                                listCijItems.Add(indentitems);
                            }

                            listCijItems = listCijItems.OrderBy(v => v.IndentItemID).ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        IndentItems indentitems = new IndentItems();
                        indentitems.ErrorMessage = ex.Message;
                        listCijItems.Add(indentitems);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listCijItems;
        }

        public Response saveCIJ(stringCIJ stringcij, MPIndent indent)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        //int isValidSession = ValidateSession(user.SessionID, cmd);
                        DeleteIndentItems(indent.ListDeleteItems);

                        cmd.CommandText = "cp_saveCIJ";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new MySqlParameter("P_CIJ_ID", stringcij.CijID));
                        cmd.Parameters.Add(new MySqlParameter("P_CIJ_CODE", stringcij.CijCode));
                        cmd.Parameters.Add(new MySqlParameter("P_CIJ_STRING", stringcij.Cij));
                        cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", stringcij.CompID));
                        cmd.Parameters.Add(new MySqlParameter("P_CIJ_TYPE", stringcij.CijType));
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", stringcij.UserID));
                        cmd.Parameters.Add(new MySqlParameter("P_IS_VALID", 1));
                        cmd.Parameters.Add(new MySqlParameter("P_DEPT_ID", stringcij.DeptID));
                        cmd.Parameters.Add(new MySqlParameter("P_ASSEET_TYPE", stringcij.AssetType));
                        cmd.Parameters.Add(new MySqlParameter("P_USER_STATUS", stringcij.UserStatus));
                        cmd.Parameters.Add(new MySqlParameter("P_USER_PRIORITY", stringcij.UserPriority));
                        cmd.Parameters.Add(new MySqlParameter("P_EXPECTED_DELIVERY", stringcij.ExpectedDelivery));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;

                            saveIndentItems(indent.ListIndentItems, response.ObjectID, "CIJ", stringcij.SessionID, stringcij.UserID);
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public stringCIJ GetCIJ(int cijID, string sessionID)
        {
            stringCIJ stringcij = new stringCIJ();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_GetCIJ";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_CIJ_ID", cijID));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            DataRow row = ds.Tables[0].Rows[0];

                            stringcij.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                            stringcij.CijCode = row["CIJ_CODE"] != DBNull.Value ? Convert.ToString(row["CIJ_CODE"]) : string.Empty;
                            stringcij.Cij = row["CIJ_STRING"] != DBNull.Value ? Convert.ToString(row["CIJ_STRING"]) : string.Empty;
                            stringcij.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                            stringcij.CijType = row["CIJ_TYPE"] != DBNull.Value ? Convert.ToString(row["CIJ_TYPE"]) : string.Empty;
                            stringcij.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                            stringcij.IsValid = row["IS_VALID"] != DBNull.Value ? Convert.ToInt32(row["IS_VALID"]) : 1;
                            stringcij.UserStatus = row["USER_STATUS"] != DBNull.Value ? Convert.ToString(row["USER_STATUS"]) : string.Empty;
                            stringcij.UserPriority = row["USER_PRIORITY"] != DBNull.Value ? Convert.ToString(row["USER_PRIORITY"]) : string.Empty;
                            stringcij.ExpectedDelivery = row["EXPECTED_DELIVERY"] != DBNull.Value ? Convert.ToString(row["EXPECTED_DELIVERY"]) : string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        stringcij.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return stringcij;
        }

        public MPIndent GetIndentDetails(int indentID, string sessionID)
        {
            MPIndent mpindent = new MPIndent();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);
                        cmd.Parameters.Clear();
                        cmd.CommandText = "cp_GetIndentDetails";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_I_ID", indentID));
                        cmd.Parameters.Add(new MySqlParameter("P_SESSION_ID", sessionID));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            DataRow row = ds.Tables[0].Rows[0];

                            mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;

                            mpindent.CIJ = new stringCIJ();

                            mpindent.CIJ.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                            mpindent.CIJ.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                            mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                            mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                            mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                            mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                            mpindent.ApprovedBy = row["APPROVED_BY"] != DBNull.Value ? Convert.ToString(row["APPROVED_BY"]) : string.Empty;
                            mpindent.PurchaseIncharge = row["PURCHASE_INCHARGE"] != DBNull.Value ? Convert.ToString(row["PURCHASE_INCHARGE"]) : string.Empty;
                            mpindent.ApproximateCostRange = row["APPROX_COST_RANGE"] != DBNull.Value ? Convert.ToDecimal(row["APPROX_COST_RANGE"]) : 0;
                            mpindent.IsMedical = row["IS_MEDICAL"] != DBNull.Value ? Convert.ToInt32(row["IS_MEDICAL"]) : 0;
                            mpindent.Attachments = row["ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row["ATTACHMENTS"]) : string.Empty;
                            mpindent.IsValid = row["IS_VALID"] != DBNull.Value ? Convert.ToInt32(row["IS_VALID"]) : 0;
                            mpindent.PdfID = row["PDF_ID"] != DBNull.Value ? Convert.ToInt32(row["PDF_ID"]) : 0;
                            mpindent.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                            mpindent.CloneIndID = row["CLONE_I_ID"] != DBNull.Value ? Convert.ToInt32(row["CLONE_I_ID"]) : 0;
                            mpindent.IsCloned = row["IS_CLONED"] != DBNull.Value ? (Convert.ToInt16(row["IS_CLONED"]) > 0 ? true : false) : false;
                            mpindent.PurchaseDirector = row["DIRECTOR"] != DBNull.Value ? Convert.ToString(row["DIRECTOR"]) : string.Empty;
                            mpindent.IsCapex = row["IS_CAPEX"] != DBNull.Value ? Convert.ToInt32(row["IS_CAPEX"]) : 0;
                            mpindent.UserStatus = row["USER_STATUS"] != DBNull.Value ? Convert.ToString(row["USER_STATUS"]) : string.Empty;
                            mpindent.UserPriority = row["USER_PRIORITY"] != DBNull.Value ? Convert.ToString(row["USER_PRIORITY"]) : string.Empty;
                            mpindent.ExpectedDelivery = row["EXPECTED_DELIVERY"] != DBNull.Value ? Convert.ToString(row["EXPECTED_DELIVERY"]) : string.Empty;


                            List<IndentItems> listIndentItems = new List<IndentItems>();


                            foreach (DataRow row1 in ds.Tables[1].Rows)
                            {
                                IndentItems indentitems = new IndentItems();

                                indentitems.IndentItemID = row1["II_ID"] != DBNull.Value ? Convert.ToInt32(row1["II_ID"]) : 0;
                                indentitems.MaterialDescription = row1["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row1["DESCRIPTION"]) : string.Empty;
                                indentitems.ExistFunctional = row1["FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["FUNCTIONAL_QTY"]) : 0;
                                indentitems.ExistNonFunctional = row1["NON_FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["NON_FUNCTIONAL_QTY"]) : 0;
                                indentitems.RequiredQuantity = row1["REQUIRED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REQUIRED_QTY"]) : 0;
                                indentitems.PurposeOfMaterial = row1["PURPOSE"] != DBNull.Value ? Convert.ToString(row1["PURPOSE"]) : string.Empty;
                                indentitems.MakeModel = row1["MAKE_MODEL"] != DBNull.Value ? Convert.ToString(row1["MAKE_MODEL"]) : string.Empty;
                                indentitems.Units = row1["UNITS"] != DBNull.Value ? Convert.ToString(row1["UNITS"]) : string.Empty;
                                indentitems.FileName = new FileUpload();
                                indentitems.FileName.FileName = row1["ATTACH_ID"] != DBNull.Value ? Convert.ToString(row1["ATTACH_ID"]) : string.Empty;

                                indentitems.IsRejected = row1["IS_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REJECTED"]) : 0;
                                indentitems.RejectedQuantity = row1["REJECTED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REJECTED_QTY"]) : 0;
                                indentitems.RejectedBy = row1["REJECTED_BY"] != DBNull.Value ? Convert.ToInt32(row1["REJECTED_BY"]) : 0;
                                indentitems.InitialQty = row1["INITIAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["INITIAL_QTY"]) : 0;
                                indentitems.RejectReason = row1["REJECT_REASON"] != DBNull.Value ? Convert.ToString(row1["REJECT_REASON"]) : string.Empty;

                                indentitems.BudgetID = row1["BC_ID"] != DBNull.Value ? Convert.ToInt32(row1["BC_ID"]) : 0;

                                indentitems.BCInfo = new BudgetCodeEntity();

                                indentitems.BCInfo.DeptCode = row1["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row1["DEPT_CODE"]) : string.Empty;
                                indentitems.BCInfo.WorkCatCode = row1["CAT_CODE"] != DBNull.Value ? Convert.ToString(row1["CAT_CODE"]) : string.Empty;
                                indentitems.BCInfo.WorkCatDesc = row1["CAT_DESC"] != DBNull.Value ? Convert.ToString(row1["CAT_DESC"]) : string.Empty;
                                indentitems.BCInfo.BudgetID = row1["BC_ID"] != DBNull.Value ? Convert.ToInt32(row1["BC_ID"]) : 0;
                                indentitems.BCInfo.BudgetCode = row1["BUDGET_CODE"] != DBNull.Value ? Convert.ToString(row1["BUDGET_CODE"]) : string.Empty;
                                indentitems.BCInfo.BudgetDesc = row1["BUDGET_DESC"] != DBNull.Value ? Convert.ToString(row1["BUDGET_DESC"]) : string.Empty;


                                if (indentitems.RequiredQuantity > 0)
                                {
                                    string xml = string.Empty;
                                    xml = Utilities.GenerateEmailBody("MPIndentItemsXML");
                                    xml = String.Format(xml, indentitems.MaterialDescription, indentitems.MakeModel, indentitems.ExistFunctional,
                                        indentitems.ExistNonFunctional, indentitems.RequiredQuantity, indentitems.Units, indentitems.PurposeOfMaterial,
                                        indentitems.BCInfo.BudgetCode);
                                    mpindent.ItemsString += xml;
                                }
                                listIndentItems.Add(indentitems);
                            }

                            listIndentItems = listIndentItems.OrderBy(v => v.IndentItemID).ToList();

                            mpindent.ListIndentItems = listIndentItems;

                        }
                    }
                    catch (Exception ex)
                    {
                        mpindent.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return mpindent;
        }

        public Response SaveIndentDetails(MPIndent mpindent)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        int isValidSession = Utilities.ValidateSession(mpindent.SessionID, cmd);

                        DeleteIndentItems(mpindent.ListDeleteItems);

                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        cmd.CommandText = "cp_SaveIndentDetails";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new MySqlParameter("P_I_ID", mpindent.IndID));
                        cmd.Parameters.Add(new MySqlParameter("P_CIJ_ID", mpindent.CIJ.CijID));
                        cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", mpindent.CIJ.CompID));
                        cmd.Parameters.Add(new MySqlParameter("P_INDENT_CODE", mpindent.IndNo));
                        cmd.Parameters.Add(new MySqlParameter("P_DEPT", mpindent.Department));
                        cmd.Parameters.Add(new MySqlParameter("P_CREATED_DATE", mpindent.RequestDate));
                        cmd.Parameters.Add(new MySqlParameter("P_REQUESTED_BY", mpindent.RequestedBy));
                        cmd.Parameters.Add(new MySqlParameter("P_APPROVED_BY", mpindent.ApprovedBy));
                        cmd.Parameters.Add(new MySqlParameter("P_PURCHASE_INCHARGE", mpindent.PurchaseIncharge));
                        cmd.Parameters.Add(new MySqlParameter("P_APPROX_COST_RANGE", mpindent.ApproximateCostRange));
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", mpindent.UserID));
                        cmd.Parameters.Add(new MySqlParameter("P_IS_MEDICAL", mpindent.IsMedical));
                        cmd.Parameters.Add(new MySqlParameter("P_ATTACHMENTS", mpindent.Attachments));
                        cmd.Parameters.Add(new MySqlParameter("P_IS_VALID", 1));
                        cmd.Parameters.Add(new MySqlParameter("P_DEPT_ID", mpindent.DeptID));
                        cmd.Parameters.Add(new MySqlParameter("P_CLONE_I_ID", mpindent.CloneIndID));
                        cmd.Parameters.Add(new MySqlParameter("P_DIRECTOR", mpindent.PurchaseDirector));
                        cmd.Parameters.Add(new MySqlParameter("P_IS_CAPEX", mpindent.IsCapex));
                        cmd.Parameters.Add(new MySqlParameter("P_USER_STATUS", mpindent.UserStatus));
                        cmd.Parameters.Add(new MySqlParameter("P_USER_PRIORITY", mpindent.UserPriority));
                        cmd.Parameters.Add(new MySqlParameter("P_EXPECTED_DELIVERY", mpindent.ExpectedDelivery));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);

                        cmd.Parameters.Clear();

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;

                            MPIndent indentDetails = new MPIndent();
                            string PDFfileName = string.Empty;
                            int margin = 24;
                            long ticks = DateTime.Now.Ticks;

                            int cijID = mpindent.CIJ.CijID;

                            if (mpindent.CloneIndID > 0)
                            {
                                cijID = 0;
                            }

                            if (cijID > 0)
                            {

                            }
                            else
                            {
                                saveIndentItems(mpindent.ListIndentItems, response.ObjectID, "INDENT", mpindent.SessionID, mpindent.UserID);
                            }

                            indentDetails = GetIndentDetails(response.ObjectID, mpindent.SessionID);

                            string indentNumber = mpindent.IndNo.Replace("/", "-");

                            PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateIndentPDF(indentDetails.ItemsString, mpindent.SessionID, mpindent, null), PdfSharp.PageSize.A4, margin);
                            pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "IndentPDF" + indentNumber + "-" + ticks + ".pdf"));
                            PDFfileName = "IndentPDF" + indentNumber + "-" + ticks + ".pdf";
                            Response responce = SaveAttachment(PDFfileName, cmd);
                            PDFfileName = responce.ObjectID.ToString();
                            response.Message = PDFfileName;

                            string attachmentsave = SaveAttachmentInDB(response.ObjectID, mpindent.UserID, PDFfileName, "INDENT");

                        }

                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public List<stringCIJ> GetCIJList(int userID, string sessionID)
        {
            List<stringCIJ> listStstringCIJ = new List<stringCIJ>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_GetCIJList";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                stringCIJ stringcij = new stringCIJ();

                                stringcij.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                                stringcij.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                stringcij.CijCode = row["CIJ_CODE"] != DBNull.Value ? Convert.ToString(row["CIJ_CODE"]) : string.Empty;
                                stringcij.Cij = row["CIJ_STRING"] != DBNull.Value ? Convert.ToString(row["CIJ_STRING"]) : string.Empty;
                                stringcij.CijType = row["CIJ_TYPE"] != DBNull.Value ? Convert.ToString(row["CIJ_TYPE"]) : string.Empty;
                                stringcij.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                stringcij.Status = row["CURRENT_STATUS"] != DBNull.Value ? Convert.ToString(row["CURRENT_STATUS"]) : "PENDING";
                                stringcij.RequestedBy = row["REQUESTOR_NAME"] != DBNull.Value ? Convert.ToString(row["REQUESTOR_NAME"]) : "";
                                stringcij.IsValid = row["IS_VALID"] != DBNull.Value ? Convert.ToInt32(row["IS_VALID"]) : 1;
                                stringcij.UserStatus = row["USER_STATUS"] != DBNull.Value ? Convert.ToString(row["USER_STATUS"]) : string.Empty;
                                stringcij.UserPriority = row["USER_PRIORITY"] != DBNull.Value ? Convert.ToString(row["USER_PRIORITY"]) : string.Empty;
                                stringcij.ExpectedDelivery = row["EXPECTED_DELIVERY"] != DBNull.Value ? Convert.ToString(row["EXPECTED_DELIVERY"]) : string.Empty;
                                stringcij.CurrentApproverName = row["CURRENT_APPROVER_NAME"] != DBNull.Value ? Convert.ToString(row["CURRENT_APPROVER_NAME"]) : string.Empty;

                                listStstringCIJ.Add(stringcij);
                            }

                            listStstringCIJ = listStstringCIJ.OrderByDescending(v => v.CijID).ToList();

                        }
                    }
                    catch (Exception ex)
                    {
                        stringCIJ stringcij = new stringCIJ();
                        stringcij.ErrorMessage = ex.Message;
                        listStstringCIJ.Add(stringcij);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listStstringCIJ;
        }

        public List<MPIndent> GetIndentList(int userID, string sessionID)
        {
            List<MPIndent> listMPIndent = new List<MPIndent>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_GetIndentList";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                MPIndent mpindent = new MPIndent();

                                mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                                mpindent.Status = row["CURRENT_STATUS"] != DBNull.Value ? Convert.ToString(row["CURRENT_STATUS"]) : string.Empty;
                                mpindent.UserWFStatus = row["USER_WF_STATUS"] != DBNull.Value ? Convert.ToString(row["USER_WF_STATUS"]) : string.Empty;

                                mpindent.CIJ = new stringCIJ();

                                mpindent.CIJ.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                                mpindent.CIJ.CijCode = row["CIJ_CODE"] != DBNull.Value ? Convert.ToString(row["CIJ_CODE"]) : string.Empty;
                                mpindent.CIJ.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                                mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                                mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                                mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                                mpindent.ApprovedBy = row["APPROVED_BY"] != DBNull.Value ? Convert.ToString(row["APPROVED_BY"]) : string.Empty;
                                mpindent.PurchaseIncharge = row["PURCHASE_INCHARGE"] != DBNull.Value ? Convert.ToString(row["PURCHASE_INCHARGE"]) : string.Empty;
                                mpindent.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                                mpindent.Attachments = row["ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row["ATTACHMENTS"]) : string.Empty;
                                mpindent.IsValid = row["IS_VALID"] != DBNull.Value ? Convert.ToInt32(row["IS_VALID"]) : 1;
                                mpindent.IsMedical = row["IS_MEDICAL"] != DBNull.Value ? Convert.ToInt32(row["IS_MEDICAL"]) : 1;
                                mpindent.IsCapex = row["IS_CAPEX"] != DBNull.Value ? Convert.ToInt32(row["IS_CAPEX"]) : 1;
                                mpindent.IsIndentAssigned = row["IS_INDENT_ASSIGNED"] != DBNull.Value ? Convert.ToInt32(row["IS_INDENT_ASSIGNED"]) : 0;
                                mpindent.UserStatus = row["USER_STATUS"] != DBNull.Value ? Convert.ToString(row["USER_STATUS"]) : string.Empty;
                                mpindent.UserPriority = row["USER_PRIORITY"] != DBNull.Value ? Convert.ToString(row["USER_PRIORITY"]) : string.Empty;
                                mpindent.ExpectedDelivery = row["EXPECTED_DELIVERY"] != DBNull.Value ? Convert.ToString(row["EXPECTED_DELIVERY"]) : string.Empty;
                                mpindent.CurrentApproverName = row["CURRENT_APPROVER_NAME"] != DBNull.Value ? Convert.ToString(row["CURRENT_APPROVER_NAME"]) : string.Empty;

                                listMPIndent.Add(mpindent);
                            }

                            listMPIndent = listMPIndent.OrderByDescending(v => v.IndID).ToList();

                        }
                    }
                    catch (Exception ex)
                    {
                        MPIndent mpindent = new MPIndent();
                        mpindent.ErrorMessage = ex.Message;
                        listMPIndent.Add(mpindent);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listMPIndent;
        }

        public Response DeleteCIJ(stringCIJ stringcij)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        int isValidSession = Utilities.ValidateSession(stringcij.SessionID, cmd);

                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        cmd.CommandText = string.Format("UPDATE CIJ SET IS_VALID = {1} WHERE CIJ_ID = {0};", stringcij.CijID, stringcij.IsValid);
                        cmd.CommandType = CommandType.Text;




                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);

                        cmd.Parameters.Clear();

                        response.ObjectID = stringcij.CijID;

                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public Response saveFileUpload(FileUpload fileupload)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        if (fileupload.FileStream != null)
                        {
                            string fileName = string.Empty;

                            long tick = DateTime.Now.Ticks;
                            fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "File_" + tick + "_" + fileupload.FileName);

                            Utilities.SaveFile(fileName, fileupload.FileStream);

                            fileName = "File_" + tick + "_" + fileupload.FileName;

                            response = SaveAttachment(fileName, cmd);

                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public Response DeleteIndentItems(List<int> listItems)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }


                        foreach (int id in listItems)
                        {

                            cmd.Parameters.Clear();
                            cmd.CommandText = string.Format("DELETE FROM indentitems WHERE II_ID = {0};", id);
                            cmd.CommandType = CommandType.Text;

                            MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                            DataSet ds = new DataSet();
                            myDA.Fill(ds);
                            cmd.Connection.Close();

                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            }

                            cmd.Parameters.Clear();
                        }

                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return response;
        }

        public Response DeleteIndent(MPIndent mpindent)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        int isValidSession = Utilities.ValidateSession(mpindent.SessionID, cmd);

                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        cmd.CommandText = string.Format("UPDATE Indent SET IS_VALID = {1} WHERE I_ID = {0};", mpindent.IndID, mpindent.IsValid);
                        cmd.CommandType = CommandType.Text;




                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);

                        cmd.Parameters.Clear();

                        response.ObjectID = mpindent.IndID;

                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public Response saveIndentItems(List<IndentItems> ListIndentItems, int id, string idType, string sessionID, int userID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        string itemsString = string.Empty;

                        //ListIndentItems = ListIndentItems.OrderBy(v => v.SlNo).ToList();

                        foreach (IndentItems II in ListIndentItems)
                        {
                            cmd.CommandText = "cp_SaveIndentItems";

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new MySqlParameter("P_II_ID", II.IndentItemID));

                            cmd.Parameters.Add(new MySqlParameter("P_DESCRIPTION", II.MaterialDescription));
                            cmd.Parameters.Add(new MySqlParameter("P_FUNCTIONAL_QTY", II.ExistFunctional));
                            cmd.Parameters.Add(new MySqlParameter("P_NON_FUNCTIONAL_QTY", II.ExistNonFunctional));
                            cmd.Parameters.Add(new MySqlParameter("P_REQUIRED_QTY", II.RequiredQuantity));
                            cmd.Parameters.Add(new MySqlParameter("P_PURPOSE", II.PurposeOfMaterial));
                            cmd.Parameters.Add(new MySqlParameter("P_MAKE_MODEL", II.MakeModel));
                            cmd.Parameters.Add(new MySqlParameter("P_UNITS", II.Units));
                            cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                            cmd.Parameters.Add(new MySqlParameter("P_BC_ID", II.BudgetID));

                            int cijID = 0;
                            int indentID = 0;

                            if (idType == "CIJ")
                            {
                                cijID = id;
                                if (!string.IsNullOrEmpty(II.FileName.FileName))
                                {
                                    response.ObjectID = Convert.ToInt32(II.FileName.FileName);
                                }
                            }
                            else if (idType == "INDENT")
                            {
                                indentID = id;

                                if (!string.IsNullOrEmpty(II.FileName.FileName))
                                {
                                    response.ObjectID = Convert.ToInt32(II.FileName.FileName);
                                }

                            }

                            cmd.Parameters.Add(new MySqlParameter("P_I_ID", indentID));
                            cmd.Parameters.Add(new MySqlParameter("P_CIJ_ID", cijID));
                            cmd.Parameters.Add(new MySqlParameter("P_ATTACH_ID", response.ObjectID));

                            MySqlDataAdapter myDA1 = new MySqlDataAdapter(cmd);
                            DataSet ds1 = new DataSet();
                            myDA1.Fill(ds1);
                            cmd.Connection.Close();

                            cmd.Parameters.Clear();

                            if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                                response.ErrorMessage = ds1.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds1.Tables[0].Rows[0][1].ToString()) : string.Empty;
                            }

                            cmd.Parameters.Clear();
                        }

                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public List<IndentItems> GetCijItems(int cijID, string sessionID)
        {
            List<IndentItems> listCijItems = new List<IndentItems>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = Utilities.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_GetCijItems";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_ID", cijID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row1 in ds.Tables[0].Rows)
                            {
                                IndentItems indentitems = new IndentItems();

                                indentitems.IndentItemID = row1["II_ID"] != DBNull.Value ? Convert.ToInt32(row1["II_ID"]) : 0;
                                indentitems.MaterialDescription = row1["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row1["DESCRIPTION"]) : string.Empty;
                                indentitems.ExistFunctional = row1["FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["FUNCTIONAL_QTY"]) : 0;
                                indentitems.ExistNonFunctional = row1["NON_FUNCTIONAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["NON_FUNCTIONAL_QTY"]) : 0;
                                indentitems.RequiredQuantity = row1["REQUIRED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REQUIRED_QTY"]) : 0;
                                indentitems.PurposeOfMaterial = row1["PURPOSE"] != DBNull.Value ? Convert.ToString(row1["PURPOSE"]) : string.Empty;
                                indentitems.MakeModel = row1["MAKE_MODEL"] != DBNull.Value ? Convert.ToString(row1["MAKE_MODEL"]) : string.Empty;
                                indentitems.Units = row1["UNITS"] != DBNull.Value ? Convert.ToString(row1["UNITS"]) : string.Empty;
                                indentitems.FileName = new FileUpload();
                                indentitems.FileName.FileName = row1["ATTACH_ID"] != DBNull.Value ? Convert.ToString(row1["ATTACH_ID"]) : string.Empty;

                                indentitems.IsRejected = row1["IS_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REJECTED"]) : 0;
                                indentitems.RejectedQuantity = row1["REJECTED_QTY"] != DBNull.Value ? Convert.ToInt32(row1["REJECTED_QTY"]) : 0;
                                indentitems.RejectedBy = row1["REJECTED_BY"] != DBNull.Value ? Convert.ToInt32(row1["REJECTED_BY"]) : 0;
                                indentitems.InitialQty = row1["INITIAL_QTY"] != DBNull.Value ? Convert.ToInt32(row1["INITIAL_QTY"]) : 0;
                                indentitems.RejectReason = row1["REJECT_REASON"] != DBNull.Value ? Convert.ToString(row1["REJECT_REASON"]) : string.Empty;

                                indentitems.BudgetID = row1["BC_ID"] != DBNull.Value ? Convert.ToInt32(row1["BC_ID"]) : 0;

                                indentitems.BCInfo = new BudgetCodeEntity();

                                indentitems.BCInfo.DeptCode = row1["DEPT_CODE"] != DBNull.Value ? Convert.ToString(row1["DEPT_CODE"]) : string.Empty;
                                indentitems.BCInfo.WorkCatCode = row1["CAT_CODE"] != DBNull.Value ? Convert.ToString(row1["CAT_CODE"]) : string.Empty;
                                indentitems.BCInfo.WorkCatDesc = row1["CAT_DESC"] != DBNull.Value ? Convert.ToString(row1["CAT_DESC"]) : string.Empty;
                                indentitems.BCInfo.BudgetID = row1["BC_ID"] != DBNull.Value ? Convert.ToInt32(row1["BC_ID"]) : 0;
                                indentitems.BCInfo.BudgetCode = row1["BUDGET_CODE"] != DBNull.Value ? Convert.ToString(row1["BUDGET_CODE"]) : string.Empty;
                                indentitems.BCInfo.BudgetDesc = row1["BUDGET_DESC"] != DBNull.Value ? Convert.ToString(row1["BUDGET_DESC"]) : string.Empty;

                                listCijItems.Add(indentitems);
                            }

                            listCijItems = listCijItems.OrderBy(v => v.IndentItemID).ToList();


                        }
                    }
                    catch (Exception ex)
                    {
                        IndentItems indentitems = new IndentItems();
                        indentitems.ErrorMessage = ex.Message;
                        listCijItems.Add(indentitems);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listCijItems;
        }

        private Response SaveAttachment(string path, MySqlCommand cmd = null)
        {
            Response response = new Response();
            try
            {

                if (cmd == null)
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = new MySqlConnection(Utilities.GetConnectionString());
                }
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                cmd.Parameters.Clear();
                cmd.CommandText = "cp_SaveAttachment";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_PATH", path));
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
                cmd.Parameters.Clear();
            }
            return response;
        }

        public Response GetSeries(string series, string seriesType, string sessionID, int compID, int deptID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        cmd.CommandText = "wf_GetSeries";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_SERIES", series));
                        cmd.Parameters.Add(new MySqlParameter("P_SERIES_TYPE", seriesType));
                        cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", compID));
                        cmd.Parameters.Add(new MySqlParameter("P_DEPT_ID", deptID));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.Message = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                            response.Message = response.Message.ToUpper();
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        public Response RejectIndentItems(List<IndentItems> listItems, int indentID, int userID, string sessionID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }


                        foreach (IndentItems II in listItems)
                        {
                            if (II.RejectedQuantity > 0 && II.IndentItemID > 0)
                            {

                                cmd.CommandText = "cp_RejectIndentItems";

                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add(new MySqlParameter("P_I_ID", indentID));
                                cmd.Parameters.Add(new MySqlParameter("P_II_ID", II.IndentItemID));

                                int requiredQty = 0;
                                requiredQty = II.RequiredQuantity - II.RejectedQuantity;

                                if (requiredQty <= 0)
                                {
                                    II.IsRejected = 1;
                                }

                                cmd.Parameters.Add(new MySqlParameter("P_REQUIRED_QTY", requiredQty));
                                cmd.Parameters.Add(new MySqlParameter("P_IS_REJECTED", II.IsRejected));
                                cmd.Parameters.Add(new MySqlParameter("P_REJECTED_QTY", II.RejectedQuantity));
                                cmd.Parameters.Add(new MySqlParameter("P_REJECTED_BY", userID));
                                cmd.Parameters.Add(new MySqlParameter("P_REJECT_REASON", II.RejectReason));

                                MySqlDataAdapter myDA1 = new MySqlDataAdapter(cmd);
                                DataSet ds1 = new DataSet();
                                myDA1.Fill(ds1);

                                cmd.Connection.Close();
                                cmd.Parameters.Clear();

                                if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                                {
                                    response.ObjectID = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                                    response.ErrorMessage = ds1.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds1.Tables[0].Rows[0][1].ToString()) : string.Empty;
                                }

                                cmd.Parameters.Clear();
                            }
                        }


                        MPIndent indentDetails = new MPIndent();
                        string PDFfileName = string.Empty;
                        int margin = 24;
                        long ticks = DateTime.Now.Ticks;

                        indentDetails = GetIndentDetails(indentID, sessionID);

                        string indentNumber = indentDetails.IndNo.Replace("/", "-");

                        PdfDocument pdf = PdfGenerator.GeneratePdf(GenerateIndentPDF(indentDetails.ItemsString, sessionID, indentDetails, null), PdfSharp.PageSize.A4, margin);
                        pdf.Save(System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "IndentPDF" + indentNumber + "-" + ticks + ".pdf"));
                        PDFfileName = "IndentPDF" + indentNumber + "-" + ticks + ".pdf";
                        Response responce = SaveAttachment(PDFfileName, cmd);
                        PDFfileName = responce.ObjectID.ToString();
                        response.Message = PDFfileName;

                        string attachmentsave = SaveAttachmentInDB(indentID, indentDetails.CreatedBy, PDFfileName, "INDENT");


                        List<Attachment> ListAttachment = new List<Attachment>();
                        Attachment emailAttachment = null;

                        if (!string.IsNullOrEmpty(PDFfileName))
                        {
                            var fileData = Utilities.DownloadFile(Convert.ToString(PDFfileName), sessionID);

                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                emailAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                                ListAttachment.Add(emailAttachment);
                            }
                        }


                        UserInfo createdBy = prm.GetUser(indentDetails.CreatedBy, sessionID);
                        UserInfo modifiedBy = prm.GetUser(userID, sessionID);

                        DateTime dateTime = DateTime.Now;


                        string body = string.Empty;
                        string bodySMS = string.Empty;

                        body = Utilities.GenerateEmailBody("INDENTItemsRejectedEndUserEmail");
                        bodySMS = Utilities.GenerateEmailBody("INDENTItemsRejectedEndUserSMS");
                        string subject = "Indent No:-  " + indentDetails.IndNo + " Items Rejected By " + modifiedBy.FirstName + " " + modifiedBy.LastName;


                        body = String.Format(body, createdBy.FirstName, createdBy.LastName, indentDetails.IndNo, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                        SendEmail(createdBy.Email, subject, body, sessionID, null, ListAttachment).ConfigureAwait(false);

                        bodySMS = String.Format(bodySMS, createdBy.FirstName, createdBy.LastName, indentDetails.IndNo, createdBy.FirstName, createdBy.LastName, dateTime, modifiedBy.FirstName, modifiedBy.LastName);
                        bodySMS = bodySMS.Replace("<br/>", "");
                        //SendSMS(string.Empty, createdBy.PhoneNum + "," + createdBy.AltPhoneNum, System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), sessionID).ConfigureAwait(false);


                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message; 
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        private string GenerateIndentPDF(string itemsString, string sessionID, MPIndent indentDetails = null, stringCIJ cijDetails = null)
        {
            UserDetails createdBy = prm.GetUserDetails(indentDetails.CreatedBy, sessionID);

            string Category = string.Empty;
            string AssetType = string.Empty;

            //if (indentDetails.CIJ.CijID > 0) {
            //    Category = "Capex";
            //}
            //else
            //{
            //    Category = "Non-Capex";
            //}

            if (indentDetails.IsCapex == -1)
            {
                Category = "Service Request";
            }
            if (indentDetails.IsCapex == 0)
            {
                Category = "Non-Capex";
            }
            if (indentDetails.IsCapex == 1)
            {
                Category = "Capex";
            }

            if (indentDetails.IsMedical > 0)
            {
                AssetType = "Medical";
            }
            else
            {
                AssetType = "Non-Medical";
            }

            //if (string.IsNullOrEmpty(createdBy.LogoURL))
            //{
            //    createdBy.LogoURL = "";
            //}
            //else
            //{
            //    createdBy.LogoURL = createdBy.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");
            //    string fileName = System.Web.HttpContext.Current.Server.MapPath("/Services" + createdBy.CompanyId + ".png");                
            //    createdBy.LogoURL = "<img width=\"100px\" height=\"50px\" src=\"" + fileName + "\" />";
            //}

            string fileName = System.Web.HttpContext.Current.Server.MapPath("/Services/companylogos/" + createdBy.CompanyId + "_LOGO.png");
            createdBy.LogoURL = "<img width=\"100px\" height=\"50px\" src=\"" + fileName + "\" />";


            string html = string.Empty;
            html = "indentPDF.html";

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                createdBy.LogoURL.ToString(), // 0
                createdBy.CompanyName.ToString(),
                Category.ToString(),
                AssetType.ToString(),
                indentDetails.IndNo.ToString(), // 4
                indentDetails.Department.ToString(),
                indentDetails.RequestDate.ToString(),
                indentDetails.ApproximateCostRange.ToString(),
                itemsString.ToString(), // 8
                indentDetails.RequestedBy.ToString(),
                indentDetails.ApprovedBy.ToString(),
                indentDetails.PurchaseIncharge.ToString(),
                indentDetails.PurchaseDirector.ToString(),// 12
                indentDetails.UserPriority.ToString(),// 13
                indentDetails.ExpectedDelivery.ToString()// 14


               );
            }
            catch(Exception Ex)
            {

            }
            return htmlRows;
        }

        private string SaveAttachmentInDB(int typeID, int userID, string attachmentID, string fileType)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = PRMUtility.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_SaveAttachmentInDB";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new MySqlParameter("P_TYPE_ID", typeID));
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_ATTACHMENT_ID", attachmentID));
                        cmd.Parameters.Add(new MySqlParameter("P_FILE_TYPE", fileType));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response.ErrorMessage;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null)
        {

            try
            {
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID);
                Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");


                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;

                smtpClient.EnableSsl = false;

                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(prm.GetFromAddress(communication.User.Email), prm.GetFromDisplayName(communication.User.Email));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }

                if (ListAttachment != null)
                {
                    foreach (Attachment singleAttachment in ListAttachment)
                    {
                        if (singleAttachment != null)
                        {
                            mail.Attachments.Add(singleAttachment);
                        }
                    }
                }

                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }

                }

                //if (!string.IsNullOrEmpty(communication.User.Email))
                //{
                //    mail.CC.Add(communication.User.Email);
                //}

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;

                //smtpClient.Send(mail);
                await smtpClient.SendMailAsync(mail);

                Response response = new Response();
                response.ObjectID = 1;
                //return response;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
                //return response;
            }
        }

    }
}