﻿using Microsoft.AspNet.SignalR;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OfficeOpenXml;
using PdfSharp.Pdf;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.Models.Vendor;
using System.Dynamic;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMPriceCapService : IPRMPriceCapService
    {
        #region Service Calls

        public Requirement GetReqData(int reqID, string sessionID)
        {
            Requirement requirement = new Requirement();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "pc_GetReqData";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", reqID));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataRow row = ds.Tables[0].Rows[0];
                            string[] arr = new string[] { };
                            byte[] test = new byte[] { };
                            requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                            requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                            requirement.RequirementID = reqID;
                            
                            List<VendorDetails> vendorDetails = new List<VendorDetails>();
                            if (ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow row1 in ds.Tables[1].Rows)
                                {
                                    VendorDetails vendor = new VendorDetails();

                                    vendor.PO = new RequirementPO();

                                    vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                                    vendorDetails.Add(vendor);
                                }
                            }

                            int productSNo = 0;
                            List<RequirementItems> ListRequirementItems = new List<RequirementItems>();
                            if (ds.Tables[2].Rows.Count > 0)
                            {
                                foreach (DataRow row2 in ds.Tables[2].Rows)
                                {
                                    RequirementItems RequirementItems = new RequirementItems();
                                    RequirementItems.ProductSNo = productSNo++;
                                    RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                    RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                                    RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                                    RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                                    RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                                    RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                                    RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                                    RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                                    RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                    RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                    RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                    RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;

                                    //RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                    //RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                    //RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                    //RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                    //RequirementItems.ItemPrice = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0; ;
                                    //RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty; ;
                                    //RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty; ;
                                    //RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty; ;
                                    //RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty; ;
                                    //RequirementItems.OthersBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty; ;
                                    //RequirementItems.RevItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0; ;
                                    //RequirementItems.UnitPrice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0; ;
                                    //RequirementItems.RevUnitPrice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0; ;
                                    //RequirementItems.CGst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0; ;
                                    //RequirementItems.SGst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0; ;
                                    //RequirementItems.IGst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0; ;
                                    //RequirementItems.UnitMRP = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0; ;
                                    //RequirementItems.UnitDiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0; ;
                                    //RequirementItems.RevUnitDiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0; ;

                                    ListRequirementItems.Add(RequirementItems);
                                }
                                requirement.ItemSNoCount = productSNo;
                                requirement.ListRequirementItems = ListRequirementItems;
                            }

                            List<RequirementItems> ListQuotations = new List<RequirementItems>();
                            if (ds.Tables[3].Rows.Count > 0)
                            {
                                foreach (DataRow row3 in ds.Tables[3].Rows)
                                {
                                    RequirementItems Quotation = new RequirementItems();
                                    //Quotation.ProductSNo = productSNo++;
                                    //Quotation.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                    //Quotation.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                                    //Quotation.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                    //Quotation.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                    //Quotation.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                    //Quotation.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                                    //Quotation.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                    //Quotation.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                                    //Quotation.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                                    //Quotation.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                                    //Quotation.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                                    //Quotation.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                                    Quotation.ItemID = row3["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row3["ITEM_ID"]) : 0;
                                    //Quotation.ProductIDorName = row3["PROD_ID"] != DBNull.Value ? Convert.ToString(row3["PROD_ID"]) : string.Empty;
                                    //Quotation.ProductNo = row3["PROD_NO"] != DBNull.Value ? Convert.ToString(row3["PROD_NO"]) : string.Empty;
                                    //Quotation.ProductDescription = row3["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row3["DESCRIPTION"]) : string.Empty;
                                    //Quotation.ProductBrand = row3["BRAND"] != DBNull.Value ? Convert.ToString(row3["BRAND"]) : string.Empty;

                                    Quotation.ItemPrice = row3["PRICE"] != DBNull.Value ? Convert.ToDouble(row3["PRICE"]) : 0; ;
                                    //Quotation.ProductIDorName = row3["PROD_ID"] != DBNull.Value ? Convert.ToString(row3["PROD_ID"]) : string.Empty; ;
                                    //Quotation.ProductNo = row3["PROD_NO"] != DBNull.Value ? Convert.ToString(row3["PROD_NO"]) : string.Empty; ;
                                    //Quotation.ProductDescription = row3["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row3["DESCRIPTION"]) : string.Empty; ;
                                    //Quotation.ProductBrand = row3["BRAND"] != DBNull.Value ? Convert.ToString(row3["BRAND"]) : string.Empty; ;
                                    Quotation.OthersBrands = row3["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row3["OTHER_BRANDS"]) : string.Empty; ;
                                    Quotation.RevItemPrice = row3["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REVICED_PRICE"]) : 0; ;
                                    Quotation.UnitPrice = row3["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_PRICE"]) : 0; ;
                                    Quotation.RevUnitPrice = row3["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_PRICE"]) : 0; ;
                                    Quotation.CGst = row3["C_GST"] != DBNull.Value ? Convert.ToDouble(row3["C_GST"]) : 0; ;
                                    Quotation.SGst = row3["S_GST"] != DBNull.Value ? Convert.ToDouble(row3["S_GST"]) : 0; ;
                                    Quotation.IGst = row3["I_GST"] != DBNull.Value ? Convert.ToDouble(row3["I_GST"]) : 0; ;
                                    Quotation.UnitMRP = row3["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_MRP"]) : 0; ;
                                    Quotation.UnitDiscount = row3["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row3["UNIT_DISCOUNT"]) : 0; ;
                                    Quotation.RevUnitDiscount = row3["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row3["REV_UNIT_DISCOUNT"]) : 0; ;

                                    ListQuotations.Add(Quotation);
                                }
                            }

                            foreach(var item in requirement.ListRequirementItems)
                            {
                                foreach (var quote in ListQuotations)
                                {
                                    if(item.ItemID == quote.ItemID)
                                    {
                                        //item.ProductIDorName = quote.ProductIDorName;
                                        //item.ProductNo = quote.ProductNo;
                                        //item.ProductDescription = quote.ProductDescription;
                                        //item.ProductBrand = quote.ProductBrand;
                                        item.ItemPrice = quote.ItemPrice;
                                        item.OthersBrands = quote.OthersBrands;
                                        item.RevItemPrice = quote.RevItemPrice;
                                        item.UnitPrice = quote.UnitPrice;
                                        item.RevUnitPrice = quote.RevUnitPrice;
                                        item.CGst = quote.CGst;
                                        item.SGst = quote.SGst;
                                        item.IGst = quote.IGst;
                                        item.UnitMRP = quote.UnitMRP;
                                        item.UnitDiscount = quote.UnitDiscount;
                                        item.RevUnitDiscount = quote.RevUnitDiscount;
                                    }
                                }
                            }



                        }
                    }
                    catch (Exception ex)
                    {
                        Requirement req = new Requirement();
                        req.ErrorMessage = ex.Message;
                        return req;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return requirement;
        }

        public Response SavePriceCap(int reqID, List<RequirementItems> listReqItems, 
            double price, int isDiscountQuotation, string sessionID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        foreach (var item in listReqItems)
                        {
                            cmd.CommandText = "pc_SavePriceCapItemPrices";

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", item.RequirementID));
                            cmd.Parameters.Add(new MySqlParameter("P_ITEM_ID", item.ItemID));
                            cmd.Parameters.Add(new MySqlParameter("P_PRICE", item.ItemPrice));
                            cmd.Parameters.Add(new MySqlParameter("P_REVICED_PRICE", item.RevItemPrice));
                            cmd.Parameters.Add(new MySqlParameter("P_UNIT_PRICE", item.UnitPrice));
                            cmd.Parameters.Add(new MySqlParameter("P_REV_UNIT_PRICE", item.RevUnitPrice));
                            cmd.Parameters.Add(new MySqlParameter("P_C_GST", item.CGst));
                            cmd.Parameters.Add(new MySqlParameter("P_S_GST", item.SGst));
                            cmd.Parameters.Add(new MySqlParameter("P_I_GST", item.IGst));
                            cmd.Parameters.Add(new MySqlParameter("P_UNIT_MRP", item.UnitMRP));
                            cmd.Parameters.Add(new MySqlParameter("P_UNIT_DISCOUNT", item.UnitDiscount));
                            cmd.Parameters.Add(new MySqlParameter("P_REV_UNIT_DISCOUNT", item.RevUnitDiscount));

                            MySqlDataAdapter myDA1 = new MySqlDataAdapter(cmd);
                            DataSet ds1 = new DataSet();
                            myDA1.Fill(ds1);
                            cmd.Connection.Close();

                            if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                            {
                                //response.ObjectID = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                                //response.ErrorMessage = ds1.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds1.Tables[0].Rows[0][1].ToString()) : string.Empty;
                            }
                            cmd.Parameters.Clear();
                        }

                        double REV_VEND_TOTAL_PRICE_NO_TAX = listReqItems.Sum(item => item.RevUnitPrice * item.ProductQuantity);
                        double INIT_VEND_TOTAL_PRICE_NO_TAX = listReqItems.Sum(item => item.UnitPrice * item.ProductQuantity);

                        cmd.CommandText = "pc_SavePriceCap";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new MySqlParameter("P_REQ_ID", reqID));
                        cmd.Parameters.Add(new MySqlParameter("P_PRICE", price));
                        cmd.Parameters.Add(new MySqlParameter("P_IS_DISCOUNT_QUOTATION", isDiscountQuotation));
                        cmd.Parameters.Add(new MySqlParameter("P_INIT_VEND_TOTAL_PRICE_NO_TAX", INIT_VEND_TOTAL_PRICE_NO_TAX));
                        cmd.Parameters.Add(new MySqlParameter("P_REV_VEND_TOTAL_PRICE_NO_TAX", REV_VEND_TOTAL_PRICE_NO_TAX));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }

        #endregion
    }
}