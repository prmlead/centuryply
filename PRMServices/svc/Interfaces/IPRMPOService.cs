﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;
using test = PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPOService
    {
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoinformation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        //List<POInformation> GetPOInformation(int reqID, int userID, string sessionID);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "savepoinfo")]
        //Response SavePOInfo(POInformation[] poList, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdespoinfo?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        POVendor GetDesPoInfo(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendors?reqid={reqID}&sessionid={sessionID}")]
        List<UserDetails> GetVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdescdispatch?poid={poID}&dtid={dtID}&sessionid={sessionID}")]
        DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID);
        
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpolist?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpoinfo?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtracklist?poid={poID}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrackList(int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtrack?poid={poID}&dtid={dispatchTrackID}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrack(int poID, int dispatchTrackID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpaymenttrack?vendorid={vendorID}&poid={poID}&sessionid={sessionID}")]
        PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getgrndetailslist?compid={compid}&uid={uid}&search={search}" +
           "&supplier={supplier}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
           "&sessionid={sessionid}")]
        List<GRNDetails> GetGRNDetailsList(int compid, string uid, string search, string supplier, string fromdate, string todate, int page, int pagesize, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "generatepopdf?ponumber={ponumber}&sessionid={sessionid}")]
        string GeneratePOPDF(string ponumber, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulelist?compid={compid}&uid={uid}&search={search}" +
         "&categoryid={categoryid}&productid={productid}&supplier={supplier}&postatus={postatus}&deliverystatus={deliverystatus}&plant={plant}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
         "&onlycontracts={onlycontracts}&excludecontracts={excludecontracts}&ackStatus={ackStatus}&buyer={buyer}&purchaseGroup={purchaseGroup}&sessionid={sessionid}")]
        List<POScheduleDetails> GetPOScheduleList(int compid, int uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
         string fromdate, string todate, int page, int pagesize, int onlycontracts, int excludecontracts, string ackStatus, string buyer, string purchaseGroup, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulelistexport?compid={compid}&uid={uid}&search={search}" +
           "&categoryid={categoryid}&productid={productid}&supplier={supplier}&postatus={postatus}&deliverystatus={deliverystatus}&plant={plant}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
           "&onlycontracts={onlycontracts}&excludecontracts={excludecontracts}&ackStatus={ackStatus}&buyer={buyer}&purchaseGroup={purchaseGroup}&sessionid={sessionid}")]
        List<POScheduleDetailsItems> GetPOScheduleList_Export(int compid, int uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
           string fromdate, string todate, int page, int pagesize, int onlycontracts, int excludecontracts, string ackStatus, string buyer, string purchaseGroup, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulefilters?compid={compid}&sessionid={sessionid}")]
        List<CArrayKeyValue> GetPOScheduleFilters(int compid, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getasndetails?compid={compid}&asnid={asnid}&ponumber={ponumber}&grncode={grncode}&asncode={asncode}&vendorid={vendorid}&sessionid={sessionid}")]
        List<ASNDetails> GetASNDetails(int compid, int asnid, string asncode, string ponumber, string grncode, int vendorid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getasndetailslist?ponumber={ponumber}&vendorid={vendorid}&sessionid={sessionid}")]
        List<ASNDetails> GetASNDetailsList(string ponumber, int vendorid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposcheduleitems?ponumber={ponumber}&moredetails={moredetails}&forasn={forasn}&sessionid={sessionid}")]
        List<POScheduleDetailsItems> GetPOScheduleItems(string ponumber, int moredetails, bool forasn, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpogeneratedetails?compid={compid}&template={template}&vendorid={vendorid}&status={status}&creator={creator}" +
            "&plant={plant}&purchasecode={purchasecode}&search={search}&sessionid={sessionid}&page={page}&pagesize={pagesize}")]
        SAPOEntity[] GetPOGenerateDetails(int compid, string template, int vendorid, string status, string creator,
            string plant, string purchasecode, string search, string sessionid, int page = 0, int pagesize = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoitems?ponumber={ponumber}&quotno={quotno}&sessionid={sessionid}")]
        SAPOEntity[] GetPOItems(string ponumber, string quotno, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "postpodata")]
        Response PostPOData(POScheduleDetailsItems[] data, bool isDraft);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "savepovendorquantityack")]
        Response SavePOVendorQuantityAck(string ponumber, string poitemline, string status, string comments, int isVendPoAck, decimal quantity, int user, string sessionid);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "checkuniqueifexists")]
        bool CheckUniqueIfExists(string param, string idtype, string sessionID);




        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedescpoinfo")]
        Response SaveDescPoInfo(POVendor povendor);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatepostatus")]
        Response UpdatePOStatus(int poID, string status, string sessionID);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedesdispatchtrack")]
        Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor);        

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedispatchtrack")]
        Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savevendorpoinfo")]
        Response SaveVendorPOInfo(VendorPO vendorpo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepaymentinfo")]
        Response SavePaymentInfo(PaymentInfo paymentinfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savePOInvoice")]
        Response SavePOInvoice(List<POInvoiceDet> INVOICE_ARR, bool doSubmitToSAP, int rowId, string sessionId);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPaymentInvoiceDetails?sessionid={sessionid}&compId={compId}&uId={uId}&poNumber={poNumber}&grnNumber={grnNumber}&invoiceNumber={invoiceNumber}&vendorCode={vendorCode}&fromdate={fromdate}&todate={todate}&search={search}")]
        List<PaymentTracking> GetPaymentInvoiceDetails(string sessionid, int compId, int uId, string poNumber, string grnNumber, string invoiceNumber, string vendorCode, string fromdate, string todate,string search);

        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getInvoiceList?COMP_ID={COMP_ID}&U_ID={U_ID}&sessionid={sessionid}&poNumber={poNumber}&invoiceNumber={invoiceNumber}&supplier={supplier}&grnNumber={grnNumber}&fromdate={fromdate}&todate={todate}&search={search}")]
        List<POInvoiceDet> GetInvoiceList(int COMP_ID, int U_ID, string sessionid, string poNumber, string invoiceNumber, string supplier, string grnNumber, string fromdate, string todate, string search);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getPaymentTrackingListByFilter?COMP_ID={COMP_ID}&USER_ID={USER_ID}&sessionid={sessionid}")]
        List<PaymentTracking> getPaymentTrackingListByFilter(int COMP_ID, int USER_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getInvoiceListByFilter?COMP_ID={COMP_ID}&USER_ID={USER_ID}&sessionid={sessionid}")]
        List<POInvoiceDet> getInvoiceListByFilter(int COMP_ID, int USER_ID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getInvoiceByID")]
        string GetInvoiceByID();

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepoinvoiceform")]
        Response SavePOInvoiceForm(test.POInvoice[] poInvDet, int invoiceId);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savePOInvoiceStatus")]
        Response savePOInvoiceStatus(string invoiceNumber, string invoiceStatus, string invoiceStatusComments, string sessionId);



        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInvoiceAudit")]
        string GetInvoiceAudit();


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInvoiceItems")]
        string GetInvoiceItems();


    }
}
