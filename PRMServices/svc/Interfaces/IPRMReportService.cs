﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMReportService
    {
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getlivebiddingreport?reqid={reqID}&count={count}&sessionid={sessionID}")]
        LiveBidding[] GetLiveBiddingReport(int reqID, int count, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getitemwisereport?reqid={reqID}&sessionid={sessionID}")]
        ItemWiseReport[] GetItemWiseReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deliverytimelinereport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetDeliveryTimeLineReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "paymenttermsreport?reqid={reqID}&sessionid={sessionID}")]
        DeliveryTimeLine[] GetPaymentTermsReport(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqdetails?reqid={reqID}&sessionid={sessionID}")]
        ReportsRequirement GetReqDetails(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "gettemplates?template={template}&compid={compID}&userid={userID}&reqid={reqID}&from={fromDate}&to={toDate}&sessionid={sessionID}")]
        string GetTemplates(string template, int compID, int userID, int reqID, string fromDate, string toDate, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getreqreportforexcel?reqid={reqID}&sessionid={sessionID}")]
        ExcelRequirement GetReqReportForExcel(int reqID, string sessionID);

        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getReqItemWiseVendors?reqid={reqID}&sessionid={sessionID}")]
        //MACRequirement GetReqItemWiseVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedReport> GetConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getLogisticConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<LogisticConsolidatedReport> GetLogisticConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAccountingConsolidatedreports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<AccountingConsolidatedReport> GetAccountingConsolidatedReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedtemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetConsolidatedTemplates(string template, string from, string to, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getconsolidatedbasepricereports?from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        List<ConsolidatedBasePriceReports> GetConsolidatedBasePriceReports(string sessionID, string from, string to, int userID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserLoginTemplates?template={template}&from={from}&to={to}&userid={userID}&sessionid={sessionID}")]
        string GetUserLoginTemplates(string template, string from, string to, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetLogsTemplates?template={template}&from={from}&to={to}&companyID={companyID}&sessionid={sessionID}")]
        string GetLogsTemplates(string template, string from, string to, int companyID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcompanysavingstats?compid={compid}&fromDate={fromDate}&toDate={toDate}&sessionid={sessionid}")]
        CArrayKeyValue[] GetCompanySavingStats(int compid, DateTime fromDate, DateTime toDate, string sessionid);

        #region QCS

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQcsReport?template={template}&reqid={reqid}&qcsID={qcsID}&userID={userID}&sessionid={sessionID}")]
        string GetQcsReport(string template, int reqid, int qcsID,int userID,string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getqcslist?uid={uid}&reqid={reqid}&sessionid={sessionid}")]
        List<QCSDetails> GetQCSList(int uid, int reqid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQCSIDS?reqid={reqid}&sessionid={sessionid}&qcsid={qcsid}")]
        List<QCSDetails> GetQCSIDS(int reqid, string sessionid, int qcsid = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GeQcsAudit?reqID={reqID}&sessionid={sessionid}")]
        List<QCSAudit> GeQcsAudit(int reqID, string sessionid);


        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "qcsdetails?qcsid={qcsid}&sessionid={sessionid}")]
        QCSDetails GetQCSDetails(int qcsid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQcsAuditDetails?reqID={reqID}&qcsID={qcsID}&verID={verID}&sessionid={sessionid}")]
        QCSAudit GetQcsAuditDetails(int reqID, int qcsID, int verID, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetQcsPRDetails?reqID={reqID}&sessionid={sessionid}")]
        QCSPRDetails GetQcsPRDetails(int reqID, string sessionid);


        [OperationContract]
       [WebInvoke(Method = "POST",
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       ResponseFormat = WebMessageFormat.Json,
       RequestFormat = WebMessageFormat.Json,
       UriTemplate = "DeleteQcs")]
        Response DeleteQcs(int reqID, int qcsID, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "ActivateQcs")]
        Response ActivateQcs(int reqID, int qcsID, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "saveqcsdetails")]
        Response SaveQCSDetails(QCSDetails qcsdetails, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "GetNewQuotations")]
        Response GetNewQuotations(QCSDetails qcsdetails, bool isNewQuotation, string sessionid);

        #endregion QCS


    }
}
