﻿using System;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Configuration;
using PRMServices.Models;
using System.Data;
using System.ServiceModel.Activation;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Xml;
using System.ComponentModel;
using System.Threading.Tasks;
using PushSharp.Google;
using Newtonsoft.Json.Linq;
using PRMServices.SQLHelper;
using System.Net.Http;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMManagement : IPRMManagement
    {
        private static MySQLBizClass sqlHelper = new MySQLBizClass();

        #region Public Methods

        #region Gets

        public MngtDashboardStats GetMngtDashboardStats(string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

            MngtDashboardStats MngtDashBoard = new MngtDashboardStats();
            try
            {
                DataSet ds = sqlHelper.SelectList("cp_mngt_DashBoardStatus", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    MngtDashBoard.TotalTurnover = row["PARAM_TOTAL_TURN_OVER"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_TURN_OVER"]) : 0;
                    MngtDashBoard.TotalSavings = row["PARAM_TOTAL_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_SAVINGS"]) : 0;
                    MngtDashBoard.AvgSavingsValue = row["PARAM_AVG_SAVINGS_VALUE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_AVG_SAVINGS_VALUE"]) : 0;
                    MngtDashBoard.AvgSavingsPercentage = row["PARAM_AVG_SAVINGS_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_AVG_SAVINGS_PERCENTAGE"]) : 0;
                    MngtDashBoard.NoOfNegotiationsRunning = row["PARAM_NO_OF_NEGOTIATIONS_RUNNING"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NEGOTIATIONS_RUNNING"]) : 0;
                    MngtDashBoard.NoOfNegotiationsCompleted = row["PARAM_NO_OF_NEGOTIATIONS_COMPLETED"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NEGOTIATIONS_COMPLETED"]) : 0;
                    MngtDashBoard.NoOfNegotiationsNotstarted = row["PARAM_NO_OF_NEGOTIATIONS_NOTSTARTED"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NEGOTIATIONS_NOTSTARTED"]) : 0;
                    MngtDashBoard.NoOfCustomerRegisterd = row["PARAM_NO_OF_CUSTOMERS_REGISTERD"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_CUSTOMERS_REGISTERD"]) : 0;
                    MngtDashBoard.NoOfVendorsRegisterd = row["PARAM_NO_OF_VENDORS_REGISTERD"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_VENDORS_REGISTERD"]) : 0;
                    MngtDashBoard.NoOfCustomerRegisterdToday = row["PARAM_NO_OF_CUSTOMERS_REGISTERD_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_CUSTOMERS_REGISTERD_TODAY"]) : 0;
                    MngtDashBoard.NoOfVendorsRegisterdToday = row["PARAM_NO_OF_VENDORS_REGISTERD_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_VENDORS_REGISTERD_TODAY"]) : 0;
                    MngtDashBoard.NoOfSubscribedCustomer = row["PARAM_NO_OF_SUBSCRIBED_CUSTOMERS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_SUBSCRIBED_CUSTOMERS"]) : 0;
                }
            }
            catch (Exception ex)
            {
                MngtDashBoard.ErrorMessage = ex.Message;
            }

            return MngtDashBoard;
        }

        public MngtDashboardStats GetMrktngDashboardStats(string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            MngtDashboardStats MngtDashBoard = new MngtDashboardStats();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                DataSet ds = sqlHelper.SelectList("cp_mngt_MrktngDashBoardStatus", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    MngtDashBoard.TotalTurnover = row["PARAM_TODAYS_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TODAYS_SAVINGS"]) : 0;
                    MngtDashBoard.NoOfNegotiationsRunning = row["PARAM_NO_OF_NEGOTIATIONS_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NEGOTIATIONS_TODAY"]) : 0;
                    MngtDashBoard.NoOfNegotiationsCompleted = row["PARAM_NO_OF_NEGOTIATIONS_WEEK"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NEGOTIATIONS_WEEK"]) : 0;
                    MngtDashBoard.NoOfNegotiationsNotstarted = row["PARAM_NO_OF_NEGOTIATIONS_LASTWEEK"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_NEGOTIATIONS_LASTWEEK"]) : 0;
                    MngtDashBoard.NoOfCustomerRegisterdToday = row["PARAM_NO_OF_CUSTOMERS_REGISTERD_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_CUSTOMERS_REGISTERD_TODAY"]) : 0;
                    MngtDashBoard.NoOfVendorsRegisterdToday = row["PARAM_NO_OF_VENDORS_REGISTERD_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_VENDORS_REGISTERD_TODAY"]) : 0;
                }
            }
            catch (Exception ex)
            {
                MngtDashBoard.ErrorMessage = ex.Message;
            }

            return MngtDashBoard;
        }

        public List<MngtUserDetails> GetUsersList(string sessionID, string userRole, string fromDate, string toDate)
        {
            List<MngtUserDetails> ListMngtUserDetails = new List<MngtUserDetails>();
            try
            {
                Utilities.ValidateSession(sessionID, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                DateTime fromDatetime = DateTime.MinValue;
                DateTime toDatetime = DateTime.MaxValue;
                if (!string.IsNullOrEmpty(fromDate))
                {
                    fromDatetime = Convert.ToDateTime(fromDate);
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    toDatetime = Convert.ToDateTime(toDate);
                }

                sd.Add("P_U_TYPE", userRole);
                sd.Add("P_FROM_DATE", fromDatetime);
                sd.Add("P_TO_DATE", toDatetime);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GrtUsersList", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtUserDetails MngtUserDetails = SetFeildsToMngtUserDetails(row, "GetUsersList");
                        ListMngtUserDetails.Add(MngtUserDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtUserDetails MngtUserDetails = new MngtUserDetails();
                MngtUserDetails.ErrorMessage = ex.Message;
            }

            return ListMngtUserDetails;
        }

        public MngtUserDetails GetUserDetails(string sessionID, int userId)
        {
            MngtUserDetails MngtUserDetails = new MngtUserDetails();
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_U_ID", userId);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetUserDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    MngtUserDetails = SetFeildsToMngtUserDetails(row, "GetUserDetails");
                    List<Credentials> ListCredentials = new List<Credentials>();
                    if (ds != null && ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            Credentials credential = new Credentials();
                            credential.IsVerified = row1["U_IS_VERIFIED"] != DBNull.Value ? Convert.ToInt16(row1["U_IS_VERIFIED"]) : 0;
                            credential.FileLink = row1["U_VERIFICATION_DOC"] != DBNull.Value ? Convert.ToString(row1["U_VERIFICATION_DOC"]) : string.Empty;
                            credential.FileType = row1["U_VERIFICATION_TYPE"] != DBNull.Value ? Convert.ToString(row1["U_VERIFICATION_TYPE"]) : string.Empty;
                            credential.CredentialID = row1["U_CRED_ID"] != DBNull.Value ? Convert.ToString(row1["U_CRED_ID"]) : string.Empty;
                            string URL = credential.FileLink != string.Empty ? credential.FileLink : string.Empty;
                            credential.FileLink = URL;
                            ListCredentials.Add(credential);
                        }

                        MngtUserDetails.UserCredentials = ListCredentials;
                    }

                    DashboardStats DashboardStats = new DashboardStats();
                    if (ds != null && ds.Tables[2].Rows.Count > 0)
                    {
                        DataRow row2 = ds.Tables[2].Rows[0];
                        DashboardStats.TotalAuctions = row2["TOTALAUCTIONS"] != DBNull.Value ? Convert.ToInt32(row2["TOTALAUCTIONS"]) : 0;
                        DashboardStats.TotalOrders = row2["TOTALORDERS"] != DBNull.Value ? Convert.ToInt32(row2["TOTALORDERS"]) : 0;
                        DashboardStats.TotalSaved = row2["TOTALSAVED"] != DBNull.Value ? Convert.ToDouble(row2["TOTALSAVED"]) : 0;
                        DashboardStats.TotalTurnover = row2["TOTALTURNOVER"] != DBNull.Value ? Convert.ToDouble(row2["TOTALTURNOVER"]) : 0;
                        MngtUserDetails.UserDashboardStats = DashboardStats;
                    }

                    if (ds != null && ds.Tables[3].Rows.Count > 0)
                    {
                        List<Requirement> Listrequirement = new List<Requirement>();
                        foreach (DataRow row3 in ds.Tables[3].Rows)
                        {
                            Requirement requirement = new Requirement();
                            string[] arr = new string[] { };
                            byte[] next = new byte[] { };
                            requirement.RequirementID = row3["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row3["REQ_ID"]) : -1;
                            requirement.Title = row3["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row3["REQ_TITLE"]) : string.Empty;
                            requirement.EndTime = row3["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row3["END_TIME"]) : DateTime.MaxValue;
                            if (requirement.EndTime == null)
                            {
                                requirement.EndTime = DateTime.MaxValue;
                            }

                            requirement.StartTime = row3["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row3["START_TIME"]) : DateTime.MaxValue;
                            if (requirement.StartTime == null)
                            {
                                requirement.StartTime = DateTime.MaxValue;
                            }

                            requirement.PostedOn = row3["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row3["REQ_POSTED_ON"]) : DateTime.MaxValue;
                            requirement.Price = row3["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["VEND_MIN_PRICE"]) : 0;
                            double RunPrice = row3["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row3["VEND_MIN_RUN_PRICE"]) : 0;
                            DateTime start = row3["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row3["START_TIME"]) : DateTime.MaxValue;
                            if (start < DateTime.Now && RunPrice > 0)
                            {
                                requirement.Price = RunPrice;
                            }

                            string POLink = row3["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row3["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                            requirement.Description = row3["REQ_DESC"] != DBNull.Value ? Convert.ToString(row3["REQ_DESC"]) : string.Empty;
                            requirement.Category = row3["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row3["REQ_CATEGORY"]).Split(',') : arr;
                            requirement.Urgency = row3["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row3["REQ_URGENCY"]) : string.Empty;
                            requirement.Budget = row3["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row3["REQ_BUDGET"]) : string.Empty;
                            requirement.DeliveryLocation = row3["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row3["REQ_DELIVERY_LOC"]) : string.Empty;
                            requirement.Taxes = row3["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row3["REQ_TAXES"]) : string.Empty;
                            requirement.PaymentTerms = row3["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row3["REQ_PAYMENT_TERMS"]) : string.Empty;
                            requirement.Currency = row3["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row3["REQ_CURRENCY"]) : string.Empty;
                            requirement.Status = row3["CLOSED"] != DBNull.Value ? Convert.ToString(row3["CLOSED"]) : string.Empty;
                            requirement.CustomerID = row3["U_ID"] != DBNull.Value ? Convert.ToInt16(row3["U_ID"]) : 0;
                            Listrequirement.Add(requirement);
                        }

                        MngtUserDetails.UserListRequirement = Listrequirement;
                    }
                }
            }
            catch (Exception ex)
            {
                MngtUserDetails.ErrorMessage = ex.Message;
            }

            return MngtUserDetails;
        }

        public List<MngtRequirementDetails> GetRequirementsList(string sessionID, string status, string fromDate, string toDate)
        {
            List<MngtRequirementDetails> ListMngtRequirementDetails = new List<MngtRequirementDetails>();
            try
            {
                Utilities.ValidateSession(sessionID, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                DateTime fromDatetime = DateTime.MinValue;
                DateTime toDatetime = DateTime.MaxValue;
                if (!string.IsNullOrEmpty(fromDate))
                {
                    fromDatetime = Convert.ToDateTime(fromDate);
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    toDatetime = Convert.ToDateTime(toDate);
                }

                sd.Add("P_STATUS", status);
                sd.Add("P_FROM_DATE", fromDatetime);
                sd.Add("P_TO_DATE", toDatetime);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetRequirements", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtRequirementDetails MngtRequirementDetails = SetFeildsToMngtRequirementDetails(row, "GetRequirementsList");
                        ListMngtRequirementDetails.Add(MngtRequirementDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtRequirementDetails MngtRequirementDetails = new MngtRequirementDetails();
                MngtRequirementDetails.ErrorMessage = ex.Message;
            }

            return ListMngtRequirementDetails;
        }

        public MngtRequirementDetails GetRequirementDetails(string sessionID, int reqId)
        {
            MngtRequirementDetails MngtRequirementDetails = new MngtRequirementDetails();
            try
            {
                Utilities.ValidateSession(sessionID, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqId);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetRequirementDetails", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    MngtRequirementDetails = SetFeildsToMngtRequirementDetails(row, "GetRequirementDetails");
                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    int rank = 1;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;
                            vendor.Rank = rank;
                            vendorDetails.Add(vendor);
                            rank++;
                        }
                    }
                    MngtRequirementDetails.AuctionVendors = vendorDetails;
                }
            }
            catch (Exception ex)
            {
                MngtRequirementDetails.ErrorMessage = ex.Message;
            }

            return MngtRequirementDetails;
        }

        public List<MngtNotes> GetMngtNotesList(string sessionID, int noteToID, int userID, string noteType)
        {
            List<MngtNotes> ListMngtNotes = new List<MngtNotes>();
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_NOTE_TYPE", noteType);
                sd.Add("P_TO_ID", noteToID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetNotes", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtNotes MngtNotes = new MngtNotes();
                        MngtNotes.NoteId = row["NOTE_ID"] != DBNull.Value ? Convert.ToInt32(row["NOTE_ID"]) : 0;
                        MngtNotes.NoteToId = row["NOTE_TO_ID"] != DBNull.Value ? Convert.ToInt32(row["NOTE_TO_ID"]) : 0;
                        MngtNotes.UserId = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;
                        MngtNotes.Notes = row["NOTES"] != DBNull.Value ? Convert.ToString(row["NOTES"]) : string.Empty;
                        MngtNotes.CreatedDate = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                        ListMngtNotes.Add(MngtNotes);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtNotes MngtNotes = new MngtNotes();
                MngtNotes.ErrorMessage = ex.Message;
            }

            return ListMngtNotes;
        }

        public List<MngtRequirementDetails> GetMngtRequirements(string sessionID, int userID)
        {
            List<MngtRequirementDetails> ListMngtRequirementDetails = new List<MngtRequirementDetails>();
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetMngtRequirements", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtRequirementDetails MngtRequirementDetails = SetFeildsToMngtRequirementDetails(row, "GetMngtRequirements");
                        ListMngtRequirementDetails.Add(MngtRequirementDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtRequirementDetails MngtRequirementDetails = new MngtRequirementDetails();
                MngtRequirementDetails.ErrorMessage = ex.Message;
            }

            return ListMngtRequirementDetails;
        }

        public List<MngtUserDetails> GetSearchUser(string sessionID, string searchKey, string searchKeyType, string searchRole)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<MngtUserDetails> ListMngtUserDetails = new List<MngtUserDetails>();
            try
            {
                searchKey = "%" + searchKey + "%";
                sd.Add("P_SEARCH_KEY", searchKey);
                sd.Add("P_SEARCH_KEY_TYPE", searchKeyType);
                sd.Add("P_SEARCH_ROLE", searchRole);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SearchUser", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtUserDetails MngtUserDetails = new MngtUserDetails();
                        MngtUserDetails.UserId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        if (MngtUserDetails.UserId > 0)
                        {
                            MngtUserDetails.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                            MngtUserDetails.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                            MngtUserDetails.PhoneNumber = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                            MngtUserDetails.EmailId = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                            MngtUserDetails.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        }
                        else
                        {
                            MngtUserDetails.ErrorMessage = row["ERROR_MSG"] != DBNull.Value ? Convert.ToString(row["ERROR_MSG"]) : string.Empty;
                        }

                        ListMngtUserDetails.Add(MngtUserDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtUserDetails MngtUserDetails = new MngtUserDetails();
                MngtUserDetails.ErrorMessage = ex.Message;
            }

            return ListMngtUserDetails;
        }

        public List<MngtMeetings> GetMeetingsList(string sessionID, int userID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<MngtMeetings> ListMngtMeetings = new List<MngtMeetings>();
            try
            {
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetMeetings", sd);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtMeetings MngtMeetings = new MngtMeetings();

                        MngtMeetings.MeetingId = row["M_ID"] != DBNull.Value ? Convert.ToInt32(row["M_ID"]) : 0;
                        MngtMeetings.UserId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        MngtMeetings.MeetingDate = row["M_DATE"] != DBNull.Value ? Convert.ToDateTime(row["M_DATE"]) : DateTime.MaxValue;
                        MngtMeetings.MeetingTitle = row["M_TITLE"] != DBNull.Value ? Convert.ToString(row["M_TITLE"]) : string.Empty;
                        MngtMeetings.MeetingDescription = row["M_DESC"] != DBNull.Value ? Convert.ToString(row["M_DESC"]) : string.Empty;
                        MngtMeetings.MeetingStatus = row["M_STATUS"] != DBNull.Value ? Convert.ToString(row["M_STATUS"]) : string.Empty;
                        MngtMeetings.CreatedDate = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                        ListMngtMeetings.Add(MngtMeetings);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtNotes MngtNotes = new MngtNotes();
                MngtNotes.ErrorMessage = ex.Message;
            }

            return ListMngtMeetings;
        }

        public List<MngtCompanies> GetMeetings(string sessionID, DateTime fromDate, DateTime toDate)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<MngtCompanies> ListMngtCompanies = new List<MngtCompanies>();
            try
            {
                DataSet ds = sqlHelper.SelectList("cp_mngt_getCompanies", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtCompanies mngtCompanies = new MngtCompanies();
                        mngtCompanies.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        mngtCompanies.PersonWorking = row["PERSON_WORKING"] != DBNull.Value ? Convert.ToString(row["PERSON_WORKING"]) : string.Empty;
                        mngtCompanies.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        mngtCompanies.Website = row["WEBSITE"] != DBNull.Value ? Convert.ToString(row["WEBSITE"]) : string.Empty;
                        mngtCompanies.TotalTurnover = row["TOTAL_TURNOVER"] != DBNull.Value ? Convert.ToString(row["TOTAL_TURNOVER"]) : string.Empty;
                        mngtCompanies.TouchpointOutCome = row["TP_OUTCOME"] != DBNull.Value ? Convert.ToString(row["TP_OUTCOME"]) : string.Empty;
                        mngtCompanies.TouchpointComments = row["TP_COMMENTS"] != DBNull.Value ? Convert.ToString(row["TP_COMMENTS"]) : string.Empty;
                        mngtCompanies.TouchpointDateModified = row["TP_DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["TP_DATE_MODIFIED"]) : DateTime.MinValue;
                        mngtCompanies.TouchpointMeetingDate = row["TP_DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["TP_DATE_TIME"]) : DateTime.MinValue;
                        mngtCompanies.TouchpointContactName = row["TP_CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["TP_CONTACT_NAME"]) : string.Empty;
                        mngtCompanies.TouchpointContactDesg = row["TP_CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["TP_CONTACT_DESIGNATION"]) : string.Empty;
                        mngtCompanies.TouchpointContactInfo = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                        mngtCompanies.CompAssTo = row["ASSIGNED_TO"] != DBNull.Value ? Convert.ToInt32(row["ASSIGNED_TO"]) : 0;
                        mngtCompanies.CompAssBy = row["ASSIGNED_BY"] != DBNull.Value ? Convert.ToInt32(row["ASSIGNED_BY"]) : 0;
                        mngtCompanies.InsideSalesComments = row["INSIDE_SALES_COMMENTS"] != DBNull.Value ? Convert.ToString(row["INSIDE_SALES_COMMENTS"]) : string.Empty;
                        mngtCompanies.ResearchTeamComments = row["RESEARCH_TEAM_COMMENTS"] != DBNull.Value ? Convert.ToString(row["RESEARCH_TEAM_COMMENTS"]) : string.Empty;
                        mngtCompanies.MarketingTeamComments = row["MARKETING_TEAM_COMMENTS"] != DBNull.Value ? Convert.ToString(row["MARKETING_TEAM_COMMENTS"]) : string.Empty;
                        mngtCompanies.GeneralComments = row["GENERAL_COMMENTS"] != DBNull.Value ? Convert.ToString(row["GENERAL_COMMENTS"]) : string.Empty;
                        ListMngtCompanies.Add(mngtCompanies);
                    }
                    
                    ListMngtCompanies = ListMngtCompanies.Where(v => v.TouchpointMeetingDate >= fromDate && v.TouchpointMeetingDate <= toDate).OrderBy(v => v.TouchpointMeetingDate).ToList();
                }
            }
            catch (Exception ex)
            {
                MngtCompanies mngtCompanies = new MngtCompanies();
                mngtCompanies.ErrorMessage = ex.Message;
            }

            return ListMngtCompanies;
        }

        public MngtDashboardStats GetUserDashboardStats(string sessionID, int userID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            MngtDashboardStats MngtDashBoard = new MngtDashboardStats();
            try
            {
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetUserDashboardStats", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    MngtDashBoard.NoOfCallsMadeToday = row["PARAM_NO_OF_CALLS_MADE_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_CALLS_MADE_TODAY"]) : 0;
                    MngtDashBoard.NoOfMeetingsSetForToday = row["PARAM_NO_OF_MEETINGS_SET_FOR_TODAY"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_MEETINGS_SET_FOR_TODAY"]) : 0;
                    MngtDashBoard.NoOfMeetingsSetForMonth = row["PARAM_NO_OF_MEETINGS_SET_FOR_MONTH"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_MEETINGS_SET_FOR_MONTH"]) : 0;
                    MngtDashBoard.NoOfProspectiveClousers = row["PARAM_NO_OF_PROSPECTIVE_CLOUSER"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_PROSPECTIVE_CLOUSER"]) : 0;
                    MngtDashBoard.NoOfClousers = row["PARAM_NO_OF_TOTAL_CLOUSER"] != DBNull.Value ? Convert.ToInt32(row["PARAM_NO_OF_TOTAL_CLOUSER"]) : 0;
                    MngtDashBoard.TomorowMeetingsCount = row["PARAM_TOMORROW_MEETINGS_COUNT"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOMORROW_MEETINGS_COUNT"]) : 0;
                }

                List<MngtTouchpoint> listMeetings = new List<MngtTouchpoint>();
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row1 in ds.Tables[1].Rows)
                    {
                        MngtTouchpoint meetings = new MngtTouchpoint();

                        meetings.CompanyID = row1["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row1["COMP_ID"]) : 0;
                        meetings.CompanyName = row1["COMP_NAME"] != DBNull.Value ? Convert.ToString(row1["COMP_NAME"]) : string.Empty;
                        meetings.TouchpointDate = row1["DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row1["DATE_TIME"]) : DateTime.MaxValue;
                        meetings.ContactName = row1["CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row1["CONTACT_NAME"]) : string.Empty;
                        meetings.ContactDesignation = row1["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row1["CONTACT_DESIGNATION"]) : string.Empty;
                        meetings.TouchpoinType = row1["TP_TYPE"] != DBNull.Value ? Convert.ToString(row1["TP_TYPE"]) : string.Empty;
                        listMeetings.Add(meetings);
                    }

                    MngtDashBoard.ListMeetings = listMeetings;
                }

                List<MngtUserDetails> listUserStatus = new List<MngtUserDetails>();
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[2].Rows)
                    {
                        MngtUserDetails userStatus = new MngtUserDetails();
                        userStatus.UserId = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        userStatus.FirstName = row2["U_NAME"] != DBNull.Value ? Convert.ToString(row2["U_NAME"]) : string.Empty;
                        userStatus.CallsMadeToday = row2["CALLS_MADE_TODAY"] != DBNull.Value ? Convert.ToInt32(row2["CALLS_MADE_TODAY"]) : 0;
                        userStatus.MeetingsSetToday = row2["MEETINGS_SET_TODAY"] != DBNull.Value ? Convert.ToInt32(row2["MEETINGS_SET_TODAY"]) : 0;
                        foreach (DataRow row3 in ds.Tables[3].Rows)
                        {
                            userStatus.UserId = row3["U_ID"] != DBNull.Value ? Convert.ToInt32(row3["U_ID"]) : 0;
                            userStatus.CompanyName = row3["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row3["COMPANY_NAME"]) : string.Empty;
                        }

                        listUserStatus.Add(userStatus);
                    }

                    MngtDashBoard.ListUserStatus = listUserStatus;
                }
            }
            catch (Exception ex)
            {
                MngtDashBoard.ErrorMessage = ex.Message;
            }

            return MngtDashBoard;
        }

        #endregion Gets

        #region Posts

        public Response VerifyUser(string sessionID, int userID, string verificationType)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            UserInfo user = new UserInfo();
            try
            {
                sd.Add("P_U_ID", userID);
                sd.Add("P_U_TYPE", verificationType);
                DataSet ds = sqlHelper.SelectList("cp_mngt_VerifyUser", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveNote(string sessionID, int toID, int userID, string notes, string noteType)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_TO_ID", toID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_NOTES", notes);
                sd.Add("P_NOTE_TYPE", noteType);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SaveNote", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveMngtRequirement(MngtRequirementDetails MngtRequirementDetails)
        {
            Utilities.ValidateSession(MngtRequirementDetails.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_MU_ID", MngtRequirementDetails.UserId);
                sd.Add("P_CUSTOMER", MngtRequirementDetails.Customer);
                sd.Add("P_REQ_TITLE", MngtRequirementDetails.ReqTitle);
                sd.Add("P_REQ_DESCRIPTION", MngtRequirementDetails.Description);
                sd.Add("P_REQ_CATEGORY", MngtRequirementDetails.Category);
                sd.Add("P_REQ_SUB_CATEGORY", MngtRequirementDetails.Subcategories);
                sd.Add("P_REQ_URGENCY", MngtRequirementDetails.Urgency);
                sd.Add("P_REQ_BUDGET", MngtRequirementDetails.Budget);
                sd.Add("P_REQ_IS_INCLUSIVE_TAX", MngtRequirementDetails.InclusiveTax);
                sd.Add("P_REQ_IS_INCLUSIVE_RREIGHT_CHARGES", MngtRequirementDetails.IncludeFreight);
                sd.Add("P_REQ_DELIVERY_DATE", MngtRequirementDetails.DeliveryTime);
                sd.Add("P_REQ_DELIVERY_LOCATION", MngtRequirementDetails.DeliveryLocation);
                sd.Add("P_REQ_PAYMENT_TERMS", MngtRequirementDetails.PaymentTerms);
                sd.Add("P_REQ_QUOTATION_FREEZE_TIME", MngtRequirementDetails.QuotationFreezTime);
                sd.Add("P_REQ_CURRENCY", MngtRequirementDetails.Currency);
                sd.Add("P_REQ_VENDORS", MngtRequirementDetails.Vendors);
                sd.Add("P_REQ_NOTES", MngtRequirementDetails.Notes);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SaveRequirement", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateDbValues(UpdateDbValues UpdateDbValues)
        {
            Utilities.ValidateSession(UpdateDbValues.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_ID", UpdateDbValues.UserId);
                sd.Add("P_REQ_ID", UpdateDbValues.ReqId);
                sd.Add("P_SAVINGS_BEFORE_NEGOTIATION", UpdateDbValues.SavingsBeforeNegotiation);
                sd.Add("P_MIN_REDUCE_AMOUNT", UpdateDbValues.MinReduceAmount);
                sd.Add("P_VENDOR_COMPARISION", UpdateDbValues.VenodComparisionPrice);
                DataSet ds = sqlHelper.SelectList("cp_mngt_UpdateDbValues", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UpdateSingleVendorPrice(UpdateDbValues UpdateDbValues)
        {
            Utilities.ValidateSession(UpdateDbValues.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_ID", UpdateDbValues.UserId);
                sd.Add("P_REQ_ID", UpdateDbValues.ReqId);
                sd.Add("P_PRICE", UpdateDbValues.VenodPrice);
                DataSet ds = sqlHelper.SelectList("cp_mngt_UpdateSingleVendorPrice", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response AssignVendorsToCustomer(string sessionID, string customerID, string vendorID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_VENDOR_ID", customerID);
                sd.Add("P_CUSTOMER_ID", vendorID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_AssignVendorsToCustomer", sd);
                if (ds != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveMeeting(MngtMeetings MngtMeetings)
        {
            Utilities.ValidateSession(MngtMeetings.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_M_ID", MngtMeetings.MeetingId);
                sd.Add("P_U_ID", MngtMeetings.UserId);
                sd.Add("P_M_DATE", MngtMeetings.MeetingDate);
                sd.Add("P_M_TITLE", MngtMeetings.MeetingTitle);
                sd.Add("P_M_DESC", MngtMeetings.MeetingDescription);
                sd.Add("P_M_STATUS", MngtMeetings.MeetingStatus);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SaveMeeting", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion Posts
       
        #region InsideSales 

        #region posts

        public Response MngtLogin(string phone, string password)
        {
            //Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_PHONE", phone);
                sd.Add("P_U_PASSWORD", password);
                DataSet ds = sqlHelper.SelectList("cp_mngt_login", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                    response.RoleID = ds.Tables[0].Rows[0][2] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][2].ToString()) : -1;

                    string sessionId = string.Empty;
                    if (Convert.ToInt32(response.ObjectID) > 0)
                    {
                        sessionId = Guid.NewGuid().ToString();
                        Utilities.CreateSession(sessionId, response.ObjectID, "WEB", null);
                        response.SessionID = sessionId;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveCompany(MngtCompanies mngtCompanies)
        {
            Utilities.ValidateSession(mngtCompanies.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_ID", mngtCompanies.UserID);
                sd.Add("P_COMP_ID", mngtCompanies.CompID);
                sd.Add("P_COMPANY_NAME", mngtCompanies.CompanyName);
                sd.Add("P_PERSON_WORKING", mngtCompanies.PersonWorking);
                sd.Add("P_WEBSITE", mngtCompanies.Website);
                sd.Add("P_C_TYPE", mngtCompanies.CType);
                sd.Add("P_TOTAL_TURNOVER", mngtCompanies.TotalTurnover);
                sd.Add("P_C_PHONE", mngtCompanies.CompanyPhone);
                sd.Add("P_INDUSTRY_1", mngtCompanies.Industry1);
                sd.Add("P_SUB_INDUSTRY_1", mngtCompanies.SubIndustry1);
                sd.Add("P_INDUSTRY_2", mngtCompanies.Industry2);
                sd.Add("P_SUB_INDUSTRY_2", mngtCompanies.SubIndustry2);
                sd.Add("P_INDUSTRY_3", mngtCompanies.Industry3);
                sd.Add("P_SUB_INDUSTRY_3", mngtCompanies.SubIndustry3);
                sd.Add("P_SECTOR", mngtCompanies.Sector);
                sd.Add("P_LEVEL_OF_OFFICE", mngtCompanies.LevelOfOffice);
                sd.Add("P_COMPANY_ENTITY", mngtCompanies.CompanyEntity);
                sd.Add("P_COMPANY_TYPE", mngtCompanies.CompanyType);
                sd.Add("P_NO_OF_EMP", mngtCompanies.NoOfEmployees);
                sd.Add("P_ADDRESS_LINE_1", mngtCompanies.AddLine1);
                sd.Add("P_ADDRESS_LINE_2", mngtCompanies.AddLine2);
                sd.Add("P_CITY", mngtCompanies.City);
                sd.Add("P_STATE", mngtCompanies.State);
                sd.Add("P_PINCODE", mngtCompanies.PinCode);
                sd.Add("P_STDCODE", mngtCompanies.StdCode);
                sd.Add("P_ASSIGNED_TO", mngtCompanies.CompAssTo);
                sd.Add("P_ASSIGNED_BY", mngtCompanies.CompAssBy);
                sd.Add("P_INSIDE_SALES_COMMENTS", mngtCompanies.InsideSalesComments);
                sd.Add("P_RESEARCH_TEAM_COMMENTS", mngtCompanies.ResearchTeamComments);
                sd.Add("P_MARKETING_TEAM_COMMENTS", mngtCompanies.MarketingTeamComments);
                sd.Add("P_GENERAL_COMMENTS", mngtCompanies.GeneralComments);
                sd.Add("P_WORKING_HOURS", mngtCompanies.WorkingHours);
                sd.Add("P_PRIORITY", mngtCompanies.Priority);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SaveCompany", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                mngtCompanies.ErrorMessage = ex.Message;
            }

            return response;
        }

        public MngtUserDetails SaveCompanyContacts(MngtUserDetails mngtUserDetails)
        {
            Utilities.ValidateSession(mngtUserDetails.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                sd.Add("P_U_ID", mngtUserDetails.UserId);
                sd.Add("P_CC_ID", mngtUserDetails.CompanyContactID);
                sd.Add("P_COMP_ID", mngtUserDetails.CompanyID);
                sd.Add("P_C_NAME", mngtUserDetails.FirstName);
                sd.Add("P_C_PHONE", mngtUserDetails.PhoneNumber);
                sd.Add("P_C_EMAIL", mngtUserDetails.EmailId);
                sd.Add("P_C_DESIGNATION", mngtUserDetails.ContactDesignation);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SaveCompanyContact", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    mngtUserDetails.CompanyContactID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    mngtUserDetails.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                mngtUserDetails.ErrorMessage = ex.Message;
            }

            return mngtUserDetails;
        }

        public Response SaveCompanyNetworks(string sessionID, int compID, int userID, int compNetID, string link, string linkType, string userName)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_CN_ID", compNetID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_LINK", link);
                sd.Add("P_LINK_TYPE", linkType);
                sd.Add("P_USER_NAME", userName);
                DataSet ds = sqlHelper.SelectList("cp_mngt_saveCompanyNetworks", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response SaveTouchpoint(MngtTouchpoint mngtTouchpoint)
        {
            Utilities.ValidateSession(mngtTouchpoint.SessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_TP_ID", mngtTouchpoint.TouchpoinID);
                sd.Add("P_COMP_ID", mngtTouchpoint.CompanyID);
                sd.Add("P_U_ID", mngtTouchpoint.UserID);
                sd.Add("P_TP_TYPE", mngtTouchpoint.TouchpoinType);
                sd.Add("P_CONTACT_NAME", mngtTouchpoint.ContactName);
                sd.Add("P_CONTACT_DESIGNATION", mngtTouchpoint.ContactDesignation);
                sd.Add("P_DATE_TIME", mngtTouchpoint.TouchpointDate);
                sd.Add("P_SPENT_TIME", mngtTouchpoint.SpentTime);
                sd.Add("P_OUTCOME", mngtTouchpoint.OutCome);
                sd.Add("P_COMMENTS", mngtTouchpoint.Comments);
                sd.Add("P_MEETING_LOCATION", mngtTouchpoint.meetingLocation);
                sd.Add("P_MARKETING_ASSIGN_TO", mngtTouchpoint.marketingUser);
                DataSet ds = sqlHelper.SelectList("cp_mngt_SaveTouchpoint", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response AssignUserToCompany(string sessionID, int compID, int userID, int assignedTo)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_ID", userID);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_ASSIGNED_TO", assignedTo);
                DataSet ds = sqlHelper.SelectList("cp_mngt_AssignUserToCompany", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteTouchPoint(string sessionID, int userID, int touchpointID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_TP_ID", touchpointID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_DeleteTouchpoint", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        #endregion posts

        #region gets

        public List<MngtUserDetails> GetInsideSalesUsers(string sessionID)
        {
            List<MngtUserDetails> listUserStatus = new List<MngtUserDetails>();
            try
            {
                Utilities.ValidateSession(sessionID, null);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                DataSet ds = sqlHelper.SelectList("cp_GetInsideSalesUsers", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[0].Rows)
                    {
                        MngtUserDetails userStatus = new MngtUserDetails();
                        userStatus.FirstName = row2["U_NAME"] != DBNull.Value ? Convert.ToString(row2["U_NAME"]) : string.Empty;
                        userStatus.UserId = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                        listUserStatus.Add(userStatus);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtUserDetails userStatus = new MngtUserDetails();
                userStatus.ErrorMessage = ex.Message;
                listUserStatus.Add(userStatus);
            }

            return listUserStatus;
        }

        public List<Response> GetTouchpointStatus(string sessionID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Response> listTouchpointStatus = new List<Response>();
            try
            {
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetTouchpointStatus", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row2 in ds.Tables[0].Rows)
                    {
                        Response TouchpointStatus = new Response();

                        TouchpointStatus.ObjectID = row2["TS_ID"] != DBNull.Value ? Convert.ToInt32(row2["TS_ID"]) : 0;
                        TouchpointStatus.Message = row2["TOUCH_POINT_STATUS"] != DBNull.Value ? Convert.ToString(row2["TOUCH_POINT_STATUS"]) : string.Empty;

                        listTouchpointStatus.Add(TouchpointStatus);
                    }
                }
            }
            catch (Exception ex)
            {
                Response TouchpointStatus = new Response();
                TouchpointStatus.ErrorMessage = ex.Message;
                listTouchpointStatus.Add(TouchpointStatus);
            }

            return listTouchpointStatus;
        }

        public List<MngtCompanies> GetCompanies(string sessionID, int userID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<MngtCompanies> ListMngtCompanies = new List<MngtCompanies>();
            try
            {
                if (userID < 0)
                {
                    userID = 0;
                }

                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_getCompanies", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtCompanies mngtCompanies = new MngtCompanies();
                        mngtCompanies.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        mngtCompanies.PersonWorking = row["PERSON_WORKING"] != DBNull.Value ? Convert.ToString(row["PERSON_WORKING"]) : string.Empty;
                        mngtCompanies.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        mngtCompanies.Website = row["WEBSITE"] != DBNull.Value ? Convert.ToString(row["WEBSITE"]) : string.Empty;
                        mngtCompanies.CType = row["C_TYPE"] != DBNull.Value ? Convert.ToString(row["C_TYPE"]) : string.Empty;
                        mngtCompanies.TotalTurnover = row["TOTAL_TURNOVER"] != DBNull.Value ? Convert.ToString(row["TOTAL_TURNOVER"]) : string.Empty;
                        mngtCompanies.CompanyPhone = row["C_PHONE"] != DBNull.Value ? Convert.ToString(row["C_PHONE"]) : string.Empty;
                        mngtCompanies.Industry1 = row["INDUSTRY_1"] != DBNull.Value ? Convert.ToString(row["INDUSTRY_1"]) : string.Empty;
                        mngtCompanies.SubIndustry1 = row["SUB_INDUSTRY_1"] != DBNull.Value ? Convert.ToString(row["SUB_INDUSTRY_1"]) : string.Empty;
                        mngtCompanies.Industry2 = row["INDUSTRY_2"] != DBNull.Value ? Convert.ToString(row["INDUSTRY_2"]) : string.Empty;
                        mngtCompanies.SubIndustry2 = row["SUB_INDUSTRY_2"] != DBNull.Value ? Convert.ToString(row["SUB_INDUSTRY_2"]) : string.Empty;
                        mngtCompanies.Industry3 = row["INDUSTRY_3"] != DBNull.Value ? Convert.ToString(row["INDUSTRY_3"]) : string.Empty;
                        mngtCompanies.SubIndustry3 = row["SUB_INDUSTRY_3"] != DBNull.Value ? Convert.ToString(row["SUB_INDUSTRY_3"]) : string.Empty;
                        mngtCompanies.Sector = row["SECTOR"] != DBNull.Value ? Convert.ToString(row["SECTOR"]) : string.Empty;
                        mngtCompanies.LevelOfOffice = row["LEVEL_OF_OFFICE"] != DBNull.Value ? Convert.ToString(row["LEVEL_OF_OFFICE"]) : string.Empty;
                        mngtCompanies.CompanyEntity = row["COMPANY_ENTITY"] != DBNull.Value ? Convert.ToString(row["COMPANY_ENTITY"]) : string.Empty;
                        mngtCompanies.CompanyType = row["COMPANY_TYPE"] != DBNull.Value ? Convert.ToString(row["COMPANY_TYPE"]) : string.Empty;
                        mngtCompanies.NoOfEmployees = row["NO_OF_EMP"] != DBNull.Value ? Convert.ToString(row["NO_OF_EMP"]) : string.Empty;
                        mngtCompanies.AddLine1 = row["ADDRESS_LINE_1"] != DBNull.Value ? Convert.ToString(row["ADDRESS_LINE_1"]) : string.Empty;
                        mngtCompanies.AddLine2 = row["ADDRESS_LINE_2"] != DBNull.Value ? Convert.ToString(row["ADDRESS_LINE_2"]) : string.Empty;
                        mngtCompanies.City = row["CITY"] != DBNull.Value ? Convert.ToString(row["CITY"]) : string.Empty;
                        mngtCompanies.State = row["STATE"] != DBNull.Value ? Convert.ToString(row["STATE"]) : string.Empty;
                        mngtCompanies.PinCode = row["PINCODE"] != DBNull.Value ? Convert.ToString(row["PINCODE"]) : string.Empty;
                        mngtCompanies.StdCode = row["STDCODE"] != DBNull.Value ? Convert.ToString(row["STDCODE"]) : string.Empty;
                        mngtCompanies.CompAssTo = row["ASSIGNED_TO"] != DBNull.Value ? Convert.ToInt32(row["ASSIGNED_TO"]) : 0;
                        mngtCompanies.CompAssBy = row["ASSIGNED_BY"] != DBNull.Value ? Convert.ToInt32(row["ASSIGNED_BY"]) : 0;
                        mngtCompanies.InsideSalesComments = row["INSIDE_SALES_COMMENTS"] != DBNull.Value ? Convert.ToString(row["INSIDE_SALES_COMMENTS"]) : string.Empty;
                        mngtCompanies.ResearchTeamComments = row["RESEARCH_TEAM_COMMENTS"] != DBNull.Value ? Convert.ToString(row["RESEARCH_TEAM_COMMENTS"]) : string.Empty;
                        mngtCompanies.MarketingTeamComments = row["MARKETING_TEAM_COMMENTS"] != DBNull.Value ? Convert.ToString(row["MARKETING_TEAM_COMMENTS"]) : string.Empty;
                        mngtCompanies.GeneralComments = row["GENERAL_COMMENTS"] != DBNull.Value ? Convert.ToString(row["GENERAL_COMMENTS"]) : string.Empty;
                        mngtCompanies.WorkingHours = row["WORKING_HOURS"] != DBNull.Value ? Convert.ToString(row["WORKING_HOURS"]) : string.Empty;
                        mngtCompanies.TouchpointOutCome = row["TP_OUTCOME"] != DBNull.Value ? Convert.ToString(row["TP_OUTCOME"]) : string.Empty;
                        mngtCompanies.TouchpointComments = row["TP_COMMENTS"] != DBNull.Value ? Convert.ToString(row["TP_COMMENTS"]) : string.Empty;
                        mngtCompanies.TouchpointDateModified = row["TP_DATE_MODIFIED"] != DBNull.Value ? Convert.ToDateTime(row["TP_DATE_MODIFIED"]) : DateTime.MaxValue;
                        mngtCompanies.TouchpointMeetingDate = row["TP_DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["TP_DATE_TIME"]) : DateTime.MaxValue;
                        mngtCompanies.TouchpointContactName = row["TP_CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["TP_CONTACT_NAME"]) : string.Empty;
                        mngtCompanies.TouchpointContactDesg = row["TP_CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["TP_CONTACT_DESIGNATION"]) : string.Empty;
                        mngtCompanies.TouchpointContactInfo = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                        mngtCompanies.Priority = row["PRIORITY"] != DBNull.Value ? Convert.ToString(row["PRIORITY"]) : string.Empty;
                        ListMngtCompanies.Add(mngtCompanies);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtCompanies mngtCompanies = new MngtCompanies();
                mngtCompanies.ErrorMessage = ex.Message;
            }

            return ListMngtCompanies;
        }

        public List<MngtUserDetails> GetCompanyContacts(string sessionID, int companyID)
        {
            Utilities.ValidateSession(sessionID, null);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<MngtUserDetails> ListMngtUserDetails = new List<MngtUserDetails>();
            try
            {
                sd.Add("P_COMP_ID", companyID);
                DataSet ds = sqlHelper.SelectList("cp_mngt_GetCompanyContacts", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        MngtUserDetails mngtUserDetails = new MngtUserDetails();
                        mngtUserDetails.CompanyContactID = row["CC"] != DBNull.Value ? Convert.ToInt32(row["CC"]) : 0;
                        mngtUserDetails.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                        mngtUserDetails.FirstName = row["C_NAME"] != DBNull.Value ? Convert.ToString(row["C_NAME"]) : string.Empty;
                        mngtUserDetails.PhoneNumber = row["C_PHONE"] != DBNull.Value ? Convert.ToString(row["C_PHONE"]) : string.Empty;
                        mngtUserDetails.EmailId = row["C_EMAIL"] != DBNull.Value ? Convert.ToString(row["C_EMAIL"]) : string.Empty;
                        mngtUserDetails.ContactDesignation = row["C_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["C_DESIGNATION"]) : string.Empty;
                        mngtUserDetails.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        mngtUserDetails.CompanyPhone = row["C_PHONE"] != DBNull.Value ? Convert.ToString(row["C_PHONE"]) : string.Empty;
                        mngtUserDetails.CompanyTurnover = row["TOTAL_TURNOVER"] != DBNull.Value ? Convert.ToString(row["TOTAL_TURNOVER"]) : string.Empty;
                        mngtUserDetails.CompanyIndustry = row["INDUSTRY_1"] != DBNull.Value ? Convert.ToString(row["INDUSTRY_1"]) : string.Empty;
                        ListMngtUserDetails.Add(mngtUserDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                MngtCompanies mngtCompanies = new MngtCompanies();
                mngtCompanies.ErrorMessage = ex.Message;
            }

            return ListMngtUserDetails;
        }

        public List<CompanyNetworks> GetCompanyNetworks(string sessionID, int userID, int compID, string linkType)
        {
            List<CompanyNetworks> listCompanyNetworks = new List<CompanyNetworks>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_mngt_GetCompanyNetworks";
                        cmd.CommandType = CommandType.StoredProcedure;
                        //cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", compID));
                        cmd.Parameters.Add(new MySqlParameter("P_LINK_TYPE", linkType));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                CompanyNetworks companyNetworks = new CompanyNetworks();

                                companyNetworks.CompNetID = row["CN_ID"] != DBNull.Value ? Convert.ToInt32(row["CN_ID"]) : 0;
                                companyNetworks.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                companyNetworks.Link = row["LINK"] != DBNull.Value ? Convert.ToString(row["LINK"]) : string.Empty;
                                companyNetworks.LinkType = row["LINK_TYPE"] != DBNull.Value ? Convert.ToString(row["LINK_TYPE"]) : string.Empty;
                                companyNetworks.UserName = row["USER_NAME"] != DBNull.Value ? Convert.ToString(row["USER_NAME"]) : string.Empty;

                                listCompanyNetworks.Add(companyNetworks);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        CompanyNetworks companyNetworks = new CompanyNetworks();
                        companyNetworks.ErrorMessage = ex.Message;
                        listCompanyNetworks.Add(companyNetworks);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return listCompanyNetworks;
        }        

        public List<MngtTouchpoint> GetTouchpoints(string sessionID, int uID, int companyID)
        {
            List<MngtTouchpoint> ListMngtTouchpoint = new List<MngtTouchpoint>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_mngt_GetTouchPoints";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", uID));
                        cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", companyID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                MngtTouchpoint mngtTouchpoint = new MngtTouchpoint();

                                mngtTouchpoint.TouchpoinID = row["TP_ID"] != DBNull.Value ? Convert.ToInt32(row["TP_ID"]) : 0;
                                mngtTouchpoint.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                mngtTouchpoint.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                mngtTouchpoint.TouchpoinType = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                                mngtTouchpoint.ContactName = row["CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["CONTACT_NAME"]) : string.Empty;
                                mngtTouchpoint.ContactDesignation = row["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["CONTACT_DESIGNATION"]) : string.Empty;
                                mngtTouchpoint.TouchpointDate = row["DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["DATE_TIME"]) : DateTime.MinValue;
                                mngtTouchpoint.SpentTime = row["SPENT_TIME"] != DBNull.Value ? Convert.ToString(row["SPENT_TIME"]) : string.Empty;
                                mngtTouchpoint.OutCome = row["OUTCOME"] != DBNull.Value ? Convert.ToString(row["OUTCOME"]) : string.Empty;
                                mngtTouchpoint.Comments = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;

                                mngtTouchpoint.meetingLocation = row["MEETING_LOCATION"] != DBNull.Value ? Convert.ToString(row["MEETING_LOCATION"]) : string.Empty;
                                mngtTouchpoint.marketingUser = row["MARKETING_ASSIGN_TO"] != DBNull.Value ? Convert.ToInt32(row["MARKETING_ASSIGN_TO"]) : 0;
                                mngtTouchpoint.marketingUserName = row["M_ASS_TO"] != DBNull.Value ? Convert.ToString(row["M_ASS_TO"]) : string.Empty;


                                ListMngtTouchpoint.Add(mngtTouchpoint);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MngtTouchpoint mngtTouchpoint = new MngtTouchpoint();
                        mngtTouchpoint.ErrorMessage = ex.Message;
                        ListMngtTouchpoint.Add(mngtTouchpoint);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return ListMngtTouchpoint;
        }

        public List<MngtTouchpoint> GetInsideSalesActions(string sessionID, int userID, string actionType, DateTime fromDate, DateTime toDate)
        {
            List<MngtTouchpoint> listMeetings = new List<MngtTouchpoint>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_mngt_GetInsideSalesActions";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_ACTION_TYPE", actionType));
                        cmd.Parameters.Add(new MySqlParameter("P_FROM_DATE", fromDate));
                        cmd.Parameters.Add(new MySqlParameter("P_TO_DATE", toDate));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                MngtTouchpoint meetings = new MngtTouchpoint();

                                meetings.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                meetings.CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty;
                                meetings.TouchpointDate = row["DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["DATE_TIME"]) : DateTime.MaxValue;
                                meetings.ContactName = row["CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["CONTACT_NAME"]) : string.Empty;
                                meetings.ContactDesignation = row["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["CONTACT_DESIGNATION"]) : string.Empty;
                                meetings.ContactPhoneEmail = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                                meetings.TouchpoinType = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                                meetings.OutCome = row["OUTCOME"] != DBNull.Value ? Convert.ToString(row["OUTCOME"]) : string.Empty;
                                meetings.Comments = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                                meetings.meetingLocation = row["MEETING_LOCATION"] != DBNull.Value ? Convert.ToString(row["MEETING_LOCATION"]) : string.Empty;
                                meetings.marketingUserName = row["MARKETING_ASSIGN_TO"] != DBNull.Value ? Convert.ToString(row["MARKETING_ASSIGN_TO"]) : string.Empty;
                                meetings.CompanyWebsite = row["WEBSITE"] != DBNull.Value ? Convert.ToString(row["WEBSITE"]) : string.Empty;
                                
                                listMeetings.Add(meetings);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MngtTouchpoint meetings = new MngtTouchpoint();
                        meetings.ErrorMessage = ex.Message;
                        listMeetings.Add(meetings);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return listMeetings;
        }

        public InsidesalesDashboard InsideSalesDashboard(string sessionID, int userID, string userType)
        {
            InsidesalesDashboard insidesalesdashboard = new InsidesalesDashboard();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_mngt_InsideSalesDashboard";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_USER_TYPE", userType));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {

                            DataRow row = ds.Tables[0].Rows[0];

                            insidesalesdashboard.TodaySuccessRation = row["TODAY_SUCCESS_RATIO"] != DBNull.Value ? Convert.ToDouble(row["TODAY_SUCCESS_RATIO"]) : 0;
                            insidesalesdashboard.OverallSuccessRation = row["OVERALL_SUCCESS_RATIO"] != DBNull.Value ? Convert.ToDouble(row["OVERALL_SUCCESS_RATIO"]) : 0;

                            insidesalesdashboard.FollowUp = row["PARAM_FOLLOW_UP"] != DBNull.Value ? Convert.ToInt32(row["PARAM_FOLLOW_UP"]) : 0;
                            insidesalesdashboard.MeetingSet = row["PARAM_MEETING_SET"] != DBNull.Value ? Convert.ToInt32(row["PARAM_MEETING_SET"]) : 0;
                            insidesalesdashboard.FollowUpMeeting = row["PARAM_FOLLOW_UP_MEETING"] != DBNull.Value ? Convert.ToInt32(row["PARAM_FOLLOW_UP_MEETING"]) : 0;
                            insidesalesdashboard.Positive = row["PARAM_POSITIVE"] != DBNull.Value ? Convert.ToInt32(row["PARAM_POSITIVE"]) : 0;
                            insidesalesdashboard.RejectedMeeting = row["PARAM_REJECTED_MEETING"] != DBNull.Value ? Convert.ToInt32(row["PARAM_REJECTED_MEETING"]) : 0;
                            insidesalesdashboard.Closure = row["PARAM_CLOSURE"] != DBNull.Value ? Convert.ToInt32(row["PARAM_CLOSURE"]) : 0;
                            insidesalesdashboard.FollowUpClosure = row["PARAM_FOLLOW_UP_CLOSURE"] != DBNull.Value ? Convert.ToInt32(row["PARAM_FOLLOW_UP_CLOSURE"]) : 0;
                            insidesalesdashboard.RejectedClosure = row["PARAM_REJECTED_CLOSURE"] != DBNull.Value ? Convert.ToInt32(row["PARAM_REJECTED_CLOSURE"]) : 0;
                            insidesalesdashboard.Trail = row["PARAM_TRAIL"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TRAIL"]) : 0;
                            insidesalesdashboard.FinalStage = row["PARAM_FINAL_STAGE"] != DBNull.Value ? Convert.ToInt32(row["PARAM_FINAL_STAGE"]) : 0;
                             
                        }

                        List<MngtTouchpoint> listMeetings = new List<MngtTouchpoint>();
                    }
                    catch (Exception ex)
                    {
                        insidesalesdashboard.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return insidesalesdashboard;
        }

        public List<MngtUserDetails> InsideSalesDailyReport(string sessionID, int userID, DateTime fromDate, DateTime toDate)
        {
            List<MngtUserDetails> listUserStatus = new List<MngtUserDetails>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_mngt_InsideSalesDailyReport";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_FROM_DATE", fromDate));
                        cmd.Parameters.Add(new MySqlParameter("P_TO_DATE", toDate));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0)
                        {

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                List<MngtCompanies> mngtcompanylist = new List<MngtCompanies>();
                                List<MngtCompanies> followupcompanylist = new List<MngtCompanies>();
                                int uID = 0;
                                MngtUserDetails userStatus = new MngtUserDetails();

                                userStatus.UserId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                userStatus.FirstName = row["U_NAME"] != DBNull.Value ? Convert.ToString(row["U_NAME"]) : string.Empty;
                                userStatus.CallsMadeToday = row["CALLS_MADE_TODAY"] != DBNull.Value ? Convert.ToInt32(row["CALLS_MADE_TODAY"]) : 0;
                                userStatus.MeetingsSetToday = row["MEETINGS_SET_TODAY"] != DBNull.Value ? Convert.ToInt32(row["MEETINGS_SET_TODAY"]) : 0;

                                foreach (DataRow row1 in ds.Tables[1].Rows)
                                {
                                    MngtCompanies mngtcompanies = new MngtCompanies();

                                    uID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;

                                    if (userStatus.UserId == uID)
                                    {
                                        mngtcompanies.UserID = uID;
                                        mngtcompanies.TouchpointId = row1["TP_ID"] != DBNull.Value ? Convert.ToInt32(row1["TP_ID"]) : 0;
                                        mngtcompanies.CompID = row1["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row1["COMP_ID"]) : 0;
                                        mngtcompanies.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                                        mngtcompanylist.Add(mngtcompanies);

                                    }
                                    userStatus.MtngSetCompanyList = mngtcompanylist;

                                }


                                foreach (DataRow row2 in ds.Tables[2].Rows)
                                {
                                    MngtCompanies mngtcompanies = new MngtCompanies();

                                    uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;

                                    if (userStatus.UserId == uID)
                                    {
                                        mngtcompanies.UserID = uID;
                                        mngtcompanies.TouchpointId = row2["TP_ID"] != DBNull.Value ? Convert.ToInt32(row2["TP_ID"]) : 0;
                                        mngtcompanies.CompID = row2["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row2["COMP_ID"]) : 0;
                                        mngtcompanies.CompanyName = row2["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row2["COMPANY_NAME"]) : string.Empty;

                                        followupcompanylist.Add(mngtcompanies);

                                    }
                                    userStatus.FollowUpCompanyList = followupcompanylist;
                                }


                                listUserStatus.Add(userStatus);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MngtUserDetails muserdetails = new MngtUserDetails();
                        muserdetails.ErrorMessage = ex.Message;
                        listUserStatus.Add(muserdetails);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return listUserStatus;
        }

        public List<MngtTouchpoint> GetMarketingActions(string sessionID, int userID, string actionType, DateTime fromDate, DateTime toDate)
        {
            List<MngtTouchpoint> listMeetings = new List<MngtTouchpoint>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_mngt_GetMarketingActions";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_ACTION_TYPE", actionType));
                        cmd.Parameters.Add(new MySqlParameter("P_FROM_DATE", fromDate));
                        cmd.Parameters.Add(new MySqlParameter("P_TO_DATE", toDate));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                MngtTouchpoint meetings = new MngtTouchpoint();

                                meetings.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                meetings.CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty;
                                meetings.TouchpointDate = row["DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["DATE_TIME"]) : DateTime.MaxValue;
                                meetings.ContactName = row["CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["CONTACT_NAME"]) : string.Empty;
                                meetings.ContactDesignation = row["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["CONTACT_DESIGNATION"]) : string.Empty;
                                meetings.ContactPhoneEmail = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                                meetings.TouchpoinType = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                                meetings.OutCome = row["OUTCOME"] != DBNull.Value ? Convert.ToString(row["OUTCOME"]) : string.Empty;
                                meetings.Comments = row["COMMENTS"] != DBNull.Value ? Convert.ToString(row["COMMENTS"]) : string.Empty;
                                meetings.meetingLocation = row["MEETING_LOCATION"] != DBNull.Value ? Convert.ToString(row["MEETING_LOCATION"]) : string.Empty;
                                meetings.marketingUserName = row["MARKETING_ASSIGN_TO"] != DBNull.Value ? Convert.ToString(row["MARKETING_ASSIGN_TO"]) : string.Empty;
                                meetings.CompanyWebsite = row["WEBSITE"] != DBNull.Value ? Convert.ToString(row["WEBSITE"]) : string.Empty;

                                listMeetings.Add(meetings);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MngtTouchpoint meetings = new MngtTouchpoint();
                        meetings.ErrorMessage = ex.Message;
                        listMeetings.Add(meetings);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return listMeetings;
        }

        public List<MngtUserDetails> MarketingDailyReport(string sessionID, int userID, DateTime fromDate, DateTime toDate)
        {
            List<MngtUserDetails> listUserStatus = new List<MngtUserDetails>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);
                        cmd.CommandText = "cp_mngt_MarketingDailyReport";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_FROM_DATE", fromDate));
                        cmd.Parameters.Add(new MySqlParameter("P_TO_DATE", toDate));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0)
                        {

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {                                
                                MngtUserDetails userStatus = new MngtUserDetails();

                                userStatus.UserId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                                userStatus.FirstName = row["U_NAME"] != DBNull.Value ? Convert.ToString(row["U_NAME"]) : string.Empty;

                                userStatus.MeetingsCount = row["MEETING_SET"] != DBNull.Value ? Convert.ToInt32(row["MEETING_SET"]) : 0;
                                userStatus.Positives = row["POSITIVES"] != DBNull.Value ? Convert.ToInt32(row["POSITIVES"]) : 0;
                                userStatus.FollowUps = row["FOLLOW_UP_MEETINGS"] != DBNull.Value ? Convert.ToInt32(row["FOLLOW_UP_MEETINGS"]) : 0;
                                userStatus.Trail = row["TRAIL"] != DBNull.Value ? Convert.ToInt32(row["TRAIL"]) : 0;
                                userStatus.Closure = row["CLOSURE"] != DBNull.Value ? Convert.ToInt32(row["CLOSURE"]) : 0;
                                
                                listUserStatus.Add(userStatus);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MngtUserDetails muserdetails = new MngtUserDetails();
                        muserdetails.ErrorMessage = ex.Message;
                        listUserStatus.Add(muserdetails);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return listUserStatus;
        }

        public List<MngtMeetingsReminders> MngtMeetingsReminders()
        {
            List<MngtMeetingsReminders> ListMngtMeetingsReminders = new List<MngtMeetingsReminders>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_mngt_GetMeetingsReminders";
                        cmd.CommandType = CommandType.StoredProcedure;

                        //cmd.Parameters.Add(new MySqlParameter("P_U_ID", uID));
                        //cmd.Parameters.Add(new MySqlParameter("P_COMP_ID", companyID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                MngtMeetingsReminders mngtMeetingsReminders = new MngtMeetingsReminders();



                                //MC., MC., MC., MTP., MTP., 
                                //MTP., MTP.,
                                //MTP., MC., MC., 'MEETINGS_REMINDERS' AS 

                                mngtMeetingsReminders.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;

                                mngtMeetingsReminders.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                                mngtMeetingsReminders.TotalTurnover = row["TOTAL_TURNOVER"] != DBNull.Value ? Convert.ToString(row["TOTAL_TURNOVER"]) : string.Empty;
                                mngtMeetingsReminders.ContactName = row["CONTACT_NAME"] != DBNull.Value ? Convert.ToString(row["CONTACT_NAME"]) : string.Empty;
                                mngtMeetingsReminders.ContactDesignation = row["CONTACT_DESIGNATION"] != DBNull.Value ? Convert.ToString(row["CONTACT_DESIGNATION"]) : string.Empty;
                                mngtMeetingsReminders.MobileAndEmail = row["TP_TYPE"] != DBNull.Value ? Convert.ToString(row["TP_TYPE"]) : string.Empty;
                                mngtMeetingsReminders.MeetingLocation = row["MEETING_LOCATION"] != DBNull.Value ? Convert.ToString(row["MEETING_LOCATION"]) : string.Empty;
                                mngtMeetingsReminders.MeetingDate = row["DATE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["DATE_TIME"]) : DateTime.MaxValue;
                                mngtMeetingsReminders.MarketingAssTo = row["MARKETING_ASSIGN_TO"] != DBNull.Value ? Convert.ToString(row["MARKETING_ASSIGN_TO"]) : string.Empty;
                                mngtMeetingsReminders.InsidesalesAssTo = row["ASSIGNED_TO"] != DBNull.Value ? Convert.ToString(row["ASSIGNED_TO"]) : string.Empty;
                                mngtMeetingsReminders.Job = row["JOB"] != DBNull.Value ? Convert.ToString(row["JOB"]) : string.Empty;

                                ListMngtMeetingsReminders.Add(mngtMeetingsReminders);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MngtMeetingsReminders mngtMeetingsReminders = new MngtMeetingsReminders();
                        mngtMeetingsReminders.ErrorMessage = ex.Message;
                        ListMngtMeetingsReminders.Add(mngtMeetingsReminders);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return ListMngtMeetingsReminders;
        }

        #endregion gets

        #endregion InsideSales

        #endregion Public Methods

        #region Private Methods

        private async Task SendPushNotificationAsync(PushNotifications push, UserInfo user)
        {
            if (user.PhoneOS.ToUpper().Contains("ANDROID"))
            {
                var config = new GcmConfiguration(ConfigurationManager.AppSettings["ANDROID_SENDER_ID"], ConfigurationManager.AppSettings["ANDROID_AUTH_TOKEN"], null);
                var gcmBroker = new GcmServiceBroker(config);
                gcmBroker.OnNotificationFailed += (notification, aggregateEx) =>
                {

                };

                gcmBroker.OnNotificationSucceeded += (notification) =>
                {
                    //Console.WriteLine("GCM Notification Sent!");
                };

                gcmBroker.Start();

                List<string> MY_REGISTRATION_IDS = new List<string>();
                MY_REGISTRATION_IDS.Add(user.PhoneID);

                string screen = "PRM360 Notification";
                string requirementId = push.RequirementID.ToString();
                string userId = user.UserID;
                string message = push.Message;

                foreach (var regId in MY_REGISTRATION_IDS)
                {
                    gcmBroker.QueueNotification(new GcmNotification
                    {
                        RegistrationIds = new List<string> { regId },
                        Data = JObject.Parse("{ \"screen_name\":\"" + screen + "\", \"message\" : \"" + message + "\",\"requirementId\":\"" + requirementId + "\",\"userId\":\"" + userId + "\" }")
                    });
                }

                gcmBroker.Stop();
            }
            else
            {

            }
        }

        private void SaveFile(string fileName, byte[] fileContent)
        {
            //PTMGenericServicesClient ptmClient = new PTMGenericServicesClient();
            //ptmClient.SaveFileBytes(fileContent, fileName);
            //File.WriteAllBytes(fileName, attachment);
        }

        private Response SaveAttachment(string path, MySqlCommand cmd = null)
        {
            Response response = new Response();
            try
            {

                if (cmd == null)
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = new MySqlConnection(GetConnectionString());
                }
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                cmd.Parameters.Clear();
                cmd.CommandText = "cp_SaveAttachment";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_PATH", path));
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
                cmd.Parameters.Clear();
            }
            return response;
        }

        private string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        private string GetMailHost()
        {
            return ConfigurationManager.AppSettings["MAILHOST"].ToString();
        }

        private string GetFromAddress()
        {
            return ConfigurationManager.AppSettings["FROMADDRESS"].ToString();
        }

        private string GetFromDisplayName()
        {
            return ConfigurationManager.AppSettings["FROMDISPLAYNAME"].ToString();
        }

        private int ValidateSession(string sessionId, MySqlCommand cmd)
        {
            if (string.IsNullOrEmpty(sessionId))
            {
                throw new Exception("Session ID is null");
            }
            cmd.Parameters.Clear();
            int isValid = 1;
            try
            {
                cmd.CommandText = "cp_ValidateSession";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_SESSION_ID", sessionId));
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);

                //cmd.Connection.Close();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                isValid = 0;
            }

            if (isValid <= 0)
            {
                throw new Exception("Invalid Session ID passed");
            }

            return isValid;
        }

        private async Task SendEmail(string To, string Subject, string Body)
        {

            try
            {

                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;

                //SmtpClient smtpClient = new SmtpClient();
                //smtpClient.Host = GetMailHost();
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpClient.EnableSsl = false;
                //smtpClient.UseDefaultCredentials = false;
                //NetworkCredential basicCredential = new NetworkCredential("no-reply@prm360.com", "N@r$y76");
                //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("azure_6f1885de2190df71277e1d1068d38b1b@azure.com", "Acads@123");

                //smtpClient.Host = GetMailHost();
                //smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpClient.EnableSsl = false;
                //smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = basicCredential;

                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(GetFromAddress(), GetFromDisplayName());
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    mail.To.Add(new MailAddress(address));
                }
                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;

                //smtpClient.Send(mail);
                await smtpClient.SendMailAsync(mail);

                Response response = new Response();
                response.ObjectID = 1;
                //return response;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
                //return response;
            }
        }

        private async Task<string> SendSMS(string from, string to, string message)
        {
            try
            {
                if (string.IsNullOrEmpty(from))
                {
                    from = ConfigurationManager.AppSettings["SMSFROM"].ToString();
                }

                message = message.Replace("&", "and").Replace("<br/>", Environment.NewLine);

                string strUrl = string.Format(ConfigurationManager.AppSettings["SMSURL"].ToString(), from, to, message);

                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    //client.BaseAddress = new Uri(strUrl).;
                    HttpResponseMessage httpResponse = client.GetAsync(strUrl).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    string result = httpResponse.Content.ReadAsStringAsync().Result;
                }

                return "";
            }
            catch
            {
                return "";
            }
        }

        private string GenerateEmailBody(string TemplateName)
        {
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/EmailFormats.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;
            string footerName = "";
            if (TemplateName.ToLower().Contains("email"))
            {
                footerName = "EmailFooter";



            }
            else
            {
                footerName = "SMSFooter";
            }
            XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
            body += footernode.InnerText;
            return body;
        }

        private string GetEnumDesc<T>(string value) where T : struct, IConvertible
        {
            var type = typeof(T);
            var memInfo = type.GetMember(value);
            var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            var description = ((DescriptionAttribute)attributes[0]).Description;
            return description;
        }

        private MngtUserDetails SetFeildsToMngtUserDetails(DataRow row, string ServiceName)
        {
            MngtUserDetails MngtUserDetails = new MngtUserDetails();
            try
            {
                if (ServiceName == "GetUsersList" || ServiceName == "GetUserDetails")
                {
                    MngtUserDetails.UserId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    MngtUserDetails.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    MngtUserDetails.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    MngtUserDetails.PhoneNumber = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    MngtUserDetails.EmailId = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    MngtUserDetails.CreatedDate = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                    MngtUserDetails.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    MngtUserDetails.IsSuperUser = row["IS_SUPER_USER"] != DBNull.Value ? (Convert.ToInt32(row["IS_SUPER_USER"]) == 1 ? true : false) : false;
                    MngtUserDetails.SuperUserId = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : 0;
                    MngtUserDetails.IsMobileVerified = row["MOBILE_VERIFIED"] != DBNull.Value ? (Convert.ToInt32(row["MOBILE_VERIFIED"]) > 0 ? true : false) : false;
                    MngtUserDetails.IsEmailVerified = row["EMAIL_VERIFIED"] != DBNull.Value ? (Convert.ToInt32(row["EMAIL_VERIFIED"]) > 0 ? true : false) : false;
                    MngtUserDetails.IsDocsVerified = row["DOCS_VERIFIED"] != DBNull.Value ? (Convert.ToInt32(row["DOCS_VERIFIED"]) > 1 ? true : false) : false;
                }
                if (ServiceName == "GetUsersList")
                {

                }
                if (ServiceName == "GetUserDetails")
                {

                }
            }
            catch (Exception ex)
            {
                MngtUserDetails.ErrorMessage = ex.Message;
            }
            finally
            {

            }
            return MngtUserDetails;
        }

        private MngtRequirementDetails SetFeildsToMngtRequirementDetails(DataRow row, string ServiceName)
        {
            MngtRequirementDetails MngtRequirementDetails = new MngtRequirementDetails();
            try
            {
                if (ServiceName == "GetRequirementsList" || ServiceName == "GetRequirementDetails" || ServiceName == "GetMngtRequirements")
                {
                    MngtRequirementDetails.UserId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    MngtRequirementDetails.ReqTitle = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    MngtRequirementDetails.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToDouble(row["REQ_BUDGET"]) : 0;
                }
                if (ServiceName == "GetRequirementsList")
                {
                    MngtRequirementDetails.BeforeNegotiationSavings = row["SAVINGS_BEFORE_NEGOTIATION"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS_BEFORE_NEGOTIATION"]) : 0;
                    MngtRequirementDetails.OnNegotiationSavings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    MngtRequirementDetails.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    MngtRequirementDetails.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    MngtRequirementDetails.CreatedDate = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    MngtRequirementDetails.ReqId = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                }
                if (ServiceName == "GetRequirementDetails")
                {
                    MngtRequirementDetails.BeforeNegotiationSavings = row["SAVINGS_BEFORE_NEGOTIATION"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS_BEFORE_NEGOTIATION"]) : 0;
                    MngtRequirementDetails.OnNegotiationSavings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    MngtRequirementDetails.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    MngtRequirementDetails.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    MngtRequirementDetails.CreatedDate = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    MngtRequirementDetails.ReqId = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    MngtRequirementDetails.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    MngtRequirementDetails.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    MngtRequirementDetails.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    MngtRequirementDetails.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    MngtRequirementDetails.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;
                    MngtRequirementDetails.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToInt16(row["MIN_REDUCE_AMOUNT"]) : 0;
                    if (MngtRequirementDetails.MinReduceAmount > 0)
                    { MngtRequirementDetails.MinBidAmount = MngtRequirementDetails.MinReduceAmount; }
                    else
                    { MngtRequirementDetails.MinBidAmount = Math.Round(Convert.ToDouble(MngtRequirementDetails.Budget) / 100, 2); }
                }
                if (ServiceName == "GetMngtRequirements")
                {
                    MngtRequirementDetails.MngtRequirementId = row["MREQ_ID"] != DBNull.Value ? Convert.ToInt32(row["MREQ_ID"]) : 0;
                    MngtRequirementDetails.Customer = row["CUSTOMER"] != DBNull.Value ? Convert.ToString(row["CUSTOMER"]) : string.Empty;
                    MngtRequirementDetails.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                    MngtRequirementDetails.DeliveryLocation = row["REQ_DELIVERY_LOCATION"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOCATION"]) : string.Empty;
                    MngtRequirementDetails.QuotationFreezTime = row["REQ_QUOTATION_FREEZE_TIME"] != DBNull.Value ? Convert.ToDateTime(row["REQ_QUOTATION_FREEZE_TIME"]) : DateTime.MaxValue;
                    MngtRequirementDetails.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    MngtRequirementDetails.Vendors = row["REQ_VENDORS"] != DBNull.Value ? Convert.ToString(row["REQ_VENDORS"]) : string.Empty;
                    MngtRequirementDetails.Notes = row["REQ_NOTES"] != DBNull.Value ? Convert.ToString(row["REQ_NOTES"]) : string.Empty;
                    MngtRequirementDetails.CreatedDate = row["DATE_CREATED"] != DBNull.Value ? Convert.ToDateTime(row["DATE_CREATED"]) : DateTime.MaxValue;
                }
                if (ServiceName == "GetRequirementDetails" || ServiceName == "GetMngtRequirements")
                {
                    MngtRequirementDetails.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    MngtRequirementDetails.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    MngtRequirementDetails.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    MngtRequirementDetails.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt16(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    MngtRequirementDetails.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt16(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    MngtRequirementDetails.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    MngtRequirementDetails.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                }


            }
            catch (Exception ex)
            {
                MngtRequirementDetails.ErrorMessage = ex.Message;
            }
            finally
            {

            }
            return MngtRequirementDetails;
        }

        #endregion Private Methods

    }
};