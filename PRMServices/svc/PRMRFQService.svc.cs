﻿using System;
using System.Data;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Common;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp.Pdf;
using System.Net.Http;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMRFQService : IPRMRFQService
    {
        PRMServices prm = new PRMServices();
        PRMCijIndentService cijIndent = new PRMCijIndentService();

        #region Get

        public List<stringCIJ> GetRFQCIJList(int userID, string sessionID)
        {
            List<stringCIJ> listStstringCIJ = new List<stringCIJ>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = ValidateSession(sessionID, cmd);

                        cmd.CommandText = "wf_GetRFQCIJList";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                stringCIJ stringcij = new stringCIJ();

                                stringcij.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                                stringcij.CijCode = row["CIJ_CODE"] != DBNull.Value ? Convert.ToString(row["CIJ_CODE"]) : string.Empty;
                                stringcij.Cij = row["CIJ_STRING"] != DBNull.Value ? Convert.ToString(row["CIJ_STRING"]) : string.Empty;
                                stringcij.CijType = row["CIJ_TYPE"] != DBNull.Value ? Convert.ToString(row["CIJ_TYPE"]) : string.Empty;
                                stringcij.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                stringcij.Status = row["CURRENT_STATUS"] != DBNull.Value ? Convert.ToString(row["CURRENT_STATUS"]) : "PENDING";
                                stringcij.RequestedBy = row["REQUESTOR_NAME"] != DBNull.Value ? Convert.ToString(row["REQUESTOR_NAME"]) : "";

                                listStstringCIJ.Add(stringcij);
                            }

                            listStstringCIJ = listStstringCIJ.OrderByDescending(v => v.CijID).ToList();

                        }
                    }
                    catch (Exception ex)
                    {
                        stringCIJ stringcij = new stringCIJ();
                        stringcij.ErrorMessage = ex.Message;
                        listStstringCIJ.Add(stringcij);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listStstringCIJ;
        }

        public List<MPIndent> GetRFQIndentList(int userID, string sessionID)
        {
            List<MPIndent> listMPIndent = new List<MPIndent>();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        int isValidSession = ValidateSession(sessionID, cmd);

                        cmd.CommandText = "wf_GetRFQIndentList";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                MPIndent mpindent = new MPIndent();

                                mpindent.IndID = row["I_ID"] != DBNull.Value ? Convert.ToInt32(row["I_ID"]) : 0;
                                mpindent.Status = row["CURRENT_STATUS"] != DBNull.Value ? Convert.ToString(row["CURRENT_STATUS"]) : string.Empty;

                                mpindent.CIJ = new stringCIJ();

                                mpindent.CIJ.CijID = row["CIJ_ID"] != DBNull.Value ? Convert.ToInt32(row["CIJ_ID"]) : 0;
                                mpindent.CIJ.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;
                                mpindent.IndNo = row["INDENT_CODE"] != DBNull.Value ? Convert.ToString(row["INDENT_CODE"]) : string.Empty;
                                mpindent.Department = row["DEPT"] != DBNull.Value ? Convert.ToString(row["DEPT"]) : string.Empty;
                                mpindent.RequestDate = row["CREATED_DATE"] != DBNull.Value ? Convert.ToString(row["CREATED_DATE"]) : string.Empty;
                                mpindent.RequestedBy = row["REQUESTED_BY"] != DBNull.Value ? Convert.ToString(row["REQUESTED_BY"]) : string.Empty;
                                mpindent.ApprovedBy = row["APPROVED_BY"] != DBNull.Value ? Convert.ToString(row["APPROVED_BY"]) : string.Empty;
                                mpindent.PurchaseIncharge = row["PURCHASE_INCHARGE"] != DBNull.Value ? Convert.ToString(row["PURCHASE_INCHARGE"]) : string.Empty;
                                mpindent.CreatedBy = row["CREATED_BY"] != DBNull.Value ? Convert.ToInt32(row["CREATED_BY"]) : 0;

                                listMPIndent.Add(mpindent);
                            }

                            listMPIndent = listMPIndent.OrderByDescending(v => v.IndID).ToList();

                        }
                    }
                    catch (Exception ex)
                    {
                        MPIndent mpindent = new MPIndent();
                        mpindent.ErrorMessage = ex.Message;
                        listMPIndent.Add(mpindent);
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return listMPIndent;
        }

        public List<int> GetRFQCreators(int indentID, string sessionID)
        {
            List<int> listAssignedUser = new List<int>();

            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        cmd.CommandText = "wf_GetRFQCreators";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new MySqlParameter("P_INDENT_ID", indentID));
                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                int userID = 0;

                                userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;


                                listAssignedUser.Add(userID);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }

            return listAssignedUser;
        }
        
        #endregion

        #region Post

        public Response SaveRFQCreators(List<UserInfo> listUsers, int indentID, string sessionID)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }

                        foreach (UserInfo UI in listUsers)
                        {

                            cmd.CommandText = "wf_SaveRFQCreators";

                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new MySqlParameter("P_U_ID", UI.UserID));

                            int IsIndentAssigned = 0;

                            if (UI.IsIndentAssigned)
                            {
                                IsIndentAssigned = 1;
                            }

                            cmd.Parameters.Add(new MySqlParameter("P_IS_ASSIGNED", IsIndentAssigned));
                            cmd.Parameters.Add(new MySqlParameter("P_INDENT_ID", indentID));

                            MySqlDataAdapter myDA1 = new MySqlDataAdapter(cmd);
                            DataSet ds1 = new DataSet();
                            myDA1.Fill(ds1);
                            cmd.Connection.Close();

                            //cmd.Parameters.Clear();

                            if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0 && ds1.Tables[0].Rows[0][0] != null)
                            {
                                response.ObjectID = ds1.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds1.Tables[0].Rows[0][0].ToString()) : -1;
                                response.ErrorMessage = ds1.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds1.Tables[0].Rows[0][1].ToString()) : string.Empty;
                            }

                            cmd.Parameters.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response;
        }
       
        #endregion

        #region Private

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString;
        }

        private int ValidateSession(string sessionId, MySqlCommand cmd)
        {
            if (string.IsNullOrEmpty(sessionId))
            {
                throw new Exception("Session ID is null");
            }
            cmd.Parameters.Clear();
            int isValid = 1;
            try
            {
                if (cmd == null)
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = new MySqlConnection(GetConnectionString());
                }
                cmd.CommandText = "cp_ValidateSession";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_SESSION_ID", sessionId));
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);

                cmd.Parameters.Clear();
                //cmd.Connection.Close();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    isValid = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (MySqlException ex)
            {
                HttpContext.Current.Server.Transfer("/prm360.html#/login");
            }
            catch (Exception ex)
            {
                isValid = 0;
            }

            if (isValid <= 0)
            {
                throw new Exception("Invalid Session ID passed");
            }

            return isValid;
        }

        private Response SaveAttachment(string path, MySqlCommand cmd = null)
        {
            Response response = new Response();
            try
            {

                if (cmd == null)
                {
                    cmd = new MySqlCommand();
                    cmd.Connection = new MySqlConnection(GetConnectionString());
                }
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }
                cmd.Parameters.Clear();
                cmd.CommandText = "cp_SaveAttachment";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_PATH", path));
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
                cmd.Parameters.Clear();
            }
            return response;
        }

        public string GenerateEmailBody(string TemplateName)
        {
            string body = string.Empty;
            //body = Engine.Razor.RunCompile(File.ReadAllText(templateFolderPath + "/EmailTemplates/" + TemplateName), "Email", null, model);
            XmlDocument doc = new XmlDocument();
            doc.Load(templateFolderPath + "/EmailTemplates/EmailFormats.xml");
            XmlNode node = doc.DocumentElement.SelectSingleNode(TemplateName);
            body = node.InnerText;
            string footerName = "";
            if (TemplateName.ToLower().Contains("email"))
            {
                footerName = "EmailFooter";
            }
            else if (TemplateName.ToLower().Contains("sms"))
            {
                footerName = "SMSFooter";
            }
            else
            {
                footerName = "FooterXML";
            }


            if (footerName == "FooterXML")
            {

            }
            else
            {
                XmlNode footernode = doc.DocumentElement.SelectSingleNode(footerName);
                body += footernode.InnerText;
            }

            return body;
        }

        public Communication GetCommunicationData(string communicationType, string sessionID = null, MySqlCommand cmd = null)
        {
            Communication communication = new Communication();

            communication.User = new User();
            communication.Company = new Company();

            try
            {

                if (cmd == null)
                {
                    cmd = new MySqlCommand(string.Empty, new MySqlConnection(GetConnectionString()));
                }
                if (cmd.Connection.State != ConnectionState.Open)
                {
                    cmd.Connection.Open();
                }

                cmd.Parameters.Clear();

                ValidateSession(sessionID, cmd);


                cmd.CommandText = "cp_GetCommunicationData";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new MySqlParameter("P_SESSION", sessionID));
                cmd.Parameters.Add(new MySqlParameter("P_C_TYPE", communicationType)); // (SMS or EMAIL or TELEGRAM)
                MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                myDA.Fill(ds);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    DataRow row = ds.Tables[0].Rows[0];

                    communication.User.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    communication.User.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    communication.User.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    communication.User.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    communication.User.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;

                    communication.Company.CompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    communication.Company.CompanyID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["COMP_ID"]) : 0;

                    communication.CompanyAddress = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[1].Rows.Count > 0 && ds.Tables[1].Rows[0][0] != null)
                    {

                        DataRow row1 = ds.Tables[1].Rows[0];

                        communication.FromAdd = row1["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row1["CONFIG_VALUE"]) : string.Empty;
                        communication.DisplayName = row1["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row1["CONFIG_TEXT"]) : string.Empty;

                    }
                }
            }
            catch (Exception ex)
            {
                communication.ErrorMessage = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return communication;
        }

        public async Task SendEmail(string To, string Subject, string Body, string sessionID = null, Attachment attachment = null, List<Attachment> ListAttachment = null)
        {

            try
            {
                Communication communication = new Communication();
                communication = Utilities.GetCommunicationData("EMAIL_FROM", sessionID);
                Body = Body.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                Body = Body.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                Body = Body.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");
                Body = Body.Replace("COMPANY_ADDRESS", !string.IsNullOrEmpty(communication.CompanyAddress) ? communication.CompanyAddress : "");


                SmtpClient smtpClient = new SmtpClient(ConfigurationManager.AppSettings["MAILHOST"].ToString());
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MAILHOST_USER"].ToString(), ConfigurationManager.AppSettings["MAILHOST_PWD"].ToString());
                smtpClient.Credentials = credentials;

                smtpClient.EnableSsl = false;

                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(prm.GetFromAddress(communication.User.Email), prm.GetFromDisplayName(communication.User.Email));
                mail.BodyEncoding = System.Text.Encoding.ASCII;
                if (attachment != null)
                {
                    mail.Attachments.Add(attachment);
                }

                if (ListAttachment != null)
                {
                    foreach (Attachment singleAttachment in ListAttachment)
                    {
                        if (singleAttachment != null)
                        {
                            mail.Attachments.Add(singleAttachment);
                        }
                    }
                }

                List<string> ToAddresses = To.Split(',').ToList<string>();
                foreach (string address in ToAddresses)
                {
                    if (!string.IsNullOrEmpty(address))
                    {
                        mail.To.Add(new MailAddress(address));
                    }

                }

                //if (!string.IsNullOrEmpty(communication.User.Email))
                //{
                //    mail.CC.Add(communication.User.Email);
                //}

                mail.Subject = Subject;
                mail.IsBodyHtml = true;
                mail.Body = Body;

                //smtpClient.Send(mail);
                await smtpClient.SendMailAsync(mail);

                Response response = new Response();
                response.ObjectID = 1;
                //return response;
            }
            catch (Exception ex)
            {
                Response response = new Response();
                response.ErrorMessage = ex.Message;
                //return response;
            }
        }

        public async Task<string> SendSMS(string from, string to, string message, string sessionID = null)
        {
            try
            {
                Communication communication = new Communication();
                communication = GetCommunicationData("SMS_FROM", sessionID);
                message = message.Replace("COMPANY_LOGO_ID", communication.Company.CompanyID > 0 ? communication.Company.CompanyID.ToString() : "0");
                message = message.Replace("COMPANY_NAME", !string.IsNullOrEmpty(communication.Company.CompanyName) ? communication.Company.CompanyName : "PRM360");
                message = message.Replace("USER_NAME", !string.IsNullOrEmpty(communication.User.FirstName + " " + communication.User.LastName) ? communication.User.FirstName + " " + communication.User.LastName : "");

                if (string.IsNullOrEmpty(from))
                {
                    from = ConfigurationManager.AppSettings["SMSFROM"].ToString();

                    if (!string.IsNullOrEmpty(communication.FromAdd))
                    {
                        from = communication.FromAdd;
                    };
                }

                message = message.Replace("&", "and").Replace("<br/>", Environment.NewLine);

                string strUrl = string.Format(ConfigurationManager.AppSettings["SMSURL"].ToString(), from, to, message);


                using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    //client.BaseAddress = new Uri(strUrl).;
                    HttpResponseMessage httpResponse = client.GetAsync(strUrl).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    string result = httpResponse.Content.ReadAsStringAsync().Result;
                }

                return "";
            }
            catch
            {
                return "";
            }
        }

        private string templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");

        private string SaveAttachmentInDB(int typeID, int userID, string attachmentID, string fileType)
        {
            Response response = new Response();
            using (MySqlCommand cmd = new MySqlCommand())
            {
                using (cmd.Connection = new MySqlConnection(Utilities.GetConnectionString()))
                {
                    try
                    {
                        if (cmd.Connection.State != ConnectionState.Open)
                        {
                            cmd.Connection.Open();
                        }
                        //int isValidSession = PRMUtility.ValidateSession(sessionID, cmd);

                        cmd.CommandText = "cp_SaveAttachmentInDB";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new MySqlParameter("P_TYPE_ID", typeID));
                        cmd.Parameters.Add(new MySqlParameter("P_U_ID", userID));
                        cmd.Parameters.Add(new MySqlParameter("P_ATTACHMENT_ID", attachmentID));
                        cmd.Parameters.Add(new MySqlParameter("P_FILE_TYPE", fileType));

                        MySqlDataAdapter myDA = new MySqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        myDA.Fill(ds);
                        cmd.Connection.Close();

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                        {
                            response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                            response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.ErrorMessage = ex.Message;
                    }
                    finally
                    {
                        cmd.Connection.Close();
                    }
                }
            }
            return response.ErrorMessage;
        }

        private string GenerateIndentPDF(string itemsString, string sessionID, MPIndent indentDetails = null, stringCIJ cijDetails = null)
        {
            UserDetails createdBy = prm.GetUserDetails(indentDetails.CreatedBy, sessionID);

            string Category = string.Empty;
            string AssetType = string.Empty;

            //if (indentDetails.CIJ.CijID > 0) {
            //    Category = "Capex";
            //}
            //else
            //{
            //    Category = "Non-Capex";
            //}

            if (indentDetails.IsCapex == -1)
            {
                Category = "Service Request";
            }
            if (indentDetails.IsCapex == 0)
            {
                Category = "Non-Capex";
            }
            if (indentDetails.IsCapex == 1)
            {
                Category = "Capex";
            }

            if (indentDetails.IsMedical > 0)
            {
                AssetType = "Medical";
            }
            else
            {
                AssetType = "Non-Medical";
            }

            //if (string.IsNullOrEmpty(createdBy.LogoURL))
            //{
            //    createdBy.LogoURL = "";
            //}
            //else
            //{
            //    createdBy.LogoURL = createdBy.LogoURL.Replace("/Services/auctionFiles//Services/auctionFiles/", "/Services/auctionFiles/");
            //    string fileName = System.Web.HttpContext.Current.Server.MapPath("/Services" + createdBy.CompanyId + ".png");                
            //    createdBy.LogoURL = "<img width=\"100px\" height=\"50px\" src=\"" + fileName + "\" />";
            //}

            string fileName = System.Web.HttpContext.Current.Server.MapPath("/Services/companylogos/" + createdBy.CompanyId + "_LOGO.png");
            createdBy.LogoURL = "<img width=\"100px\" height=\"50px\" src=\"" + fileName + "\" />";


            string html = string.Empty;
            html = "indentPDF.html";

            string filestring = File.ReadAllText(HttpContext.Current.Server.MapPath(Utilities.FILE_URL + html));
            string htmlRows = string.Empty;
            try
            {
                htmlRows = String.Format(filestring,
                createdBy.LogoURL.ToString(), // 0
                createdBy.CompanyName.ToString(),
                Category.ToString(),
                AssetType.ToString(),
                indentDetails.IndNo.ToString(), // 4
                indentDetails.Department.ToString(),
                indentDetails.RequestDate.ToString(),
                indentDetails.ApproximateCostRange.ToString(),
                itemsString.ToString(), // 8
                indentDetails.RequestedBy.ToString(),
                indentDetails.ApprovedBy.ToString(),
                indentDetails.PurchaseIncharge.ToString(),
                indentDetails.PurchaseDirector.ToString(),// 12
                indentDetails.UserPriority.ToString(),// 13
                indentDetails.ExpectedDelivery.ToString()// 14


               );
            }
            catch
            {

            }
            return htmlRows;
        }

        #endregion
    }
}  