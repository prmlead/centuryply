﻿using Microsoft.AspNet.SignalR;
using PRM.Data;
using PRMServices.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace PRMServices
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            /*** How to change timeout and keepalive settings ***/
            //// Make long polling connections wait a maximum of 110 seconds for a
            //// response. When that time expires, trigger a timeout command and
            //// make the client reconnect.
            //GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(110);

            //// Wait a maximum of 30 seconds after a transport connection is lost
            //// before raising the Disconnected event to terminate the SignalR connection.
            //GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(30);

            //// For transports other than long polling, send a keepalive packet every
            //// 10 seconds. 
            //// This value must be no more than 1/3 of the DisconnectTimeout value.
            //GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(10);
            /*** How to change timeout and keepalive settings ***/


            DbExtension.Invoke();
            AutofacConfiguration.Initialize();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            {
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept");
                HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
                HttpContext.Current.Response.End();
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}