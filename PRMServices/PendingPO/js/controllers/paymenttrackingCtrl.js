﻿prmApp
    .controller('paymenttrackingCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter", "$http", "poDomain",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, $http, poDomain) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();


            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.pageChanged = function () {
            };
            /*PAGINATION CODE*/

            $scope.paymentList = [];
            $scope.paymentListTemp = [];

            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
            $scope.userID = userService.getUserId();


            $scope.filtersList = {
                poNumber: [],
                invoiceNumber: [],
                vendorCode: [],
                grnNumber: []
            };

            $scope.filters = {
                poNumber: {},
                grnNumber: {},
                invoiceNumber: {},
                vendorCode: {},
                postingFromDate: moment().subtract(365, "days").format("YYYY-MM-DD"),
                postingToDate: moment().format('YYYY-MM-DD')
            };


            $scope.setFilters = function (currentPage) {
                if ($scope.filters.poNumber == '' || $scope.filters.invoiceNumber == '' || $scope.filters.vendorCode == '' || $scope.filters.searchKeyword == '') {
                    $scope.getPaymentInvoiceDetails();
                } else if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.poNumber) || !_.isEmpty($scope.filters.invoiceNumber) || !_.isEmpty($scope.filters.vendorCode)) {
                    $scope.getPaymentInvoiceDetails();
                }
            };

            $scope.filterByDate = function () {
                $scope.getPaymentInvoiceDetails();
            };


            $scope.getPaymentInvoiceDetails = function () {

                var poNumbers, grnNumber, invoiceNumbers, vendorCodes, postingFromDate, postingToDate;


                if (_.isEmpty($scope.filters.postingFromDate)) {
                    postingFromDate = '';
                } else {
                    postingFromDate = $scope.filters.postingFromDate;
                }

                if (_.isEmpty($scope.filters.postingToDate)) {
                    postingToDate = '';
                } else {
                    postingToDate = $scope.filters.postingToDate;
                }

                if (_.isEmpty($scope.filters.poNumber)) {
                    poNumbers = '';
                } else if ($scope.filters.poNumber && $scope.filters.poNumber.length > 0) {
                    var f1 = _($scope.filters.poNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    poNumbers = f1.join(',');
                }

                if (_.isEmpty($scope.filters.grnNumber)) {
                    grnNumber = '';
                } else if ($scope.filters.grnNumber && $scope.filters.grnNumber.length > 0) {
                    var f1 = _($scope.filters.grnNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    grnNumber = f1.join(',');
                }

                if (_.isEmpty($scope.filters.invoiceNumber)) {
                    invoiceNumbers = '';
                } else if ($scope.filters.invoiceNumber && $scope.filters.invoiceNumber.length > 0) {
                    var f2 = _($scope.filters.invoiceNumber)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    invoiceNumbers = f2.join(',');
                }

                if (_.isEmpty($scope.filters.vendorCode)) {
                    vendorCodes = '';
                } else if ($scope.filters.vendorCode && $scope.filters.vendorCode.length > 0) {
                    var f3 = _($scope.filters.vendorCode)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    vendorCodes = f3.join(',');
                }

                var params = {
                    "sessionID": $scope.sessionID,
                    "compId": $scope.compId,
                    "uId": $scope.userID,
                    "poNumber": poNumbers,
                    "grnNumber": grnNumber,
                    "invoiceNumber": invoiceNumbers,
                    "vendorCode": vendorCodes,
                    "fromdate": postingFromDate,
                    "todate": postingToDate,
                    "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : "",
                };
                PRMPOService.getPaymentInvoiceDetails(params)
                    .then(function (response) {
                        if (response) {
                            $scope.paymentList = response;
                            $scope.paymentListTemp = response;
                            $scope.totalItems = $scope.paymentList.length;
                        }

                    });
            };
            $scope.getPaymentInvoiceDetails();

            $scope.convertDate = function (date) {
                return date ? userService.toLocalDate(date).split(' ')[0] : date;
            };


            $scope.searchTable = function (search) {
                if (search) {
                    $scope.paymentList = _.filter($scope.paymentListTemp, function (item) {
                        return (item.invoiceNumber.toUpperCase().indexOf(search.toUpperCase()) > -1);
                    });
                } else {
                    $scope.paymentList = $scope.paymentListTemp;
                }
                $scope.totalItems = $scope.paymentList.length;

            };

            $scope.exportErrorDetails = function (type) {
                $state.go('moduleErrorList', { 'moduleName': type});
            };

            $scope.getDate = function (value) {
                return userService.toLocalDate(value);
            }


            $scope.getTdsCMDN = function (item) {
                if (item) {
                    var params = {
                        "invoiceNumber": item.invoiceNumber,
                        "companyId": +$scope.compId,
                        "sessionid": userService.getUserToken()
                    };
                    auctionsService.getTdsCMDN(params)
                        .then(function (response) {
                            item.tdsDetails = response;
                        });
                }
            };


            $scope.PaymentTrackingFilteredTemp = [];
            $scope.getPaymentTrackingListByFilter = function () {
                let poNumberTemp = [];
                let grnNumberTemp = [];
                let invoiceNumberTemp = [];
                let vendorCodeTemp = [];

                var params = {
                    "COMP_ID": $scope.compId,
                    "USER_ID": $scope.userID,
                    "sessionid": userService.getUserToken()
                };
                PRMPOService.getPaymentTrackingListByFilter(params)
                    .then(function (response) {
                        $scope.PaymentTrackingFilteredTemp = response;
                        $scope.PaymentTrackingFilteredTemp.forEach(function (item, index) {
                            if (item.TYPE === 'PO_NUMBER') {
                                poNumberTemp.push({ name: item.VALUE });
                            } else if (item.TYPE === 'INVOICE_NUMBER') {
                                invoiceNumberTemp.push({ name: item.VALUE });
                            } else if (item.TYPE === 'VENDOR_CODE') {
                                vendorCodeTemp.push({ name: item.VALUE });
                            } else if (item.TYPE === 'GRN_NUMBER') {
                                grnNumberTemp.push({ name: item.VALUE });
                            }
                        });
                        $scope.filtersList.poNumber = poNumberTemp;
                        $scope.filtersList.invoiceNumber = invoiceNumberTemp;
                        $scope.filtersList.vendorCode = vendorCodeTemp;
                        $scope.filtersList.grnNumber = grnNumberTemp;

                    });
            };
            $scope.getPaymentTrackingListByFilter();
            
        }]);