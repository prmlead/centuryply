﻿prmApp
    .controller('grnAuditCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "$rootScope", "fileReader","PRMPOService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, $rootScope, fileReader, PRMPOService) {

            $scope.USER_ID = userService.getUserId();
            $scope.COMP_ID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.grnNumber = $stateParams.grnNo;
            $scope.companyDesignations = [];
            $scope.companyDesignations1 = [];

            $scope.CompanyDeptDesigTypes = [];
            $scope.templateName = $state.current.name;
            $scope.loadServices = false;
            


            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;
            /*PAGINATION CODE*/

            $scope.filters1 = {
                searchKeyword: ''
            };
            $scope.filters1.toDate = moment().format('YYYY-MM-DD');
            $scope.filters1.fromDate = moment().subtract(30, "days").format("YYYY-MM-DD");
            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                if ($scope.loadServices) {
                    $scope.GetMasterDetails(($scope.currentPage - 1), 10, $scope.filters1.searchKeyword);
                }
            };


            $scope.filtersVal = {};
            $scope.tableColumns = [];
            $scope.rows = [];


         

            $scope.GetInvoiceAudit = function (recordsFetchFrom, pageSize, sharedIndex) {
                $scope.defaultParams =
                {
                    "GRN_NUMBER": $scope.grnNumber,
                    "SESSION_ID": $scope.sessionID,
                };


               

                var validParams = Object.keys($scope.defaultParams).map(function (key) {
                    return encodeURIComponent(key) + '=' + encodeURIComponent($scope.defaultParams[key]);
                }).join('&');


                PRMPOService.GetInvoiceAudit(validParams)
                    .then(function (response) {
                        if (response) {
                            $scope.loadServices = true;
                            $rootScope.$broadcast('loadServiceUpdated', { loadServices: true });
                            $scope.auditArr = JSON.parse(response).Table;
                            $scope.arr1 = JSON.parse(response).Table1;

                            $scope.auditArr.forEach(function (item) {
                                item.showData = true;
                            })


                            if ($scope.arr1 && $scope.arr1.length > 0)
                            {
                                $scope.totalItems = $scope.arr1[0].TOTAL_COUNT;
                                
                                $scope.arr1.forEach(a => delete a.TOTAL_COUNT);
                                if ($scope.tableColumns.length <= 0) {
                                    $scope.tableColumnsTemp = angular.copy(_.keys($scope.arr1[0]));
                                    $scope.tableColumnsTemp.forEach(function (item, index) {
                                        item = item.replaceAll("_", " ");
                                        $scope.tableColumns.push(item);
                                    });
                                }

                                $scope.rows = $scope.arr1;
                                $scope.arr1.forEach(function (item, index) {
                                    var obj = angular.copy(_.values(item));
                                    if (obj)
                                    {
                                        item.tableValues = [];
                                        obj.forEach(function (value, valueIndex) {
                                            item.tableValues.push(value);
                                        });
                                    }
                                });                               

                            }
                        } else {
                            $scope.rows = [];
                            $scope.arr = [];
                            $scope.totalItems = 0;
                        }

                    })
            };



            $scope.GetInvoiceAudit(0, 10, '');

            $scope.rowsTemp = [];

            $scope.filterBasedonAuditVesrion = function (auditVersion) {

                $scope.rowsTemp = $scope.rows

                $scope.rowsTemp = _.filter($scope.rowsTemp, function (item) {
                    return item.AUDIT_ENTRY == auditVersion;
                    //(String(item.AUDIT_ENTRY).toUpperCase().includes(auditVersion.toUpperCase())) == true;
                });


                $scope.auditArr.forEach(function (item) {

                    if (item.AUDIT_ENTRY == auditVersion) {
                        item.rowsTemp = $scope.rowsTemp;
                    }
                })

            }

            $scope.routeGRN = function () {
                var url = $state.href('list-GRN');
                $window.open(url, '_blank');
            }


            

        }]);