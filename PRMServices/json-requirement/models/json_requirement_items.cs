﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class json_requirement_items : json_entity
    {

        [DataMember] [DataNames("RI_ID")] public int RV_ID { get; set; }
        [DataMember] [DataNames("CATALOG_ITEM_ID")] public int CATALOG_ITEM_ID { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }        
        [DataMember] [DataNames("ITEM_CODE")] public string ITEM_CODE { get; set; }
        [DataMember] [DataNames("ITEM_NUMBER")] public string ITEM_NUMBER { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("QUANTITY")] public decimal QUANTITY { get; set; }
        [DataMember] [DataNames("UNITS")] public string UNITS { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("DESCRIPTION")] public string DESCRIPTION { get; set; }


        [DataMember] [DataNames("list_json_requirement_item_property")] public List<json_requirement_item_property> list_json_requirement_item_property { get; set; }

    }
}