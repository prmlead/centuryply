﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class TomorrowMeetings : Entity
    {
        [DataMember(Name="compId")]
        public int CompId { get; set; }

        [DataMember(Name = "meetingDateTime")]
        public DateTime MeetingDateTime { get; set; }

        [DataMember(Name = "noteToId")]
        public int NoteToId { get; set; }

        [DataMember(Name = "notes")]
        public string Notes { get; set; }

        [DataMember(Name = "createdDate")]
        public DateTime? CreatedDate { get; set; }
    }
}