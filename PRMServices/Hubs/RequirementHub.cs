﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using PRMServices.Models;
using System.Threading.Tasks;
using System.Data;
using PRMServices.SQLHelper;
using System.Collections.Concurrent;

namespace PRMServices.SignalR
{
    public class RequirementHub : Hub
    {
        //private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        //private static readonly ConcurrentDictionary<string, List<string>> GroupMemberships = new ConcurrentDictionary<string, List<string>>();
        public Task JoinGroup(string groupName)
        {
            //ConnectionGroupMap.TryAdd(Context.ConnectionId, groupName);
            //GroupMemberships.AddOrUpdate(groupName, new List<string> { Context.ConnectionId }, (key, list) =>
            //{
            //    list.Add(Context.ConnectionId);
            //    return list;
            //});
            return Groups.Add(Context.ConnectionId, groupName);
        }

        public Task LeaveGroup(string groupName)
        {
            return Groups.Remove(Context.ConnectionId, groupName);
        }

        //public override Task OnDisconnected(bool stopCalled)
        //{
        //    if (stopCalled)
        //    {
        //        Clients.All.someoneDisconnected(Context.ConnectionId);

        //        //foreach (var groupMembership in GroupMemberships)
        //        //{
        //        //    if (groupMembership.Value.Contains(Context.ConnectionId))
        //        //    {
        //        //        // Handle the disconnection event for the specific group
        //        //        var groupName = groupMembership.Key;
        //        //        logger.Error("someone disconnected sending data triggering from OnDisconnected method>>>");
        //        //        Clients.Group(groupName).someoneDisconnected();

        //        //        // Remove the connection ID from the group's membership
        //        //        groupMembership.Value.Remove(Context.ConnectionId);

        //        //        // If the group becomes empty, remove it from the dictionary
        //        //        if (groupMembership.Value.Count == 0)
        //        //        {
        //        //            GroupMemberships.TryRemove(groupMembership.Key, out _);
        //        //        }

        //        //        break;
        //        //    }
        //        //}
        //    }

        //    return base.OnDisconnected(stopCalled);



        //    //string groupName;
        //    //if (ConnectionGroupMap.TryGetValue(Context.ConnectionId, out groupName))
        //    //{
        //    //    // Handle the disconnection event and notify the remaining clients in the group
        //    //    Clients.Group(groupName).someoneDisconnected();
        //    //}
        //    //return base.OnDisconnected(stopCalled);
        //    //// Get the group name
        //    //var groupName = Context.Request.QueryString["groupName"];
        //    //if (!string.IsNullOrEmpty(groupName))
        //    //{
        //    //    // Notify the remaining clients in the group that someone has disconnected
        //    //    Clients.Group(groupName).someoneDisconnected();
        //    //    logger.Error("some one has disconnected from signalr and the group name>>>" + groupName);
        //    //    return base.OnDisconnected(stopCalled);
        //    //}
        //    //else {
        //    //    return null;
        //    //}
        //}

        public SignalRObj UpdateTime(string parameters)
        {
            SignalRObj signalRObj = new SignalRObj();
            List<object> payLoad = new List<object>();
            RequirementLite requirementLite = new RequirementLite();
            DataSet ds = new DataSet();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);
            int CallerID = Convert.ToInt32(listofParams[1]);
            long ticks = Convert.ToInt64(listofParams[2]);
            int lotId = Convert.ToInt32(listofParams[4]); // lot
            string sessionID = listofParams[3];
            string updateType = listofParams[5];
            Response response = new Response();
            PRMServices prm = new PRMServices();

            DateTime newTime = DateTime.UtcNow.AddSeconds(ticks);
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_REQ_ID", reqID);
            sd.Add("P_CLOSE", ticks);
            sd.Add("P_NEW_TIME", newTime);
            sd.Add("P_U_ID", userID);
            sd.Add("P_TYPE", "");
            sqlHelper.SelectList("cp_UpdateBidTime", sd);
            requirementLite = prm.GetRequirementDetailsSignalR(reqID);
            if (requirementLite != null && requirementLite.AuctionVendors != null && requirementLite.AuctionVendors.Count > 0)
            {
                long timeLeft = 0;
                DateTime date = Convert.ToDateTime(requirementLite.END_TIME);
                timeLeft = Convert.ToInt64((date - DateTime.UtcNow).TotalSeconds);

                if (timeLeft < 0)
                {
                    timeLeft = 0;
                }

                payLoad.Add(timeLeft);

                InvolvedParties involvedParties = new InvolvedParties();
                involvedParties.RequirementID = requirementLite.REQ_ID;
                involvedParties.CustomerID = requirementLite.CUSTOMER_U_ID;
                involvedParties.SuperUserID = requirementLite.SUPER_USER_ID;
                involvedParties.UserIDList = requirementLite.AuctionVendors.Select(x => x.VendorID).ToArray();
                involvedParties.MethodName = "UpdateTime";
                involvedParties.CallerID = CallerID;
                involvedParties.ErrorMessage = response.ErrorMessage;
                signalRObj.Inv = involvedParties;
                getSignalRObjTime(requirementLite.REQ_ID, payLoad, "CUSTOMER", lotId);
                foreach (int vendorID in involvedParties.UserIDList)
                {
                    payLoad = new List<object>();
                    payLoad.Add(timeLeft);
                    getSignalRObjTime(requirementLite.REQ_ID, payLoad, vendorID.ToString(), lotId);
                }
            }
            
            return signalRObj;
        }

        public SignalRObj UpdateTimeRefresh(string parameters)
        {
            //System.Diagnostics.Debug.WriteLine(parameters);
            SignalRObj signalRObj = new SignalRObj();
            List<object> payLoad = new List<object>();
            RequirementLite requirementLite = new RequirementLite();
            DataSet ds = new DataSet();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);
            int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[3];
            PRMServices prm = new PRMServices();

            requirementLite = prm.GetRequirementDetailsSignalR(reqID);
            if (requirementLite != null && requirementLite.AuctionVendors != null && requirementLite.AuctionVendors.Count > 0)
            {
                long timeLeft = 0;
                DateTime date = Convert.ToDateTime(requirementLite.END_TIME);
                timeLeft = Convert.ToInt64((date - DateTime.UtcNow).TotalSeconds);

                if (timeLeft < 0)
                {
                    timeLeft = 0;
                }

                payLoad.Add(timeLeft);
                InvolvedParties involvedParties = new InvolvedParties();
                involvedParties.RequirementID = requirementLite.REQ_ID;
                involvedParties.CustomerID = requirementLite.CUSTOMER_U_ID;
                involvedParties.SuperUserID = requirementLite.SUPER_USER_ID;
                involvedParties.UserIDList = requirementLite.AuctionVendors.Select(x => x.VendorID).ToArray();
                involvedParties.MethodName = "UpdateTime";
                involvedParties.CallerID = CallerID;
                involvedParties.ErrorMessage = "";
                signalRObj.Inv = involvedParties;
                getSignalRObjTime(requirementLite.REQ_ID, payLoad, "CUSTOMER", 0);
                if (involvedParties.UserIDList != null && involvedParties.UserIDList.Length > 0)
                {
                    foreach (int vendorID in involvedParties.UserIDList)
                    {
                        payLoad = new List<object>();
                        payLoad.Add(timeLeft);
                        getSignalRObjTime(requirementLite.REQ_ID, payLoad, vendorID.ToString(), 0);
                    }
                }
            }

            return signalRObj;
        }

        public Requirement DeleteRequirement(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            string reason = listofParams[3];

            Response response = new Response();
            PRMServices prm = new PRMServices();
            response = prm.DeleteRequirement(reqID, userID, sessionID, reason);
            Requirement req = new Requirement();
            DataSet ds = prm.GetRequirementDetailsHub(reqID);
            req = prm.GetRequirementDataFilter(userID, sessionID, ds);
            req.ErrorMessage += response.ErrorMessage;
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            involvedParties.MethodName = "DeleteRequirement";
            involvedParties.CallerID = CallerID;
            involvedParties.ErrorMessage = response.ErrorMessage;
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            payLoad.Add(req);
            getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad = new List<object>();
                payLoad.Add(req);
                getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
            }

            return req;
        }

        public Requirement CancelRequirement(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]); int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            string reason = listofParams[3];
            bool isEmailSend = Convert.ToBoolean(listofParams[4]);
            bool isSmsSend = Convert.ToBoolean(listofParams[5]);

            Response response = new Response();
            PRMServices prm = new PRMServices();
            response = prm.CancelRequirement(reqID, userID, sessionID, reason, isEmailSend, isSmsSend);
            Requirement req = new Requirement();
            DataSet ds = prm.GetRequirementDetailsHub(reqID);
            req = prm.GetRequirementDataFilter(userID, sessionID, ds);
            req.ErrorMessage += response.ErrorMessage;
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            involvedParties.MethodName = "CancelRequirement";
            involvedParties.CallerID = CallerID;
            involvedParties.ErrorMessage = response.ErrorMessage;
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            payLoad.Add(req);
            getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad = new List<object>();
                payLoad.Add(req);
                getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
            }

            return req;
        }

        public Response UpdateComments(Comment comment)
        {
            PRMServices prm = new PRMServices();
            Response res = prm.SaveComment(comment);
            return res;
        }

        public SignalRObj UpdateAuctionStartSignalR(RunningAuction auction)
        {
            SignalRObj signalRObj = new SignalRObj();
            List<object> payLoad = new List<object>();
            Requirement req = new Requirement();
            PRMServices prm = new PRMServices();
            Response res = prm.UpdateAuctionStart(auction.AuctionID, auction.CustomerID, auction.AuctionEnds, auction.SessionID, auction.NegotiationDuretion, auction.NegotiationSettings);
            DataSet ds = prm.GetRequirementDetailsHub(auction.AuctionID);
            req = prm.GetRequirementDataFilter(auction.CustomerID, auction.SessionID, ds);
            payLoad.Add(req);
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            if (req.AuctionVendors != null)
            {
                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            }
            involvedParties.MethodName = "UpdateAuctionStartSignalR";
            involvedParties.CallerID = auction.CustomerID;
            involvedParties.ErrorMessage = res.ErrorMessage;
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            signalRObj.Inv = involvedParties;
            getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, auction.SessionID, ds);
                payLoad = new List<object>();
                payLoad.Add(req);
                getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
            }

            
            return signalRObj;
        }

        public Requirement MakeBid(BidObject parameters)
        {
            PRMServices prm = new PRMServices();
            Requirement req = new Requirement();
            byte[] quotation = null;
            Response res = prm.MakeABid(parameters.ReqID, parameters.UserID, parameters.Price, parameters.Quotation,
                parameters.QuotationName, parameters.SessionID, parameters.Tax, parameters.Freightcharges,
                parameters.Warranty, parameters.Payment, parameters.Duration, parameters.Validity,
                parameters.QuotationObject, parameters.Revised, parameters.PriceWithoutTax, parameters.Type,
                parameters.Discount, parameters.ListRequirementTaxes, parameters.OtherProperties, parameters.Ignorevalidations
                ,parameters.Itemrevtotalprice,parameters.VendorBidPrice,0,0,0, parameters.BasePrice
                );

            req.ErrorMessage = res.ErrorMessage;
            return req;
        }

        public Requirement MakeBidLite(BidObjectLite parameters)
        {
            PRMServices prm = new PRMServices();
            Requirement req = new Requirement();
            Response res = prm.MakeABidLite(parameters.ReqID, parameters.UserID, parameters.TotalPrice,
                parameters.QuotationObject, parameters.Ignorevalidations, parameters.BasePrice,parameters.SessionID, parameters.CustCompID
                );

            req.ErrorMessage = res.ErrorMessage;
            return req;
        }


        /*public Response GetItemPricesSignalRLite(string parameters)
        {
            PRMServices prm = new PRMServices();
            Response response = new Response();
            int parameter = Convert.ToInt32(parameters);

            List<BidItemObjectLite> bidItems = prm.AllItemPricesLite(parameter);

            SignalRObj signalRObj = new SignalRObj();
            List<object> payLoad = new List<object>();

            BidObjectLite req = new BidObjectLite();
            req.ReqID = parameter;
            req.QuotationObject = bidItems.ToArray();
            payLoad.Add(req);

            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = parameter;
            involvedParties.CustCompID = req.CustCompID;
            involvedParties.MethodName = "makeBidLite";
            involvedParties.UserIDList = req.QuotationObject.Select(x => x.VendorID).Distinct().ToArray();
            signalRObj.PayLoad = payLoad.ToArray();

            getSignalRObjMakeBidLite(req.ReqID, involvedParties, payLoad, "CUSTOMER");


            foreach (int vendorID in involvedParties.UserIDList)
            {
                payLoad = new List<object>();
                BidObjectLite reqDetVendor = new BidObjectLite();
                reqDetVendor.ReqID = parameter;
                reqDetVendor.UserID = vendorID;
                reqDetVendor.QuotationObject = bidItems.Where(a => a.VendorID == vendorID).ToArray();
                payLoad.Add(reqDetVendor);
                signalRObj.PayLoad = payLoad.ToArray();
                getSignalRObjMakeBidLite(req.ReqID, involvedParties, payLoad, vendorID.ToString());
            }
            return response;
        }*/

        public List<Comment> SaveComment(Comment com)
        {
            PRMServices prm = new PRMServices();
            Response res = prm.SaveComment(com);
            DataSet ds = prm.GetRequirementDetailsHub(com.RequirementID);
            Requirement req = prm.GetRequirementDataFilter(com.UserID, com.SessionID, ds);
            //Requirement req = prm.GetRequirementDataOffline(com.RequirementID, com.UserID, com.SessionID);
            List<Comment> comments = prm.GetComments(com.RequirementID, com.UserID, com.SessionID);
            comments[0].ErrorMessage += res.ErrorMessage;
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            involvedParties.MethodName = "SaveComment";
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
            Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
            
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, com.SessionID, ds);
                Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
            }
            return comments;
        }

        public void CheckRequirement(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);
            int CallerID = userID;
            string sessionID = listofParams[2];
            string SubMethodName = string.Empty;
            if (listofParams != null && listofParams.Length > 3)
            {
                SubMethodName = listofParams[3];
            }

            int ReceiverId = -1;
            if (listofParams != null && listofParams.Length > 4)
            {
                if (!string.IsNullOrEmpty(listofParams[4]))
                {
                    ReceiverId = Convert.ToInt32(listofParams[4]);
                }
            }

            PRMServices prm = new PRMServices();
            DataSet ds = prm.GetRequirementDetailsHub(reqID);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;

            }

            Requirement req = prm.GetRequirementDataFilter(userID, sessionID, ds);
            payLoad.Add(req);
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.MethodName = "CheckRequirement";
            involvedParties.SubMethodName = SubMethodName;
            involvedParties.ReceiverId = ReceiverId;
            involvedParties.CallerID = CallerID;
            involvedParties.CustCompID = req.CustCompID;
            involvedParties.ErrorMessage = req.ErrorMessage;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            req.Inv = involvedParties;
            getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad = new List<object>();
                payLoad.Add(req);
                getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
            }
        }

        public void RestartNegotiation(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);
            int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            PRMServices prm = new PRMServices();
            Response res = prm.RestartNegotiation(reqID, userID, sessionID);
            DataSet ds = prm.GetRequirementDetailsHub(reqID);
            Requirement req = prm.GetRequirementDataFilter(userID, sessionID, ds);
            payLoad.Add(req);
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.MethodName = "RestartNegotiation";
            involvedParties.CallerID = CallerID;
            involvedParties.CustCompID = req.CustCompID;
            involvedParties.ErrorMessage = req.ErrorMessage;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            req.Inv = involvedParties;
            getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
            Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
            Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad = new List<object>();
                payLoad.Add(req);
                getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
            }
        }


        public void StopBids(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            PRMServices prm = new PRMServices();
            Response res = prm.StopBids(reqID, userID, sessionID);
            if (res.ErrorMessage == "" || res.ErrorMessage == "Failure sending mail.")
            {
                DataSet ds = prm.GetRequirementDetailsHub(reqID);
                Requirement req = prm.GetRequirementDataFilter(userID, sessionID, ds);
                payLoad.Add(req);
                InvolvedParties involvedParties = new InvolvedParties();
                involvedParties.RequirementID = req.RequirementID;
                involvedParties.CustomerID = req.CustomerID;
                involvedParties.SuperUserID = req.SuperUserID;
                involvedParties.MethodName = "StopBids";
                involvedParties.CallerID = CallerID;
                involvedParties.CustCompID = req.CustCompID;
                involvedParties.ErrorMessage = res.ErrorMessage;
                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                req.Inv = involvedParties;
                getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
                Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                foreach (int vendorID in involvedParties.UserIDList)
                {
                    req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                    payLoad = new List<object>();
                    payLoad.Add(req);
                    getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
                }
            }
        }

        public void EndNegotiation(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            PRMServices prm = new PRMServices();
            Response res = prm.EndNegotiation(reqID, userID, sessionID);
            if (res.ErrorMessage == "" || res.ErrorMessage == "Failure sending mail.")
            {
                DataSet ds = prm.GetRequirementDetailsHub(reqID);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                }

                Requirement req = prm.GetRequirementDataFilter(userID, sessionID, ds);
                payLoad.Add(req);
                InvolvedParties involvedParties = new InvolvedParties();
                involvedParties.RequirementID = req.RequirementID;
                involvedParties.CustomerID = req.CustomerID;
                involvedParties.SuperUserID = req.SuperUserID;
                involvedParties.CallerID = CallerID;
                involvedParties.CustCompID = req.CustCompID;
                involvedParties.ErrorMessage = res.ErrorMessage;
                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                involvedParties.MethodName = "EndNegotiation";
                req.Inv = involvedParties;
                getSignalRObj(req.RequirementID, involvedParties, payLoad, "CUSTOMER");
                Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                Clients.Group(Utilities.GroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                foreach (int vendorID in involvedParties.UserIDList)
                {
                    req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                    payLoad = new List<object>();
                    payLoad.Add(req);
                    getSignalRObj(req.RequirementID, involvedParties, payLoad, vendorID.ToString());
                }
            }
        }

        public void SendMessage(ChatHistory chatHistory)
        {
            //Clients.All.broadcastMessage(/chatHistory., "");
            Clients.Group(Utilities.PRMChatGroupName + chatHistory.ModuleId).chatUpdate(chatHistory);
        }

        public SignalRObj getSignalRObj(int requirementId, InvolvedParties parties, List<object> objArr, string userId)
        {
            SignalRObj signalRObj = new SignalRObj();
            signalRObj.Inv = parties;
            signalRObj.PayLoad = objArr.ToArray();
            Clients.Group(Utilities.GroupName + requirementId + "_" + userId).checkRequirement(signalRObj);
            return signalRObj;
        }

        public SignalRObj getSignalRObjTime(int requirementId, List<object> objArr, string userId, int lotId = 0)
        {
            SignalRObj signalRObj = new SignalRObj();
            signalRObj.PayLoad = objArr.ToArray();
            Clients.Group(Utilities.GroupName + requirementId + "_" + userId).timerUpdateSignalR(signalRObj);
            if (lotId > 0)
            {
                Clients.Group(Utilities.GroupName + "_LOT_" + userId).timerUpdateLotSignalR(signalRObj);
            }

            return signalRObj;
        }
        //public SignalRObj getSignalRObjMakeBidLite(int requirementId, InvolvedParties parties, List<object> objArr, string userId)
        //{
        //    SignalRObj signalRObj = new SignalRObj();
        //    signalRObj.Inv = parties;
        //    signalRObj.PayLoad = objArr.ToArray();
        //    Clients.Group(Utilities.GroupName + requirementId + "_" + userId).makeBidLite(signalRObj);
        //    return signalRObj;
        //}
    }

}