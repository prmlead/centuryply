﻿prmApp

    .controller('indentListCtrl', function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService, workflowService, cijIndentService) {

        $scope.indentList = [];
        $scope.compID = userService.getUserCompanyId();

        $scope.GetIndentList = function () {
            cijIndentService.GetIndentList(userService.getUserId(), userService.getUserToken())
                .then(function (response) {
                    $scope.indentList1 = [];
                    $scope.indentList1 = response;

                    $scope.indentList1.forEach(function (item, index) {

                        if (item.isCapex > 0) {

                            item.isCapexName = "CAPEX";
                        }
                        else if (item.isCapex == 0) {
                            item.isCapexName = "NON-CAPEX";
                        }
                        else if (item.isCapex < 0) {
                            item.isCapexName = "SERVICE REQUEST";
                        }

                        if (item.isMedical == 0) {
                            item.isMedicalName = "Non-Medical";
                        }
                        else if (item.isMedical > 0) {
                            item.isMedicalName = "Medical";
                        }

                    });
                    $scope.indentList = $scope.indentList1;
                    $scope.tempIndentLists = angular.copy($scope.indentList);
                    $scope.totalItems = $scope.indentList.length;
                })
        }

        $scope.GetIndentList();


        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.cancelIndent = function (indent, isValid) {
            indent.isValid = isValid;
            indent.sessionID = userService.getUserToken();
            var params = {
                mpindent: indent
            };

            cijIndentService.deleteIndent(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("INDENT Saved Successfully.", "success");
                    }
                });
        }

        $scope.goToIndent = function (id) {
            var url = $state.href('materialsPurchaseIndent', { "indentID": id });
            window.open(url, '_self');
        }

        $scope.goToCIJ = function (cijID) {
            var url = $state.href('cij', { "cijID": cijID });
            window.open(url, '_self');
        }

        $scope.goToIndentView = function (id) {
            var url = $state.href('materialPurchaseIndentView', { "indentID": id });
            window.open(url, '_self');
        }




        $scope.totalItems = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 8;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
        };


        $scope.indentListFilter = function (filterVal) {
            $scope.filterArray = [];
            if (filterVal == 'all') {
                $scope.indentList = $scope.indentList1;

            } else {
                $scope.filterArray = $scope.indentList1.filter(function (item) {
                    return item.isMedical == filterVal;
                });
                $scope.indentList = $scope.filterArray;

            }

            $scope.totalItems = $scope.indentList.length;
        }

        $scope.GetItemsList = function (moduleID, module, index) {
            cijIndentService.GetItemsList(moduleID, module, userService.getUserToken())
                .then(function (response) {
                    $scope.ItemsList = response;
                    $scope.indentList[index].ItemsList = [];
                    $scope.indentList[index].ItemsList = $scope.ItemsList;
                })
        }

        $scope.SaveUserStatus = function (module, id, status) {

            var params = {
                module: module,
                id: id,
                status: status,
                userId: userService.getUserId(),
                sessionID: userService.getUserToken()
            }

            workflowService.SaveUserStatus(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl('Saved Successfully', "success");
                    }

                })
        }

        $scope.searchTable = function (str) {
            $scope.indentList = $scope.indentList1.filter(function (req) {
                return (String(req.indNo).includes(str) == true || String(req.isCapexName).includes(str) == true || String(req.cij.cijCode).includes(str) == true || String(req.isMedicalName).includes(str) == true || String(req.department).includes(str) == true || String(req.requestDate).includes(str) == true || String(req.requestedBy).includes(str) == true || String(req.status).includes(str) == true || String(req.userWFStatus).includes(str) == true || String(req.currentApproverName).includes(str) == true);
            });

            $scope.totalItems = $scope.indentList.length;
        }


    });