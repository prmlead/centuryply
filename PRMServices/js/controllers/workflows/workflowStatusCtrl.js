﻿prmApp
    .controller('workflowStatusCtrl', function ($scope, $state, $stateParams, userService, growlService, workflowService, auctionsService, fileReader, $log) {
        $scope.compID = userService.getUserCompanyId();
        $scope.sessionID = userService.getUserToken();
        $scope.currentID = userService.getUserId();
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

        //$scope.getItemWorkflow = function () {
        //    workflowService.getItemWorkflow($stateParams.workflowID, $stateParams.moduleID, $stateParams.module)
        //        .then(function (response) {
        //            $scope.itemWorkflow = response;
        //            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
        //                $scope.currentStep = 0;

        //                var count = 0;

        //                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

        //                    if (track.status == 'REJECTED' && count == 0) {
        //                        count = count + 1;
        //                    }

        //                    if (track.status == 'PENDING' && count == 0) {
        //                        count = count + 1;
        //                        $scope.IsUserApproverForStage(track.approverID);
        //                        $scope.currentAccess = track.order;
        //                    }

        //                    if ((track.status == 'PENDING' || track.status == 'REJECTED') && $scope.currentStep == 0) {
        //                        $scope.currentStep = track.order;
        //                        return false;
        //                    }
        //                });
        //            }
        //        });
        //}

        //$scope.getItemWorkflow();

        //$scope.isSaveEnabled = function () {
        //    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
        //        if ($scope.itemWorkflow[0].WorkflowTracks[0].status == 'PENDING' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'REJECTED') {
        //            return false;
        //        }
        //    }
        //    else {
        //        return false;
        //    }

        //    return true;
        //};

        //$scope.updateTrack = function (step, status) {

        //    $scope.commentsError = '';
        //    if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
        //        $scope.commentsError = 'Please enter comments';
        //        return false;
        //    }

        //    step.status = status;
        //    step.sessionID = $scope.sessionID;
        //    step.modifiedBy = $scope.currentID;
        //    //step.moduleCode = $scope.cij.code;
        //    step.moduleName = 'QUOTATION';


        //    workflowService.SaveWorkflowTrack(step)
        //        .then(function (response) {
        //            console.log(response);
        //            if (response.errorMessage != '') {
        //                growlService.growl(response.errorMessage, "inverse");
        //            }
        //            else {
        //                growlService.growl("Action Saved Successfully.", "success");
        //            }
        //        })
        //}


        //$scope.IsUserApprover = false;

        //console.log("deptid>>>>>" + userService.getSelectedUserDepartmentDesignation().deptID);
        //console.log("desigid>>>>" + userService.getSelectedUserDepartmentDesignation().desigID);
        //$scope.functionResponse = false;
        //$scope.IsUserApproverForStage = function (approverID) {
        //    workflowService.IsUserApproverForStage(approverID, userService.getUserId(),
        //        userService.getSelectedUserDepartmentDesignation().deptID, userService.getSelectedUserDepartmentDesignation().desigID)
        //        .then(function (response) {
        //            $scope.IsUserApprover = response;
        //        });
        //};



        /*region start WORKFLOW*/
        //new workflow
        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListTemp = response;
                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule == $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                        }
                    });

                    if (userService.getUserObj().isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = $scope.workflowList.filter(function (item) {
                            return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                        });
                    }



                });
        };

        $scope.getWorkflows();

        $scope.getItemWorkflow = function () {
            //workflowService.getItemWorkflow(0, $stateParams.cijID, $scope.WorkflowModule)
            workflowService.getItemWorkflow($stateParams.workflowID, $stateParams.moduleID, $stateParams.module)
                .then(function (response) {
                    //console ------
                    $scope.itemWorkflow = response;
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;
                        // checking disable functionality
                        $scope.workflowObj.workflowID = $scope.itemWorkflow[0].workflowID;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                            

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && track.department.deptTypeId == 5 && userService.getSelectedUserDepartmentDesignation().deptTypeID == 5) {
                                $scope.isFormdisabled = false;
                                // return false;
                            }

                            if (track.status == 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }



                            if (track.status == 'REJECTED' && count == 0) {
                                count = count + 1;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                count = count + 1;
                                $scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                    //else {
                    //    $scope.isEditDisable = false;
                    //}
                    //$scope.checkflow();
                });

        };

        $scope.getItemWorkflow();

        $scope.isSaveEnabled = function () {
            if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                if ($scope.itemWorkflow[0].WorkflowTracks[0].status == 'PENDING' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'HOLD' || $scope.itemWorkflow[0].WorkflowTracks[0].status == 'REJECTED') {
                    return false;
                }
            }
            else {
                return false;
            }

            return true;
        };

        // Checking WF
        $scope.updateTrack = function (step, status) {


            $scope.commentsError = '';
            if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = $scope.currentID;
            step.moduleCode = $scope.CIJ_CODE;

            if (step.moduleName != '') {
                step.moduleName = step.moduleName;
            } else {
                step.moduleName = 'QUOTATION';
            }
            

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    //if (response.errorMessage != '') {
                    //    growlService.growl(response.errorMessage, "inverse");
                    //    location.reload();
                    //}
                    //else {
                    //    $scope.getItemWorkflow();
                    //}
                    if (response.errorMessage != '') {
                        swal({
                            title: response.errorMessage,
                            //text: $scope.message,
                            type: "warning",
                            //  showCancelButton: true,
                            confirmButtonColor: "#F44336",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                $scope.getItemWorkflow();
                           //     $state.go('materialsPurchaseIndent');
                                location.reload();
                            });
                    }
                    else {
                        //growlService.growl("INDENT Saved Successfully.", "success");
                        $scope.getItemWorkflow();
                      //  $state.go('materialsPurchaseIndent');
                        location.reload();
                    }
                })
        };

        $scope.IsUserApproverForStage = function (approverID) {
            workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };

        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                            disable = true;
                        }
                        else if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            })

            return disable;
        };

            /*region end WORKFLOW*/

        $scope.dateFormat = function (date) {
            return new moment(date).format("DD-MM-YYYY HH:mm");
        }


    });