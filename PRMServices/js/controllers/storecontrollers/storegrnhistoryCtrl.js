﻿prmApp
    .controller('storegrnhistoryCtrl', ["$scope", "$state", "$stateParams", "$window", "$log", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $window, $log, userService, storeService, fileReader, growlService) {
            $scope.storeID = $stateParams.storeID;
            $scope.sessionID = userService.getUserToken();
            $scope.storeItemGRNArray = [];

            $scope.handleDate = function (date) {
                return new moment(date).format("DD-MMM-YYYY");
            }

            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.selectGrnDetails = function (grnDetails) {
                $state.go("storegrndetails", { "storeID": $scope.storeID, "grnCode": grnDetails.grnCode, "grnObj": grnDetails });
            }

            $scope.getCompanyStoreItemsGRN = function () {
                storeService.getStoreItemGRN('', $scope.storeID)
                    .then(function (response) {
                        $scope.storeItemGRNArray = response;
                        $log.info($scope.storeItemGRNArray);
                    });
            };

            $scope.getCompanyStoreItemsGRN();
        }]);