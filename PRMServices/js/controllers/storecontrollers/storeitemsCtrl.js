﻿prmApp
    .controller('storeitemsCtrl', ["$scope", "$state", "$stateParams", "$log", "$window", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $log, $window, userService, storeService, fileReader, growlService) {
            $scope.storeID = $stateParams.storeID;
            $scope.itemID = $stateParams.itemID;
            $scope.storeObj = $stateParams.storeObj

            $scope.sessionID = userService.getUserToken();

            $scope.companyStoreItems = [];
            $scope.selectedStoreItems = [];
            $scope.companyStoreItemsTemp = [];

            $scope.searchKey = '';

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            }

            if (!$scope.storeObj) {
                $scope.storeObj = {
                    storeID: $scope.storeID,
                    companyID: $scope.companyID,
                    sessionID: $scope.sessionID
                };
            }
            else {
                $scope.storeObj.sessionID = $scope.sessionID;
            }

            $scope.goToStores = function () {
                $state.go("stores");
            }

            $scope.storeItemsObj = {
                storeDetails: $scope.storeObj
            };

            $scope.itemSelect = function (storeItem) {
                //if (storeItem.isSelected)
                //{
                //    $scope.selectedStoreItems.push(storeItem);
                //}
                //else
                //{
                //    $scope.selectedStoreItems = _.filter($scope.selectedStoreItems, function (item) {
                //        return item.itemID !== storeItem.itemID
                //    });
                //}

                $scope.selectedStoreItems = _.filter($scope.companyStoreItemsTemp, function (item) {
                    return item.isSelected == true;
                });
            };

            $scope.entityObj = {
                entityName: 'StoreItems',
                attachment: [],
                userid: userService.getUserId(),
                sessionID: $scope.sessionID
            };

            $scope.getCompanyStoreItems = function () {
                storeService.getcompanystoreitems($scope.storeID, $scope.itemID)
                    .then(function (response) {
                        $scope.companyStoreItems = response;
                        $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                            return item.itemCode.indexOf($scope.searchKey) > -1;
                        });

                    })
            }

            $scope.getCompanyStoreItems();

            $scope.search = function () {
                $scope.companyStoreItemsTemp = _.filter($scope.companyStoreItems, function (item) {
                    return (item.itemCode.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1 || item.itemType.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1 || item.itemDescription.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1 || item.itemName.toLowerCase().indexOf($scope.searchKey.toLowerCase()) > -1);
                });
            }

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'StoreItems',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql.fn.getstoreid = function (x) {
                    return x.storeID;
                }

                alasql('SELECT itemID as [ITEM_ID], getstoreid(storeDetails) as [STORE_ID], itemCode as [HSN_CODE], itemName as [SUB_CODE], itemType as [ITEM_TYPE], itemDescription as [ITEM_DESC], inStock as [IN_STOCK], onOrder as [ON_ORDER], itemPrice as [ITEM_PRICE], doAlert as [DO_ALERT], threshold as [THRESHOLD], iGST as [IGST], cGST as [CGST], sGST as [SGST], itemUnits as [ITEM_UNITS] INTO XLSX(?,{headers:true,sheetid: "StoreItems", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItems.xlsx", $scope.companyStoreItems]);
            };

            $scope.downloadTemplate = function () {
                var mystyle = {
                    sheetid: 'StoreItems',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                alasql.fn.getstoreid = function () {
                    return $scope.storeID;
                }

                alasql('SELECT itemID as [ITEM_ID], getstoreid() as [STORE_ID], itemCode as [HSN_CODE], itemName as [SUB_CODE], itemType as [ITEM_TYPE], itemDescription as [ITEM_DESC], inStock as [IN_STOCK], onOrder as [ON_ORDER], itemPrice as [ITEM_PRICE], doAlert as [DO_ALERT], threshold as [THRESHOLD], iGST as [IGST], cGST as [CGST], sGST as [SGST], itemUnits as [ITEM_UNITS] INTO XLSX(?,{headers:true,sheetid: "StoreItems", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["StoreItems.xlsx", $scope.storeItemsObj]);
            }


            $scope.importEntity = function () {

                var params = {
                    "entity": $scope.entityObj
                };

                storeService.importentity(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Items Saved Successfully.", "success");
                            location.reload();

                        }
                    })
            }

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.entityObj.attachment = $.makeArray(bytearray);
                            $scope.importEntity();
                        }
                    });
            }



            $scope.companyStoreItem = {};

            $scope.getCompanyStoreItem = function (itemID) {
                $scope.companyStoreItem = _.filter($scope.companyStoreItems, function (x) { return x.itemID == itemID; });
                $scope.singleItemDetails = $scope.companyStoreItem[0];
                return $scope.singleItemDetails;
            }


            $scope.goToItemEdit = function (storeitem) {
                $state.go("addnewstoreitem", { "storeID": storeitem.storeDetails.storeID, "itemID": storeitem.itemID, "itemObj": storeitem });
            }

            $scope.generategin = function () {
                if ($scope.selectedStoreItems.length > 0) {
                    $state.go("storegin", { "storeID": $scope.storeID, "itemsArr": $scope.selectedStoreItems });
                }
                else {
                    swal("Warning", "Please select items to create GIN", "warning");
                }
            }

            $scope.generategrn = function () {
                if ($scope.selectedStoreItems.length > 0) {
                    $state.go("storegrn", { "storeID": $scope.storeID, "itemsArr": $scope.selectedStoreItems });
                }
                else {
                    swal("Warning", "Please select items to create GRN", "warning");
                }
            }

            $scope.grnhistory = function () {
                $state.go("storegrnhistory", { "storeID": $scope.storeID });
            }

            $scope.ginhistory = function () {
                $state.go("storeginhistory", { "storeID": $scope.storeID });
            }


            $scope.deletestoreitem = function (storeItemId) {

                var params = {
                    storeItemId: storeItemId,
                    sessionID: $scope.sessionID
                };

                storeService.deletestoreitem(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Item Deleted Successfully.", "success");
                            location.reload();
                        }
                    })
            }


        }]);