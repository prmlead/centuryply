﻿prmApp
    .controller('storesCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "storeService", "growlService",
        function ($scope, $state, $log, $stateParams, userService, storeService, growlService) {
            $scope.compID = userService.getUserCompanyId();
            $scope.storeID = 0;
            if ($stateParams.storeID) {
                $scope.storeID = $stateParams.storeID;
            }

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            }

            $log.info($scope.storeID);
            $scope.companyStores = [];

            $scope.sessionID = userService.getUserToken();

            $scope.isCompanyStoresLoaded = false;

            $scope.getCompanyStores = function () {
                storeService.getcompanystores($scope.compID, $scope.storeID)
                    .then(function (response) {
                        $scope.companyStores = response;
                        $scope.isCompanyStoresLoaded = true;
                    });
            }

            $scope.getCompanyStores();

            $scope.goToStoreEdit = function (store) {
                $state.go("addnewstore", { "storeID": store.storeID, "storeObj": store, "companyStores": $scope.companyStores });
            }

            $scope.goToStoreInventory = function (store) {
                $state.go("storeitems", { "storeID": store.storeID, "storeObj": store, "itemID": 0 });
            }

            $scope.goToBranches = function (store) {
                var url = $state.href('branches', { "storeID": store.storeID });
                window.open(url, '_blank');
                //$state.go("branches", { "storeID": store.storeID });
            }

            $scope.deletestore = function (storeId) {

                var params = {
                    "storeId": storeId,
                    sessionID: $scope.sessionID
                };

                storeService.deletestore(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Store Deleted Successfully.", "success");
                            location.reload();
                        }
                    })
            }


        }]);