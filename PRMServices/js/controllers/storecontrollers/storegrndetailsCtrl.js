﻿prmApp
    .controller('storegrndetailsCtrl', ["$scope", "$state", "$stateParams", "$window", "$log", "$timeout", "userService", "storeService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $window, $log, $timeout, userService, storeService, fileReader, growlService) {
            $scope.storeID = $stateParams.storeID;
            $scope.grnCode = $stateParams.grnCode;
            $scope.grnObject = $stateParams.grnObj;
            $scope.sessionID = userService.getUserToken();
            $scope.storeItemGRNItemDetails = [];
            $scope.indentItemDetails = [];
            $scope.indentDetails = [];
            $scope.doPrint = false;

            $scope.handleDate = function (date) {
                return new moment(date).format("DD-MMM-YYYY");
            }

            $scope.cancelClick = function () {
                $window.history.back();
            }

            $scope.printReport = function () {
                $scope.doPrint = true;
                $timeout(function () {
                    $window.print();
                    $scope.doPrint = false
                }, 1000);
            }

            $scope.selectGrnItemDetails = function (grnDetails) {
                if ($scope.indentDetails.length <= 0) {
                    storeService.getIndentItemDetails($scope.storeID, $scope.grnCode, '')
                        .then(function (response) {
                            $scope.indentDetails = response;
                        });
                }
                else {
                    $scope.indentItemDetails = _.filter($scope.indentDetails, function (item) {
                        return item.itemID == grnDetails.itemID;
                    });
                }
            }

            $scope.getCompanyStoreItemsGRN = function () {
                storeService.getStoreItemGRN($scope.grnCode, $scope.storeID)
                    .then(function (response) {
                        $scope.storeItemGRNItemDetails = response[0];
                    });

                storeService.getIndentItemDetails($scope.storeID, '', $scope.grnCode)
                    .then(function (response) {
                        $scope.indentDetails = response;
                    });
            };

            $scope.getCompanyStoreItemsGRN();
        }]);