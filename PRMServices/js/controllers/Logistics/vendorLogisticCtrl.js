﻿prmApp

    // #region =========================================================================
    // AUCTION ITEM
    // #endregion =========================================================================

    .controller('vendorLogisticCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state", "$timeout", "auctionsService",
        "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog", "reportingService", "logisticServices", "logisticsHubName", "$dsb",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, logisticServices, logisticsHubName, $dsb) {

            // #region Initializers

            var liveId = "";

            var id = $scope.reqId = $stateParams.Id;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

            

            $scope.signalRCustomerAccess = false;
            $scope.Loding = false;
            $scope.makeaBidLoding = false;
            $scope.showTimer = false;
            $scope.userIsOwner = false;
            $scope.userID = userService.getUserId();

            $scope.sessionid = userService.getUserToken();
            $scope.allItemsSelected = true;
            $scope.RevQuotationfirstvendor = "";
            $scope.disableBidButton = false;

            $scope.nonParticipatedMsg = '';
            $scope.quotationRejecteddMsg = '';
            $scope.quotationNotviewedMsg = '';
            $scope.revQuotationRejecteddMsg = '';
            $scope.revQuotationNotviewedMsg = '';
            $scope.quotationApprovedMsg = '';
            $scope.revQuotationApprovedMsg = '';

            $scope.reduceBidAmountNote = '';
            $scope.incTaxBidAmountNote = '';
            $scope.noteForBidValue = '';
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.NegotiationEnded = false;


            $scope.currentID = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };
            
            $scope.bidAttachement = [];
            $scope.bidAttachementName = "";
            $scope.bidAttachementValidation = false;
            $scope.bidPriceEmpty = false;
            $scope.bidPriceValidation = false;
            $scope.showStatusDropDown = false;
            $scope.vendorInitialPrice = 0;
            $scope.showGeneratePOButton = false;
            $scope.isDeleted = false;
            $scope.ratingForVendor = 0;
            $scope.ratingForCustomer = 0;
            $scope.participatedVendors = [];
            $scope.vendorApprovals = [];
            var requirementHub;

            $scope.auctionItem = {
                auctionVendors: [],
                listRequirementItems: [],
                listRequirementTaxes: []
            }

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.auctionItemVendor = [];

            var requirementData = {};

            $scope.quotationAttachment = null;

            $scope.days = 0;
            $scope.hours = 0;
            $scope.mins = 0;

            $scope.NegotiationSettings = {
                negotiationDuration: ''
            };

            $scope.divfix = {};
            $scope.divfix3 = {};
            $scope.divfix1 = {};
            $scope.divfix2 = {};
            $scope.divfixMakeabid = {};
            $scope.divfixMakeabidError = {};
            $scope.boxfix = {};

            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.priceCapValue = 0;
            $scope.auctionItem.isUnitPriceBidding = 1;

            $scope.newPriceToBeQuoted = 0;

            $scope.formRequest = {
                selectedVendor: {},
                priceCapValue: 0,
                priceCapValueMsg: ''
            };

            $scope.makeABidDisable = false;

            $scope.vendorBidPrice = 0;
            $scope.revvendorBidPrice = 0;
            $scope.auctionStarted = true;
            $scope.customerBtns = true;
            $scope.showVendorTable = true;
            $scope.quotationStatus = true;
            $scope.toprankerName = "";
            $scope.vendorRank = 0;
            $scope.quotationUrl = "";
            $scope.revquotationUrl = "";
            $scope.vendorBtns = false;
            $scope.vendorQuotedPrice = 0;
            $scope.startBtns = false;
            $scope.commentsvalidation = false;
            $scope.enableMakeBids = false;
            $scope.price = "";
            $scope.startTime = '';

            $scope.priceSwitch = 0;

            $scope.poDetails = {};

            $scope.bidHistory = {};

            $scope.vendorID = 0;

            $scope.validity = '';

            $scope.warranty = 'As Per OEM';
            $scope.duration = $scope.auctionItem.deliveryTime;
            $scope.payment = $scope.auctionItem.paymentTerms;


            $scope.isQuotationRejected = -1;

            $scope.starttimecondition1 = 0;
            $scope.starttimecondition2 = 0;

            $scope.revQuotationUrl1 = 0;
            $scope.L1QuotationUrl = 0;


            $scope.notviewedcompanynames = '';
            $scope.starreturn = false;

            $scope.customerListRequirementTerms = [];
            $scope.customerDeliveryList = [];
            $scope.customerPaymentlist = [];

            $scope.listTerms = [];

            $scope.listRequirementTerms = [];

            $scope.deliveryRadio = false;
            $scope.deliveryList = [];

            $scope.paymentRadio = false;
            $scope.paymentlist = [];

            $scope.AmountSaved = 0;

            $scope.updatedeliverydateparams = {
                date: ''
            };

            $scope.updatepaymentdateparams = {
                date: ''
            };

            $scope.Loding = false;

            $scope.priceCapError = false;

            $scope.makeaBidLoding = false;

            $scope.vendorsFromPRM = 1;
            $scope.vendorsFromSelf = 2;
            
            $scope.totalprice = 0;
            $scope.taxs = 0;
            $scope.vendorTaxes = 0;
            $scope.freightcharges = 0;
            $scope.discountAmount = 0;
            $scope.totalpriceinctaxfreight = 0;

            $scope.vendorBidPriceWithoutDiscount = 0;

            $scope.revtotalprice = 0;
            $scope.revtaxs = 0;
            $scope.revvendorTaxes = $scope.vendorTaxes;
            $scope.revfreightcharges = 0;
            $scope.revtotalpriceinctaxfreight = 0;

            $scope.priceValidationsVendor = '';

            $scope.calculatedSumOfAllTaxes = 0;

            $scope.taxValidation = false;

            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;

            $scope.gstValidation = false;

            $scope.taxEmpty = false;


            $scope.rejectreson = '';
            $scope.rejectresonValidation = false;

            $scope.QuatationAprovelvalue = true;
            $scope.submitButtonShow = 0;

            $scope.NegotiationSettingsValidationMessage = '';

            if ($scope.isCustomer) {
                auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }
                    });
            }

            $scope.airline = {
                quotationItemID: 0,
                itemID: 0,
                vendorID: $scope.userID,
                requirementID: 0,
                airline: '',
                routing: '',
                transit: '',

                unitTariff: 0,
                unitPrice: 0,
                revUnitPrice: 0,
                fsc: 0,
                ssc: 0,
                misc: 0,
                otherCharges: 0,
                revOtherCharges: 0,
                xRay: 0,
                terminalHandling: 0,
                customClearance: 0,
                revCustomClearance: 0,
                serviceCharges: 0,
                revServiceCharges: 0,
                ams: 0,
                awbFees: 0,
                cgFees: 0,
                forwordDgFees: 0,

                itemPrice: 0,
                revitemPrice: 0,
                isDeleted: 0,
                isEnabled: true,
                sessionID: $scope.sessionid
            };

            $scope.deletedAirlines = [];

            $scope.quotationFieldsDisableOrHide = false;
            $scope.revQuotationFieldsDisableOrHide = false;

            $scope.showMinimized = false;
            $scope.showMinimizedStyle = {
                'max-height': '800px', 'overflow': 'auto'
            };



            $scope.tempRevTotalPrice = 0;
            $scope.enableAll = true;

            $scope.previousBidPrice = 0;

            $scope.makeBidError = '';

            $scope.disableSubmitQuotation = false;

            // #endregion Initializers

            // #region Functions


            $scope.handlePrecision = function (item, precision) {
                $scope.auctionItemVendor.auctionVendors[0].initialPrice = $scope.precisionRound($scope.auctionItemVendor.auctionVendors[0].initialPrice, precision);
                $scope.auctionItemVendor.auctionVendors[0].revPrice = $scope.precisionRound($scope.auctionItemVendor.auctionVendors[0].revPrice, precision);
                $scope.auctionItemVendor.auctionVendors[0].runningPrice = $scope.precisionRound($scope.auctionItemVendor.auctionVendors[0].runningPrice, precision);
                $scope.auctionItemVendor.auctionVendors[0].totalRunningPrice = $scope.precisionRound($scope.auctionItemVendor.auctionVendors[0].totalRunningPrice, precision);
                $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice = $scope.precisionRound($scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice, precision);
                return item;
            };

            $scope.precisionRound = function (number, precision) {
                number = parseFloat(number);
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            }


            $scope.getData = function (methodName, callerID) {

                var params = {
                    reqid: $stateParams.Id,
                    sessionid: userService.getUserToken(),
                    userid: userService.getUserId()
                }

                logisticServices.GetRequirementData(params)
                    .then(function (response) {
                        $scope.setAuctionInitializer(response, methodName, callerID);
                    });
            };

            $scope.setAuctionInitializer = function (response, methodName, callerID) {
                $scope.auctionItem = response;

                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    item.quotationStatus = '';
                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        if (item.isQuotationRejected < 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'Quotation status: PENDING';
                        }
                        else if (item.isQuotationRejected == 0) {
                            item.quotationStatusStyle = { 'color': '#67bd6a' };
                            item.quotationStatus = 'Quotation status: SELECTED FOR NEGOTIATION';
                        }
                        else if (item.isQuotationRejected > 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'Quotation status: REJECTED';
                        }
                    }
                    if ($scope.auctionItem.status == 'Negotiation Ended') {
                        if (item.isRevQuotationRejected < 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'Revised Quotation status: PENDING';
                        }

                        if (item.initialPrice == 0) {
                            item.initialPrice = 0;
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'You have missed an opportunity to participate in this Negotiation.';
                        }

                        else if (item.isRevQuotationRejected == 0) {
                            item.quotationStatusStyle = { 'color': '#67bd6a' };
                            item.quotationStatus = 'Revised Quotation status: SELECTED FOR NEGOTIATION';
                        }
                        else if (item.isRevQuotationRejected > 0) {
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'Revised Quotation status: REJECTED';
                        }
                    }

                    if ($scope.auctionItem.status == 'STARTED') {

                        if (item.initialPrice == 0) {
                            item.initialPrice = 0;
                            item.quotationStatusStyle = { 'color': '#e62739' };
                            item.quotationStatus = 'You have missed an opportunity to participate in this Negotiation.';
                        }
                    }
                    
                })

               
                

                $scope.auctionItem.auctionVendors[0].listReqItems.forEach(function (reqitem, reqindex) {
                    reqitem.listQuotationItems.forEach(function (reqitem1, reqindex1) {
                        reqitem1.TemperoryRevUnitPrice = 0;
                        reqitem1.TemperoryRevUnitPrice = reqitem1.revUnitPrice;

                        reqitem1.TemperoryRevOtherCharges = 0;
                        reqitem1.TemperoryRevOtherCharges = reqitem1.revOtherCharges;
                    })
                   
                })

                if (!$scope.auctionItem.multipleAttachments) {
                    $scope.auctionItem.multipleAttachments = [];
                }
                $scope.auctionItem.attFile = response.attachmentName;
                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {


                    var attchArray = $scope.auctionItem.attFile.split(',');

                    attchArray.forEach(function (att, index) {

                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };

                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    })

                }



                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0
                    && $scope.auctionItem.auctionVendors[0].revVendorTotalPrice > 0)
                {
                    $scope.previousBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                }

                if ((callerID == userService.getUserId() || callerID == undefined) && methodName != 'MAKE_BID') {
                    $scope.auctionItemVendor = $scope.auctionItem;

                    $scope.tempRevVendorTotalPrice = $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice;

                    if ($scope.auctionItemVendor.listRequirementItems != null) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item.TemperoryRevUnitPrice = 0;
                           item.TemperoryRevUnitPrice = item.revUnitPrice;
                        });
                    }

                    // #region Conditions

                    $scope.quotationFieldsDisableOrHide = false;
                    $scope.revQuotationFieldsDisableOrHide = false;

                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.revQuotationFieldsDisableOrHide = true;

                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                            $scope.quotationFieldsDisableOrHide = true;
                        }
                    }
                    else
                        if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') {
                            $scope.quotationFieldsDisableOrHide = true;

                            if ($scope.auctionItem.auctionVendors[0].isRevQuotationRejected == 0) {
                                $scope.revQuotationFieldsDisableOrHide = true;
                            }
                            else {
                                $scope.revQuotationFieldsDisableOrHide = false;
                            }
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.showMinimized = true;
                                $scope.showMinimizedStyle = {
                                    'max-height': '300px', 'overflow': 'auto'
                                };
                            }
                        }
                        else {
                            $scope.quotationFieldsDisableOrHide = true;
                        }


                    // #endregion Conditions

                }

                $scope.notviewedcompanynames = '';
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                        $scope.Loding = false;
                        $scope.notviewedcompanynames += item.companyName + ', ';
                        $scope.starreturn = true;
                    }
                })
                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);
                $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;
                $scope.NegotiationSettings = $scope.auctionItem.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);
                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                if (!response.auctionVendors || response.auctionVendors.length == 0) {
                    $scope.auctionItem.auctionVendors = [];
                    $scope.auctionItem.description = "";
                }
                $scope.auctionItem.quotationFreezTime = new moment($scope.auctionItem.quotationFreezTime).format("DD-MM-YYYY HH:mm");
                $scope.auctionItem.expStartTime = new moment($scope.auctionItem.expStartTime).format("DD-MM-YYYY HH:mm");
                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) {
                    $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                    $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;
                    $scope.freightcharges = $scope.auctionItem.auctionVendors[0].vendorFreight;
                    $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                    $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                    $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                    $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                    $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                    $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                    $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties;
                    $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                    $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;
                    if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                        $scope.validity = '1 Day';
                    }
                    else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                        $scope.validity = '2 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)' && $scope.validity == '') {
                        $scope.validity = '5 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)' && $scope.validity == '') {
                        $scope.validity = '10-15 Days';
                    }
                    if ($scope.warranty == '') {
                        $scope.warranty = 'As Per OEM';
                    }
                    if ($scope.duration == '') {
                        $scope.duration = $scope.auctionItem.deliveryTime;
                    }
                    if ($scope.payment == '') {
                        $scope.payment = $scope.auctionItem.paymentTerms;
                    }

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                        item.Gst = item.cGst + item.sGst + item.iGst;
                       
                        
                        if (!item.selectedVendorID || item.selectedVendorID == 0) {
                            $scope.allItemsSelected = false;
                        }
                        if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                            if (item.selectedVendorID > 0) {
                                var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                if (vendorArray.length > 0) {
                                    item.selectedVendor = vendorArray[0];
                                }
                            } else {
                                item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                            }
                        }
                    })
                    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                        $scope.disableAddButton = false;
                    }
                    $scope.revfreightcharges = $scope.auctionItem.auctionVendors[0].revVendorFreight;
                    $("#revfreightcharges").val($scope.auctionItem.auctionVendors[0].revVendorFreight);
                    $scope.TemperoryRevfreightcharges = $scope.revfreightcharges;
                    if (callerID == userService.getUserId() || callerID == undefined) {
                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;
                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                    }
                    $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;
                    if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                    }
                    $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.revquotationUrl != '') {
                            $scope.RevQuotationfirstvendor = item.revquotationUrl;
                        }
                    })
                }
                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                    $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                }
                $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                });
                $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                })
                $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                })
                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");
                var id = parseInt(userService.getUserId());
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == id;
                });
                if (id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();
                }
                $scope.quotationApprovedColor = {};
                $scope.quotationNotApprovedColor = {};
                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 && $scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationNotApprovedColor = {};
                } else if ($scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationNotApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationApprovedColor = {};
                }

                $scope.GetTotal();

            };

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                } else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;

                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                } else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                } else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                } else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.GetDateLogistics()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        $log.debug(dateFromServer);
                        var curDate = dateFromServer;
                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;

                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                        var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                        var newDate = new Date(parseInt(parseInt(date)));
                        $scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                        $scope.postedOnForExcel = new moment($scope.auctionItem.postedOn).format("DD-MM-YYYY HH:mm");
                        $scope.startTimeForExcel = new moment($scope.auctionItem.startTime).format("DD-MM-YYYY HH:mm");
                        $scope.endTimeForExcel = new moment($scope.auctionItem.endTime).format("DD-MM-YYYY HH:mm");

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i]

                            if (vendor.vendorID == userService.getUserId() && vendor.quotationUrl == "") {
                                $scope.quotationStatus = false;
                                $scope.quotationUploaded = false;
                            } else {
                                $scope.quotationStatus = true;
                                if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                                    $scope.quotationUploaded = true;
                                }
                                $scope.quotationUrl = vendor.quotationUrl;
                                $scope.revquotationUrl = vendor.revquotationUrl;
                                $scope.vendorQuotedPrice = vendor.runningPrice;
                            }
                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if (userService.getUserId() == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 0;
                                $scope.nonParticipatedMsg = 'You have missed an opportunity to participate in this Negotiation.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation submitted to the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1) {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1) {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation submitted to the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                $scope.quotationApprovedMsg = 'Your Quotation Selected by the customer.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Selected by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },
                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be reduce from bid Amount.';
                $scope.incTaxBidAmountNote = '';
                $scope.noteForBidValue = '';

                if ($scope.auctionItem.status == "STARTED") {
                    liveId = "live";
                }
                else {
                    liveId = "";
                }

            };

            $scope.disableButtons = function () {
                $scope.buttonsDisabled = true;
            }

            $scope.AddAirLine = function (reqItem, quotationItem)
            {
                $scope.auctionItemVendor.auctionVendors[0].listReqItems.forEach(function (item1, index1) {
                    

                    $scope.airline = {
                        quotationItemID: 0,
                        itemID: 0,
                        vendorID: 0,
                        requirementID: 0,
                        airline: '',
                        routing: '',
                        transit: '',

                        unitTariff: 0,
                        unitPrice: 0,
                        revUnitPrice: 0,
                        fsc: 0,
                        ssc: 0,
                        misc: 0,
                        otherCharges: 0,
                        revOtherCharges: 0,
                        xRay: 0,
                        terminalHandling: 0,
                        customClearance: 0,
                        revCustomClearance: 0,
                        serviceCharges: 0,
                        revServiceCharges: 0,
                        ams: 0,
                        awbFees: 0,
                        cgFees: 0,
                        forwordDgFees: 0,
                       
                        itemPrice: 0,
                        revitemPrice: 0,
                        isDeleted: 0,
                        isEnabled: true,
                        sessionID: $scope.sessionid
                    };

                    if (reqItem.itemID == item1.itemID) {

                        $scope.airline.itemID = reqItem.itemID;
                        $scope.airline.vendorID = $scope.userID;
                        $scope.airline.requirementID = $scope.reqId;
                        $scope.airline.sessionID = $scope.sessionid;

                        item1.listQuotationItems.push($scope.airline);
                    }
                })
            }

            $scope.DeleteAirLine = function (reqItem, quotationItem, index) {
                $scope.auctionItemVendor.auctionVendors[0].listReqItems.forEach(function (item1, index1) {
                    if (reqItem.itemID == item1.itemID) {

                        if (quotationItem.quotationItemID > 0)
                        {
                            $scope.deletedAirlines.push(quotationItem);
                            $log.info($scope.deletedAirlines);
                        }

                        item1.listQuotationItems = item1.listQuotationItems.filter(function (param, IndexParam) {
                            return (IndexParam != index);
                        });                        
                    }
                })
            }

            $scope.SubmitQuotation = function (val) {

                if ($scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice > 0) {
                    $scope.disableSubmitQuotation = true;
                    auctionsService.GetDateLogistics()
                        .then(function (responseFromServer) {
                            var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                            var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                            var timeofauctionstart = auctionStart.getTime();
                            var currentServerTime = dateFromServer.getTime();

                            if (timeofauctionstart - currentServerTime >= 3600000) {

                            }
                            else if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                                growlService.growl("You have missed the deadline for Quotation Submission", "inverse");
                                $scope.disableSubmitQuotation = false;
                                return;
                            }

                            

                            if (val == 0) {
                                var vendorDetails = {
                                    'requirementID': $scope.reqId,
                                    'vendorID': $scope.userID,
                                    'warranty': $scope.warranty,
                                    'validity': $scope.validity,
                                    'otherProperties': $scope.otherProperties,
                                    'duration': $scope.duration,
                                    'payment': $scope.payment,
                                    'revVendorTotalPrice': 0,
                                    'isAcceptedTC': $scope.auctionItemVendor.auctionVendors[0].isAcceptedTC,
                                    'isSent': val
                                };
                            }
                            else {
                                var vendorDetails = {
                                    'requirementID': $scope.reqId,
                                    'vendorID': $scope.userID,
                                    'warranty': $scope.warranty,
                                    'validity': $scope.validity,
                                    'otherProperties': $scope.otherProperties,
                                    'duration': $scope.duration,
                                    'payment': $scope.payment,
                                    'revVendorTotalPrice': $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice,
                                    'isAcceptedTC': $scope.auctionItemVendor.auctionVendors[0].isAcceptedTC,
                                    'isSent': val
                                };
                            }

                            var isRevised = false;

                            if ($scope.auctionItem.status == 'Negotiation Ended') {
                                isRevised = true
                            };


                            var params = {
                                'listLogisticReqItems': $scope.auctionItemVendor.auctionVendors[0].listReqItems,
                                'listDeletedQuotationItems': $scope.deletedAirlines,
                                'vendorDetails': vendorDetails,
                                'sessionID': $scope.sessionid,
                                'isRevised': isRevised
                            };

                            logisticServices.SubmitQuotation(params)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        swal({
                                            title: "Done!",
                                            text: 'Saved Successfully',
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                $scope.disableSubmitQuotation = false;
                                                location.reload();
                                            });
                                    } else {
                                        $scope.disableSubmitQuotation = false;
                                        swal("Error!", req.errorMessage, "error");
                                    }
                                });
                        })
                }
                else {
                    growlService.growl("Please enter valid quotation price", "inverse");
                    $scope.disableSubmitQuotation = false;
                    return;
                }
                
            }

            $scope.recalculate = function () {

                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                params.methodName = 'REFRESH';

                logisticServices.signalRFrontEnd(params)
                    .then(function (response) {
                        if (response.signalRFrontEndResult.errorMessage == '' || response.signalRFrontEndResult.errorMessage == null) {
                            swal("Done!", "A refresh command has been sent to everyone.", "success");
                        } else {
                            swal("Error!", response.StopBidsResult.errorMessage, "error");
                        }
                    });

            }

            $scope.MakeBid = function () {

                $scope.makeBidError = '';
                var bidPrice = $("#makebidvalue").val();

                if ($scope.previousBidPrice - $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice <= 0)
                {
                    $scope.makeBidError = 'Please reduce more than 0';
                    growlService.growl($scope.makeBidError, "inverse");
                    $scope.getData();
                    return;
                }
                else if ($scope.previousBidPrice <= $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice) {
                    $scope.makeBidError = 'Bid price: ' + $scope.previousBidPrice + ' should be less than previous Bid: ' + $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice;
                    growlService.growl($scope.makeBidError, "inverse");
                    $scope.getData();
                    return;
                }
                else
                if (($scope.previousBidPrice - $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice) < $scope.auctionItem.minBidAmount) {
                    $scope.makeBidError = 'Bid price: ' + $scope.previousBidPrice + ' should be less than min Bid amount ' + $scope.auctionItem.minBidAmount;
                    growlService.growl($scope.makeBidError, "inverse");
                        $scope.getData();
                        return;
                }
                if ($scope.auctionItemVendor.auctionVendors[0].runningPrice < (75 * $scope.previousBidPrice / 100)) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Maximum Reduction Error!", " You are reducing more than 25% of current bid amount. The Maximum reduction amount should not exceed more than 25% from current bid amount  " + (25 * $scope.previousBidPrice / 100) + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                    $scope.makeABidDisable = false;
                    return false;
                }


                else {
                var vendorDetails = {
                    'requirementID': $scope.reqId,
                    'vendorID': $scope.userID,
                    'warranty': $scope.warranty,
                    'validity': $scope.validity,
                    'otherProperties': $scope.otherProperties,
                    'duration': $scope.duration,
                    'payment': $scope.payment,
                    'revVendorTotalPrice': $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice
                };

                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                params.bidPrice = $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice;
                params.listRequirementItems = $scope.auctionItemVendor.auctionVendors[0].listReqItems;
                params.vendorDetails = vendorDetails;

                logisticServices.MakeBid(params)
                    .then(function (response) {
                        if (response.errorMessage == '' || response.errorMessage == null) {
                            //$scope.SubmitQuotation('MAKE_BID');                            
                        } else {                            
                            //swal("Error!", response.StopBidsResult.errorMessage, "error");
                            growlService.growl(response.errorMessage, "success");

                        }
                    });
                    }
            }

            $scope.changeShowMinimizedStyle = function (showMinimized) {
                if (showMinimized) {
                    $scope.showMinimizedStyle = {
                        'max-height': '300px', 'overflow': 'auto'
                    };
                }
                else {
                    $scope.showMinimizedStyle = {
                        'max-height': '800px', 'overflow': 'auto'
                    };
                }
            }

            $scope.enableOrDisable = function (enableAll) {
                $scope.auctionItemVendor.auctionVendors[0].listReqItems.forEach(function (RI, indexRI) {
                    RI.isEnabled = enableAll;
                })
            }



            $scope.calculateVal = function (val, valType, qty) {

                var result = 0;


                if (valType > 0 && val > 0 && qty > 0) {
                    result = parseFloat(parseFloat(val) * parseFloat(qty));
                }
                else if (val > 0 && qty > 0) {
                    result = parseFloat(val)
                }
                else {
                    result = 0;
                }

                return result;

            }


           

            $scope.GetTotal = function () {                

                $scope.auctionItemVendor.auctionVendors[0].initialPrice = 0;
                $scope.auctionItemVendor.auctionVendors[0].revPrice = 0;

                $scope.auctionItemVendor.auctionVendors[0].runningPrice = 0;
                $scope.auctionItemVendor.auctionVendors[0].totalRunningPrice = 0;
                $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice = 0;


                $scope.auctionItemVendor.auctionVendors[0].listReqItems.forEach(function (item, itemIndex) {
                    item.listQuotationItems.forEach(function (airline, airlineIndex) {

                        airline.revUnitPriceType = airline.unitPriceType;
                        airline.revOtherChargesType = airline.otherChargesType;
                        airline.revCustomClearanceType = airline.customClearanceType;
                        airline.revServiceChargesType = airline.serviceChargesType;


                        var unitTariff = $scope.calculateVal(airline.unitTariff, airline.unitTariffType, item.productQuantity);
                        var unitPrice = $scope.calculateVal(airline.unitPrice, airline.unitPriceType, item.productQuantity);
                        var revUnitPrice = $scope.calculateVal(airline.revUnitPrice, airline.revUnitPriceType, item.productQuantity);
                        var fsc = $scope.calculateVal(airline.fsc, airline.fscType, item.productQuantity);
                        var ssc = $scope.calculateVal(airline.ssc, airline.sscType, item.productQuantity);
                        var misc = $scope.calculateVal(airline.misc, airline.miscType, item.productQuantity);
                        var otherCharges = $scope.calculateVal(airline.otherCharges, airline.otherChargesType, item.productQuantity);
                        var revOtherCharges = $scope.calculateVal(airline.revOtherCharges, airline.revOtherChargesType, item.productQuantity);
                        var xRay = $scope.calculateVal(airline.xRay, airline.xRayType, item.productQuantity);
                        var terminalHandling = $scope.calculateVal(airline.terminalHandling, airline.terminalHandlingType, item.productQuantity);
                        var customClearance = $scope.calculateVal(airline.customClearance, airline.customClearanceType, item.productQuantity);
                        var revCustomClearance = $scope.calculateVal(airline.revCustomClearance, airline.revCustomClearanceType, item.productQuantity);
                        var serviceCharges = $scope.calculateVal(airline.serviceCharges, airline.serviceChargesType, item.productQuantity);
                        var revServiceCharges = $scope.calculateVal(airline.revServiceCharges, airline.revServiceChargesType, item.productQuantity);
                        var ams = $scope.calculateVal(airline.ams, airline.amsType, item.productQuantity);
                        var awbFees = $scope.calculateVal(airline.awbFees, airline.awbFeesType, item.productQuantity);
                        var cgFees = $scope.calculateVal(airline.cgFees, airline.cgFeesType, item.productQuantity);
                        var forwordDgFees = $scope.calculateVal(airline.forwordDgFees, airline.forwordDgFeesType, item.productQuantity);

                        

                        if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                            airline.revUnitPrice = airline.unitPrice;
                            airline.revServiceCharges = airline.serviceCharges;
                            airline.revOtherCharges = airline.otherCharges;
                            airline.revCustomClearance = airline.customClearance;

                            revUnitPrice = unitPrice;
                            revServiceCharges = serviceCharges;
                            revOtherCharges = otherCharges;
                            revCustomClearance = customClearance;
                        }


                        airline.itemPrice = unitPrice + fsc + ssc + misc
                                            + otherCharges + xRay + terminalHandling + customClearance + serviceCharges
                            + ams + awbFees + cgFees + forwordDgFees;


                        airline.revitemPrice = revUnitPrice + fsc + ssc + misc
                            + revOtherCharges + xRay + terminalHandling + revCustomClearance + revServiceCharges
                            + ams + awbFees + cgFees + forwordDgFees;



                        //var unitTariffTemp = parseFloat(parseFloat(airline.unitTariff) * parseFloat(item.productQuantity));
                        //var gst = airline.cGst + airline.sGst + airline.iGst;
                        //var miscTemp = parseFloat(parseFloat(airline.misc) * parseFloat(item.productQuantity));
                        //var xRayTemp = parseFloat((airline.xRay) * parseFloat(item.productQuantity));
                        //var fscTemp = parseFloat((airline.fsc) * parseFloat(item.productQuantity));
                        //var sscTemp = parseFloat((airline.ssc) * parseFloat(item.productQuantity));

                        //var unitPriceTemp = parseFloat(parseFloat(airline.unitPrice) * parseFloat(item.productQuantity));

                        //airline.itemPrice = parseFloat(airline.unitPrice) + parseFloat(airline.misc) + parseFloat(xRay) +
                        //    parseFloat(airline.serviceCharges) + parseFloat(airline.otherCharges) +
                        //    parseFloat(fscTemp) + parseFloat(sscTemp) + parseFloat(airline.hazCharges) + parseFloat(airline.customClearance) +
                        //    parseFloat(airline.cgFees) + parseFloat(airline.adc) + parseFloat(airline.awbFees) + parseFloat(airline.ediFees);

                        //airline.itemPrice = airline.itemPrice + ((airline.itemPrice / 100) * gst);


                        //if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        //    airline.revUnitPrice = airline.unitPrice;
                        //    airline.revServiceCharges = airline.serviceCharges;
                        //    airline.revOtherCharges = airline.otherCharges;
                        //}


                        //var revUnitPriceTemp = parseFloat(parseFloat(airline.revUnitPrice) * parseFloat(item.productQuantity));

                        //airline.revitemPrice = revUnitPriceTemp + miscTemp + xRayTemp + parseFloat(airline.revServiceCharges) + parseFloat(airline.revOtherCharges) +
                        //    parseFloat(fscTemp) + parseFloat(sscTemp) + parseFloat(airline.hazCharges) + parseFloat(airline.customClearance) +
                        //    parseFloat(airline.cgFees) + parseFloat(airline.adc) + parseFloat(airline.awbFees) + parseFloat(airline.ediFees);

                        //airline.revitemPrice = airline.revitemPrice + ((airline.revitemPrice / 100) * gst);

                        if (airlineIndex == 0) {
                        $scope.auctionItemVendor.auctionVendors[0].initialPrice += airline.itemPrice;
                        $scope.auctionItemVendor.auctionVendors[0].revPrice += airline.itemPrice;
                        $scope.auctionItemVendor.auctionVendors[0].runningPrice += airline.revitemPrice;
                        $scope.auctionItemVendor.auctionVendors[0].totalRunningPrice += airline.revitemPrice;
                        $scope.auctionItemVendor.auctionVendors[0].revVendorTotalPrice += airline.revitemPrice;

                        $scope.handlePrecision(0, 2);

                        }
                    })

                })


                


            }

            $scope.makeBidUnitPriceValidation = function (revPrice, TemperoryRevPrice, productIDorName) {
                if (TemperoryRevPrice < revPrice && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter price less than ' + TemperoryRevPrice + ' of ' + productIDorName);
                }

            }


            // #endregion Functions

            // #region Function Calls

            $scope.getData();

            // #endregion Function Calls

            // #region SignalR Setup

            var logisticsHub = SignalRFactory('', 'logisticsHub');

            $scope.checkConnection = function () {
                if (logisticsHub) {
                    return logisticsHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.reconnectHub = function () {
                if (logisticsHub) {
                    if (logisticsHub.getStatus() == 0) {
                        logisticsHub.reconnect();
                        return true;
                    }
                } else {
                    logisticsHub = SignalRFactory('', 'logisticsHub');
                }

            }

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    logisticsHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                } else {
                    $scope.reconnectHub();
                    logisticsHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                }
            }

            $scope.invokeSignalR('JoinGroup', 'logisticsHub' + $scope.reqId);

            $scope.signalRFrontEnd = function (methodName) {

                var params = {
                    'reqID': $scope.reqId,
                    'sessionID': $scope.sessionid,
                    'userID': $scope.customerID,
                    'methodName': methodName
                };

                logisticServices.signalRFrontEnd(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {

                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
            }

            // #endregion SignalR Setup

            // #region SignalR Responce

            logisticsHub.on('checkRequirement', function (req) {
                if (req.methodName == 'UPDATE_BID_TIME') {
                    req.auctionVendors = req.auctionVendors.filter(function (vendor) {
                        return vendor.vendorID == $scope.userID;
                    });
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                   // growlService.growl("Customer has updated negotiation time.", "success");
                    growlService.growl("Vendor has made a bid.", "success");
                }
                else
                if (req.methodName == 'STOP_BIDS') {
                        req.auctionVendors = req.auctionVendors.filter(function (vendor) {
                            return vendor.vendorID == $scope.userID;
                        });
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                    growlService.growl("Customer has updated negotiation time to 1 min, Negotiation will end in 1 min", "success");
                   // growlService.growl("Vendor has made a bid.", "success");
                }
                else
                if (req.methodName == 'RESTART_NEGOTIATION') {
                    location.reload();
                }
                else
                if (req.methodName == 'MAKE_BID')
                {
                    if (req.callerID == $scope.userID) {
                        growlService.growl("Bid Successfull.", "success");
                    }
                    req.auctionVendors = req.auctionVendors.filter(function (vendor) {
                        return vendor.vendorID == $scope.userID;
                    });
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                    
                }
                else
                {
                    req.auctionVendors = req.auctionVendors.filter(function (vendor) {
                        return vendor.vendorID == $scope.userID;
                    });
                    $scope.setAuctionInitializer(req, req.methodName, req.callerID);
                }
                
                $log.info('checkRequirement - $ignalR - ' + req.methodName);
            })

            // #endregion SignalR Responce

            // #region Countdown 

            $scope.$on('timer-tick', function (event, args) {
                $scope.countdownVal = event.targetScope.countdown;
                
                if (event.targetScope.countdown == 60 || event.targetScope.countdown == 59 || event.targetScope.countdown == 30 || event.targetScope.countdown == 29) {
                    //var msie = $(document) [0].documentMode;
                    var ua = window.navigator.userAgent;
                    var msieIndex = ua.indexOf("MSIE ");
                    var msieIndex2 = ua.indexOf("Trident");
                    // if is IE (documentMode contains IE version)
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    if (msieIndex > 0 || msieIndex2 > 0) {
                        $scope.getData();
                    }
                }
                if (event.targetScope.countdown <= 0) {
                    if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                        $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                    }
                    else if ($scope.auctionItem.status == "NOTSTARTED") {
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = userService.getUserId();
                        logisticServices.StartNegotiation(params)
                            .then(function (response) {
                                if (response.errorMessage == '') {

                                }
                            })
                        location.reload();
                    }
                }
                if (event.targetScope.countdown <= 120) {
                    $scope.timerStyle = {
                        'color': '#f00',
                        '-webkit - animation': 'flash linear 1s infinite',
                        'animation': 'flash linear 1s infinite'
                    };
                }

                //$scope.timerFloat = $('#reqTitle').visible();
                $scope.timerFloat = $('#reqTitle').show();

                if ($scope.auctionItem.status == 'STARTED' && !$scope.timerFloat) {
                    $scope.divfix = {
                        'bottom': '2%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfix3 = {
                        'bottom': '9%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfix1 = {
                        'bottom': '8%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfix2 = {
                        'bottom': '13%',
                        'left': '3%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfixMakeabid = {
                        'bottom': '14%',
                        'left': '10%',
                        'position': 'fixed',
                        'z-index': '3000'
                    };
                    $scope.divfixMakeabidError = {
                        'bottom': '13%',
                        'left': '18%',
                        'position': 'fixed',
                        'z-index': '3000',
                        'background-color': 'lightgrey'
                    };

                    if (!$scope.isCustomer) {
                        $scope.boxfix = {
                            'background-color': 'lightgrey',
                            'width': '280px',
                            'height': '120px',
                            'padding': '25px',
                            'margin': '25px',
                            'bottom': '-2%',
                            'left': '0%',
                            'position': 'fixed',
                            'z-index': '3000'
                        }
                    }
                    else {
                        $scope.boxfix = {
                            'background-color': 'lightgrey',
                            'width': '280px',
                            'height': '90px',
                            'padding': '25px',
                            'margin': '25px',
                            'bottom': '-2%',
                            'left': '0%',
                            'position': 'fixed',
                            'z-index': '3000'
                        }
                    }
                }
                else {
                    $scope.divfix = {};
                    $scope.divfix3 = {};
                    $scope.divfix1 = {};
                    $scope.divfix2 = {};
                    $scope.divfixMakeabid = {};
                    $scope.divfixMakeabidError = {};
                    $scope.boxfix = {};
                };
                if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') {
                    if ($scope.auctionItem.isUnitPriceBidding == 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                        $scope.divBorder = {
                            'background-color': '#f5b2b2'
                        };
                    }
                    if ($scope.auctionItem.isUnitPriceBidding == 0 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                        $scope.divBorder1 = {
                            'background-color': '#f5b2b2'
                        };
                    }
                }
                if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                    $scope.timerStyle = { 'color': '#000' };
                }
                if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                    // Vendor------
                    $scope.timerStyle = { 'color': '#228B22' };
                }
                if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                    $scope.disableDecreaseButtons = true;
                }
                if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                    $scope.disableDecreaseButtons = false;
                }
                if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                    $scope.showTimer = false;
                    window.location.reload();
                }
                if (event.targetScope.countdown < 1 && $scope.auctionItem.status == 'STARTED') {
                    $scope.showTimer = false;
                    $scope.auctionItem.status = 'Negotiation Ended';
                    swal({
                        title: "Thanks!",
                        text: "Negotiation complete! Thank you for being a part of this.",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                            $log.info("Negotiation Ended."); // vend
                        });
                }
                if (event.targetScope.countdown <= 3) {
                    $scope.disableAddButton = true;
                }
                if (event.targetScope.countdown < 2) {
                    $scope.disableBidButton = true;
                }
                if (event.targetScope.countdown > 3) {
                    $scope.disableAddButton = false;
                }
                if (event.targetScope.countdown >= 2) {
                    $scope.disableBidButton = false;
                }

            });

            // #endregion Countdown 

            //$scope.scrollTo = function () {
            //    $('html, body').animate({ scrollTop: $('#biddingTable').offset().top }, 'slow');
            //    return false;
            //}




            // Table Excel Export
            $scope.loadExcel = false;
            $scope.notLoadExcel = true;

            $scope.downloadExcel = function () {
                $scope.loadExcel = true;
                $scope.notLoadExcel = false;

                setTimeout(function () {
                    document.getElementById("downloadExcelButton").click();
                    $scope.$apply(function () {
                        $scope.loadExcel = false;
                        $scope.notLoadExcel = true;
                    });
                }, 1000)
            }
            // Table Excel Export

            



    }]);