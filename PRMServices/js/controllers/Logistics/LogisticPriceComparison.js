﻿prmApp.factory("LogisticPriceComparisonService", ['httpServices', 'logisticComparsion', function (httpServices, logisticComparsion) {

    var factory = {};

    factory.getComparsion = function (id) {
        return httpServices.get(logisticComparsion + "logistic/quotation_comparison/" + id)
    }

    factory.updateCharges = function (data) {
        return httpServices.post(logisticComparsion + "logistic/update_charges", data)
    }
    factory.updateCharges2 = function (data) {
        return httpServices.post(logisticComparsion + "logistic/update_charges_list", data)
    }

    return factory;

}])


prmApp.run(['$templateCache', function ($templateCache) {
    $templateCache.put('stCellAmount.html', '<span class="st-field-amount" ng-style="column.style">{{ row[column.field] }}</span>');
    //| currency : "INR "
    $templateCache.put('stCellDefault.html', '<span class="st-field-string" ng-style="column.style">{{ row[column.field] }}</span>');

}]);

prmApp.directive("stCellTemplate", ['$templateCache', '$compile', function ($templateCache, $compile) {

    return {
        restrict: "E",
        scope: {
            row: "=",
            column: "=",
            appScope: "="
        },
        link: function ($scope, $ele, $attr, $ctrl) {
            var template = "";
            var column = $scope.column;
            $scope.grid = {
                appScope: $scope.appScope
            }

            if (column.template) {
                template = column.template($scope.row, $scope.column);
            }
            else if (column.templateUrl) {
                template = column.template($scope.row, $scope.column);
            }

            if (template == "") {
                if (column.type == "amount") {
                    template = $templateCache.get('stCellAmount.html');
                }
                else {
                    template = $templateCache.get('stCellDefault.html');

                }

            }
            $ele.html(template).show();
            $compile($ele.contents())($scope);

        },
    }

}])

prmApp.controller("LogisticPriceComparisonController", ['$uibModal', '$scope', 'SettingService',
    'store', 'growlService', 'fileReader', 'LogisticPriceComparisonService', 'AngularUtility', '$state', '$stateParams', "userService", "logisticServices", "$log",
    function ($uibModal, $scope, SettingService, store, growlService,
        fileReader, logisticPriceComparisonService, AngularUtility, $state, $stateParams, userService, logisticServices, $log) {



        var _priceComparison = this;

        $scope.itemPreviousPrice = [];
        $scope.itemPreviousPrice.lastPrice = -1;
        $scope.bestPriceEnable = 0;

        $scope.formRequest = {
            isTabular: true,
            listRequirementItems: []
        };

        $scope.requirementItems = {
            productSNo: $scope.itemSNo++,
            ItemID: 0,
            productIDorName: '',
            productDescription: '',
            productQuantity: 0,
            typeOfPacking: '',
            modeOfShipment: '',
            natureOfGoods: 0,
            storageCondition: '',
            portOfLanding: '',
            finalDestination: '',
            deliveryLocation: '',
            preferredAirlines: '',
            isDeleted: 0,
            itemAttachment: '',
            attachmentName: '',
            netWeight: 0,
            ispalletize: 1,
            palletizeQty: 0,
            palletizeLength: 0,
            palletizeBreadth: 0,
            palletizeHeight: 0
        }
        $scope.formRequest.listRequirementItems.push($scope.requirementItems);
        $scope.product = $scope.formRequest.listRequirementItems.push($scope.requirementItems);
        $scope.sessionid = userService.getUserToken();
        $scope.compID = userService.getUserCompanyId();

       
        _priceComparison.lastp = [];

    _priceComparison.reqDetails = {
        RequirementId : 0,
        Title: '',
        InvoiceNumber: '',
        InvoiceDate: '',
        ConsigneeName: '',
        CustomerName: '',
        lastp : []
    };

    //$scope.auctionItem = {
    //    auctionVendors: [],
    //    listRequirementItems: [],
    //    listRequirementTaxes: []

        //}
        
        
        _priceComparison.gridOpitions = {
            itemsByPage: 20,
            pageSizeOptions: [10, 20, 50, 100],
            displayedPages: 7,
            showFilter: false,
            data: [],
            totalItems: 0,
            loading: false,
            noData: true,
            searchTerm: "",
            enableFilter: true,
            columns: [

                //{
                //    name: 'Requirement ID',
                //    field: 'RequirementId',
                //    type: 'int',
                //},

                //{
                //    name: 'Title',
                //    field: 'Title',
                //    type: 'string',
                //},

                //{
                //    name: 'Invoice Number',
                //    field: 'InvoiceNumber',
                //    type: 'string',
                //    //template: function (row, column) {
                //    //    return '<input type="number" style="width:100px;margin: auto;"   min="0" value="0" class="form-control" ng-model="row[column.field]" />'

                //    //},
                //    //visible: true

                //},
                //{
                //    name: 'Invoice Date',
                //    field: 'InvoiceDate',
                //    type: 'DateTime',
                //    //template: function (row, column) {
                //    //    return '<input type="number" style="width:100px;margin: auto;"   min="0" value="0" class="form-control" ng-model="row[column.field]" />'

                //    //},
                //    //visible: true

                //},
                //{
                //    name: 'Consignee Name',
                //    field: 'ConsigneeName',
                //    type: 'string',
                //    //template: function (row, column) {
                //    //    return '<input type="number" style="width:100px;margin: auto;"   min="0" value="0" class="form-control" ng-model="row[column.field]" />'

                //    //},
                //    //visible: true

                //},


                {
                    name: 'Company',
                    field: 'VendorCompanyName',
                    type: 'string',
                    style: {
                        "font-weight": "bold"
                    },
                    visible: true
                },
                {
                    name: 'Product',
                    field: 'ProductId',
                    type: 'string',
                    visible: true

                },

                {
                    name: 'Routing',
                    field: 'Routing',
                    type: 'string',
                    visible: true

                },
                {
                    name: 'Transit',
                    field: 'Transit',
                    type: 'string',
                    visible: true

                },
                {
                    name: 'Airlines',
                    field: 'Airlines',
                    type: 'string',
                    visible: true

                },
                {
                    name: 'Tariff',
                    field: 'Tariff',
                    type: 'amount',
                    visible: true

                },
                {
                    name: 'Offered Rate',
                    field: 'OfferedRate',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Rev Offered Rate',
                    field: 'RevisedOfferedRate',
                    type: 'amount',
                    visible: true,
                    isRevCol: true

                },

                {
                    name: 'Fsc',
                    field: 'Fsc',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Ssc',
                    field: 'Ssc',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Misc',
                    field: 'Misc',
                    type: 'amount',
                    visible: true


                },

                {
                    name: 'Other Charges',
                    field: 'OtherCharges',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Rev Other Charges',
                    field: 'RevisedOtherCharges',
                    type: 'amount',
                    visible: true,
                    isRevCol: true

                },
                {
                    name: 'Xray',
                    field: 'Xray',
                    type: 'amount',
                    visible: true

                },
                {
                    name: 'Ams',
                    field: 'Ams',
                    type: 'amount',
                    visible: true

                },
                {
                    name: 'Awb Fee',
                    field: 'AwbFee',
                    type: 'amount',
                    visible: true

                },
                {
                    name: 'Dg Fee',
                    field: 'DgFee',
                    type: 'amount',
                    visible: true

                },
                {
                    name: 'ForwordDg Fee',
                    field: 'ForwordDgFee',
                    type: 'amount',
                    visible: true

                },
                {
                    name: 'Total',
                    field: 'Total',
                    type: 'amount',
                    //template: function (row, column) {
                    //    return '<span>{{ grid.appScope.calicluateOverallTotal(row)}}</span>'
                    //    //| currency : "INR "
                    //},
                    visible: true

                },
                {
                    name: 'Rev Total',
                    field: 'RevTotal',
                    type: 'amount',
                    //template: function (row, column) {
                    //    return '<span>{{ grid.appScope.calicluateOverallTotal(row)}}</span>'
                    //    //| currency : "INR "
                    //},
                    visible: true,
                    isRevCol: true

                },

                {
                    name: 'Airway Bill NUmber',
                    field: 'AirwayBillNumber',
                    type: 'string',
                    visible: true

                },

                {
                    name: 'Shipping Bill Number',
                    field: 'ShippingBillNumber',
                    type: 'string',
                    visible: true

                },

                {
                    name: 'Drawback Amount',
                    field: 'DrawbackAmount',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'License Number',
                    field: 'LicenseNumber',
                    type: 'string',
                    visible: true

                },

                {
                    name: 'Loading/Unloading Charges',
                    field: 'LoadingUnloadingCharges',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Warehouse Storages',
                    field: 'WarehouseStorages',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Additional/Misc.Expenses',
                    field: 'AdditionalMiscExp',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'NarcoticSubcharges',
                    field: 'NarcoticSubcharges',
                    type: 'amount',
                    visible: true

                },

                {
                    name: 'Service Charges',
                    field: 'ServiceCharges',
                    type: 'decimal',
                    //template: function (row, column) {
                    //    return '<input type="number" style="width:100px;margin: auto;"  min="0" value="0" class="form-control" ng-model="row[column.field]" />'

                    //},
                    //visible: true


                },
                {
                    name: 'Custom Clearance',
                    field: 'CustomClearance',
                    type: 'decimal',
                    //template: function (row, column) {
                    //    return '<input type="number"  style="width:100px;margin: auto;"  min="0" value="0" class="form-control" ng-model="row[column.field]" />'

                    //},
                    //visible: true

                },
                {
                    name: 'Terminal Handling',
                    field: 'TerminalHandling',
                    type: 'decimal',
                    //template: function (row, column) {
                    //    return '<input type="number" style="width:100px;margin: auto;"   min="0" value="0" class="form-control" ng-model="row[column.field]" />'

                    //},
                    //visible: true

                },
                   
                    {
                        name: 'Pallet Prices',
                        field: 'palletizeNumber',
                        type: 'decimal',
                        template: function (row, column) {
                            return "<span>{{ grid.appScope.calicluatePalletizeNumber(row) > 0? grid.appScope.calicluatePalletizeNumber(row):'NA'}}</span>"
                            //| currency : "INR "
                        },
                        visible: true
                    },

                    {
                        name: 'Overall Total',
                        field: 'OverallTotal',
                        type: 'decimal',
                        template: function (row, column) {
                            return '<span>{{ grid.appScope.calicluateOverallTotal(row)}}</span>'
                            //| currency : "INR "
                        },
                        visible: true

                    },

                   

                    {
                        name: 'Vendor Remark',
                        field: 'VendorRemark',
                        type: 'string',
                        visible: true

                    }

                    //    , {
                    //    name: 'Action',
                    //    field: 'Action',
                    //    type: 'action',
                    //    template: function (row, column) {
                    //        return '<button class="btn btn-primary btn-sm" ng-click="grid.appScope.UpdateCharges(row)"><i class="fas fa-save"></i> Save changes</button>'
                    //    },
                    //    visible: true

                    //},

        ],
        }

        $scope.itemsPrices = [];
        
        $scope.getPreviousItemPrice = function (itemDetails, index) {
            $scope.itemPreviousPrice = [];
            $scope.previousPrices = [];
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.bestPriceEnable = index;
            $log.info(itemDetails);
            itemDetails.sessionID = userService.getUserToken();
            itemDetails.compID = userService.getUserCompanyId();
            logisticServices.getPreviousItemPrice(itemDetails)
                .then(function (response) {
                    var singleResponse = response;
                    var singleResponse2 = [];
                    singleResponse.forEach(function (item, itemindex) {
                        item.currentTime = new moment(item.currentTime).format("DD-MM-YYYY HH:mm");                        
                        singleResponse2.push(item);
                    })
                    $scope.itemsPrices.push(singleResponse2);

                    console.log('$scope.itemsPrices------------------------------------------------------->');
                    console.log($scope.itemsPrices);
                    console.log('$scope.itemsPrices------------------------------------------------------->');

                    $scope.previousPrices1 = [];
                    $scope.itemPreviousPrice = response;
                    $scope.itemPreviousPrice.forEach(function (item, itemindex) {
                        item.currentTime = new moment(item.currentTime).format("DD-MM-YYYY HH:mm");
                        $scope.previousPrices1.push(item);
                    })

                   
                   
                   $scope.previousPrices = $scope.previousPrices1;
                   
                });
           
        };
       // $scope.getPreviousItemPrice();

        //$scope.addField = function () {

        //    _priceComparison.gridOpitions.data.forEach(function (item2, index2) {
        //        if (item2.Ispalletize == 1) {
        //            _priceComparison.gridOpitions.columns.push({
        //                name: 'Pallet Prices',
        //                field: 'palletizeNumber', 
        //                type: 'decimal',
        //                template: function (row, column) {
        //                    return '<span>{{ grid.appScope.calicluatePalletizeNumber(row)}}</span>'
        //                    //| currency : "INR "
        //                },
        //                visible: true
        //            },);
        //        }

        //    })
        //}
        //$scope.addField();
        _priceComparison.palletvisible = false;
    _priceComparison.getList = function () {
        var id = $stateParams.reqId
        logisticPriceComparisonService.getComparsion(id).then(function (resposne) {


            if (resposne != undefined && resposne.length > 0) {
                var stats = resposne[0].RequirmentStatus;

                angular.forEach(_priceComparison.gridOpitions.columns, function (item) {
                    if (stats == "STARTED" || stats == 'NOTSTARTED' || status == 'UNCONFIRMED') {

                        if (item.isRevCol)
                            item.visible = false;
                        else
                            item.visible = true;
                        //if (item.Ispalletize == 1) {
                        //    item.palletvisible = true;
                        //} else {
                        //    item.palletvisible = false;
                        //}


                    }
                    else {
                        item.visible = true;
                    }



                })
            }


            _priceComparison.gridOpitions.data = resposne;


            var itemArray = [];
            _priceComparison.gridOpitions.data.forEach(function(item,index) {
                item.InvoiceDate = new moment(item.InvoiceDate).format("YYYY-MM-DD");
            })



            $scope.getPrices = function () {
                $scope.itemsPrices = [];
                _priceComparison.gridOpitions.data.forEach(function (item, index) {
                    console.log()
                    var isDuplicate = false;
                    
                    itemArray.forEach(function (i, ix) {
                        if (item.ItemId == i.ItemId) {
                            isDuplicate = true;
                        }
                    })

                    itemArray.push(item);

                    if (!isDuplicate) {
                        $scope.getPreviousItemPrice(item, index);
                        console.log('----------------------> Not Duplicate' + index + '--' + item.ItemId);
                    } else {
                        console.log('----------------------> Duplicate --------------->' + index + '--' + item.ItemId);
                    }

                })
            }
            $scope.getPrices();


            if (_priceComparison && _priceComparison.gridOpitions && _priceComparison.gridOpitions.data && _priceComparison.gridOpitions.data.length > 0) {
                _priceComparison.reqDetails.RequirementId = _priceComparison.gridOpitions.data[0].RequirementId;
                _priceComparison.reqDetails.Title = _priceComparison.gridOpitions.data[0].Title;
                _priceComparison.reqDetails.InvoiceNumber = _priceComparison.gridOpitions.data[0].InvoiceNumber;
                _priceComparison.reqDetails.InvoiceDate = _priceComparison.gridOpitions.data[0].InvoiceDate;
                _priceComparison.reqDetails.ConsigneeName = _priceComparison.gridOpitions.data[0].ConsigneeName;
                _priceComparison.reqDetails.CustomerName = _priceComparison.gridOpitions.data[0].CustomerName;

                if (_priceComparison.reqDetails.InvoiceDate == 'Invalid date')
                    {
                        _priceComparison.reqDetails.InvoiceDate = 'NA';
                    }

            }
           

            console.log('llllllllllllllllllll_priceComparison.gridOpitions');
            console.log(_priceComparison.gridOpitions);
            console.log('llllllllllllllllllll_priceComparison.gridOpitions');
        })
    }

    _priceComparison.UpdateCharges = function (row) {
        var data = {
            QuotationId: row.QuotationId,
            CustomClearance: row.CustomClearance,
            TerminalHandling: row.TerminalHandling,
            ServiceCharges: row.ServiceCharges
            //InvoiceNumber: row.InvoiceNumber,
            ////InvoiceDate: row.InvoiceDate,
            //ConsigneeName: row.ConsigneeName


        }
        logisticPriceComparisonService.updateCharges(data).then(function (response) {
            if (response.errorMessage == "") {
                growlService.growl(response.message, "inverse")
                _priceComparison.getList();
            } else {
                growlService.growl(response.errorMessage, "inverse")

            }
        })
        }


      
    console.log(_priceComparison.UpdateCharges);

    _priceComparison.openUpdateCharges = function () {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'UpdateChargesModal.html',
            controller: 'UpdateChargesModalCtrl',
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                options: {
                    editMode: true,
                    quotations: _priceComparison.gridOpitions.data.map(function (item) { return item.QuotationId })
                }
            }
        }).result.then(function (data) {
            _priceComparison.getList();
        });
    }



        _priceComparison.calicluateOverallTotal = function (row) {
            if (row.Ispalletize == 0) {
                if (row.RequirmentStatus == "STARTED" || row.RequirmentStatus == 'NOTSTARTED' || row.RequirmentStatus == 'UNCONFIRMED') {
                    return row.Total + row.ServiceCharges + row.TerminalHandling + row.CustomClearance + row.DrawbackAmount + row.LoadingUnloadingCharges +
                        row.WarehouseStorages + row.AdditionalMiscExp + row.NarcoticSubcharges
                }
                else {
                    return row.RevTotal + row.ServiceCharges + row.TerminalHandling + row.CustomClearance + row.DrawbackAmount + row.LoadingUnloadingCharges +
                        row.WarehouseStorages + row.AdditionalMiscExp + row.NarcoticSubcharges

                }
            }
            else {
                if (row.RequirmentStatus == "STARTED" || row.RequirmentStatus == 'NOTSTARTED' || row.RequirmentStatus == 'UNCONFIRMED') {
                    return row.Total + row.ServiceCharges + row.TerminalHandling + row.CustomClearance + (row.palletizeNumber * row.palletizeQty) + row.DrawbackAmount + row.LoadingUnloadingCharges +
                        row.WarehouseStorages + row.AdditionalMiscExp + row.NarcoticSubcharges
                }
                else {
                    return row.RevTotal + row.ServiceCharges + row.TerminalHandling + row.CustomClearance + (row.palletizeNumber * row.palletizeQty) + row.DrawbackAmount + row.LoadingUnloadingCharges +
                        row.WarehouseStorages + row.AdditionalMiscExp + row.NarcoticSubcharges

                }
            }
        

    }

        _priceComparison.calicluatePalletizeNumber = function (row) {
            if (row.RequirmentStatus == "STARTED" || row.RequirmentStatus == 'NOTSTARTED' || row.RequirmentStatus == 'UNCONFIRMED' || row.RequirmentStatus == 'Negotiation Ended') {
                return row.palletizeNumber * row.palletizeQty
            }
        }



        

    // Table Excel Export
    $scope.loadExcel = 0;
    $scope.notLoadExcel = 1;

    $scope.downloadExcel = function () {
        $scope.loadExcel = 1;
        $scope.notLoadExcel = 0;

        setTimeout(function () {
            document.getElementById("downloadExcelButton").click();
            $scope.$apply(function () {
                $scope.loadExcel = 0;
                $scope.notLoadExcel = 1;
            });
        }, 1000)
    }
    // Table Excel Export

    }]).controller("UpdateChargesModalCtrl", ['$uibModalInstance', 'options', 'SettingService', 'store', 'growlService', 'LogisticPriceComparisonService', '$stateParams', function ($uibModalInstance, options, SettingService, store, growlService, logisticPriceComparisonService, $stateParams) {
    var _modalCtrl = this;
    _modalCtrl.Model = {

    };
        _modalCtrl.AirwayBillNumberValidation = '';
        _modalCtrl.result = [];
        _modalCtrl.ispalletizedisabled = false;
    _modalCtrl.getList1 = function () {
        var id = $stateParams.reqId
        logisticPriceComparisonService.getComparsion(id).then(function (resposne) {

            if (resposne != undefined && resposne.length > 0) {
                var stats = resposne[0].RequirmentStatus;

              
            }
            _modalCtrl.result = resposne;
            _modalCtrl.result.forEach(function (item1) {
                if (item1.ProductQuantity <= 300) {
                    _modalCtrl.Model.LoadingUnloadingCharges = 250;
                }
                if (item1.Ispalletize == 1) {
                    _modalCtrl.ispalletizedisabled = true;
                }
                else if (item1.Ispalletize == 0) {
                    _modalCtrl.ispalletizedisabled = false;
                }
            })
           
        })
    }
        _modalCtrl.getList1();

        
    

    _modalCtrl.Model.CustomClearance = 2500;

    _modalCtrl.options = options;

    _modalCtrl.Update = function (form) {
        form.$submitted = true;

        //if (form.$valid == false) {
        //    if (form.AirwayBillNumber.$viewValue == undefined) {
        //        _modalCtrl.AirwayBillNumberValidation = { 'color': '#e62739' };
        //        return _modalCtrl.AirwayBillNumberValidation = "Please enter Airway Bill Number."
        //    }
        //    return false;
        //}


            _modalCtrl.AirwayBillNumberValidation =
            _modalCtrl.ShippingBillNumberValidation =
            _modalCtrl.LicenseNumberValidation =
            _modalCtrl.DrawbackAmountValidation =
            _modalCtrl.LoadingUnloadingChargesValidation =
            _modalCtrl.WarehouseStoragesValidation =
            _modalCtrl.AdditionalMiscExpValidation =
            _modalCtrl.NarcoticSubchargesValidation =
            _modalCtrl.ServiceChargesValidation =
            _modalCtrl.CustomClearanceValidation =
            _modalCtrl.TerminalHandlingValidation =
            _modalCtrl.InvoiceNumberValidation =
            _modalCtrl.CustomerNameValidation =
            _modalCtrl.ConsigneeNameValidation =
            _modalCtrl.palletizeNumberValidation =
            _modalCtrl.InvoiceDateValidation =
            "";


        if (form.$valid) {
            var data = [];
            angular.forEach(_modalCtrl.options.quotations, function (item,index) {
                if (index == 0) {
                    var ts = moment(_modalCtrl.Model.InvoiceDate, "YYYY-MM-DD").valueOf();
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    _modalCtrl.Model.InvoiceDate = "/Date(" + milliseconds + "000+0530)/";
                }

               

                data.push({
                    QuotationId: item,
                    CustomClearance: _modalCtrl.Model.CustomClearance,
                    TerminalHandling: _modalCtrl.Model.TerminalHandling,
                    ServiceCharges: _modalCtrl.Model.ServiceCharges,
                    InvoiceNumber: _modalCtrl.Model.InvoiceNumber,
                    InvoiceDate: _modalCtrl.Model.InvoiceDate,
                    ConsigneeName: _modalCtrl.Model.ConsigneeName,
                    palletizeNumber: _modalCtrl.Model.palletizeNumber,
                    AirwayBillNumber: _modalCtrl.Model.AirwayBillNumber,
                    ShippingBillNumber: _modalCtrl.Model.ShippingBillNumber,
                    LicenseNumber: _modalCtrl.Model.LicenseNumber,
                    DrawbackAmount: _modalCtrl.Model.DrawbackAmount,
                    LoadingUnloadingCharges: _modalCtrl.Model.LoadingUnloadingCharges,
                    WarehouseStorages: _modalCtrl.Model.WarehouseStorages,
                    AdditionalMiscExp: _modalCtrl.Model.AdditionalMiscExp,
                    NarcoticSubcharges: _modalCtrl.Model.NarcoticSubcharges,
                    CustomerName: _modalCtrl.Model.CustomerName
                })


            });


            logisticPriceComparisonService.updateCharges2(data).then(function (response) {
                if (response.errorMessage == "") {
                    growlService.growl(response.message, "inverse");
                    //code before the pause
                    setTimeout(function () {
                        //do what you need here
                        location.reload();
                    }, 2000);
                } else {
                    growlService.growl(response.errorMessage, "inverse");
                }
            })

            $uibModalInstance.close();


        }
        else {
            if (form.AirwayBillNumber.$viewValue == undefined || form.AirwayBillNumber.$viewValue == '' || form.AirwayBillNumber.$viewValue == null) {
                return _modalCtrl.AirwayBillNumberValidation = "Please enter valid Airway Bill Number"
            }
            if (form.ShippingBillNumber.$viewValue == undefined || form.ShippingBillNumber.$viewValue == '' || form.ShippingBillNumber.$viewValue == null) {
                return _modalCtrl.ShippingBillNumberValidation = "Please enter valid Shipping Bill Number"
            }
            if (form.LicenseNumber.$viewValue == undefined || form.LicenseNumber.$viewValue == '' || form.LicenseNumber.$viewValue == null) {
                return _modalCtrl.LicenseNumberValidation = "Please enter valid License Number"
            }
            if (form.DrawbackAmount.$viewValue == undefined || form.DrawbackAmount.$viewValue == '' || form.DrawbackAmount.$viewValue == null) {
                return _modalCtrl.DrawbackAmountValidation = "Please enter valid Drawback Amount"
            }
            if (form.LoadingUnloadingCharges.$viewValue == undefined || form.LoadingUnloadingCharges.$viewValue == '' || form.LoadingUnloadingCharges.$viewValue == null) {
                return _modalCtrl.LoadingUnloadingChargesValidation = "Please enter valid Loading/Unloading Charges"
            }
            if (form.WarehouseStorages.$viewValue == undefined || form.WarehouseStorages.$viewValue == '' || form.WarehouseStorages.$viewValue == null) {
                return _modalCtrl.WarehouseStoragesValidation = "Please enter valid Warehouse Storages"
            }
            if (form.AdditionalMiscExp.$viewValue == undefined || form.AdditionalMiscExp.$viewValue == '' || form.AdditionalMiscExp.$viewValue == null) {
                return _modalCtrl.AdditionalMiscExpValidation = "Please enter valid Additional/Misc.Expenses"
            }
            if (form.NarcoticSubcharges.$viewValue == undefined || form.NarcoticSubcharges.$viewValue == '' || form.NarcoticSubcharges.$viewValue == null) {
                return _modalCtrl.NarcoticSubchargesValidation = "Please enter valid Narcotic Subcharges"
            }
            if (form.ServiceCharges.$viewValue == undefined || form.ServiceCharges.$viewValue == '' || form.ServiceCharges.$viewValue == null) {
                return _modalCtrl.ServiceChargesValidation = "Please enter valid Service Charges"
            }
            if (form.CustomClearance.$viewValue == undefined || form.CustomClearance.$viewValue == '' || form.CustomClearance.$viewValue == null) {
                return _modalCtrl.CustomClearanceValidation = "Please enter valid Custom Clearance"
            }
            if (form.TerminalHandling.$viewValue == undefined || form.TerminalHandling.$viewValue == '' || form.TerminalHandling.$viewValue == null) {
                return _modalCtrl.TerminalHandlingValidation = "Please enter valid Terminal Handling"
            }
            if (form.InvoiceNumber.$viewValue == undefined || form.InvoiceNumber.$viewValue == '' || form.InvoiceNumber.$viewValue == null) {
                return _modalCtrl.InvoiceNumberValidation = "Please enter valid Invoice Number"
            }
            if (form.CustomerName.$viewValue == undefined || form.CustomerName.$viewValue == '' || form.CustomerName.$viewValue == null) {
                return _modalCtrl.CustomerNameValidation = "Please enter Customer Name"
            }
            if (form.ConsigneeName.$viewValue == undefined || form.ConsigneeName.$viewValue == '' || form.ConsigneeName.$viewValue == null) {
                return _modalCtrl.ConsigneeNameValidation = "Please enter Consignee Name"
            }
            if (form.palletizeNumber.$viewValue == undefined || form.palletizeNumber.$viewValue == '' || form.palletizeNumber.$viewValue == null) {
                return _modalCtrl.palletizeNumberValidation = "Please enter valid Pallet Prices"
            }
            if (form.InvoiceDate.$viewValue == undefined || form.InvoiceDate.$viewValue == '' || form.InvoiceDate.$viewValue == null) {
                return _modalCtrl.InvoiceDateValidation = "Please enter valid Invoice Date"
            }
        }
    }

    _modalCtrl.close = function () {
        $uibModalInstance.close();
    };

}]);

