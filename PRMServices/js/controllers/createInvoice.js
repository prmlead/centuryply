﻿prmApp
    .controller('createInvoiceCtrl', function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter, workflowService) {
        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.invoiceNumber = $stateParams.invoiceNumber;
        $scope.poNumber = $stateParams.poNumber;
        //$scope.asnCode = $stateParams.asnCode;
        $scope.invoiceID = $stateParams.invoiceID;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        $scope.compID = userService.getUserCompanyId();
        $scope.customerCompanyId = userService.getCustomerCompanyId();
        $scope.selectedPODetails = [{
            itemAttachment: [],
            attachmentName: '',
            attachmentsArray: []
        }];

        $scope.pendingPOItems = [{
            itemAttachment: [],
            attachmentName: '',
            attachmentsArray: []
        }];

        $scope.billedQty = 0;
        $scope.receivedQty = 0;
        $scope.rejectedQty = 0;
        $scope.remainingQty = 0;
        $scope.removeQty = 0;
        $scope.isError = false;
        $scope.ALTERNATIVE_UOM = '';
        $scope.ALTERNATIVE_UOM_QTY = '';
        $scope.SERVICE_CODE = '';
        $scope.SERVICE_DESCRIPTION = '';
        $scope.MISC_CHARGES = '';
        $scope.maxDateMoment = moment();



        if ($stateParams.detailsObj) {
            $scope.selectedPODetails = $stateParams.detailsObj;
            $scope.selectedPODetails.forEach(function (item, index) {
                item.INVOICE_QTY = item.GRN_QTY;
            });

            console.log($scope.selectedPODetails);
        }


        $scope.update = function (invoiceId) {

            $scope.tempArray = [];

            var params =
            {
                "INVOICE_ID": invoiceId,
                "SESSION_ID": userService.getUserToken()
            };

            var validParams = Object.keys(params).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
            }).join('&');

            PRMPOService.getInvoiceByID(validParams)
                .then(function (response) {
                    if (response) {
                        var arr = JSON.parse(response).Table;

                        var mainObj = arr[0];

                        mainObj.attachmentsArray = [];
                        mainObj.attachmentsArrayTemp = [];
                        mainObj.INVOICE_NUMBER_TEMP = mainObj.INVOICE_NUMBER;

                        if (mainObj.ATTACHMENTS)
                        {
                            var attachmentArr = mainObj.ATTACHMENTS.split(',');

                            if (attachmentArr != null && attachmentArr.length > 0)
                            {
                                var index = 1;
                                attachmentArr.forEach(function (item)
                                {
                                    var obj =
                                    {
                                        "fileID": item,
                                        "fileName": "File-" + index
                                    };

                                    mainObj.attachmentsArray.push(obj);
                                    mainObj.attachmentsArrayTemp.push(obj);

                                    index++;
                                });
                            }
                        }

                        $scope.selectedPODetails.push(mainObj);

                    }
                });
        };


        if (+$scope.invoiceID > 0)
        {
            $scope.selectedPODetails = [];
            $scope.update(+$scope.invoiceID);
        }


        if ($scope.isCustomer) {
            $scope.SelectedDeptId = 0;
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if (userService.getSelectedUserDepartmentDesignation()) {
                $scope.SelectedUserDepartmentDesignation = userService.getSelectedUserDepartmentDesignation();
            }
            $scope.SelectedDeptId = $scope.SelectedUserDepartmentDesignation.deptID;
            $scope.UserLocation = $scope.SelectedUserDepartmentDesignation.userLocation;
        }

        $scope.filters = {
            fromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
        }

        $scope.checkInvoiceUniqueResult = false;
        $scope.isSaveDisabled = false;

        $scope.getInvoiceList = function () {
            $scope.params = {
                "COMP_ID": $scope.isCustomer ? +$scope.compID : 0,
                "U_ID": $scope.isCustomer ? 0 : +$scope.userID,
                "fromDate": $scope.filters.fromDate,
                "toDate": $scope.filters.toDate,
                "PO_NUMBER": $scope.poNumber,
                "INVOICE_NUMBER": $scope.invoiceNumber
            }

            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.filteredPendingPOsList.REMARKS = response[0].COMMENTS;
                    $scope.filteredPendingPOsList.INVOICE_TYPE_LOOKUP = response[0].INVOICE_TYPE;
                    //$scope.filteredPendingPOsList.INDENT_OF_PURCHASE = response[0].INDENT_OF_PURCHASE;
                    //$scope.filteredPendingPOsList.REGISTERED_UNDER_GST = response[0].REGISTERED_UNDER_GST;
                    $scope.filteredPendingPOsList.EXCHANGE_RATE = response[0].EXCHANGE_RATE;
                    $scope.filteredPendingPOsList.IRN_NO = response[0].IRN_NO;
                    $scope.filteredPendingPOsList.INVOICE_NUMBER = response[0].INVOICE_NUMBER;
                    $scope.filteredPendingPOsList.INVOICE_DATE_1 = new moment(response[0].INVOICE_DATE).format("DD-MM-YYYY");
                    $scope.filteredPendingPOsList.DUE_DATE_1 = new moment(response[0].DUE_DATE).format("DD-MM-YYYY");
                    $scope.isFormdisabled = true;
                    $scope.pendingPOItems = response;
                    $scope.pendingPOItems.forEach(function (item, index) {
                        item.isErrorWeightValidation = false;
                        item.isErrorGrossWeightValidation = false;
                        item.isErrorWeightValidationRemaining = false;
                        item.isErrorRemaining = false;
                        //item.AMOUNT = 0;
                        item.NATURE_OF_TRANSACTION = item.IGST > 0 ? 'Inter-state Purchase' : 'Intra-state Purchase';
                        item.AMOUNT = item.NET_PRICE * item.INVOICE_QTY;
                        //item.TAX = item.IGST + item.CGST + item.SGST;
                        item.INVOICE_QTY_TEMP = item.INVOICE_QTY;
                        item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP = item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY;
                        $scope.getSum(item, 'QTY', index);
                    });
                    console.log($scope.pendingPOItems);

                });
        };


        $scope.getPendingPOOverall = function () {
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            var params = {
                "compid": $scope.isCustomer ? $scope.compID : 0,
                "uid": $scope.isCustomer ? 0 : +$scope.userID,
                "search": $scope.poNumber,
                "categoryid": '',
                "productid": '',
                "supplier": '',
                "postatus": '',
                "deliverystatus": '',
                "plant": '',
                "fromdate": '1970-01-01',
                "todate": '2100-01-01',
                "page": 0,
                "pagesize": 10,
                "ackStatus": '',
                "buyer": '',
                "purchaseGroup": '',
                "sessionid": userService.getUserToken()

            };

            $scope.pageSizeTemp = (params.page + 1);

            PRMPOService.getPOScheduleList(params)
                .then(function (response) {
                    $scope.pendingPOList = [];
                    $scope.filteredPendingPOsList = [];
                    if (response && response.length > 0) {
                        response.forEach(function (item, index) {
                            item.SUB_TOTAL = 0;
                            item.INVOICE_AMOUNT = 0;
                            item.TAX = 0;
                            $scope.pendingPOList.push(item);

                        });

                    }

                    $scope.filteredPendingPOsList = $scope.pendingPOList[0];

                    if ($scope.filteredPendingPOsList.PO_NUMBER !== '') {
                        var params1 = {
                            "ponumber": $scope.poNumber,
                            "moredetails": 0,
                            "forasn": false
                        };
                        PRMPOService.getPOScheduleItems(params1)
                            .then(function (response) {
                                $scope.pendingPOItems = response;
                                $scope.pendingPOItems.forEach(function (item, index) {
                                    item.INVOICE_QTY = 0;
                                    item.AMOUNT = item.NET_PRICE * item.INVOICE_QTY;

                                });
                                $scope.filteredPendingPOsList.TAX = _.sumBy($scope.pendingPOItems, function (o) { return ((o.IGST + o.SGST + o.CGST)); });

                                //pendingPODetails.pendingPOItems = $scope.pendingPOItems;
                                if ($scope.pendingPOItems.length > 0 && $scope.invoiceID > 0) {
                                    $scope.getInvoiceList();
                                }

                            });
                    }
                });
        };


        /*  $scope.getPendingPOOverall();*/

        $scope.createInvoice = function (poNumber) {
            var url = $state.href('vendorPoInvoices', { "poNumber": poNumber });
            $window.open(url, '_blank');
        };

        $scope.getSum = function (item, val, indexVal) {
            if (val == 'QTY') {
                $scope.filteredPendingPOsList.SUB_TOTAL = 0;
                $scope.filteredPendingPOsList.INVOICE_AMOUNT = 0;
                $scope.pendingPOItems.forEach(function (item1, index) {
                    if (index == indexVal) {
                        item1.AMOUNT = 0;
                        item1.AMOUNT = item1.NET_PRICE * item1.INVOICE_QTY;
                        //item1.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY = item1.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY_TEMP - item1.INVOICE_QTY;
                    }

                    $scope.filteredPendingPOsList.SUB_TOTAL = $scope.filteredPendingPOsList.SUB_TOTAL + item1.AMOUNT;


                });
                $scope.filteredPendingPOsList.TAX = _.sumBy($scope.pendingPOItems, function (o) { return ((o.IGST + o.SGST + o.CGST)); });

                $scope.filteredPendingPOsList.INVOICE_AMOUNT = $scope.filteredPendingPOsList.SUB_TOTAL + $scope.filteredPendingPOsList.TAX;

            }
        }

        $scope.isQtyError = function () {
            let isValid = false;
            isValid = _.some($scope.selectedPODetails, function (item) {
                return (parseFloat(item.INVOICE_QTY) > parseFloat(item.INVOICE_AVAILABLE_QTY));
            });
            return isValid;
        };

        $scope.areAllItemsValid = function () {
            let isValid = false;
            isValid = _.some($scope.selectedPODetails, function (item) {
                return item.isError;
            });
            return isValid;
        };

        $scope.pendingPOItems = [];
        $scope.invoiceNumber = "";
        $scope.saveInvoice = function (item) {
            let invoiceDetailsArr = [];
            $scope.pendingPOItems = [];
            $scope.isError = false;
            $scope.isErrorWeightValidation = false;
            //$scope.isErrorWeightValidationRemaining = false;
            $scope.isErrorGrossWeightValidation = false;
            $scope.invoiceNumberError = false;
            $scope.invoiceDateError = false;
            $scope.dueDateError = false;
            $scope.invoiceTypeError = false;
            $scope.isErrorRemaining = false;

            if (!$scope.selectedPODetails[0].INVOICE_NUMBER) {
                $scope.invoiceNumberError = true;
            }
            if (!$scope.selectedPODetails[0].INVOICE_DATE_1) {
                $scope.invoiceDateError = true;
            }

            if ($scope.checkInvoiceUniqueResult)
            {
                return;
            }

            if (!$scope.selectedPODetails[0].attachmentsArray || $scope.selectedPODetails[0].attachmentsArray.length <= 0) {
                swal("Error!", "Please Select the Attachments", "error");
                return;

            }

            $scope.selectedPODetails.forEach(function (item) {
                if ($scope.invoiceID == 0) {
                    if (parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY) !== 0 && ((parseFloat(item.INVOICE_QTY) > parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY)) || item.INVOICE_QTY == undefined || item.INVOICE_QTY == 0)) {
                        item.isErrorWeightValidationRemaining = true;
                        $scope.isErrorWeightValidationRemaining = true;
                    }
                } else {
                    if (parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY) !== 0 && ((parseFloat(item.INVOICE_QTY) > (parseFloat(item.INVOICE_QTY_TEMP) + parseFloat(item.TOTAL_INVOICE_LINE_ITEM_REMAINING_QTY))) || item.INVOICE_QTY == 0)) {
                        item.isErrorWeightValidation = true;
                        $scope.isErrorWeightValidation = true;
                    }

                }
            });

            if ($scope.isErrorWeightValidation || $scope.isErrorWeightValidationRemaining || $scope.isErrorGrossWeightValidation || $scope.invoiceNumberError || $scope.invoiceDateError || $scope.dueDateError || $scope.invoiceTypeError) {
                return;
            }

            $scope.invoiceNumber = $scope.selectedPODetails[0].INVOICE_NUMBER;

            var ts = moment($scope.selectedPODetails[0].INVOICE_DATE_1, "DD-MM-YYYY").valueOf();
            var m = moment(ts);
            var deliveryDate = new Date(m);
            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
            $scope.INVOICE_DATE = "/Date(" + milliseconds + "000+0530)/";

            $scope.selectedPODetails.forEach(function (item, index) {
                var isError = '';
                //item.INVOICE_DATE = $scope.selectedPODetails[0].INVOICE_DATE_1;
                item.isError = false;
                isError = $scope.isQtyError();
                if (isError) {
                    item.isError = true;
                }

                let newObj =
                {
                    "INVOICE_DATE": $scope.INVOICE_DATE,
                    "INVOICE_NUMBER": $scope.selectedPODetails[0].INVOICE_NUMBER,
                    "VENDOR_CODE": $scope.selectedPODetails[0].SUPPLIER_CODE,
                    "VENDOR_ID": $scope.userID,
                    "PO_NUMBER": item.PO_NUMBER,
                    "PO_LINE_ITEM": item.PO_LINE_ITEM,
                    "GRN_NUMBER": item.GRN_NUMBER,
                    "GRN_YEAR": moment(item.GRN_DATE, "DD/MM/YYYY").year(),
                    "GRN_LINE_ITEM": item.GRN_LINE_ITEM,
                    "QUANTITY": item.INVOICE_QTY,
                    "INVOICE_ID": item.INVOICE_ID > 0 ? item.INVOICE_ID : 0,
                    "attachmentsArray": item.attachmentsArray,
                    "SessionID": $scope.sessionID,
                    "U_ID": $scope.userID
                };

                invoiceDetailsArr.push(newObj);
            });

            if ($scope.areAllItemsValid()) {
                return;
            }

            var params = {
                "INVOICE_ARR": invoiceDetailsArr,
                "doSubmitToSAP": false,
                "rowId": +$scope.invoiceID,
                "sessionId": $scope.sessionID
            };

            PRMPOService.savePOInvoice(params)
                .then(function (response) {
                    if (response.errorMessage) {
                        swal("Error!", response.errorMessage, "error");
                    }
                    else {
                        swal({
                            title: "Thanks !",
                            text: "Submitted Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                $state.go("listInvoices");
                            });
                    }
                });
        };


        $scope.checkUserUniqueResult = function (idtype, inputvalue) {


            if (!$scope.checkInvoiceUniqueResult) {
                $scope.checkInvoiceUniqueResult = false;
            }

            if ($scope.invoiceID > 0)
            {
                if ($scope.selectedPODetails[0].INVOICE_NUMBER_TEMP == $scope.selectedPODetails[0].INVOICE_NUMBER) {
                    $scope.checkInvoiceUniqueResult = false;
                    $scope.isSaveDisabled = false;
                    return;
                }
            }

            if (inputvalue == "" || inputvalue == undefined) {
                $scope.isSaveDisabled = false;
                return false;
            }

            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "INVOICE") {
                    if ($scope.checkInvoiceUniqueResult = !response) {
                        $scope.checkInvoiceUniqueResult = !response;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.checkInvoiceUniqueResult = false;
                        $scope.isSaveDisabled = false;
                    }
                }
            });
        };

        $scope.isFormdisabled = false;

        if ($scope.isCustomer) {
            $scope.isFormdisabled = true;
            $scope.getItemWorkflow = function () {
                workflowService.getItemWorkflow(0, $scope.invoiceID, 'VENDOR_INVOICE')
                    .then(function (response) {
                        $scope.itemWorkflow = response;
                        if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                            $scope.currentStep = 0;

                            var count = 0;

                            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {
                                if (!track.multipleAttachments) {
                                    track.multipleAttachments = [];
                                }

                                if (track.status === 'APPROVED') { track.statusNew = 'Approved'; }
                                if (track.status === 'HOLD') { track.statusNew = 'Hold'; }
                                if (track.status === 'PENDING') { track.statusNew = 'Pending'; }
                                if (track.status === 'REJECTED') { track.statusNew = 'Rejected'; }

                                if (track.status === 'APPROVED' || track.status === 'HOLD') {
                                    $scope.isFormdisabled = true;
                                }

                                if (track.status === 'APPROVED') {
                                    $scope.isWorkflowCompleted = true;
                                    $scope.orderInfo = track.order;
                                    $scope.assignToShow = track.status;

                                }
                                else {
                                    $scope.isWorkflowCompleted = false;
                                }

                                if (track.status === 'REJECTED' && count == 0) {
                                    count = count + 1;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD') && count == 0) {
                                    count = count + 1;
                                    $scope.currentAccess = track.order;
                                }

                                if ((track.status === 'PENDING' || track.status === 'HOLD' || track.status === 'REJECTED') && $scope.currentStep === 0) {
                                    $scope.currentStep = track.order;
                                    return false;
                                }
                            });
                        }
                    });
            };

            $scope.updateTrack = function (step, status) {
                $scope.disableAssignPR = true;
                $scope.commentsError = '';
                if (step.comments != null || step.comments != "" || step.comments != undefined) {
                    step.comments = validateStringWithoutSpecialCharacters(step.comments);
                }
                var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
                if (step.order == tempArray.order && status === 'APPROVED') {
                    $scope.disableAssignPR = false;
                } else {
                    $scope.disableAssignPR = true;
                }

                if ($scope.isReject) {
                    $scope.commentsError = 'Please Save Rejected Items/Qty';
                    return false;
                }

                if (status === 'REJECTED' && (step.comments == null || step.comments == "")) {
                    $scope.commentsError = 'Please enter comments';
                    return false;
                }

                step.status = status;
                step.sessionID = $scope.sessionID;
                step.modifiedBy = userService.getUserId();

                step.moduleName = 'VENDOR_INVOICE';

                step.subModuleName = '';
                step.subModuleID = 0;

                workflowService.SaveWorkflowTrack(step)
                    .then(function (response) {
                        if (response.errorMessage) {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            $scope.getItemWorkflow();
                            location.reload();
                        }
                    });
            };

            $scope.getItemWorkflow();

            function validateStringWithoutSpecialCharacters(string) {
                if (string) {
                    string = string.replace(/\'/gi, "");
                    string = string.replace(/\"/gi, "");
                    string = string.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                    string = string.replace(/(\r\n|\n|\r)/gm, "");
                    string = string.replace(/\t/g, '');
                    return string;
                }
            }

            $scope.isApproverDisable = function (index) {
                var disable = true;
                var previousStep = {};
                $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {
                    if (index == stepIndex) {
                        if (stepIndex == 0) {
                            if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                        else if (stepIndex > 0) {
                            if (previousStep.status === 'PENDING' || previousStep.status === 'HOLD' || previousStep.status === 'REJECTED') {
                                disable = true;
                            }
                            else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                                (step.status === 'PENDING' || step.status === 'HOLD')) {
                                disable = false;
                            }
                            else {
                                disable = true;
                            }
                        }
                    }
                    previousStep = step;
                });

                return disable;
            };


            $scope.deptIDs = [];
            $scope.desigIDs = [];
            $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
            if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
                $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                    $scope.deptIDs.push(item.deptID);
                    item.listDesignation.forEach(function (item1, index1) {
                        if (item1.isAssignedToUser && item1.isValid) {
                            $scope.desigIDs.push(item1.desigID);
                        }
                    });
                });
            }


            $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
                var isEligible = true;

                if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                    isEligible = true;
                } else {
                    isEligible = false;
                }

                return isEligible;
            };

            $scope.showApprovedDate = function (date) {
                return userService.toLocalDate(date);
            };
        }

        $scope.completeInvoice = function (item) {
            var params =
            {
                "INVOICE_NUMBER": item[0].INVOICE_NUMBER,
                "INVOICE_ID": +$scope.invoiceID,
                "uId": +$scope.userID,
                "sessionid": userService.getUserToken()
            };

            PRMPOService.completeInvoice(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Saved Successfully.", "success");
                        location.reload();
                    }
                });
        };

        //INvoice
        $scope.getFile1 = function (id, itemid, ext) {
            $scope.filesTemp = $("#" + id)[0].files;
            $scope.filesTemp = Object.values($scope.filesTemp);
            $scope.totalASNSize = 0;
            if ($scope.filesTemp && $scope.filesTemp.length > 0) {
                $scope.filesTemp.forEach(function (item, index) {
                    $scope.totalASNSize = $scope.totalASNSize + item.size;
                });
            }
            if (($scope.totalASNSize + $scope.totalASNItemSize) > $scope.totalAttachmentMaxSize) {
                swal({
                    title: "Attachment size!",
                    text: "Total Attachments size cannot exceed 6MB",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {
                        return;
                    });
                return;
            }

            $scope.filesTemp.forEach(function (attach, attachIndex) {
                $scope.file = $("#" + id)[0].files[attachIndex];

                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: 0,
                            fileSize: 0
                        };
                        var bytearray = new Uint8Array(result);
                        fileUpload.fileSize = result.byteLength;
                        fileUpload.fileStream = $.makeArray(bytearray);
                        fileUpload.fileName = attach.name;

                        //if ($scope.selectedPODetails[0].attachmentsArrayTemp != null &&
                        //    $scope.selectedPODetails[0].attachmentsArrayTemp.length > 0)
                        //{
                        //    $scope.selectedPODetails[0].attachmentsArrayTemp = [];
                        //    $scope.selectedPODetails[0].attachmentsArray = [];
                        //}

                        if (!$scope.selectedPODetails[0].attachmentsArray)
                        {
                            $scope.selectedPODetails[0].attachmentsArray = [];
                        }

                        var ifExists = _.findIndex($scope.selectedPODetails[0].attachmentsArray, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                        if (ifExists <= -1) {
                            $scope.selectedPODetails[0].attachmentsArray.push(fileUpload);
                        }

                    });
            });
        };

        $scope.removeAttach = function (index) {
            $scope.selectedPODetails[0].attachmentsArray.splice(index, 1);
        };


        $scope.cancelInvoice = function () {
            var url = $state.href("listInvoices");
            $window.open(url, '_self');
        };

        $scope.validateInvoiceQty = function (item, index) {
            item.isErrorWeightValidationRemaining = false;
            item.isError = false;
            if (item.INVOICE_QTY > item.GRN_QTY) {
                item.isErrorWeightValidationRemaining = true;
                item.isError = true;
                return false;
            }
        };

    });