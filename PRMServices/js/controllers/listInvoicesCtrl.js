﻿prmApp
    .controller('listInvoicesCtrl', function ($scope, $state, $stateParams, userService, growlService,
        workflowService,
        auctionsService, fileReader, $log, $window, poService, PRMPOService) {
        $scope.isVendor = (userService.getUserType() == "VENDOR") ? true : false;
        $scope.userID = userService.getUserId();
        $scope.compID = userService.getUserCompanyId();
       // $scope.customerCompanyId = userService.getCustomerCompanyId();
        $scope.sessionId = userService.getUserToken();

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5;
        $scope.invoiceDetails = [];
        $scope.selectedInvoiceItems = [];
        $scope.tempArray = [];
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
           //$scope.getpendingPOlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        $scope.filtersList = {
            poNumber: [],
            invoiceNumber: [],
            supplierList: [],
            grnNumber: []
        };


        $scope.filters = {
            searchKeyword: '',
            type: '',
            status: '',
            fromDate: moment().subtract(365, "days").format("YYYY-MM-DD"),
            toDate: moment().format('YYYY-MM-DD'),
            poNumber: {},
            invoiceNumber: {},
            supplier: {},
            grnNumber: {}
        };

        $scope.setFilters = function (currentPage) {
            if ($scope.filters.poNumber == '' || $scope.filters.invoiceNumber == '' || $scope.filters.supplierList == '' || $scope.filters.searchKeyword == '' || $scope.filters.grnNumber == '') {
                $scope.getInvoiceList();
            } else if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.poNumber) || !_.isEmpty($scope.filters.invoiceNumber) || !_.isEmpty($scope.filters.supplierList) || !_.isEmpty($scope.filters.grnNumber)) {
                $scope.getInvoiceList();
            }
        };

        $scope.filterByDate = function () {
            $scope.getInvoiceList();
        };

        $scope.getInvoiceList = function () {
            var poNumbers, invoiceNumbers, suppliers, fromDate, toDate, grnNumbers;

            if (_.isEmpty($scope.filters.fromDate)) {
                fromDate = '';
            } else {
                fromDate = $scope.filters.fromDate;
            }

            if (_.isEmpty($scope.filters.toDate)) {
                toDate = '';
            } else {
                toDate = $scope.filters.toDate;
            }

            if (_.isEmpty($scope.filters.poNumber)) {
                poNumbers = '';
            } else if ($scope.filters.poNumber && $scope.filters.poNumber.length > 0) {
                var f1 = _($scope.filters.poNumber)
                    .filter(item => item.name)
                    .map('name')
                    .value();
                poNumbers = f1.join(',');
            }

            if (_.isEmpty($scope.filters.invoiceNumber)) {
                invoiceNumbers = '';
            } else if ($scope.filters.invoiceNumber && $scope.filters.invoiceNumber.length > 0) {
                var f2 = _($scope.filters.invoiceNumber)
                    .filter(item => item.name)
                    .map('name')
                    .value();
                invoiceNumbers = f2.join(',');
            }

            if (_.isEmpty($scope.filters.supplier)) {
                suppliers = '';
            } else if ($scope.filters.supplier && $scope.filters.supplier.length > 0) {
                var f3 = _($scope.filters.supplier)
                    .filter(item => item.name)
                    .map('id')
                    .value();
                suppliers = f3.join(',');
            }

            if (_.isEmpty($scope.filters.grnNumber)) {
                grnNumbers = '';
            } else if ($scope.filters.grnNumber && $scope.filters.grnNumber.length > 0) {
                var f4 = _($scope.filters.grnNumber)
                    .filter(item => item.name)
                    .map('name')
                    .value();
                grnNumbers = f4.join(',');
            }

            $scope.params = {
                "COMP_ID": +$scope.compID,
                "U_ID": +$scope.userID,
                "poNumber": poNumbers,
                "invoiceNumber": invoiceNumbers,
                "supplier": suppliers,
                "grnNumber": grnNumbers,
                "fromdate": fromDate,
                "todate": toDate,
                "search": $scope.filters.searchKeyword ? $scope.filters.searchKeyword : ""
            }

            PRMPOService.getInvoiceList($scope.params)
                .then(function (response) {
                    $scope.invoiceDetails = response;
                    $scope.invoiceDetails.forEach(function (item) {
                        item.INVOICE_DATE = new moment(item.INVOICE_DATE).format("DD-MM-YYYY");
                    });
                    $scope.totalItems = $scope.invoiceDetails.length;
                });
        }

        $scope.getInvoiceList();





        $scope.invoiceListFilteredTemp = [];
        $scope.getInvoiceListByFilter = function () {
            let poNumberTemp = [];
            let invoiceNumberTemp = [];
            let supplierListTemp = [];
            let grnListTemp = [];

            var params = {
                "COMP_ID": $scope.compID,
                "USER_ID": $scope.userID,
                "sessionid": userService.getUserToken()
            };
            PRMPOService.getInvoiceListByFilter(params)
                .then(function (response) {
                    $scope.invoiceListFilteredTemp = response;
                    $scope.invoiceListFilteredTemp.forEach(function (item, index) {
                        if (item.TYPE === 'PO_NUMBER') {
                            poNumberTemp.push({ name: item.VALUE });
                        } else if (item.TYPE === 'INVOICE_NUMBER') {
                            invoiceNumberTemp.push({ name: item.VALUE });
                        } else if (item.TYPE === 'SUPPLIER_NAME') {
                            supplierListTemp.push({ name: item.VALUE , id :item.ID });
                        } else if (item.TYPE === 'GRN_NUMBER') {
                            grnListTemp.push({ name: item.VALUE });
                        }
                    });
                    $scope.filtersList.poNumber = poNumberTemp;
                    $scope.filtersList.invoiceNumber = invoiceNumberTemp;
                    $scope.filtersList.supplierList = supplierListTemp;
                    $scope.filtersList.grnNumber = grnListTemp;

                });
        };
        $scope.getInvoiceListByFilter();




/*        Code Needs to be rewrite*/

        $scope.update = function (obj, status)
        {

            if (status == -1)
            {
                $state.go('createInvoice', { "invoiceID": obj.INVOICE_ID });
            }

            $scope.tempArray = [];

            var params =
            {
                "INVOICE_ID": obj.INVOICE_ID,
                "doSubmitToSAP": status ? true : false,
                "SESSION_ID": userService.getUserToken()
            };

            var validParams = Object.keys(params).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
            }).join('&');

            PRMPOService.getInvoiceByID(validParams)
                .then(function (response)
                {
                    if (response)
                    {
                        var arr = JSON.parse(response).Table;

                        var mainObj = arr[0];

                        if (status === 1) {
                            mainObj.INVOICE_STATUS_COMMENTS_TEMP = "APPROVED";
                            mainObj.INVOICE_STATUS = "APPROVED";
                        } else if (status === 0) {
                            mainObj.INVOICE_STATUS_COMMENTS_TEMP = 'REJECTED'
                            mainObj.INVOICE_STATUS = "REJECTED";
                        }

                        $scope.tempArray.push(mainObj);

                        angular.element('#ApprovalPopup').modal('show');

                    }
                });
        };



        $scope.GetInvoiceItems = function (invoice) {

            $scope.tempArray = [];

            var params =
            {
                "ROW_ID": invoice.ROW_ID,
                "SESSION_ID": userService.getUserToken()
            };

            var validParams = Object.keys(params).map(function (key) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
            }).join('&');

            PRMPOService.GetInvoiceItems(validParams)
                .then(function (response) {
                    if (response) {
                        var arr = JSON.parse(response).Table;

                        $scope.selectedInvoiceItems = arr;
                        invoice.invoiceItems = $scope.selectedInvoiceItems;

                    }
                });
        };


        $scope.saveDetails = function () {
            $scope.commentsError = '';
            if ($scope.tempArray[0].INVOICE_STATUS === 'REJECTED' && !$scope.tempArray[0].INVOICE_STATUS_COMMENTS_TEMP) {
                $scope.commentsError = 'Please enter comments';
            } else {
                //let invoiceDetails = {
                //    "invoiceNumber": $scope.tempArray[0].INVOICE_NUMBER,
                //    "invoiceStatus": $scope.tempArray[0].INVOICE_STATUS_COMMENTS_TEMP,
                //    "invoiceStatusComments": $scope.tempArray[0].INVOICE_STATUS_COMMENTS_TEMP,
                //    "sessionId": $scope.sessionId
                //};


                var ts = moment($scope.tempArray.INVOICE_DATE_1, "DD-MM-YYYY").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                let INVOICE_DATE = "/Date(" + milliseconds + "000+0530)/";

                $scope.tempArray[0].INVOICE_DATE = INVOICE_DATE;
                $scope.tempArray[0].INVOICE_STATUS_COMMENTS = $scope.tempArray[0].INVOICE_STATUS_COMMENTS_TEMP;
                $scope.tempArray[0].U_ID = $scope.userID;

                let invoiceDetails =
                {
                    "INVOICE_ARR": $scope.tempArray,
                    "doSubmitToSAP": $scope.tempArray[0].INVOICE_STATUS === 'APPROVED' ? true : false,
                    "rowId": +$scope.tempArray[0].ROW_ID,
                    "sessionId": $scope.sessionId
                };

                PRMPOService.savePOInvoice(invoiceDetails)
                    .then(function (response) {
                        if (response.errorMessage) {
                            angular.element('#ApprovalPopup').modal('hide');
                            swal("Error!", response.errorMessage, "error");
                        }
                        else {
                            angular.element('#ApprovalPopup').modal('hide');
                            swal({
                                title: "Thanks !",
                                text: "Saved Successfully",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    $scope.getInvoiceList();
                                });
                        }
                    });
            }
        };


        /*        Code Needs to be rewrite*/
    });