﻿
prmApp
    .controller('consalidatedReportCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;
            $scope.consalidatedReport = [];

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 8;
            $scope.revisedUserL1 = [];
            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();

            $scope.filtersList = {
                buyersList: []
            };
            $scope.buyerFilter = "";

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
            };

            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            //$scope.reportFromDate = '';
            //$scope.reportToDate = '';

            $scope.reportToDate = moment().format('YYYY-MM-DD');
            $scope.reportFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");


            $scope.getConsalidatedReport = function () {
                $scope.errMessage = '';
                reportingService.getConsolidatedReport($scope.reportFromDate, $scope.reportToDate)
                    .then(function (response) {
                        $scope.consalidatedReport = response;
                        $scope.consalidatedReportTemp = response;

                        let buyersTemp = [];
                        
                        $scope.consalidatedReport.forEach(function (item, index) {
                            item.closed = $scope.prmStatus($scope.isCustomer, item.closed);
                            item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                            item.reqPostedOn = $scope.GetDateconverted(item.reqPostedOn);
                            item.startTime = $scope.GetDateconverted(item.startTime);

                            if (String(item.startTime).includes('9999')) {
                                item.startTime = '';
                            }
                            if (buyersTemp.map(e => e.id).indexOf(item.uID) <= -1) {
                                buyersTemp.push({ id: item.uID, name: item.POSTED_BY_USER });
                            }
                        });
                        $scope.totalItems = $scope.consalidatedReport.length;
                        $scope.filtersList.buyersList = buyersTemp;
                        //$scope.getSubUserData();
                        $scope.buyerChange();
                    });
            };

            $scope.GetReport = function () {
                alasql('SELECT requirementID as [Requirement ID],title as [Requirement Title],POSTED_BY_USER as [Created By],PLANT_LOCATION as [Plant Location], ' +
                    'closed as Status, ' +
                    'reqPostedOn as [Posted On], quotationFreezTime as [Freez Time], startTime as [Negotiation scheduled time],IL1_vendTotalPrice as [Initial Least Price], ' +
                    'RL1_companyName as [L1 Company Name],  RL1_revVendTotalPrice as [L1 Rev Price], ' +
                    'RL2_companyName as [L2 Company Name],  RL2_revVendTotalPrice as [L2 Rev Price],  ' +
                    'basePriceSavings as [Savings],savingsPercentage as [Savings %] ' +
                    'INTO XLSX(?, { headers: true, sheetid: "ConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["ConsolidatedReport.xlsx", $scope.consalidatedReport]);


            };
            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            function getUserSettings() {
                userService.getUserSettings({ "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        let dateFilterValue = 1;
                        if (response && response.data) {
                            let dateFilter = response.data.filter(function (item) {
                                return item.key1 === 'DATE_FILTER';
                            });

                            if (dateFilter && dateFilter.length > 0) {
                                dateFilterValue = +dateFilter[0].value;
                            }
                        }

                        $scope.reportFromDate = moment().subtract(+dateFilterValue * 30, "days").format("YYYY-MM-DD");
                        $scope.getConsalidatedReport();
                    });
            }

            getUserSettings();

            $scope.buyerChange = function ()
            {
                $scope.consalidatedReport = [];
                if ($scope.consalidatedReportTemp && $scope.consalidatedReportTemp.length > 0) {
                    if ($scope.buyerFilter && $scope.buyerFilter.length > 0) {
                        var ids = _($scope.buyerFilter)
                            .filter(item => +item.id)
                            .map('id')
                            .value();
                        //var selectedUids = ids.join(',')
                        $scope.consalidatedReportTemp.forEach(function (item, index) {
                            if (ids.indexOf(item.uID) >= 0) {
                                $scope.consalidatedReport.push(item);
                            }
                        });
                        $scope.totalItems = $scope.consalidatedReport.length;
                    } else {
                        $scope.consalidatedReport = $scope.consalidatedReportTemp;
                        $scope.totalItems = $scope.consalidatedReport.length;
                    }
                }
            };

        }]);