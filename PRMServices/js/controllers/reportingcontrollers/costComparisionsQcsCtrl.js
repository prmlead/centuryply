﻿prmApp
.controller('costComparisionsQcsCtrl', ["$scope", "$state", "$log", "$stateParams", "userService", "auctionsService", "$window",
    "$timeout", "reportingService", "growlService", "workflowService", "PRMPOServices", "$location",
    function ($scope, $state, $log, $stateParams, userService, auctionsService, $window,
        $timeout, reportingService, growlService, workflowService, PRMPOServices, $location) {

        $scope.reqId = $stateParams.reqID;
        $scope.qcsID = +$stateParams.qcsID;
        $scope.companyId = userService.getUserCompanyId();
        $scope.poNumber = '';//$stateParams.poNumber;
        $scope.poItems = $stateParams.poItems;
        $scope.requirementDetails = {};
        $scope.qcsRequirementDetails;

        // $scope.old_requirementDetails = {};
        $scope.qcsCoreItems = [];
        $scope.isUOMDifferent = false;
        $scope.isTechSpecExport = false;
        $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;
        // $scope.isSuperUser = userService.getUserObj().isSuperUser;
        if (!$scope.isCustomer) {
            $state.go('home');
        }
        $scope.isQcsSaveDisable = false;
        //$scope.includeGstInCal = true;
        $scope.userId = userService.getUserId();
        $scope.sessionID = userService.getUserToken();
        $scope.qcsVendors = [];
        $scope.qcsItems = [];
        $scope.editForm = false;
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.vendorWidth = 12;

        $scope.DraftPOList = [];
        $scope.poTemplates = [
            { 'value': 'po-domestic-zsdm', 'name': 'Asset PO', 'template': 'ZSDM' },
            { 'value': 'po-import-zsim', 'name': 'Domestic PO', 'template': 'ZSIM' },
            { 'value': 'po-bonded-wh', 'name': 'Service PO', 'template': 'ZSBW' },
            { 'value': 'po-service-zssr', 'name': 'Budget PO', 'template': 'ZSSR' }
        ];

        $scope.filteredPOTemplates = [];
        $scope.stateDetails = {
            poTemplate: $scope.poTemplates[0]
        };

        $scope.jpgdatafunction = function () {
            var name = "QCS-ReqID-" + $scope.reqId + ".jpg";
            //document.getElementById("widget").innerHTML;

            html2canvas($("#widget"), {
                onrendered: function (can) {
                    thecan = can;

                    const a = document.createElement("a");
                    a.style = "display: none";
                    a.href = can.toDataURL();

                    jpgData = a.href;
                    console.log(jpgData);
                    a.download = name;
                    a.click();
                    document.getElementById("widget").style["display"] = "";
                }

            });

            //var myCanvas = $("#widget").val(null);
            //var dataURI = myCanvas[0].toDataURL();
            //console.log(dataURI);
        };




        $scope.IncoTermsEditableForOtherCharges = [];


        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                    }
                });
            });
        }

        $scope.listRequirementTaxes = [];
        $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
        $scope.uomDetails = [];

        /*region start WORKFLOW*/
        $scope.workflowList = [];
        $scope.itemWorkflow = [];
        $scope.obj = {
            auctionVendors: []
        };
        $scope.objNew = {
            auctionVendors: []
        };
        $scope.workflowObj = {};
        $scope.workflowObj.workflowID = 0;
        $scope.currentStep = 0;
        $scope.orderInfo = 0;
        $scope.assignToShow = '';
        $scope.isWorkflowCompleted = false;
        $scope.WorkflowModule = 'QCS';
        $scope.disableWFSelection = false;
        /*region end WORKFLOW*/

        $scope.vendorPrices = [];
        $scope.QCSDetails = {
            QCS_ID: 0,
            REQ_ID: $scope.reqId,
            U_ID: userService.getUserId(),
            QCS_CODE: '',
            PO_CODE: '',
            RECOMMENDATIONS: '',
            UNIT_CODE: '',
            IS_TAX_INCLUDED: '',
            IS_AUDIT: '',
            IS_VALID: 1
        };

        if ($scope.qcsID <= 0) {
            $scope.QCSDetails.IS_VALID = 1;
        }

        $scope.doPrint = false;
        $scope.printReport = function () {
            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false;
            }, 1000);
        };

        var jpgData = '';
        var jpgDataTemp = '';
        $scope.htmlToCanvasSaveLoading = false;
        $scope.htmlToCanvasSave = function (format) {
            $scope.htmlToCanvasSaveLoading = true;
            setTimeout(function () {
                try {
                    var name = "Comparisions-ReqID-" + $scope.reqId + "." + format;
                    var canvas = document.createElement("canvas");
                    if (format == 'pdf') {
                        document.getElementById("widget").style["display"] = "";
                    }
                    else {
                        document.getElementById("widget").style["display"] = "inline-block";
                    }

                    html2canvas($("#widget"), {
                        onrendered: function (canvas) {
                            theCanvas = canvas;

                            const a = document.createElement("a");
                            a.style = "display: none";
                            a.href = canvas.toDataURL();


                            // Add Image to HTML
                            //document.body.appendChild(canvas);

                            /* Save As PDF */
                            if (format == 'pdf') {
                                var imgData = canvas.toDataURL();
                                var pdf = new jsPDF();
                                pdf.addImage(imgData, 'JPEG', 0, 0);
                                pdf.save(name);
                            }
                            else {
                                a.download = name;
                                a.click();
                            }

                            // Clean up 
                            //document.body.removeChild(canvas);
                            document.getElementById("widget").style["display"] = "";
                            $scope.htmlToCanvasSaveLoading = false;
                        }
                    });
                }
                catch (err) {
                    document.getElementById("widget").style["display"] = "";
                    $scope.htmlToCanvasSaveLoading = false;
                }

            }, 500);

        };

        $scope.getTotalTax = function (quotation) {
            if (quotation) {
                if (!quotation.cGst || quotation.cGst == undefined) {
                    quotation.cGst = 0;
                }

                if (!quotation.sGst || quotation.sGst == undefined) {
                    quotation.sGst = 0;
                }

                if (!quotation.iGst || quotation.iGst == undefined) {
                    quotation.iGst = 0;
                }

                return quotation.cGst + quotation.sGst + quotation.iGst;

            } else {
                return 0;
            }
        };

        $scope.SaveQCSDetails = function (val) {
            $scope.saveqcsloading = true;
            $scope.new_requirementDetails = {};
            // $scope.old_requirementDetails_temp = {};
            $scope.new_requirementDetails = angular.copy($scope.requirementDetails);
            $scope.QCSDetails.QCS_TYPE = 'DOMESTIC';
            if (!$scope.QCSDetails.QCS_CODE) {
                $scope.QCSDetails.QCS_CODE = new Date().getUTCMilliseconds();
            }

            $scope.QCSDetails.REQ_JSON = JSON.stringify($scope.requirementDetails);
            if ($scope.old_requirementDetails == null || $scope.old_requirementDetails == undefined || $scope.old_requirementDetails == {}) {

            } else {
                $scope.old_requirementDetails_temp = JSON.parse($scope.old_requirementDetails.REQ_JSON);
            }


            var N = false;
            var audit = false;
            if ($scope.old_requirementDetails_temp == null || $scope.old_requirementDetails_temp == undefined || $scope.old_requirementDetails_temp == {}) {

            } else {
                $scope.old_requirementDetails_temp.auctionVendors.forEach(function (vend, vendIdx) {

                    vend.listRequirementItems.forEach(function (item, itemIdx) {
                        $scope.new_requirementDetails.auctionVendors.forEach(function (vend1, vend1Idx) {

                            vend1.listRequirementItems.forEach(function (I, idx) {
                                if (item.itemID == I.itemID) {
                                    if ((item.qtyDistributed != undefined && I.qtyDistributed != undefined) || (vend.payLoadFactor != undefined && vend1.payLoadFactor != undefined) || (vend.revChargeAny != undefined && vend1.revChargeAny)) {
                                        if ((parseFloat(item.qtyDistributed) != parseFloat(I.qtyDistributed)) || (parseFloat(vend.payLoadFactor) != parseFloat(vend1.payLoadFactor)) || (parseFloat(vend.revChargeAny) != parseFloat(vend1.revChargeAny))) {
                                            audit = true;
                                            return;
                                        }
                                    } else {
                                        audit = true;
                                        return;
                                    }
                                }
                            })
                        })
                    })
                })
            }


            var vendorAssignedItems = [];
            var requirementDetails_temp = JSON.parse($scope.QCSDetails.REQ_JSON);

            if (requirementDetails_temp) {
                requirementDetails_temp.auctionVendors.forEach(function (vendor, idx) {
                    vendor.listRequirementItems.forEach(function (item, itemIdx) {
                        if (item.qtyDistributed > 0) {
                            vendorAssignedItems.push({
                                QCS_ID: $stateParams.qcsID,
                                REQ_ID: $stateParams.reqID,
                                vendorID: vendor.vendorID,
                                vendorCode: vendor.vendorCode,
                                itemID: item.itemID,
                                assignedQty: item.qtyDistributed,
                                assignedPrice: item.revUnitPrice,
                                totalPrice: (item.revUnitPrice * item.qtyDistributed),
                                PRODUCT_ID: item.catalogueItemID
                            });
                        }
                    });
                });
            }

            $scope.QCSDetails.IS_AUDIT = audit;
            if ($scope.QCSDetails.CREATED_USER == null) {
                $scope.QCSDetails.CREATED_USER = $scope.userId;
            }
            if ($scope.QCSDetails.U_ID == 0 || $scope.QCSDetails.U_ID == null) {
                $scope.QCSDetails.U_ID = $scope.userId;
            }

            $scope.QCSDetails.IS_TAX_INCLUDED = $scope.requirementDetails.includeGstInCal;
            $scope.QCSDetails.REQ_TITLE = $scope.requirementDetails.title;
            var requirementItem = [];
            $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                vendor.listRequirementItems.forEach(function (reqItem) {
                    if (reqItem.isCoreProductCategory == 1) { requirementItem.push(reqItem); }
                });
            });

            var isQtyDistributed = false;
            var qtydis = [];
            if (requirementItem.length > 0) {
                qtydis = requirementItem.filter(function (item) {
                    if (item.qtyDistributed == undefined || item.qtyDistributed == null || item.qtyDistributed == "") { } else { return item.qtyDistributed; }
                });
            }

            if (qtydis.length == 0) {
                swal("Error!", "Please Enter Quantity Distribution for existing items", 'error');
                $scope.saveqcsloading = false;
                return;
            }


            var params = {
                "qcsdetails": $scope.QCSDetails,
                "sessionid": userService.getUserToken()
            };

            if ($scope.workflowObj.workflowID) {
                params.qcsdetails.WF_ID = $scope.workflowObj.workflowID;
            }

            //if (!params.qcsdetails.WF_ID > 0) {
            //    growlService.growl('Please select Workflow', "inverse");
            //    return;
            //}

            reportingService.SaveQCSDetails(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        //growlService.growl(response.errorMessage, "inverse");
                        swal("Error!", response.errorMessage, "error");
                        $scope.saveqcsloading = false;
                    }
                    else {
                        $scope.saveqcsloading = false;
                        if (vendorAssignedItems.length > 0) {
                            $scope.saveVendorAssignments(vendorAssignedItems, response.objectID);
                        }

                        $scope.goToQCSList($scope.reqId);
                        growlService.growl("Details saved successfully.", "success");

                    }
                });
        };

        $scope.checkobj = function (obj1, obj2) {
            try {
                var flag = true;
                if (Object.keys(obj1).length == Object.keys(obj2).length) {
                    for (let key in obj1) {

                        if (typeof (obj1[key]) != typeof (obj2[key])) {
                            return false;
                        }

                        if (obj1[key] == obj2[key]) {
                            continue;
                        }

                        else if (typeof (obj1[key]) == typeof (new Object())) {
                            if (!this.check_objects(obj1[key], obj2[key])) {
                                return false;
                            }
                        }
                        else {
                            return false;
                        }
                    }
                } else {
                    return false
                }
            } catch {
                return false
            }
            return flag;
        }


        $scope.isQCSFormdisabled = true;

        $scope.checkIsFormDisable = function () {
            $scope.isQCSFormdisabled = false;
            if ($scope.itemWorkflow.length == 0) {
                $scope.isQCSFormdisabled = true;
            } else {
                if (($scope.QCSDetails.CREATED_BY == +userService.getUserId() || $scope.QCSDetails.MODIFIED_BY == +userService.getUserId()) && $scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                    $scope.itemWorkflow[0].WorkflowTracks[0].status !== "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[0].order == 1 && $scope.itemWorkflow[0].workflowID > 0) {
                    $scope.isQCSFormdisabled = true;
                }
            }
        };

        $scope.GetQCSDetails = function () {
            var params = {
                "qcsid": $scope.qcsID,
                "sessionid": $scope.sessionID
            };
            reportingService.GetQCSDetails(params)
                .then(function (response) {
                    $scope.QCSDetails = response;
                    if (response != null || response != undefined) {
                        $scope.old_requirementDetails = angular.copy(response);
                    }

                    console.log("temp check start");
                    console.log($scope.old_requirementDetails);
                    console.log("temp check end");

                    if ($scope.QCSDetails.WF_ID > 0) {
                        $scope.workflowObj.workflowID = $scope.QCSDetails.WF_ID;
                    }

                    if ($scope.QCSDetails.REQ_JSON) {
                        $scope.qcsRequirementDetails = JSON.parse($scope.QCSDetails.REQ_JSON);
                    }

                    $scope.getRequirementData();
                    $scope.getItemWorkflow();
                });
        };

        if ($scope.qcsID > 0) {
            $scope.GetQCSDetails();
            //$scope.checkIsFormDisable();
        }

        $scope.goToQCSList = function (reqID) {
            var url = $state.href("list-qcs", { "reqID": $scope.reqId });
            window.open(url, '_self');
        };

        /*region start WORKFLOW*/

        $scope.getWorkflows = function () {
            workflowService.getWorkflowList()
                .then(function (response) {
                    $scope.workflowList = [];
                    $scope.workflowListTemp = response;
                    $scope.workflowListTemp.forEach(function (item, index) {
                        if (item.WorkflowModule == $scope.WorkflowModule) {
                            $scope.workflowList.push(item);
                        }
                    });

                    if (userService.getUserObj().isSuperUser) {
                        $scope.workflowList = $scope.workflowList;
                    }
                    else {
                        $scope.workflowList = $scope.workflowList.filter(function (item) {
                            return $scope.isUserBelongsToDept(item.deptID);

                        });
                    }
                });
        };

        $scope.getWorkflows();

        $scope.isUserBelongsToDept = function (deptID) {
            var isEligible = true;

            if ($scope.deptIDs.indexOf(deptID) != -1) {
                isEligible = true;
            } else {
                isEligible = false;
            }

            return isEligible;
        };

        $scope.getItemWorkflow = function () {
            workflowService.getItemWorkflow(0, $scope.qcsID, $scope.WorkflowModule)
                .then(function (response) {
                    $scope.itemWorkflow = response;
                    $scope.itemWorkflow[0].WorkflowTracks[0].newQuotations = $scope.QCSDetails.IS_NEW_QUOTATION === 1 ? true : false;
                    $scope.checkIsFormDisable();
                    if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                        $scope.currentStep = 0;

                        var count = 0;

                        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                            if (track.status == 'APPROVED' || track.status == 'HOLD') {
                                $scope.isFormdisabled = true;
                            }

                            if (track.status == 'APPROVED') {
                                $scope.isWorkflowCompleted = true;
                                $scope.orderInfo = track.order;
                                $scope.assignToShow = track.status;

                            }
                            else {
                                $scope.isWorkflowCompleted = false;
                            }


                            if (track.status == 'REJECTED' && count == 0) {

                                count = count + 1;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                                count = count + 1;
                                $scope.IsUserApproverForStage(track.approverID);
                                $scope.currentAccess = track.order;
                            }

                            if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                                $scope.currentStep = track.order;
                                return false;
                            }
                        });
                    }
                });
        };

        $scope.updateTrack = function (step, status) {
            $scope.disableAssignPR = true;
            $scope.commentsError = '';

            var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
            if (step.order == tempArray.order && status == 'APPROVED') {
                $scope.disableAssignPR = false;
            } else {
                $scope.disableAssignPR = true;
            }

            if ($scope.isReject) {
                $scope.commentsError = 'Please Save Rejected Items/Qty';
                return false;
            }

            if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
                $scope.commentsError = 'Please enter comments';
                return false;
            }

            if (step.newQuotations) {
                if (status != 'REJECTED') {
                    swal("Error!", "Please REJECT the status As Get New Quotations Has Been Done.", "error");
                    return;
                }
            }

            step.status = status;
            step.sessionID = $scope.sessionID;
            step.modifiedBy = userService.getUserId();

            step.moduleName = $scope.WorkflowModule;

            step.subModuleName = $scope.requirementDetails.title;
            step.subModuleID = $scope.reqId;

            workflowService.SaveWorkflowTrack(step)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        $scope.getItemWorkflow();
                        //location.reload();
                        $state.go('approval-qcs-list');
                    }
                });
        };

        $scope.assignWorkflow = function (moduleID) {
            workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                        $scope.isSaveDisable = false;
                    }
                    else {
                        //  $state.go('list-pr');
                    }
                });
        };

        $scope.IsUserApprover = false;

        $scope.functionResponse = false;

        $scope.IsUserApproverForStage = function (approverID) {
            workflowService.IsUserApproverForStage(approverID, userService.getUserId())
                .then(function (response) {
                    $scope.IsUserApprover = response;
                });
        };

        $scope.isApproverDisable = function (index) {

            var disable = true;

            var previousStep = {};

            $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

                if (index == stepIndex) {
                    if (stepIndex == 0) {
                        if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                    else if (stepIndex > 0) {
                        if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                            disable = true;
                        }
                        else if ($scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID) &&
                            (step.status == 'PENDING' || step.status == 'HOLD')) {
                            disable = false;
                        }
                        else {
                            disable = true;
                        }
                    }
                }
                previousStep = step;
            })

            return disable;
        };

        /*region end WORKFLOW*/


        $scope.editFormPage = function () {
            $scope.editForm = !$scope.editForm;
            $scope.requirementDetails.auctionVendors.forEach(function (vendor) {
                $scope.fieldValidation(vendor);
            });
        };

        $scope.fieldValidation = function (vendor) {
            if ($scope.editForm && vendor.INCO_TERMS) {
                auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                    .then(function (response) {
                        vendor.INCO_TERMS_CONFIG = response;

                    });
            } else {
                vendor.isEdit = false;
            }
        };

        $scope.fieldValidationList = function (vendor) {
            if (vendor.INCO_TERMS) {
                auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                    .then(function (response) {
                        vendor.INCO_TERMS_CONFIG = response;
                    });
            }
        };

        $scope.unitPriceCalculation = function (item) {
            //if (parseInt(item.revUnitPrice) > parseInt(item.unitPrice)) {
            item.revUnitPrice = item.unitPrice;
            //}
        };

        $scope.getRequirementData = function () {
            auctionsService.getReportrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId(), 'excludePriceCap': 1 })
                .then(function (response) {
                    $scope.qcsItems = [];
                    $scope.qcsCoreItems = [];
                    if (response.CB_TIME_LEFT > 0) {
                        response.status = "STARTED";
                    }

                    response.listRequirementItems.forEach(function (reqItem, index) {
                        reqItem.isVisible = true;
                        var qcsItem = {
                            itemID: reqItem.itemID,
                            itemName: reqItem.productIDorName,
                            isSelected: true,
                            prQuantity: reqItem.ITEM_PR_QUANTITY,
                            prNumber: reqItem.ITEM_PR_NUMBER,
                            isCoreProduct: reqItem.isCoreProductCategory,
                            isCoreProductCategory: reqItem.isCoreProductCategory,
                            productQuantity: reqItem.productQuantity
                        };

                        $scope.qcsItems.push(qcsItem);
                    });

                    $scope.qcsCoreItems = $scope.qcsItems.filter(function (qcsitem) {
                        return qcsitem.isCoreProductCategory;
                    });

                    if (response) {
                        $scope.qcsVendors = [];
                        $scope.requirementDetails = response;

                        $scope.requirementDetails.listRequirementItems.sort(function (a, b) {
                            return b.isCoreProductCategory - a.isCoreProductCategory;
                        });

                        $scope.vendorWidth = Math.floor(12 / ($scope.requirementDetails.auctionVendors.length + 1));
                        $scope.requirementDetails.auctionVendors = _.filter($scope.requirementDetails.auctionVendors, function (vendor) { return vendor.companyName != 'PRICE_CAP'; });
                        if ($scope.qcsID > 0 && $scope.qcsRequirementDetails) {
                            $scope.requirementDetails.customerComment = $scope.qcsRequirementDetails.customerComment;
                            $scope.requirementDetails.includeGstInCal = $scope.qcsRequirementDetails.includeGstInCal;
                            $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                console.log(item.productQuotationTemplateJson);
                                if (item.productQuotationTemplateJson) {
                                    item.productQuotationTemplateJsonObj = JSON.parse(item.productQuotationTemplateJson);
                                }

                                var tempItem = _.filter($scope.qcsRequirementDetails.listRequirementItems, function (qcsReqItem) { return qcsReqItem.itemID === item.itemID; });
                                if (tempItem && tempItem.length > 0) {
                                    item.qtyDistributed = tempItem[0].qtyDistributed;
                                }
                            });

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                vendor.isVisible = true;
                                var qcsVendor = {
                                    vendorID: vendor.vendorID,
                                    vendorCompany: vendor.companyName,
                                    isSelected: true,
                                    vendorCurrency: vendor.selectedVendorCurrency
                                };

                                $scope.qcsVendors.push(qcsVendor);
                                var tempQCSVendor = _.filter($scope.qcsRequirementDetails.auctionVendors, function (qcsVendor) { return qcsVendor.vendorID === vendor.vendorID; });
                                if (tempQCSVendor && tempQCSVendor.length > 0) {
                                    vendor.payLoadFactor = tempQCSVendor[0].payLoadFactor;
                                    vendor.revPayLoadFactor = tempQCSVendor[0].revPayLoadFactor;
                                    vendor.revChargeAny = tempQCSVendor[0].revChargeAny;
                                    vendor.customerComment = tempQCSVendor[0].customerComment;

                                    auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                        .then(function (response) {

                                            vendor.listRequirementItems.forEach(function (vendorItem, index) {

                                                if (vendorItem.productQuotationTemplateJson) {
                                                    vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);

                                                }

                                                var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                                var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                                if (newArray && newArray.length > 0) {
                                                    if (newArray[0].IS_CUSTOMER_EDIT == 1) {
                                                        if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {

                                                            vendorItem.vendorID = vendor.vendorID;

                                                            $scope.IncoTermsEditableForOtherCharges.push(vendorItem);

                                                            vendorItem.unitPrice = tempQCSVendorItem[0].unitPrice;
                                                            vendorItem.revUnitPrice = tempQCSVendorItem[0].revUnitPrice;

                                                        }
                                                    }
                                                }

                                                vendorItem.qtyDistributed = tempQCSVendorItem[0].qtyDistributed;

                                            });
                                        });
                                }
                            });
                            //if ($scope.requirementDetails != null) {
                            //    $scope.jpgdatafunction();
                            //}

                        } else {
                            $scope.requirementDetails.includeGstInCal = true;
                            $scope.requirementDetails.listRequirementItems.forEach(function (item, index) {
                                item.maxHeight = '';
                                if (item.productQuotationTemplateJson) {
                                    item.productQuotationTemplateJsonObj = JSON.parse(item.productQuotationTemplateJson);
                                }
                            });

                            $scope.requirementDetails.auctionVendors.forEach(function (vendor, index) {
                                vendor.isVisible = true;
                                var qcsVendor = {
                                    vendorID: vendor.vendorID,
                                    vendorCompany: vendor.companyName,
                                    isSelected: true,
                                    vendorCurrency: vendor.selectedVendorCurrency
                                };

                                $scope.qcsVendors.push(qcsVendor);
                                auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, userService.getUserToken())
                                    .then(function (response) {

                                        vendor.listRequirementItems.forEach(function (vendorItem, index) {
                                            if (vendorItem.productQuotationTemplateJson) {
                                                vendorItem.productQuotationTemplateJson = JSON.parse(vendorItem.productQuotationTemplateJson);
                                            }

                                            var newArray = response.filter(function (res) { return res.ProductId == vendorItem.catalogueItemID; });
                                            //var tempQCSVendorItem = _.filter(tempQCSVendor[0].listRequirementItems, function (qcsVendorItem) { return qcsVendorItem.itemID === vendorItem.itemID; });
                                            if (newArray && newArray.length > 0) {
                                                if (newArray[0].IS_CUSTOMER_EDIT == 1) {
                                                    //if (tempQCSVendorItem && tempQCSVendorItem.length > 0) {
                                                    vendorItem.vendorID = vendor.vendorID;
                                                    $scope.IncoTermsEditableForOtherCharges.push(vendorItem);
                                                    //}
                                                }
                                            }


                                        });

                                    });

                            });
                            //if ($scope.requirementDetails != null) {
                            //    $scope.jpgdatafunction();
                            //}
                        }
                    }

                    $scope.getVendorItemAssignments();
                });

        };

        $scope.getItemRank = function (item, vendor) {
            var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
            if (vendorItemPrices && vendorItemPrices.length > 0) {
                return vendorItemPrices[0].itemRank;
            }
        };

        if (!$scope.qcsID) {
            $scope.getRequirementData();
        }

        $scope.getVendorItemPrices = function (item, vendor) {

            var emptyObj = {
                unitPrice: 0,
                revUnitPrice: 0
            };
            if (vendor) {
                var vendorItemPrices = _.filter(vendor.listRequirementItems, function (vendorItem) { return vendorItem.itemID === item.itemID; });
                if (vendorItemPrices && vendorItemPrices.length > 0) {
                    if (+vendorItemPrices[0].revUnitPrice > +vendorItemPrices[0].unitPrice) {
                        vendorItemPrices[0].revUnitPrice = 0;
                        growlService.growl("Revised Price cannot be greater than initial Price", "inverse");
                    }

                    if (vendorItemPrices[0] && vendorItemPrices[0].productQuotationTemplateJson.length > 0) {
                        item.maxHeight = '218px';
                    } else {
                        item.maxHeight = '116px';
                    }
                    return vendorItemPrices[0];
                } else {
                    return emptyObj;
                }

            }
            else {
                return emptyObj;
            }
        };


        $scope.changeTotalBasic = function (vendor) {
            vendor.listRequirementItems.forEach(function (item, index) {

                ItemArray = $scope.requirementDetails.listRequirementItems.filter(function (res) { return res.itemID == item.itemID; });
                var data = $scope.getVendorItemPrices(ItemArray, vendor);
                // price += item.revUnitPrice * item.productQuantity;
            });

        }
        $scope.getVendorTotalPriceWithoutTax = function (vendor) {
            var price = 0;
            var ItemArray = [];
            var data = [];
            vendor.listRequirementItems.forEach(function (item, index) {
                var disQty = parseFloat(item.qtyDistributed)
                if (disQty > 0) {
                    price += item.revUnitPrice * disQty;
                } else {
                    price += item.revUnitPrice * item.productQuantity;
                }

            });

            return price;
        };

        $scope.getVendorTotalInitPriceWithoutTax = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += item.unitPrice * item.productQuantity;
            });

            return price;
        };

        $scope.changePayload = function (vendor) {
            vendor.revPayLoadFactor = vendor.payLoadFactor;
        };

        $scope.getVendorTotalLandingPrice = function (vendor, includeGstInCal) {
            var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;

            price += $scope.getVendorTotalPriceWithoutTax(vendor);

            if (includeGstInCal) {
                cGSTax = $scope.getAssignedQtyVendorTotalTax(vendor, 'CGST');
                iGSTax = $scope.getAssignedQtyVendorTotalTax(vendor, 'IGST');
                sGSTax = $scope.getAssignedQtyVendorTotalTax(vendor, 'SGST');

                price += cGSTax + iGSTax + sGSTax;
            }

            if (vendor.revPayLoadFactor) {
                price += (+vendor.revPayLoadFactor);
            }
            if (vendor.revChargeAny) {
                price -= (+vendor.revChargeAny);
            }
            vendor.landingPrice = price;
            return price;
        };

        $scope.getOtherCharges = function (vendor) {
            var price = 0, otherCharges = 0;

            if (vendor) {
                $scope.IncoTermsEditableForOtherCharges.forEach(function (incoItem, incoIndex) {
                    vendor.listRequirementItems.forEach(function (item, index) {
                        if (incoItem.catalogueItemID == item.catalogueItemID && vendor.vendorID == incoItem.vendorID) {
                            otherCharges += (+item.revUnitPrice);
                        }
                    });
                });

                price += otherCharges;
            }

            if (vendor.revPayLoadFactor) {
                price += (+vendor.revPayLoadFactor);
            }
            if (vendor.revChargeAny) {
                price -= (+vendor.revChargeAny);
            }

            return price;

        };

        $scope.getVendorTotalInitLandingPrice = function (vendor, includeGstInCal) {
            var price = 0, cGSTax = 0, sGSTax = 0, iGSTax = 0;

            price += $scope.getVendorTotalInitPriceWithoutTax(vendor);

            if (includeGstInCal) {
                cGSTax = $scope.getVendorInitTotalTax(vendor, 'CGST');
                iGSTax = $scope.getVendorInitTotalTax(vendor, 'IGST');
                sGSTax = $scope.getVendorInitTotalTax(vendor, 'SGST');

                price += cGSTax + iGSTax + sGSTax;
            }

            if (vendor.payLoadFactor) {
                price += (+vendor.payLoadFactor);
            }
            if (vendor.revChargeAny) {
                price -= (+vendor.revChargeAny);
            }
            vendor.initLandingPrice = price;
            return price;
        };

        $scope.getVendorTotalUnitItemPrices = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += (+item.unitPrice);
            });

            if (vendor.payLoadFactor) {
                price += (+vendor.payLoadFactor);
            }

            return price;
        };

        $scope.getVendorTotalRevUnitItemPrices = function (vendor) {
            var price = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                price += (+item.revUnitPrice);
            });

            if (vendor.revPayLoadFactor) {
                price += (+vendor.revPayLoadFactor);
            }

            return price;
        };


        $scope.getVendorTaxPercent = function (vendor, taxType, item1) {
            var Tax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (item1.itemID == item.itemID) {
                    if (taxType === 'CGST') {
                        Tax += item.cGst;
                    }

                    if (taxType === 'IGST') {
                        Tax += item.iGst;
                    }

                    if (taxType === 'SGST') {
                        Tax += item.sGst;
                    }
                }

            });

            return Tax;
        };

        $scope.getVendorTotalTax = function (vendor, taxType) {
            var totalTax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (taxType === 'CGST') {
                    totalTax += (item.revUnitPrice * item.productQuantity) * (item.cGst / 100);
                }

                if (taxType === 'IGST') {
                    totalTax += (item.revUnitPrice * item.productQuantity) * (item.iGst / 100);
                }

                if (taxType === 'SGST') {
                    totalTax += (item.revUnitPrice * item.productQuantity) * (item.sGst / 100);
                }
            });

            return totalTax;
        };


        $scope.getInitialVendorTotalTax = function (vendor, taxType) {
            var totalTax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (taxType === 'CGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.cGst / 100);
                }

                if (taxType === 'IGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.iGst / 100);
                }

                if (taxType === 'SGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.sGst / 100);
                }
            });

            return totalTax;
        };

        $scope.getAssignedQtyVendorTotalTax = function (vendor, taxType)
        {
            var totalTax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (taxType === 'CGST') {
                    totalTax += (item.revUnitPrice * parseFloat(item.qtyDistributed && item.qtyDistributed > 0 ? item.qtyDistributed : item.productQuantity)) * (item.cGst / 100);
                }

                if (taxType === 'IGST') {
                    totalTax += (item.revUnitPrice * parseFloat(item.qtyDistributed && item.qtyDistributed > 0 ? item.qtyDistributed : item.productQuantity)) * (item.iGst / 100);
                }

                if (taxType === 'SGST') {
                    totalTax += (item.revUnitPrice * parseFloat(item.qtyDistributed && item.qtyDistributed > 0 ? item.qtyDistributed : item.productQuantity)) * (item.sGst / 100);
                }
            });

            return totalTax;
        };

        $scope.getVendorInitTotalTax = function (vendor, taxType) {
            var totalTax = 0;
            vendor.listRequirementItems.forEach(function (item, index) {
                if (taxType === 'CGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.cGst / 100);
                }

                if (taxType === 'IGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.iGst / 100);
                }

                if (taxType === 'SGST') {
                    totalTax += (item.unitPrice * item.productQuantity) * (item.sGst / 100);
                }
            });

            return totalTax;
        };

        $scope.isNonCoreItemEditable = function (item, vendor) {
            var isEditable = false;

            if (item && !item.isCoreProductCategory && vendor && vendor.INCO_TERMS && vendor.INCO_TERMS_CONFIG) {
                vendor.INCO_TERMS_CONFIG.forEach(function (incoItem) {
                    vendor.listRequirementItems.forEach(function (vendorItem, itemIndex) {
                        if (item.catalogueItemID === incoItem.ProductId && item.catalogueItemID === vendorItem.catalogueItemID) {
                            if (incoItem.IS_CUSTOMER_EDIT) {
                                isEditable = true;
                            }
                        }
                    });
                });
            }

            return isEditable;
        };

        $scope.exportTechSpec = function () {
            setTimeout(function () {
                tableToExcel('testTable', 'Comparitives');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }, 3000);


        };

        $scope.isVendorVisible = function (qcsVendor) {
            qcsVendor.isSelected = !qcsVendor.isSelected;
            var vendorTemp = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.vendorID === qcsVendor.vendorID; });
            if (vendorTemp && vendorTemp.length > 0) {
                vendorTemp[0].isVisible = qcsVendor.isSelected;
            }
        };

        $scope.isReqItemVisible = function (qcsItem) {
            qcsItem.isSelected = !qcsItem.isSelected;
            var itemTemp = _.filter($scope.requirementDetails.listRequirementItems, function (reqItem) { return reqItem.itemID === qcsItem.itemID; });
            if (itemTemp && itemTemp.length > 0) {
                itemTemp[0].isVisible = qcsItem.isSelected;
            }
        };

        $scope.CalculateRankBasedOnLandingPrice = function (vendor) {
            vendor.landingPriceRank = 'NA';
            var validVendorsForRanking = _.filter($scope.requirementDetails.auctionVendors, function (auctionVendor) { return auctionVendor.landingPrice > 0 && auctionVendor.isQuotationRejected === 0; });

            if (validVendorsForRanking && validVendorsForRanking.length > 0) {
                var sortedVendors = _.orderBy(validVendorsForRanking, ['landingPrice'], ['asc']);
                var rank = _.findIndex(sortedVendors, function (vendor1) { return vendor1.vendorID === vendor.vendorID; });
                if (rank >= 0) {
                    vendor.landingPriceRank = rank + 1;
                }
            }

            return vendor.landingPriceRank;
        };

        $scope.CalculateSavingsBasedOnLandingPrice = function (vendor) {
            vendor.savings = 0;

            let allListRequirementItems = [];
            //    .concat(...$scope.requirementDetails.auctionVendors
            //    .filter(vendorItem => vendorItem.listRequirementItems != null && vendorItem.listRequirementItems.length > 0 && vendorItem.isQuotationRejected == 0)
            //    .map(vendorItem => vendorItem.listRequirementItems)
            //);


            $scope.requirementDetails.auctionVendors.forEach(function (vendorItem,vendorIndex) {

                if (vendorItem.listRequirementItems != null && vendorItem.listRequirementItems.length > 0 && vendorItem.isQuotationRejected == 0)
                {
                    vendorItem.listRequirementItems.forEach(function (vendorReqItem,vendorReqIndex)
                    {
                        if (!vendorReqItem.isRegret)
                        {
                            vendorReqItem.vendorID = vendorItem.vendorID;
                            allListRequirementItems.push(vendorReqItem);
                        }
                    });
                }

            });


            var filteredItems = allListRequirementItems; //_.filter(allListRequirementItems, function (vendor1) { return vendor1.unitPrice > 0 });

            if (filteredItems != null && filteredItems.length > 0)
            {
                vendor.listRequirementItems.forEach(function (actualVendorItem, actualVendorIndex) {

                    actualVendorItem.ITEM_SAVINGS = 0;

                    if (!actualVendorItem.isRegret && vendor.isQuotationRejected == 0)
                    {
                        var filteredVendorItem = _.filter(filteredItems, function (vendor1) { return vendor1.itemID == actualVendorItem.itemID});

                        if (filteredVendorItem != null && filteredVendorItem.length > 0) {
                            var initialItemLeastUnitPrice = _.sortBy(filteredVendorItem, ['unitPrice']);

                            var pricesDiff = (initialItemLeastUnitPrice[0].unitPrice - actualVendorItem.revUnitPrice);

                            var itemSavings = (pricesDiff * (actualVendorItem.qtyDistributed > 0 ? actualVendorItem.qtyDistributed : actualVendorItem.productQuantity))

                            var gst = (1 + (actualVendorItem.cGst + actualVendorItem.iGst + actualVendorItem.sGst) / 100);

                            actualVendorItem.ITEM_SAVINGS = (itemSavings * ($scope.requirementDetails.includeGstInCal ? gst : 1));

                        }
                    }
                });
            }

            vendor.savings = _.sumBy(vendor.listRequirementItems, 'ITEM_SAVINGS');
            
            return vendor.savings;
        };



        $scope.UpdateOtherCharges = function () {
            $scope.SaveQCSDetails(1);
            $scope.getCalculatedOtherCharges();

        };

        $scope.saveVendorOtherChargesObject = {
            vendorID: 0,
            DIFFERENTIAL_FACTOR: 0,
            requirementID: 0
        };

        $scope.getCalculatedOtherCharges = function () {

            $scope.saveVendorOtherCharges = [];

            $scope.requirementDetails.auctionVendors.forEach(function (item, index) {
                item.DIFFERENTIAL_FACTOR = 0;
                item.DIFFERENTIAL_FACTOR = $scope.getOtherCharges(item);

                $scope.saveVendorOtherChargesObject = {
                    vendorID: 0,
                    DIFFERENTIAL_FACTOR: 0,
                    requirementID: 0
                };

                $scope.saveVendorOtherChargesObject.vendorID = item.vendorID;
                $scope.saveVendorOtherChargesObject.DIFFERENTIAL_FACTOR = item.DIFFERENTIAL_FACTOR;
                $scope.saveVendorOtherChargesObject.requirementID = $scope.requirementDetails.requirementID;
                $scope.saveVendorOtherCharges.push($scope.saveVendorOtherChargesObject);
            });

            var params =
            {
                userID: userService.getUserId(),
                vendorOtherChargesArr: $scope.saveVendorOtherCharges,
                sessionID: userService.getUserToken()
            }


            auctionsService.saveVendorOtherCharges(params)
                .then(function (response) {

                    //if (response.errorMessage === "") {
                    //    growlService.growl("Other Charges Updated Successfully", "success");
                    //} else {
                    //    growlService.growl("Other Charges Updation Failed", "inverse");
                    //}

                    //console.log("in update charges");

                });



        };

        $scope.isUserBelongsToDeptandDesig = function (deptID, desigID) {
            var isEligible = true;

            if ($scope.deptIDs.indexOf(deptID) != -1 && $scope.desigIDs.indexOf(desigID) != -1) {
                isEligible = true;
            } else {
                isEligible = false;
            }

            return isEligible;
        };

        $scope.routeToExcelView = function () {
            var url = $state.href("cost-comparisions-qcs-excel", { "reqID": $scope.reqId, "qcsID": $scope.qcsID });
            window.open(url, '_blank');
        }

        $scope.GetNewQuotations = function (newQuotations) {

            var params = {
                "qcsdetails": $scope.QCSDetails,
                "isNewQuotation": newQuotations,
                "sessionid": userService.getUserToken()
            };

            reportingService.GetNewQuotations(params)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {

                        growlService.growl("New Quotations Requested successfully.", "success");
                    }

                })
        };

        $scope.enableForFirstApprover = function (step, type) {

            var enable = false;
            if (type === 'WORKFLOW') {
                if (step.order === 1 && (step.status === 'PENDING') && $scope.isUserBelongsToDeptandDesig(step.department.deptID, step.approver.desigID)) { // step.status === 'REJECTED' || 
                    enable = true;
                }
            }
            return enable;
        };

        $scope.downloadQcsjpg = function () {
            setTimeout(function () {
                //$scope.jpgdatafunction();
                var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
                tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
                tab_text = tab_text + '<x:Name>QCS REPORT</x:Name>';
                tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
                tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
                tab_text = tab_text + "<table border='1px'>";

                // Remove Dynamic hidden controls.
                var list = $('#dvContainer1').find('.ng-hide');
                for (var i = list.length - 1; 0 <= i; i--) {
                    if (list[i] && list[i].parentElement) {
                        list[i].parentElement.removeChild(list[i]);
                    }
                }

                // Getting dynamic controll values.
                var list = $('#dvContainer1').find('.dvItems');
                var values = "";
                for (var i = 0; i <= list.length - 1; i++) {
                    if (list[i] && list[i].parentElement) {
                        values += $(list[i]).text().trim();
                    }
                }
                //for (var i = 0; i <= list.length - 1; i++) {
                //    if (i == 0) {
                //        // Replace last comma and assign value.
                //        $($('#dvContainer1').find('.dvItems')[i]).text(values.replace(/,\s*$/, ""));
                //    }
                //    else {
                //        $($('#dvContainer1').find('.dvItems')[i]).text("");
                //    }
                //}
                tab_text = tab_text + $('#dvContainer1').html();
                tab_text = tab_text + '</table></body></html>';
                var data_type = 'data:application/vnd.ms-excel';
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                    if (window.navigator.msSaveBlob) {
                        var blob = new Blob([tab_text], {
                            type: "application/vnd.ms-excel;charset=utf-8;"
                        });
                        navigator.msSaveBlob(blob, 'Coupon.xls');
                    }
                } else {
                    window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));
                }



            }, 8000)
        }
        //$scope.downloadQcsjpg();

        $scope.getReport = function (name) {
            $scope.userID = userService.getUserId();
            reportingService.downloadQcsTemplate(name, $scope.reqId, $scope.qcsID, $scope.userID);
        };

        $scope.navigateToPOForm = function () {
            //angular.element('#templateSelection').modal('hide');
            $scope.filterTemplates();
            //$scope.stateDetails.poTemplate = $scope.filteredPOTemplates[0];
            $state.go($scope.stateDetails.poTemplate.value, { 'reqId': $scope.reqId, 'qcsId': $scope.qcsID, 'requirementDetails': $scope.requirementDetails, 'detailsObj': $scope.vendorAssignmentList, 'templateName': $scope.stateDetails.poTemplate.template, 'quoteLink': $location.absUrl() });
        };

        $scope.viewPODetails = function (poObject) {
            var params = {
                "ponumber": poObject.PO_NO,
                "quotno": poObject.QUOT_NO,
                "sessionid": $scope.userId
            };

            PRMPOServices.getPOItems(params)
                .then(function (response) {
                    poObject.POItems = response;
                    if (poObject.POItems && poObject.POItems.length > 0 && poObject.POItems[0].PO_TEMPLATE && poObject.POItems[0].PO_RAW_JSON) {
                        let selectedTemplate = poObject.POItems[0].PO_TEMPLATE;
                        let selectedTemplateObj = $scope.poTemplates.filter(function (temp) {
                            return temp.template === selectedTemplate;
                        });
                        let poRoute = ''
                        if (selectedTemplateObj && selectedTemplateObj.length > 0) {
                            poRoute = selectedTemplateObj.value;
                        }

                        if (poRoute) {
                            $state.go(poRoute, {
                                'reqId': poObject.POItems[0].REQ_ID, 'qcsId': poObject.POItems[0].QCS_ID,
                                'quotId': poObject.QUOT_NO ? poObject.QUOT_NO : '',
                                'requirementDetails': JSON.parse(poObject.POItems[0].QCS_REQUIREMENT_JSON),
                                'detailsObj': JSON.parse(poObject.POItems[0].QCS_VENDOR_ASSIGNMENT_JSON),
                                'templateName': selectedTemplate,
                                'quoteLink': poObject.POItems[0].ZZQANO,
                                'poRawJSON': JSON.parse(poObject.POItems[0].PO_RAW_JSON)
                            });
                        }
                    }
                });
        };

        $scope.getPOList = function () {
            var params = {
                'compid': $scope.companyId,
                'template': '',
                'vendorid': 0,
                'status': 'DRAFT',
                'creator': '',
                'plant': '',
                'purchasecode': '',
                'search': '/cost-comparisions-qcs/' + $scope.reqId + '/' + $scope.qcsID,
                'sessionid': $scope.sessionID,
                'page': 0,
                'pageSize': 10
            };

            PRMPOServices.getPOGenerateDetails(params)
                .then(function (response) {
                    $scope.DraftPOList = [];
                    if (response && response.length > 0) {
                        $scope.DraftPOList = response;
                    }
                });
        };

        if ($scope.reqId && $scope.qcsID) {
            $scope.getPOList();
        }

        $scope.checkIsPODisable = function () {
            let doShow = false;
            var approverCount = 0;
            if ($scope.itemWorkflow.length > 0) {
                approverCount = $scope.itemWorkflow[0].WorkflowTracks.length;
                if ($scope.itemWorkflow[0].WorkflowTracks.length > 0 &&
                    $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].status === "APPROVED" && $scope.itemWorkflow[0].WorkflowTracks[approverCount - 1].order == approverCount && $scope.itemWorkflow[0].workflowID > 0) {
                    doShow = true;
                }
            }

            if (doShow && $scope.requirementDetails && $scope.requirementDetails.listRequirementItems
                && $scope.requirementDetails.listRequirementItems.length > 0) {
                let prAssociatedItems = $scope.requirementDetails.listRequirementItems.filter(function (item) {
                    return item.ITEM_PR_NUMBER;
                });

                if (!prAssociatedItems || prAssociatedItems.length <= 0) {
                    doShow = false;
                }
            }

            if ($scope.vendorAssignmentList && $scope.vendorAssignmentList.length > 0 && doShow) {
                doShow = false;
                $scope.vendorAssignmentList.forEach(function (item, index) {
                    if (!item.poNumber && item.IS_DISABLE_QTY) {
                        doShow = true;
                    }
                });
            }

            return doShow;
        };

        $scope.goToVendorPo = function (vendor) {
            let itemsTemp = $scope.vendorAssignmentList.filter(function (qcsitem) {
                return !qcsitem.poID && qcsitem.vendorID == vendor.vendorID;
            });

            if (itemsTemp && itemsTemp.length > 0) {
                var url = $state.href("po", { "reqID": $stateParams.reqID, "vendorID": vendor.vendorID, "poID": 0 });
                window.open(url, '_blank');
            } else {
                growlService.growl("No items found to create PO.", "inverse");
            }
        };

        $scope.getItemVendorPrice = function (itemVendorObj) {
            if (itemVendorObj.vendorID && itemVendorObj.itemID) {

                let reqItem = $scope.requirementDetails.listRequirementItems.filter(function (item) {
                    return item.itemID === itemVendorObj.itemID;
                });

                let reqVendor = $scope.requirementDetails.auctionVendors.filter(function (vendor) {
                    return vendor.vendorID === itemVendorObj.vendorID;
                });

                itemVendorObj.assignedPrice = $scope.getVendorItemPrices(reqItem[0], reqVendor[0]).revUnitPrice;
            }

            return itemVendorObj.assignedPrice;
        };

        $scope.selectVendorForItemVendor = function (vendor) {
            $scope.vendorAssignmentList.forEach(function (item, index) {
                item.vendorID = vendor.vendorID;
                item.assignedPrice = $scope.getItemVendorPrice({ itemID: item.itemID, vendorID: vendor.vendorID });
            });
        };

        $scope.cloneItemVendorAssignment = function (itemVendorObj) {
            $scope.vendorAssignmentList.push({
                qcsVendorItemId: 0,
                vendorID: 0,
                vendorCode: '',
                vendorName: 'Select Vendor',
                itemID: itemVendorObj.itemID,
                assignedQty: itemVendorObj.assignedQty,
                assignedPrice: itemVendorObj.assignedPrice,
                poID: ''
            });
        };

        $scope.saveVendorAssignments = function (items, qcsID) {
            var itemsFinal = [];
            console.log(items);
            if (qcsID == 0 || qcsID == undefined || qcsID == null || qcsID == '') {
                QCSID = $stateParams.qcsID;
            } else {
                QCSID = qcsID;
            }
            if (items && items.length > 0) {
                items.forEach(function (item, index) {
                    itemsFinal.push({
                        QCS_ID: QCSID,
                        REQ_ID: $stateParams.reqID,
                        VENDOR_ID: item.vendorID,
                        VENDOR_CODE: item.vendorCode,
                        ITEM_ID: item.itemID,
                        ASSIGN_QTY: item.assignedQty,
                        ASSIGN_PRICE: item.assignedPrice,
                        TOTAL_PRICE: item.totalPrice,
                        PO_NUMBER: item.poNumber,
                        PRODUCT_ID: item.PRODUCT_ID
                    });
                });


                var params =
                {
                    items: itemsFinal,
                    sessionID: userService.getUserToken()
                };

                auctionsService.saveQCSVendorAssignments(params)
                    .then(function (response) {
                        //growlService.growl("Details saved successfully.", "success");
                    });
            }

        };

        $scope.getVendorItemAssignments = function () {
            $scope.qcsPOVendors = [];
            $scope.vendorAssignmentList = [];
            var params =
            {
                qcsid: $stateParams.qcsID,
                reqid: $stateParams.reqID,
                userid: 0,
                sessionid: userService.getUserToken()
            };

            auctionsService.getQCSVendorItemAssignments(params)
                .then(function (response) {
                    if (response && response.length > 0) {

                        let poItemsProductIdArr = [];
                        if ($scope.poItems && $scope.poItems.length > 0) {
                            $scope.poItems.forEach(function (item, index) {
                                poItemsProductIdArr.push(item.PRODUCT_ID);
                            });
                        }

                        response.forEach(function (item, index) {
                            if ($scope.poNumber && !item.PO_NUMBER && poItemsProductIdArr.includes(item.PRODUCT_ID)) {
                                item.PO_NUMBER = $scope.poNumber;
                            }

                            let itemTemp = $scope.qcsItems.filter(function (qcsitem) {
                                return qcsitem.itemID == item.ITEM_ID;
                            });

                            let vendorTemp = $scope.qcsVendors.filter(function (qcsvendor) {
                                return qcsvendor.vendorID == item.VENDOR_ID;
                            });

                            $scope.vendorAssignmentList.push({
                                qcsVendorItemId: item.QCS_VENDOR_ITEM_ID,
                                vendorID: item.VENDOR_ID,
                                vendorCode: item.VENDOR_CODE,
                                vendorName: vendorTemp && vendorTemp.length > 0 ? vendorTemp[0].vendorCompany : '',
                                itemID: item.ITEM_ID,
                                item: itemTemp ? itemTemp[0] : null,
                                assignedQty: item.ASSIGN_QTY,
                                assignedPrice: item.ASSIGN_PRICE,
                                totalPrice: (item.ASSIGN_QTY * item.ASSIGN_PRICE), //item.TOTAL_PRICE,
                                poID: item.PO_ID,
                                currency: vendorTemp && vendorTemp.length > 0 ? vendorTemp[0].vendorCurrency : '',
                                poNumber: item.PO_NUMBER,
                                PRODUCT_ID: item.PRODUCT_ID,
                                IS_DISABLE_QTY: item.IS_DISABLE_QTY
                            });
                        });

                        if ($scope.poNumber && $scope.qcsID) {
                            let itemsToSave = []
                            response.forEach(function (item, index) {
                                itemsToSave.push(
                                    {
                                        QCS_ID: $scope.qcsID,
                                        REQ_ID: $scope.reqId,
                                        vendorID: item.VENDOR_ID,
                                        vendorCode: item.VENDOR_CODE,
                                        itemID: item.ITEM_ID,
                                        assignedQty: item.ASSIGN_QTY,
                                        assignedPrice: item.ASSIGN_PRICE,
                                        totalPrice: (item.ASSIGN_QTY * item.ASSIGN_PRICE),
                                        poNumber: item.PO_NUMBER,
                                        PRODUCT_ID: item.PRODUCT_ID,
                                        IS_DISABLE_QTY: item.IS_DISABLE_QTY
                                    }
                                );
                            });

                            $scope.saveVendorAssignments(itemsToSave, $scope.qcsID);
                        }

                    } else {
                        $scope.requirementDetails.listRequirementItems.forEach(function (reqItem, index) {
                            if (reqItem.isCoreProductCategory > 0) {
                                $scope.requirementDetails.auctionVendors.forEach(function (reqVendor, index) {
                                    if ($scope.getItemRank(reqItem, reqVendor) == 1) {
                                        let itemTemp = $scope.qcsItems.filter(function (qcsitem) {
                                            return qcsitem.itemID == reqItem.itemID;
                                        });

                                        let vendorTemp = $scope.qcsVendors.filter(function (qcsvendor) {
                                            return qcsvendor.vendorID == reqVendor.vendorID;
                                        });

                                        //$scope.vendorAssignmentList.push({
                                        //    qcsVendorItemId: 0,
                                        //    vendorID: reqVendor.vendorID,
                                        //    vendorName: vendorTemp ? vendorTemp[0].vendorCompany : '',
                                        //    itemID: reqItem.itemID,
                                        //    assignedQty: reqItem.productQuantity,
                                        //    assignedPrice: $scope.getVendorItemPrices(reqItem, reqVendor).revUnitPrice,
                                        //    totalPrice: ($scope.getVendorItemPrices(reqItem, reqVendor).revUnitPrice * item.productQuantity),
                                        //    poID: ''
                                        //});
                                    }
                                });
                            }
                        });
                    }
                });
        };

        $scope.getItemRFQQuantity = function (itemId) {
            let quantity = 0;
            let itemTemp = $scope.qcsCoreItems.filter(function (item) {
                return item.itemID == itemId;
            });

            if (itemTemp && itemTemp.length > 0) {
                quantity = itemTemp[0].productQuantity;
            }

            return quantity;
        };

        $scope.checkQuantity = function (item, vendor) {
            var assignedQTy = 0;
            let maxAssignQty = item.ITEM_PR_QUANTITY;
            item.error = '';
            $scope.requirementDetails.auctionVendors.forEach(function (vendor, vendIdx) {
                vendor.listRequirementItems.forEach(function (vendorItem, idx) {
                    if (vendorItem.itemID == item.itemID && vendorItem.qtyDistributed) {
                        assignedQTy += parseFloat(vendorItem.qtyDistributed);
                        vendor.QTY_DISTRIBUTED_ITEM_ID = vendorItem.itemID;
                    }
                });
            });

            if (!maxAssignQty) {
                maxAssignQty = item.productQuantity;
            }

            let itemRevUnitPrice = $scope.getVendorItemPrices(item, vendor).revUnitPrice;

            if (assignedQTy > maxAssignQty || itemRevUnitPrice <= 0) {
                var data = $scope.getVendorItemPrices(item, vendor);
                $scope.requirementDetails.auctionVendors.forEach(function (vendor1, vendIdx) {
                    vendor1.listRequirementItems.forEach(function (vendorItem, idx) {
                        if (vendorItem.itemID == data.itemID && vendor.vendorID == vendor1.vendorID) {
                            vendorItem.qtyDistributed = 0;
                            vendor.QTY_DISTRIBUTED_ITEM_ID = 0;
                        }
                    });
                });

                if (itemRevUnitPrice <= 0) {
                    swal("Warning!", "Invalid product to assign quantity, please contact support.", "error");
                    item.error = "Invalid product to assign quantity, please contact support.";
                } else {
                    swal("Warning!", "Distributing Quantity should not exceed more than PR Quantity " + maxAssignQty, "error");
                    item.error = "Distributing Quantity should not exceed more than PR Quantity " + maxAssignQty;
                }
                
                $scope.isQcsSaveDisable = true;
                return;
            } else {
                $scope.isQcsSaveDisable = false;
                //calculateSavings(item, vendor);
                $scope.getVendorTotalPriceWithoutTax(vendor);
            }
        };

        $scope.filterTemplates = function () {
            $scope.filteredPOTemplates = [];
            let vendorCodeList = [];
            let containsGroupZ001 = false;
            let containsGroupZ002 = false;
            let containsGroupZ014 = false;
            let containsOthers = false;
            if ($scope.vendorAssignmentList && $scope.vendorAssignmentList.length > 0) {

                $scope.vendorAssignmentList.forEach(function (vendor, index) {
                    if (vendor.vendorCode) {
                        vendorCodeList.push(vendor.vendorCode.replace(/^0+/, ''));
                    }
                });

                vendorCodeList = _.uniq(vendorCodeList);

                if (vendorCodeList && vendorCodeList.length > 0) {
                    vendorCodeList.forEach(function (code, index) {
                        if (code.startsWith("1")) {
                            containsGroupZ001 = true;
                        }
                        else if (code.startsWith("3")) {
                            containsGroupZ002 = true;
                        }
                        else if (code.startsWith("8")) {
                            containsGroupZ014 = true;
                        } else {
                            containsOthers = true;
                        }
                    });
                } else {
                    containsOthers = true;
                }

                if (containsOthers || true) {
                    $scope.filteredPOTemplates = $scope.poTemplates;
                } else {
                    if (containsGroupZ001) {
                        let temp = $scope.poTemplates.filter(function (obj) {
                            return obj.template === 'ZSDM';
                        });

                        $scope.filteredPOTemplates.push(temp[0]);
                    }

                    if (containsGroupZ002) {
                        let temp = $scope.poTemplates.filter(function (obj) {
                            return obj.template === 'ZSIM';
                        });

                        $scope.filteredPOTemplates.push(temp[0]);
                    }

                    if (containsGroupZ014) {
                        let temp = $scope.poTemplates.filter(function (obj) {
                            return obj.template === 'ZSBW';
                        });

                        $scope.filteredPOTemplates.push(temp[0]);
                    }
                }

                if ($scope.filteredPOTemplates && $scope.filteredPOTemplates.length === 1) {
                    $scope.stateDetails = {
                        poTemplate: $scope.filteredPOTemplates[0]
                    };
                }
            }
        };


        $scope.getLabel = function (vendor)
        {
            var labelName = false;

            labelName = _.some(vendor.listRequirementItems, function (item) {
                return item.qtyDistributed > 0;
            });

            return labelName ? "Asgnd " : "Rev. Price Req "

        };

    }]);