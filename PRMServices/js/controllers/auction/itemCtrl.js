﻿prmApp

    // =========================================================================
    // AUCTION ITEM
    // =========================================================================

    .controller('itemCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService", "PRMLotReqService","PRMPRServices",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService, PRMLotReqService, PRMPRServices) {
            var liveId = "";
            $scope.tempScopeVariables = {};
            $scope.companyItemUnits = [];
            $scope.productConfig = [];
            $scope.requirementSettings = [];
            $scope.selectedcustomFieldList = [];
            $scope.selectedVendorForItemLevelPrices = {};
            var id = $stateParams.Id;
            $scope.reqId = $stateParams.Id;
            $scope.IS_CB_ENABLED = false;
            $scope.IS_CB_NO_REGRET = false;
            $scope.disableSubmitAllItemApproval = true;
            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            $scope.GetCustomerCompanyId = userService.getCustomerCompanyId();
            $scope.vendorIncoTerms = {};
            if ($scope.reqId > 0) {
                
            }
            else{
                $scope.reqId = $stateParams.reqID;
                $stateParams.Id = $stateParams.reqID;
            }
            $scope.cancelRfqObj = {
                reason: '',
                checkBoxEmail: true,
                checkBoxSms: true
            }
            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.customerType = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            $scope.regretDetails = [];
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
           
            $scope.listRequirementItemsTemp = [];
            $scope.compID = userService.getUserCompanyId();

            //#CB-0-2018-12-05
            $scope.goToCbVendor = function (vendorID) {
                $state.go('cb-vendor', { 'reqID': $stateParams.Id, 'vendorID': vendorID });
            };

            //Pagination
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 5;
            $scope.itemsPerPage2 = 5;
            $scope.maxSize = 8;
            //^Pagination

            $scope.goToAudit = function () {
                var url = $state.href("audit", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };
            $scope.CandSGstVal = [0, 2.5, 6, 9, 14];
            $scope.IGstVal = [0, 5, 12, 18, 28];
            
            $scope.numberOfItemDetails = 1;
            $scope.signalRCustomerAccess = false;
            $scope.Loding = false;
            $scope.makeaBidLoding = false;
            $scope.showTimer = false;
            $scope.userIsOwner = false;
            $scope.sessionid = userService.getUserToken();
            $scope.allItemsSelected = true;
            $scope.RevQuotationfirstvendor = "";
            $scope.disableBidButton = false;
            $scope.nonParticipatedMsg = '';
            $scope.quotationRejecteddMsg = '';
            $scope.quotationNotviewedMsg = '';
            $scope.revQuotationRejecteddMsg = '';
            $scope.revQuotationNotviewedMsg = '';
            $scope.quotationApprovedMsg = '';
            $scope.revQuotationApprovedMsg = '';
            $scope.reduceBidAmountNote = '';
            $scope.incTaxBidAmountNote = '';
            $scope.noteForBidValue = '';
            $scope.CSGSTFields = false;
            $scope.IGSTFilelds = false;
            $scope.disableDecreaseButtons = true;
            $scope.disableAddButton = true;
            $scope.NegotiationEnded = false;
            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Feilds';
            $scope.currentID = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };
            $scope.bidAttachement = [];
            $scope.bidAttachementName = "";
            $scope.bidAttachementValidation = false;
            $scope.bidPriceEmpty = false;
            $scope.bidPriceValidation = false;
            $scope.showStatusDropDown = false;
            $scope.vendorInitialPrice = 0;
            $scope.showGeneratePOButton = false;
            $scope.isDeleted = false;
            $scope.ratingForVendor = 0;
            $scope.ratingForCustomer = 0;
            $scope.participatedVendors = [];
            $scope.vendorApprovals = [];
            $scope.companyGSTInfo = [];
            $scope.requirementPRStatus = {
                MESSAGE: '',
                showStatus: false,
                validPRs: ''
            };

            /*region start WORKFLOW*/
            $scope.workflowList = [];
            $scope.itemWorkflow = [];
            $scope.workflowObj = {};
            $scope.workflowObj.workflowID = 0;
            $scope.currentStep = 0;
            $scope.orderInfo = 0;
            $scope.assignToShow = '';
            $scope.isWorkflowCompleted = false;
            $scope.WorkflowModule = 'VENDOR';
            $scope.disableWFSelection = false;
            /*region end WORKFLOW*/

            var requirementHub = SignalRFactory('', signalRHubName);
            $scope.auctionItem = {
                auctionVendors: [],
                listRequirementItems: [],
                listRequirementTaxes: []
            };

            $scope.auctionItemTemporary = {
                emailLinkVendors :[]
            };

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };
            $scope.auctionItemVendor = [];
            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }
            };

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    });
                }
            };

            /*Chat hub*/
            


            requirementHub.connection.start().done(function () {
                let signalRGroupName = 'requirementGroup' + $stateParams.Id + '_' + $scope.currentUserId;
                if ($scope.isCustomer) {
                    signalRGroupName = 'requirementGroup' + $stateParams.Id + '_CUSTOMER';
                }

                $scope.invokeSignalR('joinGroup', 'requirementGroup' + $stateParams.Id);
                //$scope.invokeSignalR('joinGroup', 'chatGroup' + $scope.reqId);
                $scope.invokeSignalR('joinGroup', signalRGroupName);

            });
            requirementHub.on('chatUpdate', function (payLoad) {
                if (payLoad && !payLoad.IsCustomer) {
                    toastr.info(payLoad.FromName + ' left a message: ' + payLoad.Message);
                }
                if (payLoad && payLoad.IsCustomer && payLoad.ToId === +userService.getUserId()) {
                    toastr.info('Customer left a message: ' + payLoad.Message);
                }
            });
        /*^Chat Hub*/

            $scope.setOptions = function () {
                toastr.options.positionClass = "toast-top-right";
                toastr.options.closeButton = true;
                toastr.options.showMethod = 'slideDown';
                toastr.options.hideMethod = 'slideUp';
                //toastr.options.newestOnTop = false;
                toastr.options.progressBar = true;
            };

            $scope.setOptions();
            


            var requirementData = {};
            $scope.quotationAttachment = null;
            $scope.days = 0;
            $scope.hours = 0;
            $scope.mins = 0;
            $scope.NegotiationSettings = {
                negotiationDuration: ''
            };
            $scope.divfix = {};
            $scope.divfix3 = {};
            $scope.divfix1 = {};
            $scope.divfix2 = {};
            $scope.divfixMakeabid = {};
            $scope.divfixMakeabidError = {};
            $scope.boxfix = {};
            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.priceCapValue = 0;
            $scope.auctionItem.isUnitPriceBidding = 1;
            $scope.$on("$destroy", function () {
                //$log.info('disconecting signalR');
                requirementHub.stop();
                //$log.info('disconected signalR');
            });
            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000, height: 500 });
            };

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
                $scope.invokeSignalR('UpdateTime', parties);
            };

            $scope.subIemsColumnsVisibility = {
                hideSPECIFICATION: true,
                hideQUANTITY: true,
                hideTAX: true,
                hidePRICE: true
            };

            $scope.newPriceToBeQuoted = 0;

            $scope.reduceBidAmount = function () {
                if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.auctionItem.minPrice > $scope.reduceBidValue) {
                    $("#reduceBidValue").val($scope.reduceBidValue);


                    $("#makebidvalue").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));
                    $("#reduceBidValue1").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));

                    //console.log($scope.auctionItem.minPrice - $scope.reduceBidValue);

                    //angular.element($event.target).parent().addClass('fg-line fg-toggled');
                }
                else {
                    $("#makebidvalue").val($scope.auctionItem.minPrice);
                    //swal("Error!", "Invalid bid value.", "error");
                    $("#reduceBidValue1").val($scope.auctionItem.minPrice);
                    $scope.reduceBidValue = 0;
                    $("#reduceBidValue").val($scope.reduceBidValue);
                }


            };

            $scope.bidAmount = function () {
                if ($scope.vendorBidPrice != "" && $scope.vendorBidPrice >= 0 && $scope.auctionItem.minPrice > $scope.vendorBidPrice) {
                    $("#reduceBidValue").val($scope.auctionItem.minPrice - $scope.vendorBidPrice);
                    /*angular.element($event.target).parent().addClass('fg-line fg-toggled');*/
                }
                else {
                    $("#reduceBidValue").val(0);
                    swal("Error!", "Invalid bid value.", "error");
                    $scope.vendorBidPrice = 0;
                }
            };

            $scope.formRequest = {
                selectedVendor: {},
                rankLevel: 'BOTH',
                priceCapValue: 0,
                priceCapValueMsg: ''
            };

            $scope.selectVendor = function () {
                var selVendID = $scope.formRequest.selectedVendor.vendorID;
                var winVendID = $scope.auctionItem.auctionVendors[0].vendorID;
                if (!$scope.formRequest.selectedVendor.reason && selVendID != winVendID) {
                    growlService.growl("Please enter the reason for choosing the particular vendor", "inverse");
                    return false;
                }
                var params = {
                    userID: userService.getUserId(),
                    vendorID: $scope.formRequest.selectedVendor.vendorID,
                    reqID: $scope.auctionItem.requirementID,
                    reason: $scope.formRequest.selectedVendor.reason ? $scope.formRequest.selectedVendor.reason : (selVendID != winVendID ? $scope.formRequest.selectedVendor.reason : ""),
                    sessionID: userService.getUserToken()
                }
                auctionsService.selectVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Vendor " + response.userInfo.firstName + " " + response.userInfo.lastName + " has been selected for the final  ", "inverse");
                            $scope.getData();
                        }
                    })
            }

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'QuotationDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                if ($scope.auctionItem.isDiscountQuotation == 0) {
                    var name = '';
                    if ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') {
                        name = 'UNIT_QUOTATION_' + $scope.reqId;
                    } else {
                        name = 'UNIT_BIDDING_' + $scope.reqId;
                    }
                    reportingService.downloadTemplate(name, userService.getUserId(), $scope.reqId);
                    //  alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitPrice as UnitPrice, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                }
                if ($scope.auctionItem.isDiscountQuotation == 1) {
                    alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitMRP as UnitMRP, unitDiscount as UnitDiscount, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                }

            }

            $scope.rateVendor = function (vendorID) {
                var params = {
                    uID: userService.getUserId(),
                    userID: vendorID,
                    rating: $scope.ratingForVendor,
                    sessionID: userService.getUserToken()
                }
                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    })
            }

            $scope.rateCustomer = function () {
                var params = {
                    uID: userService.getUserId(),
                    userID: $scope.auctionItem.customerID,
                    rating: $scope.ratingForCustomer,
                    sessionID: userService.getUserToken()
                }
                auctionsService.rateVendor(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Rating saved successfully. You can edit your rating as well.", "inverse");
                        }
                    });
            };

            $scope.makeABidDisable = false;

            $scope.companyINCOTerms = [];



            $scope.makeaBid1 = function () {
                let isValidItemPrice = true;
                let isValidItemPriceReduction = true;
                $scope.makeABidDisable = true;
                $(".fileinput").fileinput("clear");
                var bidPrice = $("#makebidvalue").val();
                let containsItemsPriceMoreThanCeilingPrice = false;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                    vendorItem.onlyBasePrice = 0;
                    if (!$scope.auctionItem.inclusiveTax) {
                        vendorItem.onlyBasePrice = (vendorItem.revUnitPrice * vendorItem.productQuantity);
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                    if (vendorItem.revUnitPrice) {
                        var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                            return reqItem.itemID === vendorItem.itemID;
                        });

                        if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld && +vendorItem.revUnitPrice > requirementItem[0].ceilingPrice) {
                            vendorItem.revUnitPrice = vendorItem.revUnitPriceOld ? vendorItem.revUnitPriceOld : vendorItem.revUnitPrice;
                            isValidItemPrice = false;
                            $scope.makeABidDisable = false;
                            return false;
                        } else if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld && +vendorItem.revUnitPriceOld > requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice <= requirementItem[0].ceilingPrice) {
                            if (vendorItem.revUnitPrice < (0.75 * requirementItem[0].ceilingPrice)) {
                                vendorItem.revUnitPrice = vendorItem.revUnitPriceOld ? vendorItem.revUnitPriceOld : vendorItem.revUnitPrice;
                                isValidItemPriceReduction = false;
                                $scope.makeABidDisable = false;
                                return false;
                            }
                        }
                    }

                    if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPriceOld > requirementItem[0].ceilingPrice) {
                        containsItemsPriceMoreThanCeilingPrice = true;
                    }
                });




                if (!isValidItemPrice) {
                    swal("Error!", "Your item revised amount should be less than Item Ceiling amount", 'error');
                    $scope.makeABidDisable = false;
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    return false;
                }

                if (!isValidItemPriceReduction) {
                    swal("Error!", "You are reducing more than 25% of current item amount. The Maximum reduction amount per item should not exceed more than 25% from item ceiling amount", 'error');
                    $scope.makeABidDisable = false;
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    return false;
                }

                if (bidPrice == "" || Number(bidPrice) <= 0) {
                    $scope.bidPriceEmpty = true;
                    $scope.bidPriceValidation = false;
                    $("#makebidvalue").val("");
                    $scope.makeABidDisable = false;
                    return false;
                } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice) {
                    $scope.bidPriceValidation = true;
                    $scope.bidPriceEmpty = false;
                    $scope.makeABidDisable = false;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidPriceValidation = false;
                    $scope.bidPriceEmpty = false;
                }
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                if (bidPrice > $scope.auctionItem.auctionVendors[0].runningPrice - $scope.auctionItem.minBidAmount) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Error!", "Your amount must be at least " + $scope.auctionItem.minBidAmount + " less than your previous bid.", 'error');
                    $scope.makeABidDisable = false;
                    return false;
                }
                if (bidPrice < (75 * $scope.auctionItem.auctionVendors[0].runningPrice / 100) && !containsItemsPriceMoreThanCeilingPrice) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Maximum Reduction Error!", " You are reducing more than 25% of current bid amount. The Maximum reduction amount per bid should not exceed more than 25% from current bid amount  " + $scope.auctionItem.minPrice + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                    $scope.makeABidDisable = false;
                    return false;
                }
                else {
                    var params = {};
                    params.reqID = parseInt(id);
                    params.sessionID = userService.getUserToken();
                    params.userID = parseInt(userService.getUserId());
                    params.price = $scope.precisionRound(parseFloat(bidPrice), 2);
                    params.quotationName = $scope.bidAttachementName;
                    params.ignorevalidations = containsItemsPriceMoreThanCeilingPrice;
                    params.basePrice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'onlyBasePrice');

                    params.quotationObject = $scope.auctionItemVendor.listRequirementItems;
                    params.vendorBidPrice = $scope.revvendorBidPrice;
                    params.itemrevtotalprice = $scope.revtotalprice;


                    //params.freightCharges = $scope.revfreightCharges;
                    //params.freightChargesTaxPercentage = $scope.freightChargesTaxPercentage;
                    //params.freightChargesWithTax = $scope.freightChargesWithTax;

                    //params.packingCharges = $scope.revpackingCharges;
                    //params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                    //params.packingChargesWithTax = $scope.revpackingChargesWithTax;

                    //params.installationCharges = $scope.revinstallationCharges;
                    //params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                    //params.installationChargesWithTax = $scope.revinstallationChargesWithTax;


                    if ($scope.auctionItem.isUnitPriceBidding == 1) {
                        $scope.items = {
                            itemsList: $scope.auctionItemVendor.listRequirementItems,
                            userID: userService.getUserId(),
                            reqID: params.reqID,
                            price: $scope.revtotalprice,
                            vendorBidPrice: $scope.revvendorBidPrice,

                            //freightCharges: $scope.revfreightCharges,
                            //freightChargesTaxPercentage: $scope.freightChargesTaxPercentage,
                            //freightChargesWithTax: $scope.freightChargesWithTax,

                            //packingCharges: $scope.revpackingCharges,
                            //packingChargesTaxPercentage: $scope.packingChargesTaxPercentage,
                            //packingChargesWithTax: $scope.revpackingChargesWithTax,

                            //installationCharges: $scope.revinstallationCharges,
                            //installationChargesTaxPercentage: $scope.installationChargesTaxPercentage,
                            //installationChargesWithTax: $scope.revinstallationChargesWithTax,

                            //revfreightCharges: $scope.revfreightCharges,
                            //revfreightChargesWithTax: $scope.revfreightChargesWithTax,

                            //revpackingCharges: $scope.revpackingCharges,
                            //revpackingChargesWithTax: $scope.revpackingChargesWithTax,

                            //revinstallationCharges: $scope.revinstallationCharges,
                            //revinstallationChargesWithTax: $scope.revinstallationChargesWithTax,


                            //itemRevFreightCharges: $scope.itemRevFreightCharges
                        };
                    }

                    requirementHub.invoke('MakeBid', params, function (req) {

                        if (req.errorMessage == '') {
                            $scope.makeABidDisable = false;

                            swal("Thanks !", "Your bidding process has been successfully updated", "success");
                            $("#clientsideupload").val(null);
                            $("#clientsideupload").val("");

                            if ($scope.auctionItemVendor.listRequirementItems.length > 20) {
                                if ($scope.auctionItem.isUnitPriceBidding == 1) {
                                    if ($scope.items.revinstallationCharges == null || $scope.items.revinstallationCharges == '' || $scope.items.revinstallationCharges == undefined || isNaN($scope.items.revinstallationCharges)) {
                                        $scope.items.revinstallationCharges = 0;
                                    }
                                    $scope.items.itemsList.forEach(function (item, itemIndexs) {
                                        item.ITEM_DELV_DATE1 = $scope.convertDate(item.ITEM_DELV_DATE1);
                                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                                subItem.dateCreated = "/Date(1561000200000+0530)/";
                                                subItem.dateModified = "/Date(1561000200000+0530)/";
                                            });
                                        }
                                    });
                                    auctionsService.SaveRunningItemPrice($scope.items)
                                        .then(function (response) {
                                            if (response.errorMessage != "") {
                                                growlService.growl(response.errorMessage, "inverse");
                                            } else {
                                                $scope.recalculate('', 0, false);
                                            }
                                        });
                                }
                            }



                            $scope.reduceBidValue = "";
                            $("#makebidvalue").val("");
                            $("#reduceBidValue").val("");

                        } else {
                            if (req.errorMessage != '') {

                                $scope.getData();
                                $scope.makeABidDisable = false;
                                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                requirementHub.invoke('CheckRequirement', parties, function () {

                                    $scope.reduceBidValue = "";
                                    $("#makebidvalue").val("");
                                    $("#reduceBidValue").val("");

                                    var htmlContent = 'We have some one in this range <h3>' + req.errorMessage + ' </h3> Kindly Quote some other value.'

                                    swal("Cancelled", "", "error");
                                    if (req.errorMessage && req.errorMessage.toLowerCase() === 'auction is not running anymore.') {
                                        $(".sweet-alert h2").html("<h3 style='color:red'>Oops..!! You cannot make the bid as the negotiation window for this requirement is now closed.</h3>");
                                        //$(".sweet-alert h2").html("<h3 style='color:red'>" + req.errorMessage + " </h3>");
                                    } else {
                                        $(".sweet-alert h2").html("oops...! Your Price is Too close to another Bid <br> We have some one in this range <h3 style='color:red'>" + req.errorMessage + " </h3> Kindly Quote some other value.");
                                    }

                                    $("#clientsideupload").val(null);
                                    $("#clientsideupload").val("");
                                });

                            } else {
                                $scope.makeABidDisable = false;
                                swal("Error!", req.errorMessage, "error");
                            }
                        }
                    });

                }

            };

            $scope.recalculate = function (subMethodName, receiverId, showswal) {

                if (subMethodName == null || subMethodName == '' || subMethodName == undefined) {
                    subMethodName = '';
                }

                if (receiverId >= 0) {

                } else {
                    receiverId = -1;
                }

                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken() + "$" + subMethodName + "$" + receiverId;
                $scope.invokeSignalR('CheckRequirement', parties);
                if (showswal) {
                    swal("done!", "a refresh command has been sent to everyone.", "success");
                }
            };

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                } else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                } else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;

                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                } else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                } else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                } else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                }
                //else if ($scope.auctionItem.status == "Negotiation Completed") {
                //    $scope.startBtns = false;
                //}
                else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                    $scope.userIsOwner = true;
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                        swal("Error!", "Live negotiation Access Denined", "error");
                        $state.go('home');
                    }
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                        swal("Error!", "View Requirement Access Denined", "error");
                        $state.go('home');
                    }
                    $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                    $scope.options.push($scope.auctionItem.status);
                }
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        //$log.debug(dateFromServer);
                        var curDate = dateFromServer;

                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;

                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                        //$scope.auctionItem.postedOn = new Date(parseFloat($scope.auctionItem.postedOn.substr(6)));
                        //var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                        //var newDate = new Date(parseInt(parseInt(date)));
                        //$scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                        //var date1 = $scope.auctionItem.deliveryTime.split('+')[0].split('(')[1];
                        //var newDate1 = new Date(parseInt(parseInt(date1)));
                        //$scope.auctionItem.deliveryTime = newDate1.toString().replace('Z', '');

                        //var date2 = $scope.auctionItem.quotationFreezTime.split('+')[0].split('(')[1];
                        //var newDate2 = new Date(parseInt(parseInt(date2)));
                        //$scope.auctionItem.quotationFreezTime = newDate2.toString().replace('Z', '');

                        // // #INTERNATIONALIZATION-0-2019-02-12
                        $scope.auctionItem.postedOn = userService.toLocalDate($scope.auctionItem.postedOn);

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        //$scope.auctionItem.deliveryTime = $scope.auctionItem.deliveryTime.split("GMT")[0];
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i]

                            if (vendor.vendorID == userService.getUserId() && vendor.quotationUrl == "") {
                                $scope.quotationStatus = false;
                                $scope.quotationUploaded = false;
                            } else {
                                $scope.quotationStatus = true;
                                if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                                    $scope.quotationUploaded = true;
                                }
                                $scope.quotationUrl = vendor.quotationUrl;
                                $scope.revquotationUrl = vendor.revquotationUrl;
                                $scope.vendorQuotedPrice = vendor.runningPrice;
                            }
                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            //$scope.auctionItem.minPrice = runningMinPrice;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if (userService.getUserId() == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                //$scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 'NA';
                                $scope.nonParticipatedMsg = 'You have missed an opportunity to participate in this Negotiation.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation submitted to the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1 && $scope.auctionItem.status == "Negotiation Ended") {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1 && $scope.auctionItem.status == 'Negotiation Ended') {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation submitted to the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                //$scope.quotationApprovedMsg = 'Your Quotation Approved by the customer.';
                                $scope.quotationApprovedMsg = 'Your quotation has been shortlisted for further process.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },

                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be reduce from bid Amount.';
                //$scope.incTaxBidAmountNote = 'Please Enter the consolidate bid value including tax amount.';
                $scope.incTaxBidAmountNote = '';
                //$scope.noteForBidValue = 'NOTE : If you fill one field, other will be autocalculated.';
                $scope.noteForBidValue = '';

                if ($scope.auctionItem.status == "STARTED") {
                    liveId = "live";
                }
                else {
                    liveId = "";
                }

            }

            $scope.vendorBidPrice = 0;
            $scope.revvendorBidPrice = 0;
            $scope.auctionStarted = true;
            $scope.customerBtns = true;
            $scope.showVendorTable = true;
            $scope.quotationStatus = true;
            $scope.toprankerName = "";
            $scope.vendorRank = 0;
            $scope.quotationUrl = "";
            $scope.revquotationUrl = "";
            $scope.vendorBtns = false;
            $scope.vendorQuotedPrice = 0;
            $scope.startBtns = false;
            $scope.commentsvalidation = false;
            $scope.enableMakeBids = false;
            $scope.price = "";
            $scope.startTime = '';
            $scope.customerID = userService.getUserId();

            $scope.priceSwitch = 0;

            $scope.poDetails = {};

            $scope.bidHistory = {};

            $scope.GetBidHistory = function () {
                auctionsService.GetBidHistory({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.bidHistory = response;
                    });
            }

            $scope.vendorID = 0;

            $scope.validity = '';


            if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)') {
                $scope.validity = '1 Day';
            }
            if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)') {
                $scope.validity = '2 Days';
            }
            if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 3 Days)') {
                $scope.validity = '3 Days';
            }
            if ($scope.auctionItem.urgency == 'Low (Will Take 7 Days Time)') {
                $scope.validity = '7 Days';
            }
            $scope.warranty = 'As Per OEM';
            $scope.duration = $scope.auctionItem.deliveryTime;
            $scope.payment = $scope.auctionItem.paymentTerms;
            $scope.gstNumber = $scope.auctionItem.gstNumber;
            $scope.isQuotationRejected = -1;
            $scope.starttimecondition1 = 0;
            $scope.starttimecondition2 = 0;
            $scope.revQuotationUrl1 = 0;
            $scope.L1QuotationUrl = 0;
            $scope.notviewedcompanynames = '';
            $scope.starreturn = false;
            $scope.customerListRequirementTerms = [];
            $scope.customerDeliveryList = [];
            $scope.customerPaymentlist = [];
            $scope.listTerms = [];
            $scope.listRequirementTerms = [];
            $scope.deliveryRadio = false;
            $scope.deliveryList = [];
            $scope.paymentRadio = false;
            $scope.paymentlist = [];
            $scope.vendorsFromPRM = 1;
            $scope.vendorsFromSelf = 2;
            $scope.totalprice = 0;
            $scope.taxs = 0;
            $scope.vendorTaxes = 0;
            $scope.discountAmount = 0;
            $scope.totalpriceinctaxfreight = 0;
            $scope.vendorBidPriceWithoutDiscount = 0;
            $scope.revtotalprice = 0;
            $scope.revtaxs = 0;
            $scope.revvendorTaxes = $scope.vendorTaxes;
            $scope.revtotalpriceinctaxfreight = 0;
            $scope.priceValidationsVendor = '';
            $scope.calculatedSumOfAllTaxes = 0;
            $scope.taxValidation = false;
            $scope.ItemPriceValidation = false;
            $scope.discountfreightValidation = false;
            $scope.gstValidation = false;
            $scope.freightCharges = 0;
            $scope.revfreightCharges = 0;
            $scope.packingCharges = 0;
            $scope.revpackingCharges = 0;
            $scope.installationCharges = 0;
            $scope.revinstallationCharges = 0;

            $scope.additionalSavings = 0;

            $scope.getpricecomparison = function () {
                auctionsService.getpricecomparison({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.priceCompObj = response;

                        $scope.totalItemsL1Price = 0;
                        $scope.totalItemsMinimunPrice = 0;
                        $scope.negotiationSavings = 0;
                        $scope.savingsByLeastBidder = 0;
                        $scope.savingsByItemMinPrice = 0;

                        if ($scope.priceCompObj && $scope.priceCompObj.priceCompareObject && $scope.priceCompObj.priceCompareObject.length > 0) {
                            for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                                $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                                $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                            }
                        }

                        $scope.negotiationSavings = $scope.priceCompObj.requirement.savings;
                        $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                        $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                        $scope.additionalSavings = 0;

                        $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                    })
            }

            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;
                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                        else {

                            $scope.customerListRequirementTerms.forEach(function (item, index) {

                                var obj = {
                                    currentTime: item.currentTime,
                                    errorMessage: item.errorMessage,
                                    isRevised: item.isRevised,
                                    refReqTermID: item.reqTermsID,
                                    reqID: item.reqID,
                                    reqTermsDays: item.reqTermsDays,
                                    reqTermsID: 0,
                                    reqTermsPercent: item.reqTermsPercent,
                                    reqTermsType: item.reqTermsType,
                                    sessionID: item.sessionID,
                                    userID: item.userID
                                };

                                $scope.listRequirementTerms.push(obj);

                            });

                            $scope.listRequirementTerms.forEach(function (item, index) {

                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }

                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }

                    });
            };

            $scope.GetRequirementTermsCustomer = function () {
                auctionsService.GetRequirementTerms($scope.auctionItem.customerID, $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {

                            $scope.customerListRequirementTerms = [];
                            $scope.customerDeliveryList = [];
                            $scope.customerPaymentlist = [];

                            $scope.customerListRequirementTerms = response;

                            $scope.customerListRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.customerDeliveryList.push(item);
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    else if (item.reqTermsDays == 0) {
                                        item.paymentType = '';
                                    }

                                    $scope.customerPaymentlist.push(item);
                                }
                            });
                        }


                        //$scope.GetRequirementTerms();


                    });
            };

            $scope.getUserCredentials = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getusercredentials?sessionid=' + userService.getUserToken() + "&userid=" + userService.getUserId(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {

                    $scope.CredentialsResponce = response.data;

                    $scope.CredentialsResponce.forEach(function (cred, credIndex) {
                        if (cred.fileType == "STN") {
                            $scope.gstNumber = cred.credentialID;
                        }
                    });
                    
                }, function (result) {

                });
            };

            $scope.convertToRevORcounter = '';
            $scope.colspanDynamic = 13;
            $scope.getData = function (methodName, callerID) {
                auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.setAuctionInitializer(response, methodName, callerID);
                            $scope.GetReqPRItems();
                            auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS", userService.getUserToken())
                                .then(function (unitResponse) {
                                    $scope.companyItemUnits = unitResponse;
                                });
                        }
                    });
                //$scope.$broadcast('timer-start');
            };

            //Get Min reduction amount based on min quotation amount of 1%
            $scope.getMinReductionAmount = function () {
                if ($scope.auctionItem.minBidAmount > 0) {
                    $scope.NegotiationSettings.minReductionAmount = $scope.auctionItem.minBidAmount;
                    $scope.NegotiationSettings.rankComparision = $scope.auctionItem.minVendorComparision;
                }
                else {
                    var amount = $scope.NegotiationSettings.minReductionAmount;
                    if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        var tempAuctionVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                            return (vendor.runningPrice > 0 && vendor.initialPrice > 0 && vendor.companyName != 'PRICE_CAP' && vendor.isQuotationRejected == 0);
                        });

                        var temp = _.minBy(tempAuctionVendors, 'initialPrice');
                        if (temp && temp.initialPrice && temp.initialPrice > 0) {
                            amount = temp.initialPrice * 0.005;
                        }
                    }
                    $scope.NegotiationSettings.minReductionAmount = amount;
                    $scope.NegotiationSettings.rankComparision = amount;

                }
            };


            $scope.CheckDisable = false;
            $scope.existingVendors = [];

            $scope.setAuctionInitializer = function (response, methodName, callerID) {
                $scope.convertToRevORcounter = '';
                let currentRequirementSettings = {
                    INCO_TERMS: '',
                    selectedVendorCurrency: '',
                    isVendAckChecked: ''
                };

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    currentRequirementSettings.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;
                    currentRequirementSettings.selectedVendorCurrency = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;
                    currentRequirementSettings.isVendAckChecked = $scope.auctionItem.auctionVendors[0].isVendAckChecked;
                }

                $scope.auctionItem = response;
                if ($scope.auctionItem.IS_CB_ENABLED == false) {
                    $scope.convertToRevORcounter = 'Convert to Counter Bid';
                } else if ($scope.auctionItem.IS_CB_ENABLED == true) {
                    $scope.convertToRevORcounter = 'Convert to Reverse Bid';
                }
                $scope.auctionItemTemporary = response;
                $scope.auctionItem.listRequirementItems = _.orderBy($scope.auctionItem.listRequirementItems, ['isCoreProductCategory'], ['desc']);

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors[0].INCO_TERMS = currentRequirementSettings.INCO_TERMS ? currentRequirementSettings.INCO_TERMS : $scope.auctionItem.auctionVendors[0].INCO_TERMS;
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency = currentRequirementSettings.selectedVendorCurrency ? currentRequirementSettings.selectedVendorCurrency : $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;
                    $scope.auctionItem.auctionVendors[0].isVendAckChecked = currentRequirementSettings.isVendAckChecked ? currentRequirementSettings.isVendAckChecked : $scope.auctionItem.auctionVendors[0].isVendAckChecked;
                }

                if ($scope.auctionItemTemporary) {
                    $scope.auctionItemTemporary.emailLinkVendors = $scope.auctionItemTemporary.auctionVendors.filter(function (item) {
                        //item.listWorkflowsTemp.push($scope.workflowList);
                        item.listWorkflows = [];
                        if (item.isEmailSent == undefined || item.isEmailSent == '') {
                            item.isEmailSent = false;
                        } else {
                            item.isEmailSent = true;
                        }

                        if (item.companyName != 'PRICE_CAP')
                        {
                            $scope.existingVendors.push(item.vendorID);
                        }
                        
                        return item.companyName != 'PRICE_CAP';
                    });
                    
                    // $scope.getWorkflows();

                }
                

                $scope.CB_END_TIME = $scope.auctionItem.CB_END_TIME;
                //#CB-0-2018-12-05
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors &&
                    $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].revVendorTotalPriceCB > 0 && 
                    //$scope.auctionItem.auctionVendors[0].FREEZE_CB == false &&
                    $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 &&
                    $scope.auctionItem.IS_CB_ENABLED == 1 && 
                    $scope.auctionItem.IS_CB_COMPLETED == false) {
                    $scope.goToCbVendor($scope.auctionItem.auctionVendors[0].vendorID);
                }

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0
                    && $scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                    $scope.colspanDynamic = 20;
                } else {
                    $scope.colspanDynamic = 13;
                }
                //ack code start
                if ($scope.auctionItem.auctionVendors.length > 0) {
                    if ($scope.auctionItem.auctionVendors[0].quotationUrl == '' || $scope.auctionItem.auctionVendors[0].quotationUrl == undefined) {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = false;
                    } else {
                        $scope.auctionItem.auctionVendors[0].isVendAckChecked = true;
                        $scope.CheckDisable = true;
                    }
                }
                
                    //ack code end
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    currentRequirementSettings.isVendAckChecked = $scope.auctionItem.auctionVendors[0].isVendAckChecked;
                }
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors[0].isVendAckChecked = currentRequirementSettings.isVendAckChecked ? currentRequirementSettings.isVendAckChecked : $scope.auctionItem.auctionVendors[0].isVendAckChecked;
                }

                $scope.IS_CB_ENABLED = $scope.auctionItem.IS_CB_ENABLED;
                $scope.IS_CB_NO_REGRET = $scope.auctionItem.IS_CB_NO_REGRET;
                $scope.auctionItem.isUOMDifferent = false;
                if (callerID == userService.getUserId() || callerID == undefined) {
                    $scope.auctionItemVendor = $scope.auctionItem;
                    if ($scope.auctionItemVendor.listRequirementItems != null) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item.isEdit = true;                            
                            if (item.cGst > 0 || item.sGst > 0) {
                                $scope.IGSTFields = true;
                                $scope.CSGSTFields = false;
                            } else if (item.iGst > 0) {
                                $scope.IGSTFields = false;
                                $scope.CSGSTFields = true;
                            }
                            item.oldRevUnitPrice = item.revUnitPrice;
                            item.TemperoryRevUnitPrice = 0;
                            item.TemperoryRevUnitPrice = item.revUnitPrice;
                            item.vendorUnits = (item.vendorUnits == '' || item.vendorUnits == undefined) ? item.productQuantityIn : item.vendorUnits;
                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                //$scope.auctionItem.isUOMDifferent = true;
                            }
                        });
                    }
                }

                $scope.isCurrency = false;
                $scope.fieldValidation();
                //$scope.reductionPricePercent();
                
                $scope.notviewedcompanynames = '';

                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndex) {
                    if (item.productQuotationTemplateJson && item.productQuotationTemplateJson !== '' && item.productQuotationTemplateJson) {
                        item.productQuotationTemplateArray = item.productQuotationTemplateArray = JSON.parse(item.productQuotationTemplateJson.replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t"));//JSON.parse(item.productQuotationTemplateJson);
                        item.productQuotationTemplateArray = item.productQuotationTemplateArray.filter(function (prodObj) {
                            if (prodObj) {
                                return prodObj;
                            }
                        });
                    } else {
                        item.productQuotationTemplateArray = [];
                    }

                    
                });



                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                        $scope.auctionItem.auctionVendors[0].listRequirementItems.forEach(function (i, indx) {
                            if (item.itemID && i.itemID && item.itemID == i.itemID) {
                                //console.log('ItemId:' + item.itemID + '--Rank:' + i.itemRank);
                                item.itemRank = i.itemRank;
                            }
                        });
                    }
                   // item.vendorUnits = (item.vendorUnits == '' || item.vendorUnits == undefined) ? item.productQuantityIn : item.vendorUnits;
                });

                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (!item.bestPrice) {
                        item.bestPrice = 0;
                    }

                    item.quotationRejectedComment = item.quotationRejectedComment.replace("Approved-", "");
                    item.quotationRejectedComment = item.quotationRejectedComment.replace("Reject Comments-", "");
                    if (item.quotationRejectedComment) {
                        item.quotationRejectedComment = item.quotationRejectedComment.trim();
                    }

                    item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Approved-", "");
                    item.revQuotationRejectedComment = item.revQuotationRejectedComment.replace("Reject Comments-", "");
                    if (item.revQuotationRejectedComment) {
                        item.revQuotationRejectedComment = item.revQuotationRejectedComment.trim();
                    }
                    

                    if ($scope.auctionItem.LAST_BID_ID == item.vendorID && 
                        ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED' || $scope.auctionItem.status == 'UNCONFIRMED')) {
                        item.vendorColorStyle = { 'background-color': 'lightgreen' };
                    }
                    else if (item.FREEZE_CB == true) {
                        item.vendorColorStyle = { 'background-color': 'rgb(255, 228, 225)' };
                    }
                    else if (item.VEND_FREEZE_CB == true) {
                        item.vendorColorStyle = { 'background-color': 'rgb(176, 224, 230)' };
                        //rgb(176, 224, 230)
                        //#B0E0E6
                    }
                    else {
                        item.vendorColorStyle = {};
                    }

                    item.isQuotationRejectedDB = item.isQuotationRejected;
                    item.isRevQuotationRejectedDB = item.isRevQuotationRejected;

                    item.bestPrice = 0;
                    item.hasRegretItems = false;
                    item.listRequirementItems.forEach(function (item2, index) {
                        //Core products only
                        //if (item2.isCoreProductCategory && $scope.auctionItem.lotId <= 0) {
                        if (item2.isCoreProductCategory || true) {
                            item.bestPrice += (item2.revUnitPrice * item2.productQuantity);
                        }

                        if (item2.isRegret) {
                            item.hasRegretItems = true;
                        }

                        if (item2.productQuantityIn.toUpperCase() != item2.vendorUnits.toUpperCase()) {
                            //item.isUOMDifferent = true;
                        }

                        if (item2.productQuotationTemplateJson && item2.productQuotationTemplateJson !== '' && item2.productQuotationTemplateJson) {
                            item2.productQuotationTemplateArray = JSON.parse(item2.productQuotationTemplateJson.replace(/\s+/g, ""));
                        }
                        else {
                            item2.productQuotationTemplateArray = [];
                        }

                    });

                    if (!$scope.multipleAttachmentsList) {
                        $scope.multipleAttachmentsList = [];
                    }
                    if (!item.multipleAttachmentsList) {
                        item.multipleAttachmentsList = [];
                    }
                    //item.multipleAttachmentsArray = item.multipleAttachments;
                    if (item.multipleAttachments != '' && item.multipleAttachments != null && item.multipleAttachments != undefined) {
                        var multipleAttachmentsList = item.multipleAttachments.split(',');
                        item.multipleAttachmentsList = [];
                        $scope.multipleAttachmentsList = [];
                        multipleAttachmentsList.forEach(function (att, index) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: att
                            };

                            item.multipleAttachmentsList.push(fileUpload);
                            $scope.multipleAttachmentsList.push(fileUpload);
                        })

                    }

                    if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                        $scope.Loding = false;
                        $scope.notviewedcompanynames += item.companyName + ', ';
                        $scope.starreturn = true;
                    }

                    if (!$scope.isCustomer && (item.quotationUrl == '' || item.quotationUrl == null || item.quotationUrl == undefined)) {
                        $scope.getUserCredentials();
                    }

                })
                $scope.selectedcurr = '';
                if ($scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
                    $scope.selectedcurr = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;
                }


                if (!$scope.auctionItem.multipleAttachments) {
                    $scope.auctionItem.multipleAttachments = [];
                }
                $scope.auctionItem.attFile = response.attachmentName;
                if ($scope.auctionItem.attFile != '' && $scope.auctionItem.attFile != null && $scope.auctionItem.attFile != undefined) {


                    var attchArray = $scope.auctionItem.attFile.split(',');

                    attchArray.forEach(function (att, index) {

                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: att
                        };

                        $scope.auctionItem.multipleAttachments.push(fileUpload);
                    })

                }





                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);

                if ($scope.auctionItem.listRequirementTaxes.length == 0) {
                    $scope.auctionItem.listRequirementTaxes = $scope.listRequirementTaxes;
                }
                else {
                    $scope.listRequirementTaxes = $scope.auctionItem.listRequirementTaxes;
                }

                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

                $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;

                $scope.NegotiationSettings = $scope.auctionItem.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                if (!response.auctionVendors || response.auctionVendors.length == 0) {
                    $scope.auctionItem.auctionVendors = [];
                    $scope.auctionItem.description = "";
                }

                // // #INTERNATIONALIZATION-0-2019-02-12
                $scope.auctionItem.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
                $scope.auctionItem.expStartTime = userService.toLocalDate($scope.auctionItem.expStartTime);
                $scope.auctionItem.cancelDate = userService.toLocalDate($scope.auctionItem.cancelDate);

         


                if (($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) && (callerID == userService.getUserId() || callerID == undefined)) {
                    $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                    $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;
                    
                    $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                    $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                    $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                    $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                    $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                    $scope.gstNumber = $scope.auctionItem.auctionVendors[0].gstNumber;
                    $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                    $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties;

                    $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                    $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;

                    $scope.freightCharges = $scope.auctionItem.auctionVendors[0].freightCharges;
                    $scope.freightChargesTaxPercentage = $scope.auctionItem.auctionVendors[0].freightChargesTaxPercentage;
                    $scope.freightChargesWithTax = $scope.auctionItem.auctionVendors[0].freightChargesWithTax;

                    $scope.packingCharges = $scope.auctionItem.auctionVendors[0].packingCharges;
                    $scope.packingChargesTaxPercentage = $scope.auctionItem.auctionVendors[0].packingChargesTaxPercentage;
                    $scope.packingChargesWithTax = $scope.auctionItem.auctionVendors[0].packingChargesWithTax;

                    $scope.installationCharges = $scope.auctionItem.auctionVendors[0].installationCharges;
                    $scope.installationChargesTaxPercentage = $scope.auctionItem.auctionVendors[0].installationChargesTaxPercentage;
                    $scope.installationChargesWithTax = $scope.auctionItem.auctionVendors[0].installationChargesWithTax;

                    $scope.revfreightCharges = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                    $scope.TemperoryRevfreightCharges = $scope.revfreightCharges;
                    $scope.freightChargesWithTax = $scope.auctionItem.auctionVendors[0].freightChargesWithTax;

                    $scope.revpackingCharges = $scope.auctionItem.auctionVendors[0].revpackingCharges;
                    $scope.TemperoryRevpackingCharges = $scope.revpackingCharges;
                    $scope.revpackingChargesWithTax = $scope.auctionItem.auctionVendors[0].revpackingChargesWithTax;

                    $scope.revinstallationCharges = $scope.auctionItem.auctionVendors[0].revinstallationCharges;
                    $scope.TemperoryRevinstallationCharges = $scope.revinstallationCharges;
                    $scope.revinstallationChargesWithTax = $scope.auctionItem.auctionVendors[0].revinstallationChargesWithTax;

                    $scope.resetValues();

                    //$("#totalprice").val(0);
                    //$("#revtotalprice").val(0);
                    //if (callerID == userService.getUserId() || callerID == undefined) {
                    //    $("#packingCharges").val($scope.packingCharges);
                    //    $("#packingChargesTaxPercentage").val($scope.packingChargesTaxPercentage);
                    //    $("#packingChargesWithTax").val($scope.packingChargesWithTax);
                    //    $("#revpackingCharges").val($scope.revpackingCharges);
                    //    $("#revpackingChargesWithTax").val($scope.revpackingChargesWithTax);

                    //    $("#installationCharges").val($scope.installationCharges);
                    //    $("#installationChargesTaxPercentage").val($scope.installationChargesTaxPercentage);
                    //    $("#installationChargesWithTax").val($scope.installationChargesWithTax);
                    //    $("#revinstallationCharges").val($scope.revinstallationCharges);
                    //    $("#revinstallationChargesWithTax").val($scope.revinstallationChargesWithTax);

                    //    $("#freightCharges").val($scope.freightCharges);
                    //    $("#freightChargesTaxPercentage").val($scope.freightChargesTaxPercentage);
                    //    $("#freightChargesWithTax").val($scope.freightChargesWithTax);
                    //    $("#revfreightCharges").val($scope.revfreightCharges);
                    //    $("#revfreightChargesWithTax").val($scope.revfreightChargesWithTax);
                    //}


                    //$scope.revfreightCharges = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                    //$("#revfreightCharges").val($scope.auctionItem.auctionVendors[0].revfreightCharges);
                    //$scope.TemperoryRevfreightCharges = $scope.revfreightCharges;


                    //$scope.revinstallationCharges = $scope.auctionItem.auctionVendors[0].revinstallationCharges;
                    //$("#revinstallationCharges").val($scope.auctionItem.auctionVendors[0].revinstallationCharges);
                    //$scope.TemperoryRevinstallationCharges = $scope.revinstallationCharges;


                    //$scope.revfreightCharges = $scope.auctionItem.auctionVendors[0].revfreightCharges;
                    //$("#revfreightCharges").val($scope.auctionItem.auctionVendors[0].revfreightCharges);
                    //$scope.TemperoryRevfreightCharges = $scope.revfreightCharges;





                    $scope.selectedVendorCurrency = $scope.auctionItem.auctionVendors[0].selectedVendorCurrency;

                    if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                        $scope.validity = '1 Day';
                    }
                    else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                        $scope.validity = '2 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 3 Days)' && $scope.validity == '') {
                        $scope.validity = '3 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Low (Will Take 7 Days Time)' && $scope.validity == '') {
                        $scope.validity = '7 Days';
                    }
                    if ($scope.warranty == '') {
                        $scope.warranty = 'As Per OEM';
                    }
                    if ($scope.duration == '') {
                        //$scope.duration = new moment($scope.auctionItem.deliveryTime).format("DD-MM-YYYY");
                        $scope.duration = $scope.auctionItem.deliveryTime;
                    }
                    if ($scope.payment == '') {
                        $scope.payment = $scope.auctionItem.paymentTerms;
                    }

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        item.ITEM_DELV_DATE = $scope.GetDateconverted(item.ITEM_DELV_DATE1);
                        if (item.ITEM_DELV_DATE) {
                            if (item.ITEM_DELV_DATE.includes('9999') || item.ITEM_DELV_DATE.includes('1000') || item.ITEM_DELV_DATE.includes('10000')) {
                                item.ITEM_DELV_DATE = '';
                            } else {
                                item.ITEM_DELV_DATE = $scope.GetDateconverted(item.ITEM_DELV_DATE1);
                            }
                        }
                        if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                            //$scope.auctionItem.isUOMDifferent = true;
                        }

                        item.Gst = item.cGst + item.sGst + item.iGst;

                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.Gst = item.cGst + item.sGst;
                            //item.costPrice = (1 + (item.Gst / 100));                            
                            item = $scope.handlePrecision(item, 2);
                            //item.costPrice = item.costPrice + (item.unitDiscount / 100);
                            //item.costPrice = item.unitMRP / item.costPrice;
                            //item.revCostPrice = item.unitMRP / (1 + (item.Gst / 100) + (item.revUnitDiscount / 100));


                            //item.costPrice = 100 * item.unitMRP / (100 + item.Gst)*(1 + (item.unitDiscount / 100));
                            //item.revCostPrice = 100 * item.unitMRP / (100 + item.Gst) * (1 + (item.revUnitDiscount / 100));


                            item.costPrice = (item.unitMRP * 100 * 100) / ((item.unitDiscount * 100) + 10000 + (item.unitDiscount * item.Gst) + (item.Gst * 100));
                            item.revCostPrice = (item.unitMRP * 100 * 100) / ((item.revUnitDiscount * 100) + 10000 + (item.revUnitDiscount * item.Gst) + (item.Gst * 100));


                            item.netPrice = item.costPrice * (1 + item.Gst / 100);
                            item.revnetPrice = item.revCostPrice * (1 + item.Gst / 100);

                            item.marginAmount = item.unitMRP - item.netPrice;
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;

                            item = $scope.handlePrecision(item, 2);
                        }
                        if (!item.selectedVendorID || item.selectedVendorID == 0) {
                            $scope.allItemsSelected = false;
                        }
                        if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                            if (item.selectedVendorID > 0) {
                                var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                if (vendorArray.length > 0) {
                                    item.selectedVendor = vendorArray[0];
                                }
                            } else {
                                item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                            }
                        }
                    })
                    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                        $scope.disableAddButton = false;
                    }

                    if ($scope.auctionItem && $scope.auctionItem.status && $scope.auctionItem.status == "STARTED") {
                        $scope.$broadcast('timer-set-countdown-seconds', +$scope.auctionItem.timeLeft);
                    }

                    if (callerID == userService.getUserId() || callerID == undefined) {
                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;

                        $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2);

                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                    }

                    $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;


                    $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;

                    if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                    }

                    $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;


                    //= $filter('filter')($scope.auctionItem.auctionVendors, {revquotationUrl > ''});
                    //$scope.RevQuotationfirstvendor 
                   // $scope.isRejectedPOEnable = true;
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.revquotationUrl != '') {
                            $scope.RevQuotationfirstvendor = item.revquotationUrl;
                        }
                        //if (item.isRevQuotationRejected == 1) {
                        //    $scope.isRejectedPOEnable = false;
                        //} else {
                        //    $scope.isRejectedPOEnable = true;
                        //}

                    })

                }

                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                    $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                }
                //$scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;

                $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                });

                $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                })

                $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                })


                if ($scope.auctionItem.status != 'DELETED' && $scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'STARTED' && $scope.auctionItem.status != 'NOTSTARTED' && $scope.auctionItem.status != 'Negotiation Ended') {
                    auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                        .then(function (response) {
                            $scope.poDetails = response;
                            //$scope.updatedeliverydateparams.date = $scope.poDetails.expectedDelivery;
                            if (response != undefined) {
                                //var date = $scope.poDetails.expectedDelivery.split('+')[0].split('(')[1];
                                var date1 = moment($scope.poDetails.expectedDelivery).format('DD/MM/YYYY  HH:mm');
                                $scope.updatedeliverydateparams.date = date1;

                                //var date1 = $scope.poDetails.paymentScheduleDate.split('+')[0].split('(')[1];
                                //var newDate1 = new Date(parseInt(parseInt(date1)));
                                var date2 = moment($scope.poDetails.expPaymentDate).format('DD/MM/YYYY  HH:mm');
                                $scope.updatepaymentdateparams.date = date2;
                                if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                                    $scope.updatepaymentdateparams.date = 'Payment Date';
                                }
                            }
                        });
                }

                //  $scope.auctionItem.description = $scope.auctionItem.description.replace("\u000a", "\n")
                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");


                var id = parseInt(userService.getUserId());
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == id;
                });
                if (id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();
                    //auctionsService.getcomments({ "reqid": $stateParams.Id, "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    //    .then(function (response) {
                    //        $scope.Comments = response;
                    //    });

                }


                $scope.quotationApprovedColor = {};
                $scope.quotationNotApprovedColor = {};

                if ($scope.auctionItem.auctionVendors != null && $scope.auctionItem.auctionVendors.length > 0)
                {
                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 && $scope.auctionItem.isUnitPriceBidding == 1) {
                        $scope.quotationApprovedColor = {
                            'background-color': '#f5b2b2'
                        };
                        $scope.quotationNotApprovedColor = {};
                    } else if ($scope.auctionItem.isUnitPriceBidding == 1) {
                        $scope.quotationNotApprovedColor = {
                            'background-color': '#f5b2b2'
                        };
                        $scope.quotationApprovedColor = {};
                    }
                }

                //$scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice);
                $scope.getMinReductionAmount();
                if ($scope.isCustomer) {
                    //$scope.getpricecomparison();
                }

                if ($scope.isCustomer) {
                    $scope.listRequirementItemsTemp = $scope.auctionItem.listRequirementItems;
                } else {
                    $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems;
                }

                $scope.totalItems = ($scope.listRequirementItemsTemp) ? $scope.listRequirementItemsTemp.length : 0;

                //setTimeout(function () {
                //    try {
                //        if ($scope.isCustomer && $scope.auctionItem && $scope.auctionItem.status && $scope.auctionItem.status == "STARTED") {
                //            var parties = $scope.reqId + "$" + userService.getUserId() + "$" + "0" + "$" + userService.getUserToken();
                //            $scope.invokeSignalR('UpdateTimeRefresh', parties, function () {
                //                //console.log(parties);
                //            });
                //        }
                //    }
                //    catch (err) { }
                //}, 10000);
            };

            $scope.getData();
            // first call

            if ($scope.isCustomer) {
                auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }
                    });
            }

            //$scope.GetBidHistory();

            $scope.AmountSaved = 0;

            $scope.selectItemVendor = function (itemID, vendorID) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    itemID: itemID,
                    vendorID: vendorID,
                    sessionID: userService.getUserToken()
                };
                auctionsService.itemwiseselectvendor(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            var index = -1;
                            for (var i = 0, len = $scope.auctionItemVendor.listRequirementItems.length; i < len; i++) {
                                if ($scope.auctionItemVendor.listRequirementItems[i].itemID === itemID) {
                                    index = i;
                                    $scope.auctionItemVendor.listRequirementItems[i].selectedVendorID = vendorID;
                                    break;
                                }
                                if ($scope.auctionItemVendor.listRequirementItems[i].selectedVendor == 0) {
                                    $scope.allItemsSelected = false;
                                } else {
                                    $scope.allItemsSelected = true;
                                }
                            }
                            swal("Success!", "This item has been assigned to Vendor", "success");
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.handlePrecision = function (item, precision) {

                //item.sGst = $scope.precisionRound(parseFloat(item.sGst), precision);
                //item.cGst = $scope.precisionRound(parseFloat(item.cGst), precision);
                //item.Gst = $scope.precisionRound(parseFloat(item.Gst), precision);
                //item.unitMRP = $scope.precisionRound(parseFloat(item.unitMRP), precision);
                item.unitDiscount = $scope.precisionRound(parseFloat(item.unitDiscount), precision + 6);
                item.revUnitDiscount = $scope.precisionRound(parseFloat(item.revUnitDiscount), precision + 6);
                //item.costPrice = $scope.precisionRound(parseFloat(item.costPrice), precision);
                //item.revCostPrice = $scope.precisionRound(parseFloat(item.revCostPrice), precision);
                //item.netPrice = $scope.precisionRound(parseFloat(item.netPrice), precision);
                //item.revnetPrice = $scope.precisionRound(parseFloat(item.revnetPrice), precision);
                //item.marginAmount = $scope.precisionRound(parseFloat(item.marginAmount), precision);
                //item.revmarginAmount = $scope.precisionRound(parseFloat(item.revmarginAmount), precision);
                //$scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), precision);
                //$scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), precision);
                //return item;
                return item;
            };

            $scope.$watch('auctionItemVendor.listRequirementItems', function (oldVal, newVal) {
                //$log.debug("items list has changed");
                if (!newVal) { return; }
                newVal.forEach(function (item, index) {
                    if (!item.selectedVendorID || item.selectedVendorID == 0) {
                        $scope.allItemsSelected = false;
                    }
                })
            })

            $scope.generatePDFonHTML = function () {
                auctionsService.getdate()
                    .then(function (response) {
                        var date = new Date(parseInt(response.substr(6)));
                        var obj = $scope.auctionItem;
                        $scope.POTemplate = "<div id='POTemplate' style='display:none;'><html><head><title>PRM360</title><style>.date{margin-left: 850px;}.to{margin-left: 250px;}.name{margin-left: 300px;}.sub{margin-left: 450px;}img{position: absolute; left: 750px; top:75px; z-index: -1;}</style></head><body><header><br><br><br><img src='acads360.jpg' width='50' height='50'><h1 align='center'>PRM360<img </h1></header><br><div class='date'><p><b>Date:</b> " + date + "</p><p><b>PO No:</b> " + obj.requirementID + "</p></div><div class='to'><p>To,</p><p><b>" + obj.CompanyName + ",</b></p><p><b>" + obj.deliveryLocation + ".</b></p></div><p class='name'><b>Hello </b> " + obj.auctionVendors[0].vendorName + "</p><p class='sub'><b>Sub:</b> " + obj.title + "</p><p align='center'><b>Bill of Material</b></p><table border='1' cellpadding='2' style='width:60%' align='center'><tr><th>Product Name</th><th>Description</th><th>Price</th></tr><tr><td>" + obj.title + "</td><td>" + obj.description + "</td><td>" + obj.price + "</td></tr></table><p class='to'><b>Terms & Conditions</b></p><div class='name'> <p>1. Payment : " + obj.paymentTerms + ".</p><p>2. Delivery : " + obj.deliveryLocation + ".</p><p>3. Tax : " + obj.taxes + ".</p></div><p class='to'><b>Billing and Shipping Address:</b></p><p class='to'>Savvy Associates, # 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad - 500048</p><p align=center>This is a system generated PO, henceforth sign and seal is not required.</p><br><footer class='to'>M/s. Savvy Associates, H.No: 3-4-174/21/2, Radha Krishna Nagar, Attapur, Hyderguda, Hyderabad – 48Contact,M: 91-9949245791.,<br>E: savvyassociates@gmail.com.<br><b>URL:</b> www.savvyassociates.com. </footer></body></html></div>";
                        var content = document.getElementById('content');
                        content.insertAdjacentHTML('beforeend', $scope.POTemplate);
                    })
            }

            $scope.generatePOasPDF = function (divName) {
                $scope.generatePDFonHTML();
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;

                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                    var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    popupWin.window.focus();
                    popupWin.document.write('<!DOCTYPE html><html><head>' +
                        '<link rel="stylesheet" type="text/css" href="style.css" />' +
                        '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                    popupWin.onbeforeunload = function (event) {
                        popupWin.close();
                        return '.\n';
                    };
                    popupWin.onabort = function (event) {
                        popupWin.document.close();
                        popupWin.close();
                    }
                } else {
                    var popupWin = window.open('', '_blank', 'width=800,height=600');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                    popupWin.document.close();
                }
                popupWin.document.close();
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            //doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })

                return true;
            }

            $scope.generatePO = function () {
                var doc = new jsPDF();
                doc.setFontSize(40);
                //doc.text(40, 30, "Octocat loves jsPDF", 4);
                /*doc.fromHTML($("#POTemplate")[0], 15, 15, {
                    "width": 170,
                    function() {
                        $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                    }
                })*/

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    status: 'PO Processing',
                    type: "WINVENDOR",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                            doc.save("DOC.PDF");
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.generatePOinServer = function () {
                if ($scope.POTemplate == "") {
                    $scope.generatePDFonHTML();

                }
                var doc = new jsPDF('p', 'in', 'letter');
                var specialElementHandlers = {};
                var doc = new jsPDF();
                //doc.setFontSize(40);
                doc.fromHTML($scope.POTemplate, 0.5, 0.5, {
                    'width': 7.5, // max width of content on PDF
                });
                //doc.save("DOC.PDF");
                doc.output("dataurl");
                $scope.POFile = $.makeArray(new Uint8Array(doc.output('arraybuffer')));
                var params = {
                    POfile: $scope.POFile,
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    POfileName: 'PO_req_' + $scope.auctionItem.requirementID + '.pdf',
                    sessionID: userService.getUserToken()
                }

                auctionsService.generatePOinServer(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.showStatusDropDown = true;
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.updateStatus = function (status) {
                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    status: status,
                    type: "ALLVENDORS",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updateStatus(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    });
            };

            $scope.uploadquotationsfromexcel = function (status) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken(),
                    quotationAttachment: $scope.quotationAttachment
                };
                auctionsService.uploadquotationsfromexcel(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                        }
                    });
            };


            $scope.uploadclientsidequotation = function (type) {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken(),
                    quotationAttachment: $scope.quotationAttachment
                };
                auctionsService.uploadclientsidequotation(params)
                    .then(function (response) {
                        if (response && response.length > 0 && !response[0].errorMessage) {
                            var validation = true;
                            var itemID = 0;
                            for (item in $scope.auctionItem.listRequirementItems) {
                                item = (+item);
                                var newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                if (newItem) {
                                    if ($scope.auctionItem.status == "STARTED" && $scope.auctionItem.listRequirementItems[item].revUnitPrice < newItem.revUnitPrice) {
                                        validation = false;
                                        itemID = newItem.itemID;
                                    }
                                }
                            }
                            var isSCGST = false;
                            var isIGST = false;
                            
                            if (validation) {
                                
                                
                                var TempnewItem = [];

                                for (I in $scope.auctionItem.listRequirementItems) {
                                    I = (+I);
                                    I_ITEM = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[I].itemID })[0];
                                    if (I_ITEM) {
                                        if (!I_ITEM.isRegret) {
                                            TempnewItem.push(I_ITEM);
                                        }
                                    }
                                }
                                if (TempnewItem && TempnewItem.length > 0) {
                                    if (TempnewItem[0].sGst > 0 && TempnewItem[0].cGst > 0) {
                                        isSCGST = true;
                                    } else if (TempnewItem[0].iGst > 0) {
                                        isIGST = true;
                                    }
                                    var requirementItem = [];
                                    if (isSCGST) {
                                        requirementItem = TempnewItem.filter(function (reqItem) {
                                            return reqItem.iGst;
                                        });
                                        if (requirementItem.length > 0) {
                                            swal("Error", 'Please select SGST and CGST for all items as you selected for item' +' '+ (TempnewItem[0].productIDorName), 'error');
                                            $(".fileinput").fileinput("clear");
                                            return;
                                        }
                                    }
                                    if (isIGST) {
                                        requirementItem = TempnewItem.filter(function (reqItem) {
                                            return reqItem.sGst > 0 && reqItem.cGst > 0;
                                        });
                                        if (requirementItem.length > 0) {
                                            swal("Error", 'Please select IGST for all items as you selected for item' +' '+ (TempnewItem[0].productIDorName), 'error');
                                            $(".fileinput").fileinput("clear");
                                            return;
                                        }
                                    }
                                }
                                for (item in $scope.auctionItem.listRequirementItems) {
                                    item = (+item);
                                    newItem = $filter('filter')(response, { itemID: $scope.auctionItem.listRequirementItems[item].itemID })[0];
                                    
                                    if (newItem) {
                                        $scope.auctionItem.listRequirementItems[item].unitPrice = newItem.unitPrice;
                                        $scope.auctionItem.listRequirementItems[item].revUnitPrice = newItem.revUnitPrice;
                                        $scope.auctionItem.listRequirementItems[item].cGst = newItem.cGst;
                                        $scope.auctionItem.listRequirementItems[item].sGst = newItem.sGst;
                                        $scope.auctionItem.listRequirementItems[item].iGst = newItem.iGst;
                                        $scope.auctionItem.listRequirementItems[item].Gst = newItem.iGst + newItem.sGst + newItem.cGst;
                                        $scope.auctionItem.listRequirementItems[item].itemLevelInitialComments = newItem.itemLevelInitialComments;
                                        $scope.auctionItem.listRequirementItems[item].isRegret = newItem.isRegret;
                                        $scope.auctionItem.listRequirementItems[item].regretComments = newItem.regretComments;
                                        $scope.auctionItem.listRequirementItems[item].hsnCode = newItem.hsnCode;
                                        $scope.auctionItem.listRequirementItems[item].ITEM_DELV_DATE = $scope.GetDateconverted(newItem.ITEM_DELV_DATE1);
                                        if (!newItem.isCoreProductCategory) {
                                            $scope.auctionItem.listRequirementItems[item].ITEM_DELV_DATE = null;
                                        }
                                        if (newItem.isRegret) {
                                            $scope.regretHandle($scope.auctionItem.listRequirementItems[item]);
                                            console.log(newItem.isRegret);
                                        }

                                        if (newItem.productQuotationTemplateJson && !newItem.isRegret) {
                                            $scope.auctionItem.listRequirementItems[item].productQuotationTemplateArray = JSON.parse(newItem.productQuotationTemplateJson);

                                            //TempnewItem.push(newItem);
                                        }
                                    }
                                }
                            } else {
                                swal("Error", 'New bid for item cannot be lower than the old bid. Please check the values in the excel sheet for item: ' + itemID, 'error');
                            }
                            
                            $scope.auctionItemVendor.listRequirementItems = $scope.auctionItem.listRequirementItems;
                            $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems;
                            $scope.totalItems = $scope.listRequirementItemsTemp.length;
                            $scope.unitPriceCalculation();
                            $scope.getTotalPrice($scope.discountAmount, 0, $scope.totalprice);
                            $scope.getRevTotalPrice(0, $scope.revtotalprice);
                            swal("Verify", "Please verify and Submit your prices!", "success");
                         
                            $scope.fieldValidation();

                            $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                                if (item.itemLevelInitialComments) {
                                    item.itemLevelInitialComments = item.itemLevelInitialComments.replace(/\'/gi, "");
                                    item.itemLevelInitialComments = item.itemLevelInitialComments.replace(/\"/gi, "");
                                    item.itemLevelInitialComments = item.itemLevelInitialComments.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                                    item.itemLevelInitialComments = item.itemLevelInitialComments.replace(/(\r\n|\n|\r)/gm, "");
                                    item.itemLevelInitialComments = item.itemLevelInitialComments.replace(/\t/g, '');
                                }
                                if (item.regretComments) {
                                    item.regretComments = item.regretComments.replace(/\'/gi, "");
                                    item.regretComments = item.regretComments.replace(/\"/gi, "");
                                    item.regretComments = item.regretComments.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                                    item.regretComments = item.regretComments.replace(/(\r\n|\n|\r)/gm, "");
                                    item.regretComments = item.regretComments.replace(/\t/g, '');
                                }
                                if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                                    item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                        if (subItem.SPECIFICATION) {
                                            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\'/gi, "");
                                            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\"/gi, "");
                                            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                                            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/(\r\n|\n|\r)/gm, "");
                                            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\t/g, '');
                                        }
                                    });

                                    item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);

                                } else {
                                    item.productQuotationTemplateJson = '';
                                }
                            });

                            return;
                        } else if (response && response.length > 0) {
                            swal("Error!", response[0].errorMessage, "error");
                        }

                        $("#clientsideupload").val(null);
                        $("#quotationBulkUpload").val(null);
                    });
            };

            $scope.getrevisedquotations = function () {
                var params = {
                    reqID: $scope.auctionItem.requirementID,
                    userID: userService.getUserId(),
                    sessionID: userService.getUserToken()
                };
                auctionsService.getrevisedquotations(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            location.reload();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                            //console.log(response.errorMessage);
                        }

                    })
            }

            $scope.updatedeliverydateparams = {
                date: ''
            };

            $scope.updatedeliverdate = function () {
                var ts = moment($scope.updatedeliverydateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatedeliverydateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    date: $scope.updatedeliverydateparams.date,
                    type: "DELIVERY",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updatedeliverdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })

            }

            $scope.updatepaymentdateparams = {
                date: ''
            };

            $scope.updatepaymentdate = function () {
                var ts = moment($scope.updatepaymentdateparams.date, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var deliveryDate = new Date(m);
                var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                $scope.updatepaymentdateparams.date = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    reqid: $scope.auctionItem.requirementID,
                    userid: userService.getUserId(),
                    date: $scope.updatepaymentdateparams.date,
                    type: "PAYMENT",
                    sessionID: userService.getUserToken()
                };
                auctionsService.updatepaymentdate(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.getData();
                        } else {
                            swal("Error", 'An Unexpected error has occurred. Please contact support@prm360.com for resolving this.', 'error');
                        }

                    })
            }

            $scope.RestartNegotiation = function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                $scope.isnegotiationended = false;
                $scope.NegotiationEnded = false;


                auctionsService.checkForRestartNegotiation(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                            $scope.invokeSignalR('RestartNegotiation', parties);
                        } else {
                            swal("Error", response.errorMessage , 'error');
                        }

                    })

                
            };

            $scope.saveComment = function () {
                var commentText = "";
                if ($scope.newComment && $scope.newComment != "") {
                    commentText = $socpe.newComment;
                } else if ($("#comment")[0].value != '') {
                    commentText = $("#comment")[0].value;
                } else {
                    $scope.commentsvalidation = true;
                }
                if (!$scope.commentsvalidation) {
                    var params = {};
                    auctionsService.getdate()
                        .then(function (response) {
                            var date = new Date(parseInt(response.substr(6)));
                            var myEpoch = date.getTime();
                            params.requirementID = id;
                            params.firstName = "";
                            params.lastName = "";
                            params.replyCommentID = -1;
                            params.commentID = -1;
                            params.errorMessage = "";
                            params.createdTime = "/Date(" + myEpoch + "+0000)/";
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();
                            params.commentText = commentText;
                            $scope.invokeSignalR('SaveComment', params);
                            //requirementHub.invoke('SaveComment', params, function (response) {
                            //	//auctionsService.savecomment(params).then(function (response) {
                            //	$scope.getData();
                            //	$scope.newComment = "";
                            //	$scope.commentsvalidation = false;
                            //});
                        });
                }
            }

            $scope.multipleAttachments = [];
            //$scope.multipleAttachmentsList = [];

            if (!$scope.multipleAttachmentsList) {
                $scope.multipleAttachmentsList = [];
            }

            $scope.getFile = function () {
                $scope.progress = 0;
                //var quotation = $("#quotation")[0].files[0];
                //if (quotation != undefined && quotation != '') {
                //    $scope.file = $("#quotation")[0].files[0];
                //    $log.info($("#quotation")[0].files[0]);
                //}

                //fileReader.readAsDataUrl($scope.file, $scope)
                //    .then(function (result) {
                //        var bytearray = new Uint8Array(result);
                //        if (quotation != undefined && quotation != '') {
                //            $scope.bidAttachement = $.makeArray(bytearray);
                //            $scope.bidAttachementName = $scope.file.name;
                //            $scope.enableMakeBids = true;
                //        }
                //    });

                $scope.multipleAttachments = $("#attachement")[0].files;
                $scope.multipleAttachments = Object.values($scope.multipleAttachments)

                $scope.multipleAttachments.forEach(function (item, index) {
                    fileReader.readAsDataUrl(item, $scope)
                        .then(function (result) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = item.name;
                            if (!$scope.multipleAttachmentsList) {
                                $scope.multipleAttachmentsList = [];
                            }
                            $scope.multipleAttachmentsList.push(fileUpload);
                        });
                })

            };

            $scope.updateTime = function (time,type) {
                $scope.disableDecreaseButtons = true;
                $scope.disableAddButton = true;
                
                var temp = $scope.timerCountdown;
                if (time < 0 && temp + time < 0) {
                    growlService.growl("You cannot reduce the time when it is already below 60 seconds", "inverse");
                    isDone = true;
                    return false;
                }

                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                params.newTicks = ($scope.countdownVal + time);
                params.lotID = $scope.auctionItem.lotId;
                params.type = type;
                var parties = id + "$" + $scope.currentUserId + "$" + params.newTicks + "$" + $scope.currentSessionId + "$" + params.lotID + "$" + params.type;
                $scope.invokeSignalR('UpdateTime', parties, function () {
                    //console.log('time:' + time);
                });
            }

            $scope.Loding = false;

            $scope.priceCapError = false;

            $scope.updateAuctionStart = function () {
                $scope.priceCapError = false;
                $scope.Loding = true;
                $scope.NegotiationSettingsValidationMessage = '';


                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                if ($scope.NegotiationSettingsValidationMessage != '') {
                    $scope.Loding = false;
                    return;
                }

                var params = {};
                params.auctionID = id;

                var startValue = $("#startTimeDate").val(); //$scope.startTime; //Need fix on this.

                if (startValue && startValue != null && startValue != "") {

                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(startValue, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);
                    auctionsService.getdate()
                        .then(function (response1) {
                            
                            var CurrentDateToLocal = userService.toLocalDate(response1);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            //$log.debug(CurrentDate < auctionStartDate);
                            //$log.debug('div' + auctionStartDate);
                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "Your Negotiation Start Time should be greater than current time.", "error");
                                return;
                            }


                            var milliseconds = parseInt(auctionStartDate.getTime() / 1000.0);
                            // // #INTERNATIONALIZATION-0-2019-02-12
                            params.postedOn = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                            params.auctionEnds = "/Date(" + userService.toUTCTicks(startValue) + "+0530)/";
                            params.customerID = userService.getUserId();
                            params.sessionID = userService.getUserToken();

                            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                            params.NegotiationSettings = $scope.NegotiationSettings;

                            //$scope.auctionItem.auctionVendors.forEach(function (item, index) {
                            //    if (item.companyName == "PRICE_CAP") {
                            //        if (item.totalRunningPrice > $scope.auctionItem.auctionVendors[0].totalRunningPrice) {
                            //            $scope.priceCapError = true;
                            //        }
                            //    }
                            //})

                            if ($scope.starreturn) {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to schedule the Negotiation time until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                                return;
                            }

                            if ($scope.auctionItem.auctionVendors.length == 0 || $scope.auctionItem.auctionVendors[0].quotationUrl == "" || $scope.auctionItem.auctionVendors[1].quotationUrl == "" || $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 || $scope.auctionItem.auctionVendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                                return;
                            }
                            if ($scope.auctionItem.auctionVendors[0].companyName == "") {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to create a start time until at least 2 vendors Approved.", "error");
                                return;
                            }
                            if ($scope.priceCapError == true) {
                                $scope.Loding = false;
                                swal("Not Allowed", "Price CAP Value should be less then all the vendors price.", "error");
                                return;
                            }
                            else {
                                $scope.Loding = true;
                                requirementHub.invoke('UpdateAuctionStartSignalR', params, function (req) {
                                    $scope.Loding = false;
                                    swal("Done!", "Your Negotiation Start Time Updated Successfully!", "success");
                                    $scope.Loding = false;
                                    var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                                    auctionsService.getdate()
                                        .then(function (response) {
                                            //var curDate = new Date(parseInt(response.substr(6)));

                                            var CurrentDateToLocal = userService.toLocalDate(response);

                                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                                            var m = moment(ts);
                                            var deliveryDate = new Date(m);
                                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                                            var curDate = (new Date(parseInt(CurrentDateToTicks.substr(6))));

                                            var myEpoch = curDate.getTime();
                                            if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess) && start > myEpoch) {
                                                $scope.startBtns = true;
                                                $scope.customerBtns = false;
                                            } else {

                                                $scope.startBtns = false;
                                                $scope.disableButtons();
                                                $scope.customerBtns = true;
                                            }

                                            if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 0) {
                                                $scope.showTimer = false;
                                            } else {

                                                $scope.showTimer = true;
                                            }
                                        })
                                })
                            }
                        })

                    $scope.Loding = false;


                } else {
                    $scope.Loding = false;
                    alert("Please enter the date and time to update Start Time to.");
                }
                $scope.Loding = false;
            }

            $scope.makeaBidLoding = false;

            $scope.makeaBid = function (call, isReviced) {
                $scope.makeaBidLoding = true;

                var bidPrice = 0;

                if (isReviced == 0) {
                    var bidPrice = $("#quotationamount").val();
                }
                if (isReviced == 1) {

                    var bidPrice = $("#revquotationamount").val();
                }
                $("#clientsideupload").val(null);
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = userService.getUserToken();
                params.userID = parseInt(userService.getUserId());

                params.price = $scope.precisionRound(parseFloat(bidPrice), 2);
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.discountAmount = $scope.discountAmount;

                params.tax = $scope.vendorTaxes;

                params.warranty = $scope.warranty;
                params.duration = $scope.duration;
                params.payment = $scope.payment;
                params.gstNumber = $scope.gstNumber;
                params.validity = $scope.validity;

                params.otherProperties = $scope.otherProperties;

                params.quotationType = 'USER';
                params.type = 'quotation';

                params.freightCharges = $scope.freightCharges;
                params.freightChargesTaxPercentage = $scope.freightChargesTaxPercentage;
                params.freightChargesWithTax = $scope.freightChargesWithTax;

                params.packingCharges = $scope.packingCharges;
                params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                params.packingChargesWithTax = $scope.packingChargesWithTax;

                params.installationCharges = $scope.installationCharges;
                params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                params.installationChargesWithTax = $scope.installationChargesWithTax;

                //params.unitPrice = $scope.auctionItem.unitPrice;
                //params.productQuantity = $scope.auctionItem.productQuantity;
                //params.cGst = $scope.auctionItem.cGst;
                //params.sGst = $scope.auctionItem.sGst;
                //params.iGst = $scope.auctionItem.iGst;



                params.listRequirementTaxes = $scope.listRequirementTaxes;

                params.quotationObject = $scope.auctionItemVendor.listRequirementItems;

                if (isReviced == 0) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.totalprice;
                    params.price = $scope.vendorBidPrice;
                    params.freightCharges = $scope.freightCharges;
                    params.discountAmount = $scope.discountAmount;
                    params.packingCharges = $scope.packingCharges;
                    params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                    params.packingChargesWithTax = $scope.packingChargesWithTax;
                    params.installationCharges = $scope.installationCharges;
                    params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                    params.installationChargesWithTax = $scope.installationChargesWithTax;
                    params.itemFreightCharges = $scope.itemRevFreightCharges;
                }
                if (isReviced == 1) {
                    params.revised = isReviced;
                    params.priceWithoutTax = $scope.revtotalprice;
                    params.price = $scope.revvendorBidPrice;
                    params.freightCharges = $scope.revfreightCharges;
                    params.discountAmount = $scope.discountAmount;
                    params.packingCharges = $scope.revpackingCharges;
                    params.packingChargesTaxPercentage = $scope.packingChargesTaxPercentage;
                    params.packingChargesWithTax = $scope.revpackingChargesWithTax;
                    params.installationCharges = $scope.revinstallationCharges;
                    params.installationChargesTaxPercentage = $scope.installationChargesTaxPercentage;
                    params.installationChargesWithTax = $scope.revinstallationChargesWithTax;
                    params.itemFreightCharges = $scope.itemRevFreightCharges;

                }

                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                        var timeofauctionstart = auctionStart.getTime();
                        var currentServerTime = dateFromServer.getTime();




                        if ($scope.auctionItem.status == "Negotiation Ended") {



                            // $scope.DeleteRequirementTerms();


                            if ($scope.auctionItem.status == 'Negotiation Ended') {
                            } else {
                                // $scope.SaveRequirementTerms($scope.reqId);
                            }

                            auctionsService.makeabid(params).then(function (req) {
                                if (req.errorMessage == '') {

                                    swal({
                                        title: "Thanks!",
                                        text: "Your Quotation has been Uploaded Successfully",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            location.reload();
                                        });

                                    $("#quotationamount").val("");
                                    $("#clientsideupload").val("");
                                    $scope.quotationStatus = true;
                                    $scope.auctionStarted = false;
                                    $scope.getData();
                                    $scope.makeaBidLoding = false;
                                } else {
                                    $scope.makeaBidLoding = false;
                                    swal({
                                        title: "Error",
                                        text: req.errorMessage,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    });
                                }
                            });

                        } else {
                            if (timeofauctionstart - currentServerTime >= 3600000 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                // $scope.DeleteRequirementTerms();


                                if ($scope.auctionItem.status == 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                auctionsService.makeabid(params).then(function (req) {
                                    if (req.errorMessage == '') {

                                        swal({
                                            title: "Thanks!",
                                            text: "Your Quotation has been Uploaded Successfully",
                                            type: "success",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        },
                                            function () {
                                                location.reload();
                                            });

                                        $("#quotationamount").val("");
                                        $("#clientsideupload").val("");
                                        $scope.quotationStatus = true;
                                        $scope.auctionStarted = false;
                                        $scope.getData();
                                        $scope.makeaBidLoding = false;
                                    } else {
                                        $scope.makeaBidLoding = false;
                                        swal({
                                            title: "Error",
                                            text: req.errorMessage,
                                            type: "error",
                                            showCancelButton: false,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Ok",
                                            closeOnConfirm: true
                                        });
                                    }
                                });
                            }

                            else {

                                var message = "You have missed the deadline for Quotation Submission"
                                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") { message = "Your Quotation Already Approved You cant Update Quotation Please Contact PRM360" }

                                $scope.makeaBidLoding = false;
                                swal({
                                    title: "Error",
                                    text: message,
                                    type: "error",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                });
                                $scope.getData();
                            }



                        }












                    })

            }

            $scope.revquotationupload = function () {
                $scope.makeaBidLoding = true;
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeaBidLoding = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                var params = {};
                params.reqID = parseInt(id);
                params.sessionID = userService.getUserToken();
                params.userID = parseInt(userService.getUserId());
                params.price = 0;
                params.quotation = $scope.bidAttachement;
                params.quotationName = $scope.bidAttachementName;
                params.tax = 0;
                auctionsService.revquotationupload(params).then(function (req) {
                    if (req.errorMessage == '') {
                        swal({
                            title: "Thanks!",
                            text: "Your Revised Quotation Uploaded Successfully",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                location.reload();
                            });
                        $("#quotationamount1").val("");
                        $scope.quotationStatus = true;
                        $scope.auctionStarted = false;
                        $scope.getData();
                        $scope.makeaBidLoding = false;
                    } else {
                        $scope.makeaBidLoding = false;
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            }

            $scope.$on('timer-tick', function (event, args) {
                $scope.timerCountdown = event.targetScope.countdown;
                if (event.targetScope.seconds == 5 ||
                   event.targetScope.seconds == 15 ||
                   event.targetScope.seconds == 25 ||
                   event.targetScope.seconds == 35 ||
                   event.targetScope.seconds == 45 ||
                   event.targetScope.seconds == 55) {
                    //auctionsService.CheckSystemDateTime();
                }
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    $scope.countdownVal = event.targetScope.countdown;
                    if (event.targetScope.countdown < 61 && !$scope.disableStopBids && !$scope.disablereduceTime && !$scope.buttonsDisabled) {
                        $timeout($scope.disableButtons(), 1000);
                    }
                    if (event.targetScope.countdown > 60 && $scope.buttonsDisabled) {
                        $timeout($scope.enableButtons(), 1000);
                    }
                    
                    if (event.targetScope.countdown <= 0) {
                        if ($scope.auctionStarted && ($scope.auctionItem.status == "CLOSED" || $scope.auctionItem.status == "STARTED")) {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0].runningPrice;
                        } else if ($scope.auctionItem.status == "NOTSTARTED") {
                            var params = {};
                            params.reqID = id;
                            params.sessionID = userService.getUserToken();
                            params.userID = userService.getUserId();

                            auctionsService.StartNegotiation(params)
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        $scope.getData();
                                    }
                                })

                            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                            $scope.invokeSignalR('CheckRequirement', parties);
                        }
                    }
                    if (event.targetScope.countdown <= 120) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }
                    
                    if ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') {

                        if ($scope.auctionItem.isUnitPriceBidding == 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder = {
                                'background-color': '#f5b2b2'
                            };
                        }

                        if ($scope.auctionItem.isUnitPriceBidding == 0 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isTabular == true) {
                            $scope.divBorder1 = {
                                'background-color': '#f5b2b2'
                            };
                        }
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#000' };
                    }
                    if (event.targetScope.countdown > 120 && $scope.auctionItem.status != 'NOTSTARTED') {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }
                    if (event.targetScope.countdown <= 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = true;
                    }
                    if (event.targetScope.countdown > 60 && $scope.auctionItem.status == 'STARTED') {
                        $scope.disableDecreaseButtons = false;
                    }
                    if (event.targetScope.countdown <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                        $scope.showTimer = false;
                        window.location.reload();
                    }
                    if (event.targetScope.countdown < 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended')) {
                        $scope.showTimer = false;
                        $scope.auctionItem.status = 'Negotiation Ended';

                        swal({
                            title: "Thanks!",
                            text: "Negotiation complete! Thank you for being a part of this.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //location.reload();
                                //$log.info("Negotiation Ended.");
                            });
                    }
                    if (event.targetScope.countdown <= 3) {
                        $scope.disableAddButton = true;
                    }
                    if (event.targetScope.countdown < 6) {
                        $scope.disableBidButton = true;
                    }
                    if (event.targetScope.countdown > 3) {
                        //$scope.getData();
                        $scope.disableAddButton = false;
                    }
                    if (event.targetScope.countdown >= 6) {
                        $scope.disableBidButton = false;
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED == true || $scope.auctionItem.IS_CB_ENABLED == 1) {
                    if (event.targetScope.minutes < 2) {
                        $scope.timerStyle = {
                            'color': '#f00',
                            '-webkit - animation': 'flash linear 1s infinite',
                            'animation': 'flash linear 1s infinite'
                        };
                    }

                    if (event.targetScope.minutes >= 2 || event.targetScope.days > 0 || event.targetScope.hours > 0) {
                        $scope.timerStyle = { 'color': '#228B22' };
                    }

                    if (event.targetScope.days == 0 &&
                        event.targetScope.hours == 0 &&
                        event.targetScope.minutes == 0 &&
                        (event.targetScope.seconds == 1)) {
                        location.reload();
                    }
                }
            });

            requirementHub.on('timerUpdateSignalR', function (payloadObj) {
                //console.log("timerUpdateLotSignalR" + payloadObj);
                if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0) {
                    $scope.auctionItem.timeLeft = +payloadObj.payLoad[0];
                    if ($scope.auctionItem.timeLeft) {
                        $scope.disableAddButton = false;
                        $scope.disableDecreaseButtons = false;

                        if ($scope.auctionItem.status == 'STARTED') {
                            if ($scope.auctionItem.timeLeft <= 60) {
                                $scope.disableDecreaseButtons = true;
                            }
                            if ($scope.auctionItem.timeLeft > 60) {
                                $scope.disableDecreaseButtons = false;
                            }
                        }

                        if ($scope.auctionItem.timeLeft <= 0 && $scope.auctionItem.status == 'NOTSTARTED') {
                            $scope.showTimer = false;
                            window.location.reload();
                        }
                        if ($scope.auctionItem.timeLeft < 1 && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended')) {
                            $scope.showTimer = false;
                            $scope.auctionItem.status = 'Negotiation Ended';
                        }
                    }

                    $scope.$broadcast('timer-set-countdown-seconds', +payloadObj.payLoad[0]);

                    //if ($scope.auctionItem.timeLeft > 0) {
                    //    setTimeout(function () {
                    //        try {
                    //            $scope.$apply();
                    //            if ($scope.isCustomer && $scope.auctionItem && $scope.auctionItem.status && $scope.auctionItem.status == "STARTED") {
                    //                var parties = $scope.reqId + "$" + userService.getUserId() + "$" + "0" + "$" + userService.getUserToken();
                    //                $scope.invokeSignalR('UpdateTimeRefresh', parties, function () {
                    //                    //console.log(parties); 
                    //                });
                    //            }
                    //        }
                    //        catch (err) { }
                    //    }, 10000);
                    //}
                }

                //if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0 && $scope.auctionItem && $scope.auctionItem.status !== 'STARTED') {
                //    window.location.reload();
                //}
            });

            //let isConnected = true;
            //$scope.isdisconnectedInvoked = false;
            //function diconnectedUpdateRecieved()
            //{
            //    console.log("signalr connection disconnected...");
            //    if ($scope.auctionItem.status == 'STARTED')
            //    {
            //        if ($scope.isCustomer)
            //        {
            //            let signalRGroupName = 'requirementGroup' + $stateParams.Id + '_CUSTOMER';
            //            if ($scope.isCustomer) {
            //                signalRGroupName = 'requirementGroup' + $stateParams.Id + '_CUSTOMER';
            //            }

            //            $scope.invokeSignalR('joinGroup', 'requirementGroup' + $stateParams.Id);
            //            $scope.invokeSignalR('joinGroup', signalRGroupName);
            //            var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
            //            $scope.invokeSignalR('CheckRequirement', parties);
            //        }
            //    }
            //}


            //requirementHub.on('someoneDisconnected', function ()
            //{
            //    swal("done!", "someone disconnected refreshing the live data.", "success");

            //    if ($scope.isCustomer) {
            //        let signalRGroupName = 'requirementGroup' + $stateParams.Id + '_' + $scope.currentUserId;
            //        if ($scope.isCustomer) {
            //            signalRGroupName = 'requirementGroup' + $stateParams.Id + '_CUSTOMER';
            //        }

            //        //$scope.invokeSignalR('joinGroup', 'requirementGroup' + $stateParams.Id);
            //        //$scope.invokeSignalR('joinGroup', signalRGroupName);
            //        var parties = $stateParams.Id;
            //        $scope.invokeSignalR('GetItemPricesSignalRLite', parties);
            //    }


            //    //location.reload();
            //    //var parties = $scope.reqId;
            //    //$scope.invokeSignalR('GetItemPricesSignalRLite', parties);
            //});


            //$rootScope.$on('reconnectUpdate', diconnectedUpdateRecieved);


            //requirementHub.on('makeBidLite', function (payloadObj) {
            //    if (payloadObj && payloadObj.payLoad && payloadObj.payLoad.length > 0)
            //    {
            //        $scope.auctionItem.auctionVendors.forEach(function (vendor, vendorIndex) {
            //            vendor.vendorColorStyle = '';
            //            var foundVendorOverall = '';
            //            if (payloadObj.payLoad[0].UserID === vendor.vendorID)
            //            {
            //                foundVendorOverall = payloadObj;
            //            }
            //            var vendors = _.filter(payloadObj.payLoad[0].QuotationObject, function (bidVendor) { return bidVendor.VendorID === vendor.vendorID });
            //            vendors.forEach(function (bidItem, bidItemIndex)
            //            {
            //                if (vendor.vendorID === bidItem.VendorID) {
            //                    vendor.rank = bidItem.VendorRank;
            //                    $scope.vendorRank = bidItem.VendorRank;
            //                    if (vendor.listRequirementItems && vendor.listRequirementItems.length > 0) {
            //                        vendor.listRequirementItems.forEach(function (vendorReqItem, vendorReqItemIndex) {
            //                            if (vendorReqItem.itemID === bidItem.ItemID && vendor.vendorID === bidItem.VendorID) {
            //                                vendorReqItem.itemRank = bidItem.ItemRank;
            //                                if ($scope.isCustomer)
            //                                {
            //                                    vendorReqItem.revUnitPrice = bidItem.RevUnitPrice;
            //                                    vendorReqItem.revitemPrice = bidItem.RevItemPrice;
            //                                }
            //                            }
            //                        });
            //                        //$scope.listRequirementItemsTemp = vendor.listRequirementItems;
            //                    }
            //                }
            //            });

            //            if ($scope.isCustomer) {
            //                if (foundVendorOverall) {
            //                    vendor.bestPrice = foundVendorOverall.payLoad[0].BasePrice;
            //                    vendor.totalPriceIncl = foundVendorOverall.payLoad[0].TotalPrice;
            //                    growlService.growl("A vendor has made a bid.", "success");
            //                    vendor.vendorColorStyle = { 'background-color': 'lightgreen' };
            //                }
            //            }
            //        });

            //        if ($scope.isCustomer)
            //        {
            //            $scope.auctionItem.auctionVendors = _.orderBy($scope.auctionItem.auctionVendors, o => +o.rank, ['rank'], ['asc']);
            //        }

            //        if (!$scope.isCustomer)
            //        {
            //            $scope.auctionItemVendor = $scope.auctionItem;
            //            $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
            //                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0) {
            //                    $scope.auctionItem.auctionVendors[0].listRequirementItems.forEach(function (i, indx) {
            //                        if (item.itemID && i.itemID && item.itemID == i.itemID) {
            //                            //console.log('ItemId:' + item.itemID + '--Rank:' + i.itemRank);
            //                            item.itemRank = i.itemRank;
            //                        }
            //                    });
            //                }
            //            });

            //            $scope.vendorRank = $scope.auctionItemVendor.auctionVendors[0].rank;
            //            $scope.auctionItem.minPrice = ($scope.auctionItem.minPrice - $scope.reduceBidValue);

            //        }


            //        //console.log("end time is>>>"+$scope.auctionItem.endTime);
            //    }
            //});



            requirementHub.on('checkRequirement', function (items) {
                var req = items.inv;
                var iteminvcallerID = 0;
                if ($scope.auctionItem.IS_CB_ENABLED == false || $scope.auctionItem.IS_CB_ENABLED == 0) {
                    if (items.inv) {
                        iteminvcallerID = items.inv.callerID;
                    }
                    if ($scope.isCustomer) {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {

                            if (String(userItem.superUserID) === String($scope.auctionItem.superUserID)) {
                                $scope.signalRCustomerAccess = true;
                            }

                            return String(userItem.superUserID) === String($scope.auctionItem.superUserID)
                        });
                    }
                    else {
                        var itemTemp = _.filter(items.payLoad, function (userItem) {
                            return String(userItem.entityID) === userService.getUserId()
                        });
                    }
                    let item = itemTemp[0];
                    $scope.setAuctionInitializer(item, '', iteminvcallerID);
                    $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                    $('.datetimepicker').datetimepicker({
                        useCurrent: false,
                        icons: {
                            time: 'glyphicon glyphicon-time',
                            date: 'glyphicon glyphicon-calendar',
                            up: 'glyphicon glyphicon-chevron-up',
                            down: 'glyphicon glyphicon-chevron-down',
                            previous: 'glyphicon glyphicon-chevron-left',
                            next: 'glyphicon glyphicon-chevron-right',
                            today: 'glyphicon glyphicon-screenshot',
                            clear: 'glyphicon glyphicon-trash',
                            close: 'glyphicon glyphicon-remove'

                        },

                        minDate: item.currentTime
                    });
                    if (!$scope.NegotiationEnded) {
                        $scope.$broadcast('timer-start');
                    }
                    if (id == req.requirementID && ($scope.signalRCustomerAccess || userService.getUserId() == req.customerID || userService.getUserId() == req.superUserID || req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 || req.custCompID == userService.getUserCompanyId())) {
                        if (req.methodName == "UpdateTime" && req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                            growlService.growl("Negotiation time has been updated.", 'inverse');
                        } else if (req.methodName == "MakeBid" && ($scope.signalRCustomerAccess || userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess || req.custCompID == userService.getUserCompanyId())) {
                            growlService.growl("A vendor has made a bid.", "success");
                        } else if (req.methodName == "RestartNegotiation") {
                           // growlService.growl("CANNOT RESTART THE NEGOTIATION AS QCS SENT FOR APPROVAL.", 'inverse');
                            window.location.reload();
                        } else if (req.methodName == "RevUploadQuotation") {

                        } else if (req.methodName == "StopBids") {
                            if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1) {
                                growlService.growl("Negotiation Time reduced to 1 minute by the customer. New bids will not extend time.");
                            }
                        } else if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR") {
                            if ($scope.auctionItem.status == 'STARTED') {
                                growlService.growl("A vendor has made a bid.", "success");
                            } else {
                                //growlService.growl(req.status, "success");
                            }
                        } else if (!$scope.NegotiationEnded) {
                            auctionsService.isnegotiationended(id, userService.getUserToken())
                                .then(function (response) {
                                    if (response.errorMessage == '') {
                                        if (response.objectID == 1) {
                                            $scope.NegotiationEnded = true;
                                            if (($scope.signalRCustomerAccess || userService.getUserId() == req.customerID || req.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                                                swal("Negotiation Completed!", "Congratulations! your procurement process is now completed. " + $scope.toprankerName + " is the least bidder with the value " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + " \n Your savings through PRM 360 :" + ($scope.auctionItem.savings) + " " + $scope.auctionItem.currency, "success");
                                            } else if (req.userIDList.indexOf(parseInt(userService.getUserId())) > -1 && $scope.timeLeftMessage == "Negotiation Ends in: ") {
                                                if ($scope.vendorRank == 1) {
                                                    swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Customer would be reaching out you shortly. All the best!", "success");
                                                   // swal("Negotiation Completed!", "Congratulations! you are the least bidder for this requirement. Your price is : " + $scope.auctionItem.minPrice + " " + $scope.auctionItem.currency + "\n Customer would be reaching out you shortly. All the best!", "success");
                                                } else {
                                                    swal("Negotiation Completed!", "Bidding Completed.\n Thank You for your interest on this requirement. You ranked " + $scope.vendorRank + " in this requirement. Thank you for your participation.", "success");
                                                }
                                            }
                                        }
                                    }
                                });
                        }
                    } else {
                        if (req.methodName == "SAVE_DIFFERENTIAL_FACTOR" && iteminvcallerID != userService.getUserId()) {
                            growlService.growl("A vendor has made a bid.", "success");
                        }
                    }
                }
                else if ($scope.auctionItem.IS_CB_ENABLED) {

                    if ($scope.userIsOwner) {


                        if (items.inv.methodName == 'CheckRequirement') {
                            var vendorCompanyName = '';
                            $scope.auctionItem.requirementVendorsList.forEach(function (vendor, vendorIndex) {
                                if (items.inv.callerID == vendor.vendorID) {
                                    vendorCompanyName = vendor.companyName;
                                    growlService.growl(vendorCompanyName + ' has made a bid.', "success");
                                }
                            });
                        }

                        $scope.getData();
                    } else if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0) {
                        $scope.getData();
                    }
                }
            });

            $scope.stopBids = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be stopped after one minute.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, Stop Bids!",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID;
                    $scope.invokeSignalR('StopBids', parties, function () {
                        $scope.$broadcast('timer-set-countdown-seconds', 60);
                        $scope.disableButtons();
                        swal("Done!", "Negotiation time reduced to one minute.", "success");
                    });

                    //requirementHub.invoke('StopBids', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Negotiation time reduced to one minute.", "success");
                    //});
                });
            };

            $scope.disableButtons = function () {
                $scope.buttonsDisabled = true;
            }

            $scope.enableButtons = function () {
                $scope.buttonsDisabled = false;
            }

            $scope.editRequirement = function () {

                auctionsService.CheckIfReqInLot($stateParams.Id, $scope.currentSessionId)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            //$log.info('in edit' + $stateParams.Id);
                            //$state.go('form.addnewrequirement', { 'Id': $stateParams.Id });
                            $state.go('save-requirementAdv', { 'Id': $stateParams.Id });
                        } else {
                            swal("Error!", response.errorMessage, "error");
                            return;
                        }
                    })

                
            }

            $scope.goToReqReport = function (reqID) {
                //$state.go("reports", { "reqID": reqID });

                var url = $state.href('reports', { "reqID": reqID });
                $window.open(url, '_blank');

            };

            $scope.generatePOforUser = function () {
                $state.go('po', { 'reqID': $stateParams.Id });
            }

            $scope.metrialDispatchmentForm = function () {
                $state.go('material-dispatchment', { 'Id': $stateParams.Id });
            }

            $scope.paymentdetailsForm = function () {
                $state.go('payment-details', { 'Id': $stateParams.Id });
            }

            $scope.deleteRequirement = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be deleted and an email will be sent out to all vendors involved.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    // auctionsService.updatebidtime(params);
                    // swal("Done!", "Auction time reduced to oneminute.", "success");
                    var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.reason;
                    $scope.invokeSignalR('DeleteRequirement', parties);
                    //requirementHub.invoke('DeleteRequirement', parties, function (req) {
                    //    $scope.$broadcast('timer-set-countdown-seconds', 60);
                    //    $scope.disableButtons();
                    //    swal("Done!", "Requirement has been cancelled", "success");
                    //});
                });
            }

            $scope.checkCancelRequirement = function () {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                //var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();

                auctionsService.checkForCancelRequirement(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {
                           // $scope.invokeSignalR('RestartNegotiation', parties);
                            $scope.cancelRequirement();
                        } else {
                            swal("Error", response.errorMessage, 'error');
                            angular.element('#rfqCancelPopup').modal('hide');
                        }

                    })
            }

            $scope.cancelRequirement = function () {
                swal({
                    title: "Are you sure?",
                    text: "The Negotiation will be cancelled.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, I am sure",
                    closeOnConfirm: true
                }, function () {
                    var params = {};
                    params.reqID = id;
                    params.sessionID = userService.getUserToken();
                    params.userID = userService.getUserId();
                    // auctionsService.updatebidtime(params);
                    // swal("Done!", "Auction time reduced to oneminute.", "success");
                        var parties = params.reqID + "$" + params.userID + "$" + params.sessionID + "$" + $scope.cancelRfqObj.reason + "$" + $scope.cancelRfqObj.checkBoxEmail + "$" + $scope.cancelRfqObj.checkBoxSms;
                        $scope.invokeSignalR('CancelRequirement', parties);
                        angular.element('#rfqCancelPopup').modal('hide');
                        location.reload();
                });
            }

            $scope.DeleteVendorFromAuction = function (VendoID, quotationUrl) {
                if (($scope.auctionItem.auctionVendors.length > 2 || quotationUrl == "") && (quotationUrl == "" || (quotationUrl != "" && $scope.auctionItem.auctionVendors[2].quotationUrl != ""))) {
                    swal({
                        title: "Are you sure?",
                        text: "The Vendor will be deleted and an email will be sent out to The vendor.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Yes, I am sure",
                        closeOnConfirm: true
                    }, function () {
                        var params = {};
                        params.reqID = id;
                        params.sessionID = userService.getUserToken();
                        params.userID = VendoID;

                        auctionsService.DeleteVendorFromAuction(params)
                            .then(function (response) {
                                if (response.errorMessage == '') {

                                    $scope.getData();
                                    swal("Done!", "Done! Vendor Deleted Successfully!", "success");
                                } else {
                                    swal("Error", "You cannot Delete Vendor", "inverse");
                                }
                            })
                    });
                }
                else {
                    swal("Not Allowed", "You are not allowed to Delete the Vendors.", "error");
                }
            }



            $scope.makeBidUnitPriceValidation = function (revUnitPrice, TemperoryRevUnitPrice, productIDorName) {
                if (TemperoryRevUnitPrice < revUnitPrice && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter price less than ' + TemperoryRevUnitPrice + ' of ' + productIDorName);
                }
                if ($scope.TemperoryRevfreightCharges < $scope.revfreightCharges && ($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    $scope.getData();
                    swal("Error!", 'Please enter freight price less than ' + $scope.TemperoryRevfreightCharges);
                }
            };

            $scope.makeBidOtherChargesValidation = function () {
                if (($scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'Negotiation Ended') && $scope.auctionItem.isDiscountQuotation == 0) {
                    if ($scope.TemperoryRevpackingCharges < $scope.revpackingCharges) {
                        $("#revpackingCharges").val($scope.TemperoryRevpackingCharges);
                        $scope.getData();
                        swal("Error!", 'Please enter packing charges less than ' + $scope.TemperoryRevpackingCharges);
                    }
                    else if ($scope.TemperoryRevinstallationCharges < $scope.revinstallationCharges) {
                        $("#revinstallationCharges").val($scope.TemperoryRevinstallationCharges);
                        $scope.getData();
                        swal("Error!", 'Please enter installation charges less than ' + $scope.TemperoryRevinstallationCharges);
                    }
                    else if ($scope.TemperoryRevfreightCharges < $scope.revfreightCharges) {
                        $("#revfreightCharges").val($scope.TemperoryRevfreightCharges);
                        $scope.getData();
                        swal("Error!", 'Please enter freight charges less than ' + $scope.TemperoryRevfreightCharges);
                    }
                }
            };

            $scope.unitPriceCalculation = function (val) {

                $scope.gstValidation = false;

                var itemfreight = 0;
                var itemRevfreight = 0;
                var myInit = 1;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    
                    if ((val == 'CGST' || val == 'SGST') && (item.sGst != 0 || item.cGst != 0) && myInit == 1) {
                        $scope.IGSTFields = true;
                        $scope.CSGSTFields = false;
                        myInit++;
                    } else if (val == 'IGST' && item.iGst != 0 && myInit == 1) {
                        $scope.CSGSTFields = true;
                        $scope.IGSTFields = false;
                        myInit++;
                    } else if (myInit == 1 && val != "PRC") {
                        $scope.CSGSTFields = false;
                        $scope.IGSTFields = false;
                    }

                    // item.cGst = item.Gst / 2;
                    // item.sGst = item.Gst / 2;

                    item.itemfreight = ((parseFloat(item.itemFreightCharges)) + ((parseFloat(item.itemFreightCharges / 100)) * (item.itemFreightTAX)));
                    itemRevfreight = ((parseFloat(item.itemRevFreightCharges)) + ((parseFloat(item.itemRevFreightCharges / 100)) * (item.itemFreightTAX)));
                    //  item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));
                    if (val == 'GST') {

                        if ($scope.auctionItem.isDiscountQuotation == 2) {

                            item = $scope.handlePrecision(item, 2);
                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item.netPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + item.Gst / 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;

                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = (item.marginAmount / item.costPrice);
                            item.unitDiscount = (item.marginAmount / item.netPrice);
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }
                    }
                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitPrice = item.unitPrice;
                            item.itemRevFreightCharges = item.itemFreightCharges;
                        }
                        item.TotalGstTemp = 0;
                        item.TotalrevGstTemp = 0;
                        //item.sGst = item.cGst;
                        var tempUnitPrice = item.unitPrice;
                        var tempCGst = item.cGst;
                        var tempSGst = item.sGst;
                        var tempIGst = item.iGst;

                        
                        if (item.unitPrice == undefined || item.unitPrice <= 0) {
                            item.unitPrice = 0;
                        };

                        item.itemPrice = item.unitPrice * item.productQuantity;
                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                            tempCGst = 1;
                        } else if (val == 'cGST') {
                            // item.sGst = item.cGst;
                            item.iGst = 0;
                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                           tempSGst = 1;
                        } else if (val == 'sGST') {
                            // item.cGst = item.sGst;
                            item.iGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }
                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                            tempIGst = 1;
                        } else if (val == 'iGST') {
                            item.cGst = item.sGst = 0;

                            tempCGst = item.cGst;
                            tempSGst = item.sGst;
                            tempIGst = item.iGst;
                        }

                        //if (item.cGst < 10) {
                        //    if (item.cGst.toString().length > 4) {
                        //        tempCGst = 0;
                        //        item.cGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.cGst >= 10 || item.cGst <= 100) {
                        //    if (item.cGst.toString().length > 5) {
                        //        tempCGst = 0;
                        //        item.cGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.sGst < 10) {
                        //    if (item.sGst.toString().length > 4) {
                        //        tempSGst = 0;
                        //        item.sGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.sGst >= 10 || item.sGst <= 100) {
                        //    if (item.sGst.toString().length > 5) {
                        //        tempSGst = 0;
                        //        item.sGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.iGst < 10) {
                        //    if (item.iGst.toString().length > 4) {
                        //        tempIGst = 0;
                        //        item.iGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        //if (item.iGst >= 10 || item.iGst <= 100) {
                        //    if (item.iGst.toString().length > 5) {
                        //        tempIGst = 0;
                        //        item.iGst = 0;
                        //        swal("Error!", 'Please enter valid GST');
                        //    }
                        //};

                        if ((parseFloat(item.cGst) + parseFloat(item.sGst)) > 100) {
                            item.cGst = item.sGst = tempCGst = tempSGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        } else if (item.iGst > 100) {
                            item.iGst = tempIGst = 0;
                            swal("Error!", 'Please enter valid TAX %');
                        }

                        if (item.itemFreightTAX > 100) {
                            item.itemFreightTAX = 0;
                            swal("Error!", 'Please enter valid Freight TAX %');
                        }

                        if (tempCGst == 1 && tempSGst == 1 && tempIGst == 0) {
                            item.TotalGstTemp = 1;
                        } else if (tempIGst == 1 && tempCGst == 0 && tempSGst == 0)
                        {
                            item.TotalGstTemp = 1;
                        }
                        var subitemPrice = 0;
                        if (item.productQuotationTemplateArray.length > 0) {
                            
                            item.productQuotationTemplateArray.forEach(function (subItem,subItemIdx) {

                                if (subItem.HAS_PRICE == 1) {
                                    subitemPrice += subItem.BULK_PRICE;
                                    subitemPrice = parseFloat(subitemPrice);
                                }

                            })
                        }

                        if (item.TotalGstTemp == 1) {
                            item.itemPrice = (item.itemPrice + (parseFloat(item.itemfreight)) + subitemPrice);
                        } else {
                            item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + (parseFloat(item.itemfreight)) + subitemPrice);
                        }

                       // item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + (parseFloat(item.itemfreight)));
                         
                       // item.itemPrice = (item.itemPrice + ((item.itemPrice / 100) * (parseFloat(tempCGst) + parseFloat(tempSGst) + parseFloat(tempIGst))) + (parseFloat(item.itemfreight)));

                        //item.unitPrice = tempUnitPrice;
                        //item.cGst = tempCGst;
                        //item.sGst = tempSGst;
                        //item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            // $scope.gstValidation = true;
                            // not mandatory
                        }
                    }


                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                       
                        var tempRevUnitPrice = item.revUnitPrice;
                        var tempitemRevFreightCharges = itemRevfreight;
                        tempCGst = item.cGst;
                        tempSGst = item.sGst;
                        tempIGst = item.iGst;

                        if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                            item.revUnitPrice = 0;
                        }
                        
                        if (item.itemRevFreightCharges == undefined || item.itemRevFreightCharges <= 0) {
                            item.itemRevFreightCharges = 0;
                        }

                        item.revitemPrice = item.revUnitPrice * item.productQuantity;
                        if (item.cGst == undefined || item.cGst <= 0) {
                            item.cGst = 0;
                            tempCGst = 1;
                        }

                        if (item.sGst == undefined || item.sGst <= 0) {
                            item.sGst = 0;
                            tempSGst = 1;
                        }

                        if (item.iGst == undefined || item.iGst <= 0) {
                            item.iGst = 0;
                            tempIGst = 1;
                        }

                        if (tempCGst == 1 && tempSGst == 1 && tempIGst == 0) {
                            item.TotalrevGstTemp = 1;
                        } else if (tempIGst == 1 && tempCGst == 0 && tempSGst == 0) {
                            item.TotalrevGstTemp = 1;
                        }

                        if (item.TotalrevGstTemp == 1) {
                            item.revitemPrice = (item.revitemPrice + (parseFloat(itemRevfreight)));
                        } else {
                            item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + (parseFloat(itemRevfreight)));
                        }
                        //RevfreightCharges = itemRevfreight + ((itemRevfreight / 100) * (item.cGst + item.sGst + item.iGst))

                       // item.revitemPrice = (item.revitemPrice + ((item.revitemPrice / 100) * (parseFloat(item.cGst) + parseFloat(item.sGst) + parseFloat(item.iGst))) + (parseFloat(itemRevfreight)));
                        itemRevfreight = tempitemRevFreightCharges;
                        item.revUnitPrice = tempRevUnitPrice;
                        //item.cGst = tempCGst;
                        //item.sGst = tempSGst;
                        //item.iGst = tempIGst;

                        if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                            // $scope.gstValidation = true;
                            // not mandatory
                        }
                    }
                });


                // For disablig non core item prices when total price is not gretaer than 0
                if ($scope.totalprice > 0)
                {
                    $scope.fieldValidation();
                }
                // For disablig non core item prices when total price is not gretaer than 0

            };

            $scope.regretError = false;


            $scope.freightCharges = 0;
            $scope.freightChargesTaxPercentage = 0;
            $scope.freightChargesWithTax = 0;

            $scope.revfreightCharges = 0;
            $scope.revfreightChargesWithTax = 0;

            $scope.freightChargeslocal = 0;
            $scope.freightChargesTaxPercentagelocal = 0;
            $scope.freightChargesWithTaxlocal = 0;

            $scope.revfreightChargeslocal = 0;
            $scope.revfreightChargesWithTaxlocal = 0;




            $scope.packingCharges = 0;
            $scope.packingChargesTaxPercentage = 0;
            $scope.packingChargesWithTax = 0;

            $scope.revpackingCharges = 0;
            $scope.revpackingChargesWithTax = 0;

            $scope.packingChargeslocal = 0;
            $scope.packingChargesTaxPercentagelocal = 0;
            $scope.packingChargesWithTaxlocal = 0;

            $scope.revpackingChargeslocal = 0;
            $scope.revpackingChargesWithTaxlocal = 0;





            $scope.installationCharges = 0;
            $scope.installationChargesTaxPercentage = 0;
            $scope.installationChargesWithTax = 0;

            $scope.revinstallationCharges = 0;
            $scope.revinstallationChargesWithTax = 0;

            $scope.installationChargeslocal = 0;
            $scope.installationChargesTaxPercentagelocal = 0;
            $scope.installationChargesWithTaxlocal = 0;

            $scope.revinstallationChargeslocal = 0;
            $scope.revinstallationChargesWithTaxlocal = 0;




            $scope.getTotalPrice = function (discountAmount, freightCharges, totalprice,
                packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                installationCharges, installationChargesTaxPercentage, installationChargesWithTax,
                freightChargesTaxPercentage, freightChargesWithTax) {
                if (!$scope.auctionItem) {
                    return;
                }


                $scope.regretError = false;
                $scope.priceValidationsVendor = '';
                $scope.taxValidation = false;
                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {

                    $scope.revfreightCharges = $scope.precisionRound(parseFloat(freightCharges), 2);
                    $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                    $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(freightChargesWithTax), 2);

                    $scope.revpackingCharges = $scope.precisionRound(parseFloat(packingCharges), 2);
                    $scope.packingChargesTaxPercentage = $scope.precisionRound(parseFloat(packingChargesTaxPercentage), 2);
                    $scope.revpackingChargesWithTax = $scope.precisionRound(parseFloat(packingChargesWithTax), 2);

                    $scope.revinstallationCharges = $scope.precisionRound(parseFloat(installationCharges), 2);
                    $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);
                    $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat(installationChargesWithTax), 2);
                }

                $scope.freightCharges = $scope.precisionRound(parseFloat(freightCharges), 2);
                $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                $scope.freightChargesWithTax = $scope.precisionRound(parseFloat(freightChargesWithTax), 2);

                $scope.packingCharges = $scope.precisionRound(parseFloat(packingCharges), 2);
                $scope.packingChargesTaxPercentage = $scope.precisionRound(parseFloat(packingChargesTaxPercentage), 2);
                $scope.packingChargesWithTax = $scope.precisionRound(parseFloat(packingChargesWithTax), 2);

                $scope.installationCharges = $scope.precisionRound(parseFloat(installationCharges), 2);
                $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);
                $scope.installationChargesWithTax = $scope.precisionRound(parseFloat(installationChargesWithTax), 2);

                $scope.discountAmount = $scope.precisionRound(parseFloat(discountAmount), 2);

                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = totalprice;
                    if (isNaN($scope.totalprice) || $scope.totalprice == undefined || $scope.totalprice < 0 || $scope.totalprice == '') {
                        //$("#totalprice").val(0);
                        $scope.vendorBidPrice = 0;
                        $scope.vendorBidPriceWithoutDiscount = 0;
                    }
                }
                else {
                    $scope.auctionItem.isUOMDifferent = false;
                    if ($scope.auctionItemVendor && $scope.auctionItemVendor.listRequirementItems.length > 0) {

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.unitPrice > 0) {
                                item.isRegret = false;
                                item.regretComments = '';
                            }

                            if (item.isRegret && (item.regretComments == '' || item.regretComments == undefined || item.regretComments == null)) {
                                $scope.regretError = true;
                            }

                            if (item.productQuantityIn.toUpperCase() != item.vendorUnits.toUpperCase()) {
                                //$scope.auctionItem.isUOMDifferent = true;
                            }

                            if (isNaN(item.itemPrice) || item.itemPrice == undefined || item.itemPrice < 0) {
                                $scope.ItemPriceValidation = true;
                            }

                            $scope.regretHandle(item);

                        });

                    }
                }

              

                $scope.freightChargeslocal = $scope.precisionRound(parseFloat($scope.freightCharges), 2);
                if (isNaN($scope.freightCharges) || $scope.freightCharges == undefined || $scope.freightCharges < 0) {
                    $scope.freightChargeslocal = 0;
                    $scope.discountfreightValidation = true;
                }
                $scope.freightChargeslocal = $scope.precisionRound(parseFloat($scope.freightChargeslocal), 2);

                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentage), 2);
                if (isNaN($scope.freightChargesTaxPercentage) || $scope.freightChargesTaxPercentage == undefined || $scope.freightChargesTaxPercentage < 0) {
                    $scope.freightChargesTaxPercentagelocal = 0;
                }
                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentagelocal), 2);

                $scope.freightChargesWithTax = $scope.precisionRound(parseFloat($scope.freightChargeslocal +
                    (($scope.freightChargeslocal / 100) * ($scope.freightChargesTaxPercentagelocal))), 2);
                $scope.freightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.freightChargesWithTax), 2);
                if (isNaN($scope.freightChargesWithTax) || $scope.freightChargesWithTax == undefined || $scope.freightChargesWithTax < 0) {
                    $scope.freightChargesWithTaxlocal = 0;
                }
                $scope.freightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.freightChargesWithTaxlocal), 2);




                $scope.packingChargeslocal = $scope.precisionRound(parseFloat($scope.packingCharges), 2);
                if (isNaN($scope.packingCharges) || $scope.packingCharges == undefined || $scope.packingCharges < 0) {
                    $scope.packingChargeslocal = 0;
                }
                $scope.packingChargeslocal = $scope.precisionRound(parseFloat($scope.packingChargeslocal), 2);

                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentage), 2);
                if (isNaN($scope.packingChargesTaxPercentage) || $scope.packingChargesTaxPercentage == undefined || $scope.packingChargesTaxPercentage < 0) {
                    $scope.packingChargesTaxPercentagelocal = 0;
                }
                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentagelocal), 2);

                $scope.packingChargesWithTax = $scope.precisionRound(parseFloat($scope.packingChargeslocal +
                    (($scope.packingChargeslocal / 100) * ($scope.packingChargesTaxPercentagelocal))), 2);
                $scope.packingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.packingChargesWithTax), 2);
                if (isNaN($scope.packingChargesWithTax) || $scope.packingChargesWithTax == undefined || $scope.packingChargesWithTax < 0) {
                    $scope.packingChargesWithTaxlocal = 0;
                }
                $scope.packingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.packingChargesWithTaxlocal), 2);

                $scope.installationChargeslocal = $scope.precisionRound(parseFloat($scope.installationCharges), 2);
                if (isNaN($scope.installationCharges) || $scope.installationCharges == undefined || $scope.installationCharges < 0) {
                    $scope.installationChargeslocal = 0;
                }
                $scope.installationChargeslocal = $scope.precisionRound(parseFloat($scope.installationChargeslocal), 2);

                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentage), 2);
                if (isNaN($scope.installationChargesTaxPercentage) || $scope.installationChargesTaxPercentage == undefined ||
                    $scope.installationChargesTaxPercentage < 0) {
                    $scope.installationChargesTaxPercentagelocal = 0;
                }
                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentagelocal), 2);

                $scope.installationChargesWithTax = $scope.precisionRound(parseFloat($scope.installationChargeslocal +
                    (($scope.installationChargeslocal / 100) * ($scope.installationChargesTaxPercentagelocal))), 2);
                $scope.installationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.installationChargesWithTax), 2);
                if (isNaN($scope.installationChargesWithTax) || $scope.installationChargesWithTax == undefined || $scope.installationChargesWithTax < 0) {
                    $scope.installationChargesWithTaxlocal = 0;
                }
                $scope.installationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.installationChargesWithTaxlocal), 2);


                if ($scope.freightChargesTaxPercentage > 100 || parseFloat($("#freightChargesTaxPercentage").val()) > 100) {
                    $("#freightChargesTaxPercentage").val(0);
                    swal("Error!", 'Please enter valid TAX % for Freight Charges');
                };

                if ($scope.packingChargesTaxPercentage > 100 || parseFloat($("#packingChargesTaxPercentage").val()) > 100) {
                    $("#packingChargesTaxPercentage").val(0);
                    swal("Error!", 'Please enter valid TAX % for Packing Charges');
                };

                if ($scope.installationChargesTaxPercentage > 100 || parseFloat($("#installationChargesTaxPercentage").val()) > 100) {
                    $("#installationChargesTaxPercentage").val(0);
                    swal("Error!", 'Please enter valid TAX % for Installation Charges');
                };


                $scope.discountAmountlocal = $scope.discountAmount;
                if (isNaN($scope.discountAmount) || $scope.discountAmount == undefined || $scope.discountAmount < 0) {
                    $scope.discountAmountlocal = 0;
                    $scope.discountfreightValidation = true;
                }
                $scope.discountAmountlocal = $scope.precisionRound(parseFloat($scope.discountAmountlocal), 2);

                if ($scope.auctionItem.isTabular) {



                    $scope.totalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'itemPrice');

                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), 2);


                    if ($scope.auctionItem.isDiscountQuotation == 2) {
                        $scope.totalprice = 0;
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.totalprice += item.netPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.freightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.packingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.installationChargesWithTaxlocal), 2);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.vendorBidPrice - $scope.discountAmountlocal;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }
                }
                if (!$scope.auctionItem.isTabular) {
                    $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), 2);

                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.precisionRound(parseFloat($scope.totalprice), 2)
                                * $scope.precisionRound(parseFloat(tax.taxPercentage), 2) / 100;
                        }
                    })

                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.listRequirementTaxes.forEach(function (item, index) {
                        if (item.taxName == null || item.taxName == '' || item.taxName == undefined) {
                            $scope.taxValidation = true;
                        }
                        if (item.taxPercentage < 0 || item.taxPercentage == null || item.taxPercentage == undefined) {
                            $scope.taxValidation = true;
                        }
                        if (item.taxPercentage <= 0) {
                            item.taxPercentage = 0;
                        }
                    });

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.totalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2) +
                        $scope.precisionRound(parseFloat($scope.freightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.packingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.installationChargesWithTaxlocal), 2);

                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2);

                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice;
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2) -
                        $scope.precisionRound(parseFloat($scope.discountAmountlocal), 2);
                    $scope.vendorBidPrice = $scope.precisionRound(parseFloat($scope.vendorBidPrice), 2);
                    if (!$scope.vendorBidPrice || isNaN($scope.vendorBidPrice)) {
                        $scope.vendorBidPrice = 0;
                    }



                }




            }










            $scope.getRevTotalPrice = function (revfreightCharges, revtotalprice,
                revpackingCharges, packingChargesTaxPercentage, revpackingChargesWithTax,
                revinstallationCharges, installationChargesTaxPercentage, revinstallationChargesWithTax,
                freightChargesTaxPercentage, revfreightChargesWithTax)
            {

                $scope.revfreightCharges = revfreightCharges;
                $scope.revpackingCharges = revpackingCharges;
                $scope.revinstallationCharges = revinstallationCharges;
                $scope.ItemPriceValidation = false;
                $scope.discountfreightValidation = false;

                if ($scope.auctionItem && $scope.auctionItem.auctionVendors.length > 0 && $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {

                    $scope.revfreightCharges = $scope.precisionRound(parseFloat(revfreightCharges), 2);
                    $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                    $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(revfreightChargesWithTax), 2);

                    $scope.revfreightCharges = $scope.precisionRound(parseFloat(revfreightCharges), 2);
                    $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                    $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(revfreightChargesWithTax), 2);

                    $scope.revinstallationCharges = $scope.precisionRound(parseFloat(revinstallationCharges), 2);
                    $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);
                    $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat(revinstallationChargesWithTax), 2);
                }

                $scope.freightChargesTaxPercentage = $scope.precisionRound(parseFloat(freightChargesTaxPercentage), 2);
                $scope.packingChargesTaxPercentage = $scope.precisionRound(parseFloat(packingChargesTaxPercentage), 2);
                $scope.installationChargesTaxPercentage = $scope.precisionRound(parseFloat(installationChargesTaxPercentage), 2);

                $scope.revfreightCharges = $scope.precisionRound(parseFloat(revfreightCharges), 2);
                $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat(revfreightChargesWithTax), 2);

                $scope.revpackingCharges = $scope.precisionRound(parseFloat(revpackingCharges), 2);
                $scope.revpackingChargesWithTax = $scope.precisionRound(parseFloat(revpackingChargesWithTax), 2);

                $scope.revinstallationCharges = $scope.precisionRound(parseFloat(revinstallationCharges), 2);
                $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat(revinstallationChargesWithTax), 2);


                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = revtotalprice;
                    if (isNaN($scope.revtotalprice) || $scope.revtotalprice == undefined || $scope.revtotalprice < 0 || $scope.revtotalprice == '') {
                        $("#revtotalprice").val(0);
                        $scope.revvendorBidPrice = 0;
                    }
                }
                else {
                    if ($scope.auctionItemVendor.length > 0) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                            if (item.oldRevUnitPrice > 0 && item.revUnitPrice > 0 && item.oldRevUnitPrice > item.revUnitPrice && $scope.auctionItem.status == 'STARTED') {
                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    var reductionval = parseFloat(item.oldRevUnitPrice).toFixed(2) - parseFloat(item.revUnitPrice).toFixed(2);
                                    //reductionval = parseFloat(reductionval).toFixed(2);                            
                                    if (reductionval >= item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = false;

                                    } else if (reductionval < item.itemMinReduction && reductionval > 0 && item.itemMinReduction > 0) {
                                        $scope.ItemPriceValidation = true;
                                        item.revUnitPrice = item.oldRevUnitPrice;
                                        $scope.getData();
                                        swal("Error!", 'Your Unit price reduction should be greater than Min.unit Price limit :' + item.itemMinReduction, "error");
                                        return;
                                    }
                                }

                            }

                            if (isNaN(item.revitemPrice) || item.revitemPrice == undefined || item.revitemPrice < 0) {
                                $scope.ItemPriceValidation = true;
                            }
                        })
                    }
                }


                $scope.revfreightChargeslocal = $scope.revfreightCharges;
                if (isNaN($scope.revfreightCharges) || $scope.revfreightCharges == undefined || $scope.revfreightCharges < 0) {
                    $scope.revfreightChargeslocal = 0;
                    $scope.discountfreightValidation = true;
                }
                $scope.revfreightChargeslocal = $scope.precisionRound(parseFloat($scope.revfreightChargeslocal), 2);

                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentage), 2);
                if (isNaN($scope.freightChargesTaxPercentage) || $scope.freightChargesTaxPercentage == undefined || $scope.freightChargesTaxPercentage < 0) {
                    $scope.freightChargesTaxPercentagelocal = 0;
                }
                $scope.freightChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.freightChargesTaxPercentagelocal), 2);

                $scope.revfreightChargesWithTax = $scope.precisionRound(parseFloat($scope.revfreightChargeslocal +
                    (($scope.revfreightChargeslocal / 100) * ($scope.freightChargesTaxPercentagelocal))), 2);
                $scope.revfreightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revfreightChargesWithTax), 2);
                if (isNaN($scope.revfreightChargesWithTax) || $scope.revfreightChargesWithTax == undefined || $scope.revfreightChargesWithTax < 0) {
                    $scope.revfreightChargesWithTaxlocal = 0;
                }
                $scope.revfreightChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revfreightChargesWithTaxlocal), 2);


                               
                $scope.revpackingChargeslocal = $scope.precisionRound(parseFloat($scope.revpackingCharges), 2);
                if (isNaN($scope.revpackingCharges) || $scope.revpackingCharges == undefined || $scope.revpackingCharges < 0) {
                    $scope.revpackingChargeslocal = 0;
                }
                $scope.revpackingChargeslocal = $scope.precisionRound(parseFloat($scope.revpackingChargeslocal), 2);

                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentage), 2);
                if (isNaN($scope.packingChargesTaxPercentage) || $scope.packingChargesTaxPercentage == undefined || $scope.packingChargesTaxPercentage < 0) {
                    $scope.packingChargesTaxPercentagelocal = 0;
                }
                $scope.packingChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.packingChargesTaxPercentagelocal), 2);

                $scope.revpackingChargesWithTax = $scope.precisionRound(parseFloat($scope.revpackingChargeslocal +
                    (($scope.revpackingChargeslocal / 100) * ($scope.packingChargesTaxPercentagelocal))), 2);
                $scope.revpackingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revpackingChargesWithTax), 2);
                if (isNaN($scope.revpackingChargesWithTax) || $scope.revpackingChargesWithTax == undefined || $scope.revpackingChargesWithTax < 0) {
                    $scope.revpackingChargesWithTaxlocal = 0;
                }
                $scope.revpackingChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revpackingChargesWithTaxlocal), 2);



                $scope.revinstallationChargeslocal = $scope.precisionRound(parseFloat($scope.revinstallationCharges), 2);
                if (isNaN($scope.revinstallationCharges) || $scope.revinstallationCharges == undefined || $scope.revinstallationCharges < 0) {
                    $scope.revinstallationChargeslocal = 0;
                }
                $scope.revinstallationChargeslocal = $scope.precisionRound(parseFloat($scope.revinstallationChargeslocal), 2);

                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentage), 2);
                if (isNaN($scope.installationChargesTaxPercentage) || $scope.installationChargesTaxPercentage == undefined ||
                    $scope.installationChargesTaxPercentage < 0) {
                    $scope.installationChargesTaxPercentagelocal = 0;
                }
                $scope.installationChargesTaxPercentagelocal = $scope.precisionRound(parseFloat($scope.installationChargesTaxPercentagelocal), 2);

                $scope.revinstallationChargesWithTax = $scope.precisionRound(parseFloat($scope.revinstallationChargeslocal +
                    (($scope.revinstallationChargeslocal / 100) * ($scope.installationChargesTaxPercentagelocal))), 2);
                $scope.revinstallationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTax), 2);
                if (isNaN($scope.revinstallationChargesWithTax) || $scope.revinstallationChargesWithTax == undefined || $scope.revinstallationChargesWithTax < 0) {
                    $scope.revinstallationChargesWithTaxlocal = 0;
                }
                $scope.revinstallationChargesWithTaxlocal = $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTaxlocal), 2);




                if ($scope.auctionItem.isTabular) {
                    $scope.revtotalprice = _.sumBy($scope.auctionItemVendor.listRequirementItems, 'revitemPrice');
                    if ($scope.auctionItem.isDiscountQuotation == 2) {

                        $scope.revtotalprice = 0;

                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item = $scope.handlePrecision(item, 2);
                            $scope.revtotalprice += item.revnetPrice * item.productQuantity;
                            item = $scope.handlePrecision(item, 2);
                        });
                    }



                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                        }
                    })
                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2) +

                        $scope.precisionRound(parseFloat($scope.revfreightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revpackingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTaxlocal), 2);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), 2);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }

                    $scope.reduceBidValue = $scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.revvendorBidPrice), 2);

                    $scope.reduceBidAmount();

                    //reduceBidValue

                }
                if (!$scope.auctionItem.isTabular) {
                    $scope.revtotalprice = $scope.precisionRound(parseFloat(revtotalprice), 2);


                    $scope.calculatedSumOfAllTaxes = 0;
                    $scope.listRequirementTaxes.forEach(function (tax, index) {
                        if (tax.taxPercentage != '' && tax.taxPercentage != undefined) {
                            $scope.calculatedSumOfAllTaxes += $scope.revtotalprice * tax.taxPercentage / 100;
                        }
                    })
                    $scope.vendorTaxes = $scope.calculatedSumOfAllTaxes;

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2) +
                        $scope.precisionRound(parseFloat($scope.vendorTaxes), 2) +

                        $scope.precisionRound(parseFloat($scope.revfreightChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revpackingChargesWithTaxlocal), 2) +
                        $scope.precisionRound(parseFloat($scope.revinstallationChargesWithTaxlocal), 2);

                    $scope.revvendorBidPrice = $scope.precisionRound(parseFloat($scope.revvendorBidPrice), 2);
                    if (!$scope.revvendorBidPrice || isNaN($scope.revvendorBidPrice)) {
                        $scope.revvendorBidPrice = 0;
                    }
                }

            }

            $scope.currencyValidationStyle = {};
            $scope.uploadQuotationConfirmation = function (quotationType) {
                swal({
                    title: "Please validate Terms & Conditions",
                    text: 'Kindly review Delivery and Payment terms. Once the quotation is submitted and approved, change in delivery and payment terms are considered to be a violation. Click on Cancel to review and submit again.',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Submit",
                    closeOnConfirm: true
                }, function () {
                        $scope.uploadQuotation(quotationType);
                });
            };

            $scope.uploadQuotation = function (quotationType) {

                $scope.currencyValidationStyle = {};

                if ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == null ||
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency == '' ||
                    $scope.auctionItem.auctionVendors[0].selectedVendorCurrency == undefined) {
                    document.getElementById('priceDiv').scrollIntoView();
                    $scope.currencyValidationStyle = {
                        'border-color': 'red'
                    };
                    growlService.growl('Please Select Currency', "inverse");
                    return;
                };

                //if ($scope.regretError) {
                //    growlService.growl("Please enter regret comments.", "inverse");
                //    return;
                //}

                var uploadQuotationError = false;

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                    if (uploadQuotationError == true) {
                        return false;
                    }
                    var slno = index + 1;
                    if (item.isRegret) {
                        if (item.regretComments == '' || item.regretComments == null || item.regretComments == undefined) {
                            growlService.growl("Please enter regret comments.", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                    } else {
                        if (item.unitPrice <= 0 && item.isCoreProductCategory > 0) {
                            growlService.growl("Please enter Unit price for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        if ((item.hsnCode == '' || item.hsnCode == null || item.hsnCode == undefined) && item.isCoreProductCategory > 0) {
                            growlService.growl("Please enter HSN Code for Product " + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        if ((item.hsnCode.length < 6 || item.hsnCode.length > 8) && item.isCoreProductCategory > 0) {
                            growlService.growl("HSN Code must be min 6 or max 8 length for Product" + slno + ".", "inverse");
                            uploadQuotationError = true;
                            return false;
                        }
                        //if (!item.isRegret) {
                        //    if ((item.ITEM_DELV_DATE == '' || item.ITEM_DELV_DATE == null || item.ITEM_DELV_DATE == undefined) && item.isCoreProductCategory > 0) {
                        //        growlService.growl("Please Select Delivery Date for Product " + slno + ".", "inverse");
                        //        uploadQuotationError = true;
                        //        return false;
                        //    }
                        //}
                        //else if (item.unitPrice > 0 && (+item.iGst == 0 && (+item.cGst == 0 && +item.sGst == 0)) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                        //    growlService.growl("Please enter SGST/CGST/IGST for Product " + slno + ".", "inverse");
                        //    uploadQuotationError = true;
                        //    return false;
                        //}
                        //else if (item.unitPrice > 0 && (+item.cGst == 0 && +item.sGst > 0) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                        //    growlService.growl("Please enter CGST for Product " + slno + ".", "inverse");
                        //    uploadQuotationError = true;
                        //    return false;
                        //}
                        //else if (item.unitPrice > 0 && (+item.cGst > 0 && +item.sGst == 0) && ($scope.auctionItem.auctionVendors[0].selectedVendorCurrency == 'INR')) {
                        //    growlService.growl("Please enter SGST for Product " + slno + ".", "inverse");
                        //    uploadQuotationError = true;
                        //    return false;
                        //}
                    }


                });

                var TaxFiledValidation = 0;

                if ($scope.CSGSTFields == false) {
                    TaxFiledValidation = 1;
                }

               

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    var slno1 = itemIndexs + 1;
                    if (item.isCoreProductCategory && item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            var message = '';
                            var name = subItem.NAME.toUpperCase();

                            if (!item.isRegret) {
                                if (name == "SUPPLIER PRODUCT SPECIFICATIONS") {
                                    message = "Please enter Supplier Product Specifications for Product "
                                } else if (name == "SUPPLIER SPECIFICATIONS") {
                                    message = "Please enter Supplier Specifications for Product "
                                }


                                if ((name == "SUPPLIER PRODUCT SPECIFICATIONS" || name == "SUPPLIER SPECIFICATIONS") && (subItem.SPECIFICATION == undefined || subItem.SPECIFICATION == null || subItem.SPECIFICATION == '')) {
                                    growlService.growl(message + slno1 + ".", "inverse");
                                    uploadQuotationError = true;
                                    return false;
                                }
                                if ((name == "SUPPLIER MAKE") && (subItem.SPECIFICATION == undefined || subItem.SPECIFICATION == null || subItem.SPECIFICATION == '')) {
                                    growlService.growl("Please enter Supplier Make for Product " + slno1 + ".", "inverse");
                                    uploadQuotationError = true;
                                    return false;
                                }
                            }
                           
                            

                            subItem.dateCreated = "/Date(1561000200000+0530)/";
                            subItem.dateModified = "/Date(1561000200000+0530)/";
                            
                        });
                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    }


                    //Remove Special Chars
                    //item.productQuotationTemplateJson = item.productQuotationTemplateJson.replace(/\'/gi, "\\'");
                    //item.productQuotationTemplateJson = item.productQuotationTemplateJson.replace(/(\r\n|\n|\r)/gm, "");
                    //let tempTemplateArray = JSON.parse(item.productQuotationTemplateJson);
                    //subItem.SPECIFICATION = item.productQuotationTemplateJson.SPECIFICATION.replace(/\'/gi, "\\'");
                    //if (tempTemplateArray && tempTemplateArray.length > 0) {
                    //    tempTemplateArray.forEach(function (subItem, subItemIndex) {
                    //        if (subItem.SPECIFICATION) {
                    //            subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/"/g, '\\"');
                    //        }
                    //    });

                    //    item.productQuotationTemplateJson = JSON.stringify(tempTemplateArray);
                    //}

                });
                if (uploadQuotationError == true) {
                    return;
                }
                
                var params = {
                    'quotationObject': $scope.auctionItemVendor.listRequirementItems, // 1
                    'userID': userService.getUserId(),
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'price': $scope.totalprice, // 5
                    'tax': $scope.vendorTaxes,
                    'vendorBidPrice': $scope.vendorBidPrice,
                    'warranty': $scope.warranty,
                    'duration': $scope.duration,
                    'payment': $scope.payment, // 10
                    'gstNumber': $scope.gstNumber,
                    'validity': $scope.validity,
                    'otherProperties': $scope.otherProperties,
                    'quotationType': quotationType,
                    'revised': 0, // 15
                    'discountAmount': $scope.discountAmount,
                    'listRequirementTaxes': $scope.listRequirementTaxes,
                    'quotationAttachment': $scope.quotationAttachment,
                    'multipleAttachments': $scope.multipleAttachmentsList,
                    'TaxFiledValidation': TaxFiledValidation,

                    //'freightCharges': parseFloat($scope.freightCharges), // 20
                    //'freightChargesTaxPercentage': parseFloat($scope.freightChargesTaxPercentage),
                    //'freightChargesWithTax': parseFloat($scope.freightChargesWithTax),

                    //'packingCharges': parseFloat($scope.packingCharges),
                    //'packingChargesTaxPercentage': parseFloat($scope.packingChargesTaxPercentage),
                    //'packingChargesWithTax': parseFloat($scope.packingChargesWithTax), // 25

                    //'installationCharges': parseFloat($scope.installationCharges),
                    //'installationChargesTaxPercentage': parseFloat($scope.installationChargesTaxPercentage),
                    //'installationChargesWithTax': parseFloat($scope.installationChargesWithTax),

                    //'revfreightCharges': parseFloat($scope.revfreightCharges),
                    //'revfreightChargesWithTax': parseFloat($scope.revfreightChargesWithTax), // 30

                    //'revpackingCharges': parseFloat($scope.revpackingCharges),
                    //'revpackingChargesWithTax': parseFloat($scope.revpackingChargesWithTax),

                    //'revinstallationCharges': parseFloat($scope.revinstallationCharges),
                    //'revinstallationChargesWithTax': parseFloat($scope.revinstallationChargesWithTax),

                    'selectedVendorCurrency': $scope.auctionItem.auctionVendors[0].selectedVendorCurrency, //35
                    'isVendAckChecked': $scope.auctionItem.auctionVendors[0].isVendAckChecked
                };

                params.unitPrice = $scope.auctionItem.unitPrice;
                params.productQuantity = $scope.auctionItem.productQuantity;
                params.cGst = $scope.auctionItem.cGst;
                params.sGst = $scope.auctionItem.sGst;
                params.iGst = $scope.auctionItem.iGst;
                params.INCO_TERMS = $scope.auctionItem.auctionVendors[0].INCO_TERMS;

                //auctionItem.auctionVendors[0].INCO_TERMS

                if (params.warranty == null || params.warranty == '' || params.warranty == undefined) {
                    params.warranty = '';
                    growlService.growl('Please enter Warranty.', "inverse");
                    return false;
                }

                if (params.validity == null || params.validity == '' || params.validity == undefined) {
                    params.validity = '';
                    growlService.growl('Please enter Price Validity.', "inverse");
                    return false;
                }

                if (params.otherProperties == null || params.otherProperties == '' || params.otherProperties == undefined) {
                    params.otherProperties = '';
                }

                if (params.duration == null || params.duration == '' || params.duration == undefined) {
                    params.duration = '';
                    growlService.growl('Please enter Delivery Terms.', "inverse");
                    return false;
                }

                if (params.payment == null || params.payment == '' || params.payment == undefined) {
                    params.payment = '';
                    growlService.growl('Please enter Payment Terms.', "inverse");
                    return false;
                }

                //if (params.INCO_TERMS == null || params.INCO_TERMS == '' || params.INCO_TERMS == undefined) {
                //    params.payment = '';
                //    if ($scope.totalprice > 0) {
                //        growlService.growl('Please select Inco Terms.', "inverse");
                //        return false;
                //    }
                //}

                if (params.gstNumber == null || params.gstNumber == '' || params.gstNumber == undefined) {
                    params.gstNumber = '';
                }

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    params.price = $scope.revtotalprice,
                        params.tax = $scope.vendorTaxes,
                        params.vendorBidPrice = $scope.revvendorBidPrice,
                        params.revised = 1,

                        params.freightCharges = $scope.revfreightCharges,
                        params.revinstallationCharges = $scope.revinstallationCharges,
                        params.packingCharges = $scope.revpackingCharges
                }


                if (($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED') && $scope.auctionItem.isQuotationPriceLimit && $scope.vendorBidPrice > $scope.auctionItem.quotationPriceLimit) {
                    swal("Error!", 'Your Amount Should be less than Quotation Price limit :' + $scope.auctionItem.quotationPriceLimit, "error");
                    return;
                }

                $scope.taxEmpty = false;
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                    if ((item.unitMRP > 0 && $scope.auctionItem.isDiscountQuotation == 1) || (item.unitPrice > 0 && $scope.auctionItem.isDiscountQuotation == 0)) {
                        if ((item.cGst == 0 || item.sGst == 0 || item.cGst == undefined || item.sGst == undefined || item.cGst > 100 || item.sGst > 100) && (item.iGst == 0 || item.iGst == undefined || item.iGst > 100)) {
                            // $scope.taxEmpty = true;
                            // NOT MANDATORY
                        }
                    } else {
                        if ((item.cGst == undefined || item.sGst == undefined) && (item.iGst == undefined)) {
                            // $scope.taxEmpty = true;
                            // NOT MANDATORY
                        }
                    }
                });


                if ($scope.taxEmpty == true) {

                    swal({
                        title: "warning!",
                        text: "Please fill TAXES slab",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    });

                }
                else {
                    auctionsService.getdate()
                        .then(function (responseFromServer) {
                            var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                            var auctionStart = new Date(parseInt($scope.auctionItem.startTime.substr(6)));
                            var timeofauctionstart = auctionStart.getTime();
                            var currentServerTime = dateFromServer.getTime();

                            if ($scope.auctionItem.status == "Negotiation Ended") {


                                // $scope.DeleteRequirementTerms();

                                if ($scope.auctionItem.status == 'Negotiation Ended') {
                                } else {
                                    // $scope.SaveRequirementTerms($scope.reqId);
                                }

                                params.quotationObject.forEach(function (item, index) {
                                    item.ITEM_DELV_DATE1 = $scope.convertDate(item.ITEM_DELV_DATE);
                                });

                                auctionsService.uploadQuotation(params)
                                    .then(function (req) {
                                        if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                                            if (req.errorMessage == '') {
                                                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                                $scope.invokeSignalR('CheckRequirement', parties);
                                                    requirementHub.invoke('CheckRequirement', parties, function () {                    
                                                });
                                            }

                                            let successText = 'Your Quotation uploaded successfully';
                                            let type = "success";
                                            var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                            var quotationFreeTimeMoment = moment(quotationFreeTime);
                                            var today = moment();
                                            //if (quotationFreeTimeMoment < today) {
                                            //    successText = 'Quotation Freeze time has passed, you have lost an opportunity to participate in negotiation.';
                                            //    type = "warning";
                                            //}

                                            swal({
                                                title: "Thanks!",
                                                text: successText,
                                                type: type,
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            },
                                                function () {
                                                    location.reload();
                                                });

                                        } else {
                                            swal({
                                                title: "Error",
                                                text: req.errorMessage,
                                                type: "error",
                                                showCancelButton: false,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Ok",
                                                closeOnConfirm: true
                                            },
                                                function () {
                                                    location.reload();
                                                });
                                        }
                                    });

                            }
                            else {
                                if (timeofauctionstart - currentServerTime >= 3600000 &&
                                    $scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {

                                    // $scope.DeleteRequirementTerms();

                                    if ($scope.auctionItem.status == 'Negotiation Ended') {
                                    } else {
                                        //$scope.SaveRequirementTerms($scope.reqId);
                                    }

                                    params.quotationObject.forEach(function (item, index) {
                                        item.ITEM_DELV_DATE1 = $scope.convertDate(item.ITEM_DELV_DATE);
                                    });

                                    auctionsService.uploadQuotation(params)
                                        .then(function (req) {
                                            if (req.errorMessage == '' || req.errorMessage == 'reviced') {
                                                if (req.errorMessage == '') {
                                                    var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                                    $scope.invokeSignalR('CheckRequirement', parties);
                                                        requirementHub.invoke('CheckRequirement', parties, function () {                    
                                                    });
                                                }

                                                let successText = 'Your Quotation uploaded successfully';
                                                let type = "success";
                                                var quotationFreeTime = moment($scope.auctionItem.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                                                var quotationFreeTimeMoment = moment(quotationFreeTime);
                                                var today = moment();
                                                //if (quotationFreeTimeMoment < today) {
                                                //    successText = 'Quotation Freeze time has passed, you have lost an opportunity to participate in negotiation.';
                                                //    type = "warning";
                                                //}

                                                swal({
                                                    title: "Thanks!",
                                                    text: successText,
                                                    type: type,
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                },
                                                    function () {
                                                        location.reload();
                                                    });

                                            } else {
                                                swal({
                                                    title: "Error",
                                                    text: req.errorMessage,
                                                    type: "error",
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#DD6B55",
                                                    confirmButtonText: "Ok",
                                                    closeOnConfirm: true
                                                },
                                                    function () {
                                                        location.reload();
                                                    });
                                            }
                                        });
                                } else {

                                    var message = "You have missed the deadline for Quotation Submission"
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected == 0 && $scope.auctionItem.status != "Negotiation Ended") { message = "Your Quotation Already Approved You cant Update Quotation Please Contact PRM360" }

                                    swal({
                                        title: "Error",
                                        text: message,
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                        function () {
                                            location.reload();
                                        });
                                    $scope.getData();
                                }
                            }
                        });
                }
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.file = $("#" + id)[0].files[0];
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {

                        if (id == "excelquotation") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadquotationsfromexcel();
                        } else if (id == "clientsideupload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation();
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else if (id == "quotationBulkUpload") {
                            var bytearray = new Uint8Array(result);
                            $scope.quotationAttachment = $.makeArray(bytearray);
                            $scope.uploadclientsidequotation('quotationBulkUpload');
                            $scope.quotationAttachment = null;
                            $scope.file = [];
                            $scope.file.name = '';
                        }
                        else {
                            var bytearray = new Uint8Array(result);
                            var arrayByte = $.makeArray(bytearray);
                            var ItemFileName = $scope.file.name;
                            var index = _.indexOf($scope.auctionItemVendor.listRequirementItems, _.find($scope.auctionItemVendor.listRequirementItems, function (o) {
                                return o.productSNo == id;
                            }));
                            var obj = $scope.auctionItemVendor.listRequirementItems[index];
                            obj.itemAttachment = arrayByte;
                            obj.attachmentName = ItemFileName;
                            $scope.auctionItemVendor.listRequirementItems.splice(index, 1, obj);
                        }
                    });
            };

            $scope.QuotationRatioButton = {
                upload: 'generate'
            };

            $scope.rejectreson = '';
            $scope.rejectresonValidation = false;

            $scope.QuatationAprovelvalue = true;

            $scope.QuatationAprovel = function (userID, value, comment, action, bulkQuotationString,technicalScore) {
               //auctionVendor

                if (bulkQuotationString.isQuotationRejected) { // !bulkQuotationString
                    if (comment) {
                        comment = comment.trim();
                    }

                    if (value && (comment == '' || comment == undefined)) {
                        $scope.rejectresonValidation = true;
                        return false;
                    } else {
                        $scope.rejectresonValidation = false;
                    }
                }
                let bulkquottype = (typeof bulkQuotationString);
                $scope.bulkquotarray = [];
                if (bulkquottype == "object") {
                    var quotationArr = [];
                    quotationArr.push({
                        vendorID: bulkQuotationString.vendorID,
                        reqID: bulkQuotationString.requirementID,
                        sessionID: userService.getUserToken(),
                        customerID: +userService.getUserId(),
                        technicalScore: bulkQuotationString.technicalScore,
                        value: $scope.auctionItem.status == 'Negotiation Ended' ? bulkQuotationString.isRevQuotationRejected : bulkQuotationString.isQuotationRejected,
                        reason: $scope.auctionItem.status == 'Negotiation Ended' ? bulkQuotationString.revQuotationRejectedComment : bulkQuotationString.quotationRejectedComment,
                        action: $scope.auctionItem.status == 'Negotiation Ended' ? 'revquotation' : 'quotation'
                    });
                    $scope.bulkquotarray.push(bulkQuotationString);
                    bulkQuotationString = JSON.stringify(quotationArr);
                }
                var params = {
                    'vendorID': userID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'customerID': userService.getUserId(),
                    'value': value,
                    'reason': comment,
                    'action': action,
                    'technicalScore': technicalScore,
                    'bulkQuotationString': bulkQuotationString ? bulkQuotationString : ''
                };

                if (!value) {
                    var text = "Quotation Approved Successfully";
                }
                if (value) {
                    var text = "Quotation Rejected Successfully";
                }
                //if (auctionVendor && $scope.auctionItem.isTechScoreReq && auctionVendor.technicalScore <= 0 && !value) {
                //    swal("Error!", 'Vendor can be approved only after Technical Score.', "error");
                //    return;
                //}

               
                auctionsService.QuatationAprovel(params).then(function (req) {
                    if (req.errorMessage == '') {
                        swal({
                            title: "Done!",
                            text: text,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        },
                            function () {
                                //$scope.recalculate('QUOTATION_APPROVAL_CUSTOMER', userID);
                                location.reload();
                            });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });

               
            };

            $scope.submitButtonShow = 0;

            $scope.submitButtonShowfunction = function (value, auctionVendor) {

                auctionVendor.quotationRejectedComment = '';
                $scope.submitButtonShow = value;
                let tempArr = _.filter($scope.vendorApprovals, function (vendor) {
                    return Number(vendor.vendorID) !== Number(auctionVendor.vendorID)
                });

                tempArr.push(auctionVendor);
                $scope.vendorApprovals = tempArr;

                $scope.auctionItem.auctionVendors.forEach(function (v, vI) {
                    if (v.vendorID == auctionVendor.vendorID) {
                        v.isApproveOrRejectClicked = true;
                    }
                })

            }

            $scope.saveApprovalStatus = function () {
                //$scope.vendorApprovals.forEach(function (auctionVendor, index) {
                //    if ($scope.auctionItem.status == 'Negotiation Ended') {
                //        $scope. (auctionVendor.vendorID, auctionVendor.isRevQuotationRejected, auctionVendor.revQuotationRejectedComment, 'revquotation', auctionVendor);
                //    } else {
                //        $scope.QuatationAprovel(auctionVendor.vendorID, auctionVendor.isQuotationRejected, auctionVendor.quotationRejectedComment, 'quotation', auctionVendor);
                //    }
                //});

                var quotationArr = [];
                $scope.rejectresonValidation = false;

                $scope.vendorApprovals.forEach(function (vendorItem, vendorIndex) {
                    $scope.value1 = $scope.auctionItem.status == 'Negotiation Ended' ? vendorItem.isRevQuotationRejected : vendorItem.isQuotationRejected;
                    if (vendorItem && $scope.auctionItem.isTechScoreReq && vendorItem.technicalScore <= 0 && !$scope.value1) {
                        swal("Error!", 'Vendor can be approved only after Technical Score.', "error");
                        return;
                    }

                    quotationArr.push({
                        vendorID: vendorItem.vendorID,
                        reqID: $scope.auctionItem.requirementID,
                        sessionID: userService.getUserToken(),
                        customerID: +userService.getUserId(),
                        technicalScore: vendorItem.technicalScore,
                        value: $scope.auctionItem.status == 'Negotiation Ended' ? vendorItem.isRevQuotationRejected : vendorItem.isQuotationRejected,
                        reason: $scope.auctionItem.status == 'Negotiation Ended' ? vendorItem.revQuotationRejectedComment : vendorItem.quotationRejectedComment,
                        action: $scope.auctionItem.status == 'Negotiation Ended' ? 'revquotation' : 'quotation'
                    });

                    if ($scope.auctionItem.status == 'Negotiation Ended') {
                        if (vendorItem.isRevQuotationRejected) {
                            var comment = "";
                            comment = vendorItem.revQuotationRejectedComment;

                            if (comment) {
                                comment = comment.trim();
                            }

                            if ((comment == '' || comment == undefined)) {
                                $scope.rejectresonValidation = true;
                                return false;
                            } else {
                                $scope.rejectresonValidation = false;
                            }
                        }
                    } else {
                        if (vendorItem.isQuotationRejected) {
                            var comment = "";
                            comment = vendorItem.quotationRejectedComment;

                            if (comment) {
                                comment = comment.trim();
                            }

                            if ((comment == '' || comment == undefined)) {
                                $scope.rejectresonValidation = true;
                                return false;
                            } else {
                                $scope.rejectresonValidation = false;
                            }
                        }
                    }

                });
                let showRejectValidation = _.some($scope.vendorApprovals, function (vendor) {
                    return $scope.rejectresonValidation;
                });

                quotationArr = JSON.stringify(quotationArr);
                if (!showRejectValidation) {
                    $scope.QuatationAprovel(0, 0, '', '', quotationArr,0);
                }
            };

            $scope.NegotiationSettingsValidationMessage = '';

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {

                $scope.days = days;
                $scope.hours = hours;
                $scope.mins = mins;


                //$log.info($scope.days);
                //$log.info($scope.hours);
                //$log.info($scope.mins);

                $scope.NegotiationSettingsValidationMessage = '';

                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    if (parseFloat(minReduction) <= 0 || minReduction == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Min.Amount reduction';
                        return;
                    }

                    if (parseFloat(rankComparision) <= 0 || rankComparision == undefined) {
                        $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price';
                        return;
                    }

                    if (parseFloat(minReduction) >= 0 && parseFloat(rankComparision) > parseFloat(minReduction)) {
                        $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                        return;
                    }
                } else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    $scope.NegotiationSettings.minReductionAmount = 0;
                    $scope.NegotiationSettings.rankComparision = 0;
                }

                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours == 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minutes';
                    return;
                }
                if (mins < 5 && hours == 0 && days == 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            }

            $scope.listRequirementTaxes = [];
            $scope.reqTaxSNo = 1;

            $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

            $scope.requirementTaxes =
                {
                    taxSNo: $scope.reqTaxSNo++,
                    taxName: '',
                    taxPercentage: 0
                }

            $scope.AddTax = function () {

                if ($scope.listRequirementTaxes.length > 4) {
                    return;
                }
                $scope.requirementTaxes =
                    {
                        taxSNo: $scope.reqTaxSNo++,
                        taxName: '',
                        taxPercentage: 0
                    }
                $scope.listRequirementTaxes.push($scope.requirementTaxes);
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice);
            };

            $scope.deleteTax = function (SNo) {
                $scope.listRequirementTaxes = _.filter($scope.listRequirementTaxes, function (x) { return x.taxSNo !== SNo; });
                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;
                $scope.getTotalPrice($scope.discountAmount, $scope.freightCharges, $scope.totalprice);
            };

            $scope.reports = {};

            $scope.isReportGenerated = 0;

            $scope.GetReportsRequirement = function () {
                auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.reports = response;
                        $scope.getData();
                        $scope.isReportGenerated = 1;
                    });
            }

            $scope.generateReports = function () {
                $scope.isReportGenerated = 0;
            }

            $scope.updatePriceCap = function (auctionVendor) {
                let levelPrice = 1;
                $scope.formRequest.priceCapValueMsg = '';
                let l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[0].totalPriceIncl), 2);

                if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                    levelPrice = 2;
                    l1Vendor = $scope.precisionRound(parseFloat($scope.auctionItem.auctionVendors[1].totalPriceIncl), 2);
                }

                if ($scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), 2) > 0 && $scope.precisionRound(parseFloat($scope.formRequest.priceCapValue), 2) < l1Vendor) {
                    let priceDetails = {
                        uID: auctionVendor.vendorID,
                        reqID: $scope.reqId,
                        price: $scope.formRequest.priceCapValue,
                        sessionID: userService.getUserToken()
                    };

                    auctionsService.UpdatePriceCap(priceDetails)
                        .then(function (response) {
                            if (response.errorMessage == '') {
                                growlService.growl('Successfully updated price cap.', "success");
                                $scope.formRequest.priceCapValueMsg = '';
                                auctionVendor.totalPriceIncl = $scope.formRequest.priceCapValue;
                            }
                            else {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });
                }
                else {
                    $scope.formRequest.priceCapValueMsg = 'Price cap amount should be less L' + levelPrice + ' price ' + l1Vendor;
                    // growlService.growl('Price cap amount should be more than 0  and less than L1 price ' + l1Vendor, "inverse");
                }
            }

            $scope.savepricecap = function () {

                //if ($scope.IS_CB_ENABLED == true) {
                //    $scope.auctionItem.reqType = 'REGULAR';
                //    $scope.auctionItem.priceCapValue = 0;
                //}

                //if ($scope.auctionItem.qcsWFId > 0) {
                //    swal("Error!", 'Please provide error text for not allowing to change bidding type as QCS workflow trigged ', "error");
                //    return;
                //}


                $scope.l1Price = $scope.auctionItem.auctionVendors[0].totalPriceIncl;

                $scope.l1CompnyName = $scope.auctionItem.auctionVendors[0].companyName;

                if ($scope.l1CompnyName == 'PRICE_CAP') {
                    $scope.l1Price = $scope.auctionItem.auctionVendors[1].totalPriceIncl;
                }


                if (!$scope.IS_CB_ENABLED)
                {
                    $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);
                    if ($scope.NegotiationSettingsValidationMessage !== '') {
                        $scope.Loding = false;
                        return;
                    }

                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                }
                
                if ($scope.auctionItem.reqType == 'PRICE_CAP' && ($scope.auctionItem.priceCapValue <= 0 || $scope.auctionItem.priceCapValue == undefined || $scope.auctionItem.priceCapValue == '')) {
                    swal("Error!", 'Please Enter Valid Price cap', "error");
                    return;
                }

                var params = {
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'userID': userService.getUserId(),
                    'reqType': $scope.auctionItem.reqType,
                    'priceCapValue': $scope.auctionItem.priceCapValue,
                    'isUnitPriceBidding': $scope.auctionItem.isUnitPriceBidding,
                    //#CB-0-2018-12-05
                    'IS_CB_ENABLED': $scope.IS_CB_ENABLED,
                    'IS_CB_NO_REGRET': $scope.IS_CB_NO_REGRET
                };

                if ($scope.IS_CB_ENABLED == true) {

                    swal({
                        title: "Type of bidding cannot be changed once selected. The only way to change it is by creating a new requirement.",
                        text: "Kindly confirm the action.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "OK",
                        closeOnConfirm: true
                    }, function () {
                        auctionsService.savepricecap(params).then(function (req) {
                            if (req.errorMessage == '') {
                                //code before the pause
                                setTimeout(function () {
                                    //do what you need here
                                    swal({
                                        title: "Done!",
                                        text: 'Requirement Type and Bidding Type Saved Successfully',
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55",
                                        confirmButtonText: "Ok",
                                        closeOnConfirm: true
                                    },
                                    function () {
                                        if ($scope.auctionItem.status == 'STARTED') {
                                            $scope.recalculate('', 0, true);
                                        }
                                        location.reload();
                                    });
                                }, 1000);
                            } else {
                                swal("Error!", req.errorMessage, "error");
                            }
                        });
                    })
                    
                } else {
                    auctionsService.savepricecap(params).then(function (req) {
                        if (req.errorMessage == '') {
                            swal({
                                title: "Done!",
                                text: 'Requirement Type and Bidding Type Saved Successfully',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    if ($scope.auctionItem.status == 'STARTED') {
                                        $scope.recalculate('', 0, true);
                                    }
                                    location.reload();
                                });
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }

            }

            $scope.goToReqTechSupport = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("reqTechSupport", { "reqId": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToMaterialReceived = function () {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("Material-Received", { "Id": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToVendorPo = function (vendorID) {
                var url = $state.href("po-list", { "reqID": $scope.reqId, "vendorID": vendorID, "poID": 0 });
                window.open(url, '_blank');
            }

            $scope.goToDescPo = function () {
                var url = $state.href("desc-po", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToPreNegotiation = function (Id) {
                var url = $state.href("req-savingsPreNegotiation", { "Id": $scope.reqId });
                window.open(url, '_blank');
            }

            $scope.goToComparatives = function (reqID) {
                if ($scope.auctionItem.isDiscountQuotation != 2) {
                    var url = $state.href("comparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
                else if ($scope.auctionItem.isDiscountQuotation == 2) {
                    var url = $state.href("marginComparatives", { "reqID": $scope.reqId });
                    window.open(url, '_blank');
                }
            }

            $scope.goToQCS = function (reqID, qcsID) {
                var url = $state.href("qcs", { "reqID": $scope.reqId, "qcsID": qcsID });
                window.open(url, '_self');
            };

            $scope.goToQCSList = function (reqID) {
                var url = $state.href("list-qcs", { "reqID": $scope.reqId});
                window.open(url, '_blank');
            };

            $scope.goToCostQCS = function (reqID) {
                var url = $state.href("cost-comparisions-qcs", { "reqID": $scope.reqId });
                window.open(url, '_blank');
            };

            $scope.goToRMQCS = function (reqID, itemID) {
                var url = $state.href("qcsRM", { "reqID": $scope.reqId, "itemID": itemID });
                window.open(url, '_blank');
            };

            $scope.isItemChanged = function (product) {
                var isSame = 0;
                var filterCustItem = _.filter($scope.auctionItem.listRequirementItems, function (custItem) {
                    return custItem.itemID === product.itemID;
                });

                if (filterCustItem.length > 0) {
                    if (filterCustItem[0].productIDorNameCustomer.toUpperCase() !== product.productIDorName.toUpperCase()
                        || filterCustItem[0].productNoCustomer.toUpperCase() !== product.productNo.toUpperCase()) {
                        isSame = 1;
                    }
                }

                if (filterCustItem.length <= 0) {
                    isSame = 2;
                }

                return isSame;
            }

            $scope.paymentRadio = false;

            $scope.paymentlist = [];

            $scope.addpaymentvalue = function () {
                var listpaymet =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'PAYMENT',
                    paymentType: '+'
                };

                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    listpaymet.isRevised = 1
                } else {
                    listpaymet.isRevised = 0
                }

                $scope.paymentlist.push(listpaymet);
            };

            $scope.delpaymentvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }

                $scope.paymentlist.splice(val, 1);
            };

            $scope.resetpayment = function () {
                $scope.paymentlist = [];
            };

            $scope.deliveryRadio = false;

            $scope.deliveryList = [];

            $scope.adddeliveryvalue = function (val) {
                var deliveryObj =
                {
                    reqTermsID: 0,
                    reqID: $scope.stateParamsReqID,
                    userID: userService.getUserId(),
                    reqTermsDays: 0,
                    reqTermsPercent: 0,
                    reqTermsType: 'DELIVERY',
                    refReqTermID: val
                };


                if ($scope.auctionItem.status == 'Negotiation Ended') {
                    deliveryObj.isRevised = 1
                } else {
                    deliveryObj.isRevised = 0
                }

                $scope.deliveryList.push(deliveryObj);
            };

            $scope.deldeliveryvalue = function (val, id) {

                if (id > 0) {
                    $scope.listTerms.push(id);
                }


                $scope.deliveryList.splice(val, 1);
            };

            $scope.resetdelivery = function () {
                $scope.deliveryList = [];
            };

            $scope.listRequirementTerms = [];

            $scope.SaveRequirementTerms = function (reqID) {

                $scope.listRequirementTerms = [];

                $scope.deliveryList.forEach(function (item, index) {
                    item.reqID = reqID;
                    $scope.listRequirementTerms.push(item);
                });

                $scope.paymentlist.forEach(function (item, index) {

                    item.reqID = reqID;

                    if (item.paymentType == '-') {
                        item.reqTermsDays = -(item.reqTermsDays);
                    }

                    $scope.listRequirementTerms.push(item);
                });

                var params = {
                    "listRequirementTerms": $scope.listRequirementTerms,
                    sessionID: userService.getUserToken()
                };
                if ($scope.formRequest.isForwardBidding) {

                } else {
                    auctionsService.SaveRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {
                                //growlService.growl(".", "success");
                                //$scope.GetCompanyDepartments();
                                //$scope.addnewdeptView = false;
                                //$scope.department = {
                                //    userID: $scope.userID,
                                //    deptID: 0,
                                //    sessionID: $scope.sessionID
                                //};
                                ////$window.history.back();
                            }
                        });
                }


            };

            $scope.listTerms = [];

            $scope.DeleteRequirementTerms = function () {

                var params = {
                    "listTerms": $scope.listTerms,
                    sessionID: userService.getUserToken()
                };

                if ($scope.formRequest.isForwardBidding) {

                } else {
                    auctionsService.DeleteRequirementTerms(params)
                        .then(function (response) {
                            if (response.errorMessage != '') {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                            else {

                            }
                        });
                }


            };

            $scope.mrpDiscountCalculation = function () {

                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 2) {

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.netPrice = item.costPrice * item.netPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.marginAmount = item.unitMRP - item.netPrice;

                            item.revnetPrice = 1 + (item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revnetPrice = item.revCostPrice * item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;


                            item = $scope.handlePrecision(item, 2);
                            //item.unitDiscount = item.marginAmount / item.costPrice;
                            item.unitDiscount = item.marginAmount / item.netPrice;
                            item.unitDiscount = item.unitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                            item.revitemPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item = $scope.handlePrecision(item, 2);
                            item.revmarginAmount = item.unitMRP - item.revitemPrice;
                            item = $scope.handlePrecision(item, 2);
                            //item.revUnitDiscount = item.revmarginAmount / item.revCostPrice;
                            item.revUnitDiscount = item.revmarginAmount / item.revnetPrice;

                            item.revUnitDiscount = item.revUnitDiscount * 100;
                            item = $scope.handlePrecision(item, 2);
                        }

                        if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                            item.revUnitDiscount = item.unitDiscount;

                        }


                        if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                            item.tempUitMRP = 0;
                            item.tempUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempUnitDiscount = item.unitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                item.unitDiscount = 0;
                            }

                            item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));




                            item.unitMRP = item.tempUitMRP;
                            item.unitDiscount = item.tempUnitDiscount;

                        }

                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), 2);

                        if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                            item.tempUitMRP = 0;
                            item.tempRevUnitDiscount = 0;

                            item.tempUitMRP = item.unitMRP;
                            item.tempRevUnitDiscount = item.revUnitDiscount;

                            if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                item.unitMRP = 0;
                            }
                            if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                item.revUnitDiscount = 0;
                            }

                            item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                            item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                            item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), 2);


                            item.unitMRP = item.tempUitMRP;
                            item.revUnitDiscount = item.tempRevUnitDiscount;

                        }

                    });
                }
            }

            $scope.precisionRound = function (number, precision) {
                var factor = Math.pow(10, precision);
                return Math.round(number * factor) / factor;
            }

            $scope.downloadTemplate = function () {

                var name = '';
                if ($scope.auctionItem.isDiscountQuotation == 2) {
                    if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                        name = 'MARGIN_QUOTATION_' + $scope.reqId;
                    }
                    else {
                        name = 'MARGIN_REV_QUOTATION_' + $scope.reqId;
                    }
                } else {
                    name = 'UNIT_PRICE_QUOTATION_' + $scope.reqId;
                }


                reportingService.downloadTemplate(name, userService.getUserId(), $scope.reqId);
            };

            $scope.getReport = function (name) {
                reportingService.downloadTemplate(name + "_" + $scope.reqId, userService.getUserId(), $scope.reqId);
            }

            $scope.getLotReport = function (name,reqID) {
                PRMLotReqService.GetLotDetailReport(0, reqID)
                    .then(function (response) {

                    })
            }

            $scope.EnableMarginFields = function (val) {
                var params = {
                    isEnabled: val,
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken()
                }
                auctionsService.EnableMarginFields(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    })
            }

            $scope.numToDecimal = function (val, decimalRound) {

                val = parseFloat(val);
                return val.toFixed(decimalRound);
            }

            $scope.GetReqDataPriceCap = function () {
                var params = {
                    'reqid': $scope.reqId,
                    'sessionid': userService.getUserToken()
                };

                if ($scope.isCustomer && $scope.auctionItem.reqType && $scope.auctionItem.reqType === 'PRICE_CAP') {
                    priceCapServices.GetReqData(params)
                        .then(function (response) {
                            $scope.priceCapData = response;

                            if ($scope.priceCapData && $scope.priceCapData.listRequirementItems && $scope.priceCapData.listRequirementItems.length > 0) {
                                $scope.priceCapData.listRequirementItems.forEach(function (item, index) {
                                    item.Gst = item.cGst + item.sGst + item.iGst;
                                })
                            }

                            $scope.priceCapCalculations();
                        });
                }
            };

            $scope.GetReqDataPriceCap();

            $scope.SavePriceCapItemLevel = function () {
                var params = {
                    reqID: $scope.reqId,
                    listReqItems: $scope.priceCapData.listRequirementItems,
                    price: $scope.formRequest.priceCapValue,
                    isDiscountQuotation: $scope.auctionItem.isDiscountQuotation,
                    sessionID: userService.getUserToken()
                }
                priceCapServices.SavePriceCap(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Saved Successfully.", "success");
                            if ($scope.auctionItem.status == 'STARTED') {
                                $scope.recalculate('', 0, true);
                            }
                            location.reload();
                        }
                        else {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                    })
            }

            $scope.GetDifferentialFactorPrice = function (vendorID) {



                $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {

                    if (vendorID == vendor.vendorID) {
                        vendor.changedDF = {
                            'background-color': 'yellow'
                        }
                    }
                    var QP = vendor.QP;
                    var BP = vendor.BP;
                    var RP = vendor.RP;
                    var DIFFERENTIAL_FACTOR = vendor.DIFFERENTIAL_FACTOR;
                    vendor.DF_REV_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);

                    if ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED') {
                        vendor.DF_REVISED_VENDOR_TOTAL_PRICE = parseFloat(vendor.revVendorTotalPrice) + parseFloat(DIFFERENTIAL_FACTOR);


                        vendor.QP = parseFloat(vendor.initialPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.BP = parseFloat(vendor.totalPriceIncl) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                        vendor.RP = parseFloat(vendor.revVendorTotalPrice) + parseFloat(vendor.DIFFERENTIAL_FACTOR);
                    }
                })

            };


            $scope.SaveDifferentialFactor = function (auctionVendor) {


                var params = {
                    'vendorID': auctionVendor.vendorID,
                    'reqID': $scope.auctionItem.requirementID,
                    'sessionID': userService.getUserToken(),
                    'customerID': userService.getUserId(),
                    'value': auctionVendor.DIFFERENTIAL_FACTOR,
                    'reason': ''
                };

                auctionsService.SaveDifferentialFactor(params).then(function (req) {
                    if (req.errorMessage == '') {
                        growlService.growl('Saved Successfully.', "success");

                        var parties = params.reqID + "$" + userService.getUserId() + "$" + userService.getUserToken() + "$" + "SAVE_DIFFERENTIAL_FACTOR";

                        $scope.invokeSignalR('CheckRequirement', parties);
                        //swal({
                        //    title: "Done!",
                        //    text: 'Saved Successfully',
                        //    type: "success",
                        //    showCancelButton: false,
                        //    confirmButtonColor: "#DD6B55",
                        //    confirmButtonText: "Ok",
                        //    closeOnConfirm: true
                        //},
                        //    function () {
                        //        location.reload();
                        //    });

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            }

            //$scope.priceCapCalculations = function () {
            //    $scope.priceCapData.forEach(function (item, index) {

            //    })
            //}

            $scope.priceCapCalculations = function () {

                if ($scope.auctionItem.isDiscountQuotation == 1 || $scope.auctionItem.isDiscountQuotation == 0) {

                    $scope.formRequest.priceCapValue = 0;
                    if ($scope.priceCapData && $scope.priceCapData.listRequirementItems) {
                        $scope.priceCapData.listRequirementItems.forEach(function (item, index) {

                            //if ($scope.priceCapData.auctionVendors[0].isQuotationRejected != 0) {
                            //    item.revUnitDiscount = item.unitDiscount;
                            //}

                            item.cGst = item.Gst / 2;
                            item.sGst = item.Gst / 2;

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.revUnitDiscount = item.unitDiscount;
                                    item.tempUitMRP = 0;
                                    item.tempUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempUnitDiscount = item.unitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.unitDiscount == undefined || item.unitDiscount <= 0) {
                                        item.unitDiscount = 0;
                                    }

                                    item.unitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.unitDiscount));

                                    item.unitMRP = item.tempUitMRP;
                                    item.unitDiscount = item.tempUnitDiscount;


                                    if ($scope.auctionItem.isDiscountQuotation == 1) {
                                        item.reductionPrice = (item.unitMRP - item.unitPrice) * item.productQuantity;
                                        item.reductionPrice = $scope.precisionRound(parseFloat(item.reductionPrice), 2);
                                    }

                                }


                                ////////////////////
                                item.sGst = item.cGst;
                                var tempUnitPrice = item.unitPrice;
                                var tempCGst = item.cGst;
                                var tempSGst = item.sGst;
                                var tempIGst = item.iGst;
                                if (item.unitPrice == undefined || item.unitPrice <= 0) {
                                    item.unitPrice = 0;
                                };

                                if ($scope.auctionItem.isDiscountQuotation == 0) {
                                    item.revUnitPrice = item.unitPrice;
                                }

                                item.itemPrice = item.unitPrice * item.productQuantity;


                                item.itemPrice = item.itemPrice + ((item.itemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.unitPrice = tempUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                };
                                /////////////////////

                            }

                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'Negotiation Ended' || $scope.auctionItem.status == 'STARTED' || $scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {

                                if ($scope.auctionItem.isDiscountQuotation == 1) {
                                    item.tempUitMRP = 0;
                                    item.tempRevUnitDiscount = 0;

                                    item.tempUitMRP = item.unitMRP;
                                    item.tempRevUnitDiscount = item.revUnitDiscount;

                                    if (item.unitMRP == undefined || item.unitMRP <= 0) {
                                        item.unitMRP = 0;
                                    }
                                    if (item.revUnitDiscount == undefined || item.revUnitDiscount <= 0) {
                                        item.revUnitDiscount = 0;
                                    }

                                    item.revUnitPrice = item.unitMRP - ((item.unitMRP / 100) * (item.revUnitDiscount));

                                    item.revReductionPrice = (item.unitMRP - item.revUnitPrice) * item.productQuantity;

                                    item.revReductionPrice = $scope.precisionRound(parseFloat(item.revReductionPrice), 2);


                                    item.unitMRP = item.tempUitMRP;
                                    item.revUnitDiscount = item.tempRevUnitDiscount;
                                }

                                ////////////////////////
                                var tempRevUnitPrice = item.revUnitPrice;
                                var tempCGst = item.cGst;
                                var tempSGst = item.sGst;
                                var tempIGst = item.iGst;

                                if (item.revUnitPrice == undefined || item.revUnitPrice <= 0) {
                                    item.revUnitPrice = 0;
                                };

                                item.revitemPrice = item.revUnitPrice * item.productQuantity;

                                if (item.cGst == undefined || item.cGst <= 0) {
                                    item.cGst = 0;
                                };
                                if (item.sGst == undefined || item.sGst <= 0) {
                                    item.sGst = 0;
                                };
                                if (item.iGst == undefined || item.iGst <= 0) {
                                    item.iGst = 0;
                                };

                                item.revitemPrice = item.revitemPrice + ((item.revitemPrice / 100) * (item.cGst + item.sGst + item.iGst));

                                item.revUnitPrice = tempRevUnitPrice;
                                item.cGst = tempCGst
                                item.sGst = tempSGst;
                                item.iGst = tempIGst;

                                if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                                    // $scope.gstValidation = true;
                                    // not mandatory
                                }
                                //////////////////////

                            }

                            if (item.revitemPrice > 0) {
                                $scope.formRequest.priceCapValue = parseFloat($scope.formRequest.priceCapValue);
                                $scope.formRequest.priceCapValue += item.revitemPrice;
                            }


                            //item.unitPrice = $scope.numToDecimal(item.unitPrice, 2);
                            item.itemPrice = $scope.numToDecimal(item.itemPrice, 2);

                            //item.revUnitPrice = $scope.numToDecimal(item.revUnitPrice, 2);
                            item.revitemPrice = $scope.numToDecimal(item.revitemPrice, 2);


                            $scope.formRequest.priceCapValue = $scope.numToDecimal($scope.formRequest.priceCapValue, 2);


                        });
                    }
                }
            }

            $scope.removeAttach = function (index) {
                $scope.multipleAttachmentsList.splice(index, 1);
                $scope.auctionItem.auctionVendors[0].multipleAttachmentsList.splice(index, 1);
            }

            $scope.regretHandle = function (item) {
                if (item.isRegret) {
                    item.unitPrice = 0;
                    item.Gst = 0;
                    item.sGst = 0;
                    item.cGst = 0;
                    item.iGst = 0;
                    //item.regretComments = '';
                    item.itemFreightCharges = 0;
                    item.itemFreightTAX = 0;

                    if (item.isCoreProductCategory && item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {

                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {

                            subItem.SPECIFICATION = '';

                        });

                        item.productQuotationTemplateJson = '';
                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                    }


                } else {
                    item.regretComments = '';
                }


                if ($scope.totalprice == 0) {

                    $("#totalprice").val(0);
                    $("#revtotalprice").val(0);

                    $("#packingCharges").val(0);
                    $("#packingChargesTaxPercentage").val(0);
                    $("#packingChargesWithTax").val(0);
                    $("#revpackingCharges").val(0);
                    $("#revpackingChargesWithTax").val(0);

                    $("#installationCharges").val(0);
                    $("#installationChargesTaxPercentage").val(0);
                    $("#installationChargesWithTax").val(0);
                    $("#revinstallationCharges").val(0);
                    $("#revinstallationChargesWithTax").val(0);

                    $("#freightCharges").val(0);
                    $("#freightChargesTaxPercentage").val(0);
                    $("#freightChargesWithTax").val(0);
                    $("#revfreightCharges").val(0);
                    $("#revfreightChargesWithTax").val(0);

                    $("#vendorBidPrice").val(0);
                    $("#revvendorBidPrice").val(0);

                } 

                
            }

            $scope.showVendorRegretDetails = function (vendor) {
                $scope.regretDetails = vendor.listRequirementItems;
            };

            //#CB-0-2018-12-05
            $scope.goToCbCustomer = function (vendorID) {
                var url = $state.href("cb-customer", { "reqID": $scope.reqId, "vendorID": vendorID });
                window.open(url, '_blank');
            };

            //$scope.UpdateCBTime = function () {

            //    var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
            //    var m = moment(ts);
            //    var date = new Date(m);
            //    var milliseconds = parseInt(date.getTime() / 1000.0);
            //    var cbEndTime = "/Date(" + milliseconds + "000+0530)/";

            //    var params = {
            //        cbEndTime: cbEndTime,
            //        userID: userService.getUserId(),
            //        reqID: $scope.reqId,
            //        sessionID: userService.getUserToken()
            //    }

            //    auctionsService.UpdateCBTime(params)
            //        .then(function (response) {
            //            if (response.errorMessage != "") {
            //                growlService.growl(response.errorMessage, "inverse");
            //            } else {
            //                $scope.recalculate();
            //                growlService.growl('Saved Successfully', "inverse");
            //            }
            //        });
            //};

            $scope.UpdateCBTime = function () {




                var CB_END_TIME = $("#CB_END_TIME").val(); //$scope.startTime; //Need fix on this.

                if (CB_END_TIME && CB_END_TIME != null && CB_END_TIME != "") {

                    //var ts = moment($scope.startTime, "DD-MM-YYYY HH:mm").valueOf();
                    var ts = moment(CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var auctionStartDate = new Date(m);
                    auctionsService.getdate()
                        .then(function (getdateResponse) {
                            //var CurrentDate = moment(new Date(parseInt(getdateResponse.substr(6))));

                            var CurrentDateToLocal = userService.toLocalDate(getdateResponse);

                            var ts = moment(CurrentDateToLocal, "DD-MM-YYYY HH:mm").valueOf();
                            var m = moment(ts);
                            var deliveryDate = new Date(m);
                            var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                            var CurrentDateToTicks = "/Date(" + milliseconds + "000+0530)/";

                            var CurrentDate = moment(new Date(parseInt(CurrentDateToTicks.substr(6))));

                            if (CurrentDate >= auctionStartDate) {
                                $scope.Loding = false;
                                swal("Done!", "End time should be greater than current time.", "error");
                                return;
                            }

                            swal({
                                title: "Are you sure?",
                                text: "Your negotiation process will be automatically closed at " + $scope.formRequest.CB_END_TIME + ".",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#F44336",
                                confirmButtonText: "OK",
                                closeOnConfirm: true
                            }, function () {

                                var ts = moment($scope.formRequest.CB_END_TIME, "DD-MM-YYYY HH:mm").valueOf();
                                var m = moment(ts);
                                var date = new Date(m);
                                var milliseconds = parseInt(date.getTime() / 1000.0);
                                // // #INTERNATIONALIZATION-0-2019-02-12
                                var cbEndTime = "/Date(" + userService.toUTCTicks(CB_END_TIME) + "+0530)/";
                                //000

                                var params = {
                                    cbEndTime: cbEndTime,
                                    userID: userService.getUserId(),
                                    reqID: $scope.reqId,
                                    sessionID: userService.getUserToken()
                                }

                                auctionsService.UpdateCBTime(params)
                                    .then(function (response) {
                                        if (response.errorMessage == "") {
                                           
                                            $scope.recalculate('UPDATE_CB_TIME_CUSTOMER', 0, false);
                                            growlService.growl('Saved Successfully', "inverse");
                                            
                                        } else {
                                             growlService.growl(response.errorMessage, "inverse");
                                           // swal("Error", response.errorMessage, 'error');
                                        }
                                    });
                            })


                        })
                }
                else {
                    swal("Done!", "Please enter the date and time to update End Time.", "error");
                    //alert("Please enter the date and time to update Start Time to.");
                }


            };

            $scope.CBStopQuotations = function (value) {
                
                var params = {
                    stopCBQuotations: value,
                    userID: userService.getUserId(),
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken()
                }

                swal({
                    title: "Are you sure?",
                    text: "You will not receive any further quotations.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.CBStopQuotations(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('CB_STOP_QUOTATIONS_CUSTOMER', 0, false);
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
                })
            };

            $scope.CBMarkAsComplete = function (value) {

                var params = {
                    isCBCompleted: value,
                    userID: userService.getUserId(),
                    reqID: $scope.reqId,
                    sessionID: userService.getUserToken()
                }

            /*    swal({
                    title: "Once requirement is marked as completed user will not have privileges to edit and negotiate with vendors.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {*/
                    auctionsService.CBMarkAsComplete(params)
                        .then(function (response) {
                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                                $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                                $scope.getrevisedquotations();
                                growlService.growl('Saved Successfully', "inverse");
                            }
                        });
              //  })
            };

            $scope.changeBiddingType = function (value) {
                if (value == '1' || value == '2') {
                    $scope.IS_CB_ENABLED = true;
                    if (value == '2') {
                        $scope.IS_CB_NO_REGRET = true;
                    } else {
                        $scope.IS_CB_NO_REGRET = false;
                    }
                }
                if (value == '0') {
                    $scope.IS_CB_ENABLED = false;
                }
            }

            $scope.resetValues = function () {
                    $("#packingCharges").val($scope.packingCharges);
                    $("#packingChargesTaxPercentage").val($scope.packingChargesTaxPercentage);
                    $("#packingChargesWithTax").val($scope.packingChargesWithTax);
                    $("#revpackingCharges").val($scope.revpackingCharges);
                    $("#revpackingChargesWithTax").val($scope.revpackingChargesWithTax);

                    $("#installationCharges").val($scope.installationCharges);
                    $("#installationChargesTaxPercentage").val($scope.installationChargesTaxPercentage);
                    $("#installationChargesWithTax").val($scope.installationChargesWithTax);
                    $("#revinstallationCharges").val($scope.revinstallationCharges);
                    $("#revinstallationChargesWithTax").val($scope.revinstallationChargesWithTax);

                    $("#freightCharges").val($scope.freightCharges);
                    $("#freightChargesTaxPercentage").val($scope.freightChargesTaxPercentage);
                    $("#freightChargesWithTax").val($scope.freightChargesWithTax);
                    $("#revfreightCharges").val($scope.revfreightCharges);
                    $("#revfreightChargesWithTax").val($scope.revfreightChargesWithTax);
            }

            auctionsService.GetCompanyConfiguration($scope.compID, "INCO", userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyINCOTerms = unitResponse;
                });
            
            $scope.shouldShow = function (permissionLevel) {
                // put your authorization logic here
                if (permissionLevel) {
                    return true;
                } else {
                    return false;
                }
            }


            $scope.markAsCompleteREQ = function (reqId,value,isMACForCBOrNot) {
                var params = {
                    isREQCompleted: value,
                    userID: userService.getUserId(),
                    reqID: reqId,
                    sessionID: userService.getUserToken()
                }

                swal({
                    title: "Once requirement is marked as completed user will not have privileges to edit this requirement.",
                    text: "Kindly confirm.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                    auctionsService.markAsCompleteREQ(params)
                        .then(function (response) {

                            //$scope.CBMarkAsComplete(1);

                            if (response.errorMessage != "") {
                                growlService.growl(response.errorMessage, "inverse");
                            } else {
                             //   $scope.recalculate('CB_MARK_AS_COMPLETED_CUSTOMER', 0, false);
                             //   $scope.getrevisedquotations();
                                growlService.growl('Saved Successfully', "success");
                                location.reload();
                                //auctionItem.IS_CB_ENABLED
                                if (isMACForCBOrNot == true) {
                                    $scope.CBMarkAsComplete(1);
                                } else {

                                }
                                
                              
                            }


                        });
                })
            }








            $scope.GetItemUnitPrice = function (isPerUnit, templateIndex) {
                //if (itemTemplate && itemTemplate.HAS_PRICE <= 0) {
                //    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                //        if (item.productQuotationTemplateArray != null && item.productQuotationTemplateArray != undefined && item.productQuotationTemplateArray.length > 0) {
                //            item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                //        } else {
                //            item.productQuotationTemplateJson = '';
                //        }
                //    });
                //    return;
                //}
                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (isPerUnit) {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    //UNIT_PRICE = BULK_PRICE * CONSUMPTION;
                                    //REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION;
                                    if (CONSUMPTION > 0) {
                                        subItem.BULK_PRICE = UNIT_PRICE / CONSUMPTION;
                                    }
                                    else {
                                        subItem.BULK_PRICE = 0;
                                    }
                                    if (REV_CONSUMPTION > 0) {
                                        subItem.REV_BULK_PRICE = REV_UNIT_PRICE / REV_CONSUMPTION;
                                    }
                                    else {
                                        subItem.REV_BULK_PRICE = 0;
                                    }
                                    //subItem.UNIT_PRICE = UNIT_PRICE;
                                    //subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;

                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;

                                    //}
                                }
                            });
                        }
                    }
                    else {
                        if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                            item.unitPrice = 0;
                            item.revUnitPrice = 0;
                            item.UNIT_PRICE = 0;
                            item.REV_UNIT_PRICE = 0;
                            item.margin15 = 0;
                            item.revmargin15 = 0;
                            item.margin3 = 0;
                            item.revmargin3 = 0;

                            item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                                if (templateIndex == subItemIndex || templateIndex == -1) {
                                    if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0) {
                                        subItem.REV_BULK_PRICE = subItem.BULK_PRICE;
                                        subItem.REV_CONSUMPTION = subItem.CONSUMPTION;
                                        subItem.REV_TAX = subItem.TAX;
                                        subItem.REV_UNIT_PRICE = subItem.UNIT_PRICE;
                                    }

                                    var BULK_PRICE = 0;
                                    if (parseFloat(subItem.BULK_PRICE) > 0) {
                                        BULK_PRICE = parseFloat(subItem.BULK_PRICE);
                                    }
                                    var REV_BULK_PRICE = 0;
                                    if (parseFloat(subItem.REV_BULK_PRICE) > 0) {
                                        REV_BULK_PRICE = parseFloat(subItem.REV_BULK_PRICE);
                                    }

                                    var CONSUMPTION = 0;
                                    if (parseFloat(subItem.CONSUMPTION) > 0) {
                                        CONSUMPTION = parseFloat(subItem.CONSUMPTION);
                                    }
                                    var REV_CONSUMPTION = 0;
                                    if (parseFloat(subItem.REV_CONSUMPTION) > 0) {
                                        REV_CONSUMPTION = parseFloat(subItem.REV_CONSUMPTION);
                                    }

                                    var TAX = 0;
                                    if (parseFloat(subItem.TAX) > 0) {
                                        TAX = parseFloat(subItem.TAX);
                                    }
                                    var REV_TAX = 0;
                                    if (parseFloat(subItem.REV_TAX) > 0) {
                                        REV_TAX = parseFloat(subItem.REV_TAX);
                                    }

                                    var UNIT_PRICE = 0;
                                    if (parseFloat(subItem.UNIT_PRICE) > 0) {
                                        UNIT_PRICE = parseFloat(subItem.UNIT_PRICE);
                                    }
                                    var REV_UNIT_PRICE = 0;
                                    if (parseFloat(subItem.REV_UNIT_PRICE) > 0) {
                                        REV_UNIT_PRICE = parseFloat(subItem.REV_UNIT_PRICE);
                                    }

                                    UNIT_PRICE = BULK_PRICE * CONSUMPTION + ((BULK_PRICE * CONSUMPTION) * (TAX / 100));
                                    REV_UNIT_PRICE = REV_BULK_PRICE * REV_CONSUMPTION + ((REV_BULK_PRICE * REV_CONSUMPTION) * (TAX / 100));

                                    subItem.UNIT_PRICE = UNIT_PRICE;
                                    subItem.REV_UNIT_PRICE = REV_UNIT_PRICE;


                                    //if (subItem.IS_CALCULATED == 0) {
                                    //    //item.unitPrice += UNIT_PRICE;
                                    //    //item.revUnitPrice += REV_UNIT_PRICE;
                                    //    item.UNIT_PRICE += UNIT_PRICE;
                                    //    item.REV_UNIT_PRICE += REV_UNIT_PRICE;
                                    //}
                                }
                            });
                        }
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 0) {
                                if (subItem.UNIT_PRICE > 0) {
                                    item.UNIT_PRICE += parseFloat(subItem.UNIT_PRICE);
                                }
                                if (subItem.REV_UNIT_PRICE > 0) {
                                    item.REV_UNIT_PRICE += parseFloat(subItem.REV_UNIT_PRICE);
                                }
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        //item.margin15 = (item.UNIT_PRICE / 100) * 15;
                        //item.revmargin15 = (item.REV_UNIT_PRICE / 100) * 15;

                        //item.margin3 = (item.UNIT_PRICE / 100) * 3;
                        //item.revmargin3 = (item.REV_UNIT_PRICE / 100) * 3;
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 1) {
                                subItem.UNIT_PRICE = item.UNIT_PRICE;
                                subItem.REV_UNIT_PRICE = item.REV_UNIT_PRICE;
                            }
                            if (subItem.IS_CALCULATED == 2) {
                                subItem.UNIT_PRICE = item.margin15;
                                subItem.REV_UNIT_PRICE = item.revmargin15;
                            }
                            if (subItem.IS_CALCULATED == 3) {
                                subItem.UNIT_PRICE = item.margin3;
                                subItem.REV_UNIT_PRICE = item.revmargin3;
                            }
                            if (subItem.IS_CALCULATED == 4) {
                                item.UNIT_PRICE = parseFloat(item.UNIT_PRICE) + parseFloat(subItem.UNIT_PRICE);
                                item.REV_UNIT_PRICE = parseFloat(item.REV_UNIT_PRICE) + parseFloat(subItem.REV_UNIT_PRICE);
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0 && $scope.isUnitPriceEditableForSubItems(item.productQuotationTemplateArray)) {
                        item.unitPrice = parseFloat(item.UNIT_PRICE) + parseFloat(item.margin15) + parseFloat(item.margin3);
                        item.revUnitPrice = parseFloat(item.REV_UNIT_PRICE) + parseFloat(item.revmargin15) + parseFloat(item.revmargin3);
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.IS_CALCULATED == 5) {
                                subItem.UNIT_PRICE = item.unitPrice;
                                subItem.REV_UNIT_PRICE = item.revUnitPrice;
                            }
                        });
                    }
                });

                $scope.auctionItemVendor.listRequirementItems.forEach(function (item, itemIndexs) {
                    if (item.productQuotationTemplateArray && item.productQuotationTemplateArray.length > 0) {
                        item.productQuotationTemplateArray.forEach(function (subItem, subItemIndex) {
                            if (subItem.SPECIFICATION) {
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\'/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\"/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/[`^_|\?;:'",<>\{\}\[\]\\\/]/gi, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/(\r\n|\n|\r)/gm, "");
                                subItem.SPECIFICATION = subItem.SPECIFICATION.replace(/\t/g, '');
                            }
                        });

                        item.productQuotationTemplateJson = JSON.stringify(item.productQuotationTemplateArray);
                        
                    } else {
                        item.productQuotationTemplateJson = '';
                    }
                });

            };


            $scope.VendorItemlevelPrices = [];
            $scope.showVendorItemlevelPrices = function (vendor) {
                $scope.disableSubmitAllItemApproval = true;
                $scope.selectedVendorForItemLevelPrices = vendor;
                $scope.VendorItemlevelPrices = [];
                $scope.VendorItemlevelPrices = vendor.listRequirementItems;

                $scope.VendorItemlevelPrices.sort(function (a, b) {
                    return b.isCoreProductCategory - a.isCoreProductCategory;
                })

                $scope.VendorItemlevelPrices.map((element) => {
                    return element.otherProperties = vendor.otherProperties;
                });
            };

            function hasDuplicates(arr) {
                var counts = [];

                for (var i = 0; i <= arr.length - 1; i++) {
                    if (counts[arr[i].productIDorName] === undefined) {
                        counts[arr[i].productIDorName] = 1;
                    } else {
                        return true;
                    }
                }
                return false;
            }

            $scope.createLot = function (requirementId)
            {
                $scope.checkIsLotValid(requirementId);
            };

            $scope.checkIsLotValid = function (reqId) {
                PRMLotReqService.isLotValid(reqId)
                    .then(function (response) {
                        if (response) {
                            if ($scope.starreturn) {
                                $scope.Loding = false;
                                swal("Not Allowed", "You are not allowed to create lot until all the Vendor Quotations are validated. (They should be either approved/rejected). The following vendors quotations are yet to be validated. (" + $scope.notviewedcompanynames + ")", "error");
                                return;
                            }

                            var items = $scope.auctionItem.listRequirementItems.filter(function (item) {
                                return (item.isCoreProductCategory === 1);
                            });
                            let findDuplicates = hasDuplicates(items);
                            if (findDuplicates) {
                                swal("Error!", "Cannot create lot with duplicate items", "error");
                                return;
                            } else {
                                swal({
                                    title: "Once Lot Requirement is Created Vendors cannot submit Quotations for this Requirement.",
                                    text: "Kindly confirm.",
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#F44336",
                                    confirmButtonText: "OK",
                                    closeOnConfirm: true
                                }, function () {
                                        PRMLotReqService.lotdetails(0, reqId)
                                        .then(function (response) {
                                            if (response) {
                                                if (response) {
                                                    $state.go("save-lot-details", { "Id": response.LotId, "reqId": reqId });
                                                } 
                                            }

                                           
                                        });
                                });
                            }
                        } else {
                            swal({
                                title: "Error",
                                text: "Cannot create lot as Negotiation is ended",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            }, function () {
                                $scope.getData();
                            });
                        }

                    });
                

            }

            $scope.isValidLotBidding = function () {
                var vendors = $scope.auctionItem.auctionVendors.filter(function (vend) {
                    return vend.companyName != "PRICE_CAP";
                });
                if (vendors.length == 0 || vendors[0].quotationUrl == "" || vendors[0].isQuotationRejected != 0 || vendors.length == 1 || vendors[1].quotationUrl == "" || vendors[1].isQuotationRejected != 0 || $scope.starttimecondition1 != 0 || $scope.starttimecondition2 != 0) {
                    return false;
                }
                else {
                    return true;
                }
            };

            $scope.handleSubItemsVisibility = function (product) {
                product.subIemsColumnsVisibility = {
                    hideSPECIFICATION: true,
                    hideQUANTITY: true,
                    hideTAX: true,
                    hidePRICE: true
                };
                var productQuotationTemplateArray = product.productQuotationTemplateArray;
                if (productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_SPECIFICATION === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideSPECIFICATION = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_QUANTITY === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideQUANTITY = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hideTAX = false;
                    }

                    items = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if (items && items.length > 0) {
                        product.subIemsColumnsVisibility.hidePRICE = false;
                    }

                    //product.subIemsColumnsVisibility = _.clone($scope.subIemsColumnsVisibility);
                }
            };

            $scope.isUnitPriceEditableForSubItems = function(productQuotationTemplateArray) {
                var isDisabled = false;
                if(productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_PRICE === 1);
                    });

                    if(hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };

            $scope.isTaxFieldEditableForSubItems = function(productQuotationTemplateArray) {
                var isDisabled = false;
                if(productQuotationTemplateArray && productQuotationTemplateArray.length > 0) {
                    var hasOnlySpecificationItems = productQuotationTemplateArray.filter(function (template) {
                        return (template.HAS_TAX === 1);
                    });

                    if(hasOnlySpecificationItems && hasOnlySpecificationItems.length > 0) {
                        isDisabled = true;
                    }
                }

                return isDisabled;
            };
            
            $scope.getCustomFieldsByModuleId = function () {
                 params = {
                    "compid": userService.getUserCompanyId(),
                    "fieldmodule": 'REQUIREMENT',
                    "moduleid": $stateParams.Id,
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetCustomFieldsByModuleId(params)
                .then(function (response) {
                    $scope.selectedcustomFieldList = response;                   
                });
            };

            //$scope.getCustomFieldsByModuleId(); //Temporarly disabled

            $scope.IsAllChecked = false;
            $scope.isSaveEmailDisabled = true;
           // $scope.disableToOtherUsers = [];



            $scope.SendEmailLinksToVendors = function (emailToVendors) {

                $scope.vendors = [];

                emailToVendors.forEach(function (item, index) {
                    if (item.isEmailSent == true) {
                        if (item.WF_ID > 0) {
                            item.VendorObj =
                                {
                                    vendorID: item.vendorID,
                                    WF_ID: item.WF_ID
                                }
                            item = item.VendorObj;
                            $scope.vendors.push(item);
                        } else {
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (mand, mandIndex) {
                                if (mand.vendorID == item.vendorID) {
                                    mand.showingWorkflowErrorMessage = 'Please Select Workflow';
                                }
                            })
                            return;
                        }
                    }
                    
                });
                
                if ($scope.vendors.length <= 0) {
                    growlService.growl("Mandatory fields are required.", "inverse");
                    return;
                }

                
                var params =
                {
                    userID: userService.getUserId(),
                    vendorIDs: $scope.vendors,
                    sessionID: userService.getUserToken()
                }

                auctionsService.shareemaillinkstoVendors(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            angular.element('#showVendors').modal('hide');
                            growlService.growl("Email Has Been Send To The Selected Vendors ", "success");
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                item.isEmailSent = false;
                                item.WF_ID = 0;
                                item.showingWorkflowErrorMessage = '';
                            });
                            $scope.IsAllChecked = false;

                         //   $scope.disableToOtherUsers = angular.copy($scope.vendors);
                        }
                    });

            };



            $scope.showErrorMessagesForVendors = function () {
                $scope.disableVendors = [];
                var vendorsString = '';
                vendorsString = $scope.existingVendors.join(',');

                var params =
                {
                    userID: userService.getUserId(),
                    vendorIDs: vendorsString,
                    sessionID: userService.getUserToken()
                }

                auctionsService.GetSharedLinkVendors(params)
                    .then(function (response) {
                        //$scope.auctionItemTemporary.emailLinkVendors
                        $scope.disableVendors = response;

                        $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                            item.isDisable = false;
                            item.message = '';
                            item.errorMessage = '';
                            $scope.disableVendors.forEach(function (item1, index1) {
                                if (item.vendorID == item1.vendorID)
                                {
                                    item.isDisable = true;
                                    item.message = 'Vendor has already received the registration link by'; 
                                    item.errorMessage = item1.errorMessage;
                                }
                            })
                        })
                    });

            }



            $scope.selectAll = function (value) {
                if (value) {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = true;
                        $scope.disableVendors.forEach(function (item1, index1) {
                            if (item.vendorID == item1.vendorID) {
                                item.isEmailSent = false;
                            }
                            return;
                        })
                    });
                } else {
                    $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                        item.isEmailSent = false;
                    });
                    //$scope.isSaveEmailDisabled = true;
                }
                
            };


            $scope.selectedVendor = function () {
                //console.log($scope.vendors.length);
            }


            /*region start WORKFLOW*/

            $scope.getWorkflows = function () {
                workflowService.getWorkflowList()
                    .then(function (response) {
                        $scope.workflowList = [];
                        $scope.workflowListTemp = response;
                        $scope.workflowListTemp.forEach(function (item, index) {
                            if (item.WorkflowModule == $scope.WorkflowModule) {
                                $scope.workflowList.push(item);
                                
                            }
                        });

                        if ($scope.auctionItemTemporary) {
                            $scope.auctionItemTemporary.emailLinkVendors.forEach(function (item, index) {
                                item.listWorkflows = $scope.workflowList;
                            });
                        }
                        
                        if (userService.getUserObj().isSuperUser) {
                            $scope.workflowList = $scope.workflowList;
                        }
                        else {
                            $scope.workflowList = $scope.workflowList.filter(function (item) {
                                return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                            });
                        }



                    });
            };
            

            /*region end WORKFLOW*/

            $scope.fieldValidation = function () {
                if ($scope.auctionItem.auctionVendors.length > 0) {
                    $scope.auctionItem.auctionVendors.forEach(function (vendor, index) {
                        if (vendor.INCO_TERMS && !$scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            $scope.vendorIncoTerms[vendor.INCO_TERMS] = [];
                            auctionsService.getIncotermProductConfig(vendor.INCO_TERMS, $scope.currentSessionId)
                                .then(function (response) {
                                    $scope.vendorIncoTerms[vendor.INCO_TERMS] = response;
                                    $scope.auctionItem.auctionVendors.forEach(function (vendor1, index) {
                                        if (vendor1.INCO_TERMS === vendor.INCO_TERMS) {
                                            validateIncoTerms(response);
                                        }
                                    });
                                });
                        } else if (vendor.INCO_TERMS && $scope.vendorIncoTerms[vendor.INCO_TERMS]) {
                            validateIncoTerms($scope.vendorIncoTerms[vendor.INCO_TERMS]);
                        }
                    });
                }
            };

            function validateIncoTerms(incoTerms) {
                incoTerms.forEach(function (incoItem, itemIndexs) {
                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                        if (item.isCoreProductCategory == 0 && item.catalogueItemID == incoItem.ProductId) {
                            if ($scope.userIsOwner && incoItem.IS_CUSTOMER_EDIT) {
                                item.isEdit = false;
                            } else if (!$scope.userIsOwner && incoItem.IS_VENDOR_EDIT && $scope.totalprice > 0) {
                                item.isEdit = false;
                            } else {
                                item.isEdit = true;
                            }
                        } else if (item.isCoreProductCategory) {
                            item.isEdit = false;
                        }

                    });
                });
            }


            $scope.getDFTotalPrice = function (qp, oc) {
                qp = parseFloat(qp);
                oc = parseFloat(oc);
                var result = parseFloat(qp + oc).toFixed(2);
                return result;
            };

            $scope.detectLinks = function urlify(text) {
                if (text) {
                    var urlRegex = /(https?:\/\/[^\s]+)/g;
                    return text.replace(urlRegex, function (url) {
                        return '<a target="_blank" href="' + url + '">' + url + '</a>';
                    });
                }
                
            };

            $scope.saveItemLevelCeilingPrice = function () {
                var updatedItems = [];
                $scope.auctionItem.listRequirementItems.forEach(function (item, itemIndexs) {
                    updatedItems.push({
                        ceilingPrice: item.ceilingPrice,
                        itemID: item.itemID,
                        requirementID: item.requirementID
                    });
                });

                var params = {
                    user: userService.getUserId(),
                    requirementitemlist: updatedItems,
                    sessionid: userService.getUserToken()
                };

                auctionsService.saveRequirementItemList(params)
                    .then(function (response) {
                        if (response.errorMessage == "") {
                            growlService.growl("Details have been saved successfully", "inverse");
                            location.reload();
                        }
                    });
            };

            $scope.getItemCeilingPrie = function (itemId) {
                let ceilingPrice = 0;
                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                    return reqItem.itemID === itemId;
                });

                if (requirementItem && requirementItem.length > 0) {
                    ceilingPrice = requirementItem[0].ceilingPrice;
                }

                return ceilingPrice;
            };

            $scope.validateCeilingPrice = function (product) {
                //let currentRevPrice = product.revUnitPriceOld;
                //let ceilingPrice = $scope.getItemCeilingPrie(product.itemID);
                //if (ceilingPrice && product.revUnitPrice) {
                //    if (+product.revUnitPrice > ceilingPrice) {
                //        swal("Error!", "Your item revised amount should be less than Item Ceiling amount " + ceilingPrice, 'error');
                //        product.revUnitPrice = currentRevPrice;
                //    }
                //}
            };

            $scope.hasOtherCharges = function () {
                let hasOtherCharges = false;
                let vendors = $scope.auctionItem.requirementVendorsList ? $scope.auctionItem.requirementVendorsList : $scope.auctionItem.auctionVendors;
                //if ($scope.formRequest.rankLevel == 'REQUIREMENT') {
                    
                //}
                if ($scope.auctionItem && vendors && vendors.length > 0) {
                    let otherChargesVendor = vendors.filter(function (vendor) {
                        return vendor.DIFFERENTIAL_FACTOR > 0;
                    });

                    if (otherChargesVendor && otherChargesVendor.length > 0) {
                        hasOtherCharges = true;
                    }
                }

                return hasOtherCharges;
            };


            $scope.isValidCeilingAccept = function () {
                let isValid = false;
                if ($scope.auctionItem) {
                    let vendors = $scope.auctionItem.requirementVendorsList ? $scope.auctionItem.requirementVendorsList : $scope.auctionItem.auctionVendors;
                    if (vendors && vendors.length > 0) {
                        let currentVendor = vendors.filter(function (vendor) {
                            return +vendor.vendorID === +userService.getUserId();
                        });

                        if (currentVendor && currentVendor.length > 0 && currentVendor[0].listRequirementItems && currentVendor[0].listRequirementItems.length > 0) {
                            currentVendor[0].listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                                var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                                    return reqItem.itemID === vendorItem.itemID;
                                });

                                if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice > requirementItem[0].ceilingPrice) {
                                    isValid = true;
                                }
                            });
                        }
                    }
                }

                return isValid;
            };

            $scope.acceptCeilingPrices = function () {
                swal({
                    title: "Accept Ceiling Price ?",
                    text: "Please click to accept Ceiling Prices for all items in requirement.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "OK",
                    closeOnConfirm: true
                }, function () {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (vendorItem, itemIndexs) {
                            var requirementItem = $scope.auctionItem.listRequirementItems.filter(function (reqItem) {
                                return reqItem.itemID === vendorItem.itemID;
                            });

                            if (requirementItem && requirementItem.length > 0 && requirementItem[0].ceilingPrice && +vendorItem.revUnitPrice > requirementItem[0].ceilingPrice) {
                                vendorItem.revUnitPriceOld = vendorItem.revUnitPrice;
                                vendorItem.revUnitPrice = requirementItem[0].ceilingPrice;
                                $scope.unitPriceCalculation('PRC');
                                $scope.validateCeilingPrice(vendorItem);
                                $scope.makeBidUnitPriceValidation(vendorItem.revUnitPrice, vendorItem.TemperoryRevUnitPrice, vendorItem.productIDorName);
                                $scope.getRevTotalPrice($scope.revfreightCharges, $scope.revtotalprice, $scope.revpackingChargesWithTax, $scope.revpackingCharges, $scope.revinstallationCharges);
                            }
                        });

                        $scope.makeaBid1();
                });
            };

            $scope.GetCompanyGSTInfo = function () {
                let params = {
                    companyId: userService.getUserCompanyId()
                };
                auctionsService.getCompanyGSTInfo(params)
                    .then(function (response) {
                        $scope.companyGSTInfo = response;
                    });
            };

            if (!$scope.isCustomer) {
                $scope.GetCompanyGSTInfo();
            }

            $scope.itemQuotationApproval = function (item, vendor) {
                var params = {
                    'vendorId': vendor.vendorID,
                    'reqId': $scope.auctionItem.requirementID,
                    'sessionId': $scope.currentSessionId,
                    'customerId': $scope.currentUserId,
                    'itemId': item.itemID,
                    'value': item.isItemQuotationRejected,
                    'comment': item.itemQuotationRejectedComment
                };

                auctionsService.ItemQuotationApproval(params).then(function (req) {
                    if (!req.errorMessage) {
                        toastr.info('Successfully saved.');

                    } else {
                        swal("Error!", req.errorMessage, "error");
                    }
                });
            };

            $scope.isItemFullyApproved = function (vendor, item, checkAuctionStatus) {
                let isApproved = false;
                item.quotationApprovedColor = {
                    'background-color': '#f5b2b2'
                };

                if (vendor && vendor.isQuotationRejected === 0 && item.isItemQuotationRejected === 0) {
                    isApproved = true;
                    item.quotationApprovedColor = {};
                }

                if (checkAuctionStatus && !isApproved && $scope.auctionItem.status === 'STARTED') {
                    isApproved = true;
                    item.quotationApprovedColor = {};
                }

                return isApproved;
            };

            $scope.handleAllItemRejection = function (value) {
                $scope.disableSubmitAllItemApproval = false;
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    $scope.VendorItemlevelPrices.forEach(function (item, index) {
                        item.isItemQuotationRejected = value;
                        item.itemQuotationRejectedComment = $scope.formRequest.overallItemLevelComments;
                    });
                }
            };

            $scope.submitAllItemRejectionStatus = function (vendor) {
                if ($scope.VendorItemlevelPrices && $scope.VendorItemlevelPrices.length > 0) {
                    var params = {
                        'items': [],
                        'vendorId': vendor.vendorID,
                        'reqId': $scope.auctionItem.requirementID,
                        'customerId': $scope.currentUserId,
                        'sessionId': $scope.currentSessionId
                    };

                    $scope.VendorItemlevelPrices.forEach(function (currentItem, index) {
                        var item = {
                            itemID: currentItem.itemID,
                            isItemQuotationRejected: currentItem.isItemQuotationRejected,
                            itemQuotationRejectedComment: currentItem.itemQuotationRejectedComment
                        };

                        params.items.push(item);
                    });

                    auctionsService.ItemLevelQuotationApproval(params).then(function (req) {
                        if (!req.errorMessage) {
                            toastr.info('Successfully saved.');
                            location.reload();
                        } else {
                            swal("Error!", req.errorMessage, "error");
                        }
                    });
                }
            };

            $scope.anyItemQuotationsRejected = function () {
                let contains = false;
                if (!$scope.isCustomer && $scope.auctionItem && $scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length > 0 &&
                    $scope.auctionItem.auctionVendors[0].listRequirementItems) {
                    let vendorQuotationItems = $scope.auctionItem.auctionVendors[0].listRequirementItems;
                    if (vendorQuotationItems && vendorQuotationItems.length > 0) {
                        var quotationRejetedItems = vendorQuotationItems.filter(function (reqItem) {
                            return reqItem.isItemQuotationRejected === 1;
                        });

                        if (quotationRejetedItems && quotationRejetedItems.length > 0) {
                            contains = true;
                        }
                    }
                }

                return contains;
            };

            $scope.getRequirementSettings = function () {
                $scope.requirementSettings = [];
               // $scope.formRequest.rankLevel = 'REQUIREMENT';
                $scope.formRequest.rankLevel = 'BOTH';
                auctionsService.getRequirementSettings({ "reqid": $scope.reqId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        $scope.requirementSettings = response;
                        if ($scope.requirementSettings && $scope.requirementSettings.length > 0) {
                            var rankLevel = $scope.requirementSettings.filter(function (setting) {
                                return setting.REQ_SETTING === 'RANK_LEVEL';
                            });

                            if (rankLevel && rankLevel.length > 0) {
                                $scope.formRequest.rankLevel = rankLevel[0].REQ_SETTING_VALUE;
                            }
                        }
                    });
            };

            $scope.getRequirementSettings();

            $scope.saveRankLevelSetting = function () {
                $scope.tempScopeVariables.RankTypeSaving = true;
                let param = {
                    requirementSetting: {
                        REQ_ID: $scope.reqId,
                        REQ_SETTING: 'RANK_LEVEL',
                        REQ_SETTING_VALUE: $scope.formRequest.rankLevel ? $scope.formRequest.rankLevel : 'BOTH',
                        USER: $scope.currentUserId,
                        sessionID: $scope.currentSessionId
                    }
                };

                auctionsService.saveRequirementSetting(param)
                    .then(function (response) {
                        $scope.tempScopeVariables.RankTypeSaving = false;
                        if (response && !response.errorMessage) {
                            swal("Thanks !", "Successfully updated", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.saveLotvendorComparision = function () {
                $scope.tempScopeVariables.vendComp = true;
                //if ($scope.NegotiationSettingsValidationMessage != '') {
                //    return;
                //}
                let param = {
                    reqID: $scope.reqId,
                    userID: $scope.currentUserId,
                    minReduction: $scope.NegotiationSettings.minReductionAmount,
                    rankComp: $scope.NegotiationSettings.rankComparision,
                    sessionID: $scope.currentSessionId
                };

                PRMLotReqService.UpdateVendorRankComparision(param)
                    .then(function (response) {
                        $scope.tempScopeVariables.vendComp = false;
                        if (response.errorMessage == "") {
                            growlService.growl("Saved successfully", "success");
                        } else {
                            growlService.growl(response.errorMessage, 'inverse');
                        }
                    })
            }

            $scope.displayItemLevelRank = function () {
                let itemLevel = $scope.formRequest.rankLevel && ($scope.formRequest.rankLevel === 'BOTH' || $scope.formRequest.rankLevel === 'ITEM');
                if (!itemLevel && $scope.anyItemQuotationsRejected()) {
                    itemLevel = true;
                }

                return itemLevel;
            };

            $scope.displayRequirementLevelRank = function () {
                let display = true;
                if ($scope.formRequest.rankLevel && $scope.formRequest.rankLevel === 'ITEM') {
                    display = false;
                }

                return display;
            };

            $scope.conertToCounterBid = function () {
                let param = {
                    reqId: $scope.reqId,
                    isCounterBid: $scope.IS_CB_ENABLED ? 0 : 1,
                    user: $scope.currentUserId,
                    sessionId: $scope.currentSessionId
                };

                auctionsService.toggleCounterBid(param)
                    .then(function (response) {
                        if (response && response.errorMessage === '') {
                            swal("Thanks !", "Successfully updated", "success");
                            location.reload();
                        } else {
                            swal("Error!", response.errorMessage, "error");
                        }
                    });
            };

            $scope.searchTable = function (str) {
                if ($scope.isCustomer) {
                    $scope.listRequirementItemsTemp = $scope.auctionItem.listRequirementItems.filter(function (item) {
                        return item.productIDorName.toUpperCase().indexOf(str.toUpperCase()) >= 0;
                    });
                } else {
                    $scope.listRequirementItemsTemp = $scope.auctionItemVendor.listRequirementItems.filter(function (item) {
                        return item.productIDorName.toUpperCase().indexOf(str.toUpperCase()) >= 0;
                    });
                }

                $scope.totalItems = $scope.listRequirementItemsTemp.length;
            };


            $scope.GetReqPRItems = function () {
                $scope.requirementPRStatus.MESSAGE = '';
                $scope.requirementPRStatus.showStatus = false;
                $scope.requirementPRStatus.validPRs = '';

                var params = {
                    "reqid": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };

                PRMPRServices.GetPRItemsByReqId(params)
                    .then(function (response) {
                        //console.log(response);
                        let inValidItems = _.filter(response, function (prItem) {
                            return prItem.IS_VALID <= 0;
                        });

                        let validPRs = _.filter(response, function (prItem) {
                            return prItem.IS_VALID > 0;
                        });

                        let validPRArray = [];
                        if (validPRs && validPRs.length > 0) {
                            validPRs.forEach(function (item, itemIndexs) {
                                if (!validPRArray.includes(item.PR_NUMBER)) {
                                    validPRArray.push(item.PR_NUMBER);
                                }
                            });

                            if (validPRArray && validPRArray.length > 0) {
                                $scope.requirementPRStatus.validPRs = validPRArray.join();
                            }
                        }

                        if (inValidItems && inValidItems.length > 0) {

                            let message = 'PR NUMBER - ITEM NUMBER - ITEM NAME<br/><br/>';
                            inValidItems.forEach(function (item, itemIndexs) {
                                message = message + item.PR_NUMBER + ' - ' + item.ITEM_NUM + ' - ' + item.ITEM_NAME + '.';
                            });

                            swal({
                                title: "It is observed that following PR-Items linked to this RFQ has been deleted.",
                                text: message + '<br/> <strong>Note</strong>: On clicking Continue. These PR-Items would be delinked automatically from the RFQ. You can continue with RFQ with these items as well however the PO would not be created for these deleted items',
                                type: "warning",
                                html: true,
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Continue",
                                cancelButtonText: "Cancel",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                                function (isConfirm) {
                                    if (isConfirm) {
                                        PRMPRServices.DeLinkInValidPRItems(params)
                                            .then(function (response) {
                                                growlService.growl("In-Valid PR Items are de-linked with Requirement.", "success");
                                            });
                                    } else {
                                        $state.go('home');
                                    }
                                });
                        }

                        let modiFiedItems = _.filter(response, function (prItem) {
                            return prItem.IS_MODIFIED > 0;
                        });

                        if (modiFiedItems && modiFiedItems.length > 0) {
                            $scope.requirementPRStatus.MESSAGE = 'Please validate the PRs added to the RFQ, ';
                            $scope.requirementPRStatus.showStatus = true;

                            modiFiedItems.forEach(function (item, itemIndexs) {
                                $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + '(' + item.PR_NUMBER + ' - ' + item.ITEM_NUM + ' - ' + item.ITEM_NAME + ' modified on ' + userService.toLocalDate(item.MODIFIED_DATE) + '), ';
                            });

                            $scope.requirementPRStatus.MESSAGE = $scope.requirementPRStatus.MESSAGE + ' the requirement last updated time (' + userService.toLocalDate($scope.auctionItem.dateModified) + '). Please review the changes and do the needful.';
                        }
                    });
            };

            //$scope.updateOnlineStatus = function () {
            //    console.log('sk siva' + navigator.onLine);
            //};

            //$window.addEventListener('online', $scope.updateOnlineStatus);
            //$window.addEventListener('offline', $scope.updateOnlineStatus);

            //setInterval(function () {
            //    try {
            //        if ($scope.isCustomer && $scope.auctionItem && $scope.auctionItem.status && $scope.auctionItem.status == "STARTED") {
            //            var parties = id + "$" + userService.getUserId() + "$" + "0" + "$" + userService.getUserToken();
            //            $scope.invokeSignalR('UpdateTimeRefresh', parties, function () { console.log(parties); });
            //        }
            //    }
            //    catch (err) {
                    
            //    }

            //}, 10000);

            $scope.convertDate = function (date) {
                if (date) {
                    var ts = moment(date, "DD-MM-YYYY HH:mm").valueOf();
                    var m = moment(ts);
                    var quotationDate = new Date(m);
                    var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                    return "/Date(" + milliseconds + "000+0530)/";
                } else {
                    return null;
                }
            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return new moment(dateBefore).format("DD-MM-YYYY HH:mm");
                } 
            };



            var isMobile = {
                Android: function () {
                    return navigator.userAgent.match(/Android/i);
                },
                BlackBerry: function () {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                iOS: function () {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                Opera: function () {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                Windows: function () {
                    return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
                },
                any: function () {
                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                }
            };

            if (isMobile.any()) {
                var windowState = 'focus';
                window.addEventListener("focus", handleBrowserState.bind(true));
                window.addEventListener("blur", handleBrowserState.bind(false));

                function handleBrowserState(isActive, isact) {
                    if (isActive.type !== windowState) {
                        windowState = isActive.type;
                        if (windowState === 'focus') {
                            location.reload();
                        }
                    }
                }

                window.addEventListener('offline', () => location.reload());
            }


            //$scope.bindTimer = function ()
            //{
            //    //if ($scope.isCustomer) {
            //    //    alert();
            //    //} else {
            //    //    alert();
            //    //}
            //}

        }]);