﻿prmApp
    .controller('marginCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state", "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog", "reportingService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService, userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService) {


            //#region Variables Initialization

            // EMPTY OBJECTS
            var requirementData = $scope.divfix = $scope.divfix3 = $scope.divfix1 = $scope.divfix2 = $scope.divfixMakeabid = $scope.divfixMakeabidError = $scope.boxfix = $scope.auctionItem = $scope.NegotiationSettings = $scope.poDetails = $scope.bidHistory = $scope.updatedeliverydateparams = $scope.updatepaymentdateparams = $scope.requirementTaxes = $scope.reports = $scope.formRequest = $scope.formRequest.selectedVendor = {};
            
            // EMPTY ARRAY
            $scope.auctionItem.auctionVendors = $scope.auctionItem.listRequirementItems = $scope.auctionItem.listRequirementTaxes = $scope.participatedVendors = $scope.vendorApprovals = $scope.auctionItemVendor = $scope.customerListRequirementTerms = $scope.customerDeliveryList = $scope.customerPaymentlist = $scope.listTerms = $scope.listRequirementTerms = $scope.deliveryList = $scope.paymentlist = $scope.listRequirementTaxes = [];

            // NULL
            $scope.quotationAttachment = null;

            // FALSE
            $scope.signalRCustomerAccess = $scope.Loding = $scope.makeaBidLoding = $scope.showTimer = $scope.userIsOwner = $scope.disableBidButton = $scope.NegotiationEnded = $scope.bidAttachementValidation = $scope.bidPriceEmpty = $scope.bidPriceValidation = $scope.showStatusDropDown = $scope.showGeneratePOButton = $scope.isDeleted = $scope.makeABidDisable = $scope.vendorBtns = $scope.startBtns = $scope.commentsvalidation = $scope.enableMakeBids = $scope.starreturn = $scope.deliveryRadio = $scope.paymentRadio = $scope.Loding = $scope.priceCapError = $scope.makeaBidLoding = $scope.taxValidation = $scope.ItemPriceValidation = $scope.discountfreightValidation = $scope.rejectresonValidation = $scope.paymentRadio = false;

            // TRUE
            $scope.allItemsSelected = $scope.disableDecreaseButtons = $scope.disableAddButton = $scope.auctionStarted = $scope.customerBtns = $scope.showVendorTable = $scope.quotationStatus = $scope.QuatationAprovelvalue = true;

            // EMPTY STRING
            $scope.nonParticipatedMsg = $scope.quotationRejecteddMsg = $scope.quotationNotviewedMsg = $scope.revQuotationRejecteddMsg = $scope.revQuotationNotviewedMsg = $scope.quotationApprovedMsg = $scope.revQuotationApprovedMsg = $scope.reduceBidAmountNote = $scope.incTaxBidAmountNote = $scope.noteForBidValue = $scope.toprankerName = $scope.quotationUrl = $scope.revquotationUrl = $scope.price = $scope.startTime = $scope.validity = $scope.notviewedcompanynames = $scope.updatedeliverydateparams.date = $scope.updatepaymentdateparams.date = $scope.priceValidationsVendor = $scope.rejectreson = $scope.NegotiationSettingsValidationMessage = $scope.requirementTaxes.taxName = $scope.formRequest.priceCapValueMsg = '';

            // ZERO
            $scope.days = $scope.hours = $scope.mins = $scope.auctionItem.priceCapValue = $scope.vendorInitialPrice = $scope.ratingForVendor = $scope.ratingForCustomer = $scope.vendorBidPrice = $scope.revvendorBidPrice = $scope.vendorRank = $scope.vendorQuotedPrice = $scope.priceSwitch = $scope.vendorID = $scope.starttimecondition1 = $scope.starttimecondition2 = $scope.revQuotationUrl1 = $scope.L1QuotationUrl = $scope.AmountSaved = $scope.totalprice = $scope.taxs = $scope.vendorTaxes = $scope.freightcharges = $scope.discountAmount = $scope.totalpriceinctaxfreight = $scope.vendorBidPriceWithoutDiscount = $scope.revtotalprice = $scope.revtaxs = $scope.revvendorTaxes = $scope.revfreightcharges = $scope.revtotalpriceinctaxfreight = $scope.calculatedSumOfAllTaxes = $scope.submitButtonShow = $scope.requirementTaxes.taxPercentage = $scope.isReportGenerated = $scope.formRequest.priceCapValue = 0;

            // NON EMPTY VALUES
            var id = $stateParams.Id;
            $scope.reqId = $stateParams.Id;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.sessionid = userService.getUserToken();
            $scope.customerID = userService.getUserId();            

            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';            
            
            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.isUnitPriceBidding = $scope.reqTaxSNo = 1;

            $scope.requirementTaxes.taxSNo = $scope.reqTaxSNo++;
            $scope.currentID = $scope.isQuotationRejected = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };

            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Fields';

            //#endregion

            //#region Access Check

            if ($scope.isCustomer) {
                auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }
                    });
            }

            // #endregion

            // #region SignalR

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }

            }

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                }
            }

            $log.info('trying to connect to service')
            requirementHub = SignalRFactory('', signalRHubName);
            $scope.invokeSignalR('joinGroup', 'requirementGroup' + $stateParams.Id);

            $log.info('connected to service')

            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR')
                requirementHub.stop();
                $log.info('disconected signalR')
            });

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
                $scope.invokeSignalR('UpdateTime', parties);
                //requirementHub.invoke('UpdateTime', parties, function (req) {
                //    //$log.info(req);
                //})
            };


            // #endregion

            //#region Call SignalR

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
                $scope.invokeSignalR('UpdateTime', parties);
            };           

            //#endregion

            //#region Bid related functions

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000 });
            };

            $scope.reduceBidAmount = function () {
                if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.auctionItem.minPrice > $scope.reduceBidValue) {
                    $("#reduceBidValue").val($scope.reduceBidValue);
                    $("#makebidvalue").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));
                    $("#reduceBidValue1").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));
                }
                else {
                    $("#makebidvalue").val($scope.auctionItem.minPrice);
                    $("#reduceBidValue1").val($scope.auctionItem.minPrice);
                    $scope.reduceBidValue = 0;
                    $("#reduceBidValue").val($scope.reduceBidValue);
                }
            };

            $scope.bidAmount = function () {
                if ($scope.vendorBidPrice != "" && $scope.vendorBidPrice >= 0 && $scope.auctionItem.minPrice > $scope.vendorBidPrice) {
                    $("#reduceBidValue").val($scope.auctionItem.minPrice - $scope.vendorBidPrice);
                    /*angular.element($event.target).parent().addClass('fg-line fg-toggled');*/
                }
                else {
                    $("#reduceBidValue").val(0);
                    swal("Error!", "Invalid bid value.", "error");
                    $scope.vendorBidPrice = 0;
                }
            };


            //#endregion

        }]);