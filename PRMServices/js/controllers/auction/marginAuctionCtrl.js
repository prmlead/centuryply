﻿prmApp
    .controller('marginAuctionCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state", "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog", "reportingService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService, userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService) {


            //#region Variables Initialization

            // EMPTY OBJECTS
            var requirementData = $scope.divfix = $scope.divfix3 = $scope.divfix1 = $scope.divfix2 = $scope.divfixMakeabid = $scope.divfixMakeabidError = $scope.boxfix = $scope.auctionItem = $scope.NegotiationSettings = $scope.poDetails = $scope.bidHistory = $scope.updatedeliverydateparams = $scope.updatepaymentdateparams = $scope.requirementTaxes = $scope.reports = $scope.formRequest = $scope.formRequest.selectedVendor = {};

            // EMPTY ARRAY
            $scope.auctionItem.auctionVendors = $scope.auctionItem.listRequirementItems = $scope.auctionItem.listRequirementTaxes = $scope.participatedVendors = $scope.vendorApprovals = $scope.auctionItemVendor = $scope.customerListRequirementTerms = $scope.customerDeliveryList = $scope.customerPaymentlist = $scope.listTerms = $scope.listRequirementTerms = $scope.deliveryList = $scope.paymentlist = $scope.listRequirementTaxes = [];

            // NULL
            $scope.quotationAttachment = null;

            // FALSE
            $scope.signalRCustomerAccess = $scope.Loding = $scope.makeaBidLoding = $scope.showTimer = $scope.userIsOwner = $scope.disableBidButton = $scope.NegotiationEnded = $scope.bidAttachementValidation = $scope.bidPriceEmpty = $scope.bidPriceValidation = $scope.showStatusDropDown = $scope.showGeneratePOButton = $scope.isDeleted = $scope.makeABidDisable = $scope.vendorBtns = $scope.startBtns = $scope.commentsvalidation = $scope.enableMakeBids = $scope.starreturn = $scope.deliveryRadio = $scope.paymentRadio = $scope.Loding = $scope.priceCapError = $scope.makeaBidLoding = $scope.taxValidation = $scope.ItemPriceValidation = $scope.discountfreightValidation = $scope.rejectresonValidation = $scope.paymentRadio = false;

            // TRUE
            $scope.allItemsSelected = $scope.disableDecreaseButtons = $scope.disableAddButton = $scope.auctionStarted = $scope.customerBtns = $scope.showVendorTable = $scope.quotationStatus = $scope.QuatationAprovelvalue = true;

            // EMPTY STRING
            $scope.nonParticipatedMsg = $scope.quotationRejecteddMsg = $scope.quotationNotviewedMsg = $scope.revQuotationRejecteddMsg = $scope.revQuotationNotviewedMsg = $scope.quotationApprovedMsg = $scope.revQuotationApprovedMsg = $scope.reduceBidAmountNote = $scope.incTaxBidAmountNote = $scope.noteForBidValue = $scope.toprankerName = $scope.quotationUrl = $scope.revquotationUrl = $scope.price = $scope.startTime = $scope.validity = $scope.notviewedcompanynames = $scope.updatedeliverydateparams.date = $scope.updatepaymentdateparams.date = $scope.priceValidationsVendor = $scope.rejectreson = $scope.NegotiationSettingsValidationMessage = $scope.requirementTaxes.taxName = $scope.formRequest.priceCapValueMsg = '';

            // ZERO
            $scope.days = $scope.hours = $scope.mins = $scope.auctionItem.priceCapValue = $scope.vendorInitialPrice = $scope.ratingForVendor = $scope.ratingForCustomer = $scope.vendorBidPrice = $scope.revvendorBidPrice = $scope.vendorRank = $scope.vendorQuotedPrice = $scope.priceSwitch = $scope.vendorID = $scope.starttimecondition1 = $scope.starttimecondition2 = $scope.revQuotationUrl1 = $scope.L1QuotationUrl = $scope.AmountSaved = $scope.totalprice = $scope.taxs = $scope.vendorTaxes = $scope.freightcharges = $scope.discountAmount = $scope.totalpriceinctaxfreight = $scope.vendorBidPriceWithoutDiscount = $scope.revtotalprice = $scope.revtaxs = $scope.revvendorTaxes = $scope.revfreightcharges = $scope.revtotalpriceinctaxfreight = $scope.calculatedSumOfAllTaxes = $scope.submitButtonShow = $scope.requirementTaxes.taxPercentage = $scope.isReportGenerated = $scope.formRequest.priceCapValue = 0;

            // NON EMPTY VALUES
            var id = $stateParams.Id;
            $scope.reqId = $stateParams.Id;
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.sessionid = userService.getUserToken();
            $scope.customerID = userService.getUserId();

            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

            $scope.auctionItem.reqType = 'REGULAR';
            $scope.auctionItem.isUnitPriceBidding = $scope.reqTaxSNo = 1;

            $scope.requirementTaxes.taxSNo = $scope.reqTaxSNo++;
            $scope.currentID = $scope.isQuotationRejected = -1;
            $scope.timerStyle = { 'color': '#000' };
            $scope.savingsStyle = { 'color': '#228B22' };
            $scope.restartStyle = { 'color': '#f00' };

            $scope.uploadQuotationButtonMsg = 'The Submit Quotation option would be available only if prices are entered above.';
            $scope.uploadQuotationTaxValidationMsg = 'Please check all Tax Fields';

            //#endregion

            //#region Access Check

            if ($scope.isCustomer) {
                auctionsService.GetIsAuthorized(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response.errorMessage != "") {
                            growlService.growl(response.errorMessage, "inverse");
                            $state.go('home');
                            return false;
                        }
                    });
            }

            // #endregion

            // #region SignalR

            $scope.checkConnection = function () {
                if (requirementHub) {
                    return requirementHub.getStatus();
                } else {
                    return 0;
                }
            };

            $scope.reconnectHub = function () {
                if (requirementHub) {
                    if (requirementHub.getStatus() == 0) {
                        requirementHub.reconnect();
                        return true;
                    }
                } else {
                    requirementHub = SignalRFactory('', signalRHubName);
                }

            }

            $scope.invokeSignalR = function (methodName, params, callback) {
                if ($scope.checkConnection() == 1) {
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                } else {
                    $scope.reconnectHub();
                    requirementHub.invoke(methodName, params, function (req) {
                        if (callback) {
                            callback();
                        }
                    })
                }
            }

            $log.info('trying to connect to service')
            requirementHub = SignalRFactory('', signalRHubName);
            $scope.invokeSignalR('joinGroup', 'requirementGroup' + $stateParams.Id);

            $log.info('connected to service')

            $scope.$on("$destroy", function () {
                $log.info('disconecting signalR')
                requirementHub.stop();
                $log.info('disconected signalR')
            });




            // #endregion

            //#region Call SignalR

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
                $scope.invokeSignalR('UpdateTime', parties);
            };

            $scope.updateTimeLeftSignalR = function () {
                var parties = id + "$" + userService.getUserId() + "$" + "10000" + "$" + userService.getUserToken();
                $scope.invokeSignalR('UpdateTime', parties);
            };

            $scope.recalculate = function (showSwal) {
                var params = {};
                params.reqID = id;
                params.sessionID = userService.getUserToken();
                params.userID = userService.getUserId();
                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                $scope.invokeSignalR('CheckRequirement', parties);
                if (showSwal) {
                    swal("Done!", "A refresh command has been sent to everyone.", "success");
                }
            }

            //#endregion

            //#region Miscellaneous

            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsUpdateStartTime.html', width: 1000 });
            };

            $scope.exportItemsToExcel = function () {
                var mystyle = {
                    sheetid: 'QuotationDetails',
                    headers: true,
                    column: {
                        style: 'font-size:15px;background:#233646;color:#FFF;'
                    }
                };

                if ($scope.auctionItem.isDiscountQuotation == 0) {
                    alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitPrice as UnitPrice, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                }
                if ($scope.auctionItem.isDiscountQuotation == 1) {
                    alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productBrand as Brand, productQuantityIn as [Units],unitMRP as UnitMRP, unitDiscount as UnitDiscount, cGst, sGst, iGst INTO XLSX(?,{headers:true,sheetid: "QuotationDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["QuotationDetails.xlsx", $scope.auctionItemVendor.listRequirementItems]);
                }

            }

            $scope.handlePrecision = function (item, precision) {

                item.sGst = $scope.precisionRound(parseFloat(item.sGst), precision);
                item.cGst = $scope.precisionRound(parseFloat(item.cGst), precision);
                item.Gst = $scope.precisionRound(parseFloat(item.Gst), precision);
                item.unitMRP = $scope.precisionRound(parseFloat(item.unitMRP), precision);
                item.unitDiscount = $scope.precisionRound(parseFloat(item.unitDiscount), precision + 6);
                item.revUnitDiscount = $scope.precisionRound(parseFloat(item.revUnitDiscount), precision + 6);
                item.costPrice = $scope.precisionRound(parseFloat(item.costPrice), precision);
                item.revCostPrice = $scope.precisionRound(parseFloat(item.revCostPrice), precision);
                item.netPrice = $scope.precisionRound(parseFloat(item.netPrice), precision);
                item.revnetPrice = $scope.precisionRound(parseFloat(item.revnetPrice), precision);
                item.marginAmount = $scope.precisionRound(parseFloat(item.marginAmount), precision);
                item.revmarginAmount = $scope.precisionRound(parseFloat(item.revmarginAmount), precision);
                $scope.totalprice = $scope.precisionRound(parseFloat($scope.totalprice), precision);
                $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), precision);
                return item;
            }            

            //#endregion
            
            //#region Bidding Process

            $scope.makeaBid1 = function () {
                $scope.makeABidDisable = true;
                var bidPrice = $("#makebidvalue").val();

                $log.info(bidPrice);

                if (bidPrice == "" || Number(bidPrice) <= 0) {
                    $scope.bidPriceEmpty = true;
                    $scope.bidPriceValidation = false;
                    $("#makebidvalue").val("");
                    $scope.makeABidDisable = false;
                    return false;
                } else if (!isNaN($scope.auctionItem.minPrice) && $scope.auctionItem.minPrice > 0 && bidPrice >= $scope.auctionItem.minPrice) {
                    $scope.bidPriceValidation = true;
                    $scope.bidPriceEmpty = false;
                    $scope.makeABidDisable = false;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidPriceValidation = false;
                    $scope.bidPriceEmpty = false;
                }
                if (($scope.bidAttachementName == "" || $scope.bidAttachement.length == 0) && $scope.quotationStatus == false) {
                    $scope.bidAttachementValidation = true;
                    $scope.makeABidDisable = false;
                    return false;
                } else {
                    $scope.bidAttachementValidation = false;
                }
                if (bidPrice > $scope.auctionItem.auctionVendors[0].runningPrice - $scope.auctionItem.minBidAmount) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Error!", "Your amount must be at least " + $scope.auctionItem.minBidAmount + " less than your previous bid.", 'error');
                    $scope.makeABidDisable = false;
                    return false;
                }
                if (bidPrice < (75 * $scope.auctionItem.auctionVendors[0].runningPrice / 100)) {
                    $scope.reduceBidValue = "";
                    $("#makebidvalue").val("");
                    $("#reduceBidValue").val("");
                    $scope.getData();
                    swal("Maximum Reduction Error!", " You are reducing more than 25% of current bid amount. The Maximum reduction amount per bid should not exceed more than 25% from current bid amount  " + $scope.auctionItem.minPrice + ". Incase if You want to Reduce more Please Do it in Multiple Bids", "error");
                    $scope.makeABidDisable = false;
                    return false;
                }
                else {
                    var params = {};
                    params.reqID = parseInt(id);
                    params.sessionID = userService.getUserToken();
                    params.userID = parseInt(userService.getUserId());
                    params.price = $scope.precisionRound(parseFloat(bidPrice), 2);
                    params.quotationName = $scope.bidAttachementName;

                    requirementHub.invoke('MakeBid', params, function (req) {

                        if (req.errorMessage == '') {
                            $scope.makeABidDisable = false;

                            swal("Thanks !", "Your bidding process has been successfully updated", "success");

                            if ($scope.auctionItem.isUnitPriceBidding == 1) {
                                $scope.items = {
                                    itemsList: $scope.auctionItemVendor.listRequirementItems,
                                    userID: userService.getUserId(),
                                    reqID: params.reqID,
                                    price: $scope.revtotalprice,
                                    vendorBidPrice: $scope.revvendorBidPrice,
                                    freightcharges: $scope.revfreightcharges,

                                };

                                auctionsService.SaveRunningItemPrice($scope.items)
                                    .then(function (response) {
                                        if (response.errorMessage != "") {
                                            growlService.growl(response.errorMessage, "inverse");
                                        } else {
                                            $scope.recalculate(false);
                                        }
                                    });
                            }

                            $scope.reduceBidValue = "";
                            $("#makebidvalue").val("");
                            $("#reduceBidValue").val("");

                        } else {
                            if (req.errorMessage != '') {

                                $scope.makeABidDisable = false;

                                var parties = id + "$" + userService.getUserId() + "$" + userService.getUserToken();
                                requirementHub.invoke('CheckRequirement', parties, function () {

                                    $scope.reduceBidValue = "";
                                    $("#makebidvalue").val("");
                                    $("#reduceBidValue").val("");

                                    $scope.getData();

                                    var htmlContent = 'We have some one in this range <h3>' + req.errorMessage + ' </h3> Kindly Quote some other value.'

                                    swal("Cancelled", "", "error");
                                    $(".sweet-alert h2").html("oops...! Your Price is Too close to another Bid <br> We have some one in this range <h3 style='color:red'>" + req.errorMessage + " </h3> Kindly Quote some other value.");
                                });

                            } else {
                                $scope.makeABidDisable = false;
                                swal("Error!", req.errorMessage, "error");
                            }
                        }
                    });

                }

            }

            $scope.setFields = function () {
                if ($scope.auctionItem.status == "CLOSED") {
                    $scope.mactrl.skinSwitch('green');
                    if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                        $scope.errMsg = "Negotiation has been completed. You can generate the Purchase Order by pressing the button below.";
                    }
                    else {
                        $scope.errMsg = "Negotiation has completed.";
                    }
                    $scope.showStatusDropDown = false;
                    $scope.showGeneratePOButton = true;
                }
                else if ($scope.auctionItem.status == "UNCONFIRMED" || $scope.auctionItem.status == "NOTSTARTED") {
                    $scope.mactrl.skinSwitch('teal');
                    $scope.errMsg = "Negotiation has not started yet.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = false;
                    $scope.timeLeftMessage = "Negotiation Starts in: ";
                    $scope.startBtns = true;
                    $scope.customerBtns = false;
                }
                else if ($scope.auctionItem.status == "STARTED") {
                    $scope.mactrl.skinSwitch('orange');
                    $scope.errMsg = "Negotiation has started.";
                    $scope.showStatusDropDown = false;
                    $scope.auctionStarted = true;

                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION
                    $scope.timeLeftMessage = "Negotiation Ends in: ";
                    // Dont Change This Text "Negotiation Ends in: " it has dependency in ENDNEGOTIATION

                    $scope.startBtns = false;
                    $scope.customerBtns = true;
                }
                else if ($scope.auctionItem.status == "DELETED") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "This requirement has been cancelled.";
                    $scope.showStatusDropDown = false;
                    $scope.isDeleted = true;
                }
                else if ($scope.auctionItem.status == "Negotiation Ended") {
                    $scope.mactrl.skinSwitch('bluegray');
                    $scope.errMsg = "Negotiation has been completed.";
                    $scope.showStatusDropDown = false;
                }
                else if ($scope.auctionItem.status == "Vendor Selected") {
                    $scope.mactrl.skinSwitch('bluegray');
                    if ($scope.auctionItem.isTabular) {
                        $scope.errMsg = "Please select vendors for all items in order to provide Purchase Order Information.";
                    } else {
                        $scope.errMsg = "Please click the button below to provide the Purchase Order information.";
                    }
                    $scope.showStatusDropDown = false;
                }
                else if ($scope.auctionItem.status == "PO Processing") {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.errMsg = "The PO has been generated. Please find the PO here: ";
                    $scope.showStatusDropDown = false;
                }
                else {
                    $scope.mactrl.skinSwitch('lightblue');
                    $scope.showStatusDropDown = true;
                }
                if (($scope.auctionItem.customerID == userService.getUserId() || $scope.auctionItem.superUserID == userService.getUserId() || $scope.auctionItem.customerReqAccess)) {
                    $scope.userIsOwner = true;
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(546654) && $scope.auctionItem.status == "STARTED") {
                        swal("Error!", "Live negotiation Access Denined", "error");
                        $state.go('home');
                    }
                    if ($scope.userIsOwner && !$rootScope.isUserEntitled(591159)) {
                        swal("Error!", "View Requirement Access Denined", "error");
                        $state.go('home');
                    }
                    $scope.options = ['PO Sent', 'Material Received', 'Payment Processing', 'Payment Released'];
                    $scope.options.push($scope.auctionItem.status);
                }
                var start = $scope.auctionItem.startTime.split('+')[0].split('(')[1];
                auctionsService.getdate()
                    .then(function (responseFromServer) {
                        var dateFromServer = new Date(parseInt(responseFromServer.substr(6)));
                        $log.debug(dateFromServer);
                        var curDate = dateFromServer;

                        var myEpoch = curDate.getTime();
                        $scope.timeLeftMessage = "";
                        if (start > myEpoch) {
                            $scope.auctionStarted = false;
                            $scope.timeLeftMessage = "Negotiation Starts in: ";
                            $scope.startBtns = true;
                            $scope.customerBtns = false;
                        } else {
                            $scope.auctionStarted = true;
                            $scope.timeLeftMessage = "Negotiation Ends in: ";
                            $scope.startBtns = false;
                            $scope.customerBtns = true;
                        }
                        if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                            $scope.startBtns = false;
                            $scope.customerBtns = false;
                        }
                        if ($scope.auctionItem.timeLeft == null || $scope.auctionItem.timeLeft < 1) {
                            $scope.showTimer = false;
                            $scope.disableButtons();
                        } else {
                            $scope.showTimer = true;
                        }
                        var date = $scope.auctionItem.postedOn.split('+')[0].split('(')[1];
                        var newDate = new Date(parseInt(parseInt(date)));
                        $scope.auctionItem.postedOn = newDate.toString().replace('Z', '');

                        var minPrice = 0;
                        if ($scope.auctionItem.status == "NOTSTARTED") {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].initialPrice : 0;
                        } else {
                            $scope.auctionItem.minPrice = $scope.auctionItem.auctionVendors[0] ? $scope.auctionItem.auctionVendors[0].runningPrice : 0;
                        }
                        for (var i in $scope.auctionItem.auctionVendors) {

                            $scope.nonParticipatedMsg = '';
                            $scope.quotationRejecteddMsg = '';
                            $scope.quotationNotviewedMsg = '';
                            $scope.revQuotationRejecteddMsg = '';
                            $scope.revQuotationNotviewedMsg = '';
                            $scope.quotationApprovedMsg = '';
                            $scope.revQuotationApprovedMsg = '';

                            var vendor = $scope.auctionItem.auctionVendors[i]

                            if (vendor.vendorID == userService.getUserId() && vendor.quotationUrl == "") {
                                $scope.quotationStatus = false;
                                $scope.quotationUploaded = false;
                            } else {
                                $scope.quotationStatus = true;
                                if ($scope.auctionItem.customerID != userService.getUserId() && $scope.auctionItem.superUserID != userService.getUserId() && !$scope.auctionItem.customerReqAccess) {
                                    $scope.quotationUploaded = true;
                                }
                                $scope.quotationUrl = vendor.quotationUrl;
                                $scope.revquotationUrl = vendor.revquotationUrl;
                                $scope.vendorQuotedPrice = vendor.runningPrice;
                            }
                            if (i == 0 && vendor.initialPrice != 0) {
                                minPrice = vendor.initialPrice;
                            } else {
                                if (vendor.initialPrice < minPrice && vendor.initialPrice != 0) {
                                    minPrice = vendor.initialPrice;
                                }
                            }
                            $scope.vendorInitialPrice = minPrice;
                            var runningMinPrice = 0;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice > 0 && $scope.auctionItem.auctionVendors[i].runningPrice < $scope.vendorInitialPrice) {
                                runningMinPrice = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            //$scope.auctionItem.minPrice = runningMinPrice;
                            if ($scope.auctionItem.auctionVendors[i].runningPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].runningPrice = 'NA';
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = 'NA';
                                $scope.auctionItem.auctionVendors[i].rank = 'NA';
                            } else {
                                $scope.vendorRank = vendor.rank;
                                if (vendor.rank == 1) {
                                    $scope.toprankerName = vendor.vendorName;
                                    if (userService.getUserId() == vendor.vendorID) {
                                        $scope.options = ['PO Accepted', 'Material Dispatched', 'Payment Acknowledged'];
                                        $scope.options.push($scope.auctionItem.status);
                                        if ($scope.auctionItem.status == "STARTED") {
                                            $scope.enableMakeBids = true;
                                        }
                                    }
                                }
                                //$scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice + ($scope.auctionItem.auctionVendors[i].runningPrice * $scope.auctionItem.auctionVendors[i].taxes) / 100;
                                $scope.auctionItem.auctionVendors[i].totalPriceIncl = $scope.auctionItem.auctionVendors[i].runningPrice;
                            }
                            if ($scope.auctionItem.auctionVendors[i].initialPrice == 0) {
                                $scope.auctionItem.auctionVendors[i].initialPrice = 'NA';
                                $scope.nonParticipatedMsg = 'You have missed an opportunity to participate in this Negotiation.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 1) {
                                $scope.quotationRejecteddMsg = 'Your quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == -1) {
                                $scope.quotationNotviewedMsg = 'Your quotation Not viewed by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 1) {
                                $scope.revQuotationRejecteddMsg = 'Your Rev.Quotation rejected by the customer.';
                            }
                            else if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == -1) {
                                $scope.revQuotationNotviewedMsg = 'Your Rev.Quotation Not viewed by the customer.';
                            }

                            if ($scope.auctionItem.auctionVendors[i].isQuotationRejected == 0) {
                                $scope.quotationApprovedMsg = 'Your Quotation Approved by the customer.';
                            }
                            if ($scope.auctionItem.auctionVendors[i].isRevQuotationRejected == 0) {
                                $scope.revQuotationApprovedMsg = 'Your Rev.Quotation Approved by the customer.';
                            }

                        }
                        $scope.$broadcast('timer-set-countdown-seconds', $scope.auctionItem.timeLeft);
                        $('.datetimepicker').datetimepicker({
                            useCurrent: false,
                            icons: {
                                time: 'glyphicon glyphicon-time',
                                date: 'glyphicon glyphicon-calendar',
                                up: 'glyphicon glyphicon-chevron-up',
                                down: 'glyphicon glyphicon-chevron-down',
                                previous: 'glyphicon glyphicon-chevron-left',
                                next: 'glyphicon glyphicon-chevron-right',
                                today: 'glyphicon glyphicon-screenshot',
                                clear: 'glyphicon glyphicon-trash',
                                close: 'glyphicon glyphicon-remove'

                            },

                            minDate: curDate
                        });
                    })


                $scope.reduceBidAmountNote = 'Specify the value to be reduce from bid Amount.';
                //$scope.incTaxBidAmountNote = 'Please Enter the consolidate bid value including tax amount.';
                $scope.incTaxBidAmountNote = '';
                //$scope.noteForBidValue = 'NOTE : If you fill one field, other will be autocalculated.';
                $scope.noteForBidValue = '';

                if ($scope.auctionItem.status == "STARTED") {
                    liveId = "live";
                }
                else {
                    liveId = "";
                }

            }

            $scope.GetBidHistory = function () {
                auctionsService.GetBidHistory({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.bidHistory = response;
                    });
            }

            if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)') {
                $scope.validity = '1 Day';
            }
            if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)') {
                $scope.validity = '2 Days';
            }
            if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)') {
                $scope.validity = '5 Days';
            }
            if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)') {
                $scope.validity = '10-15 Days';
            }

            $scope.reduceBidAmount = function () {
                if (!isNaN($scope.reduceBidValue) && $scope.reduceBidValue != "" && $scope.reduceBidValue >= 0 && $scope.auctionItem.minPrice > $scope.reduceBidValue) {
                    $("#reduceBidValue").val($scope.reduceBidValue);


                    $("#makebidvalue").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));
                    $("#reduceBidValue1").val($scope.precisionRound(parseFloat($scope.auctionItem.minPrice - $scope.reduceBidValue), 2));

                    console.log($scope.auctionItem.minPrice - $scope.reduceBidValue);

                    //angular.element($event.target).parent().addClass('fg-line fg-toggled');
                }
                else {
                    $("#makebidvalue").val($scope.auctionItem.minPrice);
                    //swal("Error!", "Invalid bid value.", "error");
                    $("#reduceBidValue1").val($scope.auctionItem.minPrice);
                    $scope.reduceBidValue = 0;
                    $("#reduceBidValue").val($scope.reduceBidValue);
                }


            }

            //#endregion

            //#region get from server

            $scope.GetRequirementTerms = function () {
                auctionsService.GetRequirementTerms(userService.getUserId(), $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.listRequirementTerms = response;
                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {

                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                        else {
                            $scope.customerListRequirementTerms.forEach(function (item, index) {
                                var obj = {
                                    currentTime: item.currentTime,
                                    errorMessage: item.errorMessage,
                                    isRevised: item.isRevised,
                                    refReqTermID: item.reqTermsID,
                                    reqID: item.reqID,
                                    reqTermsDays: item.reqTermsDays,
                                    reqTermsID: 0,
                                    reqTermsPercent: item.reqTermsPercent,
                                    reqTermsType: item.reqTermsType,
                                    sessionID: item.sessionID,
                                    userID: item.userID
                                };
                                $scope.listRequirementTerms.push(obj);
                            });

                            $scope.listRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.deliveryList.push(item);
                                    $scope.deliveryRadio = true;
                                }
                                else if (item.reqTermsType == 'PAYMENT') {
                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    $scope.paymentlist.push(item);
                                    $scope.paymentRadio = true;
                                }
                            });
                        }
                    });
            };

            $scope.GetRequirementTermsCustomer = function () {
                auctionsService.GetRequirementTerms($scope.auctionItem.customerID, $scope.reqId, userService.getUserToken())
                    .then(function (response) {
                        if (response && response.length > 0) {

                            $scope.customerListRequirementTerms = [];
                            $scope.customerDeliveryList = [];
                            $scope.customerPaymentlist = [];
                            $scope.customerListRequirementTerms = response;
                            $scope.customerListRequirementTerms.forEach(function (item, index) {
                                if (item.reqTermsType == 'DELIVERY') {
                                    $scope.customerDeliveryList.push(item);
                                }
                                else if (item.reqTermsType == 'PAYMENT') {
                                    if (item.reqTermsDays > 0) {
                                        item.paymentType = '+';
                                    }
                                    else if (item.reqTermsDays < 0) {
                                        item.paymentType = '-';
                                        item.reqTermsDays = -(item.reqTermsDays);
                                    }
                                    else if (item.reqTermsDays == 0) {
                                        item.paymentType = '';
                                    }
                                    $scope.customerPaymentlist.push(item);
                                }
                            });
                        }
                        $scope.GetRequirementTerms();
                    });
            };

            $scope.getData = function (methodName, callerID) {
                auctionsService.getrequirementdata({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        $scope.setAuctionInitializer(response, methodName, callerID);
                    });
            }

            $scope.$watch('auctionItemVendor.listRequirementItems', function (oldVal, newVal) {
                $log.debug("items list has changed");
                if (!newVal) { return; }
                newVal.forEach(function (item, index) {
                    if (!item.selectedVendorID || item.selectedVendorID == 0) {
                        $scope.allItemsSelected = false;
                    }
                })
            })

            $scope.setAuctionInitializer = function (response, methodName, callerID) {
                $scope.auctionItem = response;
                $scope.GetRequirementTermsCustomer();
                if (callerID == userService.getUserId() || callerID == undefined) {
                    $scope.auctionItemVendor = $scope.auctionItem;
                    if ($scope.auctionItemVendor.listRequirementItems != null) {
                        $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {
                            item.TemperoryRevUnitPrice = 0;
                            item.TemperoryRevUnitPrice = item.revUnitPrice;
                        });

                    }

                }
                $scope.notviewedcompanynames = '';
                $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                    if (item.quotationUrl != '' && item.isQuotationRejected == -1) {
                        $scope.Loding = false;
                        $scope.notviewedcompanynames += item.companyName + ', ';
                        $scope.starreturn = true;
                    }
                })
                $scope.notviewedcompanynames = $scope.notviewedcompanynames.substring(0, $scope.notviewedcompanynames.length - 2);
                if ($scope.auctionItem.listRequirementTaxes.length == 0) {
                    $scope.auctionItem.listRequirementTaxes = $scope.listRequirementTaxes;
                }
                else {
                    $scope.listRequirementTaxes = $scope.auctionItem.listRequirementTaxes;
                }

                $scope.listRequirementTaxeslength = $scope.listRequirementTaxes.length;

                $scope.reqTaxSNo = $scope.auctionItem.taxSNoCount;

                $scope.NegotiationSettings = $scope.auctionItem.NegotiationSettings;
                var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                $scope.days = parseInt(duration[0]);
                duration = duration[1];
                duration = duration.split(":", 4);
                $scope.hours = parseInt(duration[0]);
                $scope.mins = parseInt(duration[1]);

                $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';

                if (!response.auctionVendors || response.auctionVendors.length == 0) {
                    $scope.auctionItem.auctionVendors = [];
                    $scope.auctionItem.description = "";
                }

                $scope.auctionItem.quotationFreezTime = new moment($scope.auctionItem.quotationFreezTime).format("DD-MM-YYYY HH:mm");
                $scope.auctionItem.expStartTime = new moment($scope.auctionItem.expStartTime).format("DD-MM-YYYY HH:mm");

                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length != 0) {
                    $scope.totalprice = $scope.auctionItem.auctionVendors[0].initialPriceWithOutTaxFreight;
                    $scope.vendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.vendorBidPrice = $scope.auctionItem.auctionVendors[0].totalInitialPrice;
                    $scope.freightcharges = $scope.auctionItem.auctionVendors[0].vendorFreight;
                    $scope.discountAmount = $scope.auctionItem.auctionVendors[0].discount;
                    $scope.vendorBidPriceWithoutDiscount = $scope.vendorBidPrice + $scope.discountAmount;
                    $scope.vendorID = $scope.auctionItem.auctionVendors[0].vendorID;
                    $scope.warranty = $scope.auctionItem.auctionVendors[0].warranty;
                    $scope.duration = $scope.auctionItem.auctionVendors[0].duration;
                    $scope.payment = $scope.auctionItem.auctionVendors[0].payment;
                    $scope.validity = $scope.auctionItem.auctionVendors[0].validity;
                    $scope.otherProperties = $scope.auctionItem.auctionVendors[0].otherProperties;

                    $scope.revvendorTaxes = $scope.auctionItem.auctionVendors[0].taxes;
                    $scope.isQuotationRejected = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.quotationRejectedComment = $scope.auctionItem.auctionVendors[0].quotationRejectedComment;
                    $scope.revQuotationRejectedComment = $scope.auctionItem.auctionVendors[0].revQuotationRejectedComment;

                    if ($scope.auctionItem.urgency == 'Critical (Will be Closed Today)' && $scope.validity == '') {
                        $scope.validity = '1 Day';
                    }
                    else if ($scope.auctionItem.urgency == 'High (Will be Closed in 2 Days)' && $scope.validity == '') {
                        $scope.validity = '2 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Medium (Will be Closed in 5 Days)' && $scope.validity == '') {
                        $scope.validity = '5 Days';
                    }
                    else if ($scope.auctionItem.urgency == 'Low (Will Take 10-15 Days Time)' && $scope.validity == '') {
                        $scope.validity = '10-15 Days';
                    }
                    if ($scope.warranty == '') {
                        $scope.warranty = 'As Per OEM';
                    }
                    if ($scope.duration == '') {
                        $scope.duration = $scope.auctionItem.deliveryTime;
                    }
                    if ($scope.payment == '') {
                        $scope.payment = $scope.auctionItem.paymentTerms;
                    }

                    $scope.auctionItemVendor.listRequirementItems.forEach(function (item, index) {

                        item.Gst = item.cGst + item.sGst + item.iGst;

                        if ($scope.auctionItem.isDiscountQuotation == 2) {


                            if ($scope.auctionItem.isTabular && ($scope.auctionItem.status == 'UNCONFIRMED' || $scope.auctionItem.status == 'NOTSTARTED')) {
                                item.revCostPrice = item.costPrice;
                                item.revmarginAmount = item.marginAmount;
                            }

                            item = $scope.handlePrecision(item, 2);
                            item.Gst = item.cGst + item.sGst;
                            item = $scope.handlePrecision(item, 2);
                            item.costPrice = (item.unitMRP * 100 * 100) / ((item.unitDiscount * 100) + 10000 + (item.unitDiscount * item.Gst) + (item.Gst * 100));
                            item.revCostPrice = (item.unitMRP * 100 * 100) / ((item.revUnitDiscount * 100) + 10000 + (item.revUnitDiscount * item.Gst) + (item.Gst * 100));
                            item.netPrice = item.costPrice * (1 + item.Gst / 100);
                            item.revnetPrice = item.revCostPrice * (1 + item.Gst / 100);
                            item.marginAmount = item.unitMRP - item.netPrice;
                            item.revmarginAmount = item.unitMRP - item.revnetPrice;
                            item = $scope.handlePrecision(item, 2);
                        }
                        if (!item.selectedVendorID || item.selectedVendorID == 0) {
                            $scope.allItemsSelected = false;
                        }
                        if ($scope.auctionItem.status == "Negotiation Ended" || $scope.auctionItem.status == "Vendor Selected" || $scope.auctionItem.status == "PO Processing") {
                            if (item.selectedVendorID > 0) {
                                var vendorArray = $filter('filter')($scope.auctionItem.auctionVendors, { vendorID: item.selectedVendorID });
                                if (vendorArray.length > 0) {
                                    item.selectedVendor = vendorArray[0];
                                }
                            } else {
                                item.selectedVendor = $scope.auctionItem.auctionVendors[0];
                            }
                        }
                    })
                    if (!$scope.auctionItem.isStopped && $scope.auctionItem.timeLeft > 0) {
                        $scope.disableAddButton = false;
                    }
                    $scope.revfreightcharges = $scope.auctionItem.auctionVendors[0].revVendorFreight;
                    $("#revfreightcharges").val($scope.auctionItem.auctionVendors[0].revVendorFreight);
                    $scope.TemperoryRevfreightcharges = $scope.revfreightcharges;

                    if (callerID == userService.getUserId() || callerID == undefined) {
                        $scope.revtotalprice = $scope.auctionItem.auctionVendors[0].revPrice;

                        $scope.revtotalprice = $scope.precisionRound(parseFloat($scope.revtotalprice), 2);

                        $scope.revvendorBidPrice = $scope.auctionItem.auctionVendors[0].revVendorTotalPrice;
                    }
                    $scope.starttimecondition1 = $scope.auctionItem.auctionVendors[0].isQuotationRejected;
                    $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[0].revquotationUrl;
                    if ($scope.auctionItem.auctionVendors[0].companyName == 'PRICE_CAP') {
                        $scope.revQuotationUrl1 = $scope.auctionItem.auctionVendors[1].revquotationUrl;
                    }
                    $scope.L1QuotationUrl = $scope.auctionItem.auctionVendors[0].quotationUrl;
                    $scope.auctionItem.auctionVendors.forEach(function (item, index) {
                        if (item.revquotationUrl != '') {
                            $scope.RevQuotationfirstvendor = item.revquotationUrl;
                        }
                    })
                }

                if ($scope.auctionItem.auctionVendors && $scope.auctionItem.auctionVendors.length >= 2) {
                    $scope.starttimecondition2 = $scope.auctionItem.auctionVendors[1].isQuotationRejected;
                }
                $scope.participatedVendors = $scope.auctionItem.auctionVendors.filter(function (vendor) {
                    return (vendor.runningPrice > 0 && vendor.companyName != 'PRICE_CAP');
                });
                $scope.revisedParticipatedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP');
                })

                $scope.revisedApprovedVendors = $scope.participatedVendors.filter(function (vendor) {
                    return (vendor.revquotationUrl != null && vendor.revquotationUrl != '' && vendor.companyName != 'PRICE_CAP' && vendor.isRevQuotationRejected == 0);
                })
                if ($scope.auctionItem.status != 'DELETED' && $scope.auctionItem.status != 'UNCONFIRMED' && $scope.auctionItem.status != 'STARTED' && $scope.auctionItem.status != 'NOTSTARTED' && $scope.auctionItem.status != 'Negotiation Ended') {
                    auctionsService.getpodetails({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                        .then(function (response) {
                            $scope.poDetails = response;
                            if (response != undefined) {
                                var date1 = moment($scope.poDetails.expectedDelivery).format('DD/MM/YYYY  HH:mm');
                                $scope.updatedeliverydateparams.date = date1;
                                var date2 = moment($scope.poDetails.expPaymentDate).format('DD/MM/YYYY  HH:mm');
                                $scope.updatepaymentdateparams.date = date2;
                                if ($scope.updatepaymentdateparams.date == '31/12/9999') {
                                    $scope.updatepaymentdateparams.date = 'Payment Date';
                                }
                            }
                        });
                }
                $scope.description = $scope.auctionItem.description.replace(/\u000a/g, "</br>");
                $scope.deliveryLocation = $scope.auctionItem.deliveryLocation.replace(/\u000a/g, "</br>");
                $scope.paymentTerms = $scope.auctionItem.paymentTerms.replace(/\u000a/g, "</br>");
                $scope.deliveryTime = $scope.auctionItem.deliveryTime.replace(/\u000a/g, "</br>");

                var id = parseInt(userService.getUserId());
                var result = $scope.auctionItem.auctionVendors.filter(function (obj) {
                    return obj.vendorID == id;
                });
                if (id != $scope.auctionItem.customerID && id != $scope.auctionItem.superUserID && !$scope.auctionItem.customerReqAccess && result.length == 0) {
                    swal("Access denied", "You do not have access to this requirement because you are not part of this requirements process.", 'error');
                    $state.go('home');
                } else {
                    $scope.setFields();
                }
                $scope.quotationApprovedColor = {};
                $scope.quotationNotApprovedColor = {};

                if ($scope.auctionItem.auctionVendors[0].isQuotationRejected != 0 && $scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationNotApprovedColor = {};
                } else if ($scope.auctionItem.isUnitPriceBidding == 1) {
                    $scope.quotationNotApprovedColor = {
                        'background-color': '#f5b2b2'
                    };
                    $scope.quotationApprovedColor = {};
                }
            }
            $scope.getData();
            //#endregion

        }]);