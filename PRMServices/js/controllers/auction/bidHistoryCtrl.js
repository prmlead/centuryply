﻿prmApp

    .controller('bidHistoryCtrl', ["$scope", "$http", "$state", "domain", "$filter", "$log", "$stateParams", "$timeout", "auctionsService", "fwdauctionsService", "userService", "SignalRFactory", "fileReader", "growlService",
        function ($scope, $http, $state, domain, $filter, $log, $stateParams, $timeout, auctionsService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService) {
        $scope.bidhistory = {};
        $scope.bidhistory.uID = $stateParams.Id;
        $scope.bidhistory.reqID = $stateParams.reqID;
        $log.info($stateParams.isfwd);
        $scope.isForwardBidding = $stateParams.isfwd == 'true' ? true : false;

        $scope.userID = userService.getUserId();
        $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;

        $scope.bidHistory = {};
        $scope.CovertedDate = '';
        $scope.Name = 'No previous bids';

        $scope.show = false;

        $scope.data = [];
        $scope.categories = [];
        $scope.auctionItem = [];

        $scope.renderChart = function (response) {
            $scope.bidHistory = response;

            if ($scope.bidHistory.length > 0) {
                $scope.Name = $scope.bidHistory[0].firstName + ' ' + $scope.bidHistory[0].lastName;
            }

            $scope.bidHistory.forEach(function (item, index) {
                $scope.data.push(item.bidAmount);

                item.createdTime = userService.toLocalDate(item.createdTime);

                //var time = new moment(item.createdTime).format("HH:mm");

                $scope.categories.push(item.createdTime.toString())

            });

            $scope.startTime = $scope.bidHistory[0].createdTime;
            $scope.endTime = $scope.bidHistory[$scope.bidHistory.length - 1].createdTime;


            $scope.chartOptions = {
                title: {
                    text: 'Bid History Graph'
                },
                xAxis: {
                    categories: $scope.categories,
                    title: {
                        text: 'Time ( Start Time: ' + $scope.startTime + ' - End Time: ' + $scope.endTime + ')'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Bid Price'
                    }
                },

                series: [{
                    name: $scope.Name,
                    data: $scope.data


                }]
            };

            $scope.show = true;
        }

        $scope.GetBidHistory = function () {
            if ($scope.isForwardBidding) {
                fwdauctionsService.GetBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.renderChart(response);
                    });
            }
            else {
                auctionsService.GetBidHistory({ "reqid": $scope.bidhistory.reqID, 'userid': $scope.bidhistory.uID, "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.renderChart(response);
                    });
            }
            
        }

        $scope.GetBidHistory();

        $scope.GetDateconverted = function (dateBefore) {

            var date = dateBefore.split('+')[0].split('(')[1];
            var newDate = new Date(parseInt(parseInt(date)));
            $scope.CovertedDate = newDate.toString().replace('Z', '');
            return $scope.CovertedDate;

        };

        $scope.getData = function () {
            auctionsService.getrequirementdata({ "reqid": $scope.bidhistory.reqID, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    if (response) {
                        $scope.auctionItem = response;
                    }
                });
        };

        $scope.getData();

        $scope.CBPricesAudit = {};

        $scope.GetCBPricesAudit = function () {
            auctionsService.GetCBPricesAudit($scope.bidhistory.reqID, $scope.bidhistory.uID, userService.getUserToken())
                .then(function (response) {
                    $scope.CBPricesAudit = response;

                    $scope.CBPricesAudit.forEach(function (history, historyIndex) {

                        if ($scope.isCustomer) {
                            if ($scope.bidhistory.uID == history.CREATED_BY) {
                                history.isTargetBid = true;
                            } else {
                                history.isTargetBid = false;
                            }
                        }
                        else {
                            if ($scope.userID != history.CREATED_BY) {
                                history.isTargetBid = true;
                            } else {
                                history.isTargetBid = false;
                            }
                        }

                    })

                });
        };

        $scope.GetCBPricesAudit();

        $scope.GetDateconverted = function (dateBefore) {
            if (dateBefore) {
                return userService.toLocalDate(dateBefore);
            }
        };

    }]);