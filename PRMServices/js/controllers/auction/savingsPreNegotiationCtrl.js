﻿prmApp
    .controller('savingsPreNegotiationCtrl', ["$scope", "$state", "$stateParams", "$window", "$timeout", "userService", "auctionsService",
    function ($scope, $state, $stateParams, $window, $timeout, userService, auctionsService) {
        $scope.id = $stateParams.Id;
        $scope.totalItemsMinPrice = 0;
        $scope.totalLeastBidderPrice = 0;
        $scope.totalInitialPrice = 0;
        $scope.sessionid = userService.getUserToken();

        $scope.getData = function () {
            auctionsService.GetPriceComparisonPreNegotiation({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.auctionItem = response.requirement;
                    $scope.priceCompObj = response;
                    $scope.totalItemsL1Price = 0;
                    $scope.totalItemsMinimunPrice = 0;
                    $scope.negotiationSavings = 0;
                    $scope.savingsByLeastBidder = 0;
                    $scope.savingsByItemMinPrice = 0;

                    $scope.L1CompanyName = '';
                    $scope.L1CompanyName = $scope.auctionItem.auctionVendors[0].companyName;

                    if ($scope.auctionItem.auctionVendors[0].companyName == "PRICE_CAP") {
                        $scope.L1CompanyName = $scope.auctionItem.auctionVendors[1].companyName;
                    }

                    for (var i = 0; i < $scope.priceCompObj.priceCompareObject.length; i++) {
                        $scope.totalItemsL1Price += $scope.priceCompObj.priceCompareObject[i].leastBidderPrice;
                        $scope.totalItemsMinimunPrice += $scope.priceCompObj.priceCompareObject[i].minPrice;
                    }

                    $scope.negotiationSavings = $scope.auctionItem.savings;
                    $scope.savingsByLeastBidder = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsL1Price;
                    $scope.savingsByItemMinPrice = $scope.priceCompObj.minQuotationPrice - $scope.totalItemsMinimunPrice;

                    $scope.additionalSavings = 0;

                    $scope.additionalSavings = $scope.totalItemsL1Price - $scope.totalItemsMinimunPrice;

                })
        }


        $scope.isReportGenerated = 0;

        $scope.GetReportsRequirement = function () {
            auctionsService.GetReportsRequirement({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                .then(function (response) {
                    $scope.reports = response;
                    $scope.getData();
                    $scope.isReportGenerated = 1;
                });
        }

        $scope.getData();




        $scope.doPrint = false;


        $scope.printReport = function () {

            $scope.priceCompObj.priceCompareObject.forEach(function (item, index) {

                item.expanded = true;

            })

            $scope.doPrint = true;
            $timeout(function () {
                $window.print();
                $scope.doPrint = false

                $scope.priceCompObj.priceCompareObject.forEach(function (item, index) {

                    item.expanded = false;

                })

            }, 1000);

        };
    }]);