﻿prmApp
    .controller('generatePOCtrl', ["$scope", "$state", "$stateParams", "$window", "userService", "growlService", "fileReader", "$log", "poService", "auctionsService" , 
        function ($scope, $state, $stateParams, $window, userService, growlService, fileReader, $log, poService, auctionsService) {
            $scope.reqID = $stateParams.reqID;
            $scope.vendorID = $stateParams.vendorID;
            $scope.poID = $stateParams.poID;

            $scope.po = {};
            $scope.auctionVendors = [];

            $scope.sessionID = userService.getUserToken();
            $scope.isCustomer = userService.getUserType() == "CUSTOMER" ? true : false;
            $scope.compID = userService.getUserCompanyId();

            if ($scope.isCustomer) {
                $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            }

            $scope.deliveryAddress = '';
            $scope.purchaseID = '';
            $scope.indentID = '';
            $scope.requirement = 


            $scope.getData = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.reqID, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        //if (response) {
                        //  //  $scope.setAuctionInitializer(response, methodName, callerID);
                        //    auctionsService.GetCompanyConfiguration(response.custCompID, "ITEM_UNITS", userService.getUserToken())
                        //        .then(function (unitResponse) {
                        //            $scope.companyItemUnits = unitResponse;
                        //        });
                        //}
                        $scope.requirement = response;


                        $scope.requirement.auctionVendors.forEach(function (item, index) {
                            if ($scope.vendorID == item.vendorID) {
                                $scope.vendorID = item.vendorID;
                            }
                        })


                    });
            };

            $scope.getData();



            $scope.GetSeries = function (series, deptID) {
                $scope.purchaseID = 'PRM/PO';
            }


            if ($stateParams.poID > 0) {

            } else {
                $scope.GetSeries('', '');
            }



            $scope.updateForAllItems = function () {

                $scope.po.listPOItems.forEach(function (item, index) {
                    item.expectedDeliveryDate = $scope.expectedDeliveryDate;
                    item.deliveryAddress = $scope.deliveryAddress;
                });

                //for (i = 0; i < $scope.po.listPOItems.length; i++) {
                //    $scope.po.listPOItems.expectedDeliveryDate = $scope.expectedDeliveryDate;
                //}
            }

            $scope.autoUpdateForAllItems = function () {

                $scope.po.listPOItems.forEach(function (item, index) {
                    item.purchaseID = $scope.purchaseID;
                    item.indentID = $scope.indentID;
                });

                //for (i = 0; i < $scope.po.listPOItems.length; i++) {
                //    $scope.po.listPOItems.expectedDeliveryDate = $scope.expectedDeliveryDate;
                //}
            }

            //listPOItems


            $scope.changeVendorsPrices = function (vendorID) {
                //$scope.requirement.reqItems.forEach(function (item, index) {
                //    item.reqVendors.forEach(function (item1, index1) {
                //        if (vendorID == item1.vendorID) {
                //            item.vendorID = item1.vendorID;
                //            item.revVendorUnitPrice = item1.quotationPrices.revUnitPrice;
                //            item.poUnitPrice = item.revVendorUnitPrice;
                //        }
                //    })
                //})
                //var url = $state.href("po-list", { "reqID": $scope.reqID, "vendorID": vendorID, "poID": $scope.poID });
                //window.open(url, '_blank');
                $state.go('po', { "reqID": $scope.reqID, "vendorID": vendorID, "poID": 0 });
            }

            $scope.taxCalculation = function () {
                $scope.po.listPOItems.forEach(function (item, index) {

                    var tempRevUnitPrice = item.poPrice;
                    var tempCGst = item.cGst;
                    var tempSGst = item.sGst;
                    var tempIGst = item.iGst;

                    if (item.poPrice == undefined || item.poPrice <= 0) {
                        item.poPrice = 0;
                    };

                    item.poTotalPrice = item.poPrice * item.vendorPOQuantity;

                    if (item.cGst == undefined || item.cGst <= 0) {
                        item.cGst = 0;
                    };
                    if (item.sGst == undefined || item.sGst <= 0) {
                        item.sGst = 0;
                    };
                    if (item.iGst == undefined || item.iGst <= 0) {
                        item.iGst = 0;
                    };

                    item.poTotalPrice = item.poTotalPrice + ((item.poTotalPrice / 100) * (item.cGst + item.sGst + item.iGst));

                    item.poPrice = tempRevUnitPrice;
                    item.cGst = tempCGst
                    item.sGst = tempSGst;
                    item.iGst = tempIGst;

                    if (item.cGst < 0 || item.sGst < 0 || item.iGst < 0 || item.cGst == undefined || item.sGst == undefined || item.iGst == undefined || item.cGst > 100 || item.sGst > 100 || item.iGst > 100) {
                        $scope.gstValidation = true;
                    };
                });
            }






            $scope.GetVendorPoInfo = function () {
                poService.GetVendorPoInfo($scope.reqID, $scope.vendorID, $stateParams.poID)
                    .then(function (response) {
                        $scope.po = response;


                        $scope.deliveryAddress = $scope.po.req.deliveryLocation;

                        $scope.po.common = false;

                        $scope.po.listPOItems.forEach(function (item, index) {

                            if ($stateParams.poID > 0) {
                                $scope.indentID = item.indentID;
                                $scope.purchaseID = item.purchaseID;
                            } else {
                                $scope.indentID = $scope.po.req.prCode;
                            }

                            if ($stateParams.poID == 0) {
                                item.vendorPOQuantity = item.reqQuantity;
                                item.poPrice = item.vendorPrice;
                                item.deliveryAddress = $scope.deliveryAddress;
                            }

                            item.expectedDeliveryDate = new moment(item.expectedDeliveryDate).format("DD-MM-YYYY");
                            item.poPrice = item.vendorPrice;
                            if (item.expectedDeliveryDate.indexOf('-9999') > -1) {
                                item.expectedDeliveryDate = "";
                            }
                        });

                        $scope.taxCalculation();

                    });
            }

            $scope.GetVendorPoInfo();

            this.UploadPO = 0;

            $scope.converToDate = function (date) {
                var fDate = new moment(date).format("DD-MM-YYYY");

                if (fDate.indexOf('-9999') > -1) {
                    fDate = "-";
                }

                return fDate;
            }


            $scope.postRequest = function (sendToVendor) {

                $scope.po.sessionID = userService.getUserToken();
                $scope.po.isPOToVendor = sendToVendor;

                $scope.po.listPOItems.forEach(function (item, index) {
                    var ts = moment(item.expectedDeliveryDate, "DD-MM-YYYY").valueOf();
                    var m = moment(ts);
                    var deliveryDate = new Date(m);
                    var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                    item.expectedDeliveryDate = "/Date(" + milliseconds + "000+0530)/";
                    item.isPOToVendor = sendToVendor;
                    item.COMP_ID = $scope.compID;
                    item.DEPT_ID = $scope.deptID;
                });


                var params = {
                    'vendorpo': $scope.po
                }
                poService.SaveVendorPOInfo(params)
                    .then(function (response) {
                        if (response.errorMessage == '') {

                            //$window.history.back();

                            swal({
                                title: "Done!",
                                text: "Purchase Orders Generated Successfully.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Ok",
                                closeOnConfirm: true
                            },
                                function () {
                                    //location.reload();
                                    $state.go('po-list', { "reqID": $stateParams.reqID, "vendorID": $stateParams.vendorID, "poID": 0 });
                                });
                        }
                    });
            }


            $scope.goToDispatchTrack = function (poID, dTID) {
                //$state.go("reqTechSupport", { "reqId": $scope.reqId }, { reload: true, newtab: true });

                var url = $state.href("dispatchtrack", { "poID": poID, "dTID": dTID });
                window.open(url, '_blank');
            }


            $scope.cancelPO = function () {
                $state.go("po-list", { "reqID": $stateParams.reqID, "vendorID": $stateParams.vendorID, "poID": 0 });
            }



            $scope.getFile1 = function (id, doctype, ext) {
                $scope.progress = 0;
                $scope.file = $("#" + id)[0].files[0];
                $scope.docType = doctype + "." + ext;
                fileReader.readAsDataUrl($scope.file, $scope)
                    .then(function (result) {
                        if (id == "poFile") {
                            $scope.po.poFile = { "fileName": '', 'fileStream': null };
                            var bytearray = new Uint8Array(result);
                            $scope.po.poFile.fileStream = $.makeArray(bytearray);
                            $scope.po.poFile.fileName = $scope.file.name;
                        }

                    });
            };

            $scope.isExists = false;

            $scope.CheckUniqueIfExists = function (param, idtype) {

                $scope.isExists = false;

                $scope.params = {
                    param: param,
                    idtype: idtype,
                    sessionID: userService.getUserToken()
                }

                poService.CheckUniqueIfExists($scope.params)
                    .then(function (response) {
                        $scope.isExists = response;
                    });
            }

        }]);