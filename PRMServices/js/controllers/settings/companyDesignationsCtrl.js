﻿prmApp
    .controller('companyDesignationsCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.companyDesignations = [];


            $scope.CompanyDeptDesigTypes = [];

            $scope.designation = {
                userID: $scope.userID,
                desigID: 0,
                sessionID: $scope.sessionID
            };

            $scope.designations = {
                userID: $scope.userID,
                desigID: 0,
                desigCode: '',
                desigDesc: '',
                sessionID: $scope.sessionID,
                selectedDesigType: {}
            };

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;


            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            /*PAGINATION CODE*/

            $scope.addnewdesigView = false;

            $scope.GetCompanyDesignations = function () {
                auctionsService.GetCompanyDesignations($scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.companyDesignations = response;
                        $scope.totalItems = $scope.companyDesignations.length;
                    })
            }

            $scope.GetCompanyDesignations();


            $scope.SaveCompanyDesignations = function () {


                if ($scope.designation.desigCode == undefined || $scope.designation.desigCode == "") {
                    growlService.growl("Please Enter Designation Name.", "inverse");
                    return;
                }

                //if ($scope.designation.desigDesc == undefined) {
                //    growlService.growl("Please Enter Desg Desc.","inverse");
                //}

                if ($scope.designation.selectedDesigType == undefined || $scope.designation.selectedDesigType == null || $scope.designation.selectedDesigType.typeID == undefined || $scope.designation.selectedDesigType.typeID == "" || $scope.designation.selectedDesigType.typeID == 0) {
                    growlService.growl("Please Select Designation Type.", "inverse");
                    return;
                }


                var params = {
                    "companyDesignations": $scope.designation
                };

                auctionsService.SaveCompanyDesignations(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Designation Saved Successfully.", "success");
                            $scope.GetCompanyDesignations();
                            $scope.addnewdesigView = false;
                            $scope.designation = {
                                userID: $scope.userID,
                                desigID: 0,
                                sessionID: $scope.sessionID
                            };
                            //$window.history.back();
                        }
                    });

            };

            $scope.editDesignation = function (companyDepartment) {
                $scope.addnewdesigView = true;
                $scope.designation = companyDepartment;
                $scope.designation.selectedDesigType.typeID = companyDepartment.selectedDesigType.typeID;
                $scope.designation.sessionID = $scope.sessionID;
                $scope.designation.userID = $scope.userID;
                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            };

            $scope.closeEditDesignation = function () {
                $scope.addnewdesigView = false;
                $scope.designation = {
                    userID: $scope.userID,
                    desigID: 0,
                    sessionID: $scope.sessionID
                };
            };


            $scope.DeleteDesignation = function (companyDesignations) {

                var params = {
                    desigID: companyDesignations.desigID,
                    sessionID: $scope.sessionID
                };

                auctionsService.DeleteDesignation(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Designation Deleted Successfully.", "success");
                            $scope.GetCompanyDesignations();
                            //$window.history.back();
                        }
                    });
                $scope.addnewdesigView = false;
            };

            $scope.departmentUsers = [];
            $scope.deptUsersView = false;
            $scope.deptCode = '';

            $scope.GetDepartmentUsers = function (companyDepartment) {
                auctionsService.GetDepartmentUsers(companyDepartment.deptID, $scope.userID, $scope.sessionID)
                    .then(function (response) {
                        $scope.departmentUsers = response;
                    })
                $scope.deptCode = companyDepartment.deptCode;
                $scope.deptUsersView = true;

                document.body.scrollTop = 0; // For Chrome, Safari and Opera 
                document.documentElement.scrollTop = 0; // For IE and Firefox
            }

            $scope.SaveUserDepartments = function () {

                var params = {
                    "listUserDepartments": $scope.departmentUsers,
                    "sessionID": userService.getUserToken()
                };

                auctionsService.SaveUserDepartments(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            //swal("Error!", 'Not Saved', 'error');

                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {

                            growlService.growl("Users Added to Department Successfully.", "success");
                            $scope.deptUsersView = false;
                        }
                    })
            };

            $scope.cancelDeptUsersView = function () {
                $scope.deptUsersView = false;
                $scope.deptCode = '';
            };

            $scope.GetCompanyDeptDesigTypes = function () {
                auctionsService.GetCompanyDeptDesigTypes($scope.userID, 'DESIG', $scope.sessionID)
                    .then(function (response) {
                        $scope.CompanyDeptDesigTypes = response;
                        $log.info($scope.CompanyDeptDesigTypes);
                    })
            };

            $scope.GetCompanyDeptDesigTypes();


        }]);