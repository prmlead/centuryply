﻿prmApp
 .controller('profileSettingsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService) {

            $scope.NegotiationSettingsValidationMessage = '';
            $scope.prSettings = [];
            $scope.NegotiationSettings = [];
            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();

            $scope.otherSettings = {
                dateFilter: 1
            };

            $scope.weekDays = [{
                name: 'Sunday',
                isSelected: false
            }, {
                name: 'Monday',
                isSelected: false
            }, {
                name: 'Tuesday',
                isSelected: false
            }, {
                name: 'Wednesday',
                isSelected: false
            }, {
                name: 'Thursday',
                isSelected: false
            }, {
                name: 'Friday',
                isSelected: false
            }, {
                name: 'Saturday',
                isSelected: false
            }];

            $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';


            function getUserSettings() {
                userService.getUserSettings({ "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        if (response && response.data) {
                            let dateFilter = response.data.filter(function (item) {
                                return item.key1 === 'DATE_FILTER';
                            });

                            if (dateFilter && dateFilter.length > 0) {
                                $scope.otherSettings.dateFilter = dateFilter[0].value;
                            }
                        }   
                    });
            }

            getUserSettings();

            $scope.callGetUserDetails = function () {
                $log.info("IN GET USER DETAILS");
                userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        if (response != undefined) {
                            $scope.userDetails = response;

                            $scope.NegotiationSettings = $scope.userDetails.NegotiationSettings;
                            var duration = $scope.NegotiationSettings.negotiationDuration.split(" ", 4);
                            $scope.days = parseInt(duration[0]);
                            duration = duration[1];
                            duration = duration.split(":", 4);
                            $scope.hours = parseInt(duration[0]);
                            $scope.mins = parseInt(duration[1]);

                        }
                    });
            };

            $scope.callGetUserDetails();

            $scope.NegotiationTimeValidation = function (days, hours, mins, minReduction, rankComparision) {
                $scope.NegotiationSettingsValidationMessage = '';

                if (minReduction <= 0  || minReduction == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Min.Amount reduction';
                    return;
                }

                if (rankComparision <= 0 || rankComparision == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Set Min 0.01 for Rank Comparision price';
                    return;
                }

                if (minReduction >= 0 && rankComparision > minReduction) {
                    $scope.NegotiationSettingsValidationMessage = 'Please enter Valid Rank Comparision price less than Min. Amount reduction';
                    return;
                }
                if (days == undefined || days < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Days';
                    return;
                }
                if (hours < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (mins < 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if (mins >= 60 || mins == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Minutes';
                    return;
                }
                if (hours > 24 || hours == undefined) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours';
                    return;
                }
                if (hours == 24 && mins > 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Valid Hours & Minutes';
                    return;
                }
                if (mins < 5 && hours == 0 && days == 0) {
                    $scope.NegotiationSettingsValidationMessage = 'Please Enter Min 5 Minutes';
                    return;
                }
            };

            $scope.saveOtherSettings = function (type) {
                let items = [];
                switch (type) {
                    case 'DATE_FILTER':
                        items.push({
                            'key1': type,
                            'value': $scope.otherSettings.dateFilter
                        });
                        break;
                    default:
                        items = [];
                }

                if (items.length > 0) {
                    let params = {
                        userid: $scope.currentUserId,
                        sessionid: $scope.currentSessionId,
                        items: items
                    };

                    $http({
                        method: 'POST',
                        url: domain + 'saveothersettings',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data && !response.data.errorMessage) {
                            userService.userSettings = [];
                            growlService.growl('Successfully Saved', 'success');
                        }
                        else {
                            growlService.growl('Not Saved', 'inverse');
                        }
                    });
                }
            };

            $scope.saveNegotiationSettings = function () {
                $scope.NegotiationTimeValidation($scope.days, $scope.hours, $scope.mins, $scope.NegotiationSettings.minReductionAmount, $scope.NegotiationSettings.rankComparision);

                if ($scope.NegotiationSettingsValidationMessage == '') {
                    $scope.NegotiationSettings.negotiationDuration = $scope.days + ' ' + $scope.hours + ':' + $scope.mins + ':00.0';
                    var params = {
                        "NegotiationSettings": {
                            "userID": userService.getUserId(),
                            "minReductionAmount": $scope.NegotiationSettings.minReductionAmount,
                            "rankComparision": $scope.NegotiationSettings.rankComparision,
                            "negotiationDuration": $scope.NegotiationSettings.negotiationDuration,
                            "sessionID": userService.getUserToken(),
                            "compInterest": $scope.NegotiationSettings.compInterest,
                        }
                    };
                    $http({
                        method: 'POST',
                        url: domain + 'savenegotiationsettings',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data && (response.data.errorMessage == "" || response.data.errorMessage == null)) {
                            //$scope.NegotiationSettings = response.data;
                            growlService.growl('Successfully Saved', 'success');
                        }
                        else {
                            growlService.growl('Not Saved', 'inverse');
                        }
                    });
                }
            };

            $scope.goTocatergory = function () {            
                var url = $state.href('category', {});
                    $window.open(url, '_blank');
            };

            $scope.goToworkflow = function () {
                var url = $state.href('workflow', {});
                $window.open(url, '_blank');
            };

            $scope.savePRSettings = function () {
                var days = '';
                var tempDays = [];
                var selectedDays = $scope.weekDays.filter(function (day) {
                    return day.isSelected;
                });

                if (selectedDays && selectedDays.length > 0) {
                    selectedDays.forEach(function (obj, itemIndex) {
                        tempDays.push(obj.name);
                    });

                    days = _.join(tempDays, ';');
                }

                $scope.prSettings[0].configValue = days;
                $scope.prSettings[0].configText = days;
                var params = {
                    "listCompanyConfiguration": $scope.prSettings,
                    "sessionID": userService.getUserToken()
                };
                $http({
                    method: 'POST',
                    url: domain + 'savecompanyconfiguration',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data && !response.data.errorMessage) {
                        growlService.growl('Successfully Saved', 'success');
                        location.reload();
                    }
                    else {
                        growlService.growl('Not Saved', 'inverse');
                    }
                });
            };

            $scope.getPRSettings = function () {
                auctionsService.GetCompanyConfiguration(userService.getUserCompanyId(), "PR_SETTINGS", userService.getUserToken())
                    .then(function (response) {
                        $scope.prSettings = response;
                        if (!$scope.prSettings || $scope.prSettings.length <= 0) {
                            $scope.prSettings = [{
                                "compConfigID": 0,
                                "compID": userService.getUserCompanyId(),
                                "configKey": "PR_SETTINGS",
                                "configValue": "",
                                "configText": '',
                                "isValid": true
                            }];
                        } else {
                            var days = $scope.prSettings[0].configValue;

                            if (days) {
                                var tempDays = days.split(";");
                                tempDays.forEach(function (obj, itemIndex) {
                                    var weekDay = $scope.weekDays.filter(function (day) {
                                        return day.name === obj;
                                    });

                                    if (weekDay && weekDay.length > 0) {
                                        weekDay[0].isSelected = true;
                                    }
                                });
                            }
                        }
                    });
            };

            $scope.getPRSettings();

        }]);