﻿
prmApp
    .controller('profileReqsCtrl', ["$timeout", "$uibModal", "$state", "$window", "$scope", "growlService", "userService", "auctionsService", "fwdauctionsService", "$http", "domain", "$rootScope", "fileReader", "$filter", "$log", "reportingService", "$stateParams",
        function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, $stateParams) {
            $scope.formRequest = {};
            $scope.formRequest.isForwardBidding = false;

            /*pagination code*/
            $scope.totalItems = 0;
            $scope.totalVendors = 0;
            $scope.totalSubuser = 0;
            $scope.totalInactiveVendors = 0;
            $scope.totalLeads = 0;
            $scope.currentPage = 1;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage = 10;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize = 10;
            $scope.myAuctions = [];
            $scope.categories = [];
            $scope.reqStatus = '';
            $scope.biddingType = 'ALL';
            $scope.category = 'ALL CATEGORIES';
            $scope.currentSessionId = userService.getUserToken();
            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.otherSettings = {
                dateFilter: 1
            };

            $scope.isSuperUser = userService.getUserObj().isSuperUser;
            /* CLIENT STATUS MAPPING TO PRM STATUS */
            $scope.isCustomer = userService.getUserType();
            $scope.prmStatus = function (type, status) {
                return userService.NegotiationStatus(type, status);
            };
            /* CLIENT STATUS MAPPING TO PRM STATUS */

            // Clickable dashboard //
            $scope.NavigationFilters = $stateParams.filters;
            // Clickable dashboard //

            //$scope.setPage = function (pageNo) {
            //    $scope.currentPage = pageNo;
            //};

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getAuctions(($scope.currentPage - 1), 10, $scope.searchKeyword);

            };

            $scope.GetDateconverted = function (dateBefore) {
                if (dateBefore) {
                    return userService.toLocalDate(dateBefore);
                }
            };

            $scope.pageChanged = function () {
                //$scope.getAuctions();
            };

            $scope.filterDate = {
                reqToDate: '',
                reqFromDate: ''
            };

            $scope.filterDate.reqToDate = moment().format('YYYY-MM-DD');
            $scope.filterDate.reqFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            $scope.getAuctions = function (recordsFetchFrom, pageSize, searchKeyword) {
                $log.info($scope.formRequest.isForwardBidding);
                if (!$scope.formRequest.isForwardBidding) {


                    //if (isfilter) {
                    //    $scope.totalItems = 0;
                    //    $scope.currentPage = 1;
                    //    $scope.currentPage2 = 1;
                    //    $scope.itemsPerPage = 8;
                    //    $scope.itemsPerPage2 = 8;
                    //    $scope.maxSize = 8;
                    //    $scope.myAuctions = [];
                    //}

                  //  auctionsService.getmyAuctions({ "userid": userService.getUserId(), "reqstatus": $scope.reqStatus, "sessionid": userService.getUserToken() })
                    auctionsService.getmyAuctions({ "userid": userService.getUserId(), "status": $scope.reqStatus, "fromDate": $scope.filterDate.reqFromDate, "toDate": $scope.filterDate.reqToDate, "sessionid": userService.getUserToken(), "page": recordsFetchFrom * pageSize, "pagesize": pageSize, "search": searchKeyword ? searchKeyword : '' })
                        .then(function (response) {

                            if (response) {
                                response.forEach(function (item, index) {
                                    item.qcsCodesArray = [];
                                    if (item.qcsCodes) {
                                        var values = item.qcsCodes.split(',');
                                        item.qcsCodesArray = values;
                                    }
                                });
                            }
                            $scope.myAuctions1 = [];
                            //$scope.myAuctions1 = $scope.myAuctions.concat(response);
                            $scope.myAuctions1 = response;
                            $scope.myAuctions1 = $scope.myAuctions1.filter(function (auction) {
                                return (auction.biddingType !== 'TENDER');
                            });
                            $scope.myAuctions1.forEach(function (item, index) {
                                item.postedOn = $scope.GetDateconverted(item.postedOn);
                                item.quotationFreezTime = $scope.GetDateconverted(item.quotationFreezTime);
                                item.expStartTime = $scope.GetDateconverted(item.expStartTime);
                                item.startTime = $scope.GetDateconverted(item.startTime);

                                if (String(item.startTime).includes('9999') || String(item.startTime).includes('10000')) {
                                    item.startTime = '-';
                                }


                                if (item.status == 'UNCONFIRMED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'NOT STARTED') {
                                    item.statusColor = 'text-warning';
                                }
                                else if (item.status == 'STARTED') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Negotiation Ended') {
                                    item.statusColor = 'text-success';
                                }
                                else if (item.status == 'Qcs Pending') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Qcs Created') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Qcs Approval Pending') {
                                    item.statusColor = 'text-danger';
                                }
                                else if (item.status == 'Qcs Approved') {
                                    item.statusColor = 'text-success';
                                }
                                else {
                                    item.statusColor = '';
                                }


                                if (item.isRFP) {
                                    item.rfpStatusColor = 'orangered';
                                } else {
                                    item.rfpStatusColor = 'darkgreen';
                                }

                                if (item.status == "UNCONFIRMED" && (item.status != "NOTSTARTED" || item.status != "STARTED" || item.status != "Negotiation Ended")) {
                                    if (item.noOfVendorsInvited === 0) {
                                        item.status = "Saved_As_Draft";
                                        item.statusColor = '';
                                    }
                                }
                            });

                            $scope.myAuctions = $scope.myAuctions1;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions[0].totalCount;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }

                            $scope.tempStatusFilter = angular.copy($scope.myAuctions);
                            $scope.tempCategoryFilter = angular.copy($scope.myAuctions);


                            // Clickable dashboard //

                            if ($scope.NavigationFilters && $scope.NavigationFilters.status) {
                                //$scope.getStatusFilter($scope.NavigationFilters.status);
                                $scope.reqStatus = $scope.NavigationFilters.status === 'ALL' ? '' : $scope.NavigationFilters.status;
                            }

                            // Clickable dashboard //

                        });
                }
                else {
                    fwdauctionsService.getmyAuctions({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                        .then(function (response) {
                            $scope.myAuctions = response;
                            if ($scope.myAuctions.length > 0) {
                                $scope.myAuctionsLoaded = true;
                                $scope.totalItems = $scope.myAuctions.length;
                            } else {
                                $scope.myAuctionsLoaded = false;
                                $scope.totalItems = 0;
                                $scope.myAuctionsMessage = "There are no auctions running right now for you.";
                            }
                        });
                }
            };

            $scope.GetRequirementsReport = function () {
                auctionsService.getrequirementsreport({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {
                        $scope.reqReport = response;
                        alasql.fn.handleDate = function (date) {
                            return new moment(date).format("MM/DD/YYYY");
                        }
                        alasql('SELECT requirementID as RequirementID, title as RequirementTitle, handleDate(postedDate) as [PostedDate], status as Status, vendorID as VendorID, companyName as CompanyName, userPhone as Phone,userEmail as Email, itemID as [ItemID], productBrand as Brand, productQuantity as Quantity, productQuantityIn as [Units], costPrice as [InitialCostPrice], revCostPrice as [FinalCostPrice], netPrice as [InitialNetPrice], marginAmount as [InitialMarginAmount], unitDiscount as [MarginPercentage],revUnitDiscount as [RevisedMarginPercentage],unitMRP as [MRP],gst as [GST],savings as [Savings], handleDate(startTime) as [StartTime], status as [Status], othersBrands as [ManufacturerName] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xlsx", $scope.reqReport]);
                    });

                //alasql('SELECT requirementID as [RequirementID],title as [Title],vendorID as [VendorID],companyName as [Company],userPhone as [Phone],userEmail as [Email],itemID as [ItemID],productBrand as [Brand],productQuantity as [Quantity], productQuantityIn as [Units],costPrice as [InitialCostPrice],revCostPrice as [FinalCostPrice],netPrice as [NetPrice],marginAmount as [InitialMarginAmount],unitDiscount as [InitialMargin],revUnitDiscount as [FinalMargin],unitMRP as [MRP],gst as [GST],maxInitMargin as [MaximumInitialMargin],maxFinalMargin as [MaximumFinalMargin],savings as [Savings] INTO XLSX(?,{headers:true,sheetid: "MarginTypeConsolidatedReport", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["MarginTypeConsolidatedReport.xslx", $scope.reqReport]);

            };
            
            $scope.goToReqReport = function (reqID) {
                    //$state.go("reports", { "reqID": reqID });

                    var url = $state.href('reports', { "reqID": reqID });
                    $window.open(url, '_blank');

            };

            $scope.cloneRequirement = function (requirement) {
                let tempObj = {};
                let stateView = 'clone-requirement';
                if (requirement.biddingType && requirement.biddingType === 'TENDER') {
                    stateView = 'save-tender';
                }
                
                tempObj.cloneId = requirement.requirementID;
                userService.setCloneId(requirement.requirementID);
                $state.go(stateView, { 'Id': requirement.requirementID });
            };

            $scope.chatRequirement = function (requirement) {
                var url = $state.href('req-chat', { "reqId": requirement.requirementID });
                $window.open(url, '_blank');
            };

            $scope.searchTable = function (str) {

                //$scope.category = 'ALL CATEGORIES';
                //$scope.reqStatus = 'ALL';
                //$scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                //    return (String(req.requirementID).includes(str) == true || String(req.title).includes(str) == true || req.category[0].includes(str) == true );
                //});

                // RFQ Date Based Filter // 

                str = str.toLowerCase();
                $scope.category = 'ALL CATEGORIES';
                $scope.reqStatus = 'ALL';
                $scope.myAuctions = $scope.myAuctions1.filter(function (req) {
                    return (String(req.requirementID).includes(str) == true
                        || String(req.title.toLowerCase()).includes(str) == true
                        || String(req.userName.toLowerCase()).includes(str) == true
                        || String(req.postedOn).includes(str) == true
                        || req.category[0].toLowerCase().includes(str) == true);
                });

                // RFQ Date Based Filter //

                $scope.totalItems = $scope.myAuctions.length;
            };

            
            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' }
                }).then(function (response) {
                    if (response && response.data) {

                        if (response.data.length > 0) {
                            $scope.categories = _.uniq(_.map(response.data, 'category'));

                            $scope.categories.push('ALL CATEGORIES');

                            //categories.splice(categories.indexOf("ALL"), 1);
                            //categories.unshift('ALL');

                            $scope.categories = $scope.categories.filter(item => item !== "ALL CATEGORIES" && item !== "");
                            $scope.categories.unshift("ALL CATEGORIES");

                            //console.log($scope.categories);

                            $scope.categoriesdata = response.data;
                            $scope.showCategoryDropdown = true;
                        }
                    }
                }, function (result) {
                });

            };


            $scope.getCategories();

            $scope.getStatusFilter = function (filterVal) {
                $scope.filterArray = [];
                if (filterVal == 'ALL') {
                    $scope.myAuctions = $scope.myAuctions1;

                } else {
                    $scope.filterArray = $scope.tempStatusFilter.filter(function (item) {
                        return $scope.prmStatus($scope.isCustomer, item.status) === $scope.prmStatus($scope.isCustomer, filterVal);
                    });
                    $scope.myAuctions = $scope.filterArray;
                }

                $scope.totalItems = $scope.myAuctions.length;

            };

            $scope.getCategoryFilter = function (filterCategoryVal) {
                $scope.filterCatArray = [];
                if (filterCategoryVal == 'ALL CATEGORIES') {
                    $scope.myAuctions = $scope.myAuctions1;
                } else {
                    $scope.filterCatArray = $scope.tempCategoryFilter.filter(function (item) {
                        return item.category[0] == filterCategoryVal;
                    });
                    $scope.myAuctions = $scope.filterCatArray;

                }

                $scope.totalItems = $scope.myAuctions.length;
            };

            function getUserSettings() {
                userService.getUserSettings({ "userid": $scope.currentUserId, "sessionid": $scope.currentSessionId })
                    .then(function (response) {
                        if (response && response.data) {
                            let dateFilter = response.data.filter(function (item) {
                                return item.key1 === 'DATE_FILTER';
                            });

                            if (dateFilter && dateFilter.length > 0) {
                                $scope.otherSettings.dateFilter = dateFilter[0].value;
                            }
                        }

                        if ($scope.otherSettings.dateFilter && +$scope.otherSettings.dateFilter > 0) {
                            $scope.filterDate.reqFromDate = moment().subtract(+$scope.otherSettings.dateFilter * 30, "days").format("YYYY-MM-DD");
                        }
                        
                        $scope.getAuctions(0, 10, $scope.searchKeyword);
                    });
            }

            getUserSettings();

            $scope.goToQCS = function (reqId, qcsId) {
                return userService.goToQCS(0, userService.getUserToken(), +qcsId);
            };

           
        }]);