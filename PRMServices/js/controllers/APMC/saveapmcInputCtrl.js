﻿prmApp
    .controller('saveapmcInputCtrl', ["$scope", "$stateParams", "$state", "$window", "$log", "$filter", "$http", "domain", "userService", "growlService", "apmcService",
        function ($scope, $stateParams, $state, $window, $log, $filter, $http, domain, userService, growlService, apmcService) {
            $scope.companyID = userService.getUserCompanyId();
            $scope.sessionID = userService.getUserToken();
            $scope.isCustomer = userService.getUserType() == 'CUSTOMER';

            $scope.apmcvendorlist = [];
            $scope.apmcDashboard = {};

            $scope.getapmcvendorlist = function () {
                apmcService.getapmcvendorlist()
                    .then(function (response) {
                        $scope.apmcvendorlist = response;
                    })
            }

            $scope.selectVendor = function (vendor) {
                vendor.isSelected = true;
            }

            $scope.saveapmcinput = function () {
                $scope.apmcDashboard.apmcInput.company = {
                    companyID: userService.getUserCompanyId()
                }
                $scope.apmcDashboard.apmcInput.userID = userService.getUserId();
                var params = {
                    apmcInput: $scope.apmcDashboard.apmcInput
                }
                apmcService.saveapmcinput(params)
                    .then(function (response) {
                        $scope.selectedapmcs = $filter('filter')($scope.apmcvendorlist, { isSelected: true });
                        for (i = 0; i < $scope.selectedapmcs.length; i++) {
                            $scope.selectedapmcs[i].apmcNegotiationID = response.objectID;
                            $scope.selectedapmcs[i].custPriceInclFinal = $scope.apmcDashboard.apmcInput.inputPrice;
                            $scope.selectedapmcs[i].vendorCompany = {
                                vendorID: $scope.selectedapmcs[i].company.companyID
                            }
                        }
                        var params = {
                            apmcNegotiationList: $scope.selectedapmcs,
                            apmcNegotiationID: response.objectID,
                            userID: userService.getUserId()
                        }
                        apmcService.saveapmcnegotiationlist(params)
                            .then(function (response) {
                                if (response.errorMessage != '') {
                                    growlService.growl(response.errorMessage, "inverse");
                                }
                                else {
                                    growlService.growl("Saved Successfully.", "success");
                                    //$scope.getapmcdashboard();
                                    $state.go('apmcdashboard');
                                }
                            })
                    })
            }

            $scope.getapmcvendorlist();

        }]);