﻿
var editAttrDialogModel = function () {
    this.visible = false;
};
editAttrDialogModel.prototype.edit = function (property) {
    this.prop = {
        propId: property.propId,
        propName: property.propName,
        propDesc: property.propDesc != null ? property.propDesc : "",
        propDataType: property.propDataType,
        propType: property.propType,
        propOptions: property.propOptions != null ? property.propOptions.split('^^') : []
    };
    //this.prop = property;
    //this.prop.propOptions = property.propOptions != null ? property.propOptions.split('$$') : [];
    this.visible = true;
};
editAttrDialogModel.prototype.new = function () {
    this.prop = [{
        propId : 0,
        propName: '',
        propDesc: '',
        propDataType: '',
        propType: '',
        propOptions: []
    }];
    this.visible = true;
};
editAttrDialogModel.prototype.close = function () {
    this.visible = false;
};



prmApp.directive('editAttrDialog', [function () {
    return {
        restrict: 'AE',
        scope: {
            model: '=',
            saveclick:"&"
        },
        controller: function ($scope) {
            $scope.addOption = function () {
                if (!$scope.model.prop.propOptions) {
                    $scope.model.prop.propOptions = [];
                }
                if ($scope.model.prop.propOptions.indexOf('') == -1) {
                    $scope.model.prop.propOptions.push('');
                }
            };
            $scope.saveOption = function (index, value) {
                $scope.model.prop.propOptions[index] = value;
            };
            $scope.removeOption = function (index) {
                $scope.model.prop.propOptions.splice(index, 1);
            };
        },
        link: function (scope, element, attributes) {
            scope.$watch('model.visible', function (newValue) {
                var modalElement = element.find('.modal');
                modalElement.modal(newValue ? 'show' : 'hide');
            });

            element.on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.model.visible = true;
                });
            });

            element.on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.model.visible = false;
                });
            });

        },
        templateUrl: "views/CatalogMgmt/edit-attr-dialog.html"
    };
}]);

prmApp
      .controller('attributeCtrl', ['$scope', 'catalogService', 'userService', 'growlService', function ($scope, catalogService, userService, growlService) {


          $scope.editDialog = new editAttrDialogModel();
          $scope.compId = userService.getUserCompanyId();


          $scope.totalItems = 0;
          $scope.currentPage = 1;
          $scope.currentPage2 = 1;
          $scope.itemsPerPage = 6;
          $scope.itemsPerPage2 = 6;
          $scope.maxSize = 6;
         

          $scope.setPage = function (pageNo) {
              $scope.currentPage = pageNo;
          };

          $scope.pageChanged = function () {
              
          };



          $scope.getProperties = function () {
             

              catalogService.getProperties($scope.compId, 0, 99)
                  .then(function (response) {
                      $scope.properties1 = response;
                      $scope.properties = response;
                     

                      if ($scope.properties.length > 0) {
                          $scope.propertiesLoaded = true;
                          $scope.totalItems = $scope.properties.length;
                      } else {
                          $scope.propertiesLoaded = false;
                          $scope.totalItems = 0;

                      }
                  });
             
          }
          $scope.getProperties();


          $scope.searchTable = function (str) {

              console.log(str);
              if (!str || str == '' || str == undefined || str == null) {
                  $scope.properties = $scope.properties1;
              }
              else {
                  $scope.properties = $scope.properties1.filter(function (properties) {
                      return (String(properties.propName).includes(str) == true || String(properties.propDesc).includes(str) == true || String(properties.propType).includes(str) == true || String(properties.propDataType).includes(str) == true);
                  });
              }


              $scope.totalItems = $scope.properties.length;
          }

          $scope.saveProperty = function (propData) {
              $scope.propertyObj = {
                  propId: propData.propId == null ? 0 : propData.propId,
                  propName: propData.propName,
                  propDesc: propData.propDesc,
                  propType: propData.propType,
                  propDataType: propData.propDataType,
                  propValue: propData.propValue == null ? "" : propData.propValue,
                  propOptions: propData.propOptions == null ? "" : (angular.isArray(propData.propOptions) ? propData.propOptions.join('^^') : propData.propOptions),                  
                  propDefaultValue: '',
                  isValid: 1,
                  user: userService.getUserId(),
                  companyId: $scope.compId
              };
              var param = {
                  property: $scope.propertyObj,
                  sessionID: userService.getUserToken()
              }
              catalogService.savecatalogproperty(param)
                  .then(function (response) {
                      if (response.errorMessage != '') {
                          growlService.growl(response.errorMessage, "inverse");
                      }
                      else {
                          growlService.growl("property added/updated Successfully.", "success");
                          $scope.getProperties();
                      }
                  });
          }

          $scope.addProperty = function () {
              $scope.propertyObj = {
                  propId: 0,
                 
                
                  propName: $scope.propName,
                  propDesc: $scope.propDesc,
                  propType: $scope.propType,
                  propDataType: $scope.propDataType,
                  propValue: $scope.propValue,
                  propOptions :$scope.propOptions,
                  u_id: $scope.u_id,
                  companyId: $scope.compId,
                  propDefaultValue : $scope.propDefaultValue,
                  isValid: 1,
                  ModifiedBy: userService.getUserId()
              };
              var param = {
                  property: $scope.propertyObj,
                  sessionID: userService.getUserToken()
              }

              catalogService.savecatalogproperty(param)
                  .then(function (response) {
                      //$scope.properties = response;
                      if (response.errorMessage != '') {
                          growlService.growl(response.errorMessage, "inverse");
                      }
                      else {
                          growlService.growl("property Added Successfully.", "success");
                          //$scope.goToPropertyEdit(response.repsonseId);
                      }


                     
                  });

          }

       
                       

          //$scope.productAttribute = false;
          //$scope.productAttributebtn = function () {
          //    $scope.productAttribute = true;
          //}


          //$scope.attribute = {};

          //$scope.clickedAtrribute = {};

          

          $scope.selectAttribute = function (attribute) {
              //  console.log(attribute);
             // $scope.clickedAtrribute = attribute;
              //$scope.clickedAttrName = 'test 123';
              $scope.clickedAtrribute = [{
                  propName: 'test 345',
                  propDesc: 'test desc'
              }];
          };

          $scope.updateAttribute = function (attribute) {
              console.log('update');
          };




          //$scope.editAttribute = function (productAttribute) {
          //    $scope.addnewattributeView = true;
          //    $scope.attribute = productAttribute;

          //};

          //$scope.closeEditAttribute = function () {
          //    $scope.addnewattributeView = false;
          //    $scope.attribute = {


          //    };
          //};


         











          $scope.options = '';


          $scope.editOption = false;
          $scope.goToAddOptionBtn = function () {
              $scope.editOption = true;
          }


         

         
      }]);
