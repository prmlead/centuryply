prmApp
    // =========================================================================
    // Header
    // =========================================================================
    .controller('headerCtrl', ["$timeout", "messageService", "$scope", "userService", "$rootScope", "$interval", "Idle",
        function ($timeout, messageService, $scope, userService, $rootScope, $interval, Idle) {
            Idle.watch();
            this.openSearch = function () {
                angular.element('#header').addClass('search-toggled');
                angular.element('#top-search-wrap').find('input').focus();
            };

            $scope.showAddNewReq = function () {
                if (window.location.hash != "#/login" && userService.getUserType() == "CUSTOMER") {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.isCatalogueEnabled = function () {
                if (userService.getUserObj().isCatalogueEnabled) {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.showIsUserVendor = function () {
                if (window.location.hash != "#/login" && userService.getUserType() == "VENDOR") {
                    return true;
                } else {
                    return false;
                }
            };

            $scope.showIsSuperUser = function () {
                if (userService.getUserObj().isSuperUser) {
                    return true;
                } else {
                    return false;
                }
            };

            this.closeSearch = function () {
                angular.element('#header').removeClass('search-toggled');
            };

            // Get messages and notification for header
            this.img = messageService.img;
            this.user = messageService.user;
            this.user = messageService.text;

            /* this.messageResult = messageService.getMessage(this.img, this.user, this.text);*/


            //Clear Notification
            this.clearNotification = function ($event) {
                $event.preventDefault();

                var x = angular.element($event.target).closest('.listview');
                var y = x.find('.lv-item');
                var z = y.size();

                angular.element($event.target).parent().fadeOut();

                x.find('.list-group').prepend('<i class="grid-loading hide-it"></i>');
                x.find('.grid-loading').fadeIn(1500);
                var w = 0;

                y.each(function () {
                    var z = $(this);
                    $timeout(function () {
                        z.addClass('animated fadeOutRightBig').delay(1000).queue(function () {
                            z.remove();
                        });
                    }, w += 150);
                });

                $timeout(function () {
                    angular.element('#notifications').addClass('empty');
                }, (z * 150) + 200);
            };

            // Clear Local Storage
            this.clearLocalStorage = function () {

                //Get confirmation, if confirmed clear the localStorage
                swal({
                    title: "Are you sure?",
                    text: "All your saved localStorage values will be removed",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#F44336",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    localStorage.clear();
                    swal("Done!", "localStorage is cleared", "success");
                });
            };

            //Fullscreen View
            this.fullScreen = function () {
                //Launch
                function launchIntoFullscreen(element) {
                    if (element.requestFullscreen) {
                        element.requestFullscreen();
                    } else if (element.mozRequestFullScreen) {
                        element.mozRequestFullScreen();
                    } else if (element.webkitRequestFullscreen) {
                        element.webkitRequestFullscreen();
                    } else if (element.msRequestFullscreen) {
                        element.msRequestFullscreen();
                    }
                }

                //Exit
                function exitFullscreen() {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                }

                if (exitFullscreen()) {
                    launchIntoFullscreen(document.documentElement);
                }
                else {
                    launchIntoFullscreen(document.documentElement);
                }
            };


            $scope.tConvert = function (time) {
                // Check correct time format and split into components
                time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

                if (time.length > 1) { // If time format correct
                    time = time.slice(1);  // Remove full string match value
                    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
                    time[0] = +time[0] % 12 || 12; // Adjust hours
                }
                return time.join(' '); // return adjusted time or original string
            };

            $(".open-close").click(function () {
                $("body").toggleClass("show-sidebar");
            });

            $scope.sideBarClick = function () {
                $("body").toggleClass("show-sidebar");
            };
            
            $scope.toDoCalander = false;
            $scope.myStoryBoard = false;

            $scope.myDate = function (data) {
                data = data / 1000.0;
                var thisDate = "/Date(" + data + "000+0530)/";
                var todayDateTime = new Date();
                var timeDateStamp = new moment(thisDate).format("DD-MM-YYYY") + " " + todayDateTime.getHours() + ":" + todayDateTime.getMinutes() + ":00";
                timeDateStamp = userService.toUTCTicks(timeDateStamp) / 1000.0;
                timeDateStamp = "/Date(" + timeDateStamp + "000+0530)/";
                var params = {
                    myDate: timeDateStamp,
                    userId: userService.getUserId(),
                    userType: userService.getUserType()
                };

                params.myDate = new moment(params.myDate).format("YYYY-MM-DD");
                console.log(params);
                userService.getToDoData(params)
                    .then(function (response) {
                        if (response) {
                            console.log(response);
                            response.forEach(function (item, index) {

                                var quotationFreezTime = new moment(item.quotationFreezTime).format("DD-MM-YYYY");
                                quotationFreezTime = userService.toLocalDate(quotationFreezTime);
                                quotationFreezTime = new moment(quotationFreezTime).format("DD-MM-YYYY");

                                var startTime = new moment(item.startTime).format("DD-MM-YYYY");
                                startTime = userService.toLocalDate(startTime);
                                startTime = new moment(startTime).format("DD-MM-YYYY");

                                var todayDate = new moment(timeDateStamp).format("DD-MM-YYYY");
                                todayDate = userService.toLocalDate(todayDate);
                                todayDate = new moment(todayDate).format("DD-MM-YYYY");
                                if (quotationFreezTime == todayDate) {
                                    item.quotationFreezTime = userService.toLocalDate(item.quotationFreezTime);
                                    myTime = item.quotationFreezTime.split(" ");
                                    item.quotationFreezTime = $scope.tConvert(myTime[1]);

                                } else {
                                    item.quotationFreezTime = "";
                                }

                                if (startTime == todayDate) {
                                    item.startTime = userService.toLocalDate(item.startTime);
                                    myTime = item.startTime.split(" ");
                                    item.startTime = $scope.tConvert(myTime[1]);
                                } else {
                                    item.startTime = "";
                                }
                            });

                            $scope.records = response;
                        }
                        else {
                            console.log(response.errorMessage, "inverse");
                        }

                    });
            };

            $scope.calanderData = function () {
                $scope.myStoryBoard = false;
                $scope.toDoCalander = $scope.toDoCalander ? false : true;
                $rootScope.isCalanderLoadFirstTime++;
                Date.prototype.getUnixTime = function () { return this.getTime() / 1000 | 0 };
                console.log(new Date().getUnixTime());
                if ($scope.toDoCalander == true) {
                    $scope.myDate((new Date()).getUnixTime() * 1000);
                }
            };

            angular.element(document).on('click', function () {
                $scope.toDoCalander = false;
                //$scope.$apply();
            });

            if ($rootScope.isCalanderLoadFirstTime == 0 || $rootScope.isCalanderLoadFirstTime == 1) {
                $scope.calanderData();
            }

            $scope.myPanel = parseInt(window.innerHeight - 110);

            $scope.timesRun = 0;
            $scope.myVar = "";
            $scope.barWidth = 0;
            $scope.countNewStry = 0;
            $scope.bgImg;
            $scope.createStory = false;
            $scope.viewStory = false;

            $scope.stryArrCnt = 0;

            $scope.dataRecords = [
                { "US_ID": "1", "U_NAME": "Rathan Reddy", "US_DESCRIPTION": "Hai this is Rathan", "US_ATTACHMENT": "rabbit.jpg", "DATE_CREATED": "2019-06-11 11:30:27", "status": "0" },
                { "US_ID": "2", "U_NAME": "Jay Krishna Chari", "US_DESCRIPTION": "Hai this is Jay KrishnaHai this is Jay Krishna Hai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay KrishnaHai this is Jay Krishna", "US_ATTACHMENT": "dog.jpg", "DATE_CREATED": "2019-06-11 10:30:27", "status": "0" },
                { "US_ID": "3", "U_NAME": "Chetan Puranam", "US_DESCRIPTION": "Hay this is me", "US_ATTACHMENT": "download.jpg", "DATE_CREATED": "2019-06-10 13:30:27", "status": "0" },
                { "US_ID": "4", "U_NAME": "Meghana Reddy", "US_DESCRIPTION": "Hey this is meghana reddy", "US_ATTACHMENT": "gif.png", "DATE_CREATED": "2019-06-11 16:30:27", "status": "0" },
                { "US_ID": "5", "U_NAME": "Akshara Reddy", "US_DESCRIPTION": "Hai this is akshara reddy", "US_ATTACHMENT": "apple.png", "DATE_CREATED": "2019-06-12 11:30:27", "status": "0" },
                { "US_ID": "6", "U_NAME": "Srija", "US_DESCRIPTION": "", "US_ATTACHMENT": "fishpond.jpg", "DATE_CREATED": "2019-06-11 11:30:27", "status": "0" }
            ];

            $scope.stryArrCnt = $scope.dataRecords.length;
            $scope.zeroArrCnt = $scope.stryArrCnt;
            $scope.openNewStory = function (a) {

                $scope.myStoryBoard = true;
                $scope.toDoCalander = false;
                if (a == 1) {
                    $scope.viewStory = true;
                    $scope.createStory = false;
                    $scope.timesRun = 0;
                    $scope.barWidth = 0;
                    $scope.storyRecord = $scope.dataRecords[$scope.countNewStry];
                    $scope.dataRecords[$scope.countNewStry].status = 1;
                    $scope.bgImg = '/img/story/' + $scope.storyRecord["US_ATTACHMENT"];
                    $scope.myVar = setInterval(storyTimer, 100);
                    $scope.zeroArrCnt = 0;
                    $scope.countNewStry++;
                } else {
                    $scope.viewStory = false;
                    $scope.createStory = true;
                    clearTimeout($scope.myVar);
                }
            };

            function storyTimer() {
                $scope.timesRun++;
                $scope.barWidth = (100 / 100) * $scope.timesRun + '%';
                //console.log($scope.timesRun);
                $scope.$apply();
                if ($scope.timesRun === 101) {
                    clearTimeout($scope.myVar);
                    //console.log($scope.stryArrCnt + '--------' + $scope.countNewStry + '/////' + $scope.timesRun);
                    if ($scope.stryArrCnt === $scope.countNewStry) {
                        $scope.countNewStry = 0;
                        $scope.closeStory();

                    } else {
                        $scope.barWidth = 0;
                        $scope.$apply();
                        setTimeout(function () { $scope.openNewStory(1); }, 100);
                    }
                }
            }

            $scope.closeStory = function () {
                clearTimeout($scope.myVar);
                if ($scope.countNewStry > 0) {
                    $scope.countNewStry = $scope.countNewStry - 1;
                }
                $scope.storyRecord = "";
                $scope.bgImg = "";

                $scope.toDoCalander = false;
                $scope.myStoryBoard = false;
                $scope.barWidth = 0;
                $scope.$apply();
            };

            $scope.openSpecificStory = function (id) {
                clearTimeout($scope.myVar);

                $scope.countNewStry = id;
                $scope.timesRun = 0;
                $scope.barWidth = 0;
                $scope.openNewStory(1);
            };

            $scope.loremIpsum = "";
            $timeout(expand, 0);

            $scope.autoExpand = function (e) {
                var element = typeof e === 'object' ? e.target : document.getElementById(e);
                var scrollHeight = (element && element.scrollHeight) ? element.scrollHeight - 20 : 0; // replace 40 by the sum of padding-top and padding-bottom
                if (scrollHeight <= 310 && element) {
                    element.style.height = scrollHeight + "px";
                }
            };

            function expand() {
                $scope.autoExpand('TextArea');
            }

            console.log("print before going inside");
            if (!$scope.showAddNewReq()) {
                console.log("going inside");
                //Idle timeout  https://github.com/moribvndvs/ng-idle
                $scope.$on('IdleStart', function () {
                    // the user appears to have gone idle
                    console.log("the user appears to have gone idle");
                });

                $scope.$on('IdleWarn', function (e, countdown) {
                    // follows after the IdleStart event, but includes a countdown until the user is considered timed out
                    // the countdown arg is the number of seconds remaining until then.
                    // you can change the title or display a warning dialog from here.
                    // you can let them resume their session by calling Idle.watch()
                    console.log("the user appears to have gone idle - WARN");
                    //swal("Warning!", "You're being timed out due to inactivity. Please choose to stay signed in. Otherwise, you will be logged off automatically.", 'warning');
                    swal({
                        title: "Warning!",
                        text: "You're being timed out due to inactivity. Please choose to stay signed in. Otherwise, you will be logged off automatically.",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#F44336",
                        confirmButtonText: "Stay logged in",
                        closeOnConfirm: true
                    }, function () {
                        location.reload();
                    });
                });

                $scope.$on('IdleTimeout', function () {
                    // the user has timed out (meaning idleDuration + timeout has passed without any activity)
                    // this is where you'd log them
                    console.log("the user appears to have gone idle- TIMEOUT");
                    swal.close();
                    userService.logout().then(function () { });
                });

                $scope.$on('IdleEnd', function () {
                    // the user has come back from AFK and is doing stuff. if you are warning them, you can use this to hide the dialog
                    console.log("the user appears to have gone idle-  END");
                });

                $scope.$on('Keepalive', function () {
                    // do something to keep the user's session alive
                    console.log("the user appears to have gone idle -  KEEP ALIVE");
                    //auctionsService.getdate()
                    //    .then(function (response) {
                    //        console.log(response);
                    //    });
                });

                //^ Idle timeout  https://github.com/moribvndvs/ng-idle
            }
        }]);



    prmApp.directive('dateAsMs', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ngModelCtrl) {
                ngModelCtrl.$parsers.push(function (value) {
                    if (value && value.getTime) {
                        return value.getTime();
                    } else {
                        return value;
                    }
                });
            }
        };
    });