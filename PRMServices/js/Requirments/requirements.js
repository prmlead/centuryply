﻿prmApp.factory("PrmRequirementService", ['httpServices', 'requirementDomain', 'domain', 'userService', function (httpServices, requirementDomain, domain, userService) {

    var factory = {};
    factory.insertRequirementTemplate = function (data) {
        return httpServices.post(requirementDomain + "requirement_template/insert", data)
    }
    factory.updateRequirementTemplate = function (data) {
        return httpServices.post(requirementDomain + "requirement_template/update", data)
    }
    factory.deleteRequirementTemplate = function (DataCue) {
        return httpServices.post(requirementDomain + "requirement_template/delete", DataCue)
    }

    factory.getByPageRequirementTemplate = function (data) {
        return httpServices.post(requirementDomain + "requirement_template/getbypage", data)
    }

    factory.getRequirementTemplate = function (data) {
        return httpServices.post(requirementDomain + "requirement_template/get", data)
    }

    factory.getByIdRequirementTemplate = function (id) {
        return httpServices.get(requirementDomain + "requirement_template/getbyid/" + id)
    }



    return factory;

}]);

prmApp.controller("RequirementTemplateController", ['$uibModal', '$state', 'PrmRequirementService', '$scope', 'SettingService', 'store', 'growlService', 'fileReader', 'realTimePriceService', 'AngularUtility', '$stateParams', 'PrmMasterService',
   function ($uibModal, $state, _prmRequirementService, $scope, SettingService, store, growlService, fileReader, realTimePriceService, AngularUtility, $stateParams, PrmMasterService) {

       var _requirementTemplateCtrl = this;
       _requirementTemplateCtrl.createupdateview = function () {
           return "views/Requirements/Templates/createorupdate.html"
       }
       _requirementTemplateCtrl.gridOpitions = {
           itemsByPage: 20,
           pageSizeOptions: [10, 20, 50, 100],
           displayedPages: 7,
           showFilter: false,
           data: [],
           totalItems: 0,
           loading: false,
           noData: true,
           searchTerm: "",
           enableFilter: true,
           columns: [

           ],
       }

       _requirementTemplateCtrl.RequirementTemplateModel = {
           AllowedDepartments: [],
           SelectedDepartments: [],
       }

       _requirementTemplateCtrl.UserList = [];
       _requirementTemplateCtrl.DepartmentList = [];
       _requirementTemplateCtrl.getDeparments = function () {
           PrmMasterService.getCompanyDepartments().then(function (response) {
               _requirementTemplateCtrl.DepartmentList = response;
               angular.forEach(_requirementTemplateCtrl.DepartmentList, function (item) {
                   var index = _requirementTemplateCtrl.RequirementTemplateModel.AllowedDepartments.indexOf(item.deptID);
                   if (index !== -1) {
                       item.Selected = true;
                   }
               })


           })
       };

       _requirementTemplateCtrl.getCompanyusers = function () {
           _prmRequirementService.getCompanyusers().then(function (response) {
               _requirementTemplateCtrl.UserList = response;
               var allowedUser = [];
               angular.forEach(_requirementTemplateCtrl.UserList, function (item) {
                   var index = _requirementTemplateCtrl.RequirementTemplateModel.AllowedUsers.indexOf(parseInt(item.userID));
                   if (index !== -1) {
                       item.Selected = true;
                   }
               });
           })
       };



       _requirementTemplateCtrl.tableState = {};
       _requirementTemplateCtrl.getByPage = function (table) {
           if (table == undefined)
               return false;

           _requirementTemplateCtrl.tableState = table;

           var pager = AngularUtility.preparePager(table, _requirementTemplateCtrl.gridOpitions.columns);

           _prmRequirementService.getByPageRequirementTemplate(pager).then(function (response) {
               var _jsonResposne = JSON.parse(response)
               if (_jsonResposne.Data != undefined) {
                   _requirementTemplateCtrl.gridOpitions.data = _jsonResposne.Data;
                   _requirementTemplateCtrl.gridOpitions.totalItems = _jsonResposne.TotalCount;
                   table.pagination.totalItemCount = _jsonResposne.TotalCount;
                   table.pagination.numberOfPages = _jsonResposne.TotalPages;
                   _requirementTemplateCtrl.gridOpitions.loading = false;
               }
               else {
                   _requirementTemplateCtrl.gridOpitions.data = [];
               }


           })
       }

       _requirementTemplateCtrl.getById = function () {
           _prmRequirementService.getByIdRequirementTemplate($stateParams.id).then(function (response) {
               if (response != null) {
                   _requirementTemplateCtrl.RequirementTemplateModel = response;
               }

           })
       }

       _requirementTemplateCtrl.getRequirementTemplate = function () {
           var data = {
               Departments: [67],
               Projects: []
           }
           _prmRequirementService.getRequirementTemplate(data).then(function (response) {

               console.log(response)
           })
       }

       _requirementTemplateCtrl.getRequirementTemplate();


       _requirementTemplateCtrl.Create = function (form) {
           form.$submitted = true;
           if (form.$valid) {

               var model = angular.copy(_requirementTemplateCtrl.RequirementTemplateModel);

               if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                   growlService.growl("please select aleast one department", "inverse");
                   return false;
               }





               if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                   model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

               if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                   model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

               _prmRequirementService.insertRequirementTemplate(model).then(function (response) {
                   if (response.errorMessage == "") {
                       _requirementTemplateCtrl.backTo();

                       growlService.growl(response.message, "inverse");
                   }
                   else {
                       growlService.growl(response.message, "inverse");
                   }
               })
           }
       }

       _requirementTemplateCtrl.Update = function (form) {
           form.$submitted = true;
           if (form.$valid) {

               var model = angular.copy(_requirementTemplateCtrl.RequirementTemplateModel);

               if (model.LimitedToDepartment && model.SelectedDepartments.length == 0) {
                   growlService.growl("please select aleast one department", "inverse");
                   return false;
               }


               if (model.SelectedUsers != [] && model.SelectedUsers != undefined)
                   model.AllowedUsers = model.SelectedUsers.map(function (item) { return item.userID })

               if (model.SelectedDepartments != [] && model.SelectedDepartments != undefined)
                   model.AllowedDepartments = model.SelectedDepartments.map(function (item) { return item.deptID })

               _prmRequirementService.updateRequirementTemplate(model).then(function (response) {
                   if (response.errorMessage == "") {
                       _requirementTemplateCtrl.backTo();

                       growlService.growl(response.message, "inverse");
                   }
                   else {
                       growlService.growl(response.message, "inverse");
                   }
               })
           }
       }

       _requirementTemplateCtrl.Delete = function (row) {
           _prmRequirementService.deleteRequirementTemplate({ id: row.Id }).then(function (response) {
               if (response.errorMessage == "") {
                   _requirementTemplateCtrl.getByPage(_requirementTemplateCtrl.tableState)
               }
               else {
                   growlService.growl(response.message, "inverse");
               }
           })
       }

       _requirementTemplateCtrl.edit = function (row) {
           $state.go("pages.requirement_template_edit", { id: row.Id })
       }
       _requirementTemplateCtrl.backTo = function () {
           $state.go("pages.requirement_template")
       }

       _requirementTemplateCtrl.add = function () {
           $state.go("pages.requirement_template_add")
       }
   }]);
