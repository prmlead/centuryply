prmApp
    .service('reportingService', ["reportingDomain", "userService", "httpServices", "$window", function (reportingDomain, userService, httpServices, $window) {
        //var domain = 'http://182.18.169.32/services/';
        var reportingService = this;

        reportingService.getLiveBiddingReport = function (reqID) {
            let url = reportingDomain + 'getlivebiddingreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getItemWiseReport = function (reqID) {
            let url = reportingDomain + 'getitemwisereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getDeliveryDateReport = function (reqID) {
            let url = reportingDomain + 'deliverytimelinereport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getPaymentTermsReport = function (reqID) {
            let url = reportingDomain + 'paymenttermsreport?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetReqDetails = function (reqID) {
            let url = reportingDomain + 'getreqdetails?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        reportingService.downloadQcsTemplate = function (template, reqid, qcsID,userID) {
            let url = reportingDomain + 'GetQcsReport?template=' + template + '&reqid=' + reqid + '&qcsID=' + qcsID + '&userID=' + userID + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };


        reportingService.downloadTemplate = function (template, userid, reqid) {
            let url = reportingDomain + 'gettemplates?template=' + template + '&userid=' + userid + '&reqid=' + reqid + '&compID=' + userService.getUserCompanyId() + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.downloadConsolidatedTemplate = function (template, from, to, userid) {
            let url = reportingDomain + 'getconsolidatedtemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.GetUserLoginTemplates = function (template, from, to, userid) {
            let url = reportingDomain + 'GetUserLoginTemplates?template=' + template + '&from=' + from + '&to=' + to + '&userid=' + userid + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        reportingService.GetLogsTemplates = function (template, from, to, companyID) {
            let url = reportingDomain + 'GetLogsTemplates?template=' + template + '&from=' + from + '&to=' + to + '&companyID=' + companyID + '&sessionid=' + userService.getUserToken();
            let data = httpServices.get(url).then(function (response) {
                if (response) {
                    response = b64toBlob(response, "application/ vnd.openxmlformats - officedocument.spreadsheetml.sheet");
                    var linkElement = document.createElement('a');
                    try {
                        //var blob = b64toBlob(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//new Blob([response], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var url = $window.URL.createObjectURL(response);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", template + ".xlsx");

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                    catch (ex) { }
                    return response;
                }
            }, function (result) {
                //console.log("date error");
            });
        };

        function b64toBlob(b64Data, contentType, sliceSize) {
            contentType = contentType || '';
            sliceSize = sliceSize || 512;

            var byteCharacters = atob(b64Data);
            var byteArrays = [];

            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                var slice = byteCharacters.slice(offset, offset + sliceSize);

                var byteNumbers = new Array(slice.length);
                for (var i = 0; i < slice.length; i++) {
                    byteNumbers[i] = slice.charCodeAt(i);
                }

                var byteArray = new Uint8Array(byteNumbers);

                byteArrays.push(byteArray);
            }

            var blob = new Blob(byteArrays, { type: contentType });
            return blob;
        }

        reportingService.GetReqReportForExcel = function (reqid, sessionid) {
            let url = reportingDomain + 'getreqreportforexcel?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.GetReqItemWiseVendors = function (reqid, sessionid) {
            let url = reportingDomain + 'getReqItemWiseVendors?reqid=' + reqid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.getConsolidatedReport = function (fromDate, toDate) {
            let url = reportingDomain + 'getconsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getLogisticConsalidatedReport = function (fromDate, toDate) {
            let url = reportingDomain + 'getLogisticConsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.getconsolidatedbasepricereport = function (fromDate, toDate) {
            let url = reportingDomain + 'getconsolidatedbasepricereports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetAccountingConsolidatedReports = function (fromDate, toDate) {
            let url = reportingDomain + 'getAccountingConsolidatedreports?from=' + fromDate + '&to=' + toDate + '&userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        //#region QCS

        reportingService.GetQCSDetails = function (params) {
            let url = reportingDomain + 'qcsdetails?qcsid=' + params.qcsid + '&sessionid=' + params.sessionid;
            return httpServices.get(url);
        };

        reportingService.GetQcsAuditDetails = function (params) {
            let url = reportingDomain + 'GetQcsAuditDetails?reqID=' + params.reqID + '&qcsID=' + params.qcsID + '&verID=' + params.verID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQCSList = function (params) {
            let url = reportingDomain + 'getqcslist?uid=' + params.uid + '&reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GetQCSIDS = function (params) {
            let url = reportingDomain + 'GetQCSIDS?reqid=' + params.reqid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.GeQcsAudit = function (params) {
            let url = reportingDomain + 'GeQcsAudit?reqID=' + params.reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.SaveQCSDetails = function (params) {
            let url = reportingDomain + 'saveqcsdetails';
            return httpServices.post(url, params);
        };

        reportingService.GetNewQuotations = function (params) {
            let url = reportingDomain + 'GetNewQuotations';
            return httpServices.post(url, params);
        };

        //#endregion QCS

        //#region RM QCS

        reportingService.GetRMQCSReportForExcel = function (reqid, itemID, sessionid) {
            let url = reportingDomain + 'getrmqcsreportforexcel?reqid=' + reqid + '&itemID=' + itemID + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        reportingService.GetRMQCSDetails = function (params) {
            let url = reportingDomain + 'getrmqcsdetails?qcsid=' + params.qcsid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.SaveRMQCSDetails = function (params) {
            let url = reportingDomain + 'savermqcsdetails';
            return httpServices.post(url, params);
        };

        reportingService.GetCompanySavingStats = function (params) {
            let url = reportingDomain + 'getcompanysavingstats?compid=' + params.compid + '&fromdate=' + params.fromDate + '&todate=' + params.toDate + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        reportingService.DeleteQcs = function (params) {
            let url = reportingDomain + 'DeleteQcs';
            return httpServices.post(url, params);
        };

        reportingService.ActivateQcs = function (params) {
            let url = reportingDomain + 'ActivateQcs';
            return httpServices.post(url, params);
        };

        reportingService.GetQcsPRDetails = function (params) {
            let url = reportingDomain + 'GetQcsPRDetails?reqID=' + params.reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        //#endregion RM QCS

        return reportingService;
    }]);