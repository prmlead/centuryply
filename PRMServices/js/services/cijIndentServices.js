prmApp

    .service('cijIndentService', ["cijindentDomain", "userService", "httpServices", function (cijindentDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var cijIndentService = this;
        
        //storeService.savestore = function (params) {
        //    let url = storeDomain + 'savestore';
        //    return httpServices.post(url, params);
        //};

        cijIndentService.EditCijIndentDetails = function (params) {

            var url = cijindentDomain + 'editcijindentdetails';
            return httpServices.post(url, params);
        };
        cijIndentService.GetCIJList = function (userid, sessionid) {

            var url = cijindentDomain + 'getcijlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.deleteCIJ = function (params) {
            var url = cijindentDomain + 'deletecij';
            return httpServices.post(url, params);
        };

        cijIndentService.saveCIJ = function (params) {
            var url = cijindentDomain + 'savecij';
            return httpServices.post(url, params);
        };
        cijIndentService.GetCIJ = function (cijid, sessionid) {

            var url = cijindentDomain + 'getcij?cijid=' + cijid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.GetCijItems = function (id, sessionid) {

            var url = cijindentDomain + 'getcijitems?id=' + id + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.GetIndentList = function (userid, sessionid) {

            var url = cijindentDomain + 'getindentlist?userid=' + userid + '&sessionid=' + sessionid;
            return httpServices.get(url);
        };

        cijIndentService.deleteIndent = function (params) {
            var url = cijindentDomain + 'deleteindent';
            return httpServices.post(url, params);
        };

        cijIndentService.SaveIndentDetails = function (params) {
            var url = cijindentDomain + 'saveindentdetails';
            return httpServices.post(url, params);
        };

        cijIndentService.saveFileUpload = function (params) {
            var url = cijindentDomain + 'savefileupload';
            return httpServices.post(url, params);
        };


        cijIndentService.GetSeries = function (series, seriestype, deptid) {

            var url = cijindentDomain + 'getseries?series=' + series + '&seriestype=' + seriestype + '&compid=' + userService.getUserCompanyId() + '&deptid=' + deptid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        cijIndentService.GetItemsList = function (moduleid, module, sessionid) {
           
                var url = cijindentDomain + 'getitemslist?moduleid=' + moduleid + '&module=' + module + '&sessionid=' + sessionid;
                return httpServices.get(url);
        };

        cijIndentService.GetIndentDetails = function (indentID, sessionid) {
           
                var url = cijindentDomain + 'getindentdetails?indentid=' + indentID + '&sessionid=' + sessionid;
                return httpServices.get(url);
            };
        
        return cijIndentService;
    }]);