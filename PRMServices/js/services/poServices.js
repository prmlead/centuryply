prmApp
    .service('poService', ["poDomain", "userService", "httpServices", function (poDomain, userService, httpServices) {
        //var domain = 'http://182.18.169.32/services/';
        var poService = this;

        poService.getpoinformation = function (reqID) {
            let url = poDomain + 'getpoinformation?userid=' + userService.getUserId() + '&reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.savepoinfo = function (params) {
            let url = poDomain + 'savepoinfo';
            return httpServices.post(url, params);
        };

        poService.getdespoinfo = function (reqID) {
            let url = poDomain + 'getdespoinfo?userid=' + userService.getUserId() + '&reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.GetVendors = function (reqID) {
            let url = poDomain + 'getvendors?&reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.saveDescPoInfo = function (params) {
            let url = poDomain + 'savedescpoinfo';
            return httpServices.post(url, params);
        };

        poService.UpdatePOStatus = function (params) {
            let url = poDomain + 'updatepostatus';
            return httpServices.post(url, params);
        };

        poService.GetDescDispatch = function (poid, dtid) {
            let url = poDomain + 'getdescdispatch?poid=' + poid + '&dtid=' + dtid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SaveDesDispatchTrack = function (params) {
            let url = poDomain + 'savedesdispatchtrack';
            return httpServices.post(url, params);
        };

        poService.GetDispatchTrackList = function (poid, dTID) {
            let url = poDomain + 'getdispatchtracklist?poid=' + poid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.getDispatchTrack = function (poid, dTID) {
            let url = poDomain + 'getdispatchtrack?poid=' + poid + '&dtid=' + dTID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SaveDispatchTrack = function (params) {
            let url = poDomain + 'savedispatchtrack';
            return httpServices.post(url, params);
        };

        poService.GetVendorPoList = function (reqID, vendorID, poid) {
            let url = poDomain + 'getvendorpolist?reqid=' + reqID + '&userid=' + vendorID + '&poid=' + poid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.GetVendorPoInfo = function (reqID, vendorID, poid) {
            let url = poDomain + 'getvendorpoinfo?reqid=' + reqID + '&userid=' + vendorID + '&poid=' + poid + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SaveVendorPOInfo = function (params) {
            let url = poDomain + 'savevendorpoinfo';
            return httpServices.post(url, params);
        };

        poService.GetPaymentTrack = function (vendorID, poID) {
            let url = poDomain + 'getPaymentTrack?&vendorid=' + vendorID + '&poid=' + poID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        poService.SavePaymentInfo = function (params) {
            let url = poDomain + 'savepaymentinfo';
            return httpServices.post(url, params);
        };

        poService.CheckUniqueIfExists = function (params) {
            let url = poDomain + 'checkuniqueifexists';
            return httpServices.post(url, params);
        };

        poService.SavePOVendorQuantityAck = function (params) {
            let url = poDomain + 'savepovendorquantityack';
            return httpServices.post(url, params);
        };

        return poService;
    }]);