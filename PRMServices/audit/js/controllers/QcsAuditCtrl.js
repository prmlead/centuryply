prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('QcsAuditCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService",
        "$http", "$window", "domain", "fileReader", "growlService", "$log", "$filter", "ngDialog", "auditreportingServices", "reportingService",
        function ($state, $stateParams, $scope, auctionsService, userService,
            $http, $window, domain, fileReader, growlService, $log, $filter, ngDialog, auditreportingServices, reportingService) {
            $scope.auditLogs = [];
            $scope.reqId = $stateParams.reqID;
            $scope.requirementDetails = {};
            $scope.auditData = [];
            $scope.auditDetails = [];
            $scope.filteredDetails = [];
            $scope.QCSAuditList = [];

            $scope.GeQcsAudit = function () {
                var params = {

                    "reqID": $scope.reqId,
                    "sessionid": userService.getUserToken()
                };
                reportingService.GeQcsAudit(params)
                    .then(function (response) {
                        $scope.QCSAuditList = response;
                        $scope.QCSAuditList.forEach(function (qcs, qcsIndex) {
                            qcs.CREATED_DATE = userService.toLocalDate(qcs.CREATED_DATE);
                        });

                    });
            };

            $scope.GeQcsAudit();


            $scope.goToAuditDomesticQCS = function (reqID,qcsID,verID,wfID) {
                
                var url = $state.href("cost-comparisions-qcs-audit", { "reqID": reqID, "qcsID": qcsID, "verID": verID, "wfID": wfID });
                window.open(url, '_self');
            };

            

            $scope.getAuditDetails = function (audit) {
                $scope.auditData.forEach(function (audit1, vI) {
                    audit1.expanded = false;
                });

                audit.expanded = true;
                $scope.filteredDetails = _.filter($scope.auditDetails, function (detail) {
                    return detail.auditVersion === audit.auditVersion;
                });

                if ($scope.filteredDetails) {
                    $scope.filteredDetails.forEach(function (val, vI) {
                        val.dateCreated1 = userService.toLocalDate(val.dateCreated);
                    });
                } else {
                    $scope.filteredDetails = [];
                }
            };

            

            $scope.getRequirementDetails = function () {
                auctionsService.getrequirementdata({ "reqid": $scope.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
                    .then(function (response) {
                        if (response) {
                            $scope.requirementDetails = response;
                        }
                    });
            };
            
            $scope.getRequirementDetails();

        }]);