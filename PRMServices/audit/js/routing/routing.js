﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
            .state('audit', {
                url: '/audit/:reqID',
                templateUrl: 'audit/views/Audit.html'
            })

                .state('QcsAudit', {
                    url: '/QcsAudit/:reqID',
                    templateUrl: 'audit/views/QcsAudit.html'
                })

                .state('cost-comparisions-qcs-audit', {
                    url: '/cost-comparisions-qcs-audit/:reqID/:qcsID/:verID/:wfID',
                    templateUrl: 'audit/views/cost-comparisions-qcs-audit.html'
                })

        }]);