﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class Audit : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string tableName = string.Empty;
        [DataMember(Name = "tableName")]
        public string TableName
        {
            get
            {
                if (!string.IsNullOrEmpty(tableName))
                { return tableName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { tableName = value; }
            }
        }

        string columnName = string.Empty;
        [DataMember(Name = "columnName")]
        public string ColumnName
        {
            get
            {
                if (!string.IsNullOrEmpty(columnName))
                { return columnName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { columnName = value; }
            }
        }

        string columnNameDesc = string.Empty;
        [DataMember(Name = "columnNameDesc")]
        public string ColumnNameDesc
        {
            get
            {
                if (!string.IsNullOrEmpty(columnNameDesc))
                { return columnNameDesc; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { columnNameDesc = value; }
            }
        }

        string oldValue = string.Empty;
        [DataMember(Name = "oldValue")]
        public string OldValue
        {
            get
            {
                if (!string.IsNullOrEmpty(oldValue))
                { return oldValue; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { oldValue = value; }
            }
        }

        string newValue = string.Empty;
        [DataMember(Name = "newValue")]
        public string NewValue
        {
            get
            {
                if (!string.IsNullOrEmpty(newValue))
                { return newValue; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { newValue = value; }
            }
        }

        string auditBy = string.Empty;
        [DataMember(Name = "auditBy")]
        public string AuditBy
        {
            get
            {
                if (!string.IsNullOrEmpty(auditBy))
                { return auditBy; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { auditBy = value; }
            }
        }

        string actionType = string.Empty;
        [DataMember(Name = "actionType")]
        public string ActionType
        {
            get
            {
                if (!string.IsNullOrEmpty(actionType))
                { return actionType; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { actionType = value; }
            }
        }

        DateTime dateCreated = DateTime.MaxValue;
        [DataMember(Name = "dateCreated")]
        public DateTime? DateCreated
        {
            get
            {
                return dateCreated;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    dateCreated = value.Value;
                }
            }
        }

        string moreDetails = string.Empty;
        [DataMember(Name = "moreDetails")]
        public string MoreDetails
        {
            get
            {
                if (!string.IsNullOrEmpty(moreDetails))
                { return moreDetails; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { moreDetails = value; }
            }
        }

        string auditVersion = string.Empty;
        [DataMember(Name = "auditVersion")]
        public string AuditVersion
        {
            get
            {
                if (!string.IsNullOrEmpty(auditVersion))
                { return auditVersion; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { auditVersion = value; }
            }
        }

        string auditComments = string.Empty;
        [DataMember(Name = "auditComments")]
        public string AuditComments
        {
            get
            {
                if (!string.IsNullOrEmpty(auditComments))
                { return auditComments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { auditComments = value; }
            }
        }
    }
}