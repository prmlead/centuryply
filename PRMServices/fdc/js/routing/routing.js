﻿prmApp
    .config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "$provide", "domain", "version",
        function ($stateProvider, $urlRouterProvider, $httpProvider, $provide, domain, version) {
            $stateProvider
            .state('view-fwd-req', {
                url: '/view-fwd-req/:Id',
                templateUrl: 'forward/views/viewFwdReq.html'
            })

        }]);