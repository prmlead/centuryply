﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SAPIntegration.CenturyPOCreation1;

namespace SAPIntegration
{
    class Program
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static string _archiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"].ToString();
        private static string _errorFolder = ConfigurationManager.AppSettings["ErrorFolder"].ToString();
        private static string _stageFolder = ConfigurationManager.AppSettings["StageFolder"].ToString();
        private static string _processFolder = ConfigurationManager.AppSettings["ProcessFolder"].ToString();
        //private static string _erpVendFolder = ConfigurationManager.AppSettings["ErpVendorFolder"].ToString();

        static void Main(string[] args)
        {
            char csvDelimiter = ';';
            string sheetName = string.Empty;
            DataTable excelData = new DataTable();

            string[] filePaths = Directory.GetFiles(_stageFolder);


            if (filePaths != null && filePaths.Length > 0)
            {
                foreach (var filepath in filePaths)
                {
                    if (File.Exists(filepath))
                    {
                        var ticks = DateTime.Now.Ticks;
                        string processFolder = _processFolder + "\\" + Path.GetFileName(filepath);
                        processFolder = processFolder.Replace(".xls", ".csv");

                        string archiveFolder = _archiveFolder + "\\" + Path.GetFileName(filepath);
                        archiveFolder = archiveFolder.Replace(".xls", ".csv");

                        string errorFolder = _errorFolder + "\\" + Path.GetFileName(filepath);
                        errorFolder = errorFolder.Replace(".xls", ".csv");

                        try
                        {

                            File.Move(filepath, processFolder);

                            string fileName = Path.GetFileName(processFolder);
                            var dt = Helper.ReadCsvFile(processFolder, csvDelimiter, fileName);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                PostData(dt, "ERP_PR_STAGE", fileName.Replace(".csv", "_" + ticks + ".csv"));
                            }

                            archiveFolder = archiveFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, archiveFolder);
                        }
                        catch (Exception ex)
                        {
                            errorFolder = errorFolder.Replace(".csv", "_" + ticks + ".csv");
                            File.Move(processFolder, errorFolder);
                            logger.Error(ex, Path.GetFileName(filepath));
                        }
                    }
                }
            }

            //CreatePO();
        }

        private static void PostData(DataTable dt, string tableName, string fileName)
        {
            string filePath = @"json\" + tableName + ".json";
            try
            {
                if (!System.IO.File.Exists(filePath))
                {
                    Console.WriteLine("not found: " + filePath);
                    return;
                }

                string rowJson = System.IO.File.ReadAllText(filePath);
                var drList = dt.AsEnumerable().ToList();
                var bathes = drList.ChunkBy(Convert.ToInt32(ConfigurationManager.AppSettings["BATCH_SIZE"]));

                foreach (var batch in bathes)
                {
                    string bodyText = string.Empty;
                    foreach (DataRow dr in batch)
                    {
                        string rowJsonModified = rowJson;
                        foreach (DataColumn column in dr.Table.Columns)
                        {
                            if (column.DataType == typeof(DateTime))
                            {
                                var value = GetValue(column, dr).Replace(@"""", @"\""");
                                var temp = value.Split('/').ToList();
                                if (temp[0].Length == 1 && !temp[0].StartsWith("0"))
                                {
                                    temp[0] = "0" + temp[0];
                                }

                                if (temp[1].Length == 1 && !temp[1].StartsWith("0"))
                                {
                                    temp[1] = "0" + temp[1];
                                }
                                value = string.Join("/", temp);

                                rowJsonModified = rowJsonModified.Replace($@"${column.ColumnName}$", value);
                            }
                            else
                            {
                                rowJsonModified = rowJsonModified.Replace($@"${column.ColumnName}$", GetValue(column, dr).Replace(@"""", @"\"""));
                            }
                        }

                        bodyText += rowJsonModified + ",";
                    }

                    bodyText = bodyText.Trim(',');
                    string json = "{   \"data\":[ BODYTEXT ] }  ".Replace("BODYTEXT", bodyText);
                    logger.Debug("tableName JSON:" + json);
                    string url = ConfigurationManager.AppSettings["PRM_JP_API"];
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url + "/" + tableName);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        logger.Debug("tableName Response:" + result);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Debug("TableName:" + tableName + "--ERROR POSTING:" + ex.Message);
                logger.Error(ex);
                throw new Exception("Error sending data to PRM", ex);
            }
        }


        private static string GetValue(DataColumn column, DataRow dr)
        {
            string value = string.Empty;
            if (column.DataType == typeof(int) || column.DataType == typeof(decimal) || column.DataType == typeof(double))
            {
                if (dr[column.ColumnName] == null || dr[column.ColumnName].ToString().Trim() == string.Empty)
                {
                    if (column.ColumnName.Equals("PR_NUMBER") || column.ColumnName.Equals("MATERIAL_CODE"))
                    {
                        throw new Exception("Required fields are not present");
                    }
                    value = "0";
                }
                else
                {
                    value = dr[column.ColumnName].ToString().Trim();
                }
            }
            else
            {
                value = dr[column.ColumnName].ToString().Trim();
            }

            return value;
        }


        public static void CreatePO()
        {
            try
            {
                SI_PurchaseOrder_OBService request = new SI_PurchaseOrder_OBService();
                request.Url = ConfigurationManager.AppSettings["CreatePO"].ToString();
                NetworkCredential netCredential = new NetworkCredential(ConfigurationManager.AppSettings["UserId"].ToString(), ConfigurationManager.AppSettings["UserPwd"].ToString());
                Uri uri = new Uri(ConfigurationManager.AppSettings["CreatePO"].ToString());
                ICredentials credentials = netCredential.GetCredential(uri, "Basic");
                request.Credentials = credentials;
                request.PreAuthenticate = true;
                request.Timeout = 240000;
                DT_PRM360_PurchaseOrder_Request payloadReq = new DT_PRM360_PurchaseOrder_Request();
                DT_PRM360_PurchaseOrder_RequestHeader payload = new DT_PRM360_PurchaseOrder_RequestHeader();
                payload.DOC_TYPE = "ZNN";
                payload.COMP_CODE = "";
                payload.CURRENCY = "INR";
                payload.VENDOR = "0001000001";
                payload.PMNTTRMS = "0001";
                payload.PO_REL_IND = "X";
                //payload.PURCH_ORG = "";
                payload.PUR_GROUP = "205";
                payload.INCOTERMS1 = "CFR";
                List<DT_PRM360_PurchaseOrder_RequestHeaderItem> items = new List<DT_PRM360_PurchaseOrder_RequestHeaderItem>();
                payload.Item = items.ToArray();

                DT_PRM360_PurchaseOrder_RequestHeaderItem item = new DT_PRM360_PurchaseOrder_RequestHeaderItem();
                item.SHORT_TEXT = "SPRING FOR QUICK MARKER";
                item.ITEM_CAT = "G06008";
                item.MATL_GROUP = "G06008";
                item.MATERIAL = "G06008000015";

                item.PLANT = "2200";
                item.PO_ITEM = "00010";
                item.PREQ_ITEM = "0010572423";
                item.QUANTITY = "1";
                item.DELIVERY_DATE = "2022-12-12";
                item.NET_PRICE = "100.00";
                item.PO_UNIT = "PC";

                payloadReq.Header = payload;
                var temp = request.SI_PurchaseOrder_OB(payloadReq);
                if (temp != null && temp.Count() > 0)
                {
                    logger.Info("Found entries: " + temp.Count());
                    logger.Info("EBELN: " + temp[0].EBELN);
                    logger.Info("GRN_NO: " + temp[0].GRN_NO);
                    logger.Info("MESSAGE: " + temp[0].MESSAGE);
                    logger.Info("MSGTY: " + temp[0].MSGTY);
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }            
        }
    }

    public static class ListExtensions
    {
        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }
    }
}
