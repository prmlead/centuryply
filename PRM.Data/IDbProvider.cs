﻿using System.Data;

namespace PRM.Data
{
    public interface IDbProvider
    {

        IDbConnection Connection(string connectionString = "", string providerName = "");
    }
}