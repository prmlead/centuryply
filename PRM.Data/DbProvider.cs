﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Data
{
    public class DbProvider  : IDbProvider, IDisposable
    {

        private string _connectionString ;
        private DbConnection _connection;
        //private DbProviderFactory _provider;
        private string _providerName;
        public DbProvider()
        {

        }

        public IDbConnection Connection(string connectionString = "",string providerName = "")
        {

            try
            {
                _connectionString = string.IsNullOrEmpty(connectionString) ? ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ConnectionString : connectionString;
                _providerName = string.IsNullOrEmpty(providerName) ? ConfigurationManager.ConnectionStrings["MySQLConnectionString"].ProviderName : providerName;
                //_provider = DbProviderFactories.GetFactory(_providerName);
                _connection = new MySqlConnection(_connectionString);
                _connection.Open();
                return _connection;
            }
            catch(MySqlException ex)
            {
                _connection.Close();

                return _connection;
            }

        }

        public void Dispose()
        {
            if (_connection != null)
                _connection.Close();
        }
    }
}
