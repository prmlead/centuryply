﻿using DAILYREPORT.PRMVENDJOBService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAILYREPORT
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static bool sendEmail = Convert.ToBoolean(ConfigurationManager.AppSettings["SEND_EMAIL"].ToString());
        private static bool sendSms = Convert.ToBoolean(ConfigurationManager.AppSettings["SEND_SMS"].ToString());
        private static string userId = Convert.ToString(ConfigurationManager.AppSettings["USER_ID"].ToString());
        private static string bodyContent = Convert.ToString(ConfigurationManager.AppSettings["BODY_CONTENT"].ToString());
        private static int dateDiff = Convert.ToInt32(ConfigurationManager.AppSettings["DATE_DIFF"].ToString());
        static void Main(string[] args)
        {
            DateTime thisDay = DateTime.Today;
            try
            {
                PRMVENDJOBServiceClient service = new PRMVENDJOBServiceClient();

                //DateTime date = DateTime.Now.AddDays(-1);
                var reminders = service.GetPendingNegotiationsReport("PENDING_NEGOTIATIONS_REPORT",DateTime.UtcNow,DateTime.UtcNow, userId, sendEmail, sendSms,bodyContent, dateDiff);

                if (reminders > 0)
                {
                    logger.Debug("DAILY REPORT JOB SUCCESSFULLY RAN ON>>>>" + thisDay.ToString("D") + "With Records Of Length>>>>" + reminders);
                }
                else
                {
                    logger.Debug("DAILY REPORT JOB SUCCESSFULLY RAN ON>>>>" + thisDay.ToString("D") + " " + "But Doesn't Have Any Records To Send Email");
                }
            }
            catch (Exception ex)
            {
                logger.Error("DAILY REPORT JOB DID Not RUN ON>>>>" + thisDay.ToString("D") + " " + " Because of " + ex.Message);
            }
        }
    }
}
