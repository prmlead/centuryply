﻿using System;
using System.Configuration;
using JOBS.PRMVENDJOBService;

namespace JOBS
{
    class Program
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private static bool sendEmail = Convert.ToBoolean(ConfigurationManager.AppSettings["SEND_EMAIL"].ToString());
        private static bool sendSms = Convert.ToBoolean(ConfigurationManager.AppSettings["SEND_SMS"].ToString());
        private static int userId = Convert.ToInt32(ConfigurationManager.AppSettings["USER_ID"].ToString());
        private static int timeGap = Convert.ToInt32(ConfigurationManager.AppSettings["TIME_GAP"].ToString());
        static void Main(string[] args)
        {
            DateTime thisDay = DateTime.Today;
            try
            {
                PRMVENDJOBServiceClient service = new PRMVENDJOBServiceClient();
                var reminders = service.RemindVendorsBeforeNeg30min(sendEmail, sendSms, userId, DateTime.UtcNow, timeGap);
                if (reminders > 0)
                {
                    logger.Debug("REMIND VENDORS JOB SUCCESSFULLY RAN ON>>>>" + thisDay.ToString("D") + "With Records Of Length>>>>" + reminders);
                }
                else
                {
                    logger.Debug("REMIND VENDORS JOB SUCCESSFULLY RAN ON>>>>" + thisDay.ToString("D") + " " + "But Doesn't Have Any Records To Send Email");
                }
            }
            catch (Exception ex)
            {
                logger.Error("REMIND VENDORS JOB DID Not RUN ON>>>>" + thisDay.ToString("D") + " " + " Because of " + ex.Message);
            }

        }
    }
}
