﻿using PRM.Core.Domain.Logistics;
using System.Collections;
using System.Collections.Generic;

namespace PRM.Services.Logistics
{
    public interface ILogisticQuotationService
    {
        IList<LogisticQuotationReport> GetLogisticQuotationByRequrimentId(int requirment);

        void UpdateQuotationCharges(LogisticQuotationReport entity);


    }
}