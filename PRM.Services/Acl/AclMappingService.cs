﻿using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Core.Pagers;
using PRM.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Acl
{
    public class AclMappingService     : IAclMappingService
    {
        public AclMappingService(IRepository<AclMapping> aclMappingRep,
        IDbProvider dbProvider)
        {
            _aclMappingRep = aclMappingRep;
            _dbProvider = dbProvider;
        }

        private readonly IRepository<AclMapping> _aclMappingRep;
        private readonly IDbProvider _dbProvider;
        public void DeleteAclMapping(AclMapping entity)
        {
            _aclMappingRep.Delete(entity);
        }

        public AclMapping GetAclMappingById(int id)
        {

            return _aclMappingRep.GetById(id);
        }

        public IPagedList<AclMapping> GetAclMappingList(Pager pager, int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM master_AclMapping";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if (!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Tttle Like %{0}%", pager.Criteria.SearchTerm));
                    WhereColumns.Add(string.Format(" Details Like %{0}%", pager.Criteria.SearchTerm));


                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<AclMapping>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<AclMapping>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<AclMapping> GetAclMappingList(object filter)
        {
            return _aclMappingRep.GetList(filter).ToList();
        }

        public void InsertAclMapping(AclMapping entity)
        {
            _aclMappingRep.Insert(entity);
        }

        public void UpdateAclMapping(AclMapping entity)
        {
            _aclMappingRep.Update(entity);
        }

        public void InsertDepartmentMapping(int companyId, string entityName, int entityId, int[] deparmentIds)
        {
            if(deparmentIds.Any())
            foreach (var dep in deparmentIds)
            {
                _aclMappingRep.Insert(new AclMapping()
                {
                    Company_Id = companyId,
                    CreatedOnUtc = DateTime.UtcNow,
                    Deleted = false,
                    EntityName = entityName,
                    EntityId = entityId,
                    Type = (int)AclMappingType.Department,
                    Value = dep
                });
            }
        }

        public void InsertProjectMapping(int companyId, string entityName, int entityId, int[] projectIds)
        {

            if (projectIds.Any())
                foreach (var dep in projectIds)
            {
                _aclMappingRep.Insert(new AclMapping()
                {
                    Company_Id = companyId,
                    CreatedOnUtc = DateTime.UtcNow,
                    Deleted = false,
                    EntityName = entityName,
                    EntityId = entityId,
                    Type = (int)AclMappingType.Project,
                    Value = dep
                });
            }
        }

        public void InsertUserMapping(int companyId, string entityName, int entityId, int[] userId)
        {
            if (userId.Any())
                foreach (var dep in userId)
            {
                _aclMappingRep.Insert(new AclMapping()
                {
                    Company_Id = companyId,
                    CreatedOnUtc = DateTime.UtcNow,
                    Deleted = false,
                    EntityName = entityName,
                    EntityId = entityId,
                    Type = (int)AclMappingType.User,
                    Value = dep
                });
            }
        }
    }
}
